@file:JvmName("ConvertVariantContextToVariantInfo")
package net.maizegenetics.pangenome.api

import htsjdk.variant.variantcontext.VariantContext
import net.maizegenetics.dna.map.Chromosome


data class ASMVariantInfo(val asmChr: String = "", val asmStart: Int = -1, val asmEnd: Int = -1, val asmStrand:String = ".")
/**
 * Function to convert a VariantContext into a VariantInfo object.
 * Need taxon and gamete id to get the correct genotype from the VariantContext.
 * The gamete ID is used to get the correct allele for the genotype.
 *
 * Using genotype position index instead of taxon.  Otherwise converting context to info
 * when processing consensus fails.  Consensus has a list of taxon.  originally we passed in the
 * first one.  But that could be "ref", and when processing the gvcf file we don't necessarily
 * have ref in there, it may be the gvcf file for another line when they had the same data.
 * Normally, "ref" would be chosen when taxa are equal.  But for smallSeq, the ranking file
 * ends up with Ref as first, so it gets rank 1, which is lowest.
 */
fun convertContextToInfo(variantContext: VariantContext,  gameteId : Int = 0) : HaplotypeNode.VariantInfo {
    val genotype = variantContext.getGenotype(0).getAllele(gameteId).displayString
    val refAllele = variantContext.reference.baseString
    val isRefBlock = isRefBlock(variantContext)

    //handle the case where there is more than one alt allele
    //use Allele.displayString to return symbolic alleles (.baseString returns then as "")
    val firstAltAllele = variantContext.alternateAlleles[0].displayString ?: ""

    val altAllele = when {
        isRefBlock -> ""
        variantContext.alternateAlleles.size == 1 -> firstAltAllele
        genotype == null -> firstAltAllele
        genotype == "" -> firstAltAllele
        genotype == refAllele -> firstAltAllele
        else -> genotype
    }

    val depths = if (isRefBlock) {
        val depthArray = IntArray(1)
        depthArray[0] = variantContext.getGenotype(0).dp
        depthArray
    } else variantContext.getGenotype(0).ad

    val asmInfo = extractASMInfo(variantContext)

    return HaplotypeNode.VariantInfo(
        variantContext.contig,
        variantContext.start,
        variantContext.end,
        genotype,
        refAllele,
        altAllele,
        !isRefBlock,
        depths,
        asmInfo.asmChr,
        asmInfo.asmStart,
        asmInfo.asmEnd,
        asmInfo.asmStrand
    )
}

fun convertGVCFContextToInfo(variantContext: VariantContext, gameteId : Int = 0) : HaplotypeNode.VariantInfo {
    return convertContextToInfo(variantContext, gameteId)
}

/**
 * Function to convert a list of variantContexts into a list of VariantInfos
 */
fun convertVCListToVariantInfoList(variants : List<VariantContext>, taxon: String, gameteId: Int = 0) : List<HaplotypeNode.VariantInfo> {
    return variants.map { convertContextToInfo(it,gameteId) }
            .toList()
}
fun convertGVCFListToVariantInfoList(variants : List<VariantContext>, gameteId: Int = 0) : List<HaplotypeNode.VariantInfo> {
    return variants.map { convertGVCFContextToInfo(it,gameteId) }
            .toList()
}

fun extractASMInfo(variantContext: VariantContext) : ASMVariantInfo {
    val asmChr = variantContext.getAttributeAsString("ASM_Chr","0")
    val asmStart = variantContext.getAttributeAsInt("ASM_Start", 0)
    val asmEnd = variantContext.getAttributeAsInt("ASM_End", 0)
    val asmStrand = variantContext.getAttributeAsString("ASM_Strand", ".")

    return ASMVariantInfo(asmChr, asmStart, asmEnd, asmStrand)
}

/**
 * Helper function to check to see if a gvcf Record represents a reference block or just a single variant.
 */
fun isRefBlock(gvcfRecord : VariantContext) : Boolean {
    return gvcfRecord.hasAttribute("END") && gvcfRecord.getReference().getBaseString().length == 1
}

fun determineASMInfo(list : List<HaplotypeNode.VariantInfo>, refRangeStart:Int, refRangeEnd:Int) : ASMVariantInfo {
    if (list == null || list.size == 0) {
        println("WARNING: ConvertVariantContextToVariantInfo:determineASMINfo has empty variant list, creating default ASMVariantInfo object")
        return ASMVariantInfo("0",0,0,".")
    }
    val firstInfo = list.first()
    val lastInfo = list.last()

    val chrom = firstInfo.asmChr()

    // Adjust the start/end positions based on where they overlap the reference coordinates.
    // When the alignment starts before the beginning of the refRange, the start of a forward
    // strand alignment gets adjusted, while the end of a reverse strand alignment gets adjusted.

    var startPos = firstInfo.asmStart()
    var endPos = if(firstInfo.asmChr() == lastInfo.asmChr()) lastInfo.asmEnd() else 0

    //Adjusting the start for a forward strand
    if (refRangeStart > firstInfo.start() && list.first().asmStrand() == "+") {
        startPos = startPos + (refRangeStart - firstInfo.start())
    }
    // On the reverse strand, it is the end that gets truncated when the
    // refRange start is later than the first element of the list starts
    if (refRangeStart > lastInfo.end() && lastInfo.asmStrand() == "-") {
        if (endPos > 0) endPos = endPos + (refRangeStart - lastInfo.end())
    }

    // When the alignment ends after the end of the refRange, the end of a forward strand
    // alignment gest adjusted, while the start of a reverse strand alignment is adjusted.
    if (refRangeEnd < lastInfo.end() && list.last().asmStrand() == "+") {
        if (endPos > 0) endPos = endPos - (lastInfo.end() - refRangeEnd)
    }

    if (refRangeEnd < lastInfo.start() && lastInfo.asmStrand() == "-"){
        startPos = startPos - (startPos - refRangeEnd)
    }

    val strand = if(firstInfo.asmChr() == lastInfo.asmChr() && firstInfo.asmStrand() == lastInfo.asmStrand()) firstInfo.asmStrand() else "."

    return ASMVariantInfo(chrom, startPos, endPos, strand)
}
