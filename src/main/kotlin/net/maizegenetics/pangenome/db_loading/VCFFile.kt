@file:JvmName("VCFFile")

package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimaps
import htsjdk.variant.vcf.VCFFileReader
import java.io.File

private val vcfFileCache = Multimaps.synchronizedListMultimap(ArrayListMultimap.create<String, VCFFileReader>())

/**
 * Gets a VCFFileReader for the given file.  If the file is already open, it will return the existing reader.
 * readerReturned is used to return it to the cache for use by other threads.
 */
@Synchronized
fun vcfFileReader(vcfFile: String, readerReturned: VCFFileReader? = null): VCFFileReader? {

    return if (readerReturned == null) {

        val cached = vcfFileCache.get(vcfFile)

        if (cached != null && cached.isNotEmpty()) {
            cached.removeFirst()
        } else {
            try {
                val vcfIndex = "$vcfFile.tbi"
                VCFFileReader(File(vcfFile), File(vcfIndex))
            } catch (e: Exception) {
                val filesInDirectory = File(vcfFile).parentFile.walk().asSequence().map { it.toString() }.joinToString(":")
                throw IllegalStateException("VCFFile: vcfFileReader: Problem getting reader for $vcfFile. Exception: ${e.message}.\nFiles in directory: $filesInDirectory")
            }
        }

    } else {
        vcfFileCache.put(vcfFile, readerReturned)
        null
    }

}
