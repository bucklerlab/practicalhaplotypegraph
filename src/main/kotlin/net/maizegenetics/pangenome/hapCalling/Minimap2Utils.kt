@file:JvmName("Minimap2Utils")

package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import htsjdk.samtools.*
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.prefs.TasselPrefs
import net.maizegenetics.util.DirectoryCrawler
import net.maizegenetics.util.Utils
import java.io.BufferedInputStream
import java.io.File
import java.nio.file.Paths
import kotlin.collections.HashSet
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import org.apache.logging.log4j.LogManager
import java.lang.IllegalStateException
import java.nio.file.Path


/**
 * Utility class holding various functions used by the minimap2 processing pipeline.
 * Each function itself should have a better description of what it does.
 */
private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.hapCalling.Minimap2Utils")

data class BestAlignmentGroup(val readName: String, val strand : Boolean, val bestEditDistance:Int, val lengthOfMapping: Int, val listOfHapIds: MutableSet<Int>, val hapIdToSecondaryStats: MutableMap<Int,SecondaryStats>)
data class SecondaryStats(val AS: Int, val de: Float, val startPosition : Int)
data class SingleHapMapping(val readName: String, val hapIdSet: Set<Int>)
data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
data class HapIdSetCount(val hapIdSet : Set<Int>, val count : Int)
data class HapIdStats(val hapId : Int, var count : Int, var NM : Int, var AS : Int, var de : Float, var startPositions : MutableList<Int>) {
    fun addStats(newCount : Int, newNM : Int, newAS : Int, newDE : Float, newStart : Int) {
        count += newCount
        NM += newNM
        AS += newAS
        de += newDE
        startPositions.add(newStart)
    }
}

enum class ReadMappingInputFileFormat {
    FASTQ,SAM
}

/**
 * Function to create a HapId to RefRange mapping for all of the HapIds in a graph
 */
fun getHapToRefRangeMap(graph: HaplotypeGraph) : Map<Int, ReferenceRange> {
    return graph.referenceRanges()
            .flatMap { graph.nodes(it) }
            .map { Pair(it.id(),it.referenceRange()) }
            .toMap()
}
fun getHapIdToSequenceLength(graph: HaplotypeGraph) : Map<Int,Int> {
    return graph.referenceRanges()
            .flatMap { graph.nodes(it) }
            .map { Pair(it.id(),it.haplotypeSequence().length()) }
            .toMap()
}

fun getRefRangeToHapidMap(graph: HaplotypeGraph) : Map<Int,Map<Int,Int>>{
    //This creates a map of ReferenceRangeId -> (map of hapid -> index)
    return graph.referenceRanges().associateBy(
        {refrange -> refrange.id()},
        { refrange ->
            graph.nodes(refrange).map { it.id() }.sorted()
                .mapIndexed { index, hapid ->  Pair(hapid, index)}
                .toMap()
        }
    )
}

fun getHaplotypeListIdForGraph(graph: HaplotypeGraph, phgAccessor: PHGdbAccess) : Int {
    val haplotypeIdList = graph.referenceRangeList().map { rr -> graph.nodes(rr).map { node -> node.id() } }.flatten()
    return phgAccessor.putHalotypeListData(haplotypeIdList)
}

fun filterRead(currentSamRecord: SAMRecord, pairedEnd:Boolean, clippingFilter :Boolean = true) : Boolean {
    //Ignore unmapped reads
    if(currentSamRecord.readUnmappedFlag) {
        return true
    }
    if(pairedEnd) {
        try {
            //Ignore mappings where pair of reads are on the same strand
            if (currentSamRecord.mateNegativeStrandFlag == currentSamRecord.readNegativeStrandFlag) {
                return true
            }
        }
        catch(e: Exception) {
            myLogger.info("Error processing read ${currentSamRecord.readName}.  Unable to properly pair the reads.")
            throw e
        }

    }

    //Ignore clipped reads
    if(clippingFilter && currentSamRecord.cigar.isClipped) {
        return true
    }

    return false
}

data class ReadMappingKeyFileParsed(val keyFileRecordsToFileMap : Map<KeyFileUniqueRecord,Pair<String,String>>,
                                    val fileNameToPathMap : Map<String, Path>,
                                    val keyFileColumnNameMap : Map<String,Int>,
                                    val keyFileLines : List<List<String>>)

fun loadInReadMappingKeyFile(keyFileName: String,
                             inputFileFormat: ReadMappingInputFileFormat,
                             inputFileDir: String,
                             methodName: String) : ReadMappingKeyFileParsed {
    val columnMappingAndLines = readInKeyFile(keyFileName)
    val keyFileColumnNameMap = columnMappingAndLines.first
    val keyFileLines = columnMappingAndLines.second

    //Get out the column indices as they will be consistent for the whole file
    val taxonCol = keyFileColumnNameMap["cultivar"]?:-1
    val fileNameCol1 = keyFileColumnNameMap["filename"]?:-1
    val fileNameCol2 = if (keyFileColumnNameMap.containsKey("filename2")) keyFileColumnNameMap["filename2"]?:-1 else -1
    val flowcellCol = keyFileColumnNameMap["flowcell_lane"]?:-1

    //Check will automatically throw an IllegalStateException if the logic fails.
    check(taxonCol != -1) { "Error processing keyfile.  Must have cultivar column." }
    check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
    check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }

    val globString = when(inputFileFormat) {
        ReadMappingInputFileFormat.FASTQ -> "glob:*{.fastq.gz,.fq.gz,.fastq,.fq}"
        ReadMappingInputFileFormat.SAM -> "glob:*{.sam,.bam}"
    }

    val directoryFilePaths = DirectoryCrawler.listPaths(globString, Paths.get(inputFileDir))
    val fileNameToPathMap = directoryFilePaths.associateBy { it.fileName.toString() }
    val directoryFileNames = fileNameToPathMap.keys.toHashSet()

    val keyFileRecordsToFileMap = mutableMapOf<KeyFileUniqueRecord,Pair<String,String>>()
    val keyFileRecordsToEntry = mutableMapOf<List<String>, KeyFileUniqueRecord>()
    val keyFileRecords = keyFileLines
        .filter { isKeyEntryInDir(directoryFileNames,it,fileNameCol1, fileNameCol2) }
        .map {
            val file1 = it[fileNameCol1]
            val file2 = if (fileNameCol2 != -1) it[fileNameCol2] else ""
            val taxon = it[taxonCol]
            //fileGrpName = file names because sometimes user does not know plate and flow cell
            val uniqueRecord = KeyFileUniqueRecord(methodName, taxon, it[flowcellCol])

            keyFileRecordsToFileMap[uniqueRecord] = Pair(file1,file2)
            keyFileRecordsToEntry[it] = uniqueRecord
            uniqueRecord
        }
        .toList()

    verifyNoDuplicatesInKeyFile(keyFileRecords)
    return ReadMappingKeyFileParsed(keyFileRecordsToFileMap,fileNameToPathMap,keyFileColumnNameMap,keyFileLines)
}


/**
 * Function to load the SAM file into a SAM reader
 * Could remove this, but we should leave it in as you may need to read from a file in the future.
 */
fun loadSAMFileIntoSAMReader(fileName:String) : SamReader {
    return SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT)
            .open(File(fileName))
}

/**
 * Method to run minimap2 using information provided by the Key file.
 *
 * This assumes that there must be a column named filename and if paired end is requested a column named filename2.
 *
 * There are additional pieces of information that will also be provided by the keyfile, but will be implemented once the DB changes are implemented.
 *
 */
fun runMinimapFromKeyFile(minimapLocation: String, keyFileName:String,
                          inputFileDir : String, referenceFile: String,
                          graph: HaplotypeGraph?,
                          maxRefRangeError: Double,
                          methodName : String, methodDescription: String?,
                          pluginParams: Map<String,String>,
                          outputDebugReadMappingDir : String,
                          outputSecondaryMappingStats : Boolean = false,
                          maxSecondary: Int = 20,
                          inputFileFormat: ReadMappingInputFileFormat = ReadMappingInputFileFormat.FASTQ,
                          fParameter: String = "f1000,5000",
                          isTestMethod: Boolean,
                          updateDB: Boolean = true,
                          runWithoutGraph : Boolean = false,
                          hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                          hapIdToLengthMap:Map<Int,Int>,
                          refRangeToHapIdMap: Map<Int, Map<Int, Int>>,
                          inputFileName: String="") {

    if(!runWithoutGraph && graph == null) {
        throw IllegalStateException("Cannot run without graph and not provide a graph.")
    }
    else if(graph != null) {
        myLogger.info("Running with graph a graph Object regardless of runWithoutGraph Parameter")
    }
    else {
        myLogger.info("Running without graph: $runWithoutGraph")
    }
    val phg = if(!runWithoutGraph) {
        var dbConnect = DBLoadingUtils.connection(false)
        PHGdbAccess(dbConnect)
    }
    else {
        null
    }

    val parsedKeyFile = loadInReadMappingKeyFile(keyFileName, inputFileFormat, inputFileDir ,methodName)
    val keyFileRecordsToFileMap = parsedKeyFile.keyFileRecordsToFileMap
    val fileNameToPathMap = parsedKeyFile.fileNameToPathMap
    val keyFileRecordsToMappingId = mutableMapOf<KeyFileUniqueRecord,Int>()

    val haplotypeListId = if(!runWithoutGraph) {
        getHaplotypeListIdForGraph(graph!!, phg!!) //We can force the !! as we know both graph and phg is not null
    }
    else {
        -1
    }
    // Update the read mapping hash - this is used by getReadMappingId() in the loop below
    // This assumes there  will be no duplicates in this file we are processing. We are only
    // updating the hash here - not after each submission below.
    // Inside outputKeyFiles() it will be run again to pick up the new readMappingIds that
    // were created below.
    if(!runWithoutGraph) {
        var time = System.nanoTime();
        myLogger.info("runMinimapFromKeyFile: calling updateReadMappingHash()")
        phg!!.updateReadMappingHash()  //Can do !! as we know it is not null

        var hashLoadTime = (System.nanoTime() - time) / 1e9
        myLogger.info("runMinimapFromKeyFile, updateReadMappingHash took ${hashLoadTime} seconds")
    }
    keyFileRecordsToFileMap.keys.map { Pair(it,keyFileRecordsToFileMap[it]) }
        .filter { inputFileName=="" || File("${inputFileDir}/${it.second?.first}")==File(inputFileName) }
        .forEach {
            val keyFileRecord = it.first
            val taxon = keyFileRecord.taxonName
            val fileGroupName = keyFileRecord.fileGroupName
            val file1 = it.second?.first ?: ""
            val file2 = it.second?.second ?: ""

            check(file1 != "") { "file1 is missing.  Check your keyfile: cultivar ${taxon}, flowcell_lane ${fileGroupName} " }

            //Check to see if the read mapping is already stored in the db.
            val readMappingId = if(!runWithoutGraph) {
                phg!!.getReadMappingId(taxon, methodName, fileGroupName)
            }
            else {
                -1
            }

            //If the id == -1, it means the read mapping is not in the db, we need to process.
            if(readMappingId == -1) {
                myLogger.info("Setting up MinimapRun for: cultivar ${taxon}, flowcell_lane ${fileGroupName}.")
                val startTimeSetUpRun = System.nanoTime()

                val samReader = when(inputFileFormat) {
                    ReadMappingInputFileFormat.FASTQ -> setupMinimapRun(minimapLocation, referenceFile, fileNameToPathMap[file1].toString(), if(file2!="") fileNameToPathMap[file2].toString() else "", maxSecondary, fParameter)
                    ReadMappingInputFileFormat.SAM -> loadSAMFileIntoSAMReader(fileNameToPathMap[file1].toString())
                }


                myLogger.info("Time spent setting up run: Taxon:${taxon} : ${(System.nanoTime() - startTimeSetUpRun)/1E9}sec")

                val keyFileObj = File(keyFileName)

                val keyFileNameWithoutExtension = keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
                val outputHapIdToStatsFile = if(outputSecondaryMappingStats) {"${keyFileNameWithoutExtension}_${taxon}_${fileGroupName}_additionalMappingStats.txt" } else { "" }

                val processingSamFileStartTime = System.nanoTime()
                val hapIdMapping = scoreSamFileCountHapSetHits(samReader, hapIdToRefRangeMap, maxRefRangeError, (file2 != ""),outputHapIdToStatsFile,hapIdToLengthMap) //if file2 =="" it is single end so paired = false
                myLogger.info("Time spent processing SAM: Taxon:${taxon} : ${(System.nanoTime() - processingSamFileStartTime)/1E9}sec")

                if(hapIdMapping.size == 0) {
                    myLogger.info("No reads have mapped for: cultivar ${taxon}, flowcell_lane ${fileGroupName}")
                }
                else {
                    if(!runWithoutGraph) {
                        myLogger.info("Done running minimap2 for: cultivar ${taxon}, flowcell_lane ${fileGroupName}")
                        loadBitSetEncodedReadMappingsToDB(
                            hapIdMapping,
                            taxon,
                            fileGroupName,
                            pluginParams,
                            methodDescription,
                            phg!!,
                            methodName,
                            keyFileRecordsToMappingId,
                            keyFileRecord,
                            outputDebugReadMappingDir,
                            hapIdToRefRangeMap,
                            refRangeToHapIdMap,
                            haplotypeListId,
                            isTestMethod,
                            updateDB
                        )
                    }
                    else {
                        myLogger.info("Done running minimap2 for: cultivar ${taxon}, flowcell_lane ${fileGroupName}")
                        saveBitSetEncodedReadMappingsToFile(hapIdMapping, taxon, fileGroupName, pluginParams, methodDescription, methodName, outputDebugReadMappingDir, hapIdToRefRangeMap, refRangeToHapIdMap)

                    }
                }
            }
            else {
                myLogger.info("Skipping Keyfile entry: cultivar ${taxon}, flowcell_lane $fileGroupName has already been processed and loaded into the DB.")
            }
        }

    if(!runWithoutGraph) {
        //Output the key files
        outputKeyFiles(keyFileName, parsedKeyFile.keyFileLines, parsedKeyFile.keyFileColumnNameMap["flowcell_lane"]!!, phg!!, parsedKeyFile.keyFileColumnNameMap["cultivar"]!!, methodName, parsedKeyFile.keyFileColumnNameMap)

        phg!!.close()
    }
}

/**
 * Function to output mapping id and path key files to be used by the path finding.
 */
fun outputKeyFiles(keyFileName: String, keyFileLines: List<List<String>>, flowcellCol: Int, phg: PHGdbAccess, taxonCol: Int, methodName: String, keyFileColumnNameMap: Map<String, Int>) {
    //Create the outputFileNames
    val keyFileObj = File(keyFileName)

    //Check to see if the folder path is included.
    //If it is not included, keyFileObj.parent will return a null so we can just preappend ./ to the filename.
    val keyFileNameWithoutExtension = if (keyFileObj.parent != null) {
        keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
    } else {
        "./" + keyFileObj.nameWithoutExtension
    }

    val outputMappingKeyFile = "${keyFileNameWithoutExtension}_withMappingIds.txt"
    val outputPathKeyFile = "${keyFileNameWithoutExtension}_pathKeyFile.txt"

    //Export the mapping ids
    //keyFileColumnNameMap is a Map<String,Int> of name, column index
    //keyFileLines is a List of List<String> with the split contents of each line in the key file

    // Update the hash so all readmapping ids are known.
    // This will pickup all the read mappings created from the calling method.
    // If we don't call updateReadMappingHash() here, getReadMappingId() will
    // query the DB for each readmapping that isn't present.
    // That might be quicker if the number of readmappings just entered is low
    var time = System.nanoTime();
    myLogger.info("outputKeyFiles: calling updateReadMappingHash()")
    phg.updateReadMappingHash()
    var hashLoadTime = (System.nanoTime() - time)/1e9
    myLogger.info("outputKeyFiles, updateReadMappingHash took ${hashLoadTime} seconds")

    //export each row of the keyfile with the read mapping id appended
    val keyFileLinesWithId = keyFileLines.map { keyFileRow ->
        val fileGroup = keyFileRow[flowcellCol]
        val id = phg.getReadMappingId(keyFileRow[taxonCol], methodName, fileGroup)
        Pair(keyFileRow, id)
    }.filter { it.second > -1 }

    Utils.getBufferedWriter(outputMappingKeyFile).use { output ->
        val getColumnResorted = mutableListOf<String>()
        keyFileColumnNameMap.keys.forEach { keyColumnName ->
            val columnIndex = keyFileColumnNameMap[keyColumnName]
            check(columnIndex != null) { "Column name ${keyColumnName} does not exist in the map" }
            getColumnResorted.add(columnIndex, keyColumnName)
        }

        output.write("${getColumnResorted.joinToString("\t")}\treadMappingIds\n")
        keyFileLinesWithId.forEach { output.write("${it.first.joinToString("\t")}\t${it.second}\n") }
    }

    //if there are duplicate entries in the key file (same taxon AND file group), that results in
    // multiple entries in the keyFileLinesWithId with the same taxon, file group, and read mapping id. As a result,
    // it is necessary to eliminate potential duplicates in the read mapping id lists by converting those lists to sets.
    val taxonToHapIds = keyFileLinesWithId.groupBy({ it.first[taxonCol] }, { it.second })
        .mapValues { (_, ids) -> ids.toSet() }


    //Write out a sample path key file, one row per taxon in the original key file
    Utils.getBufferedWriter(outputPathKeyFile).use { output ->
        output.write("SampleName\tReadMappingIds\tLikelyParents\n")
        taxonToHapIds.keys.forEach { taxon ->
            val hapIds = taxonToHapIds[taxon] ?: listOf()
            output.write("${taxon}\t${hapIds.joinToString(",")}\t\t\n")
        }
    }
}

/**
 * Function to load the Read Mappings to the DB.  This will encode the ReadMappings and the load them in given the provided information.
 */


fun loadReadMappingsToDB(hapIdMapping: Map<List<Int>, Int>,
                                 taxon: String,
                                 fileGroupName: String,
                                 pluginParams: Map<String, String>,
                                 methodDescription: String?,
                                 phg: PHGdbAccess,
                                 methodName: String,
                                 keyFileRecordsToMappingId: MutableMap<KeyFileUniqueRecord, Int>,
                                 keyFileRecord: KeyFileUniqueRecord,
                                 outputDebugReadMappingDir: String,
                                 isTestMethod:Boolean,
                                 hapListId: Int,
                                 hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                 refRangeToHapIdMap: Map<Int, Map<Int,Int>>) {

    myLogger.info("Compressing ReadMappings into GZipped Json.")
    //Encode the HapIdMapping into the serialized format
    val encodedReadMapping = encodeHapIdsAsBitSet(hapIdMapping, hapIdToRefRangeMap, refRangeToHapIdMap)
    check(encodedReadMapping != null) { "Error Encoding Read Mappings: cultivar ${taxon}, flowcell_lane ${fileGroupName}" }
    myLogger.info("Done Compressing ReadMappings.  Pushing it to the DB.")

    val finalParamMap = pluginParams.toMutableMap()
    if (methodDescription != null) finalParamMap.put("notes", methodDescription)

    //Create object to upload to the DB and return it.
    val readMappingId = phg.putReadMappingData(methodName, finalParamMap, taxon, fileGroupName, encodedReadMapping, isTestMethod, hapListId)

    keyFileRecordsToMappingId[keyFileRecord] = readMappingId

    if (outputDebugReadMappingDir != "") {
        // SOme of our cultivars have a / in their names, which is a problem
        // when the cultivar is used in the file name as below
        // This should not change the value of taxon
        var fileNameTaxon = taxon.replace("/","-")
        val outputFileName = "${outputDebugReadMappingDir}/${fileNameTaxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))
    }

    myLogger.info("Done processing cultivar ${taxon}, flowcell_lane ${fileGroupName}.  Moving on to next keyfile Entry.")
}

/**
 * Function to load the Read Mappings to the DB.  This will encode the ReadMappings and the load them in given the provided information.
 */
fun loadBitSetEncodedReadMappingsToDB(hapIdMapping: Map<List<Int>, Int>,
                                              taxon: String,
                                              fileGroupName: String,
                                              pluginParams: Map<String, String>,
                                              methodDescription: String?,
                                              phg: PHGdbAccess,
                                              methodName: String,
                                              keyFileRecordsToMappingId: MutableMap<KeyFileUniqueRecord, Int>,
                                              keyFileRecord: KeyFileUniqueRecord,
                                              outputDebugReadMappingDir: String,
                                              hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                              refRangeToHapIdMap: Map<Int, Map<Int,Int>>,
                                              hapListId: Int,
                                              isTestMethod: Boolean,
                                              updateDB: Boolean = true) {
    myLogger.info("Compressing ReadMappings into GZipped Json.")
    //Encode the HapIdMapping into the serialized format
    val encodedReadMapping = encodeHapIdsAsBitSet(hapIdMapping, hapIdToRefRangeMap, refRangeToHapIdMap)
    check(encodedReadMapping != null) { "Error Encoding Read Mappings: cultivar ${taxon}, flowcell_lane ${fileGroupName}" }
    myLogger.info("Done Compressing ReadMappings.  Pushing it to the DB.")

    val finalParamMap = pluginParams.toMutableMap()
    if (methodDescription != null) finalParamMap.put("notes", methodDescription)

    //If the updateDB flag is on, we need to write to the db.
    if(updateDB) {
        //Create object to upload to the DB and return it.
        val readMappingId = phg.putReadMappingData(
            methodName,
            finalParamMap,
            taxon,
            fileGroupName,
            encodedReadMapping,
            isTestMethod,
            hapListId
        )

        keyFileRecordsToMappingId[keyFileRecord] = readMappingId
    }
    if (outputDebugReadMappingDir != "") {
        // SOme of our cultivars have a / in their names, which is a problem
        // when the cultivar is used in the file name as below
        // This should not change the value of taxon
        var fileNameTaxon = taxon.replace("/","-")
        val outputFileName = "${outputDebugReadMappingDir}/${fileNameTaxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))
    }

    myLogger.info("Done processing cultivar ${taxon}, flowcell_lane ${fileGroupName}.  Moving on to next keyfile Entry.")
}

fun saveBitSetEncodedReadMappingsToFile(hapIdMapping: Map<List<Int>, Int>,
                                              taxon: String,
                                              fileGroupName: String,
                                              pluginParams: Map<String, String>,
                                              methodDescription: String?,
                                              methodName: String,
                                              outputDebugReadMappingDir: String,
                                              hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                              refRangeToHapIdMap: Map<Int, Map<Int,Int>>) {
    myLogger.info("Compressing ReadMappings into GZipped Json.")
    //Encode the HapIdMapping into the serialized format
    val encodedReadMapping = encodeHapIdsAsBitSet(hapIdMapping, hapIdToRefRangeMap, refRangeToHapIdMap)
    check(encodedReadMapping != null) { "Error Encoding Read Mappings: cultivar ${taxon}, flowcell_lane ${fileGroupName}" }
    myLogger.info("Done Compressing ReadMappings.  Pushing it to the DB.")

    val finalParamMap = pluginParams.toMutableMap()
    if (methodDescription != null) finalParamMap.put("notes", methodDescription)

    if (outputDebugReadMappingDir != "") {
        // SOme of our cultivars have a / in their names, which is a problem
        // when the cultivar is used in the file name as below
        // This should not change the value of taxon
        var fileNameTaxon = taxon.replace("/","-")
        val outputFileName = "${outputDebugReadMappingDir}/${fileNameTaxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
            DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))
    }

    myLogger.info("Done processing cultivar ${taxon}, flowcell_lane ${fileGroupName}.  Moving on to next keyfile Entry.")
}


/**
 * Function to read in the key file.  The first of the pair is the column mapping and the second is a 2-d list.
 */
fun readInKeyFile(fileName: String) : Pair<Map<String,Int>,List<List<String>>> {
    val lines = Utils.getBufferedReader(fileName).readLines()

    val cols = lines[0].split("\t")
    val columnMapping = cols.indices.map { Pair(cols[it],it) }.toMap()

    val linesSplit = (1 until lines.size).map { lines[it] }
            .filter { it != "" }
            .map { it.split("\t") }
            .toList()

   return Pair(columnMapping,linesSplit)
}

/**
 * verify that there are no duplicate entries in the key file.  This is just to let the user know if there is a duplicate.
 */
fun verifyNoDuplicatesInKeyFile(keyFileRecords : List<KeyFileUniqueRecord>) {
    val keySet = mutableSetOf<KeyFileUniqueRecord>()
    for(record in keyFileRecords) {
        if(keySet.contains(record)) {
            myLogger.warn("Duplicate KeyFile Entry Found in the Keyfile:${record.taxonName} ${record.fileGroupName} Using the last entry in the keyfile.")
        }
        keySet.add(record)
    }
}

/**
 * Method to check to see if there are missing files found in the key file but are missing in the Directory.
 * If they are its ok, we expect the keyfile to have more entries than the directory.
 */
fun isKeyEntryInDir(fileNames : HashSet<String>, currentKeyRecord : List<String>, fileCol1 : Int, fileCol2 : Int) : Boolean {
    val file1 = currentKeyRecord[fileCol1]
    return if(fileCol2 == -1) {
        val isValidFilename = fileNames.contains(file1)
        if (!isValidFilename) myLogger.error("input directory does not contain $file1. The directory name should not be part of the file name in the keyfile.")
        isValidFilename
    } else {
        val file2 = currentKeyRecord[fileCol2]
        //condition file2=="" allows filename2 to be blank.
        val areValidFilenames = fileNames.contains(file1) && (file2 == "" || fileNames.contains(file2))
        if (!areValidFilenames) myLogger.error("input directory does not contain both of $file1 and $file2. The directory name should not be part of the filename in the keyfile.")
        areValidFilenames
    }
}

/**
 * Function that sets up the paired or single end minimap commands and builds the SamReader
 *
 * The SamReader is used later to pick optimal hits from the alignments.
 *
 * If secondFastq is "", this will assume that single end is what needs to be run
 */
fun setupMinimapRun(minimapLocation : String, referenceFile : String, firstFastq:String, secondFastq:String = "", maxSecondary : Int = 20, fParameter : String = "f1000,5000") : SamReader {
    val threadnum = Integer.toString(Math.max(TasselPrefs.getMaxThreads() - 1, 1))
    val command = if (secondFastq == "")  arrayOf(minimapLocation, "-ax", "sr", "-t", threadnum, "--secondary=yes","-N$maxSecondary", "-${fParameter}", "--eqx", referenceFile, firstFastq)
                    else arrayOf(minimapLocation, "-ax", "sr", "-t", threadnum,"--secondary=yes", "-N$maxSecondary", "-${fParameter}", "--eqx", referenceFile, firstFastq,secondFastq)

    myLogger.info("Running Minimap2 Command:\n${command.joinToString(" ")}")
    val minimap2Process = ProcessBuilder(*command)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
    val minimap2Out = BufferedInputStream(minimap2Process.inputStream, 5000000)

    //create a SamReader from that
    return SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT).open(SamInputResource.of(minimap2Out))
}

/**
 * Function to score a sam record.  This will output a Map<List<Int>,Int> which is the hapId Hit set.
 *
 * For each subset of hapids we return a count of how many reads hit that exact subset.  This mapping is used in the HMM.
 */
fun scoreSamFileCountHapSetHits(samReader: SamReader, hapIdToRefRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double, pairedEnd :Boolean, outputDebugFile : String, hapIdToLengthMap : Map<Int,Int> ) : Map<List<Int>,Int> {
    myLogger.info("Running in Low memory mode.  Simply counting the number of reads which hit a given set of haplotype ids")

    var currentStoredReadName = ""
    val samIterator = samReader.iterator()

    var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
    var mappingCounter = 0

    val hapIdMultiset = HapIdMultiset()

    val hapIdToStatMap = mutableMapOf<Int,HapIdStats>()
    var totalNextTimeList = mutableListOf<Double>()
    while (samIterator.hasNext()) {
        val nextStartTime = System.nanoTime()
        val currentSamRecord = samIterator.next()
        mappingCounter++
        if (mappingCounter % 1000000 == 0) {
            myLogger.info("Processed ${mappingCounter} alignments.")
        }

        if (filterRead(currentSamRecord, pairedEnd)) {
            continue
        }
        val readName = currentSamRecord.readName
        //Set the read name for the first mapping in the iterator
        if (currentStoredReadName == "") {
            currentStoredReadName = readName
        }
        if (readName != currentStoredReadName) {
            addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)
            //reset the map and current name
            currentStoredReadName = readName //update the read name
            bestReadMap = mutableMapOf() //reset the bestReadMap so we can add new entries to it
        }

        attemptToAddSAMRecordToBestReadMap(pairedEnd, currentSamRecord, readName, bestReadMap)
    }
    //Need to do it one last time to be sure we process the last read
    addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)



    myLogger.info("TotalTiming For samReader.next:${totalNextTimeList.sum()}")
//    println(totalNextTimeList.joinToString(","))
    //Exporting hapId->Stats
    exportHapIdStats(outputDebugFile, hapIdToStatMap, hapIdToLengthMap)

    val multisetMap = hapIdMultiset.getMap()
    return multisetMap.keys.map { Pair(it.toList(),multisetMap[it]?:0) }.toMap()
}

/**
 * Function to export the HapIdStats file.  If outputDebugFile is not specified, this will do nothing.
 */
fun exportHapIdStats(outputDebugFile: String, hapIdToStatMap: MutableMap<Int, HapIdStats>, hapIdToLengthMap: Map<Int, Int>) {
    if (outputDebugFile != "") {
        Utils.getBufferedWriter(outputDebugFile).use { out ->
            out.write("hapId\thapLength\tcount\ttotalNM\ttotalAS\ttotalDE\tlistOfStartPos\n")
            hapIdToStatMap.keys
                    .map { Pair(hapIdToStatMap[it]!!, hapIdToLengthMap[it]) }
                    .forEach { statsPair ->
                        val stats = statsPair.first
                        val hapLength = statsPair.second
                        out.write("${stats.hapId}\t${hapLength}\t${stats.count}\t${stats.NM}\t${stats.AS}\t${stats.de}\t${stats.startPositions.joinToString(",")}\n")
                    }
        }
    }
}

/**
 * Function to try to add a SAM record to bestReadMap.
 * bestReadMap holds the currently best know set of reads which have the best(lowest) NM.
 * If the current Record is better, we replace the old entry with a new one.
 * If the current Record is the same, we add it to the list
 * If the current Record is worse, we ignore.
 */
fun attemptToAddSAMRecordToBestReadMap(pairedEnd: Boolean, currentSamRecord: SAMRecord, readName: String, bestReadMap: MutableMap<Pair<String, Boolean>, BestAlignmentGroup>) {
    val isNegStrand = if (!pairedEnd) pairedEnd else currentSamRecord.readNegativeStrandFlag
    val bestReadMapKey = Pair(readName, isNegStrand)

    //Check to see if its in the map
    if (!bestReadMap.containsKey(bestReadMapKey)) {
        bestReadMap[bestReadMapKey] = BestAlignmentGroup(readName, isNegStrand,
                currentSamRecord.getIntegerAttribute("NM"),
                currentSamRecord.readLength,
                mutableSetOf(currentSamRecord.contig.toInt()), mutableMapOf(Pair(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                currentSamRecord.getAttribute("de") as Float,
                currentSamRecord.alignmentStart)))) //THis assumes that we use the hapID as the haplotype name
    } else {
        //We have the key already in our map, so we need to see if the edit distance is lower
        val currentBestAlignmentGroup = bestReadMap[bestReadMapKey]

        val currentEditDistance = currentSamRecord.getIntegerAttribute("NM")
        val currentBestEditDistance = currentBestAlignmentGroup?.bestEditDistance ?: Int.MAX_VALUE
        if (currentEditDistance < currentBestEditDistance) {
            //Replace this group with the new mapping
            bestReadMap.replace(bestReadMapKey, BestAlignmentGroup(readName, isNegStrand,
                    currentSamRecord.getIntegerAttribute("NM"),
                    currentSamRecord.readLength,
                    mutableSetOf(currentSamRecord.contig.toInt()), mutableMapOf(Pair(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                    currentSamRecord.getAttribute("de") as Float,
                    currentSamRecord.alignmentStart)))))
        } else if (currentEditDistance == currentBestEditDistance) {
            //Add this id to the list
            val currentBestGroupList = currentBestAlignmentGroup?.listOfHapIds ?: mutableSetOf()
            if (!currentBestGroupList.contains(currentSamRecord.contig.toInt())) {
                (currentBestAlignmentGroup?.hapIdToSecondaryStats
                        ?: mutableMapOf()).put(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                        currentSamRecord.getAttribute("de") as Float,
                        currentSamRecord.alignmentStart))
            }
            currentBestGroupList.add(currentSamRecord.contig.toInt())

        }

    }
}

fun addBestReadMapToHapIdMultiset(bestReadMap: MutableMap<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRefRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double, pairedEnd: Boolean, hapIdMultiset: HapIdMultiset, hapIdToStatMap: MutableMap<Int, HapIdStats>) {
    //Need to filter out hapids which span multiple reference ranges
    val bestHitMap = keepHapIdsForSingleRefRange(bestReadMap, hapIdToRefRangeMap, maxRefRangeError)

    //should only be one read name, but this code works for the full mem so I copied it and adapted
    val readNames = bestHitMap.keys
            .map { it.first }
            .toSet()

    if (pairedEnd) {
        convertHitMapToSingleHapMapping(readNames, bestHitMap, hapIdToRefRangeMap,
                { name, hitMap -> hitMap.containsKey(Pair(name, true)) && hitMap.containsKey(Pair(name, false)) },
                { name, hitMap ->
                    val forwardMappings = hitMap[Pair(name, false)]?.listOfHapIds ?: listOf<Int>()
                    val reverseMappings = hitMap[Pair(name, true)]?.listOfHapIds ?: listOf<Int>()
                    Pair(name, forwardMappings.intersect(reverseMappings))
                })
                .forEach {
//                            out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                    hapIdMultiset.add(it.second.hapIdSet)
                    it.second.hapIdSet.forEach { hapId ->

                        val secondaryStats = bestHitMap[Pair(readNames.first(), false)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, false)
                        val secondaryStatsForward = bestHitMap[Pair(readNames.first(), true)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, true)
                    }
                }
    } else {
        convertHitMapToSingleHapMapping(readNames, bestHitMap, hapIdToRefRangeMap,
                { name, hitMap -> hitMap.containsKey(Pair(name, false)) },//Create a filtering lambda and pass it
                { name, hitMap ->
                    Pair(name, hitMap[Pair(name, false)]?.listOfHapIds ?: setOf<Int>())
                }) //Create a mapping lambda and pass it
                .forEach {
//                            out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                    hapIdMultiset.add(it.second.hapIdSet)
                    it.second.hapIdSet.forEach { hapId ->

                        val secondaryStats = bestHitMap[Pair(readNames.first(), false)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, false)
                    }
                }
    }
}

/**
 * Function to add the secondary stats to the hapIdToStatMap even when the read is not already there.
 */
private fun addSecondaryStats(hapIdToStatMap: MutableMap<Int, HapIdStats>, hapId: Int, bestHitMap: Map<Pair<String, Boolean>, BestAlignmentGroup>, readNames: Set<String>, secondaryStats: SecondaryStats?, strand: Boolean) {
    if (!hapIdToStatMap.containsKey(hapId)) {
        hapIdToStatMap[hapId] = HapIdStats(hapId, 1, bestHitMap[Pair(readNames.first(), strand)]?.bestEditDistance ?: 0,
                secondaryStats?.AS ?: 0, secondaryStats?.de ?: 1.0f, mutableListOf(secondaryStats?.startPosition ?: -1))
    } else {
        hapIdToStatMap[hapId]?.addStats(1, bestHitMap[Pair(readNames.first(), strand)]?.bestEditDistance ?: 0,
                secondaryStats?.AS ?: 0, secondaryStats?.de ?: 1.0f, secondaryStats?.startPosition ?: -1)
    }
}

/**
 * Function to keep hapids if they are hitting multiple reference ranges too frequently.  If there is a little bit of noise it can be filtered.
 */
fun keepHapIdsForSingleRefRange(bestHitMap: Map<Pair<String,Boolean>, BestAlignmentGroup>, hapIdToRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double) : Map<Pair<String,Boolean>, BestAlignmentGroup> {
    return bestHitMap.entries
            .map{
                if(spansSingleRefRange(it,hapIdToRangeMap)) {
                    Pair(it.key,it.value)
                }
                else {
                    removeExtraRefRangeHits(it,hapIdToRangeMap, maxRefRangeError)
                }
            }
            .filter { it.second.listOfHapIds.size>0 } //Filter out any reads which do not have any mappings
            .toMap()
}

/**
 * Function to check to see if the reads only hit one reference range
 */
fun spansSingleRefRange(currentMapping: Map.Entry<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRangeMap : Map<Int, ReferenceRange>): Boolean {
    val hapIds = currentMapping.value.listOfHapIds

    val numberOfRefRangeGroups = hapIds.map { hapIdToRangeMap[it]?.id() }
            .groupBy { it }
            .count()
    return (numberOfRefRangeGroups==1)
}

/**
 * Function to remove any reads which hit more than one reference range ambiguously.
 */
fun removeExtraRefRangeHits(currentMapping: Map.Entry<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRangeMap : Map<Int, ReferenceRange>, maxRefRangeError: Double) : Pair<Pair<String,Boolean>,BestAlignmentGroup> {
    val bestMappings = currentMapping.value
    val hapIds = bestMappings.listOfHapIds

    val refRangeToHapIdMultimap = HashMultimap.create<Int,Int>()
    hapIds.forEach {
        check(hapIdToRangeMap.containsKey(it)) {"HapID ${it} is not found in the graph.  HapID ${it} is was aligned against which means that it is in the Pangenome.fa, but not in the graph passed to FastqToReadMappingPlugin or SAMToReadMappingPlugin.  Please check and make sure that the methods used when creating the graph match the methods used when creating the pangenome."}
        refRangeToHapIdMultimap.put(hapIdToRangeMap[it]?.id(),it)
    }

    val highestCountRefRangeId = findBestRefRange(refRangeToHapIdMultimap,maxRefRangeError)

    if(highestCountRefRangeId == -1) {
        return Pair(currentMapping.key,BestAlignmentGroup(bestMappings.readName,bestMappings.strand,bestMappings.bestEditDistance,bestMappings.lengthOfMapping, mutableSetOf(), mutableMapOf()))
    }

    //Now that we know what the highest count was, we can create a new Best Alignment Group
    return Pair(currentMapping.key,BestAlignmentGroup(bestMappings.readName,bestMappings.strand,bestMappings.bestEditDistance,bestMappings.lengthOfMapping,refRangeToHapIdMultimap[highestCountRefRangeId],bestMappings.hapIdToSecondaryStats))
}

/**
 * Function to check to see if the read hits a multiple reference ranges less than maxRefRangeError
 * Basically this is to filter out any reads which hit multiple reference ranges equally well
 */
fun findBestRefRange(refRangeToIdMapping : Multimap<Int, Int>, maxRefRangeError:Double) : Int {
    var bestRefRange = -1
    var totalNumberOfHits = 0
    var bestCount = 0

    refRangeToIdMapping.keySet().forEach {
        if(refRangeToIdMapping[it].size > bestCount) {
            bestCount = refRangeToIdMapping[it].size
            bestRefRange = it
        }
        totalNumberOfHits+= refRangeToIdMapping[it].size
    }

    if(maxRefRangeError <= 1 - (bestCount.toDouble()/totalNumberOfHits)) {
        return -1
    }

    return bestRefRange
}