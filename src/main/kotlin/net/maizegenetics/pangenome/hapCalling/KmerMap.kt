package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.LongArraySet
import it.unimi.dsi.fastutil.longs.LongOpenHashSet
import it.unimi.dsi.fastutil.longs.LongSet
import net.maizegenetics.dna.BaseEncoder
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants
import java.util.*


enum class KmerMapType { COUNT, REFRANGEID }
/**
 * KmerMap provides high efficiency in both speed and memory efficiency for storing and counting kmers.
 * It is backed with a primitive hashmap.
 * All kmers are encoded into Longs, so only kmers upto to 32bp in length can be stored.
 */
open class KmerMap(sequence: String = "",
                   val kmerSize: Int,
                   val stepSize: Int = 1,
                   val kmerPrefix: Char? = null,
                   val refRangeId: Int = -1,
                   val mapType: KmerMapType = KmerMapType.COUNT
) {
    internal val kmerToCount = Long2IntOpenHashMap()
    internal val kmerDuplicates = LongOpenHashSet()

    init {
        if (kmerSize !in 1..32) throw IllegalArgumentException("Kmers must be 1 to 32 nucleotides in length")
        addDNASeqToMap(sequence)
    }

    protected fun addDNASeqToMap(dna: String) {
        val dnaAsByte = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(dna)
        val kmerPrefixByte = if (kmerPrefix != null) NucleotideAlignmentConstants.getNucleotideAlleleByte(kmerPrefix) else -1
        for (i in 0..dnaAsByte.size - kmerSize step stepSize) {
            if ((kmerPrefix != null) && dnaAsByte[i] != kmerPrefixByte) continue
            val seqAsLong = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(dnaAsByte, i, i + kmerSize))
            when (mapType) {
                KmerMapType.COUNT -> kmerToCount.compute(seqAsLong) { kmer, count -> if (count == null) 1 else count + 1 }
                KmerMapType.REFRANGEID -> kmerToCount.compute(seqAsLong) { kmer, refID -> if (refID == null) refRangeId else -1 }
            }
        }
    }

    private fun kmerToDNA(kmerAsLong: Long) = BaseEncoder.getSequenceFromLong(kmerAsLong, kmerSize.toByte())

    fun kmersAsLongSet(): LongSet {
        if (kmerDuplicates.isEmpty()) return kmerToCount.keys
        val result = LongArraySet()
        result.addAll(kmerToCount.keys)
        result.addAll(kmerDuplicates)
        return result
    }

    fun kmersAsLongArray(): LongArray {
        if (kmerDuplicates.isEmpty()) kmerToCount.keys.toLongArray()
        return kmerToCount.keys.toLongArray() + kmerDuplicates.toLongArray()
    }

    fun kmersAsLongList(): List<Long> = kmerToCount.keys.toList()
    fun kmersAsDNA(): Set<String> = kmerToCount.keys.map { kmerToDNA(it) }.toSet()
    fun kmerLongSequence(): Sequence<Long> = kmerToCount.keys.asSequence()
    fun kmerDNASequence(): Sequence<String> = kmerToCount.keys.map { kmerToDNA(it) }.asSequence()
    fun size(): Int = kmerToCount.size + kmerDuplicates.size

    operator fun get(kmer: Long): Int = if (kmerDuplicates.contains(kmer)) -1 else kmerToCount[kmer]
    operator fun get(kmerSeq: String): Int = get(BaseEncoder.getLongFromSeq(kmerSeq))
    fun asSequence(): Sequence<Pair<Long, Int>> = kmerToCount.asSequence().map { (kmer, count) -> kmer to count }
    fun asDNASequence(): Sequence<Pair<String, Int>> = kmerToCount.asSequence()
            .map { (kmer, count) -> kmerToDNA(kmer) to count }

    override fun toString() = asDNASequence()
            .map { (kmer, count) -> "$kmer=$count" }
            .joinToString(prefix = "{", postfix = "}")

    /**
     *
     */
    fun toStringLong() = asSequence()
            .map { (kmer, count) -> "$kmer=$count" }
            .joinToString(prefix = "{", postfix = "}")
}

/**
 * Mutable version with ability to add individual kmers or whole sets of others KmerCounters
 */
class MutableKmerCounter(sequence: String = "",
                         kmerSize: Int,
                         stepSize: Int = 1,
                         kmerPrefix: Char? = null
) : KmerMap(sequence, kmerSize, stepSize, kmerPrefix) {

    /**
     * Add kmer by Long representation as defined by BaseEncoder (fast)
     */
    fun add(kmer: Long) = add(kmer, 1)

    /**
     * Add kmer by byte array
     */
    fun add(kmerAsByte: ByteArray) = add(byteArrayToKmerLong(kmerAsByte, kmerSize), 1)

    /**
     * Add kmers by String representation of DNA - slowest way if lots of overlapping kmers
     */
    fun add(kmer: String) = add(dnaToKmerLong(kmer, kmerSize))

    fun add(kmer: String, occurrences: Int) = add(dnaToKmerLong(kmer, kmerSize), occurrences)
    fun add(kmer: Long, occurrences: Int) = kmerToCount.compute(kmer) { _, count -> if (count == null) occurrences else count + occurrences }

    fun addAll(newKmerCounts: KmerMap) = newKmerCounts.asSequence().forEach { (kmer, count) -> this.add(kmer, count) }
    operator fun plusAssign(newKmerCounts: KmerMap) = addAll(newKmerCounts)

    /**
     * Add kmer sequence using the previously defined kmerSize, stepSize, and kmerPrefix (very fast)
     */
    fun addAll(sequence: String) = addDNASeqToMap(sequence)
}

/**
 * Mutable version of the KmerMap which allows for unique kmer->id mappings.
 * If a kmer is seen in multiple ids( or in the same id), its id in the map is set to -1
 * This datastructure can then be used to further purge out repetative kmers
 */
class MutableKmerToIdMap(kmerSize: Int,
                         kmerPrefix: Char? = null
) : KmerMap(kmerSize = kmerSize, kmerPrefix = kmerPrefix, mapType = KmerMapType.REFRANGEID) {

    /**
     * Basic put function for String kmer and its associated id
     */
    fun put(kmer: String, id: Int) = put(dnaToKmerLong(kmer, kmerSize), id)

    /**
     * Put function taking in a ByteArray representation of the kmer and its associated id
     */
    fun put(kmerAsByte: ByteArray, id: Int) = put(byteArrayToKmerLong(kmerAsByte, kmerSize), id)

    /**
     * Put function from a Long Representation of the kmer and its Id
     * We apply the lambda here which will set the id to -1 if we have already seen this Id
     */
    fun put(kmer: Long, id: Int): Int {
        if (kmerDuplicates.contains(kmer)) return -1
        val result = kmerToCount.compute(kmer) { _, storedId -> if (storedId == null) id else -1 }
        if (result == -1) {
            kmerToCount.remove(kmer)
            kmerDuplicates.add(kmer)
        }
        return result!!
    }

    /**
     * Loop through the KmerMap and put each key-value pair into the existing map.
     */
    fun putAll(newKmerMap: KmerMap) = newKmerMap.asSequence().forEach { (kmer, storedId) -> this.put(kmer, storedId) }

    /**
     * Method to add in all the kmers stored in a primitive Long Set into the kmerToIdMap using the input id.
     */
    fun putAll(kmerSet: LongSet, id: Int) = kmerSet.forEach { put(it, id) }
}