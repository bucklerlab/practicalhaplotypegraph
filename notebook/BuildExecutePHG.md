# Building and Executing PHG Project
##### *Author: Terry Casstevens*

*NOTE: These instructions assume repositories are in the same directory*

### Prepare Execution Directory
* git clone https://bitbucket.org/tasseladmin/tassel-5-standalone.git

### Build TASSEL 5 Source
*NOTE: This not needed if using latest TASSEL 5 Release. This is if you committed recent changes or have uncommitted changes in your development environment that you need.*

* git clone https://bitbucket.org/tasseladmin/tassel-5-source.git

* cd ./tassel-5-source

* mvn package -Dmaven.test.skip=true

* cp dist/sTASSEL.jar ../tassel-5-standalone/sTASSEL.jar

### Build PHG Source
*NOTE: This can be your development local repository, and on a branch other than master*

* git clone https://\<userid\>@bitbucket.org/bucklerlab/practicalhaplotypegraph.git

* cd ./practicalhaplotypegraph

* git checkout \<branch\> *(Only if you want a branch other than master)*

* mvn package -Dmaven.test.skip=true

* cp dist/phg.jar ../tassel-5-standalone/lib/phg.jar

* cp lib/jbwa.jar ../tassel-5-standalone/lib/jbwa.jar *(NOTE: Maybe needed depending on code you are running)*

### Setting up jbwa
*NOTE: This is needed if you are running code (i.e. FastqToHapCountPlugin) that uses jbwa*

* git clone https://github.com/lindenb/jbwa.git

* cd ./jbwa

* make *(builds native library - needs wget installed)*

* -Djava.library.path=/\<directory\>/jbwa/src/main/native *(make part of java command in run\_pipeline.pl and run\_anything.pl)*

* gradle -b publish_jbwa.gradle jar *(NOTE copying jbwa.jar in above step makes this unnecessary. builds executable jar file in jbwa/build/libs)*

### Execute PHG
*NOTE: If you are using libraries (i.e. jbwa.jar) not already in TASSEL 5, put them in ./tassel-5-standalone/lib*

* cd ./tassel-5-standalone

* ./run_pipeline.pl -Xmx5g -debug -importGuess filename.vcf -PHGPlugin -param1 value1 -endPlugin -AnotherPHGPlugin -endPlugin -export output.vcf -exportType VCF

* ./run_anything.pl -Xmx5g net.maizegenetics.pangenome.api.SomePHGClass *(For testing only)*

    * *NOTE: Put this lines of code at beginning of main()*
    
    * LoggingUtils.setupDebugLogging();
    
    * TasselLogging.basicLoggingInfo();