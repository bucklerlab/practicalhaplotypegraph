/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

import net.maizegenetics.util.Utils;

/**
 * Takes a list of fasta file, creates a chell script to load them.
 * 
 * @author lcj34
 *
 */
public class ShellScript_createLoadHaplotypes {

    public static void processMain(String assemblyNameFile, String outputFile, String gvcf, String variants ) {
        BufferedReader br = Utils.getBufferedReader(assemblyNameFile);
        List<String> assemblyList = new ArrayList<String>();
        
        BufferedWriter bw = Utils.getBufferedWriter(outputFile);
        try {
            String line;
            while ((line=br.readLine()) != null) {
                assemblyList.add(line);
            }
            br.close();
//            String initialCommand = "time java -jar -Xms200g -Xmx225g jars/LoadHapSequencesToPHGdb.jar dbs/hackathon_trimmedAnchors.db trimmed_haplotypes_fromZack/";
//            // to the string above, we need the file name, which is <assembly>_trimmed_FromZack.fa
//            for (String assembly : assemblyList) {
//                StringBuilder sb = new StringBuilder(initialCommand);
//                System.out.println("processing assembly: " + assembly) ;
//                String justName = assembly.substring(0,assembly.indexOf("haplotype_Caller")-1);
//                sb.append(assembly + "_trimmed_FromZack.fa ")
//                  .append("PHGUploadFiles/" + justName + "_load_data.txt none load_trimmed_haplotypesFromZack/ ")                  
//                  .append(" > load_trimmed_haplotypesFromZack/load_output_" + assembly + ".txt\n");
//                bw.write(sb.toString());
//            }
            // THese are for the AUg 8, 2017 gvcf filtered files on cbsumm11 from Zack,
            // leading up to next hackathon (aug 28)
            //String initialCommand = "time java -jar -Xms100g -Xmx125g jars/LoadHapSequencesToPHGdb.jar dbs/august2017_forGrantNO_UNIQUE.db /workdir/zrm22/FilteredGVCFAndFastas99UpperBound/AllFastas/";
           //String initialCommand = "time java -jar -Xms100g -Xmx125g jars/LoadHapSequencesToPHGdb.jar dbs/august2017_forGrantNO_UNIQUE.db zack_haplotypes/"; // on cbsumm15 - testing no UNIQUE constraint
            String initialCommand = "time java -jar -Xms100g -Xmx125g jars/LoadHapSequencesToDBPlugin.jar dbs/testNewSchema_21august2017_PLUGIN_AnchorPHG.db /workdir/lcj34/phg_pipeline/robertNovagene_outputDir/Fastas/"; // on cbsulm07 - load ZeaxujAndNam
            
            // to the string above, we need the file name, which is <assembly>_trimmed_FromZack.fa
            for (String assembly : assemblyList) {
                StringBuilder sb = new StringBuilder(initialCommand);
                System.out.println("processing assembly: " + assembly) ;
                String justName = assembly.substring(0,assembly.indexOf("haplotype_caller")-1);
                // below - if on cbsumm11 - want /workdir/zrm22/DBUploadFiles/
                // below - the ZeaxujAndNam are on cbsulm07
                sb.append(assembly + " ")
                  //.append("zack_DBUploadFiles/" + justName + "_load_data.txt none load_zackHaps_toNon_uniqueDB/ ")
                .append("/workdir/lcj34/phg_pipeline/DBUploadFiles/" + justName + "_load_data.txt " + gvcf + " " + variants + " load_terryHaps_toNewSchema/ ") 
                  .append(" > load_terryHaps_toNewSchema/load_output_" + justName + ".txt\n");
                bw.write(sb.toString());
            }
            bw.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
//        String assemblyFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/interAnchors_June/perAnchor_toPerHaplotype_files/assemblyList_noAssemblies.txt";
//        String output = "/Users/lcj34/notes_files/repgen/wgs_pipeline/interAnchors_June/perAnchor_toPerHaplotype_files/TrimmedAssembly_loadScript.sh";
        
        //String assemblyFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/gvcf_filtered_data/Zack_filteredGVCF_files_aug2017.txt";
       ///String assemblyFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/gvcf_filtered_data/ZeaxujAndNam_fastas.txt";
        String assemblyFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/DBUploadFiles/cbsumm11_filesForDBLoading_FULLNAME.txt";
        //String output = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/gvcf_filtered_data/gvcfFilterd_loadScript.sh";
        // THe non-unique db is a db with the UNIQUE constraint removed from anchor_sequences
        // anc the PRIMARY_KEY removed from anchorseq_consensus_anchorseq db. This was for testing
        // loading of consensus which was taking FOREVER with constraints.
        //String output = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/gvcf_filtered_data/gvcfFilterd_loadScript_forNonUNIQUEdb.sh";
       // String output = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/gvcf_filtered_data/gvcfFilterd_loadScript_ZeaxujAndNam.sh";
        String output = "/Users/lcj34/notes_files/repgen/wgs_pipeline/DBUploadFiles/newSchema_loadScript_Terrymm11.sh";
        
        // these are the fake file names on cbsumm15, on cbsumm11 they were inconsistantly called gvcfFile.txt
        String gvcf = "gvcf_file.txt"; // these are fake files - they must exist in dir where run this script
        String variants = "variant_file.txt";
        processMain(assemblyFile,output,gvcf,variants);
        

    }

}
