package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.HashMultiset
import com.google.common.collect.Multimap
import com.google.common.collect.Multiset
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.SamReader
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.api.referenceRanges
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon
import net.maizegenetics.util.Utils



/**
 * This class will be the 3rd attempt for a genotyping pipeline.
 *
 * For now read in a SAM file  Generate from minimap2 and will output a HapCount File.
 * Inclusions are counted if both pairs occur within the same haplotype.
 * Exclusions are counted for the rest of the reads which are not hit by this pair.
 *
 * Future TODOs:
 *      - Run Minimap using ProcessBuilder and directly output a HapCount File
 *      - Figure out DB updating for HapCounts
 *
 *
 */
@Deprecated("Use FastqToMappingPlugin instead.")
class FastqToHapCountMinimapPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(FastqToHapCountMinimapPlugin::class.java)

    private var samFileName = PluginParameter.Builder("samFile", null, String::class.java)
            .guiName("SAM file to process")
            .inFile()
            .required(true)
            .description("Name of the SAM file to process")
            .build()

    private var readOutputFile = PluginParameter.Builder("readMappingOutput", null, String::class.java)
            .guiName("Output Read-> haplotype Mapping File")
            .outFile()
            .required(true)
            .description("Name of Read->haplotype Mapping file.")
            .build()

    private var hapCountFile = PluginParameter.Builder("hapCountFile", null, String::class.java)
            .guiName("Output Haplotype Count File")
            .outFile()
            .required(true)
            .description("Name of the haplotype count file.")
            .build()

    private var pairedMode = PluginParameter.Builder("paired", true, Boolean::class.java)
            .required(false)
            .description("Run in paired mode.  This will remove any haplotype counts if they are not agreed by the pair.")
            .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRageErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    override fun processData(input: DataSet?): DataSet? {
        myLogger.error("Deprecated: Use FastqToMappingPlugin instead.")
        return null
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToHapCountMinimapPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "FastqToHapCountMinimapPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Align a Fastq file and export a hapCount File "
    }

    /**
     * Name of the SAM file to process
     *
     * @return SAM file to process
     */
    fun samFileName(): String {
        return samFileName.value()
    }

    /**
     * Set SAM file to process. Name of the SAM file to process
     *
     * @param value SAM file to process
     *
     * @return this plugin
     */
    fun samFileName(value: String): FastqToHapCountMinimapPlugin {
        samFileName = PluginParameter(samFileName, value)
        return this
    }

    /**
     * Name of Read->haplotype Mapping file.
     *
     * @return Output Read-> haplotype Mapping File
     */
    fun readOutputFile(): String {
        return readOutputFile.value()
    }

    /**
     * Set Output Read-> haplotype Mapping File. Name of Read->haplotype
     * Mapping file.
     *
     * @param value Output Read-> haplotype Mapping File
     *
     * @return this plugin
     */
    fun readOutputFile(value: String): FastqToHapCountMinimapPlugin {
        readOutputFile = PluginParameter(readOutputFile, value)
        return this
    }

    /**
     * Name of the haplotype count file.
     *
     * @return Output Haplotype Count File
     */
    fun hapCountFile(): String {
        return hapCountFile.value()
    }

    /**
     * Set Output Haplotype Count File. Name of the haplotype
     * count file.
     *
     * @param value Output Haplotype Count File
     *
     * @return this plugin
     */
    fun hapCountFile(value: String): FastqToHapCountMinimapPlugin {
        hapCountFile = PluginParameter(hapCountFile, value)
        return this
    }

    /**
     * Run in paired mode.  This will remove any haplotype
     * counts if they are not agreed by the pair.
     *
     * @return Paired
     */
    fun pairedMode(): Boolean {
        return pairedMode.value()
    }

    /**
     * Set Paired. Run in paired mode.  This will remove any
     * haplotype counts if they are not agreed by the pair.
     *
     * @param value Paired
     *
     * @return this plugin
     */
    fun pairedMode(value: Boolean): FastqToHapCountMinimapPlugin {
        pairedMode = PluginParameter(pairedMode, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Rage Err
     */
    fun maxRefRangeError(): Double? {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Rage Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Rage Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double?): FastqToHapCountMinimapPlugin {
        maxRefRangeError = PluginParameter(maxRefRangeError, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generate(FastqToHapCountMinimapPlugin::class.java)
}