#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/Tx303_HBEN2ADXX_AGTTCC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Tx303_HFFGKADXX_AGTTCC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Tx303_HL5WNCCXX_L6.clean_srt_dedup.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o Tx303_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant Tx303_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o Tx303_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V Tx303_haplotype_caller_output.vcf \
   -o Tx303_haplotype_caller_output.fasta

