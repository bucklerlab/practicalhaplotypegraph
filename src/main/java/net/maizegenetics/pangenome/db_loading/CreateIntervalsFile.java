/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import net.maizegenetics.util.Utils;

/**
 * This method must be re-worked - getHaplotypeAnchorCoordinates no longer
 * accesses the correct tables.
 * 
 * THis method pulls ref chrom, start and end positions for ref anchors.
 * It creates a file that is in the form needed for haplotype caller,
 * ie 8:12921-13181
 * @author lcj34
 *
 */
public class CreateIntervalsFile {
    public static void processMain(String db, String refLine, String outputFile) {
        
//        try {
//            PHGDataWriter phg = new PHGSQLite(db);
//            Map<Integer, String> anchorDataMap = phg.getHaplotypeAnchorCoordinates( refLine, 0,1);
//            System.out.println("Number of anchors from db: " + anchorDataMap.keySet().size());
//            List<Integer> anchoridList = new ArrayList<Integer>(anchorDataMap.keySet());
//
//            Collections.sort(anchoridList);
//            BufferedWriter bw = Utils.getBufferedWriter(outputFile);
//            for (int idx = 0; idx <anchoridList.size(); idx++) {
//                int anchorID = anchoridList.get(idx);
//                String[] anchorData = anchorDataMap.get(anchorID).split(":");
//                
//                String intervalsData = anchorData[0] + ":" + anchorData[1] + "-" + anchorData[2] + "\n";
//                bw.write(intervalsData);
//            }
//            bw.close();
//                       
//        } catch (Exception exc) {
//            exc.printStackTrace();
//        }
        
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.out.println("Begin ..");
        // Hackathon maize db
//        String db = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/hackathon_trimmedAnchors_justAnchors_noInterANchors.db";
//        String intervalsFile = "/Users/lcj34/notes_files/hackathons/hackathon_june2017/hackathon_trimmed.intervals";
        
        // Hackathon sorghum db
        String refLine = "SorghumRef";
        String db = "/Users/lcj34/notes_files/hackathons/hackathon_june2017/dbs/sorghum_justAnchor_June23.db";
        String intervalsFile = "/Users/lcj34/notes_files/hackathons/hackathon_june2017/hackathon_sorghum_June23.intervals";
        processMain(db,refLine, intervalsFile);
        System.out.println("FInished - file written to " + intervalsFile);
    }

}
