package net.maizegenetics.pangenome.db_loading

import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.HaplotypeSequence
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.hapcollapse.GVCFUtils
import net.maizegenetics.taxa.TaxaListBuilder
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail
import org.xerial.snappy.Snappy
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*
import java.util.stream.Collectors
import kotlin.math.pow


@ExtendWith(SmallSeqExtension::class)
class DBLoadingUtilsIT {

    private val vcfFileName = "data/simpleGVCFTestFile.g.vcf"

    // TODO: Add to SmallSeqExtension ability to generate SmallSeq SQLite database
    @Test
    fun testSerializingVCFFile() {
        val vcfReader = VCFFileReader(File(vcfFileName), false)
        val listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList())

        val vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName, false, false)
        val vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded)

        for (i in 0 until vcfDecoded.size) {
            val encodedVC = vcfDecoded[i]
            val originalVC = listOfVariants[i]
            testVariantContextsEqual(encodedVC,originalVC)
        }
    }

    @Test
    fun testSerializingVCFFileOnlyVariants() {
        val vcfReader = VCFFileReader(File(vcfFileName), false)
        val listOfVariants = vcfReader.iterator().stream().filter { it.isVariant }.collect(Collectors.toList())

        val vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName, true, false)
        val vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded)

        for (i in 0 until vcfDecoded.size) {
            val encodedVC = vcfDecoded[i]
            val originalVC = listOfVariants[i]
            testVariantContextsEqual(encodedVC, originalVC)
        }
    }

    @Test
    fun testSerializingVCFFileMergedRefs() {
        val vcfReader = VCFFileReader(File(vcfFileName), false)
        val listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList())
        val listOfVariantsGVCF = GVCFUtils.convertVCFToGVCF(listOfVariants)

        val vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName,false,true)
        val vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded)

        for (i in 0 until vcfDecoded.size) {
            val encodedVC = vcfDecoded[i]
            val originalVC = listOfVariantsGVCF[i]
            testVariantContextsEqual(encodedVC,originalVC)
        }
    }

    private fun testVariantContextsEqual(encodedVC: VariantContext, originalVC: VariantContext) {
        assertEquals(originalVC.contig, encodedVC.contig, "Serializing VCF file fail, chromosomes does not match")
        assertEquals(originalVC.start, encodedVC.start, "Serializing VCF file fail, startPosition does not match")
        assertEquals(originalVC.end, encodedVC.end, "Serializing VCF file fail, endPosition does not match")

        val encodedGT = encodedVC.getGenotype(0);
        val originalGT = originalVC.getGenotype(0);
        assertEquals(originalGT.genotypeString, encodedGT.genotypeString, "Serializing VCF file fail, GT call does not match")
        assertEquals(originalGT.dp, encodedGT.dp, "Serializing VCF file fail, DP call does not match")
        assertEquals(originalGT.gq, encodedGT.gq, "Serializing VCF file fail, GQ call does not match")
        assertArrayEquals(originalGT.ad, encodedGT.ad, "Serializing VCF file fail, AD call does not match")
        assertArrayEquals(originalGT.pl, encodedGT.pl, "Serializing VCF file fail, PL call does not match")
    }

    @Test
    fun testMergingRefRanges() {
        val vcfReader = VCFFileReader(File(vcfFileName), false)
        val listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList())
        val variantsMerged = GVCFUtils.convertVCFToGVCF(listOfVariants)

        val ranges = listOf(1 ..37, 38..38, 39 .. 40, 41 .. 41, 42 .. 45,
            46 .. 50, 51 .. 55, 60 .. 65, 66 .. 69, 69 .. 69, 71 .. 131,
            200 .. 210, 210 .. 210, 213 .. 216, 219 ..  220, 220 .. 220,
            230 .. 232, 233 .. 234, 234 .. 250 )

        assertEquals(ranges.size, variantsMerged.size, "Merged variants size does not match")

        for( idx in ranges.indices) {
            val range = ranges[idx]
            val vc = variantsMerged[idx]
            assertEquals(range.first, vc.start, "Start position does not match")
            assertEquals(range.last, vc.end, "End position does not match")
        }
    }

    @Test
    fun testVerifyIntervalRanges( ) {
        // NOTE: Ensure multiline strings are separated with an actual tab character!
        val anchorFile = "${SmallSeqExtension.testDataDir}/testAnchorFile.txt"
        File(anchorFile).bufferedWriter().use {
            // Lines 4 and 5 overlap line 3
            // Line 8 overlaps line 7 (First chr2 anchor)
            // Line 13 overlaps line 12 (but different chrom, so should not count)
            val anchorContents = """
                chr1	10575	13198
                chr1	20460	29234
                chr1	141765	145627
                chr1	143661	146302
                chr1	144363	148863
                chr1	175219	177603
                chr2	81176	84375
                chr2	82776	87024
                chr2	108577	113286
                chr3	116671	122941
                chr3	140393	145805
                chr3	159053	164568
                chr5	158053	164568
            """.trimIndent()
            it.write(anchorContents)
        }

        val overlaps1 = DBLoadingUtils.verifyIntervalRanges(anchorFile)
        assertEquals(3, overlaps1.size)

        val containsEntry1 = overlaps1.contains("chr1\t143661\t146302");
        assertEquals(containsEntry1,true);

        val containsEntry2 = overlaps1.contains("chr5\t158053\t164568");
        assertEquals(false, containsEntry2)

        // Do again, but have the intervals out of order in the anchor file.
        File(anchorFile).bufferedWriter().use {
            // Line 3 overlaps line 1
            // Line 5 overlaps lines 3 and 4
            // Line 8 overlaps line 7
            // Line 13 overlaps line 12 (but different chrom, so should not count)
            val anchorContents = """
                chr1	141765	145627
                chr1	20460	29234
                chr1	143661	146302
                chr1	10575	13198
                chr1	144363	148863
                chr1	175219	177603
                chr2	82776	87024
                chr2	81176	84375
                chr2	108577	113286
                chr3	116671	122941
                chr3	140393	145805
                chr3	159053	164568
                chr5	158053	164568
            """.trimIndent()
            it.write(anchorContents)
        }

        val overlaps2 = DBLoadingUtils.verifyIntervalRanges(anchorFile)
        assertEquals(3, overlaps2.size)

        val containsEntry3 = overlaps2.contains("chr1\t143661\t146302");
        assertEquals(containsEntry3,true);

        val containsEntry4 = overlaps2.contains("chr5\t158053\t164568");
        assertEquals(containsEntry4,false);

        // Write anchor file with NO overlaps
        // Do again, but have the intervals out of order in the anchor file.
        File(anchorFile).bufferedWriter().use {
            val anchorContents = """
                chr1	10575	13198
                chr1	20460	29234
                chr1	141765	145627
                chr1	146661	147302
                chr1	148363	150863
                chr1	175219	177603
                chr2	81176	81375
                chr2	82776	87024
                chr2	108577	113286
                chr3	116671	122941
                chr3	140393	145805
                chr3	159053	164568
                chr5	158053	164568
            """.trimIndent()
            it.write(anchorContents)
        }

        val overlaps3 = DBLoadingUtils.verifyIntervalRanges(anchorFile);
        assertEquals(0, overlaps3.size)
    }

    @Test
    fun testCreateInitialAlleles() {
        val bases = 5.0 // A, C, G, T, N
        val maxKmerLen2 = 2
        val allelesKmerLen2 = DBLoadingUtils.createInitialAlleles(maxKmerLen2)
        var numAllelesKmerLen2 = 0.0
        for (idx in 1..maxKmerLen2) {
            numAllelesKmerLen2 += bases.pow(idx)
        }
        assertEquals(numAllelesKmerLen2.toInt(), allelesKmerLen2.size)

        val maxKmerLen3 = 3
        val allelesKmerLen3 = DBLoadingUtils.createInitialAlleles(maxKmerLen3)
        var numAllelesKmerLen3 = 0.0
        for (idx in 1..maxKmerLen3) {
            numAllelesKmerLen3 += bases.pow(idx)
        }
        assertEquals(numAllelesKmerLen3.toInt(), allelesKmerLen3.size)

        val maxKmerLen4 = 4
        val allelesKmerLen4 = DBLoadingUtils.createInitialAlleles(maxKmerLen4)
        var numAllelesKmerLen4 = 0.0
        for (idx in 1..maxKmerLen4) {
            numAllelesKmerLen4 += bases.pow(idx)
        }
        assertEquals(numAllelesKmerLen4.toInt(), allelesKmerLen4.size)
    }

    @Test
    fun testEncodeandDecodePathArrayForMultipleLists() {
        val refRange1 = ReferenceRange("ref1", Chromosome.instance("1"),1,20, 1)
        val refRange2 = ReferenceRange("ref2", Chromosome.instance("2"),1,20, 2)
        val refRange3 = ReferenceRange("ref3", Chromosome.instance("3"),1,20, 3)
        val refRange4 = ReferenceRange("ref4", Chromosome.instance("1"),1,20, 4)
        val refRange5 = ReferenceRange("ref5", Chromosome.instance("1"),1,20, 5)

        val seq1 = "ACGTTGGCCAATAGC"
        val seq2 = "TTCCGGAATAGCAT"
        val seq3 = "GGGGAAAATTTTCCC"
        val seq4 = "AAAAGGGGTTTTCCC"
        val seq5 = "ACACACTGTGTGTAGC"

        val hapSeq1 = HaplotypeSequence.getInstance(seq1,refRange1, 34.5, "RTYU")
        val hapSeq2 = HaplotypeSequence.getInstance(seq2,refRange2, 34.5, "TYRU")
        val hapSeq3 = HaplotypeSequence.getInstance(seq3,refRange3, 34.5, "DFGH")
        val hapSeq4 = HaplotypeSequence.getInstance(seq4,refRange4, 34.5, "GHJL")
        val hapSeq5 = HaplotypeSequence.getInstance(seq5,refRange5, 34.5, "VVBM")

        val taxaList = TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0:Mo17_0")).build()

        val node1 = HaplotypeNode(hapSeq1, taxaList,6, "0", 0, 0, ".",-1, -1)
        val node2 = HaplotypeNode(hapSeq2, taxaList, 7, "0", 0, 0, ".",-1, -1)
        val node3 = HaplotypeNode(hapSeq3, taxaList, 8, "0", 0, 0, ",",-1, -1)
        val node4 = HaplotypeNode(hapSeq4, taxaList, 9, "0", 0, 0, ".", -1, -1)
        val node5 = HaplotypeNode(hapSeq5, taxaList, -1, "0", 0, 0, ".", -1, -1)

        val diploidData = arrayListOf<List<HaplotypeNode>>()

        val firstList =  arrayListOf<HaplotypeNode>(node1, node2, node4)
        val secondList = arrayListOf<HaplotypeNode>(node2, node3, node4)
        val thirdList =  arrayListOf<HaplotypeNode>(node2, node5, node3)

        diploidData.add(firstList)
        diploidData.add(secondList)

        val diploidDataAsBytes = DBLoadingUtils.encodePathArrayForMultipleLists(diploidData)

        val decodedLists = DBLoadingUtils.decodePathsForMultipleLists(diploidDataAsBytes)
        assertEquals(2, decodedLists.size);
        val list1 = decodedLists[0]
        val list2 = decodedLists[1]

        assertEquals(3, list1.size)
        assertEquals(3, list2.size)

        assertEquals(6, list1[0])
        assertEquals(7, list1[1])

        assertEquals(8, list2[1])

        // Test with uneven lists, including a -1 `hapid`
        diploidData.clear();
        diploidData.add(firstList);
        diploidData.add(thirdList);

        val diploidDataAsBytesUneven = DBLoadingUtils.encodePathArrayForMultipleLists(diploidData);
        val decodedListsUneven = DBLoadingUtils.decodePathsForMultipleLists(diploidDataAsBytesUneven);

        assertEquals(2, decodedListsUneven.size)
        val list1Uneven = decodedListsUneven[0]
        val list2Uneven = decodedListsUneven[1]

        assertEquals(3, list1Uneven.size)
        assertEquals(2, list2Uneven.size) // the -1 is dropped, so list2Uneven has only 1 entry

        assertEquals(8, list2Uneven[1]);
    }

    @Test
    fun testDecodeOldPathsWithNewMethod() {
        val bBuff = ByteBuffer.allocate((Integer.SIZE / 8) + Integer.SIZE / 8 * 20)
        bBuff.putInt(20); // store the size (number of `hapid`s) as first value
        for (idx in 1 until 21) {
            bBuff.putInt(idx)
        }

        try {
            val dataAsByteArray = Snappy.compress(Arrays.copyOf(bBuff.array(), bBuff.position()))

            // Data is encoded using old method, decode it using MultipleLists method
            val decodedLists = DBLoadingUtils.decodePathsForMultipleLists(dataAsByteArray);
            assertEquals(1, decodedLists.size)
            val list1 = decodedLists[0]

            assertEquals(20, list1.size)
            val list1Item1 = list1[0]
            assertEquals(1, list1Item1)
            val list1Item2 = list1[1]
            assertEquals(2, list1Item2)
            val list1Item15 = list1[14]
            assertEquals(15, list1Item15)
        } catch (e: IOException) {
            fail("Could not compress byte array")
        }
    }

    @Test
    fun testformatMethodParamsToJSON() {
        val pluginParams = HashMap<String,String>()
        pluginParams.put("userNotes","param values based on analysis X and reason Y for NAM")
        pluginParams.put("param1","0.01")
        pluginParams.put("param2","true")
        pluginParams.put("param3","parallel_asmKeyFile.txt")

        val jsonStringOutput = DBLoadingUtils.formatMethodParamsToJSON(pluginParams)
        assertTrue(jsonStringOutput.contains("userNotes"))

        // JsonString has comma as separator. With 4 items, there should be 3 commas
        assertEquals(3, jsonStringOutput.chars().filter { it == ','.code }.count())

        // JsonString separates key/value with a colon.  The list should have 4 parameters,
        // hence 4 colons.  (This test won't work if the key or values have colons in them)
        assertEquals(4, jsonStringOutput.chars().filter { it == ':'.code}.count())

        val paramsAsMap = DBLoadingUtils.parseMethodJsonParamsToString(jsonStringOutput)
        assertEquals(4, paramsAsMap.keys.size)

        //Check the map returned contains all the values we originally stored to the JSON string
        pluginParams.keys.stream().forEach {
            assertTrue(paramsAsMap.containsKey(it))
        }
    }

    @Test
    fun testNonJSONParse() {
        val nonJSON = "here is a user desc that is not json"
        try {
            val paramsMap = DBLoadingUtils.parseMethodJsonParamsToString(nonJSON)
            assertTrue(paramsMap.containsKey("notes"))
            assertEquals(1, paramsMap.keys.size)
        } catch (exc: Exception) {
            fail("testNonJSONParse failed - exception should have been caught in parseMethodJsonParamsToString")
            exc.printStackTrace()
        }
    }

    @Test
    fun testBGzipTabXonExistingFile()
    {
        // write a file, bgzip and tabix it, then redo the bgzip/tabix
        // to verify there is no problem using "force" on an existing file
        val testGvcf = "${SmallSeqExtension.testDataDir}/testGvcf.vcf"

        // Read the existing vcf test file, write to this test's test file

        val vcfLines = File(vcfFileName).readLines()
        File(testGvcf).bufferedWriter().use {
            // write vcfLines to outputfile
            vcfLines.forEach { line -> it.write(line + "\n") }
        }
        // Verify created vcf file exists
        assertTrue(File(testGvcf).exists())

        //call bgzip and tabix on the test file
        val bgzippedGVCFFileName = bgzipAndIndexGVCFfile(testGvcf)
        // verify the bgzipped file exists
        assertTrue(File(bgzippedGVCFFileName).exists())

        // bgzip will rename the file so that it is called <filename>.gz,
        // Must re-copy the unzipped file over.  Verify it will bgzip when there
        // is already a file named <filename>.gz
        File(testGvcf).bufferedWriter().use {
            // write vcfLines to outputfile
            vcfLines.forEach { line -> it.write(line + "\n") }
        }

        // re-call bgzip and tabix on the test file
        val bgzippedGVCFFileName2 = bgzipAndIndexGVCFfile(testGvcf)
        // verify the bgfzipped file names are the same
        assertEquals(bgzippedGVCFFileName, bgzippedGVCFFileName2)

        // verify no errors and the bgzipped file exists
        // Note: bgzipAndIndexGVCFfile ran both bgzip and tabix,
        // so duplicates of both were tested here.
        assertTrue(File(bgzippedGVCFFileName).exists())



    }
}
