#!/bin/bash

LINE_ARRAY=(Ki11 HP301 P39 P39 P39 M162W M162W M162W Tx303 Tx303 Tx303)
ID_ARRAY=(4882_6665_17304_HFFGKADXX_Ki11_CTTGTA HP301_HL5WNCCXX_L5 4882_6665_17310_HBEN2ADXX_P39Goodman-Buckler_GTGAAA 4882_6665_17310_HFFGKADXX_P39Goodman-Buckler_GTGAAA P39Goodman-Buckler_HL5WNCCXX_L6 4882_6665_17309_HBEN2ADXX_M162W_GTCCGC 4882_6665_17309_HFFGKADXX_M162W_GTCCGC M162W_HL5WNCCXX_L6 4882_6665_17306_HBEN2ADXX_Tx303_AGTTCC 4882_6665_17306_HFFGKADXX_Tx303_AGTTCC Tx303_HL5WNCCXX_L5)
FIRST_FILE_NAME=(4882_6665_17304_HFFGKADXX_Ki11_CTTGTA_R1.fastq.gz HP301_HL5WNCCXX_L5_1.clean.renamed.fq.gz 4882_6665_17310_HBEN2ADXX_P39Goodman-Buckler_GTGAAA_R1.fastq.gz 4882_6665_17310_HFFGKADXX_P39Goodman-Buckler_GTGAAA_R1.fastq.gz P39Goodman-Buckler_HL5WNCCXX_L6_1.clean.renamed.fq.gz 4882_6665_17309_HBEN2ADXX_M162W_GTCCGC_R1.fastq.gz 4882_6665_17309_HFFGKADXX_M162W_GTCCGC_R1.fastq.gz M162W_HL5WNCCXX_L6_1.clean.renamed.fq.gz 4882_6665_17306_HBEN2ADXX_Tx303_AGTTCC_R1.fastq.gz 4882_6665_17306_HFFGKADXX_Tx303_AGTTCC_R1.fastq.gz Tx303_HL5WNCCXX_L5_1.clean.renamed.fq.gz)
SECOND_FILE_NAME=(4882_6665_17304_HFFGKADXX_Ki11_CTTGTA_R2.fastq.gz HP301_HL5WNCCXX_L5_2.clean.renamed.fq.gz 4882_6665_17310_HBEN2ADXX_P39Goodman-Buckler_GTGAAA_R2.fastq.gz 4882_6665_17310_HFFGKADXX_P39Goodman-Buckler_GTGAAA_R2.fastq.gz P39Goodman-Buckler_HL5WNCCXX_L6_2.clean.renamed.fq.gz 4882_6665_17309_HBEN2ADXX_M162W_GTCCGC_R2.fastq.gz 4882_6665_17309_HFFGKADXX_M162W_GTCCGC_R2.fastq.gz M162W_HL5WNCCXX_L6_2.clean.renamed.fq.gz 4882_6665_17306_HBEN2ADXX_Tx303_AGTTCC_R2.fastq.gz 4882_6665_17306_HFFGKADXX_Tx303_AGTTCC_R2.fastq.gz Tx303_HL5WNCCXX_L5_2.clean.renamed.fq.gz)
# to get the arrayLength, ${#ArrayName[@]}

#Set up the senteion variables and the reference path
license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02

SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
SENTIEONBWA='/programs/sentieon-genomics-201611.02/bin/bwa'
NUMBER_THREADS=20
REFERENCE='/SSD/zrm22/BAMFileGeneration/agpv4Ref/Zea_mays.AGPv4.dna.toplevel.fa.gz'

for((i=0;i<${#LINE_ARRAY[@]};i++));
do
    SAMPLE1=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${FIRST_FILE_NAME[i]}
    SAMPLE2=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${SECOND_FILE_NAME[i]}
    SORTED_BAM_1=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted.bam
 
    time $SENTIEONBWA mem -M -R '@RG\tID:${ID_ARRAY[i]} \tSM:${LINE_ARRAY[i]} \tPL:ILLUMINA' -t $NUMBER_THREADS $REFERENCE $SAMPLE1 $SAMPLE2 | $SENTIEON util sort -o $SORTED_BAM_1 -t $NUMBER_THREADS --sam2bam -i -

    SCORE_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_score.txt
    DEDUP_METRIC_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedupMetric.txt
    DEDUP_BAM=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedup.bam

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo LocusCollector --fun score_info $SCORE_TXT

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo Dedup --rmdup --score_info $SCORE_TXT --metrics $DEDUP_METRIC_TXT $DEDUP_BAM
done


