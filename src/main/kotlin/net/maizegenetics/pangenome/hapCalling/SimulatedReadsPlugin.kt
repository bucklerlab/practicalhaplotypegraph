package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.plugindef.*
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.io.PrintWriter
import java.lang.IllegalArgumentException
import java.lang.StringBuilder
import javax.swing.ImageIcon

class SimulatedReadsPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private var coverage = PluginParameter.Builder("coverage", 1.0, Double::class.javaObjectType)
            .description("average coverage of reads for a reference range")
            .build()
    private var lineName = PluginParameter.Builder("lineName", null, String::class.java)
            .required(true)
            .description("the name of the line for which reads will be simulated. If not specified, all lines will be simulated for the haplotype method.")
            .build()
    private var haplotypeMethod = PluginParameter.Builder("haplotypeMethod", "mummer4", String::class.java)
            .description("the haplotype method used to simulate reads")
            .build()
    private var fastqDir = PluginParameter.Builder("fastqDir", null, String::class.java)
            .description("the directory to which the simulated read fastq will be written. " +
                    "The default will write to a temp directory, which will not be available after the plugin finishes.")
            .outDir()
            .build()
    private var pairedEnd = PluginParameter.Builder("pairedEnd", false, Boolean::class.javaObjectType)
            .description("Should paired end reads be generated?")
            .build()
    private var readLength = PluginParameter.Builder("readLength", 150, Int::class.javaObjectType)
            .description("length of reads to be generated")
            .build()
    private var insertLength = PluginParameter.Builder("insertLength", 50, Int::class.javaObjectType)
            .description("for paired end reads the number of base pairs between reads")
            .build()

    private val myLogger = LogManager.getLogger(SimulatedReadsPlugin::class.java)
    private val complimentMap = mapOf('A' to 'T', 'G' to 'C', 'T' to 'A', 'C' to 'G',
            'a' to 't', 't' to 'a', 'c' to 'g', 'g' to 'c')

    override fun processData(input: DataSet): DataSet? {
        val hapGraph = input.getDataOfType(HaplotypeGraph::class.java)[0].data as HaplotypeGraph

        if (pairedEnd.value()) {
            val fastqFilenameList = simulatePairedEndReadsForTaxon(lineName.value(), hapGraph)
            val fastqFilename = simulateSingleReadsForTaxon(lineName()!!, hapGraph)
        }
        return null
    }

    fun simulateSingleReadsForTaxon(taxonName : String, myGraph : HaplotypeGraph) : String {
        val filename = "${taxonName}_${haplotypeMethod()}_${coverage()}X_SR.fq.gz"

        val myWriter = Utils.getBufferedWriter("${fastqDir()}$filename")
        if (myGraph != null) myGraph.chromosomes().forEach { chr ->
            val tree = myGraph.tree(chr)
            tree.entries.forEach { entry ->
                val taxonNode = entry.value.find { it.taxaList().map{ it.name}.contains(taxonName) }
                val hapid = taxonNode?.id() ?: -1
                if (taxonNode != null && hapid > 0) {
                    val fastqString = createSingleReads(taxonNode.haplotypeSequence().sequence(), coverage.value(), readLength.value(), hapid)
                    if (fastqString.length > 0) myWriter.write(fastqString)
                }
            }
        }
        myLogger.info("Finished writing reads to $filename.")
        myWriter.close()
        return filename
    }

    fun simulatePairedEndReadsForTaxon(taxonName : String, myGraph : HaplotypeGraph) : Pair<String,String> {
        val filename1 = "${taxonName}_${haplotypeMethod()}_${coverage()}X_PE1.fq.gz"
        val filename2 = "${taxonName}_${haplotypeMethod()}_${coverage()}X_PE2.fq.gz"

        val myWriter1 = Utils.getBufferedWriter("${fastqDir()}${filename1}")
        val myWriter2 = Utils.getBufferedWriter("${fastqDir()}${filename2}")
        myGraph.chromosomes().forEach { chr ->
            val tree = myGraph.tree(chr)
            tree.entries.forEach { entry ->
                val taxonNode = entry.value.find { it.taxaList().map{ it.name}.contains(taxonName) }
                val hapid = taxonNode?.id() ?: -1
                if (taxonNode != null && hapid > 0) {
                    val fastqString = createPairedEndReads(taxonNode.haplotypeSequence().sequence(), coverage.value(),
                        readLength.value(), insertLength.value(), hapid)
                    if (fastqString.size == 2) {
                        myWriter1.write(fastqString[0])
                        myWriter2.write(fastqString[1])
                    }
                }
            }
        }

        myLogger.info("Finished writing reads to $filename1 and $filename2.")
        myWriter1.close()
        myWriter2.close()

        return Pair(filename1,filename2)
    }

    override fun getToolTipText(): String = "Simulate reads from haplotypes"

    override fun getIcon(): ImageIcon? {
        val imageURL = BestHaplotypePathPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String = "Simulate Reads"

    fun createSingleReads(sequence : String, depth : Double, readLength : Int, hapid :Int) : String {
        //no errors, from CreateTestGenomes.createFastqString
        if (sequence.length < readLength) return ""
        val readNumber = (depth * sequence.length / readLength).toInt()
        val maxStart = sequence.length - readLength;
        val qualityScores = "H".repeat(readLength)
        val fastq = StringBuilder()
        for (i in 0 until readNumber) {
            val startPos = (Math.random() * maxStart).toInt()
            fastq.append("@M01032:387:000000000-ANP68:1:1102:${hapid}:${startPos} 1:N:0:CCTAAGAC+GCGTAAGA").append("\n")
            val read = sequence.substring(startPos, startPos + readLength)
            fastq.append(read).append("\n")
            fastq.append("+\n")
            fastq.append(qualityScores).append("\n")
        }
        return fastq.toString()
    }

    fun createPairedEndReads(sequence : String, depth : Double, readLength : Int, insertSize : Int, hapid :Int) : List<String> {
        //no errors, from CreateTestGenomes.createFastqString
        val templateLength = 2 * readLength + insertSize
        if (sequence.length < templateLength) return ArrayList<String>();
        val readNumber = (depth * sequence.length / templateLength).toInt()
        val maxStart = sequence.length - templateLength;
        val qualityScores = "H".repeat(readLength)
        val fastq1 = StringBuilder()
        val fastq2 = StringBuilder()
        for (i in 0 until readNumber) {
            val startPos = (Math.random() * maxStart).toInt()
            fastq1.append("@M01032:387:000000000-ANP68:1:1102:${hapid}:${startPos} 1:N:0:CCTAAGAC+GCGTAAGA").append("\n")
            fastq2.append("@M01032:387:000000000-ANP68:1:1102:${hapid}:${startPos} 2:N:0:CCTAAGAC+GCGTAAGA").append("\n")
            val read1 = sequence.substring(startPos, startPos + readLength)
            val read2 = sequence.substring(startPos + readLength + insertSize, startPos + 2 * readLength + insertSize)

            fastq1.append(read1).append("\n")
            fastq1.append("+\n")
            fastq1.append(qualityScores).append("\n")

            fastq2.append(reverseCompliment(read2)).append("\n")
            fastq2.append("+\n")
            fastq2.append(qualityScores).append("\n")
        }
        return listOf(fastq1.toString(), fastq2.toString())
    }

    fun reverseCompliment(seq : String) : String {
        return seq.asSequence().map {complimentMap[it] ?: "N"}.joinToString("").reversed()
    }

    /**
     * average coverage of reads for a reference range
     *
     * @return Coverage
     */
    fun coverage(): Double {
        return coverage.value()
    }

    /**
     * Set Coverage. average coverage of reads for a reference
     * range
     *
     * @param value Coverage
     *
     * @return this plugin
     */
    fun coverage(value: Double): SimulatedReadsPlugin {
        coverage = PluginParameter<Double>(coverage, value)
        return this
    }

    /**
     * the name of the line for which reads will be simulated.
     * If not specified, all lines will be simulated for the
     * haplotype method.
     *
     * @return Line Name
     */
    fun lineName(): String? {
        return lineName.value()
    }

    /**
     * Set Line Name. the name of the line for which reads
     * will be simulated. If not specified, all lines will
     * be simulated for the haplotype method.
     *
     * @param value Line Name
     *
     * @return this plugin
     */
    fun lineName(value: String): SimulatedReadsPlugin {
        lineName = PluginParameter<String>(lineName, value)
        return this
    }

    /**
     * the haplotype method used to simulate reads
     *
     * @return Haplotype Method
     */
    fun haplotypeMethod(): String {
        return haplotypeMethod.value()
    }

    /**
     * Set Haplotype Method. the haplotype method used to
     * simulate reads
     *
     * @param value Haplotype Method
     *
     * @return this plugin
     */
    fun haplotypeMethod(value: String): SimulatedReadsPlugin {
        haplotypeMethod = PluginParameter<String>(haplotypeMethod, value)
        return this
    }

    /**
     * the directory to which the simulated read fastq will
     * be written. The default will write to a temp directory,
     * which will not be available after the plugin finishes.
     *
     * @return Fastq Dir
     */
    fun fastqDir(): String {
        if (fastqDir.value().endsWith("/")) return fastqDir.value()
        return "${fastqDir.value()}/"
    }

    /**
     * Set Fastq Dir. the directory to which the simulated
     * read fastq will be written. The default will write
     * to a temp directory, which will not be available after
     * the plugin finishes.
     *
     * @param value Fastq Dir
     *
     * @return this plugin
     */
    fun fastqDir(value: String): SimulatedReadsPlugin {
        fastqDir = PluginParameter<String>(fastqDir, value)
        return this
    }

    /**
     * Should paired end reads be generated?
     *
     * @return Paired End
     */
    fun pairedEnd(): Boolean {
        return pairedEnd.value()
    }

    /**
     * Set Paired End. Should paired end reads be generated?
     *
     * @param value Paired End
     *
     * @return this plugin
     */
    fun pairedEnd(value: Boolean): SimulatedReadsPlugin {
        pairedEnd = PluginParameter<Boolean>(pairedEnd, value)
        return this
    }

    /**
     * length of reads to be generated
     *
     * @return Read Length
     */
    fun readLength(): Int {
        return readLength.value()
    }

    /**
     * Set Read Length. length of reads to be generated
     *
     * @param value Read Length
     *
     * @return this plugin
     */
    fun readLength(value: Int): SimulatedReadsPlugin {
        readLength = PluginParameter<Int>(readLength, value)
        return this
    }

    /**
     * for paired end reads the number of base pairs between
     * reads
     *
     * @return Insert Length
     */
    fun insertLength(): Int {
        return insertLength.value()
    }

    /**
     * Set Insert Length. for paired end reads the number
     * of base pairs between reads
     *
     * @param value Insert Length
     *
     * @return this plugin
     */
    fun insertLength(value: Int): SimulatedReadsPlugin {
        insertLength = PluginParameter<Int>(insertLength, value)
        return this
    }

}

//fun main() {
//    GeneratePluginCode.generateKotlin(SimulatedReadsPlugin::class.java)
//}