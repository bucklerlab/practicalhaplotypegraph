#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/CML277_HFFGKADXX_GATCAG.bam \
   -I /SSD/terry/haplotype_caller/bam_files/CML277_HL5WNCCXX_L3.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/CML277_HB1JCADXX_GATCAG.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o CML277_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant CML277_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o CML277_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V CML277_haplotype_caller_output.vcf \
   -o CML277_haplotype_caller_output.fasta

