package net.maizegenetics.pangenome.hapCalling

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertNotNull
import kotlin.test.assertEquals
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.hapcollapse.KmerHashMapFromGraphTest
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import java.io.File
import kotlin.test.fail

@ExtendWith(SmallSeqExtension::class)
class KmerReadMapperIT {
    companion object {
        val kmerMapFile = "${SmallSeqExtension.baseDir}/kmerHashMap.txt"
        val kmerInputFileDir = SmallSeqExtension.dataDir
        val kmerKeyFile = "${SmallSeqExtension.baseDir}/kmerKeyFile.txt"
        val kmerPathKeyFile = "${SmallSeqExtension.baseDir}/kmerKeyFile_pathKeyFile.txt"
        val kmerHapIdMap = "${SmallSeqExtension.baseDir}/kmer_hapid_map.json"
        val outputDir = "${System.getProperty("user.home")}/phgTests_kmerTests/"

        val kmerMethodName = "KmerReadMapper_SmallSeq"
        val kmerMethodDescription = "UNUSED"
        val kmerPluginParams = mapOf<String, String>() // TODO: Do we need any params?
        val kmerDebugDir = SmallSeqExtension.readMappingDirKmers

        val kmerIsTestMethod = false
        val kmerNumThreads = 1

        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .includeVariantContexts(false)
            .includeSequences(false)
            .methods(SmallSeqExtension.haplotypeMethod)
            .configFile(SmallSeqExtension.configFilePath)
            .performFunction(null)

        @JvmStatic
        @BeforeAll
        fun setup() {
            ParameterCache.load(SmallSeqExtension.configFilePath)

            // Create kmer hash map file
            val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqExtension.configFilePath)
                .methods(SmallSeqExtension.haplotypeMethod)
                .build()
            val hashMapper = KmerHashMapFromGraph(graph)
            val map = hashMapper.processGraphKmersSinglePass()
            hashMapper.saveKmerHashesAndHapids(kmerMapFile, map,true)

            CreateHapIdMapsPlugin().outputMapFile(kmerHapIdMap).performFunction(graphDS)

            File(kmerDebugDir).mkdirs()
            File(SmallSeqExtension.genotypingKeyFilePath)
                .copyTo(File(kmerKeyFile), overwrite = true)
            File(outputDir).mkdirs()
            File("${outputDir}/debug/").mkdirs()
        }
    }

    @Disabled
    @Test
    fun testFastqFromKeyFile() {
        fail("Not yet implemented Fully.  Hard to compare counts.")
        KmerReadMapper.mapFastqFromKeyFile(
            kmerHashMappingFile = kmerMapFile,
            inputFileDir = kmerInputFileDir,
            keyFileName = kmerKeyFile,
            methodName = kmerMethodName,
            methodDescription = kmerMethodDescription,
            pluginParams = kmerPluginParams,
            outputDebugReadMappingDir = kmerDebugDir,
            isTestMethod = kmerIsTestMethod,
            nThreads = kmerNumThreads,
            updateDb = true,
            inputFileName = "",
        )
        checkMappingResults()
    }

    @Disabled
    @Test
    fun testFastqFromKeyFileNoGraph() {
        val hapIdMaps = deserializeGraphIdMaps(kmerHapIdMap)
        KmerReadMapper.mapFastqFromKeyFileNoGraph(
            kmerHashMappingFile = kmerMapFile,
            inputFileDir = kmerInputFileDir,
            keyFileName = kmerKeyFile,
            methodName = kmerMethodName,
            methodDescription = kmerMethodDescription,
            pluginParams = kmerPluginParams,
            outputDebugReadMappingDir = kmerDebugDir,
            nThreads = kmerNumThreads,
            inputFileName = "",
            graphIdMaps = hapIdMaps,
        )
    }

    @Disabled
    @Test
    fun testMapReadsPairedFastq() {
        // TODO: Test KmerReadMapper.mapReadsFromPairedFastqFiles()
        //       SmallSeq test data does not currently provide
        //       paired-end FASTQS (they are all R1)
        // NOTE: Need to add filename2 column to `kmerKeyFile` for second paired-end read
        fail("Not yet implemented.")
    }

    // TODO: Determine appropriate tests for kmer read mapping
    fun checkMappingResults() {
        val phg = PHGdbAccess(DBLoadingUtils.connection(false))
        val readMappingDecoder = ReadMappingDecoder(phg)

        val datasetResult = graphDS?.getDataOfType(HaplotypeGraph::class.java)
        assertNotNull(datasetResult)
        assertEquals(1, datasetResult.size, "Must be a single HaplotypeGraph")
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph

        val originalMappingKeyFile = readKeyFile(SmallSeqExtension.genotypingPathKeyFilePath)
        val kmerMappingKeyFile = readKeyFile(kmerPathKeyFile)

        assertEquals(
            originalMappingKeyFile.keys.size,
            kmerMappingKeyFile.keys.size,
            "Number of taxa in keyfiles do not match"
        )
        assertEquals(
            originalMappingKeyFile.keys.size,
            originalMappingKeyFile.keys.filter { kmerMappingKeyFile.keys.contains(it) }.count(),
            "All keys in original are not in kmer keyfile")
        assertEquals(
            kmerMappingKeyFile.keys.size,
            kmerMappingKeyFile.keys.filter { originalMappingKeyFile.keys.contains(it) }.count(),
            "All Keys in kmer are not in original keyfile"
        )

        // Compare ReadMappings for each taxon
        originalMappingKeyFile.keys.forEach { taxon ->
            val originalReadMappings = readMappingDecoder
                .getDecodedReadMappingForMappingId(originalMappingKeyFile[taxon]?.toInt() ?: -1)
            val kmerReadMappings = readMappingDecoder
                .getDecodedReadMappingForMappingId(kmerMappingKeyFile[taxon]?.toInt() ?: -1)

            // Check to see if the hapIdSets are equivalent
            assertEquals(
                originalReadMappings.keys.size,
                originalReadMappings.keys.filter { kmerReadMappings.keys.contains(it) }.count(),
                "Original read mapping HapId sets are missing from Kmers: $taxon"
            )
            assertEquals(
                kmerReadMappings.keys.size,
                kmerReadMappings.keys.filter { originalReadMappings.keys.contains(it) }.count(),
                "Kmer read mapping HapId sets are missing from original: $taxon"
            )

            originalReadMappings.keys.forEach { hapIdList ->
                assertEquals(
                    originalReadMappings[hapIdList],
                    kmerReadMappings[hapIdList],
                    "Counts are different for hapIds:$hapIdList"
                )
            }
        }

        phg.close()
    }

    /**
     * Very rudimentary unit test checking that mapping 100bp reads against the dummy kmer map will create the correct number of counts
     */
    @Test
    fun buildKmerMapFromDummyGraph() {
        ParameterCache.load(SmallSeqExtension.configFilePath)
        val graph = buildSimpleDummyGraph()

        val createPlugin = CreateHapIdMapsPlugin()

        val hapIdMap = getHapToRefRangeMap(graph)
        val hapIdToSequenceLength = getHapIdToSequenceLength(graph)
        val refRangeToHapidMap = getRefRangeToHapidMap(graph)

        createPlugin.createAndWriteHapIdMaps(hapIdMap, hapIdToSequenceLength, refRangeToHapidMap, 1 , mapOf(),"${outputDir}/hapIdMapFile.txt")

        //Build the kmerMap with default parameters
        val kmerMapFromGraph  = KmerHashMapFromGraph(graph, .75, 3L, 1L)
        val map = kmerMapFromGraph.processGraphKmersSinglePass()
        //Save out the map:
        kmerMapFromGraph.saveKmerHashesAndHapids("${outputDir}/kmerMapFromGraphTest.txt", map,false)

        //create a set of reads from the a set of nodes for sample1 in the graph
        val taxa1Nodes = graph.referenceRanges().flatMap { graph.nodes(it) }.filter { it.taxaList().contains(Taxon("sample1")) }

        //Extract 100bp reads from the nodes in taxa1Nodes
        val reads = taxa1Nodes.flatMap { it.haplotypeSequence().sequence().windowed(100,1,false) }

        //write out the reads into a fastq file
        writeOutSampleFastq(reads, "sample1")

        //Make a keyfile
        Utils.getBufferedWriter("${outputDir}/keyfile.txt").use { writer ->
            writer.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
            writer.write("sample1\tdummy1\tsample1.fastq\t1\n")
        }

        //Now, read in the reads and map them to the graph
        KmerReadMapperPlugin()
            .kmerMapFilePath("${outputDir}/kmerMapFromGraphTest.txt")
            .inputFileDirectory(outputDir)
            .keyFilePath("${outputDir}/keyfile.txt")
            .readMethod("TEST_KMER_MAPPING_ReadMappings")
            .debugDir("${outputDir}/debug/")
            .isTestMethod(true)
            .numberOfThreads(5)
            .updateDB(false)
            .runWithoutGraph(true)
            .inputFile("sample1.fastq")
            .hapIdMapFile("${outputDir}/hapIdMapFile.txt")
            .performFunction(null)


        //check the result of the kmer mapping found in the debug directory against the expected result
        val generatedReadMappings = Utils.getBufferedReader("${outputDir}/debug/sample1_dummy1_ReadMapping.txt").readLines()

        val mappings = generatedReadMappings.filter { !it.startsWith("#") && !it.startsWith("HapIds")}
            .map { it.split("\t") }
            .associate { it[0].split(",").map{id -> id.toInt()}.toSortedSet() to it[1].toInt() }

        //get graphIds for sample1 from graph
        val graphIds = taxa1Nodes.map { it.id() }.toSet()
        println(graphIds)

        for(hapIdSet in mappings.keys) {
            for(hapId in hapIdSet) {
                //because this is a very simple use case, there should only be one and it should match sample1 ids
                Assertions.assertTrue(hapId in graphIds, "Discrepancy in read mapping hapIds")
            }
        }

        //Because the reads are the entire length of the haplotype, there should only be one mapping count per read mapping directly to the haplotype from which it came
        for(mappingCount in mappings.values) {
            assertEquals(1, mappingCount, "Discrepancy in read mapping count")
        }
    }

    private fun writeOutSampleFastq(reads: List<String>, sampleName: String) {
        Utils.getBufferedWriter("${outputDir}/${sampleName}.fastq").use { writer ->
            reads.forEachIndexed { index, read ->
                writer.write("@read$index\n")
                writer.write("$read\n")
                writer.write("+\n")
                writer.write("I".repeat(read.length))
                writer.write("\n")
            }
        }
    }

    fun readKeyFile(filename: String): Map<String, String> {
        return Utils.getBufferedReader(filename).readLines()
            .filter { !it.startsWith("SampleName") }.map { it.split("\t") }
            .map { Pair(it[0],it[1]) }
            .toMap()
    }
}