package net.maizegenetics.pangenome.processAssemblyGenomes

import htsjdk.samtools.SAMSequenceDictionary
import htsjdk.samtools.SAMSequenceRecord
import htsjdk.variant.variantcontext.Allele
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.variantcontext.VariantContextBuilder
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import htsjdk.variant.vcf.VCFHeader
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.pangenome.db_loading.bgzipAndIndexGVCFfile
import net.maizegenetics.pangenome.db_loading.exportVariantContext
import net.maizegenetics.pangenome.hapCalling.HapCallingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon


/**
 * This plugin will take in a MAF file and will output a GVCF file for use in the PHG.
 * It will keep track of SNP, indel and Ref positions and will also keep track of the ASM chrom, start, end and strand positions.
 *
 * This allows you to look up the anchorwave aligned positions with respect to the reference.
 */
class MAFToGVCFPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(MAFToGVCFPlugin::class.java)

    enum class OUTPUT_TYPE {
        gvcf,vcf
    }


    data class MAFRecord(val score: Double, val refRecord: AlignmentBlock, val altRecord : AlignmentBlock)
    data class AlignmentBlock(val chromName: String, val start: Int, val size: Int, val strand : String, val chrSize: Int, val alignment: String)

    data class AssemblyVariantInfo (var chr : String, var startPos : Int, var endPos : Int, var genotype : String, var refAllele : String,
                                    var altAllele : String, var isVariant: Boolean, var alleleDepths : IntArray = intArrayOf(),
                                    var asmChrom : String = "", var asmStart : Int = -1, var asmEnd : Int = -1, var asmStrand : String="")

    //Making these early so we do not need to continually make int arrays which hold the same depths.
    val refDepth = intArrayOf(30,0)
    val altDepth = intArrayOf(0,30,0)
    val missingDepth = intArrayOf(0,0,0)

    //These arrays represent the PL(Phred-likelihood) tag in VCF.  Basically the lowest number is the most likely allele.
    //In the case of refPL the first is the most likely and in the case of the altPL the first alternate allele is the most likely.
    val refPL = intArrayOf(0,90,90)
    val altPL = intArrayOf(90,90,0)
    val missingPL = intArrayOf(90,90,90)

    //Value to set the DP tag for each of the variants.
    val totalDP = 30

    private var myReference = PluginParameter.Builder("referenceFasta", null, String::class.java)
        .description("Input Reference Fasta")
        .required(true)
        .inFile()
        .build()

    private var myMAFFile = PluginParameter.Builder("mafFile", null, String::class.java)
        .description("Input MAF file.  Please note that this needs to be a MAF file with 2 samples.  " +
                "The first will be assumed to be the Reference and the second will be the assembly.")
        .required(true)
        .inFile()
        .build()

    private var sampleName = PluginParameter.Builder("sampleName", null, String::class.java)
        .description("Sample Name to write to the GVCF file")
        .required(true)
        .build()

    private var myGVCFOutput = PluginParameter.Builder("gvcfOutput", null, String::class.java)
        .description("Output GVCF file. If generating two output files, _1 and _2 will be appended to the filename.")
        .required(true)
        .outFile()
        .build()

    private var fillGaps = PluginParameter.Builder("fillGaps", false, Boolean::class.javaObjectType)
        .description("When true, if the maf file does not fully cover the reference genome any gaps " +
                "in coverage will be filled in with reference blocks. This is necessary if the resulting GVCFs are to be combined.")
        .build()

    private var twoGvcfs = PluginParameter.Builder("twoGvcfs", false, Boolean::class.javaObjectType)
        .description("The input maf was created from a diploid alignment and should be used to create two separate gVCF files.")
        .build()

    private var outputJustGT = PluginParameter.Builder("outputJustGT", false,Boolean::class.javaObjectType)
        .description("Output just the GT flag.  If set to false(default) will output DP, AD and PL fields")
        .build()

    private var outputType = PluginParameter.Builder("outputType", OUTPUT_TYPE.gvcf,OUTPUT_TYPE::class.javaObjectType)
        .description("Output GVCF typed files. If set to gvcf(default) it will send all REFBlocks, SNPs and Indels.  If set to vcf, it will only output SNPs, Indels and missing values(if fillGaps = true) ")
        .build()

    private var bgzipAndIndex = PluginParameter.Builder("bgzipAndIndex", true,Boolean::class.javaObjectType)
        .description("If set to true(default) will bgzip and index the output GVCF file.  " +
                "If Bgzip and Tabix are not on the system path this will fail if set to true.")
        .build()

    override fun processData(input: DataSet?): DataSet? {
        myLogger.info("Loading in Reference File")
        val refGenomeSequence = GenomeSequenceBuilder.instance(reference())

        val variantsMap = createVariantContextsFromMAF(refGenomeSequence)
        check(variantsMap.size == 1 || variantsMap.size == 2) {"Expected either 1" +
                " or 2 variant maps but there are ${variantsMap.size}"}

        if (variantsMap.size == 1) {
            val sampleName = variantsMap.keys.first()
            val variants = variantsMap.values.first()
            exportVariantContext(sampleName, variants, gVCFOutput(), refGenomeSequence)
            if(bgzipAndIndex()) {
                // compress and index the file with bgzip and tabix.
                bgzipAndIndexGVCFfile(gVCFOutput())
            }
        } else if (variantsMap.size == 2) {
            val outputNames = twoOutputFiles()
            variantsMap.entries.forEachIndexed { index, (name, variants) ->
                val outputFile = exportVariantContext(name, variants, outputNames[index], refGenomeSequence)
                if(bgzipAndIndex()) {
                    bgzipAndIndexGVCFfile(outputNames[index])
                }
            }
        }

        return null
    }

    fun twoOutputFiles() : Array<String> {
        val outname = gVCFOutput()

        return when {
            outname.contains(",") -> {
                outname.split("'").toTypedArray()
            }
            outname.endsWith(".g.vcf") -> {
                val prefix = outname.substringBefore(".g.vcf")
                arrayOf("${prefix}_1.g.vcf","${prefix}_2.g.vcf")
            }
            outname.endsWith(".gvcf") -> {
                val prefix = outname.substringBefore(".gvcf")
                arrayOf("${prefix}_1.gvcf","${prefix}_2.gvcf")
            }
            outname.endsWith(".vcf") -> {
                val prefix = outname.substringBefore("vcf")
                arrayOf("${prefix}_1.vcf","${prefix}_2.vcf")
            }
            else -> {
                arrayOf("${outname}_1.gvcf","${outname}_2.gvcf")
            }
        }
    }

    fun createVariantContextsFromMAF(refGenomeSequence: GenomeSequence): Map<String, List<VariantContext>> {
        myLogger.info("Loading in MAF file")
        val mafRecords = MafUtils.loadInMAFFile(mAFFile())

        return if (twoGvcfs()) {
            val splitGenomes = splitMafRecordsIntoTwoGenomes(mafRecords)

            if (splitGenomes.size == 1) {
                myLogger.info("${mAFFile()} had no overlapping blocks and was not split")
                mapOf(sampleName() to buildVariantsForAllAlignments(sampleName(), mafRecords, refGenomeSequence))
            }

            else {
                splitGenomes.mapIndexed { index, mafrecs ->
                    //append _1 and _2 to the sampleName for the split genomes
                    val genomeName = "${sampleName()}_${index + 1}"
                    Pair(genomeName, buildVariantsForAllAlignments(genomeName, mafrecs, refGenomeSequence))
                }.toMap()
            }

        } else {
            mapOf(sampleName() to buildVariantsForAllAlignments(sampleName(), mafRecords, refGenomeSequence))
        }
    }

    private fun splitMafRecordsIntoTwoGenomes(mafRecords: List<MAFRecord>) : List<List<MAFRecord>> {
        //split maf records into two non-overlapping sets
        //since the maf records are ordered by reference chr, start they can be handled sequentially

        myLogger.info("Splitting genomes")
        val genome1 = mutableListOf<MAFRecord>()
        val genome2 = mutableListOf<MAFRecord>()

        var lastGenome1Record = mafRecords.first()
        genome1.add(lastGenome1Record)

        mafRecords.drop(1).forEach { record ->
            if (recordsOverlap(lastGenome1Record, record)) genome2.add(record)
            else {
                lastGenome1Record = record
                genome1.add(record)
            }
        }

        //do any genome2 records overlap? If so, throw an exception
        myLogger.info("Checking for overlaps in second genome")
        for (ndx in 1 until genome2.size) {
            if (recordsOverlap(genome2[ndx - 1], genome2[ndx])) {
                val record1 = genome2[ndx - 1]
                val record2 = genome2[ndx]
                throw IllegalStateException("Genome2 records overlap: " +
                        "${record1.refRecord.chromName}:${record1.refRecord.start} to ${record1.refRecord.start + record1.refRecord.size - 1} and " +
                        "${record2.refRecord.chromName}:${record2.refRecord.start} to ${record2.refRecord.start + record2.refRecord.size - 1}")
            }
        }

        //both genomes should cover all positions, so
        //add sequence from genome2 missing in genome1 to genome1
        MafUtils.augmentList(genome1, genome2)
        genome1.sortWith(compareBy({ Chromosome.instance(it.refRecord.chromName.split(".").last()) }, { it.refRecord.start }))

        //add sequence from genome1 missing in genome2 to genome2
        MafUtils.augmentList(genome2, genome1)
        genome2.sortWith(compareBy({ Chromosome.instance(it.refRecord.chromName.split(".").last()) }, { it.refRecord.start }))

        return listOf(genome1, genome2)
    }

    private fun recordsOverlap(mafRecord1: MAFRecord, mafRecord2: MAFRecord) : Boolean {
        return when {
            //chromosome names are not the same -> no overlap
            mafRecord1.refRecord.chromName != mafRecord2.refRecord.chromName -> false
            //record1 end comes before record2 -> no overlap
            mafRecord1.refRecord.start + mafRecord1.refRecord.size <= mafRecord2.refRecord.start -> false
            //record1 start comes after record2 -> no overlap
            mafRecord1.refRecord.start >= mafRecord2.refRecord.start + mafRecord2.refRecord.size -> false
            //otherwise -> overlap
            else -> true
        }
    }

    /**
     * Function to build the variants for all the alignments.
     */
    fun buildVariantsForAllAlignments(sampleName: String, mafRecords:List<MAFRecord>, refGenomeSequence: GenomeSequence) : List<VariantContext>{
        var variantInfos = mutableListOf<AssemblyVariantInfo>()

        for(record in mafRecords) {
            variantInfos.addAll(buildTempVariants(refGenomeSequence, record))
        }

        if(fillGaps() && outputType() == OUTPUT_TYPE.vcf) {
            //If we are returning a VCF, we first need to determine where the missing regions are.  We can use FillInMissingVariantBlocks to do so.
            //Then once we have the gaps filled in, we remove any referenceBlocks and we have out VCF with missing values.
            variantInfos = fillInMissingVariantBlocks(variantInfos, refGenomeSequence, false)
            variantInfos = removeRefBlocks(variantInfos)
        }
        else if (fillGaps()) {
            variantInfos = fillInMissingVariantBlocks(variantInfos, refGenomeSequence, true)
        }

        return createVariantContextsFromInfo(sampleName, variantInfos)
    }

    /**
     * Function to build the AssemblyVariantInfos found in the given Maf record.
     */
    fun buildTempVariants(refSequence: GenomeSequence, mafRecord : MAFRecord) : List<AssemblyVariantInfo> {
        //Build a list of VariantInfos for each alignment state
        val chrom = Chromosome.instance(mafRecord.refRecord.chromName.split(".").last())

        val refAlignment = mafRecord.refRecord.alignment
        val altAlignment = mafRecord.altRecord.alignment

        // Display alignment data to help identify alignment block which with problem.
        check(refAlignment.length == altAlignment.length) {"Ref and Alt alignments are not the same size, chrom=${chrom}, refsize=${refAlignment.length}, altsize=${altAlignment.length}, score=${mafRecord.score}, refStart=${mafRecord.refRecord.start}, refSize=${mafRecord.refRecord.size},altStart=${mafRecord.altRecord.start}, altSize=${mafRecord.altRecord.size}"}


        var currentAlignmentBp = 0   //position in both alignment blocks
        val asmStrand = mafRecord.altRecord.strand     //We need to keep track of the strand as well
        check(asmStrand =="-" || asmStrand == "+") {"ASM Strand is not - or +."}
        var currentRefBp = mafRecord.refRecord.start   //position in ref sequence. That is, alignment bp minus dashes for REF line
        var currentASMBp = if(asmStrand == "-") {mafRecord.altRecord.start + mafRecord.altRecord.size -1  }
        else {mafRecord.altRecord.start}//position in the alt sequence.  That is alignment bp minus dashes for ASM line

        val asmChrom = Chromosome.instance(mafRecord.altRecord.chromName.split(".").last()).name
        var currentRefBlockBoundaries = Pair(-1,-1)
        var currentAsmBlockBoundaries = Pair(-1,-1)
        val variantList = mutableListOf<AssemblyVariantInfo>()

        //Does the refAlignment or the altAlignment start with a dash(gap)?
        //if so, move to the first position at both ref and alt have nucleotides
        //The refAllele will be the ref string up to and including that position, without dashes.
        //The altAllele will be the alt string up to and including that position, without dashes.


        if((refAlignment[currentAlignmentBp] == '-' || altAlignment[currentAlignmentBp] == '-') && currentRefBp==1) {
            val startingAlt = java.lang.StringBuilder()
            val startingRef = java.lang.StringBuilder()

            val asmCurrentStart = currentASMBp //Keep track of the initial asmStart position.

            //Handle an insertion at the start of the chrom
            //Keep processing until the ref and alt nucleotides are present
            while (refAlignment[currentAlignmentBp] == '-'  || altAlignment[currentAlignmentBp] == '-') {
                val refChar = refAlignment[currentAlignmentBp]
                val altChar = altAlignment[currentAlignmentBp]
                if (refChar != '-') {
                    startingRef.append(refChar)
                    currentRefBp++
                }
                if (altChar != '-') {
                    startingAlt.append(altChar)
                    if(asmStrand == "-") {
                        currentASMBp--
                    }
                    else {
                        currentASMBp++
                    }
                }

                currentAlignmentBp++
            }

            //at this point both ref and alt chars are nucleotides
            startingRef.append(refAlignment[currentAlignmentBp])
            startingAlt.append(altAlignment[currentAlignmentBp])

            variantList += buildIndel(chrom, 1, startingRef.toString(), startingAlt.toString(),asmChrom, asmCurrentStart, asmStrand )

            currentRefBp++
            if(asmStrand == "-") {
                currentASMBp--
            }
            else {
                currentASMBp++
            }
            currentAlignmentBp++


        }

        while (currentAlignmentBp < refAlignment.length) {
            //If they are the same add to the current refBlock
            if(refAlignment[currentAlignmentBp] == '-' && altAlignment[currentAlignmentBp] == '-') {
                currentAlignmentBp++
            }
            else if(refAlignment[currentAlignmentBp] == altAlignment[currentAlignmentBp]) { //If the alleles match we have a reference block
                if(currentRefBlockBoundaries == Pair(-1,-1)) { //Check to see if its the first base pair in a reference block
                    //New RefBlock
                    currentRefBlockBoundaries = Pair(currentRefBp, currentRefBp)
                }
                else {//Otherwise its an existing RefBlock.
                    currentRefBlockBoundaries = Pair(currentRefBlockBoundaries.first, currentRefBp)
                }

                if(currentAsmBlockBoundaries == Pair(-1,-1)) { //Check to see if its the first bp for the assembly blocks
                    currentAsmBlockBoundaries = Pair(currentASMBp, currentASMBp)
                }
                else { //If its existing, just update.
                    currentAsmBlockBoundaries = Pair(currentAsmBlockBoundaries.first, currentASMBp)
                }


                currentRefBp++
                if(asmStrand == "-") {
                    currentASMBp--
                }
                else {
                    currentASMBp++
                }
                currentAlignmentBp++
            }
            else {
                //Check SNP, if SNP, write out the Previous refBlock and make a SNP VariantInfo, resetRefBlock
                if(currentRefBlockBoundaries != Pair(-1,-1)) {
                    variantList += buildRefBlockVariantInfo(refSequence, chrom, currentRefBlockBoundaries, asmChrom, currentAsmBlockBoundaries, asmStrand)
                }
                //resetRefBlock
                currentRefBlockBoundaries = Pair(-1,-1)
                currentAsmBlockBoundaries = Pair(-1, -1)

                //Make sure they both are not '-', If so its a SNP
                if (refAlignment[currentAlignmentBp] != '-' && altAlignment[currentAlignmentBp] != '-') {

                    //Write out SNP
                    variantList += buildSNP(chrom, currentRefBp, refAlignment[currentAlignmentBp], altAlignment[currentAlignmentBp], asmChrom, currentASMBp, asmStrand )

                    currentRefBp++
                    if(asmStrand == "-") {
                        currentASMBp--
                    }
                    else {
                        currentASMBp++
                    }
                    currentAlignmentBp++
                }
                else {
                    //If an indel, append to the previous variant
                    val prefix = if(variantList.isEmpty()) null else variantList.removeLast()

                    //If the previous variant is a refblock , drop the last nucleotide to resize the refblock then append it to the variantList
                    //The final nucleotide will be used to start the new indel

                    //If the previous variant is a refblock of length 1, prepend it to this deletion
                    //If the previous variant is a refblock of length > 1, prepend the last nucleotide of the ref block
                    //then make a new refblock without the final nucleotide
                    //If the previous variant is an SNP prepend it to the deletion
                    //If the previous variant is an indel, prepend it to the deletion
                    val refStringBuilder = StringBuilder()
                    val altStringBuilder = StringBuilder()

                    if(prefix == null) {
                        val allele = refSequence.genotypeAsString(chrom, currentRefBp)
                        refStringBuilder.append(allele)
                        altStringBuilder.append(allele)
                    }
                    else if (!prefix.isVariant) {
                        //the prefix is a ref block
                        if (prefix.endPos - prefix.startPos + 1 > 1) variantList += resizeRefBlockVariantInfo(prefix)
                        val startRefPos = prefix.endPos
                        val allele = refSequence.genotypeAsString(chrom, startRefPos)
                        refStringBuilder.append(allele)
                        altStringBuilder.append(allele)
                    } else  {
                        //the prefix is a SNP or an indel
                        refStringBuilder.append(prefix.refAllele)
                        altStringBuilder.append(prefix.altAllele)
                    }

                    //walk until the indel ends (both sequences are non-gap) or until the end of the block is reached
                    while (currentAlignmentBp < refAlignment.length &&
                        (refAlignment[currentAlignmentBp] == '-' || altAlignment[currentAlignmentBp] == '-')) {

                        if (refAlignment[currentAlignmentBp] != '-') {
                            refStringBuilder.append(refAlignment[currentAlignmentBp])
                            currentRefBp++
                        }
                        if (altAlignment[currentAlignmentBp] != '-') {
                            altStringBuilder.append(altAlignment[currentAlignmentBp])
                            if(asmStrand == "-") {
                                currentASMBp--
                            }
                            else {
                                currentASMBp++
                            }
                        }
                        currentAlignmentBp++
                    }

                    //create a variant info and append it to the list
                    //check to see if ref and alt are the same. It is rare but can happen and needs to called a ref block
                    val refString = refStringBuilder.toString()
                    val altString = altStringBuilder.toString()
                    val startRefPos = currentRefBp - refString.length
                    val startASMPos =  if(asmStrand=="-") {currentASMBp + altString.length}
                    else {currentASMBp - altString.length}
                    if (refString == altString) {
                        currentRefBlockBoundaries = Pair(startRefPos, currentRefBp - 1)
                        currentAsmBlockBoundaries = if(asmStrand=="-"){Pair(currentASMBp + 1, startASMPos)}
                        else {Pair(startASMPos, currentASMBp -1)}
                    }
                    //also need to check whether the alt and ref Strings are the same length
                    //if they are process them into SNPs and ref blocks
                    else if (refString.length == altString.length) {
                        variantList.addAll(processIdenticalLengthStrings(refString, altString, startRefPos, chrom, refSequence, asmChrom, startASMPos, asmStrand))

                    } else {
                        variantList+=buildIndel(chrom, startRefPos, refString, altString, asmChrom,startASMPos, asmStrand)
                    }
                }

            }
        }

        //Write out existing refBlock if we have one
        if(currentRefBlockBoundaries != Pair(-1,-1)) {
            variantList += buildRefBlockVariantInfo(refSequence, chrom, currentRefBlockBoundaries, asmChrom, currentAsmBlockBoundaries, asmStrand)
        }

        return variantList
    }

    /**
     * Function to fill in the missing variant blocks between MAF records.
     * If fillWithRef is set to true it will make VariantBlocks, if false it will make missing blocks.
     *
     * This will also not return an assembly coordinate in the output VariantInfos.
     * This is because Anchorwave did not find an alignment for these in-between regions.
     * We could try to put in some coords but it gets very tricky to do so due to inversions.
     */
    private fun fillInMissingVariantBlocks(tempVariantInfos: MutableList<AssemblyVariantInfo>, refGenomeSequence: GenomeSequence, fillWithRef : Boolean = true) : MutableList<AssemblyVariantInfo> {
        //Checks whether reference positions are contiguous with no missing positions.
        // If there are any gaps in coverage it inserts a reference block.
        // This is necessary because combining GVCFs requires that they fully cover the reference genome.
        //sort the resulting list by chromosome name and start position
        val tempSortedList = tempVariantInfos
            .map { Pair(Chromosome.instance(it.chr),it) }
            .toMutableList()

        tempSortedList.sortWith() { a, b ->
            val chrcomp = a.first.compareTo(b.first)
            if (chrcomp == 0) a.second.startPos.compareTo(b.second.startPos) else chrcomp

        }


        val finalSortedList = tempSortedList.map { it.second }

        var previousInfo = AssemblyVariantInfo("NA", 0, 0, "","","",false)
        val filledVariantList = mutableListOf<AssemblyVariantInfo>()
        for (varinfo in finalSortedList) {

            //Setting all the asm positions to be missing.  This is due to us not really knowing based on alignment where things need to be.
            val asmPositions = Pair(-1,-1)
            if (varinfo.chr == previousInfo.chr) {
                check(varinfo.startPos > previousInfo.endPos) {"VariantInfo start <= previous end at ${varinfo.chr}:${varinfo.startPos}. Previous end was ${previousInfo.endPos} "}

                //add a refblock if this start > previous end plus one
                if (varinfo.startPos > previousInfo.endPos + 1) {
                    val sameChr = (varinfo.asmChrom == previousInfo.asmChrom)

                    if(fillWithRef) {
                        filledVariantList.add(
                            buildRefBlockVariantInfoZeroDepth(
                                refGenomeSequence,
                                varinfo.chr,
                                Pair(previousInfo.endPos + 1, varinfo.startPos - 1),
                                if (sameChr) varinfo.asmChrom else "",
                                asmPositions,
                                ""
                            )
                        )
                    }
                    else {
                        filledVariantList.add(
                            buildMissingRegion(varinfo.chr,
                                previousInfo.endPos+1,
                                varinfo.startPos-1,
                                refGenomeSequence.genotypeAsString(Chromosome.instance(varinfo.chr), previousInfo.endPos+1,previousInfo.endPos+1),
                                if(sameChr) varinfo.asmChrom else "",
                                asmPositions.first,
                                asmPositions.second,
                                ""))
                    }
                }
            }
            else {
                //this is the first variant info in a chromosome
                //  check the previous variant info to make sure it ended at the chromosome end
                if (previousInfo.chr != "NA") {
                    val previousChromEnd = refGenomeSequence.chromosomeSize(Chromosome.instance(previousInfo.chr))
                    val sameChr = (varinfo.asmChrom == previousInfo.asmChrom)
                    if (previousInfo.endPos < previousChromEnd) {
                        if (fillWithRef) {
                            filledVariantList.add(
                                buildRefBlockVariantInfoZeroDepth(
                                    refGenomeSequence,
                                    previousInfo.chr,
                                    Pair(previousInfo.endPos + 1, previousChromEnd),
                                    if (sameChr) varinfo.asmChrom else "",
                                    asmPositions,
                                    ""
                                )
                            )
                        }
                        else {
                            filledVariantList.add(
                                buildMissingRegion(previousInfo.chr,
                                    previousInfo.endPos+1,
                                    previousChromEnd,
                                    refGenomeSequence.genotypeAsString(Chromosome.instance(previousInfo.chr), previousInfo.endPos+1,previousInfo.endPos+1),
                                    if(sameChr) varinfo.asmChrom else "",
                                    asmPositions.first,
                                    asmPositions.second,
                                    ""))
                        }
                    }
                }
                // if this variant does not start at one add a ref block
                if (varinfo.startPos > 1) {
                    val sameChr = (varinfo.asmChrom == previousInfo.asmChrom)
                    if(fillWithRef) {
                        filledVariantList.add(
                            buildRefBlockVariantInfoZeroDepth(
                                refGenomeSequence,
                                varinfo.chr,
                                Pair(1, varinfo.startPos - 1),
                                if (sameChr) varinfo.asmChrom else "",
                                asmPositions,
                                ""
                            )
                        )
                    }
                    else {
                        filledVariantList.add(
                            buildMissingRegion(varinfo.chr,
                                1,
                                varinfo.startPos-1,
                                refGenomeSequence.genotypeAsString(Chromosome.instance(varinfo.chr), 1,1),
                                if(sameChr) varinfo.asmChrom else "",
                                asmPositions.first,
                                asmPositions.second,
                                ""))
                    }
                }
            }

            filledVariantList.add(varinfo)
            previousInfo = varinfo
        }

        return filledVariantList
    }

    /**
     * Function to convert a multibp substitution into a series of SNPs.  This allows the GVCF to pass a vcf-validator.
     */
    private fun processIdenticalLengthStrings(refString: String, altString: String, startPos: Int, chrom: Chromosome, refseq: GenomeSequence, assemblyChrom: String, asmStartPos : Int, asmStrand: String): List<AssemblyVariantInfo> {
        //consolidate ref blocks
        val variantList = mutableListOf<AssemblyVariantInfo>()
        var block = Pair(-1,-1)
        var asmBlock = Pair(-1,-1)

        check(asmStrand=="+" || asmStrand=="-"){"ASM Strand is not + or -"}
        for (index in 0 until refString.length) {

            if (refString[index] != altString[index]) {
                //add the previous refBlock if there is one
                if (block.first > -1) {
                    variantList.add(buildRefBlockVariantInfo(refseq, chrom, block, assemblyChrom, asmBlock, asmStrand))
                    block = Pair(-1,-1)
                    asmBlock = Pair(-1,-1)
                }

                //add the SNP
                variantList.add(buildSNP(chrom, startPos + index, refString[index], altString[index], assemblyChrom, asmStartPos+index, asmStrand))
            }
            else if (block.first == -1) {
                block = Pair(startPos + index,startPos + index)
                asmBlock = if(asmStrand == "+") Pair(asmStartPos + index, asmStartPos + index)
                            else Pair(asmStartPos - index, asmStartPos - index)
            }
            else {
                block = Pair(block.first, startPos + index)
                asmBlock =if(asmStrand == "+") Pair(asmBlock.first, asmStartPos + index)
                            else Pair(asmBlock.first, asmStartPos - index)
            }

        }

        //if the final position was in a ref block add that
        if (block.first > -1) variantList.add(buildRefBlockVariantInfo(refseq, chrom, block,assemblyChrom, asmBlock,asmStrand))
        return variantList
    }

    fun removeRefBlocks(variantInfos: MutableList<AssemblyVariantInfo>) : MutableList<AssemblyVariantInfo> {
        return variantInfos.filter { it.isVariant }.toMutableList()
    }

    fun createVariantContextsFromInfo(sampleName: String, variantInfos: List<AssemblyVariantInfo>) : List<VariantContext> {
        return variantInfos.map { convertVariantInfoToContext(sampleName, it) }
    }

    /**
     * Function to turn the AssemblyVariantInfo into an actual VariantContext.
     * If the Assembly annotations are not in the VariantInfo, we do not add them into the VariantContext.
     */
    fun convertVariantInfoToContext(sampleName: String, variantInfo : AssemblyVariantInfo) : VariantContext {
        val startPos = variantInfo.startPos
        val endPos = variantInfo.endPos
        val refAllele = variantInfo.refAllele
        val altAllele = variantInfo.altAllele
        val alleleDepths = variantInfo.alleleDepths
        val chrom = variantInfo.chr

        val assemblyChrom = variantInfo.asmChrom
        val assemblyStart = variantInfo.asmStart
        val assemblyEnd = variantInfo.asmEnd
        val assemblyStrand = variantInfo.asmStrand

        val alleles = if(altAllele == "." || refAllele == altAllele) {
            listOf<Allele>(Allele.create(refAllele, true), Allele.NON_REF_ALLELE)
        }
        else {
            listOf<Allele>(Allele.create(refAllele, true), Allele.create(altAllele, false), Allele.NON_REF_ALLELE)
        }



        val genotype = if (variantInfo.isVariant) {
            if(variantInfo.genotype == ".") {
                listOf(Allele.NO_CALL)
            }
            else {
                listOf(alleles[1])
            }
        }
        else {
            listOf(alleles[0])
        }

        val plArray = if(variantInfo.isVariant) {
            altPL
        }
        else {
            refPL
        }



        val genotypeBuilder = GenotypeBuilder(sampleName,genotype)

        val currentGenotype = if(!outputJustGT()) {
            genotypeBuilder.DP(totalDP)
                .AD(alleleDepths)
                .PL(plArray)
        }
        else {
            genotypeBuilder
        }.make()


        val vcBuilder = VariantContextBuilder(".", chrom, startPos.toLong(), endPos.toLong(), alleles)

        if(!variantInfo.isVariant || altAllele == ".") {
            vcBuilder.attribute("END", endPos)
        }

        if(assemblyChrom != "") {
            vcBuilder.attribute("ASM_Chr", assemblyChrom)
        }

        if(assemblyStart != -1) {
            vcBuilder.attribute("ASM_Start", assemblyStart)
        }

        if(assemblyEnd != -1 ) {
            vcBuilder.attribute("ASM_End", assemblyEnd)
        }

        if(assemblyStrand != "") {
            vcBuilder.attribute("ASM_Strand", assemblyStrand)
        }

        return vcBuilder.genotypes(currentGenotype).make()
    }

    /**
     * Functio to build a reference block AssemblyVariantInfo
     */
    private fun buildRefBlockVariantInfo(refSequence: GenomeSequence, chrom: Chromosome, currentRefBlockBoundaries: Pair<Int, Int>, assemblyChrom : String, currentAssemblyBoundaries : Pair<Int,Int>, assemblyStrand : String): AssemblyVariantInfo {
        return AssemblyVariantInfo(chrom.name, currentRefBlockBoundaries.first, currentRefBlockBoundaries.second, "REF",
            refSequence.genotypeAsString(chrom, currentRefBlockBoundaries.first),".",false,
            refDepth, assemblyChrom, currentAssemblyBoundaries.first, currentAssemblyBoundaries.second, assemblyStrand)
    }

    /**
     * Method to build a Reference Block AssemblyVariantInfo setting the depth to 0.
     * This is mainly used to fill in missing basepairs between MAF entries.
     */
    private fun buildRefBlockVariantInfoZeroDepth(refSequence: GenomeSequence, chrom: String, currentRefBlockBoundaries: Pair<Int, Int>, assemblyChrom : String, currentAssemblyBoundaries : Pair<Int,Int>, assemblyStrand: String): AssemblyVariantInfo {
        return AssemblyVariantInfo(chrom, currentRefBlockBoundaries.first, currentRefBlockBoundaries.second, "REF",
            refSequence.genotypeAsString(Chromosome.instance(chrom), currentRefBlockBoundaries.first),".",false,
            intArrayOf(0,0), assemblyChrom, currentAssemblyBoundaries.first, currentAssemblyBoundaries.second, assemblyStrand)
    }

    /**
     * Method to resize the previous Reference block Variant Info.  We only need to delete 1 bp off the end of the Blocks.
     * We need to do this otherwise we will cover base pairs surrounding the indels.
     */
    private fun resizeRefBlockVariantInfo(variantInfo: AssemblyVariantInfo): AssemblyVariantInfo {
        check(variantInfo.asmStrand=="-" || variantInfo.asmStrand=="+") {"ASM Strand is not + or -"}
        val asmStart = variantInfo.asmStart
        val asmEnd = if(variantInfo.asmStrand == "-") variantInfo.asmEnd+1 else variantInfo.asmEnd-1

        return AssemblyVariantInfo(variantInfo.chr, variantInfo.startPos, variantInfo.endPos-1, variantInfo.genotype,
            variantInfo.refAllele,variantInfo.altAllele,variantInfo.isVariant,
            variantInfo.alleleDepths, variantInfo.asmChrom, asmStart, asmEnd , variantInfo.asmStrand)
    }

    /**
     * Method to build SNP AssemblyVariantInfos
     */
    private fun buildSNP(chrom: Chromosome, position:Int, refAllele: Char, altAllele : Char, assemblyChrom : String, assemblyPosition : Int, assemblyStrand: String) : AssemblyVariantInfo {
        return  AssemblyVariantInfo(chrom.name, position, position, "${altAllele}","$refAllele",
            "${altAllele}", true, altDepth, assemblyChrom, assemblyPosition, assemblyPosition, assemblyStrand )
    }

    /**
     * Method to build an indel AssemblyVariantInfos.
     */
    private fun buildIndel(chrom: Chromosome, position: Int, refAlleles:String, altAlleles: String, assemblyChrom: String, assemblyStart : Int, assemblyStrand: String, startInsertion:Boolean = false) : AssemblyVariantInfo {
        check(assemblyStrand == "+" || assemblyStrand=="-") {"Assembly Strand is not + or -"}
        val assemblyEnd = if(startInsertion) {
                            assemblyStart
                        }
                        else if(assemblyStrand =="+") {
                            assemblyStart + altAlleles.length -1
                        }
                        else {
                            assemblyStart - altAlleles.length +1
                        }

        return  AssemblyVariantInfo(chrom.name, position, position + refAlleles.length -1, altAlleles,refAlleles,
            altAlleles, true, altDepth , assemblyChrom, assemblyStart, assemblyEnd , assemblyStrand)
    }

    private fun buildMissingRegion(chrom: String, startPos: Int, endPos: Int, refAlleles : String,
                                   assemblyChrom: String, assemblyStart: Int, assemblyEnd : Int, assemblyStrand: String) : AssemblyVariantInfo {
        return AssemblyVariantInfo(chrom, startPos, endPos, ".",refAlleles, ".", true, missingDepth, assemblyChrom, assemblyStart, assemblyEnd, assemblyStrand)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MAFToGVCFPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MAFToGVCFPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin create a GVCF from a MAF file."
    }



    /**
     * Input Reference Fasta
     *
     * @return Reference Fasta
     */
    fun reference(): String {
        return myReference.value()
    }

    /**
     * Set Reference Fasta. Input Reference Fasta
     *
     * @param value Reference Fasta
     *
     * @return this plugin
     */
    fun reference(value: String): MAFToGVCFPlugin {
        myReference = PluginParameter<String>(myReference, value)
        return this
    }

    /**
     * Input MAF file
     *
     * @return Maf File
     */
    fun mAFFile(): String {
        return myMAFFile.value()
    }

    /**
     * Set Maf File. Input MAF file
     *
     * @param value Maf File
     *
     * @return this plugin
     */
    fun mAFFile(value: String): MAFToGVCFPlugin {
        myMAFFile = PluginParameter<String>(myMAFFile, value)
        return this
    }

    /**
     * Sample Name to write to the GVCF file
     *
     * @return Sample Name
     */
    fun sampleName(): String {
        return sampleName.value()
    }

    /**
     * Set Sample Name. Sample Name to write to the GVCF file
     *
     * @param value Sample Name
     *
     * @return this plugin
     */
    fun sampleName(value: String): MAFToGVCFPlugin {
        sampleName = PluginParameter<String>(sampleName, value)
        return this
    }

    /**
     * Output GVCF file
     *
     * @return Gvcf Output
     */
    fun gVCFOutput(): String {
        return myGVCFOutput.value()
    }

    /**
     * Set Gvcf Output. Output GVCF file
     *
     * @param value Gvcf Output
     *
     * @return this plugin
     */
    fun gVCFOutput(value: String): MAFToGVCFPlugin {
        myGVCFOutput = PluginParameter<String>(myGVCFOutput, value)
        return this
    }

    /**
     * When true, if the maf file does not fully cover the
     * reference genome any gaps in coverage will be filled
     * in with reference blocks. This is necessary if the
     * resulting GVCFs are to be combined.
     *
     * @return Fill Gaps
     */
    fun fillGaps(): Boolean {
        return fillGaps.value()
    }

    /**
     * Set Fill Gaps. When true, if the maf file does not
     * fully cover the reference genome any gaps in coverage
     * will be filled in with reference blocks. This is necessary
     * if the resulting GVCFs are to be combined.
     *
     * @param value Fill Gaps
     *
     * @return this plugin
     */
    fun fillGaps(value: Boolean): MAFToGVCFPlugin {
        fillGaps = PluginParameter<Boolean>(fillGaps, value)
        return this
    }

    /**
     * The input maf was created from a diploid alignment
     * and should be used to create two separate gVCF files.
     *
     * @return Two Gvcfs
     */
    fun twoGvcfs(): Boolean {
        return twoGvcfs.value()
    }

    /**
     * Set Two Gvcfs. The input maf was created from a diploid
     * alignment and should be used to create two separate
     * gVCF files.
     *
     * @param value Two Gvcfs
     *
     * @return this plugin
     */
    fun twoGvcfs(value: Boolean): MAFToGVCFPlugin {
        twoGvcfs = PluginParameter(twoGvcfs, value)
        return this
    }

    /**
     * Output just the GT flag.  If set to false(default)
     * will output DP, AD and PL fields
     *
     * @return Output Just G T
     */
    fun outputJustGT(): Boolean {
        return outputJustGT.value()
    }

    /**
     * Set Output Just G T. Output just the GT flag.  If set
     * to false(default) will output DP, AD and PL fields
     *
     * @param value Output Just G T
     *
     * @return this plugin
     */
    fun outputJustGT(value: Boolean): MAFToGVCFPlugin {
        outputJustGT = PluginParameter<Boolean>(outputJustGT, value)
        return this
    }


    /**
     * Output GVCF typed files. If set to gvcf(default) it
     * will send all REFBlocks, SNPs and Indels.  If set to
     * vcf, it will only output SNPs, Indels and missing values(if
     * fillGaps = true)
     *
     * @return Output Type
     */
    fun outputType(): OUTPUT_TYPE {
        return outputType.value()
    }

    /**
     * Set Output Type. Output GVCF typed files. If set to
     * gvcf(default) it will send all REFBlocks, SNPs and
     * Indels.  If set to vcf, it will only output SNPs, Indels
     * and missing values(if fillGaps = true)
     *
     * @param value Output Type
     *
     * @return this plugin
     */
    fun outputType(value: OUTPUT_TYPE): MAFToGVCFPlugin {
        outputType = PluginParameter<OUTPUT_TYPE>(outputType, value)
        return this
    }

    /**
     * If set to true(default) will bgzip and index the output
     * GVCF file.
     *
     * @return Bgzip And Index
     */
    fun bgzipAndIndex(): Boolean {
        return bgzipAndIndex.value()
    }

    /**
     * Set Bgzip And Index. If set to true(default) will bgzip
     * and index the output GVCF file.
     *
     * @param value Bgzip And Index
     *
     * @return this plugin
     */
    fun bgzipAndIndex(value: Boolean): MAFToGVCFPlugin {
        bgzipAndIndex = PluginParameter<Boolean>(bgzipAndIndex, value)
        return this
    }
}

fun main() {
    GeneratePluginCode.generateKotlin(MAFToGVCFPlugin::class.java)
}