genes_bed_file=$1
genome_file=$2
slop_size=${3:-1000}


intergene_split_file=`mktemp`
intergene_split_noends_file=`mktemp`

#bt_bin="$(which bedtools)"

$bt_bin complement -nonamecheck -i $genes_bed_file -g $genome_file | \
    $awk_bin 'BEGIN{FS="\t";OFS="\t"}{end1=sprintf("%d",($3+$2)/2); start2=end1+1; print $1,end1,start2}' > $intergene_split_file

$bt_bin groupby -g 1 -c 2 -o first,last -i $intergene_split_file |\
    $awk_bin 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,($2+1)"\n"$1,$3,$3+1}' |\
    $bt_bin subtract -nonamecheck -A -a $intergene_split_file -b stdin > $intergene_split_noends_file

rm $intergene_split_file

$bt_bin slop -b $slop_size -i $genes_bed_file -g $genome_file | \
    $bt_bin merge -d 0 | \
    $bt_bin subtract -nonamecheck -a stdin -b $intergene_split_noends_file | \
    $bt_bin intersect -nonamecheck -wa -wb -a stdin -b $genes_bed_file | \
    $bt_bin groupby -g 1,2,3 -c 7 -o distinct | \
    sort -k1,1V -k2,2n -t$'\t'


rm $intergene_split_noends_file
    

