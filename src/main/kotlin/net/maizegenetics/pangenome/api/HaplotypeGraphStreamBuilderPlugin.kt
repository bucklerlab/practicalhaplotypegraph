package net.maizegenetics.pangenome.api

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.Datum
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.util.stream.Stream
import javax.swing.ImageIcon


/**
 * This plugin creates a stream of the specified haplotype graph.
 *
 * @author Terry Casstevens Created August 03, 2017
 */
class HaplotypeGraphStreamBuilderPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(HaplotypeGraphStreamBuilderPlugin::class.java)

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("Database configuration file")
            .guiName("Config File")
            .required(true)
            .inFile()
            .build()

    private var myMethods = PluginParameter.Builder("methods", null, String::class.java)
            .required(true)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" + "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
            .build()

    private var myIncludeSequences = PluginParameter.Builder("includeSequences", true, Boolean::class.javaObjectType)
            .description("Whether to include sequences in haplotype nodes.")
            .build()

    private var myIncludeVariantContexts = PluginParameter.Builder("includeVariantContexts", false, Boolean::class.javaObjectType)
            .description("Whether to include variant contexts in haplotype nodes.")
            .build()

    private var myNumRangesPerQuery = PluginParameter.Builder("numRangesPerQuery", 1000, Int::class.javaObjectType)
            .description("Number of reference ranges that will be retrieved from the database as a chunk.")
            .build()

    private var localGVCFFolder = PluginParameter.Builder("localGVCFFolder", null, String::class.java)
        .description("Folder where gvcfs will be downloaded and stored.  This is required if includeVaraintContexts is true.")
        .guiName("Local GVCF Folder")
        .inDir()
        .required(false)
        .build()

    override fun processData(input: DataSet?): DataSet {

        try {
            val graph = graph(configFile(), convertMethods(methods()), numRangesPerQuery(), includeSequences(), includeVariantContexts(),
                localGVCFFolder())
            return DataSet(Datum("PHG", graph, "methods: " + methods()), this)
        } catch (e: Exception) {
            myLogger.debug(e.message, e)
            throw IllegalStateException("processData: Problem creating graph stream: " + e.message)
        }

    }

    fun build(): Stream<Pair<ReferenceRange, Set<HaplotypeNode>>> {
        return performFunction(null).getData(0).data as Stream<Pair<ReferenceRange, Set<HaplotypeNode>>>
    }

    /**
     * Database configuration file
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Database configuration file
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): HaplotypeGraphStreamBuilderPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Pairs of methods (haplotype method name and range group
     * method name). Method pair separated by a comma, and
     * pairs separated by semicolon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @return Methods </haplotype></range></haplotype></range></haplotype>
     */
    fun methods(): String {
        return myMethods.value()
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name
     * and range group method name). Method pair separated
     * by a comma, and pairs separated by semicolon. The range
     * group is optional
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): HaplotypeGraphStreamBuilderPlugin {
        myMethods = PluginParameter<String>(myMethods, value)
        return this
    }

    /**
     * Whether to include sequences in haplotype nodes.
     *
     * @return Include Sequences
     */
    fun includeSequences(): Boolean {
        return myIncludeSequences.value()
    }

    /**
     * Set Include Sequences. Whether to include sequences
     * in haplotype nodes.
     *
     * @param value Include Sequences
     *
     * @return this plugin
     */
    fun includeSequences(value: Boolean): HaplotypeGraphStreamBuilderPlugin {
        myIncludeSequences = PluginParameter<Boolean>(myIncludeSequences, value)
        return this
    }

    /**
     * Whether to include variant contexts in haplotype nodes.
     *
     * @return Include Variant Contexts
     */
    fun includeVariantContexts(): Boolean {
        return myIncludeVariantContexts.value()
    }

    /**
     * Set Include Variant Contexts. Whether to include variant
     * contexts in haplotype nodes.
     *
     * @param value Include Variant Contexts
     *
     * @return this plugin
     */
    fun includeVariantContexts(value: Boolean): HaplotypeGraphStreamBuilderPlugin {
        myIncludeVariantContexts = PluginParameter<Boolean>(myIncludeVariantContexts, value)
        return this
    }

    /**
     * Number of reference ranges that will be retrieved from
     * the database as a chunk.
     *
     * @return Num Ranges Per Query
     */
    fun numRangesPerQuery(): Int {
        return myNumRangesPerQuery.value()
    }

    /**
     * Set Num Ranges Per Query. Number of reference ranges
     * that will be retrieved from the database as a chunk.
     *
     * @param value Num Ranges Per Query
     *
     * @return this plugin
     */
    fun numRangesPerQuery(value: Int): HaplotypeGraphStreamBuilderPlugin {
        myNumRangesPerQuery = PluginParameter<Int>(myNumRangesPerQuery, value)
        return this
    }

    /**
     * Local folder to which gvcf file will be downloaded.
     *
     * @return Local GVCF folder path
     */
    fun localGVCFFolder(): String {
        return localGVCFFolder.value()
    }

    /**
     * Set local gvcf folder path
     *
     * @param value Local GVCF Folder path
     *
     * @return this plugin
     */
    fun localGVCFFolder(value: String): HaplotypeGraphStreamBuilderPlugin {
        localGVCFFolder = PluginParameter(localGVCFFolder, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Haplotype Graph Stream Builder"
    }

    override fun getToolTipText(): String {
        return "Haplotype Graph Stream Builder"
    }

    companion object {

        fun convertMethods(methodPairs: String): List<Pair<String, String>> {

            val pairs = methodPairs.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val result = ArrayList<Pair<String, String>>()
            for (current in pairs) {
                val methods = current.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (methods.size == 1) {
                    result.add(Pair(methods[0].trim { it <= ' ' }, ""))
                } else if (methods.size == 2) {
                    if (methods[1].trim { it <= ' ' } == "*") {
                        result.add(Pair(methods[0].trim { it <= ' ' }, ""))
                    } else {
                        result.add(Pair(methods[0].trim { it <= ' ' }, methods[1].trim { it <= ' ' }))
                    }
                }
            }
            return result

        }

    }
}
