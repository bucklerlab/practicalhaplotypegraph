/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import com.google.common.collect.ImmutableSet;
import com.google.common.io.CharStreams;
import net.maizegenetics.util.LoggingUtils;
import org.junit.Before;
import org.junit.Test;

import net.maizegenetics.plugindef.DataSet;
import org.sqlite.SQLiteConfig;

import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * @author lcj34
 *
 */


public class DBLoadingMethodsTests {

    @Before
    public void setup() {
        LoggingUtils.setupDebugLogging();
    }


    @Test
    public void testPutPathsData() throws Exception {

        String taxon = "lynnTaxon";
        String methodName = "lynnPathMethod";
        String methodDescription = "lynn method description";
        List<Integer> readMappingIds = Arrays.asList(28, 49, 52, 34, 65, 12);

        // This doesn't reflect the actual data that will be stored, but is fine for testing.
        // I only need something that is a byte array.  This is easy
        byte[] pathBytes = "12,13,14,15,16".getBytes();
        // Create a db
        String userHome = System.getProperty("user.home");
        String dbName = userHome + "/testPutPathsDB.db";

        // Delete db if it exists - starting fresh
        if (Files.exists(Paths.get(dbName))) {
            Files.delete(Paths.get(dbName));
        }

        String url = "jdbc:sqlite:" + dbName;
        try {
            // getting connection creates the db
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();

            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dbName, config.toProperties());
            connection.setAutoCommit(true);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            // set the schema
            String schema = CharStreams.toString(new InputStreamReader(PHGdbAccess.class.getResourceAsStream("PHGSchema.sql")));
            statement.executeUpdate(schema);

            // input data
            PHGDataWriter phg = new PHGdbAccess(connection);
            Map<String,String> plugParams = new HashMap<>();
            plugParams.put("notes",methodDescription);
            //Should add another test where isTestMethod parameter is "true"
            phg.putPathsData(methodName, plugParams, taxon, readMappingIds, pathBytes,false);

            // Now verify the data has entries
            String query = "select * from read_mapping_paths;";
            List<String> readMappingData = new ArrayList<String>();
            ResultSet rs = connection.createStatement().executeQuery(query);
            int path_id;
            int read_mapping_id;
            readMappingData.add("PATHID\tREAD_MAPPING_ID");
            while (rs.next()) {
                path_id = rs.getInt("path_id");
                read_mapping_id = rs.getInt("read_mapping_id");
                StringBuilder sb = new StringBuilder();
                sb.append(path_id).append("\t").append(read_mapping_id);
                readMappingData.add(sb.toString());
            }

            assertEquals(7, readMappingData.size());
            System.out.println("\nread_mapping_paths stored in db:");
            for (String entry : readMappingData) {
                System.out.println(entry);
            }

        } catch (Exception exc) {
            throw new IllegalStateException("failed to process data for testPutPathsData: " + exc.getMessage());
        }
    }
    
}

class LoadingConstants {
    public static final String anchorBedFile = "data/hackathon_trimmed_intervals.bed";
    public static final String refLoadDataFile = "data/sample_load_data.txt";
}
