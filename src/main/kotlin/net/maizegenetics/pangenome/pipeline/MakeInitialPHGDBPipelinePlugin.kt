package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.db_loading.GetDBConnectionPlugin
import net.maizegenetics.pangenome.db_loading.LoadAllIntervalsToPHGdbPlugin
import net.maizegenetics.pangenome.liquibase.LiquibaseUpdatePlugin
import net.maizegenetics.pangenome.liquibase.defineDefaultLiquibaseDir
import net.maizegenetics.plugindef.*
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.lang.IllegalStateException
import java.nio.file.Files
import java.nio.file.Paths
import javax.swing.ImageIcon

/**
 * This class runs the loadALlIntervalsToPHGdbPlugin to create and populate with reference ranges
 * and initial PHG databse.
 */

class MakeInitialPHGDBPipelinePlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(MakeInitialPHGDBPipelinePlugin::class.java)

    private var liquibaseDir = PluginParameter.Builder("liquibaseDir", null, String::class.java)
        .description("Liquibase release directory.")
        .required(false)
        .build()

    private var localGVCFFolder = PluginParameter.Builder("localGVCFFolder", null, String::class.java)
        .description("This is where the reference GVCF file will be written.")
        .guiName("Local GVCF Folder")
        .inDir()
        .required(false)
        .build()

    override fun postProcessParameters() {
        // if no liquibase directory is specified, set the value from
        // defineDefaultLiquibaseDir()
        if (liquibaseDir.value() == null) {
            liquibaseDir(defineDefaultLiquibaseDir()) // function defined in LiquibaseUpdatePlugin
        }
    }

    /**
     * Function to run the steps previously done by LoadGenomeIntervals.sh
     */
    override fun processData(input: DataSet?): DataSet? {
        //Steps in LoadGenomeIntervals.sh
        myLogger.info("Beginning loading Genome Intervals step.")
        loadGenomeIntervals()
        myLogger.info("Done loading Genome Intervals step.")

        myLogger.info("Checking if Liquibase can be run.")
        if (liquibaseDir.value() != null && Files.exists(Paths.get("${liquibaseDir()}/changelogs"))) {
            myLogger.info("Liquibase can be run.  Setting it up using changelogsync.")
            setupLiquibase()
            myLogger.info("Done setting up Liquibase.")
        } else {
            myLogger.warn("Warning:  liquibase directory not found: db has not had changelogsync run.\n Please provide a valid liquibase directory string, run liquibase manually or run liquibase from the PHG docker. ")
        }

        myLogger.info("MakeInitialPHGDBPipelinePlugin complete!")
        return null
    }


    /**
     * Function to loadGenomeIntervals.  Check the dbConfigFile in the parameter cache.
     */
    private fun loadGenomeIntervals() {
        val dbConnectionDataSet = GetDBConnectionPlugin(null, false)
                .configFile(ParameterCache.value("configFile").get()) //Should not need this. leaving for now until SmallSeq is fixed to make sure it works.
                .createNew(true)
                .performFunction(null)

        //Run the plugin - LoadGenomeIntervalsToPHGdbPlugin is old, use LoadAllIntervalsToPHGdbPlugin
        val loadGenomeIntervalsPlugin = LoadAllIntervalsToPHGdbPlugin(null, false)
        loadGenomeIntervalsPlugin.setConfigParameters()
        loadGenomeIntervalsPlugin
                .localGVCFFolder(localGVCFFolder())
                .refGenome(ParameterCache.value("referenceFasta").get())
                .performFunction(dbConnectionDataSet)

    }

    /**
     * Function to run the initial run of Liquibase running the 'changeLogSync' command.
     */
    private fun setupLiquibase() {
        val liquibasePlugin = LiquibaseUpdatePlugin(null, false)
        liquibasePlugin.setConfigParameters()
        liquibasePlugin
            .command("changeLogSync")
            .liquibaseDir(liquibaseDir())
            .performFunction(null)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MakeInitialPHGDBPipelinePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MakeInitialPHGDBPipelinePlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to create the DB."
    }

    /**
     * Liquibase release directory.
     *
     * @return Liquibase Directory
     */
    fun liquibaseDir(): String {
        return liquibaseDir.value()
    }

    /**
     * Set Liquibase Directory.
     *
     * @param value Liquibase Directory
     *
     * @return this plugin
     */
    fun liquibaseDir(value: String?): MakeInitialPHGDBPipelinePlugin {
        liquibaseDir = PluginParameter(liquibaseDir, value)
        return this
    }

    /**
     * This is where the reference GVCF file will be written.
     *
     * @return Local GVCF Folder
     */
    fun localGVCFFolder(): String? {
        return localGVCFFolder.value()
    }

    /**
     * This is where the reference GVCF file will be written.
     *
     * @param value Local GVCF Folder
     *
     * @return this plugin
     */
    fun localGVCFFolder(value: String?): MakeInitialPHGDBPipelinePlugin {
        localGVCFFolder = PluginParameter<String>(localGVCFFolder, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(MakeInitialPHGDBPipelinePlugin::class.java)
}