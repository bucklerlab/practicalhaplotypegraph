package net.maizegenetics.pangenome.hapcollapse;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.maizegenetics.analysis.distance.DistanceMatrixPlugin;
import net.maizegenetics.analysis.distance.IBSDistanceMatrix;
import net.maizegenetics.analysis.filter.FilterSiteBuilderPlugin;
import net.maizegenetics.dna.snp.*;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.max;
import static net.maizegenetics.dna.snp.GenotypeTableUtils.getDiploidValue;
import static net.maizegenetics.dna.snp.GenotypeTableUtils.getDiploidValues;

/**
 * Created by edbuckler on 6/19/17.
 */
public class CreateHaplotypesFromFasta {
    //    public static final String localDirectory="/Users/edbuckler/temp/exportedFastaFromV2DB_anchorLongNsRemoved_MAFFTAligned_Trimmed/";
//    public static final String localDirectoryOut="/Users/edbuckler/temp/filtered/";
    public static final String localDirectory = "/Users/edbuckler/temp/chr10fastafilesFilt/";
    public static final String localConsensusDirectoryOut = "/Users/edbuckler/temp/consensus/";
    public static final String loggingFile = "/Users/edbuckler/temp/logging.txt";
    public static final String anchorSummaryFile = "/Users/edbuckler/temp/consensusSummaryMAF02_170621_all.txt";

    private static final double maxDistance = 0.002; //maximum distance between taxa to merge
    private static final int minSites = 100; //minimum number of sites to find a match

    private static final String[] assemblies={"W22Assembly", "B104Assembly", "CML247Assembly","PH207Assembly","EP1Assembly"};


    public static void main(String[] args) {

        try {
            LoggingUtils.setupDebugLogging();
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(anchorSummaryFile));
            writer.write("AnchorID\tOriginalSites\tSegSites\tConsensusSeqSite\tFilteredTaxa\tConsesusHaplotypes\tNumTaxaInTop10\t");
            writer.write(DistanceReport.headers());
            writer.newLine();
            List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.fa.gz", Paths.get(localDirectory));
            Pattern pattern = Pattern.compile("Id(\\d*)");
            fastaPaths.stream().limit(10000).forEach(fastaPath -> {
                try {
                    System.out.println(fastaPath.toString());
                    final Matcher matcher = pattern.matcher(fastaPath.toString());
                    matcher.find();
                    String anchorIdString = matcher.group(1);
                    //System.out.println(anchorIdString);
                    final GenotypeTable origGenotypeTable = ImportUtils.readFasta(fastaPath.toString());
                    int[] invariantAndHighFreqPL = IntStream.range(0, origGenotypeTable.numberOfSites())
                            .filter(site -> {
                                double maf = origGenotypeTable.minorAlleleFrequency(site);
                                return (maf < 0.0001) || (maf > 0.02);
                            }).toArray();
                    GenotypeTable gtSegSite = FilterGenotypeTable.getInstance(origGenotypeTable, invariantAndHighFreqPL);
                    if (gtSegSite == null) return;
                    double maxDistanceSeg = maxDistance * origGenotypeTable.numberOfSites() / gtSegSite.numberOfSites();
                    System.out.println("OrigSites=" + origGenotypeTable.numberOfSites() + "\tMAF filter segsites=" + gtSegSite.numberOfSites() + "\tnew maxDistance=" + maxDistanceSeg);
                    Multimap<Taxon, Taxon> hapClusters = FindHaplotypeClustersPlugin.findHapClusterHighCoverage(gtSegSite, minSites, maxDistance);
                    GenotypeTable consensusGT = createConsensusGenotypeTable(origGenotypeTable, hapClusters);
                    hapClusters.asMap().forEach((core, members) -> {
                        System.out.print(core.getName() + "\t");
                        System.out.println(members.size() + "\t"/*+members.toString()*/);
                        //System.out.println(consensusGT.genotypeAsStringRow(consensusGT.taxa().indexOf(core)));
                    });
                    //writeFasta(localConsensusDirectoryOut+"anchorId"+anchorIdString+".fa.gz",consensusGT,Optional.of(hapClusters));
                    int top10haps = haplotypesCaptured(hapClusters, 10);
                    System.out.println(top10haps);
                    String s = String.format("%s\t%d\t%d\t%d\t%d\t%d\t%d\t", anchorIdString, origGenotypeTable.numberOfSites(), getSegSites(origGenotypeTable, 0, 0, 0.0001),
                            getSegSites(consensusGT, 0, 0, 0.0001), origGenotypeTable.numberOfTaxa(), consensusGT.numberOfTaxa(), top10haps);
                    writer.write(s);
                    DistanceReport dr=diversityReport(origGenotypeTable);
                    writer.write(dr.formatString());
                    writer.newLine();

                    //System.out.println(hapClusters.toString());
                    //GenotypeTable collapsedHaplotypes = createHaplotypeAlignment(gt, 100, 0.002);
                    //writer.write(resultLine.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static DistanceReport diversityReport(GenotypeTable genotypeTable) {
        DistanceMatrix distanceMatrix = DistanceMatrixPlugin.getDistanceMatrix(genotypeTable);
        DistanceReport dr=new DistanceReport();

        final int b73RefIndex=genotypeTable.taxa().indexOf("B73Ref");
        int b73Hap=genotypeTable.taxa().indexOf("B73_Haplotype_Caller");
        int cml247AssemIndex=genotypeTable.taxa().indexOf("CML247Assembly");
        int cml247Hap=genotypeTable.taxa().indexOf("CML247_Haplotype_Caller");
        int w22AssemIndex=genotypeTable.taxa().indexOf("W22Assembly");
        int w22Hap=genotypeTable.taxa().indexOf("W22_Haplotype_Caller");

        if(b73Hap>-1) dr.b73dist=distanceMatrix.getDistance(b73RefIndex, b73Hap);
        if (cml247AssemIndex>-1 && cml247Hap>-1) dr.cml247dist=distanceMatrix.getDistance(cml247AssemIndex, cml247Hap);
        if(w22AssemIndex>-1 && w22Hap>-1) dr.w22dist=distanceMatrix.getDistance(w22AssemIndex, w22Hap);

        dr.closeB73Haplotypes=(int)IntStream.range(0,genotypeTable.numberOfTaxa())
                .mapToDouble(taxaIndex -> distanceMatrix.getDistance(b73RefIndex, taxaIndex))
                .filter(Double::isFinite)
                .filter(distance -> distance<0.002)
                .count();
        dr.meanDivergence = distanceMatrix.meanDistance();
        return dr;
    }

    private static class DistanceReport{
        double b73dist, cml247dist, w22dist; //distance between assembly and Haplotype caller
        int closeB73Haplotypes;
        double meanDivergence;

        public DistanceReport() {
            b73dist=Double.NaN;
            cml247dist=Double.NaN;
            w22dist=Double.NaN;
        }

        public String formatString() {
            return String.format("%.4f\t%.4f\t%.4f\t%d\t%.4f\t",b73dist, cml247dist, w22dist, closeB73Haplotypes, meanDivergence);
        }

        public static String headers() {
            return "b73dist\tcml247dist\tw22dist\tcloseB73Haplotypes\tmeanDivergence\t";
        }
    }

    private static int haplotypesCaptured(Multimap<Taxon, Taxon> hapClusters, int maxClusters) {
        int[] haplotypesInClusters = hapClusters.asMap().entrySet().stream()
                .mapToInt(entry -> entry.getValue().size())
                .sorted()
                .toArray();
        return IntStream.range(max(0, haplotypesInClusters.length - maxClusters), haplotypesInClusters.length).map(i -> haplotypesInClusters[i]).sum();

    }

    private static GenotypeTable createConsensusGenotypeTable(GenotypeTable gt, Multimap<Taxon, Taxon> hapClusters) {
        GenotypeTableBuilder gB = GenotypeTableBuilder.getTaxaIncremental(gt.positions());
        for (Map.Entry<Taxon, Collection<Taxon>> taxonTaxonSet : hapClusters.asMap().entrySet()) {
            byte[] sequence = consensusGameteCalls(gt, taxonTaxonSet.getValue());
            gB.addTaxon(taxonTaxonSet.getKey(), sequence);
        }
        return gB.build();

    }

    private static int getSegSites(GenotypeTable gt, int minSiteCount, int minorAlleleCount, double minMAF) {
        DataSet filter = new FilterSiteBuilderPlugin(null, false)
                .siteMinCount(minSiteCount)
                .siteMinAlleleFreq(minMAF)
                .performFunction(DataSet.getDataSet(gt));
        if (filter.getSize() == 2) return ((GenotypeTable) filter.getData(0).getData()).numberOfSites();
        return 0;
    }

    private static GenotypeTable getSegSitesGT(GenotypeTable gt, int minSiteCount, int minorAlleleCount, double minMAF, double maxMAF) {
        DataSet filter = new FilterSiteBuilderPlugin(null, false)
                .siteMinCount(minSiteCount)
                .siteMinAlleleFreq(minMAF)
                .siteMinCount(minorAlleleCount)
                .siteMaxAlleleFreq(maxMAF)
                .performFunction(DataSet.getDataSet(gt));
        if (filter.getSize() == 2) return ((GenotypeTable) filter.getData(0).getData());
        return null;
    }

    private static int numberOfGaps(GenotypeTable gt, int taxonId) {
        return (int) IntStream.range(0, gt.numberOfSites())
                .filter(index -> gt.genotype(taxonId, index) == NucleotideAlignmentConstants.GAP_DIPLOID_ALLELE)
                .count();
    }

    private static String sequenceToString(GenotypeTable gt, int taxonId) {
        StringBuilder sb = new StringBuilder();
        IntStream.range(0, gt.numberOfSites())
                .forEach(index -> sb.append(gt.genotypeAsString(taxonId, index)));
        return sb.toString();
    }

    private static void writeFasta(String filePathString, GenotypeTable genotypeTable, Optional<Multimap<Taxon, Taxon>> hapClusters) {
        try (BufferedWriter writer = Utils.getBufferedWriter(filePathString)) {
            for (int taxaIndex = 0; taxaIndex < genotypeTable.numberOfTaxa(); taxaIndex++) {
                writer.write(">" + genotypeTable.taxa().get(taxaIndex).getName());
                if (hapClusters.isPresent()) {
                    String out = hapClusters.get().get(genotypeTable.taxa().get(taxaIndex)).stream()
                            .map(Taxon::getName)
                            .collect(Collectors.joining(","));
                    writer.write("=" + out);
                }
                writer.newLine();
                writer.write(sequenceToString(genotypeTable, taxaIndex));
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static Multimap<Taxon, Taxon> findHapClusterByUPGMA(GenotypeTable baseAlign, int minSites, double maxDistance) {
        return null;
    }

    private static Multimap<Taxon, Taxon> findHapClusterByCentrality(GenotypeTable baseAlign, int minSites, double maxDistance) {
        return null;
    }

    private static Multimap<Taxon, Taxon> findHapClusterByTerry(GenotypeTable baseAlign, int minSites, double maxDistance) {
        return null;
    }

    private static int[] countUnknown(byte[] b) {
        int cnt = 0, cntHet = 0;
        for (int i = 0; i < b.length; i++) {
            if (b[i] == GenotypeTable.UNKNOWN_DIPLOID_ALLELE) {
                cnt++;
            } else if (GenotypeTableUtils.isHeterozygous(b[i])) {
                cntHet++;
            }
        }
        int[] result = {cnt, cntHet};
        return result;
    }

    private static byte[] consensusGameteCalls(GenotypeTable a, Collection<Taxon> taxa) {
        int[] taxaIndex = taxa.stream().mapToInt(taxon -> a.taxa().indexOf(taxon)).toArray();
        byte[] calls = new byte[a.numberOfSites()];
        Arrays.fill(calls, GenotypeTable.UNKNOWN_DIPLOID_ALLELE);
        for (int s = 0; s < a.numberOfSites(); s++) {
            int[] alleleCounts = new int[20];  //TODO splits byte to half bytes
            for (int t : taxaIndex) {
                byte[] alleles = getDiploidValues(a.genotype(t, s));
                alleleCounts[alleles[0]]++;
                alleleCounts[alleles[1]]++;
            }
            byte maxAt = 0;  //TODO increase logic on ties etc.
            for (byte i = 0; i < alleleCounts.length; i++) {
                maxAt = alleleCounts[i] > alleleCounts[maxAt] ? i : maxAt;
            }
            if (alleleCounts[maxAt] > 0) calls[s] = getDiploidValue(maxAt, maxAt);
        }
        return calls;
    }
}
