package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.HashMultimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.*
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import org.jetbrains.kotlinx.dataframe.DataFrame
import org.jetbrains.kotlinx.dataframe.api.toDataFrame
import java.awt.Frame
import java.sql.Connection
import javax.swing.ImageIcon

/**
 * This class takes a config file and list of  methods.  For each reference range ID,
 * it grabs from the haplotypes table the haplotypes_id, ref_range_id, gamete_grp_id, seq_len
 * and method_id for all entries with a method in the provided list.
 *
 * It writes an output file with these comma separated column headers:
 * refRangeID      group_type chr     start   end     method  hapid   taxon   len
 *
 * NOTE: The file can get very large if multiple methods are used for a db with
 * millions of entries in the haplotype table.  When running for multiple consensus
 * methods, it is better to run this plugin once for each consensus method.
 */
class AssemblyConsensusMetricPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = LogManager.getLogger(AssemblyConsensusMetricPlugin::class.java)
    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
        .description("File with information for database access, required if a graph is not passed to the plugin")
        .required(false)
        .inFile()
        .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("File for writing matrix of haplotype coverage")
            .required(true)
            .outFile()
            .build()

    private var methods = PluginParameter.Builder("methods", null, String::class.java)
        .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" +
                "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
        .required(false)
        .build()

    data class ConsensusDataRow(val refRangeID:Int, val groupType:String, val chr:String, val start:Int, val end:Int, val refRangeLen:Int, val hapid:Int, val taxa:String, val len:Int)
    override fun processData(input: DataSet?): DataSet? {

        // If a graph was sent in, use that.  We won't then need a db connection
        var graph: HaplotypeGraph? = null
        if (input != null) {
            val graphData = input.getDataOfType(HaplotypeGraph::class.java)
            if (graphData.size >  0) {
                myLogger.info("Graph supplied from input DataSet")
                graph = graphData[0].data as HaplotypeGraph
            }
        }
        if (graph == null && ( configFile() == null || methods() == null)) {
            throw IllegalArgumentException("Must supply either a config file with db connection information AND a list of methods, or a haplotype graph as part of the plugin's Dataset Input")
        }

        if (graph == null) {
            myLogger.info("Getting graph from config file db  ...")

            graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile())
                .includeVariantContexts(false)
                .includeSequences(false)
                .methods(methods())
                .build()
        }


        // This needed as we need to grab reference ranges based on refRangeID
        val referenceRanges = mutableMapOf<Int,ReferenceRange>()
        // The Triple here is hapid, comma-separated list of taxa, sequence length
        val refRangeToGameteTriple = HashMultimap.create<Int, Triple<Int,String, Int>>()

        // Make list of reference ranges for each assembly - do this by traversing the nodes
        // in the graph

        // Works - taxaName must be Set, not List - no duplicates!
        // This is for non-consensus haplotypes, so only pick 1 taxon per gamete_grp_id
        graph!!.nodeStream().forEach{ node ->
            val refRangeId = node.referenceRange().id()
            val taxonSB = StringBuilder()
            node.taxaList().forEach {
                taxonSB.append(it.name).append(":")
            }
            val sbLen = taxonSB.length
            taxonSB.setLength(sbLen-1) // strip the last ","
            val seqLen = node.haplotypeSequence().length()
            refRangeToGameteTriple.put(refRangeId,Triple(node.id(),taxonSB.toString(),seqLen))
            referenceRanges.put(refRangeId,node.referenceRange())
        }

        val df = writeOutFileAndDF(referenceRanges,refRangeToGameteTriple,outputFile())

        return DataSet(Datum("DataFrame", df, "kotlin DataFrame from csv file ${outputFile()}" ), this)
    }

    fun writeOutFileAndDF(referenceRanges: Map<Int,ReferenceRange>,
                          refRangeToGameteTriple: HashMultimap<Int, Triple<Int,String, Int>>,
                          outputFile:String): DataFrame<ConsensusDataRow> {
        val headerLine = "refRangeID\tgroupType\tchr\tstart\tend\thapid\ttaxa\tlen\n"

        var consensusDF: DataFrame<ConsensusDataRow> = mutableListOf<ConsensusDataRow>().toDataFrame()
        var bw = Utils.getBufferedWriter(outputFile)

        try {
            bw.write(headerLine)
            val rangeKeySet = referenceRanges.keys.sorted()
            val consensusList = mutableListOf<String>()
            for (key in rangeKeySet) {
                val refRangeData = referenceRanges[key]
                val gameteData = refRangeToGameteTriple[key]
                val chr = refRangeData!!.chromosome().name
                val start = refRangeData!!.start()
                val end = refRangeData!!.end()
                val rangeLen = end - start + 1;
                val groupType = refRangeData!!.groupMethods().joinToString(",")
                for (entry in gameteData) {
                    // These are consensus, so should have multiple entries for each range
                    val line = "${key},${groupType},${chr},${start},${end},${rangeLen}, ${entry.first},${entry.second}, ${entry.third}\n"
                    consensusList.add(line) // for the dataFrame later
                    bw.write(line)
                }
            }
            consensusDF = consensusList.map{ entry ->
                val fields = entry.split(",")
                ConsensusDataRow(fields[0].trim().toInt(),fields[1],fields[2],fields[3].trim().toInt(),fields[4].trim().toInt(),fields[5].trim().toInt(),fields[6].trim().toInt(),fields[7], fields[8].trim().toInt())
            }.toDataFrame()

            bw.close()
        } catch (exc: Exception) {
            bw.close()
            throw IllegalStateException("Error processing data for or writing output file ${outputFile()}", exc)
        }
        //val dfFromCSV = DataFrame.readCSV(outputFile())
        return consensusDF
    }

    override fun getToolTipText(): String {
        return ("Provides metrics on haplotype size per reference range for haplotypes created with specified methods.")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyConsensusMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return ("AssemblyConsensusMetricPlugin")
    }

    /**
     * FIle with infor for database access
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. FIle with infor for database access
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): AssemblyConsensusMetricPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }


    /**
     * File for writing matrix of gene coverage
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. File for writing matrix of gene coverage
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): AssemblyConsensusMetricPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

    /**
     * Commas separated list of mathod names
     *
     * @return Methods
     */
    fun methods(): String {
        return methods.value()
    }

    /**
     * Set Methods. Commas separated list of mathod names
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): AssemblyConsensusMetricPlugin {
        methods = PluginParameter<String>(methods, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyConsensusMetricPlugin::class.java)
}