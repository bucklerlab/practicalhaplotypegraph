// Generated by the protocol buffer compiler. DO NOT EDIT!
// source: main/kotlin/net/maizegenetics/pangenome/io/haplotypegraph.proto

// Generated files should ignore deprecation warnings
@file:Suppress("DEPRECATION")
package net.maizegenetics.pangenome.io;

@kotlin.jvm.JvmName("-initializehaplotypeNodeProto")
public inline fun haplotypeNodeProto(block: net.maizegenetics.pangenome.io.HaplotypeNodeProtoKt.Dsl.() -> kotlin.Unit): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto =
  net.maizegenetics.pangenome.io.HaplotypeNodeProtoKt.Dsl._create(net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto.newBuilder()).apply { block() }._build()
/**
 * Protobuf type `net.maizegenetics.pangenome.io.HaplotypeNodeProto`
 */
public object HaplotypeNodeProtoKt {
  @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
  @com.google.protobuf.kotlin.ProtoDslMarker
  public class Dsl private constructor(
    private val _builder: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto.Builder
  ) {
    public companion object {
      @kotlin.jvm.JvmSynthetic
      @kotlin.PublishedApi
      internal fun _create(builder: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto.Builder): Dsl = Dsl(builder)
    }

    @kotlin.jvm.JvmSynthetic
    @kotlin.PublishedApi
    internal fun _build(): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto = _builder.build()

    /**
     * `int32 hapId = 1;`
     */
    public var hapId: kotlin.Int
      @JvmName("getHapId")
      get() = _builder.getHapId()
      @JvmName("setHapId")
      set(value) {
        _builder.setHapId(value)
      }
    /**
     * `int32 hapId = 1;`
     */
    public fun clearHapId() {
      _builder.clearHapId()
    }

    /**
     * `.net.maizegenetics.pangenome.io.HaplotypeSequenceProto haplotypeSequence = 2;`
     */
    public var haplotypeSequence: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto
      @JvmName("getHaplotypeSequence")
      get() = _builder.getHaplotypeSequence()
      @JvmName("setHaplotypeSequence")
      set(value) {
        _builder.setHaplotypeSequence(value)
      }
    /**
     * `.net.maizegenetics.pangenome.io.HaplotypeSequenceProto haplotypeSequence = 2;`
     */
    public fun clearHaplotypeSequence() {
      _builder.clearHaplotypeSequence()
    }
    /**
     * `.net.maizegenetics.pangenome.io.HaplotypeSequenceProto haplotypeSequence = 2;`
     * @return Whether the haplotypeSequence field is set.
     */
    public fun hasHaplotypeSequence(): kotlin.Boolean {
      return _builder.hasHaplotypeSequence()
    }

    /**
     * An uninstantiable, behaviorless type to represent the field in
     * generics.
     */
    @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
    public class TaxaProxy private constructor() : com.google.protobuf.kotlin.DslProxy()
    /**
     * `repeated string taxa = 3;`
     * @return A list containing the taxa.
     */
    public val taxa: com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>
      @kotlin.jvm.JvmSynthetic
      get() = com.google.protobuf.kotlin.DslList(
        _builder.getTaxaList()
      )
    /**
     * `repeated string taxa = 3;`
     * @param value The taxa to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("addTaxa")
    public fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.add(value: kotlin.String) {
      _builder.addTaxa(value)
    }
    /**
     * `repeated string taxa = 3;`
     * @param value The taxa to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("plusAssignTaxa")
    @Suppress("NOTHING_TO_INLINE")
    public inline operator fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.plusAssign(value: kotlin.String) {
      add(value)
    }
    /**
     * `repeated string taxa = 3;`
     * @param values The taxa to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("addAllTaxa")
    public fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.addAll(values: kotlin.collections.Iterable<kotlin.String>) {
      _builder.addAllTaxa(values)
    }
    /**
     * `repeated string taxa = 3;`
     * @param values The taxa to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("plusAssignAllTaxa")
    @Suppress("NOTHING_TO_INLINE")
    public inline operator fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.plusAssign(values: kotlin.collections.Iterable<kotlin.String>) {
      addAll(values)
    }
    /**
     * `repeated string taxa = 3;`
     * @param index The index to set the value at.
     * @param value The taxa to set.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("setTaxa")
    public operator fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.set(index: kotlin.Int, value: kotlin.String) {
      _builder.setTaxa(index, value)
    }/**
     * `repeated string taxa = 3;`
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("clearTaxa")
    public fun com.google.protobuf.kotlin.DslList<kotlin.String, TaxaProxy>.clear() {
      _builder.clearTaxa()
    }
    /**
     * `int32 genomeFileId = 4;`
     */
    public var genomeFileId: kotlin.Int
      @JvmName("getGenomeFileId")
      get() = _builder.getGenomeFileId()
      @JvmName("setGenomeFileId")
      set(value) {
        _builder.setGenomeFileId(value)
      }
    /**
     * `int32 genomeFileId = 4;`
     */
    public fun clearGenomeFileId() {
      _builder.clearGenomeFileId()
    }

    /**
     * `int32 gvcfFileId = 5;`
     */
    public var gvcfFileId: kotlin.Int
      @JvmName("getGvcfFileId")
      get() = _builder.getGvcfFileId()
      @JvmName("setGvcfFileId")
      set(value) {
        _builder.setGvcfFileId(value)
      }
    /**
     * `int32 gvcfFileId = 5;`
     */
    public fun clearGvcfFileId() {
      _builder.clearGvcfFileId()
    }

    /**
     * `string asmContig = 6;`
     */
    public var asmContig: kotlin.String
      @JvmName("getAsmContig")
      get() = _builder.getAsmContig()
      @JvmName("setAsmContig")
      set(value) {
        _builder.setAsmContig(value)
      }
    /**
     * `string asmContig = 6;`
     */
    public fun clearAsmContig() {
      _builder.clearAsmContig()
    }

    /**
     * `int32 asmStartCoordinate = 7;`
     */
    public var asmStartCoordinate: kotlin.Int
      @JvmName("getAsmStartCoordinate")
      get() = _builder.getAsmStartCoordinate()
      @JvmName("setAsmStartCoordinate")
      set(value) {
        _builder.setAsmStartCoordinate(value)
      }
    /**
     * `int32 asmStartCoordinate = 7;`
     */
    public fun clearAsmStartCoordinate() {
      _builder.clearAsmStartCoordinate()
    }

    /**
     * `int32 asmEndCoordinate = 8;`
     */
    public var asmEndCoordinate: kotlin.Int
      @JvmName("getAsmEndCoordinate")
      get() = _builder.getAsmEndCoordinate()
      @JvmName("setAsmEndCoordinate")
      set(value) {
        _builder.setAsmEndCoordinate(value)
      }
    /**
     * `int32 asmEndCoordinate = 8;`
     */
    public fun clearAsmEndCoordinate() {
      _builder.clearAsmEndCoordinate()
    }

    /**
     * `string asmStrand = 9;`
     */
    public var asmStrand: kotlin.String
      @JvmName("getAsmStrand")
      get() = _builder.getAsmStrand()
      @JvmName("setAsmStrand")
      set(value) {
        _builder.setAsmStrand(value)
      }
    /**
     * `string asmStrand = 9;`
     */
    public fun clearAsmStrand() {
      _builder.clearAsmStrand()
    }
  }
}
@kotlin.jvm.JvmSynthetic
@com.google.errorprone.annotations.CheckReturnValue
public inline fun net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto.copy(block: net.maizegenetics.pangenome.io.HaplotypeNodeProtoKt.Dsl.() -> kotlin.Unit): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProto =
  net.maizegenetics.pangenome.io.HaplotypeNodeProtoKt.Dsl._create(this.toBuilder()).apply { block() }._build()

public val net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeNodeProtoOrBuilder.haplotypeSequenceOrNull: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto?
  get() = if (hasHaplotypeSequence()) getHaplotypeSequence() else null

