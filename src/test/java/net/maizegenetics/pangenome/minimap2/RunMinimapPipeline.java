package net.maizegenetics.pangenome.minimap2;

import static net.maizegenetics.pangenome.minimap2.Minimap2Paths.minimap2Path;
import static net.maizegenetics.pangenome.minimap2.Minimap2Paths.outputDir;
import static net.maizegenetics.pangenome.minimap2.Minimap2Paths.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Range;
import com.google.common.collect.Streams;
import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.util.DirectoryCrawler;
import org.apache.commons.io.FileUtils;

/**
 * This pipeline starts with 2 fasta files: one for the reference
 * and one for an assembly.  From these 2 files it processes, using
 * minimap2 and sam/bcftools, to create a gvcf file to be used for
 * calling SNPs.
 * 
 * This test pipeline also includes the creation of inter-anchor regions
 *
 * @author lcj34
 * @author Ed Buckler
 */
public class RunMinimapPipeline {
    public static void main(String[] args) throws IOException {

        Long totalTime = System.nanoTime();
//        try {
//            System.out.println("Creating output dir");
//            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(new File(outputDir));
//            Files.createDirectories(Paths.get(outputDir));
//            Files.createDirectories(Paths.get(dataDir));
//            for (Path path : DirectoryCrawler.listPaths("glob:*",Paths.get(boxW22AlignmentDir))) {
//                Files.copy(path,Paths.get(dataDir+path.getFileName()));
//            }
//            // uncompress fastas if necessary
//
//            if (Files.exists(Paths.get(compressedW22Contigs))) {
//                System.out.println("Uncompressing w22");
//                uncompressFile(compressedW22Contigs, uncompressedW22Contigs);
//            }
//            if (Files.exists(Paths.get(compressedB73Chrom))) {
//                System.out.println("UNcompressing b73");
//                uncompressFile(compressedB73Chrom, uncompressedB73Chrom);
//            }
//
//        } catch (IOException e) {
//            System.err.println("Errors in creating genomes");
//            e.printStackTrace();
//        }
//
//        String shortName = uncompressedB73Chrom.substring(uncompressedB73Chrom.lastIndexOf("/")+1);
//        String b73ChromMMI = outputDir + shortName + ".mmi";
//        shortName = uncompressedW22Contigs.substring(uncompressedB73Chrom.lastIndexOf("/")+1);
//        String w22ChromMMI = outputDir + shortName + ".mmi";
//        // Step1: create minimap index.
//
//        System.out.println("Creating minimap2 indexes");
//        createMinimapIndex(uncompressedB73Chrom,b73ChromMMI);
//
//        System.out.println("Calling createSamFromMinimap");
//        createSamFromMinimap(uncompressedW22Contigs, b73ChromMMI, w22B73OutputSam);
//
//        System.out.println("Call createSortedIndexedBAM");
//        createSortedIndexedBAM(w22B73OutputSam, sortedBam);
//
//        System.out.println("Call regionalizedBam");
//        sliceBAM( sortedBam,  regionalizedBam,  regions);
//
//        System.out.println("call runMPileup");
//        runMPileup(uncompressedB73Chrom, regionalizedBam, mpileupBCF);
//
//        System.out.println("call gcvfFromBCF");
//        gvcfFromBCf(mpileupBCF,  gvcfFile);

        // The block of code below is the initial test code for creating inter-anchor regions.
        // It is not complete.  Overlaps need to be identified between inter-anchor regions,
        // and between anchors/inter-anchors.  The contig size may also need adjusting (that is
        // the pangenome.processAssemblyGenomes.CreateContigFastaFromAssemblyGenomePlugin.java)
        System.out.println("Extract inter-anchors");
//        10:1049718-1053027
//        10:1096862-1101234
        GenomeSequence assemblyGenomeSeq = GenomeSequenceBuilder.instance(compressedW22Chrom);
        Files.lines(Paths.get(interanchors))
                .skip(1)
                .map(line -> {
                    String[] tokens = line.split(",");
                   // System.out.println(line);
                    int start=Integer.parseInt(tokens[1]);
                    int end=Integer.parseInt(tokens[2]);
                    if(end<=start) {return Range.closed(Position.of(tokens[0], -2),
                            Position.of(tokens[0], -1));}
                    return Range.closed(Position.of(tokens[0], start), Position.of(tokens[0], end));

                }).filter(range -> range.lowerEndpoint().getChromosome().getName().equals("10"))
                .filter(range -> range.lowerEndpoint().getPosition() > 1_000_000)
                .filter(range -> range.upperEndpoint().getPosition() < 2_000_000)
                .forEach(interval -> {
                    //System.out.println();
                    //System.out.println(interval.toString());
                    try {
                        extractSelectedIntervals(regionalizedBam, interval, assemblyGenomeSeq);
                    } catch (IllegalStateException e) {
                        System.err.println(interval.toString()+" Error:"+e.getMessage());
                    } catch (NegativeArraySizeException e) {
                        System.err.println("Inversion error:"+interval.toString()+" Error:"+e.getMessage());
                    }
                });
        //System.out.println(sequence);

               System.out.println("Finished executing pipeline in " + (System.nanoTime() - totalTime)/1e9 + " seconds");
    }

    private static String extractSelectedIntervals(String bamFile, Range<Position> refIntervalQuery, GenomeSequence assemblyGenome) throws IllegalStateException {
        SamReader bamReader = SamReaderFactory.makeDefault().open(new File(bamFile));
        System.out.println(bamReader.getResourceDescription());
        String chromosome = refIntervalQuery.lowerEndpoint().getChromosome().getName();
        try {
            System.out.println("\nRefInterAnchorCoordinates: " + refIntervalQuery.lowerEndpoint().getPosition() + ":" 
                    + refIntervalQuery.upperEndpoint().getPosition());
            int targetRefPosition = refIntervalQuery.lowerEndpoint().getPosition();
            int assemblyStart = extractAssemblyPosition(assemblyGenome, bamFile, chromosome, targetRefPosition);
            targetRefPosition = refIntervalQuery.upperEndpoint().getPosition();
            int assemblyEnd = extractAssemblyPosition(assemblyGenome, bamFile, chromosome, targetRefPosition);
            return assemblyGenome.genotypeAsString(Chromosome.instance(chromosome), assemblyStart, assemblyEnd);
        } catch (IllegalStateException e) {
            throw e;
        }
    }

    // Find SAM records whose query overlaps the given reference range position
    // Records that are not primary (based on SAM file FLAGs field) are tossed.  If after filtering
    // there are still multiple alignments to the same region an error is returned.

    private static int extractAssemblyPosition(GenomeSequence assemblyGenome, String bamFile, String chromosome, int targetRefPosition) throws IllegalStateException {
        SamReader bamReader = SamReaderFactory.makeDefault().open(new File(bamFile));
        SAMRecord goodSAMRecord = Streams.stream(bamReader.queryOverlapping(chromosome, targetRefPosition, targetRefPosition))
                //.filter(samRecord -> !samRecord.isSecondaryOrSupplementary())
                .reduce((s1, s2) -> {
                    throw new IllegalStateException("Too many alignments at "+chromosome+":"+targetRefPosition);
                })
                .orElseThrow(() -> new IllegalStateException("No alignment present at "+chromosome+":"+targetRefPosition));
        // Alignment blocks from the CIGAR string. Filter out blocks whose sequence range
        // does not fall include the specified target reference position.
        AlignmentBlock alignmentBlock = goodSAMRecord.getAlignmentBlocks().stream()
                .filter(ab -> ab.getReferenceStart() + ab.getLength() > targetRefPosition)
                .filter(ab -> ab.getReferenceStart() < targetRefPosition)
                .findFirst().orElseThrow(() -> new IllegalStateException("No alignment block (i.e. CIGAR is insertion or deletion) at "+chromosome+":"+targetRefPosition));
        System.out.println(alignmentBlock.getReferenceStart() + " length:" + alignmentBlock.getLength());
        int assemblyContigStart = Integer.parseInt(goodSAMRecord.getReadName().split(":")[1]);
        int assemblyStart = assemblyContigStart + alignmentBlock.getReadStart() + (targetRefPosition - alignmentBlock.getReferenceStart()) - 1;
       // System.out.println("targetRefPosition:\t" + targetRefPosition+"\t-> assemblyStart\t"+assemblyStart);
        System.out.println("targetRefPosition:\t" + targetRefPosition+"\t-> assemblyStart\t"+goodSAMRecord.getReadName());
        System.out.println("ASSEM:"+assemblyGenome.genotypeAsString(Chromosome.instance(chromosome), assemblyStart, assemblyStart + 50));
        GenomeSequence b73genome = GenomeSequenceBuilder.instance(uncompressedB73Chrom);
        System.out.println("REF  :"+b73genome.genotypeAsString(Chromosome.instance(chromosome), targetRefPosition, targetRefPosition + 50));
        return assemblyStart;
    }

    private static void uncompressFile(String compressedFile, String uncompressedName) {
        try {
            // uncompress files
            ProcessBuilder builder = new ProcessBuilder(
                    "gunzip", compressedFile);
            Process process;
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) System.out.println("Error uncompressing file: " + compressedFile + " Error:" + error);
        } catch (Exception exc) {
            System.err.println("Errors uncompressing file " + compressedFile);
            throw new IllegalStateException("Error uncompressing file " + compressedFile + " " + exc.getMessage());
        }

    }

    // Create indices for ref or assembly
    private static void createMinimapIndex(String fastaFile, String indexName) {
        try {

            ProcessBuilder builder = new ProcessBuilder(
                    minimap2Path, "-x", "asm10", "-d", indexName, fastaFile);

            Process process;
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error running minimap Index");
                throw new IllegalStateException("Error creating minimap index");
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    //Create sam file from assembly fasta using index
    private static void createSamFromMinimap(String fastaFile, String indexName, String outputSamFile) {
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    minimap2Path, "-ax", "asm10", "-N", "0", indexName, fastaFile);

            System.out.println("Command:" + builder.command().toString());
            Process process;
            File outputFile = new File(outputSamFile);
            builder.redirectOutput(outputFile);
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error creating SAM file from  minimap2");
                throw new IllegalStateException("Error creating SAM File from minimap2 ");
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    // Converts SAM to BAM, sorts and indexes the BAM file
    private static void createSortedIndexedBAM(String samFile, String sortedBam) {
        try {

            String samName = samFile.substring(0, samFile.indexOf("."));
            String unsortedBam = samName + ".bam";
            ProcessBuilder builder = new ProcessBuilder(
                    samToolsPath, "view", "-bS", samFile);

            Process process;
            File outputFile = new File(unsortedBam);
            builder.redirectOutput(outputFile);
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error creating UNsorted BAM file ");
                throw new IllegalStateException("Error creating UNsorted BAM ");
            }

            builder = new ProcessBuilder(
                    samToolsPath, "sort", "--threads", "8", "-T", "./tmp", "-o", sortedBam, unsortedBam);
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                System.out.println("Error creating sorted Bam file ");
                throw new IllegalStateException("Error creating sorted BAM File  ");
            }

            builder = new ProcessBuilder(
                    samToolsPath, "index", sortedBam);
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                System.out.println("Error creating index on sorted Bam file ");
                throw new IllegalStateException("Error creating index on sorted BAM File  ");
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }


    // Section BAM to select region area
    private static void sliceBAM(String inputBAM, String outputBAM, String regionFile) {

        try {

            ProcessBuilder builder = new ProcessBuilder(
                    samToolsPath, "view", "-b", "-o", outputBAM, "-L", regionFile, inputBAM);

            Process process;
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error slicing BAM file ");
                throw new IllegalStateException("Error slicing BAM File  ");
            }

            // INdex the sliced BAM file
            builder = new ProcessBuilder(
                    samToolsPath, "index", outputBAM);
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                System.out.println("Error creating index on sliced Bam file ");
                throw new IllegalStateException("Error creating index on sliced BAM File  ");
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    // Run mpileup on sorted/indexed bam file - get bcf file
    private static void runMPileup(String refFasta, String bamFile, String outputBCF) {
        try {

            // The Minimap3PipelinePlugin code adds the -l <intervalsFilet> option
            // to the mpileup command.  The intervalsFile is the bed file formatted
            // version of the hackathon_trimmed.intervals file stored in pangenome.data
            // in this repository.
            ProcessBuilder builder = new ProcessBuilder(
                    samToolsPath, "mpileup", "-ugf", refFasta, bamFile, "-t", "DP");

            Process process;
            File outputFile = new File(outputBCF);

            builder.redirectOutput(outputFile);

            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error running mpileup " + error + ", refFasta:" + refFasta
                        + " bamFile: " + bamFile);
                throw new IllegalStateException("Error running mpileup");
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    //Create gcvf from bcf using bcftools
    private static void gvcfFromBCf(String bcfFile, String gvcfFile) {
        try {

            ProcessBuilder builder = new ProcessBuilder(
                    bcftoolsPath, "call", "-m", "--ploidy", "1", "-g", "0", "-o", gvcfFile, bcfFile);

            Process process;
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                System.out.println("Error running gvcfFromGCF " + error + ", bcfFile:" + bcfFile
                        + " gvcfFile output: " + gvcfFile);
                throw new IllegalStateException("Error running gvcfFromBCF");
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
