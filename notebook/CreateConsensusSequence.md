#This document denotes the steps to run the Consensus finding algorithm from September 15, 2017-present.

###Author: Zack Miller

##Inputs
* Directory of GVCF files
* Interval File
* Reference file


##Outputs
* A set of consensus vcf files.  One for each haplotype.
* A set of Fasta files one for each anchor region.


##Important files
* Interval file
        
        /data1/PHG/hackathon_trimmed.intervals

* GVCFs
    * CIMMYT+NAM(Contains .idx files which GVCFTyper Needs)
    
            /data1/PHG/gvcfFilteredFastas/CIMMYTAndNAM/GVCFs/
            
    * Novogene Files(No idx, you will have to regenerate them)
    
            /data1/PHG/gvcfFilteredFastas/NovoGene/GVCFs/
            
    * CIMMYT+NAM+Novogene = 306 taxa(They are also copied here)
    
            /data1/PHG/GVCFTyperResults/306Taxa/GVCFs/
            
        * This directory has the index files as well.  I suggest you use these if you need to remake the VCF file

* Sample export VCFs for current pipeline
    
        /data1/PHG/GVCFTyperResults/110Taxa/

* Example scripts:
    * GVCFTyper to run all intervals
    
            cbsublfs1.tc.cornell.edu://data1/PHG/GVCFTyperResults/306Taxa/run_gvcf.sh
        
    * Also stored in repo
         
            /scripts/hapcollapse/run_gvcf.sh
            
* VCF result of GVCFTyper run on 306 taxa(CIMMYT+NAM+Novogene), index is also included

            cbsublfs1.tc.cornell.edu://data1/PHG/GVCFTyperResults/306Taxa/gvcfTyperOutputFiltered_EmitModeConfAllAnchors.vcf
    

##Current Working Pipeline(Small files)

* To Run GVCFTyper one anchor at a time use GVCFTyperPlugin:

            new GVCFTyperPlugin(null,false)
                            .inputGVCFDir(gvcfDirectory)
                            .inputIntervalFile(anchorIntervalFile.intervals)
                            .referenceFile(referenceFile.fa)
                            .numThreads(100)
                            .outputVCFDir(outputVCFDirectory)
                            .emitModeParam(GVCFTyperPlugin.EMIT_MODE.confident)
                            .processData(null);
                        
* Once you have a VCF file for each interval, run FindHaplotypeClustersPlugin.java:
        
            DataSet input = DataSet.getDataSet(ImportUtils.readFromVCF(vcfFileNamAndPath));
            
            FindHaplotypeClustersPlugin plugin = new FindHaplotypeClustersPlugin(null, false)
                    .outFile(nameOfOutputVCFFileDirectory)
                    .maxDistFromFounder(0.001)
                    .sequenceOutDir(nameOfOutputSequenceDirectory)
                    .referenceSequence(genomeSequence)
                    .intervalFile(intervalFile);
            
            plugin.processData(input);
        
    * an example main method I used to run is here:
    
             public static void main(String args[]) {
                    if(args.length!=6) {
                        System.out.println("Incorrect number of command line parameters.");
                        System.out.println("Please do the following:");
                        System.out.println(">java -Xmx100g -jar FindHaplotypeClustersPluginTest.jar inputVCFDirectory outputVCFDirectory outputSequenceDirectory refFasta errorLogFileName intervalFileName");
                    }
                    List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.vcf", Paths.get(args[0]));
                    try (BufferedWriter vcfErrorLogFile = Utils.getBufferedWriter(args[4])){
                        vcfErrorLogFile.write("File\tErrorMessage\n");
                        GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(args[3]);
            
                        for(int i = 0; i < fastaPaths.size();i++) {
                            Path fastaPath = fastaPaths.get(i);
                            try {
                                DataSet input = DataSet.getDataSet(ImportUtils.readFromVCF(fastaPath.toString()));
            
                                FindHaplotypeClustersPlugin plugin = new FindHaplotypeClustersPlugin(null, false)
                                        .outFile(args[1])
                                        .maxDistFromFounder(0.001)
                                        .sequenceOutDir(args[2])
                                        .referenceSequence(genomeSequence)
                                        .intervalFile(args[5]);
            
                                        plugin.processData(input);
                            }catch (Exception e) {
                                System.out.println("Error in this vcf:" + fastaPath.toString());
                                try {
                                    vcfErrorLogFile.write(fastaPath + "\t" + e.getMessage()+"\n");
                                }
                                catch(Exception innerExc) {
                                    innerExc.printStackTrace();
                                }
                                e.printStackTrace();
                            }
                        }
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
        
* Find HaplotypeClustersPlugin will create a set of consensus vcf files and fasta files for the current anchor interval you are processing.
  * This plugin is called a number of times(in maize 37k times) as it only works on one anchor at a time.
  * While you are looping remember to catch Exceptions but not stop running the program.  GVCFTyper will sometimes export a vcf file which does not have any intervals in it.  If there are no intervals, the Plugin will throw an exception which should be written to a file.
  * FindHaplotypeClustersPluginTest.java has an example in the main method of how this could be done easily.
  
* When You have a vcf and a fasta for each consensus sequence you can load things into the db

##Proposed New Pipeline
* Current pipeline is very slow as we add more taxa to it.  
    * Will take 2-3 days of runtime to run GVCFTyper interval by interval
    * To remedy this, run GVCFTyper once over all intervals
    * Only problem is it makes a huge VCF file(310 GB for 306 anchors) but it takes a few hours as opposed to days
* To handle the huge VCF file, we have 3 options.  First one is preferred, but will likely take more time to implement:
  * Retool FindHaplotypeClustersPlugin to htsjdk and just query the current interval region you are processing.
    * This would work, but the only downside is that we use TASSEL's vcf reader to get a GenotypeTable for the vcf
    * If you could create a simple GenotypeTable built on the List<VariantContext> returned by htsjdk, this would work well.
    * Long term I think this is the way it should be.
  * Only partially retool FindHaplotypeCallerPlugin to write the requested interval's VariantContexts to a temp VCF file
    * Then load in that VCF like we do now.
    * Pros:
        * Don't need to change TASSEL code
        * You don't even have to change the Plugin much, just add an additional step to output the subsetted VCF file
    * Cons:
        * Bit of a hack
        * Have to make a bunch of small vcf files only to be read in and then discarded
  * Write a simple plugin which just goes through each interval and pulls out the requested interval and writes a VCF file
    * Pros:
        * No modifying FindHaplotypeClustersPlugin as you now have a directory of VCF files
        * Logic to pull the sub regions would likely be very simple using htsjdk and you can keep the vcf header the same as the original big file
        * Would likely be very quick to get up and running and should be relatively fast.
    * Cons:
        * Also a hack
        * Long term not really a viable solution
        
        
##Troubleshooting
* Indexing issues:  If htsjdk cannot find index, use either Sentieon or make a tabix

    * Tabix indexing

            System.out.println("bgzip the file");
            String outputGVCFFilteredGzipped = outputGVCFFilteredFile + ".gz";
            builder = new ProcessBuilder(
                    "bgzip",  outputGVCFFilteredFile
                   
            );
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) System.err.println("\nERROR " + error + " createing bgzipped  version of file: " + outputGVCFFilteredFile);
            
            System.out.println("Create  index as tbi file");            
            String filteredIndexFile = outputGVCFFilteredGzipped + ".tbi";
            // -t is for TBI-format index
            // While the docs show a -o option, when given one bcftools dies with "invalid option -- 'o'.
            // Hence, the default output file is used, which is input file + .tbi
            builder = new ProcessBuilder(
                    "bcftools", "index","-t", outputGVCFFilteredGzipped                    
            );
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            
    * Sentieon indexing(Shell script):
    
            for gvcf in $gvcfFiles
            do
               echo 'Indexing'${gvcf}
               $SENTIEON util vcfindex ${GVCFFileDir}/${gvcf}
            done