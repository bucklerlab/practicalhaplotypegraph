Small Seq test is designed to provide known genomes sequences that are evolved to create
a coalescent and recombination in one of the sequences.

Essentially, this provides a:
* Reference genome
* Assembled genomes
* High coverage fastq
* Low coverage fastq
* Recombinant sequences

Install 
## install BWA
code
git clone https://github.com/lh3/bwa.git
cd bwa; make

## Install SAM tools
http://www.htslib.org/download/

Need to install 
xz (via Homebrew on macOS) is installed - or use the disable lzma command

````commandline
cd samtools-1.x    # and similarly for bcftools and htslib
./configure --prefix=/where/to/install
./configure --prefix=/Users/edbuckler/clsoftware --disable-lzma
make
make install
export PATH=/Users/edbuckler/clsoftware/bin:$PATH 
````
## Install Picard tools
????

````sql
-- Table: anchor_haplotypes
CREATE TABLE anchor_haplotypes (
    anchor_haplotypes_id INTEGER PRIMARY KEY,
    hap_group_id INTEGER, --could be changed to groups of haplotypes
    anchorid INTEGER NOT NULL,
    anchor_sequence_id INTEGER, --These are really reference genomes used by the GVCF
    method_id INTEGER,
    gvcf BLOB,
    sequence TEXT,  --derived sequence based on gvcf and anchor_sequence_id
    additional_data TEXT,
    UNIQUE (hap_group_id, anchorid, anchor_sequence_id, method_id),
    FOREIGN KEY (hap_group_id) REFERENCES hap_groups(hap_group_id),
    FOREIGN KEY (anchorid) REFERENCES anchors(anchorid),
    FOREIGN KEY (anchor_sequence_id) REFERENCES anchor_sequences(anchor_sequence_id),
    FOREIGN KEY (method_id) REFERENCES methods(method_id)
);

CREATE TABLE hap_groups (
    hap_group_id INTEGER,
    hap_id INTEGER, --could be changed to groups of haplotypes
    UNIQUE (hapid, hap_group_id)
);
````