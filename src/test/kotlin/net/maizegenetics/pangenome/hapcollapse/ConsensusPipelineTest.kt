package net.maizegenetics.pangenome.hapcollapse

import com.google.common.collect.*
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.VariantsProcessingUtils
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.pangenome.api.HaplotypeNode.VariantInfo
import net.maizegenetics.pangenome.db_loading.GZipCompression
import net.maizegenetics.pangenome.db_loading.GetDBConnectionPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Test
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.PrintWriter
import java.lang.StringBuilder
import java.sql.Connection
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.HashMap
import kotlin.test.assertEquals

fun main() {
//    runSmallSeqPipeline()
//    compareVariants()
//    displayVariants()
//    testConsensusVariants()
//    testSorghum()
//    compareSequences()
//    countNsForHaplotypeSequence()

}

fun runSmallSeqPipeline() {
    LoggingUtils.setupDebugLogging()
    //test RunHapConsensusPipelinePlugin using an existing SmallSeq db
    //the test will add consensus haplotypes

//    val configFilename = SmallSeqPaths.dbConfigFile
    val configFilename = "/Users/peterbradbury/temp/configSQLite.txt"

    //build a haplotype graph
    val hapgraphDs : DataSet = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(configFilename)
            .methods("GATK_PIPELINE")
            .includeVariantContexts(true)
            .performFunction(null)

    RunHapConsensusPipelinePlugin().reference(SmallSeqPaths.refGenomePath)
            .dbConfigFile(configFilename)
            .collapseMethod("consensusTest")
            .collapseMethodDetails("consensusTest;detail")
            .minAlleleFrequency(0.5)
            .minTaxaCoverage(0.5)
            .maxNumberOfClusters(5)
            .minSiteForComp(40)
            .performFunction(hapgraphDs)

}

//fun compareVariants() {
//    LoggingUtils.setupDebugLogging()
//    val graph1 = HaplotypeGraphBuilderPlugin(null,false)
//            .configFile(SmallSeqPaths.dbConfigFile)
//            .methods("CONSENSUS")
//            .includeVariantContexts(true)
//            .build()
//
//    for (refrange in graph1.referenceRangeList()) {
//        val nodes = graph1.nodes(refrange)
//        println("\nCONSENSUS range at ${refrange.start()}:")
//        for (node in nodes) {
//            println("${node.taxaList().map{t -> t.getName()}}")
//            println("sequence length = ${node.haplotypeSequence().length()}")
//        }
//
//    }
//
//    val graph2 = HaplotypeGraphBuilderPlugin(null,false)
//            .configFile(SmallSeqPaths.dbConfigFile)
//            .methods("consensusTest")
//            .includeVariantContexts(true)
//            .build()
//
//    for (refrange in graph2.referenceRangeList()) {
//        val nodes = graph2.nodes(refrange)
//        println("\nconsensusTest range at ${refrange.start()}:")
//        for (node in nodes) println("${node.taxaList().map{t -> t.getName()}}")
//    }
//
//    //compare variants
//    for (refrange in graph1.referenceRangeList()) {
//        val nodes1 = graph1.nodes(refrange)
//        val nodes2 = graph2.nodes(refrange)
//        for (node in nodes1) {
//            val headTaxa = node.taxaList();
//            val headNames = headTaxa.map {it.getName()}.joinToString(",")
//            val compNode = nodes2.find { comp -> taxaListsEqual(node.taxaList(), comp.taxaList()) }
//            if (compNode == null) println("No matching node found for $headNames at pos ${refrange.start()}")
//            else {
//                val nodeIds = node.variantInfos().get().filter {it.isVariant}.map {it.variantId()}
//                val compIds = compNode.variantInfos().get().filter {it.isVariant}.map {it.variantId()}
//                val nodeNotInComp = nodeIds.filter {!compIds.contains(it)}
//                val compNotInNode = compIds.filter {!nodeIds.contains(it)}
//                println("$headNames at ${refrange.start()}: ${nodeIds.size} node ids, ${compIds.size} comp ids," +
//                        " ${nodeNotInComp.size} nodes not in comp, ${compNotInNode.size} comps not in node")
//                val headStart = node.variantInfos().get().minOf {it.start()}
//                val headEnd = node.variantInfos().get().maxOf {it.end()}
//                val compStart = compNode.variantInfos().get().minOf {it.start()}
//                val compEnd = compNode.variantInfos().get().maxOf {it.end()}
//                println("head range = [$headStart..$headEnd], comp range = [$compStart..$compEnd]")
//            }
//
//        }
//    }
//
//    //count missing sites
//    println("")
//    for (refrange in graph1.referenceRangeList()) {
//        val nodes1 = graph1.nodes(refrange)
//        val nodes2 = graph2.nodes(refrange)
//        for (node in nodes1) {
//            val headTaxa = node.taxaList();
//            val headNames = headTaxa.map {it.getName()}.joinToString(",")
//            val compNode = nodes2.find { comp -> taxaListsEqual(node.taxaList(), comp.taxaList()) }
//            if (compNode == null) println("No matching node found for $headNames at pos ${refrange.start()}")
//            else {
//                val headSet: RangeSet<Int> = TreeRangeSet.create()
//                for (info in node.variantInfos().get()) headSet.add(Range.closed(info.start(), info.end()))
//                val missingHead = (headSet.span().lowerEndpoint() .. headSet.span().upperEndpoint()).filter {!headSet.contains(it)}.count()
//
//                val compSet: RangeSet<Int> = TreeRangeSet.create()
//                for (info in compNode.variantInfos().get()) compSet.add(Range.closed(info.start(), info.end()))
//                val missingComp = (compSet.span().lowerEndpoint() .. compSet.span().upperEndpoint()).filter {!compSet.contains(it)}.count()
//
//                println("$headNames, range at ${refrange.start()}: $missingHead head sites missing, $missingComp comp sites missing.")
//
//                print("head: ")
//                val sitesHead = (headSet.span().lowerEndpoint() .. headSet.span().upperEndpoint()).filter {!headSet.contains(it)}.joinToString(",")
//                println(sitesHead)
//
//                print("comp: ")
//                val sitesComp = (compSet.span().lowerEndpoint() .. compSet.span().upperEndpoint()).filter {!compSet.contains(it)}.joinToString(",")
//                println(sitesComp)
//
//            }
//
//        }
//    }
//
//
//}

fun taxaListsEqual(tlist1: TaxaList, tlist2: TaxaList) : Boolean {
    for (taxon in tlist1) if (!tlist2.contains(taxon)) return false;
    for (taxon in tlist2) if (!tlist1.contains(taxon)) return false;
    return true;
}

fun displayVariants() {
    LoggingUtils.setupDebugLogging()
    val graph = HaplotypeGraphBuilderPlugin(null,false)
            .configFile(SmallSeqPaths.dbConfigFile)
            .methods("GATK_PIPELINE")
            .includeVariantContexts(true)
            .build()

    val myRefRange = graph.referenceRangeList().get(18);
    val nodeList = graph.nodes(myRefRange)
    for (node in nodeList) {
        val tnames = node.taxaList().map{it.name}.joinToString(",")
        println("Taxon: $tnames")
        val infoList = node.variantInfos().get();

        for (info in infoList) {
            println("at ${info.start()} to ${info.end()}  geno ${info.genotypeString()} ref ${info.refAlleleString()} alt ${info.altAlleleString()}")
        }
    }
}

fun testSorghum() {
    val config = "/Users/peterbradbury/temp/sorghum/config_sorghumtest.txt"
    val myMethod = "GATK_PIPELINE"
    val outdir = "/Users/peterbradbury/temp/sorghum/out"
    val collapseMethod = "collapse_test1"
    val collapseDetail = "collapse_detail"
    val referenceName = "/Users/peterbradbury/temp/sorghum/Sbicolor_313_v3.0_numberedChr.fa"

    LoggingUtils.setupDebugLogging()

    val myGraphDataset = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(config).methods(myMethod).includeVariantContexts(true).performFunction(null)

    RunHapConsensusPipelinePlugin(null,false)
            .reference(referenceName).dbConfigFile(config)
            .collapseMethod(collapseMethod).collapseMethodDetails(collapseDetail).performFunction(myGraphDataset)

}

class ConsensusPipelineTest {


    @Test fun testVariantOrder() {
        val variant1 = VariantInfo("1", 1, 1, "A", "A", "C", true, 0L)
        val variant2 = VariantInfo("1", 5, 5, "A", "A", "C", true, 0L)
        val variant3 = VariantInfo("1", 10, 10, "A", "A", "C", true, 0L)
        val variant4 = VariantInfo("2", 1, 1, "A", "A", "C", true, 0L)

        var varList = arrayListOf<VariantInfo>(variant1, variant2, variant3, variant4)
        Assert.assertEquals(true, ConsensusProcessingUtils.variantsInOrder(varList))

        varList = arrayListOf<VariantInfo>(variant2, variant1, variant3, variant4)
        Assert.assertEquals(false, ConsensusProcessingUtils.variantsInOrder(varList))

        varList = arrayListOf<VariantInfo>(variant1, variant2, variant4, variant3)
        Assert.assertEquals(true, ConsensusProcessingUtils.variantsInOrder(varList))

    }

    @Test
    fun convertVariantsToSequenceTest() {
        //test ConsensusProcessingUtils.convertVariantsToSequence
        //public static String convertVariantsToSequence(List<VariantInfo> variants, ReferenceRange refRange, GenomeSequence refSequence)
        //test 1. create specific variant infos to test resulting sequence
        //test 2. Can we use that to reconstitute SmallSeqVariant sequence

        val myChr = Chromosome.instance(1);
        val refseq = GenomeSequenceBuilder.instance(myChr, "ACGTACGTAAAATTTT");
        val refRange = ReferenceRange("test", myChr,1,16, 1);

//        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
//        String altAllele, boolean isVariant, long varLong)

        //test a SNP
        val variants = ArrayList<HaplotypeNode.VariantInfo>();
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 16, "REF", "REF", "NON_REF", false, 1L));
        var testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTAAAATTTT", testseq);

        //add a second SNP
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 9, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 10, 10, "T", "A", "T", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 11, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTATAATTTT", testseq);

        //add a deletion
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 11, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 12, 14, "A", "ATT", "A", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 15, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTAAAATT", testseq);

        //add an insertion (TT in the middle of 4 A's)
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 9, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 10, 10, "ATT", "A", "ATT", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 11, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTAATTAATTTT", testseq);

        //deletion followed by insertion
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 11, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 12, 14, "A", "ATT", "A", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 14, 14, "TGG", "T", "TGG", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 15, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTAAAAGGTT", testseq);

        //variant that is a ref call
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 5, "G", "A", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 6, 9, "CGTA", "CGTA", "C", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 10, 10, "T", "A", "T", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 11, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTGCGTATAATTTT", testseq);

        //ref call overlapped by alt call
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 10, "ACGTAA", "ACGTAA", "A", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 7, 7, "T", "G", "T", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 8, 10, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 11, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTACTTAAAATTTT", testseq);

        //ref call (alt is deletion) overlapped by alt call for a deletion
        variants.clear()
        variants.add(HaplotypeNode.VariantInfo("1", 1, 4, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 5, 10, "ACGTAA", "ACGTAA", "A", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 7, 8, "G", "GT", "G", true, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 9, 10, "REF", "REF", "NON_REF", false, 1L));
        variants.add(HaplotypeNode.VariantInfo("1", 11, 16, "REF", "REF", "NON_REF", false, 1L));
        testseq = ConsensusProcessingUtils.convertVariantsToSequence(variants, refRange, refseq)

        println("ref: ${refseq.genotypeAsString(myChr,1,16)}, alt: ${testseq}")
        assertEquals("ACGTACGAAAATTTT", testseq);

    }


}

fun testConsensusVariants() {
    val refRange = ReferenceRange("refrange", Chromosome.instance("1"), 1, 40, 0)
    val taxa = listOf<Taxon>(Taxon("t1"), Taxon("t2"), Taxon("t3"), Taxon("t4"), Taxon("t5"), Taxon("t6"))
    val infoList = RunHapConsensusPipelinePlugin(null, false).RunMergeAndCluster()
            .consensusVariants(makeConsensusVariantList(), taxa, refRange)
    infoList.forEach { println("${it.start()} to ${it.end()} : geno ${it.genotypeString()} ref ${it.refAlleleString()} alt ${it.altAlleleString()}") }

    //expected sequence
    val refSeq = "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
    val expectedSeq = "CCCCCCCCCACCCCGCNCCACCTCCCCAGGGCCCCACCCCC"

    val ref = GenomeSequenceBuilder.instance(Chromosome.instance("1"), refSeq)
    val obsSeq = ConsensusProcessingUtils.convertVariantsToSequence(infoList, refRange, ref)
    println(expectedSeq)
    println(obsSeq)
    println("obs = expected is ${obsSeq.equals(expectedSeq)}")

    val infoList2 = RunHapConsensusPipelinePlugin(null, false).RunMergeAndCluster()
            .consensusVariants(makeListWithDelCombos(), taxa, refRange)
    infoList2.forEach { println("${it.start()} to ${it.end()} : geno ${it.genotypeString()} ref ${it.refAlleleString()} alt ${it.altAlleleString()}") }

    val expected2 = "CCCCCCCCCACCCCGCCCCCTTTCCCCCCCCCCGCCCCCCC"
    val obs2 = ConsensusProcessingUtils.convertVariantsToSequence(infoList2, refRange, ref)
    println("-------------\nDeletion test")
    println(expected2)
    println(obs2)
    println("obs = expected is ${obs2.equals(expected2)}")

}

private fun rangeMapFromVariants(varIndex: IntArray, varList: List<VariantInfo>) : RangeMap<Int,VariantInfo> {
    val varmap = TreeRangeMap.create<Int,VariantInfo>()
    for (ndx in varIndex) {
        varmap.put(Range.closed(varList[ndx].start(), varList[ndx].end()), varList[ndx])
    }
    return varmap
}

fun makeConsensusVariantList() : Map<Taxon, RangeMap<Int,VariantInfo>> {

    val baseVariants = ArrayList<VariantInfo>()

    baseVariants.add(VariantInfo("1",10,10,"A","C","A",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",15,15,"G","C","G",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",20,22,"A","CCC","A",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",25,25,"T","C","T",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",30,30,"AGGG","C","AGGG",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",35,35,"A","C","A",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))

    val taxa = listOf<Taxon>(Taxon("t1"), Taxon("t2"), Taxon("t3"), Taxon("t4"), Taxon("t5"), Taxon("t6"))

    val baseRangemap = TreeRangeMap.create<Int, VariantInfo>()
    for (i in 0 until 6) baseRangemap.put(Range.closed(baseVariants[i].start(), baseVariants[i].end()), baseVariants[i])

    val fullRangemap = insertRefBlocks(baseRangemap, 1, 40)

    val taxaRangemap = HashMap<Taxon, RangeMap<Int,VariantInfo>>()
    taxa.subList(0,3).forEach { taxaRangemap.put(it, fullRangemap) }

    baseVariants.add(VariantInfo("1",17,17,"G","C","G",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    val baseRangemap2 = TreeRangeMap.create<Int, VariantInfo>()
    for (i in 0 until 7) baseRangemap2.put(Range.closed(baseVariants[i].start(), baseVariants[i].end()), baseVariants[i])
    val fullRangemap2 = insertRefBlocks(baseRangemap2, 1, 40)

    taxa.subList(3,6).forEach { taxaRangemap.put(it, fullRangemap2) }

    return taxaRangemap
}

fun makeListWithDelCombos() : Map<Taxon, RangeMap<Int,VariantInfo>> {

    val baseVariants = ArrayList<VariantInfo>()

    baseVariants.add(VariantInfo("1",10,10,"A","C","A",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",15,15,"G","C","G",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",20,22,"C","CCC","C",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",22,22,"CTTT","C","CTTT",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",30,30,"CCCCC","CCCCC","C",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    baseVariants.add(VariantInfo("1",33,33,"G","A","G",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))

    val taxa = listOf<Taxon>(Taxon("t1"), Taxon("t2"), Taxon("t3"), Taxon("t4"), Taxon("t5"), Taxon("t6"))

    val baseRangemap = TreeRangeMap.create<Int, VariantInfo>()
    for (i in 0 until baseVariants.size) baseRangemap.put(Range.closed(baseVariants[i].start(), baseVariants[i].end()), baseVariants[i])

    val fullRangemap = insertRefBlocks(baseRangemap, 1, 40)

    val taxaRangemap = HashMap<Taxon, RangeMap<Int,VariantInfo>>()
    taxa.subList(0,4).forEach { taxaRangemap.put(it, fullRangemap) }

    baseVariants.add(VariantInfo("1",17,17,"G","C","G",
            true, ConsensusProcessingUtils.encodeVariantToLong(1,0,2,false)))
    val baseRangemap2 = TreeRangeMap.create<Int, VariantInfo>()
    for (i in 0 until baseVariants.size) baseRangemap2.put(Range.closed(baseVariants[i].start(), baseVariants[i].end()), baseVariants[i])
    val fullRangemap2 = insertRefBlocks(baseRangemap2, 1, 40)

    taxa.subList(4,6).forEach { taxaRangemap.put(it, fullRangemap2) }

    return taxaRangemap
}

fun insertRefBlocks(inputRangeMap: RangeMap<Int,VariantInfo>, start: Int, end: Int) : RangeMap<Int,VariantInfo> {
    //insert ref blocks at non-variant position in range
    var previousEnd = start - 1

    val mapBuilder = ImmutableRangeMap.builder<Int,VariantInfo>()
    for (mapEntry in inputRangeMap.asMapOfRanges().entries) {
        val nextRange = mapEntry.key
        if (nextRange.lowerEndpoint() > previousEnd + 1) {
            val refBlockStart = previousEnd + 1
            val refBlockEnd = nextRange.lowerEndpoint() - 1
            val refBlockLength = refBlockEnd - refBlockStart + 1
            val refBlockDepth = 2;
            val varLong = ConsensusProcessingUtils.encodeRefBlockToLong(refBlockLength, refBlockDepth, refBlockStart)
            var refBlockInfo = VariantInfo("1", refBlockStart, refBlockEnd, "REF", "REF", "NON-REF", false, varLong)
            mapBuilder.put(Range.closed(refBlockStart,refBlockEnd), refBlockInfo)
        }
        mapBuilder.put(mapEntry.key, mapEntry.value)
        previousEnd = nextRange.upperEndpoint()
    }

    //add the final ref block
    if (end > previousEnd) {
        val refBlockStart = previousEnd + 1
        val refBlockEnd = end
        val refBlockLength = refBlockEnd - refBlockStart + 1
        val refBlockDepth = 2;
        val varLong = ConsensusProcessingUtils.encodeRefBlockToLong(refBlockLength, refBlockDepth, refBlockStart)
        var refBlockInfo = VariantInfo("1", refBlockStart, refBlockEnd, "REF", "REF", "NON-REF", false, varLong)
        mapBuilder.put(Range.closed(refBlockStart,refBlockEnd), refBlockInfo)
    }

    return mapBuilder.build()
}

fun compareSequences() {
    val config = "/Users/peterbradbury/temp/sorghum/config_sorghumtest.txt"

    val dbconn = GetDBConnectionPlugin(null,false).configFile(config)
            .createNew(false).performFunction(null).getData(0).data as Connection

    val statement = dbconn.createStatement()

    val taxonName = "DEKABES"
    val chr = "1"
    val rangeStart = 102030

    //4 = GATK PIPELINE
    //5 = consensus test
    var sqlstr = "select sequence, seq_len from haplotypes " +
            "where ref_range_id = (select ref_range_id FROM reference_ranges where chrom = $chr AND range_start = $rangeStart) " +
            "AND gamete_grp_id in (select gamete_grp_id FROM gamete_haplotypes gh, gametes gam, genotypes geno WHERE gh.gameteid = gam.gameteid AND gam.genoid = geno.genoid AND geno.line_name = '$taxonName') " +
            "AND method_id=4"

    println(sqlstr)

    var rs = statement.executeQuery(sqlstr)
    rs.next()
    val seq4 = GZipCompression.decompress(rs.getBytes("sequence"))
    val seqlen4 = rs.getInt("seq_len")
    rs.close()

    println(seqlen4)
    println(seq4)
    println("Ns in seq4 = ${countN(seq4)}")

    sqlstr = "select sequence, seq_len from haplotypes " +
            "where ref_range_id = (select ref_range_id FROM reference_ranges where chrom = $chr AND range_start = $rangeStart) " +
            "AND gamete_grp_id in (select gamete_grp_id FROM gamete_haplotypes gh, gametes gam, genotypes geno WHERE gh.gameteid = gam.gameteid AND gam.genoid = geno.genoid AND geno.line_name = '$taxonName') " +
            "AND method_id=5"

    println(sqlstr)

    rs = statement.executeQuery(sqlstr)
    rs.next()
    val seq5 = GZipCompression.decompress(rs.getBytes("sequence"))
    val seqlen5 = rs.getInt("seq_len")
    rs.close()

    println(seqlen5)
    println(seq5)
    println("Ns in seq5 = ${countN(seq5)}")

    println("seq4 = seq5: ${seq4.equals(seq5)}")
    statement.close()
    dbconn.close()
}

fun countN(seq : String) : Int {
    return seq.filter{ it == 'N'}.length
}

fun countNsForHaplotypeSequence() {
    //for each haplotype
    //range id, hap id, sequence length, # N

    //for each haplotype get sequence bytes, convert to sequence, count N's, output
    LoggingUtils.setupDebugLogging();
    val config = "/Users/peterbradbury/temp/sorghum/config_sorghumtest.txt"

    val dbconn = GetDBConnectionPlugin(null,false).configFile(config)
            .createNew(false).performFunction(null).getData(0).data as Connection

    val statement = dbconn.createStatement()

    val outname = "/Users/peterbradbury/temp/sorghum/out/haplotype_stats_method5.txt"
    val sqlstr = "select h.ref_range_id, chrom, range_start, range_end, haplotypes_id, seq_len, sequence, method_id " +
            "FROM reference_ranges rr, haplotypes h WHERE method_id=5 AND h.ref_range_id=rr.ref_range_id order by h.ref_range_id"
    println(sqlstr)
    val rs = statement.executeQuery(sqlstr)

    PrintWriter(outname).use {
        it.println("refRange\tchr\tstart\tend\tlength\thapid\tseq_len\tNcount")
        while (rs.next()) {
            val seqstr = GZipCompression.decompress(rs.getBytes("sequence"))
            val ncount = countN(seqstr)
            val start = rs.getInt("range_start")
            val end = rs.getInt("range_end");
            it.println("${rs.getInt("ref_range_id")}\t${rs.getInt("chrom")}\t$start" +
                    "\t$end\t${end - start + 1}\t${rs.getInt("haplotypes_id")}" +
                    "\t${rs.getInt("seq_len")}\t${ncount}")
        }
    }

    rs.close()
    statement.close()
    dbconn.close()
}

