--liquibase formatted sql

--## update paths table for postgres and sqlite

--changeset lcj34:updatepathstable_sqlite dbms:sqlite

DROP TABLE IF EXISTS paths;
CREATE TABLE paths(path_id INTEGER PRIMARY KEY, rm_group_id INTEGER, method_id INTEGER, haplotype_paths BLOB);

--changeset lcj34:updatepathstable_postgres dbms:postgresql

DROP TABLE IF EXISTS paths;
CREATE TABLE paths(path_id SERIAL PRIMARY KEY, rm_group_id INTEGER, method_id INTEGER, haplotype_paths bytea);
