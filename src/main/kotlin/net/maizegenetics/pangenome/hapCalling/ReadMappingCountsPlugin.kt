package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.*
import net.maizegenetics.util.Utils
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * ReadMappingCountsPlugin writes a file with read mapping counts by taxa and file-group for a read method.
 * The input parameters are the name of the file
 * to which the results will be written (outputFile) and the read method name (readMethod).
 *
 * The plugin expects the database config file to have already been loaded into ParameterCache, perhaps using
 * -configParameters or ParameterCache.load().
 */
class ReadMappingCountsPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
        .outFile()
        .required(true)
        .description("The file path for storing the read counts by taxon and file group.")
        .build()

    private var readMethod = PluginParameter.Builder("readMethod", null, String::class.java)
        .required(true)
        .description("The name of the read method for which to report read counts")
        .build()

    override fun pluginDescription(): String {
        return "Report the number of reads mapped to all taxa for a read method. The plugin expects the database config " +
                "parameters to be loaded into the ParameterCache, perhaps with -configParameters or ParameterCache.load()."
    }

    data class ReadMappingCount(val taxon: String, val fileGroup: String, val readCount: Int)

    override fun processData(input: DataSet?): DataSet? {
        val readCountList = listReadCounts(readMethod())
        Utils.getBufferedWriter(outputFile()).use { myWriter ->
            myWriter.write("taxon\tfileGroup\treadCount\n")
            readCountList.forEach {
                myWriter.write("${it.taxon}\t${it.fileGroup}\t${it.readCount}\n")
            }
        }

        return null
    }

    fun listReadCounts(method: String): List<ReadMappingCount>{

        val phgdb = PHGdbAccess(DBLoadingUtils.connection(false))

        val readMappingCounts = mutableListOf<ReadMappingCount>()
        val decoder = ReadMappingDecoder(phgdb)

        val mappings = phgdb.getReadMappingIdsForReadMethod(method)
        for (readMapping in mappings) {
            val hapidSetCounts = decoder.getDecodedReadMappingForMappingId(readMapping.readMappingId)
            val readCount = hapidSetCounts.values.sum()
            readMappingCounts.add(ReadMappingCount(taxon = readMapping.genoName,
                fileGroup = readMapping.fileGroupName,
                readCount = readCount))
        }

        phgdb.close()
        return readMappingCounts
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Read Counts"
    }

    override fun getToolTipText(): String {
        return "Report number of read mappings for a method"
    }

    /**
     * The file path for storing the read counts by taxon
     * and file group.
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. The file path for storing the read
     * counts by taxon and file group.
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): ReadMappingCountsPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

    /**
     * The name of the read method for which to report read
     * counts
     *
     * @return Read Method
     */
    fun readMethod(): String {
        return readMethod.value()
    }

    /**
     * Set Read Method. The name of the read method for which
     * to report read counts
     *
     * @param value Read Method
     *
     * @return this plugin
     */
    fun readMethod(value: String): ReadMappingCountsPlugin {
        readMethod = PluginParameter<String>(readMethod, value)
        return this
    }

}
