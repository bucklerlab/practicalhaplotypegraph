package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.plugindef.ParameterCache
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.sql.Connection
import java.sql.SQLException
import kotlin.test.assertEquals
import kotlin.test.fail

@ExtendWith(SmallSeqExtension::class)
class ChangeDBMethodTypePluginIT {

    @Test
    fun testChangeDBMethodPlugin() {
        val methodName = "GATK_PIPELINE_PATH"
        val newType = DBLoadingUtils.MethodType.TEST_PATHS.value
        val oldType = DBLoadingUtils.MethodType.PATHS.value

        ParameterCache.load(SmallSeqExtension.configFilePath)

        val dbConnectDS = GetDBConnectionPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .createNew(false) // make sure this is false!! don't want to overwrite the db
            .performFunction(null)

        // verify the current value of the method type
        try {
            val query = "SELECT method_type from methods where name='${methodName}';"
            val connection = dbConnectDS.getData(0).data as Connection
            var origType = 0
            connection.createStatement().executeQuery(query).use { rs ->
                if (rs.next()) { origType = rs.getInt(1) }
                assertEquals(oldType, origType)
            }
        } catch (sqle: SQLException) {
            throw fail("Error verifying current value of method type: ${sqle.message}")
        }

        ChangeDBMethodTypePlugin(null,false)
            .methodName(methodName)
            .methodType(newType)
            .performFunction(dbConnectDS)

        // verify the changed value of the method type
        try {
            val query = "SELECT method_type from methods where name='${methodName}';"
            val connection = dbConnectDS.getData(0).data as Connection
            var changedType = 0
            connection.createStatement().executeQuery(query).use { rs ->
                if (rs.next()) { changedType = rs.getInt(1) }
                assertEquals(newType, changedType)
            }

        } catch (sqle: SQLException) {
            fail("Error verifying the changed value of method type: ${sqle.message}")
        }
    }
}