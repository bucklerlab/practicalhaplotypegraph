#!/bin/bash -x

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201704.01
SENTIEON='/programs/sentieon-genomics-201704.01/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'

BAMFilesInDir='FilteredBAMFiles'
HaplotyperOutputDir='HaplotyperOutput'
FilteredGVCFOutputDir='GVCFFilteredFiles'

cd $BAMFilesInDir
taxaNames="`ls *.bam | cut -d- -f1 | sort -u`"
cd ..

for taxon in $taxaNames
do

echo "Taxon: " $taxon

time bcftools filter ${HaplotyperOutputDir}/${taxon}_haplotype_caller_output.g.vcf \
   -e "FORMAT/DP>16 || FORMAT/DP<3 || FORMAT/GQ<50 || QUAL<200 || (FORMAT/AD[0]>0 && FORMAT/AD[1]>0) || (FORMAT/AD[0]>0 && FORMAT/AD[2]>0) || (FORMAT/AD[1]>0 && FORMAT/AD[2]>0)" \
   -o ${FilteredGVCFOutputDir}/${taxon}_haplotype_caller_output_BestFiltersBySearch.g.vcf
done