/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Utils;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

import net.maizegenetics.pangenome.db_loading.CreateIntervalsFileFromGffPlugin;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;

import static net.maizegenetics.pangenome.db_loading.CreateIntervalsFileFromGffPlugin.addRange;
import static net.maizegenetics.pangenome.db_loading.CreateIntervalsFileFromGffPlugin.createFlankingList;
import static org.junit.Assert.*;

/**
 * Test class to verify methods internal to the CreateIntervalsFileFromGff class
 * 
 * @author lcj34
 *
 */
public class CreateIntervalsFileFromGffPluginTest {

    String userHome = System.getProperty("user.home");
    String outputDir = userHome + "/temp/CreateIntervalsFromGffFile/";

    String refFile = outputDir + "testRefGenome.fa";

    int numFlanking = 100; // number flanking bps to add on each end
    GenomeSequence myRefSequence;

    @Before
    public void setup() throws Exception {
        // Could create the reference sequence in the setup if multiple
        // test cases will need it.  RIght now, only 1 does, so created
        // it in that test case.

        LoggingUtils.setupDebugLogging();

        // Reset temp files
        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(new File(outputDir));
            Files.createDirectories(Paths.get(outputDir));
        } catch (IOException ioe) {
            throw new IllegalStateException("GFFTests:setup: error deleting/creating folders: " + ioe.getMessage());
        }

        createTestRefGenome(refFile);

    }
    
    // Test the addRanges() class.  This class merges embedded or overlapping genes
    // into 1 entry, then adds them to a map.  The
    @Test
    public void testAddRanges() throws Exception {
        RangeMap<Position,String> geneRangeMap = TreeRangeMap.create();
        Chromosome chr1 = new Chromosome("1");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,5).build(),
                new GeneralPosition.Builder(chr1,15).build()),"firstGene");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,20).build(),
                new GeneralPosition.Builder(chr1,25).build()),"gene2");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,30).build(),
                new GeneralPosition.Builder(chr1,52).build()),"gene3");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,33).build(),
                new GeneralPosition.Builder(chr1,45).build()),"embeddedGene1");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,47).build(),
                new GeneralPosition.Builder(chr1,60).build()),"overlappedGene1");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,75).build(),
                new GeneralPosition.Builder(chr1,102).build()),"gene4");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,110).build(),
                new GeneralPosition.Builder(chr1,200).build()),"gene5");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,150).build(),
                new GeneralPosition.Builder(chr1,250).build()),"overlappedGene2");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,300).build(),
                new GeneralPosition.Builder(chr1,302).build()),"gene6");
        
        System.out.println("Last gene added, size of range: " + geneRangeMap.asMapOfRanges().size());
        
        assertEquals(geneRangeMap.asMapOfRanges().size(),6);
        String thirtySixtyValue = "gene3-embeddedGene1-overlappedGene1";
        Position thirty = new GeneralPosition.Builder(chr1,30).build();
               
        String thirtySixtyFromMap = geneRangeMap.get(thirty);
        assertTrue(thirtySixtyValue.equals(thirtySixtyFromMap));
        
        Position twoHundred = new GeneralPosition.Builder(chr1,200).build();
        String twoHundredFromMap = geneRangeMap.get(twoHundred);
        assertTrue(twoHundredFromMap.equals("gene5-overlappedGene2"));
        
        for (Map.Entry<Range<Position>,String> entry: geneRangeMap.asMapOfRanges().entrySet()){
            Range<Position> range = entry.getKey();
            int start = range.lowerEndpoint().getPosition();
            int end = range.upperEndpoint().getPosition();
            String gene = entry.getValue();
            System.out.println(start + "," + end + " " + gene);
        }
        System.out.println("\nFinished testAddRanges!\n");       
    }
    
    // The createFlankingList class takes a map of anchor coordinates
    // and adds flanking bp's to the ends.  If there are insufficient
    // number of bps between anchors to satisfy user request, the existing
    // bp's are split between the 2 anchors.
    @Test
    public void testCreateFlankingList() throws Exception {
        // testRefGenome has 1 chromosome and a sequence with 4959 bps.
        // Create test genes based on those values
        RangeMap<Position,String> geneRangeMap = TreeRangeMap.create();
        Chromosome chr1 = new Chromosome("1");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,442).build(),
                new GeneralPosition.Builder(chr1,498).build()),"gene1");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,708).build(),
                new GeneralPosition.Builder(chr1,757).build()),"gene2");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,922).build(),
                new GeneralPosition.Builder(chr1,951).build()),"gene3");
        // ALlowing the next 2 to overlap when we flank with 100 bps
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,1216).build(),
                new GeneralPosition.Builder(chr1,1283).build()),"gene4");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,1486).build(),
                new GeneralPosition.Builder(chr1,1497).build()),"gene5");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,2221).build(),
                new GeneralPosition.Builder(chr1,2226).build()),"gene6");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,2483).build(),
                new GeneralPosition.Builder(chr1,2490).build()),"gene7");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,2869).build(),
                new GeneralPosition.Builder(chr1,2891).build()),"gene8");
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,3003).build(),
                new GeneralPosition.Builder(chr1,3057).build()),"gene9");
        // This range cannot be flanked by 100 as it hits the end of the ref chrom - will
        // verify the end is 4959 (the end of the chromosome) is 4959
        addRange(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,4904).build(),
                new GeneralPosition.Builder(chr1,4945).build()),"gene10");
        
        
        System.out.println("Last gene added, size of range: " + geneRangeMap.asMapOfRanges().size());
        assertEquals(geneRangeMap.asMapOfRanges().size(),10);
        
        // Create ref sequence. Needed for createFlankingLIst to find the length of each chromosome
        // when adding flanking to last gene.
        System.out.println("Create genome sequence ...");
        myRefSequence = GenomeSequenceBuilder.instance(refFile);
        int chromLen = myRefSequence.chromosomeSize(Chromosome.instance("chr1"));
        System.out.println("setup: chromLen = " + chromLen);
        
        System.out.println("Create gene-with-flanking map");
      
        RangeMap<Position,String> flankingGeneMap = createFlankingList( geneRangeMap,  100, myRefSequence);
        
        assertEquals(flankingGeneMap.asMapOfRanges().size(),10);
        
        // gene1 - check range based on original lower bound
        // THe getEntry call returns the range containing this position
        Position gene = new GeneralPosition.Builder(chr1,442).build();
        int geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        int geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(342, geneLowerFlank);
        assertEquals(598,geneUpperFlank);
        
        //gene2
        // there is not 200 bps between the end of gene2 and start of gene3, so the code
        // splits the difference when flanking, which gets the end here as 839 and the
        // beginning of gene 3 (flanked) as 840)
        gene = new GeneralPosition.Builder(chr1,708).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(608,geneLowerFlank);
        assertEquals(839,geneUpperFlank);

        //gene3
        gene = new GeneralPosition.Builder(chr1,922).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(841,geneLowerFlank);
        assertEquals(1051, geneUpperFlank);
        
        // gene4
        gene = new GeneralPosition.Builder(chr1,1216).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(1116,geneLowerFlank);
        assertEquals(1383, geneUpperFlank);
        
        // gene5
        gene = new GeneralPosition.Builder(chr1,1486).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(1386,geneLowerFlank);
        assertEquals(1597,geneUpperFlank);
        
        // gene6 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,2226).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(2121,geneLowerFlank);
        assertEquals(2326,geneUpperFlank);
        
        // gene7 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,2490).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(2383,geneLowerFlank);
        assertEquals(2590,geneUpperFlank);
   
        // gene8 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,2891).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(2769,geneLowerFlank);
        assertEquals(2947,geneUpperFlank);
        
        // gene9 - checking with original upperbound now
        // another overlap.  splitting the difference between the previous gene and this one,
        // these was only 112 bps between the two
        gene = new GeneralPosition.Builder(chr1,3057).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(2948,geneLowerFlank);
        assertEquals(3157,geneUpperFlank);
        
        // gene10 - checking with a value from the middle of the original range
        // This range cannot be flanked by 100 as it hits the end of the ref chrom - will
        // verify the end is 4959 (the end of the chromosome) is 4959
        gene = new GeneralPosition.Builder(chr1,4950).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(4804,geneLowerFlank);
        assertEquals(4959,geneUpperFlank);
        
        System.out.println("\nFinished testCreateFlankingList!\n"); 
    }

    public static void createTestRefGenome(String refFasta) {

        try (BufferedWriter bw = Utils.getBufferedWriter(refFasta)) {
            // Create the file we're going to verify
            bw.write(">chr1\n");

            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n");
            bw.write("GTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAAACCATTTCTTGCTTAAAAACTCGTACGTGTTAGCCTTCG\n");
            bw.write("GTATTATTGAAAATGGTCATTCATGGCTATTTTTCGGCAAAATGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTC\n");
            bw.write("ATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAGAAA\n");
            bw.write("TCCGTAGGTGTTAGCCTTCGATATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCAT\n");
            bw.write("TGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGA\n");
            bw.write("GACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCATTCATGGCTATTTTCGACAAA\n");
            bw.write("AATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCAC\n");
            bw.write("ATTCTTGGATTTATGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCAT\n");
            bw.write("TCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCATAGGCTCATACACCTCACCCCACATAT\n");
            bw.write("GTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTCTTAGCCTCTA\n");
            bw.write("ATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTC\n");
            bw.write("ATAAATCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGACCATTTCTTGGTCAAAAA\n");
            bw.write("CTCGTACGTGTTAGCCTTTGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGGCAAAATGGGGGTTGTGTGGCCATT\n");
            bw.write("GATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTATCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAA\n");
            bw.write("ACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAA\n");
            bw.write("TGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACAT\n");
            bw.write("TCTTGAATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGAGTTAGCCTTCGATATTATTGAAAATGGTCGTTC\n");
            bw.write("ATGGCCATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGT\n");
            bw.write("TTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGT\n");
            bw.write("ATTATTGTAAATGGTCATTCATGGCTATTTTCGACAACAATGTGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCAT\n");
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATA\n");
            bw.write("CGTAGGTCTTAGCCTCTAATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTT\n");
            bw.write("ATCATTGACTAGAGGCTCATAAATCTCACCCCTCATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGA\n");
            bw.write("CCATTTCTTGGTCAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAAT\n");
            bw.write("GGGGGTTCTGTGGCCATTGATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATT\n");
            bw.write("CTTGGATTTCTGGTGGAGACCATTTCTTGGGCAAAAATCCGAATGTGTTAGCCTTTGGTATTTTTGAAAATGGTCATTCA\n");
            bw.write("TGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCTTCGACCAGAGGCTCATACACCTCACTACACATATGTT\n");
            bw.write("TCCTTGCCATAGATCACATTCTTGGATTTATGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCAGTG\n");
            bw.write("TCATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAAATTAGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCAT\n");
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATC\n");
            bw.write("CGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTG\n");
            bw.write("ATCATCGACCAGACGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTTGTGGAGA\n");
            bw.write("CCATTTCTTGGTCTAAAATCCGTAGGTGTTAGCCTCTAGTATTATTGAAAATGGTCGCTCATGGCTATTTTCAAGGTCGC\n");
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n");
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATC\n");
            bw.write("CGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTG\n");
            bw.write("ATCATCGACCAGACGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTTGTGGAGA\n");
            bw.write("CCATTTCTTGGTCTAAAATCCGTAGGTGTTAGCCTCTAGTATTATTGAAAATGGTCGCTCATGGCTATTTTCAAGGTCGC\n");
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n");
            bw.write("GTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCG\n");
            bw.write("GTATTATTGAAAATGGTCGTTCATGGCTATTTTCGGCAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCA\n");
            bw.write("TAAACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAAT\n");
            bw.write("CCGTACGTGTTTGCCTTTGGTATTTTTGAAAATGTTTATTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATT\n");
            bw.write("GATCTTCGACCAGAGGCTCATACACCTCACTACACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAG\n");
            bw.write("ACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCAGTGTCATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAA\n");
            bw.write("ATTAGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACA\n");
            bw.write("TTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTT\n");
            bw.write("CATGGCTATTTTCGACAAAAATGAGGGTTGTGTGGCCATTGATCATCGACCAGACGCTCATACACCTCACCCCACATATG\n");
            bw.write("TTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGTTCTAAAATCCGTAGGTGTTATCCTCTAG\n");
            bw.write("TATTATTGAAAATGGTCGTTCATGGCTATTTTGAAGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCA\n");
            bw.write("TTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATATGTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGG\n");
            bw.write("AAACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTTCGGCA\n");
            bw.write("AAATGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCA\n");
            bw.write("CATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGATATTATTGAAAATGGTCG\n");
            bw.write("TTCATGGCTATTTTCGGCAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATA\n");
            bw.write("TGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTC\n");
            bw.write("GGTATTATTGTAAATGGTCATTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCT\n");
            bw.write("CATACACCTCACCCCACATATGTTTCCTTGCCGTAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAA\n");
            bw.write("ATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTGCGACAAAAATTAGGGTTGTGTGGCCA\n");
            bw.write("TTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTG\n");
            bw.write("AGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAA\n");
            bw.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
