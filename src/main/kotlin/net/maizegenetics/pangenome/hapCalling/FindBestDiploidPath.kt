package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Multimap
import net.maizegenetics.analysis.imputation.EmissionProbability
import net.maizegenetics.analysis.imputation.TransitionProbability
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import org.apache.commons.math3.distribution.BinomialDistribution
import org.apache.logging.log4j.LogManager

import java.util.*

/**
 * Used to find the most likely pair of paths through a graph given a set of read mappings.
 * The two paths represent the phased haplotypes imputed from the reads.
 * @param [myGraph] the HaplotypeGraph to be used for path finding
 * @param [readHapids] a Multimap of [ReferenceRange] -> [HapIdSetCount] retrieved from a PHG DB
 * @param [probabilityCorrect] the probability that a read has aligned correctly
 * @param [minTransitionProbability] the minimum transition probability
 * @param [maxNodesPerRange] ranges with more nodes will not be used
 * @param [minReadsPerRange] ranges with fewer reads will not be used
 * @param [removeRangesWithEqualCounts] ranges for which all hapid sets have the same count will not be used
 * @param [maxReadsPerKB] ranges with more reads per kb will not be used
 * @param [splitNodes]  should nodes with more than one taxon be split into nodes of one taxon each
 * @param [splitTransitionProb] the probability of a node given that the previous node was the same taxon or 1 - probability of a recombination
 */
class DiploidCountsToPath(val myGraph: HaplotypeGraph,
                          val readHapids: Multimap<ReferenceRange, HapIdSetCount>,
                          val probabilityCorrect: Double = 0.99,
                          val minTransitionProbability: Double = 0.001,
                          val maxNodesPerRange: Int = 11,
                          val minReadsPerRange: Int = 1,
                          val removeRangesWithEqualCounts: Boolean = false,
                          val maxReadsPerKB: Int = 100,
                          val splitNodes: Boolean = false,
                          val splitTransitionProb: Double = 0.99) {

    val myLogger = LogManager.getLogger(DiploidCountsToPath::class.java)

    /**
     * Finds the most likely diploid path through myGraph given readHapids.
     * Before the path is imputed, the graph is filtered based on [maxNodesPerRange], [minReadsPerRange],
     * [removeRangesWithEqualCounts], and [maxReadsPerKB] then missing sequence nodes are added and, finally,
     * the nodes are split into individual taxa if [splitNodes] is true.
     */
    fun getDiploidPath(): List<List<HaplotypeNode>> {

        val pathList: List<MutableList<HaplotypeNode>> = listOf(ArrayList<HaplotypeNode>(), ArrayList<HaplotypeNode>())
        val filteredGraph = filteredGraph()

        //deal with splitTaxa here
        //need to add nodes for missing haplotypes before splitting taxa
        //if not splitting just add missing sequence nodes
        val graphForPathFinding = if (splitNodes)
            CreateGraphUtils.nodesSplitByIndividualTaxa(CreateGraphUtils.addMissingSequenceNodes(filteredGraph), splitTransitionProb)
        else CreateGraphUtils.addMissingSequenceNodes(filteredGraph)

        //run the Viterbi algorithm for each chromosome
        for (chr in graphForPathFinding.chromosomes()) {
            val rangeToNodes = graphForPathFinding.tree(chr)

            val emissionProbability = DiploidEmissionProbability(rangeToNodes, readHapids, probabilityCorrect)
            val nodeList = ArrayList<List<HaplotypeNode>>(rangeToNodes.values)
            val transitionProbability = DiploidTransitionProbability(graphForPathFinding, nodeList, minTransitionProbability)
            val numberOfRanges = nodeList.size
            val probStartNode: DoubleArray = startProbabilities(nodeList[0])
            val viterbi = ViterbiForDiploidPath(numberOfRanges, transitionProbability, emissionProbability, probStartNode)
            viterbi.calculate()

            val mostProbableNodes: IntArray = viterbi.getMostProbableIntegerStateSequence()

            //decode the states
            for (ndx in mostProbableNodes.indices) {
                val nodes = nodeList.get(ndx)
                val nodeIndexPair = decodeState(mostProbableNodes[ndx].toInt(), nodes.size)
                pathList.get(0).add(nodes.get(nodeIndexPair.first))
                pathList.get(1).add(nodes.get(nodeIndexPair.second))
            }
        }
        return pathList
    }

    private fun decodeState(state: Int, numberOfNodes: Int): Pair<Int, Int> = Pair(state / numberOfNodes, state % numberOfNodes)

    /**
     * Filters myGraph using readHapids based on [maxNodesPerRange], [minReadsPerRange],
     * [removeRangesWithEqualCounts], and [maxReadsPerKB]
     */
    fun filteredGraph(): HaplotypeGraph {
        //count ranges that will not pass filters
        var maxNodesCount = 0
        var equalCountsCount = 0
        var maxReadsCount = 0
        var minReadsCount = 0
        var countOfRangesWithNoReads = 0;
        myGraph.referenceRanges().forEach {
            val myReads = readHapids.get(it)
            val numberOfReads = myReads.map { it.count }.sum()
            if(numberOfReads < minReadsPerRange) minReadsCount++
            if (numberOfReads == 0) countOfRangesWithNoReads++;
            if (numberOfReads > 0) {
                val readsPerKB = numberOfReads / (it.end() - it.start() + 1).toDouble() * 1000.0
                if (readsPerKB > maxReadsPerKB) maxReadsCount++
                if (myGraph.nodes(it).size > maxNodesPerRange) maxNodesCount++
                if (removeRangesWithEqualCounts) {
                    val firstCount = myReads.iterator().next().count
                    if (!myReads.any { it.count != firstCount }) equalCountsCount++
                }
            }
        }
        myLogger.info("${maxNodesCount} ranges have more than max nodes.")
        myLogger.info("${equalCountsCount} ranges have nodes with equal counts")
        myLogger.info("${maxReadsCount} ranges have too many reads per kb")
        myLogger.info("${minReadsCount} ranges have too few reads.")
        myLogger.info("${countOfRangesWithNoReads} ranges have 0 reads.")

        //create a list of ranges to be removed from myGraph
        val rangesToRemove = myGraph.referenceRanges().filter {
            val myReads = readHapids.get(it)
            val numberOfReads = myReads.map { it.count }.sum()
            if (numberOfReads == 0) {
                if (minReadsPerRange < 1) false else true
            } else {
                val readsPerKB = numberOfReads / (it.end() - it.start() + 1).toDouble() * 1000.0

            //set hasEqualCounts to false when numberOfReads == 0
            //do not want to remove 0 read ranges when minReads = 0
            val hasEqualCounts = if (numberOfReads == 0 || !removeRangesWithEqualCounts) false
            else {
                val firstCount = myReads.iterator().next().count
                myReads.all{it.count == firstCount}
            }

                //the filter criteria: if all of the conditions are true, the range will be retained
                myGraph.nodes(it).size > maxNodesPerRange || hasEqualCounts
                        || readsPerKB > maxReadsPerKB || numberOfReads < minReadsPerRange
            }
        }.toList()
        myLogger.info("In filter graph, ${rangesToRemove.size} ranges will be removed")
        return CreateGraphUtils.removeRefRanges(myGraph, rangesToRemove)
    }
}

/**
 * Used to calculate the emission probability of a pair of nodes given a set of read mappings.
 * @param rangeToNodes          a map of [ReferenceRange] -> list of [HaplotypeNode]
 * @param readHapids            a Multimap of [ReferenceRange] -> [HapIdSetCount], the read mappings
 * @param probabilityCorrect    the probability that a read was aligned to the right place
 */
class DiploidEmissionProbability(val rangeToNodes: NavigableMap<ReferenceRange, List<HaplotypeNode>>,
                                 val readHapids: Multimap<ReferenceRange, HapIdSetCount>,
                                 val probabilityCorrect: Double) : EmissionProbability() {

    private var myCurrentRangeIndex: Int = -1
    private var rangeProbabilities: DoubleArray = DoubleArray(0)
    private val myRanges: List<ReferenceRange> = rangeToNodes.keys.toList()

    /**
     * Returns the emission probability for a given state index and range index.
     * The range index is the zero-based index into the ordered list of ranges for a HaplotypeGraph.
     * The state index is the zero-based index into an ordered list of states, where each state is an ordered pair
     * of HaplotypeNodes returned by the method [nodePairs]. The emission probability for a given node pair is calculated
     * by the method [nodePairProbabilityList].
     */
    override fun getProbObsGivenState(state: Int, rangeIndex: Int): Double {
        if (myCurrentRangeIndex != rangeIndex) {
            myCurrentRangeIndex = rangeIndex
            assignRangeProbabilities()
        }
        return rangeProbabilities[state]
    }

    /**
     * Calls getProbObsGivenState(state, rangeIndex) where rangeIndex = node.
     * The value of obs is not used.
     */
    override fun getProbObsGivenState(state: Int, obs: Int, node: Int): Double {
        //ignores value of obs
        return getProbObsGivenState(state, node)
    }

    private fun assignRangeProbabilities() {
        val myCurrentRange: ReferenceRange = myRanges.get(myCurrentRangeIndex)
        val currentNodes: List<HaplotypeNode> = rangeToNodes.get(myCurrentRange)
                ?: throw IllegalArgumentException("Range at chr ${myCurrentRange.chromosome().name}, pos ${myCurrentRange.start()} has no nodes.")

        //get the list of read node matches for this reference range
        val currentReadMatches = readHapids.get(myCurrentRange)

        //convert that list to counts as Long -> Int, where long is two hapids encoded
        val nodePairCounts: Map<Long, Int> = countsFromHapidList(currentReadMatches)
        //get the list of node pairs
        val myNodePairs = nodePairs(currentNodes)
        val totalCount = nodePairCounts.getOrDefault(-1, 0)

        //if total count = 0, assign probability of 1/n to all nodes
        // otherwise, use the method nodePairProbabilityList(...)
        if (totalCount == 0) {
            val prob = 1.0 / myNodePairs.size.toDouble()
            rangeProbabilities = DoubleArray(myNodePairs.size, { i -> prob })
        } else {
            val nodeProbList = nodePairProbabilityList(currentNodes, currentReadMatches.toList(), probabilityCorrect)
            rangeProbabilities = nodeProbList.toDoubleArray()
        }
    }

}


/**
 * Calculates the probability of a specific node pair given the previous node pair
 * @param [myGraph] the HaplotypeGraph used for imputation
 * @param [nodeList] an list containing for each [ReferenceRange] a list of HaplotypeNodes
 * @param [minTransitionProbability] the minimum transition probability that will be returned
 */
class DiploidTransitionProbability(val myGraph: HaplotypeGraph,
                                   val nodeList: ArrayList<List<HaplotypeNode>>,
                                   val minTransitionProbability: Double) : TransitionProbability() {
    var currentNode: Int = 1;
    var nodePairList: List<Pair<HaplotypeNode, HaplotypeNode>> = nodePairs(nodeList.get(1))
    var previousNodePairList: List<Pair<HaplotypeNode, HaplotypeNode>> = nodePairs(nodeList.get(0))

    /**
     * sets the current reference range
     */
    override fun setNode(node: Int) {
        //The term node as used here is confusing. The transition probability interface uses node to mean the position or
        // (or time) in the markov chain. This corresponds to the reference range index. The first reference range (or node)
        // index is 0. The current node must have a previous node. As a result, the current node must be at least one.
        // The expression nodeList.get(node) refers to the the list of HaplotypeNodes at reference range <node>.
        if (node < 1) throw java.lang.IllegalArgumentException("Cannot set node to a value less than 1")
        if (node != currentNode) {
            if (node == currentNode + 1) {
                previousNodePairList = nodePairList
                nodePairList = nodePairs(nodeList.get(node))
            } else {
                previousNodePairList = nodePairs(nodeList.get(node - 1))
                nodePairList = nodePairs(nodeList.get(node))
            }
            currentNode = node
        }

    }

    override fun getNumberOfStates(): Int {
        return nodePairList.size
    }

    override fun getTransitionProbability(state1: Int, state2: Int): Double {
        val nodePair1 = previousNodePairList[state1]
        val nodePair2 = nodePairList[state2]
        val firstEdge = myGraph.edge(nodePair1.first, nodePair2.first)
        val secondEdge = myGraph.edge(nodePair1.second, nodePair2.second)
        val firstProb = if (firstEdge.isPresent) firstEdge.get().edgeProbability() else 0.0
        val secondProb = if (secondEdge.isPresent) secondEdge.get().edgeProbability() else 0.0

        return maxOf(minTransitionProbability, firstProb) * maxOf(minTransitionProbability, secondProb)
    }
}

//start of package level (static) methods

/**
 * @param [hapidList] A collection of HapIdSetCount, a set of haplotype ids and a count
 *
 * @return For each possible hapid pair, the number of times it was observed in hapidList
 * Haplotype ids are integers. A pair of haplotype ids is encoded as a long.
 */
fun countsFromHapidList(hapidList: Collection<HapIdSetCount>): Map<Long, Int> {
    //an IntArray from hapidList contains all of the hap ids hit by one read
    //for each IntArray add a count for every possible pair in the list including each hapid with itself
    //each pair will only be sorted in order
    //TODO consider Fastutils Long2IntMap (implemented by Long2IntOpenHashMap)
    val pairMap: MutableMap<Long, Int> = HashMap()
    pairMap.put(-1, hapidList.map { it.count }.sum())  //this is totalCount (N)
    hapidList.forEach {
        val hapids = ArrayList<Int>(it.hapIdSet)
        val numberOfHapids = hapids.size
        val setCount = it.count
        for (i0 in 0 until numberOfHapids) {
            for (i1 in i0 until numberOfHapids) {
                val key = if (hapids[i0] <= hapids[i1]) intArrayToLong(intArrayOf(hapids[i0], hapids[i1])) else intArrayToLong(intArrayOf(hapids[i1], hapids[i0]))
                val value = pairMap.get(key)
                if (value != null) pairMap.put(key, value + setCount) else pairMap.put(key, setCount)
            }
        }
    }
    return pairMap
}

/**
 * @param [intPair] an IntArray holding a pair of integers
 * @return    a Long that combines (encodes) the pair of integers in [intPair]
 */
fun intArrayToLong(intPair: IntArray): Long {
    return intPair[0].toLong().shl(32) + intPair[1].toLong()
}

/**
 * @param [myLong] a long
 * @return   the pair of integers encoded by [myLong]
 */
fun longToIntArray(myLong: Long): IntArray {
    return intArrayOf(myLong.shr(32).toInt(), (myLong and java.lang.Long.parseLong("ffffffff", 16)).toInt())
}

/**
 * Generates a list of all possible node pairs a list of longs
 * @param [nodeList] a list of HaplotypeNodes
 * @return   a list of all possible pairs of HaplotypeNodes
 */
fun nodePairs(nodeList: List<HaplotypeNode>): List<Pair<HaplotypeNode, HaplotypeNode>> {
    return nodeList.flatMap { first -> nodeList.map { Pair(first, it) } }
}

/**
 * For each pair of HaplotypeNodes in nodePairs, the method calculates the probability of observing the nodePairCounts
 * assuming they were generated by that pair of nodes.
 *
 * When both nodes in the pair are the same node, the probability is calculated from a binomial distribution.
 * Let nTotal be the total number of reads and nPair, be the number of reads hitting the node under consideration.
 * Then P(counts | nodePair) = binomial(trials = nTotal, successes = nPair, probabilityCorrect)
 *
 * When the pair of nodes are not the same, represent the node pair as (A,B). Let nAnotB be the number of reads that hit A
 * but not B, nBnotA be the number of nodes that hit B but not A, nAB be the number of reads that hit both A and B, and
 * nNotAB be the number of reads hitting neither A nor B. Also let pC/2 = probabilityCorrect/2 and pErr = 1 - probabilityCorrect.
 * Then calculate the probability P(counts | nodePair) as
 * sum from n = 0 to nAB of multinomial{counts = (nAnotB + n, nBnotA + nAB - n, nNotAB), probabilities = (pC/2, pC/2, pErr)}
 */
fun nodePairProbabilityList(nodeList : List<HaplotypeNode>, hapidSetCounts : List<HapIdSetCount>, probabilityCorrect : Double): List<Double> {
    require(nodeList.size > 0) {"nodeList of size zero"}
    val halfProb = probabilityCorrect / 2
    val pErr = 1 - probabilityCorrect

    //hapidSetCounts might have hapid sets that do not contain any of the haplotype ids in the node list
    //Since the objective is to choose the best pair of hapids in the node list, haplotype sets that do not contain any
    //of those nodes are not informative
    val nodeHapids = nodeList.map { it.id() }
    val filteredSetCounts = hapidSetCounts.filter { it.hapIdSet.intersect(nodeHapids).size > 0 }

    //get a list of node pairs
    val nodePairList = nodePairs(nodeList)

    return if (nodeList.size == 1) {
        listOf(1.0)
    } else {
        nodePairList.map { nodePr ->

            if (nodePr.first.id() == nodePr.second.id()) {
                val totalCount = filteredSetCounts.map { it.count }.sum()
                val firstCount = filteredSetCounts.filter { it.hapIdSet.contains(nodePr.first.id()) }.map { it.count }.sum()
                BinomialDistribution(totalCount, probabilityCorrect).probability(firstCount)
            } else {
                val firstNotSecondCount = filteredSetCounts.filter { it.hapIdSet.contains(nodePr.first.id()) }
                    .filterNot { it.hapIdSet.contains(nodePr.second.id()) }
                    .map { it.count }.sum()
                val secondNotFirstCount = filteredSetCounts.filter { it.hapIdSet.contains(nodePr.second.id()) }
                    .filterNot { it.hapIdSet.contains(nodePr.first.id()) }
                    .map { it.count }.sum()
                val firstAndSecondCount = filteredSetCounts.filter { it.hapIdSet.contains(nodePr.first.id()) }
                    .filter { it.hapIdSet.contains(nodePr.second.id()) }
                    .map { it.count }.sum()
                val neitherFirstNorSecondCount = filteredSetCounts.filterNot { it.hapIdSet.contains(nodePr.first.id()) }
                    .filterNot { it.hapIdSet.contains(nodePr.second.id()) }
                    .map { it.count }.sum()

                (0..firstAndSecondCount).map { multinomialProbability(intArrayOf(firstNotSecondCount + it, secondNotFirstCount + firstAndSecondCount - it, neitherFirstNorSecondCount),
                    doubleArrayOf(halfProb, halfProb, pErr))}.sum()
            }

        }
    }

}

/**
 * @param [nodeList]
 * @return   a DoubleArray containing the probabilities for each of a group of HaplotypeNodes.
 * Since there is no prior information about the nodes, the probability of each
 * node equal 1/(number of nodes)
 */
fun startProbabilities(nodeList: List<HaplotypeNode>): DoubleArray {
    val nNodes = nodeList.size * nodeList.size
    val prob = 1.0 / (nNodes.toDouble())
    return DoubleArray(nNodes, { prob })
}

/**
 * @param [counts] The counts of each of set of classes
 * @param [probabilities] The probability of each class
 * @return The probability of an array of counts of some classes given the probability of each class,
 * The size of the counts array and the probabilities array are expected to be equal.
 */
fun multinomialProbability(counts: IntArray, probabilities: DoubleArray): Double {
    if (counts.size != probabilities.size) throw java.lang.IllegalArgumentException("multinomialProbability error: counts and probabilities arrays do not have the same size.")
    val N = counts.sum()

    val logprod = (0 until counts.size).map { counts[it] * Math.log(probabilities[it]) }.sum()
    val numerator = logFactorial(N)
    val denom = counts.map { logFactorial(it) }.sum()
    val logprob = numerator - denom + logprod
    return Math.pow(Math.E, logprob)

}

//factorials of 0 to 10
val smallFactorials: DoubleArray = doubleArrayOf(1.0, 1.0, 2.0, 6.0, 24.0, 120.0, 720.0, 5040.0, 40320.0, 362880.0, 3628800.0).map { Math.log(it) }.toDoubleArray()

/**
 *  Calculates the log factorial of any positive integer using the exact value for 0 to 10 and
 *  Stirlings approximation for integers greater than 10. The formula is taken from the
 *  Wikipedia article for Stirlings approximation
 *  @param [intval] an integer
 *  @return   the natural log of the factorial of intval
 */
fun logFactorial(intval: Int): Double {
    if (intval <= 10) return smallFactorials[intval] else {
        val n = intval.toDouble()
        return n * Math.log(n) + 0.5 * Math.log(2.0 * Math.PI * n) - n
    }
}

fun memoryUse(): String {
    val rt = Runtime.getRuntime()
    for (i in 1..3) rt.gc()
    val freeMem = rt.freeMemory()
    val totalMem = rt.totalMemory()
    val usedMem = totalMem - freeMem

    return "Memory status: total = $totalMem, free = $freeMem, used = $usedMem"
}

/**
 * Calculates the most probable state sequence given a set of observations using the Viterbi algorithm
 * described in Rabiner (1989) Proceedings of the IEEE 77(2):257-286
 * @param [numberOfObservations] The number of reference ranges (positions) being imputed
 * @param [myTransitionMatrix] a [TransitionProbability]
 * @param [probObservationGivenState] an [EmissionProbability]
 * @param [pTrue] the initial state probabilities for the states at the first reference range (position)
 */
class ViterbiForDiploidPath(val numberOfObservations: Int, val myTransitionMatrix: TransitionProbability, val probObservationGivenState: EmissionProbability, val pTrue: DoubleArray) {

    //adapted from Rabiner (1989) Proceedings of the IEEE 77(2):257-286
    //initialize
    //d(0, i) = p(Obs-0|S(i)*p(S(i))
    //where d0 = path length, Obs-0 = observation 0, S0 = true state 0
    //iterate:
    //for t = 1 to n
    //d(t,S(j)) = max(j){d(t-1,S(i)) * p[S(j)|S(i)] * p[Obs(t)|S(j)]}
    //h(t, j) = the value of i that maximizes distance
    //where h() = path history
    //termination
    //choose state that maximizes path length
    //back tracking
    //S(t) = h(t+1, S(t+1)), to decode best sequence

    internal var history: ArrayList<IntArray>
    internal var distance: DoubleArray
    internal var probTrueStates: DoubleArray //ln of probabilities
    internal var numberOfCurrentNodeStates: Int = 0
    internal var numberOfPreviousNodeStates: Int = 0

    init {
        probTrueStates = DoubleArray(pTrue.size)
        for (i in pTrue.indices) {
            probTrueStates[i] = Math.log(pTrue[i])
        }
        history = ArrayList(numberOfObservations)
        distance = DoubleArray(probTrueStates.size)
    }

    fun calculate() {
        initialize()
        for (i in 1 until numberOfObservations) {
            updateDistanceAndHistory(i)
        }
    }

    fun initialize() {
        val n = probTrueStates.size
        numberOfPreviousNodeStates = n
        numberOfCurrentNodeStates = numberOfPreviousNodeStates
        distance = DoubleArray(n)
        for (i in 0 until n) {
            try {
                distance[i] = probObservationGivenState.getLnProbObsGivenState(i, 0, 0) + probTrueStates[i]
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    private fun updateDistanceAndHistory(node: Int) {

        myTransitionMatrix.setNode(node)
        numberOfPreviousNodeStates = numberOfCurrentNodeStates
        numberOfCurrentNodeStates = myTransitionMatrix.numberOfStates
        val candidateDistance = Array(numberOfPreviousNodeStates) { DoubleArray(numberOfCurrentNodeStates) }

        val distanceLength = distance.size

        try { //this try block for debugging
            for (i in 0 until numberOfPreviousNodeStates) { //this is the number of nodes for the previous anchor
                for (j in 0 until numberOfCurrentNodeStates) {
                    candidateDistance[i][j] = distance[i] + myTransitionMatrix.getLnTransitionProbability(i, j) + probObservationGivenState.getLnProbObsGivenState(j, 0, node)
                }
            }
        } catch (e: Exception) {
            println(String.format("at node %d, distance.length = %d, numberOfPreviousNodeStates = %d, and numberOfCurrentNodeStates = %d",
                    node, distance.size, numberOfPreviousNodeStates, numberOfCurrentNodeStates))
            throw RuntimeException(e)
        }

        //find the maxima
        val max = IntArray(numberOfCurrentNodeStates)
        for (i in 0 until numberOfPreviousNodeStates) {
            for (j in 0 until numberOfCurrentNodeStates) {
                if (candidateDistance[i][j] > candidateDistance[max[j]][j]) max[j] = i
            }
        }

        //update distance and history
        distance = DoubleArray(numberOfCurrentNodeStates)
        val nodeHistory = IntArray(numberOfCurrentNodeStates)
        history.add(nodeHistory)
        for (j in 0 until numberOfCurrentNodeStates) {
            distance[j] = candidateDistance[max[j]][j]
            nodeHistory[j] = max[j]
        }

        //if the min distance is less than -1e100, subtract the min distance;
        var maxd = distance[0]
        var mind = 0.0
        for (i in 0 until numberOfCurrentNodeStates) {
            if (distance[i] > maxd) maxd = distance[i]
            if (distance[i] != java.lang.Double.NEGATIVE_INFINITY && distance[i] < mind) mind = distance[i]
        }
        if (mind < -1e100) {
            for (i in 0 until numberOfCurrentNodeStates) {
                distance[i] -= maxd
            }
        }
    }

    /**
     *
     * @return    an int array representing the sequence of most likely states given the data
     */
    fun getMostProbableIntegerStateSequence(): IntArray {
        val seq = IntArray(numberOfObservations)
        var finalState: Int = 0

        for (i in 1 until distance.size) {
            if (distance[i] > distance[finalState]) finalState = i
        }

        //S(t) = h(t+1, S(t+1)), to decode best sequence
        seq[numberOfObservations - 1] = finalState
        for (i in numberOfObservations - 2 downTo 0) {

            try {
                seq[i] = history[i][seq[i + 1]]
            } catch (e: java.lang.Exception) {
                println("Error when i = $i and seq[i+1] = ${seq[i + 1]}, numberOfObservations = $numberOfObservations")
                throw java.lang.RuntimeException(e)
            }
        }
        return seq
    }

    fun setStateProbability(probTrueState: DoubleArray) {
        val n = probTrueState.size
        probTrueStates = DoubleArray(n)
        for (i in 0 until n) {
            probTrueStates[i] = Math.log(probTrueState[i])
        }
    }
}

