package net.maizegenetics.pangenome.api;

/**
 * @author Terry Casstevens Created June 19, 2017
 * @author Zack Miller
 * @author Karl Kremling
 * @author Jacob Washburn
 *
 * Class for edges in pratical haplotype graph.  Edges contain no sequence data. They simply connect one HaplotypeNode
 * to another and have a probability that the rightHapNode of the current edge is the next HaplotypeNode in the desired
 * path when moving from left to right in the graph.
 */
public class HaplotypeEdge {

    private final HaplotypeNode myLeftHapNode;
    private final HaplotypeNode myRightHapNode;
    private final double myEdgeProbability;

    /**
     * Constructor
     *
     * @param leftHapNode left node
     * @param rightHapNode right node
     * @param edgeProbability probability right node is next in sequence starting from left node
     */
    public HaplotypeEdge(HaplotypeNode leftHapNode, HaplotypeNode rightHapNode, double edgeProbability) {

        myLeftHapNode = leftHapNode;
        myRightHapNode = rightHapNode;
        if (myLeftHapNode.referenceRange().compareTo(myRightHapNode.referenceRange()) >= 0) {
            throw new IllegalArgumentException("HaplotypeEdge: init: left node's reference range should come before right node's reference range.\n" +
                    myLeftHapNode + "\n" + myRightHapNode);
        }
        if (!myLeftHapNode.referenceRange().chromosome().equals(myRightHapNode.referenceRange().chromosome())) {
            throw new IllegalArgumentException("HaplotypeEdge: init: left node's reference range should be the same chromosome as right node's reference range.\n" +
                    myLeftHapNode + "\n" + myRightHapNode);
        }
        if (edgeProbability <= 0.0 || edgeProbability > 1.0) {
            throw new IllegalArgumentException("HaplotypeEdge: init: edge probability out of range: " + edgeProbability + "\n" +
                    myLeftHapNode + "\n" + myRightHapNode);
        }
        myEdgeProbability = edgeProbability;
    }

    /**
     * HaplotypeNode to the left of the current edge.
     */
    public HaplotypeNode leftHapNode() {
        return myLeftHapNode;
    }

    /**
     * HaplotypeNode to the right of the current edge
     */
    public HaplotypeNode rightHapNode() {
        return myRightHapNode;
    }

    /**
     * Probability that the node to the right of this edge {@link #rightHapNode()} is the next HaplotypeNode when
     * traversing the graph from left to right. This value has no biological meaning when traversing from right to
     * left.
     */
    public double edgeProbability() {
        return myEdgeProbability;
    }

    @Override
    public String toString() {
        return "HaplotypeEdge{" +
                "myLeftHapNode=" + myLeftHapNode +
                ", myRightHapNode=" + myRightHapNode +
                ", myEdgeProbability=" + myEdgeProbability +
                '}';
    }
}
