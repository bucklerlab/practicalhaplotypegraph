package net.maizegenetics.pangenome.gvcfFiltering;

import net.maizegenetics.util.LoggingUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class FilterGVCFSingleFilePluginTest {
    String testingDir = System.getProperty("user.home") + "/temp/FilterGVCFSingleFileTest/";
    String inputFile = "data/simpleGVCFTestFile.g.vcf";
    String allPassFile = "data/simpleGVCFTestFile_allPass.g.vcf.gz";
    String over5PassFile = "data/simpleGVCFTestFile_DP_over_5.g.vcf.gz";

    String blankConfig = testingDir + "/config_blank.txt";
    String filterConfig = testingDir + "/config_filter.txt";

    @Before
    public void setup() {
        LoggingUtils.setupDebugLogging();

        File testDir = new File(testingDir);

        try {
            for(File file: testDir.listFiles()) {
                file.delete();
            }

            testDir.delete();


        } catch (Exception e) { System.out.println("Could not delete testing directory"); }

        testDir.mkdirs();

        // make config files
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(blankConfig, false));
            writer.write("# this file intentionally left empty\n");
            writer.close();

            BufferedWriter writer2 = new BufferedWriter(new FileWriter(filterConfig, false));
            writer2.write("DP_min=6\n");
            writer2.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testAllRecordsPass() {
        FilterGVCFSingleFilePlugin plugin = new FilterGVCFSingleFilePlugin(null, false)
                .inputFile(inputFile)
                .outputFile(testingDir + "/simpleGVCFTestFile_allPass.g.vcf")
                .configFile(blankConfig);

        plugin.processData(null);

        Assert.assertTrue(compareVCFContents(allPassFile, testingDir + "/simpleGVCFTestFile_allPass.g.vcf.gz"));
    }

    @Test
    public void testSimpleFilter() {
        FilterGVCFSingleFilePlugin plugin = new FilterGVCFSingleFilePlugin(null, false)
                .inputFile(inputFile)
                .outputFile(testingDir + "/simpleGVCFTestFile_DP_over_5.g.vcf")
                .configFile(filterConfig);

        plugin.processData(null);

        Assert.assertTrue(compareVCFContents(over5PassFile, testingDir + "/simpleGVCFTestFile_DP_over_5.g.vcf.gz"));
    }

    /* Compares non-header lines of two vcf files. Order and contents must be identical to return true. */
    private boolean compareVCFContents(String file1, String file2) {
        try(BufferedReader reader1 = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file1))));
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file2))))) {

            String line1 = reader1.readLine();
            String line2 = reader2.readLine();

            // skip headers
            // note: any comment lines past the headers will be considered for matching
            while(line1.startsWith("#")) {
                line1 = reader1.readLine();
            }
            while(line2.startsWith("#")) {
                line2 = reader2.readLine();
            }

            // now, compare files line by line
            while((line1 = reader1.readLine()) != null) {
                line2 = reader2.readLine();
                if(!line1.equals(line2)) {
                    System.out.println("Line difference found:");
                    System.out.println("file 1: " + line1);
                    System.out.println("file 2: " + line2);
                    return false;
                }
            }

            // also make sure they end at the same point
            if(reader2.readLine() == null) {
                return true;
            } else {
                System.out.println("Files do not end at the same line.");
                return false;
            }

        } catch (Exception e) {
            System.out.println("Exception reading file: " + e.getMessage() );
            return false;
        }

    }

}
