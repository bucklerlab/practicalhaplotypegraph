package net.maizegenetics.pangenome.hapcollapse;

/**
 * Simple class to run the GVCFTyperPlugin until it gets integrated into TASSEL proper.
 * Will be deleted once the plugin is in TASSEL as it can run on run_pipeline.sh.
 * Created by zrm22 on 8/2/17.
 */
public class RunGVCFTyper {

    public static void main(String[] args) {
        //Test runner to make sure the command is correct
//        new GVCFTyperPlugin(null,false)
//                .inputGVCFDir("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/GVCFTyperPluginTests/InputGVCFs/")
//                .inputIntervalFile("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/GVCFTyperPluginTests/fiveInterval.intervals")
//                .referenceFile("/Volumes/ZackBackup/Temp/Pangenome/RefUsedForPHG/Zea_mays.AGPv4.dna.toplevel.fa.gz")
//                .numThreads(20)
//                .outputVCFDir("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/GVCFTyperPluginTests/OutputGVCFs/")
//                .emitModeParam(GVCFTyperPlugin.EMIT_MODE.confident)
//                .processData(null);



        new GVCFTyperPlugin(null,false)
                .inputGVCFDir(args[0])
                .inputIntervalFile(args[1])
                .referenceFile(args[2])
                .numThreads(Integer.parseInt(args[3]))
                .outputVCFDir(args[4])
                .emitModeParam(GVCFTyperPlugin.EMIT_MODE.confident)
                .processData(null);



    }
}
