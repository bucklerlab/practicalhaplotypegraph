package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.*
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.dna.snp.io.ReadBedfile
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.canVariantInfoBeAddedToRefBlockList
import net.maizegenetics.pangenome.api.convertGVCFContextToInfo
import net.maizegenetics.pangenome.api.mergeRefBlocks
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

/**
 * This class contains a plugin that will take GVCF files together with bedfile defined intervals
 * and create a metrics file showing data on the interval chrom/start/end/len, the length of this interval
 * for each taxa based on GVCF information, how much of each interval is covered by each GVCF taxa.
 *
 * The GVCF parsing is largely pulled from db_loading/LoadHaplotypesFromGVCFPlugin
 *
 * The "numBPsCovered" is defined as the length of the taxon sequence, minus the total number of insertions.
 * Insertions are determined by counting the length difference when  the VariantContext was a variant, and
 * the query_len was greater than the ref_len.  Add those bp counts together to get total insertions, subtract
 * that from the length.  Deletions do not need to be considered as they are not part of the assembly sequence length.
 *
 * The results are a tab-delimited file.  No DB access is required for this plugin
 */
class GVCFBedfileMetricPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(GVCFBedfileMetricPlugin::class.java)

    // the anchorDataMapping value is <intervalID,Pair<seqLen,numRefOverlaps>.  seqLen=0 means no mapping to this refRange
    // For the Pair<>, numRefOverlaps indicates the number of reference positions that were "matched" (not identical, but that had coverage)
    //   This is calculated as assembly sequence length - insertions
    data class ChromosomeGVCFRecord(val currentChrom : String, val currentTaxon : String,  val anchorDataMapping : Map<Int, Pair<Int,Int>>)

    private var myGVCFKeyFile = PluginParameter.Builder("gvcfKeyFile", null, String::class.java)
        .description("Keyfile used to specify the inputs to the DB.  Must have columns sample_name, files. Ordering of the columns does not matter.\n" +
                "sample_name is the name of the taxon you are uploading.\n" +
                "files is a comma-separated list of file names(without path) providing the names of the GVCF files to be uploaded. \n")
        .inFile()
        .required(true)
        .build()

    private var myGVCFDirectory = PluginParameter.Builder("gvcfDir",null, String::class.java)
        .description("Directory holding all the gvcf files.")
        .inDir()
        .required(true)
        .build()

    private var myOutputFile = PluginParameter.Builder("outputFile",null, String::class.java)
        .description("File where metrics will be written.")
        .outFile()
        .required(true)
        .build()

    private var myReference = PluginParameter.Builder("reference", null, String::class.java)
        .description("Input Reference Fasta")
        .required(true)
        .inFile()
        .build()

    private var bedFile = PluginParameter.Builder("bedFile", null, String::class.java)
        .description("File holding the Interval Range Information")
        .required(true)
        .inFile()
        .build()

    private var myNumThreads = PluginParameter.Builder("numThreads",3, Int::class.javaObjectType)
        .description("Number of threads used to upload.  The GVCF upload will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
        .required(false)
        .build()

    override fun processData(input: DataSet?): DataSet? {
        val focusRanges = ReadBedfile.getClosedRangesAsPositions(bedFile()).asRanges()
        myLogger.info("processData: focusRanges created")

        check(focusRanges.isNotEmpty()) { "Error Loading in Bed file, file is empty.  Please double check: ${bedFile()}." }

        val referenceSequence = GenomeSequenceBuilder.instance(reference())

        try {
            //Parse the keyfile and verify that the required headers are present
            myLogger.info("before caling parseKeyFile, the keyFile is ${gVCFKeyFile()}")
            val keyFileParsed = parseKeyFile(gVCFKeyFile())
            val headerMap = keyFileParsed.first
            val sampleNameIndex = headerMap["sample_name"]?:-1
            val filesIndex = headerMap["files"]?:-1

            if(sampleNameIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'sample_name' found.")
            }
            if(filesIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'files' found.")
            }

            val entries = keyFileParsed.second

            //Get the RangeMap<Position,IntervalID>
            //This map will allow us to easily figure out what reference range we are in based on the position we are looking at.
            //This is actually mostly so we can associate a RefRange ID for the DB uploading based on the start position of the BED file.
            //Because we do not have a graph at the moment, we have to use direct SQL calls from the phg object.
            val allIntervalRanges = createRangesWithIDFromBedFile(focusRanges)

            val chrToIntervalMapping = mutableMapOf<String, RangeMap<Position, Int>>()

            // These ranges translated from the bedFIle are the 1-based inclusive/inclusive start/end
            allIntervalRanges.asMapOfRanges()
                .keys
                .forEach {
                    val chr = it.lowerEndpoint().chromosome.name
                    if(!chrToIntervalMapping.containsKey(chr)) {
                        chrToIntervalMapping[chr] = TreeRangeMap.create<Position,Int>()
                    }

                    chrToIntervalMapping[chr]?.put(it,allIntervalRanges[it.lowerEndpoint()])
                }

            // Create initial data consisting of IntervalID, chr, start, end and refLength
            val refDataList = createInitialData(focusRanges,allIntervalRanges)
            myLogger.info("createInitialData finished")

            //Coroutines
            //create Blocking scope otherwise execution of this will not wait for it to finish
            runBlocking {
                //Setup the channels  The input channel will hold KeyFile rows.  The Result Channel holds ChromosomeGVCFRecords to write.
                // Defaulting queueSize to 30.
                val inputChannel = Channel<List<String>>(30)
                val resultChannel = Channel<ChromosomeGVCFRecord>(30)
                val numThreads = (numThreads() - 2).coerceAtLeast(1)
                //Setup the channel to pass information to the worker coroutines
                //Basically this just needs to add each keyfile row to the channel so the worker coroutines can start working on them when ready.
                launch {
                    myLogger.info("Adding entries to the inputChannel:")
                    // No filtering, all should be gvcf and all added for this plugin
                    entries.forEach {
                        myLogger.info("Adding: ${it[0]}")
                        inputChannel.send(it)
                    }
                    myLogger.info("Done Adding KeyFile entries to the inputChannel:")
                    inputChannel.close() //Need to close this here to show the workers that it is done adding more data
                }
                //For the number of threads on the machine, set up
                val workerThreads = (1..numThreads).map { threadNum ->
                    launch { processKeyFileEntry(inputChannel,resultChannel, headerMap, referenceSequence, focusRanges, chrToIntervalMapping) }
                }

                launch {
                    processGVCFResultsToFile(refDataList,resultChannel)
                    myLogger.info("Finished writing metrics file to ")
                }

                //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
                //If this is not here, this will run forever.
                launch {
                    workerThreads.forEach { it.join() }
                    resultChannel.close()
                }

            }

        } catch (exc:Exception) {
            throw IllegalStateException("Error processing gvcf file: ${exc.message}")
        }
        return null
    }

    fun createRangesWithIDFromBedFile(focusRanges:Set<Range<Position>>):RangeMap<Position,Int> {
        val intervalRanges:RangeMap<Position,Int> = TreeRangeMap.create()

        focusRanges.forEachIndexed { index, range ->
            intervalRanges.put(range,index+1) // +1 as rangeIds should be positive (ie start at 1), the Set is 0-based
        }

        return intervalRanges
    }

    fun parseKeyFile(keyfile:String) :Pair<Map<String,Int>,List<List<String>>> {

        var headerLineMap = mutableMapOf<String,Int>()
        val fileLines = Utils.getBufferedReader(keyfile).readLines()
            .filter { it!="" } //Get rid of any empty lines in the file.
            .map { it.split("\t") }

        val headerLine = fileLines.first()
        for(i in 0 until headerLine.size) {
            headerLineMap[headerLine[i]] = i
        }

        return Pair(headerLineMap, fileLines.drop(1)) //list.drop(1) removes the first entry in the list.  In this case its the header line.
    }

    /**
     * Method processes each keyFile entry in a multithreaded manner.  By setting this function to use Dispatcher.Default, it parallelizes automatically.
     * This will go through each file for the taxon and extract out sequence and VariantContexts for each reference range.
     * It will then collect the information into a ChromsomeGVFRecord and add it to a list.
     * Once the file is done processing, all the chromosomes are passed to the result channel, then added to the metrics file.
     */
    private suspend fun processKeyFileEntry(inputChannel: Channel<List<String>>, resultChannel: Channel<ChromosomeGVCFRecord>, headerMap : Map<String,Int>, referenceSequence : GenomeSequence, focusRanges : Set<Range<Position>>, allIntervalRegions : Map<String,RangeMap<Position,Int>>) = withContext(
        Dispatchers.Default) {
        for(channelEntry in inputChannel) {
            val sampleNameIndex = headerMap["sample_name"]?:-1
            val filesIndex = headerMap["files"]?:-1

            val line = channelEntry[sampleNameIndex]
            val files =  channelEntry[filesIndex].split(",")

            var totalTimeCreatingSeq = 0L
            var totalTimeCreatingObject = 0L

            //Loop through each file in the keyFile line.
            (0 until files.size).forEach { gvcfFileIndex ->
                val chrNameToChrObject = focusRanges.map { it.lowerEndpoint().chromosome }.distinct().map { Pair(it.name,it) }.toMap()
                val currentGVCFFile = "${gVCFDirectory()}/${files[gvcfFileIndex]}"
                var anchorSequences = HashMap<Int, Pair<Int,Int>>()

                var chromAnchorStartEnd: RangeMap<Position, Int>? = null

                var currentChromosome = ""
                var prevChromosome = ""

                myLogger.info("Done setting up variables for file ${currentGVCFFile}. Moving on to processing reference ranges.")

                val gvcfFileReader = VCFFileReader(File(currentGVCFFile), false)
                val gvcfFileIterator = gvcfFileReader.iterator()

                //Grab the first gvcf record
                var currentGVCFRecord: VariantContext? = gvcfFileIterator.next()

                // Need to make sure that the gvcf chromosome is not behind the first bed file chromosome
                // If we do not do this, it will not write anything to the DB as it will be out of order.
                //In kotlin < on objects will resolve a .compareTo() Call
                while(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) < focusRanges.first().lowerEndpoint().chromosome) ) {
                    currentGVCFRecord = gvcfFileIterator.next()
                }

                var startTime = System.nanoTime()
                for (currentRange in focusRanges) {

                    check(!(currentRange.lowerBoundType()== BoundType.OPEN || currentRange.upperBoundType()== BoundType.OPEN)) { "Error reading BED file.  Currently have either an Open started or a Closed ended range." }

                    val variantTempList = mutableListOf<HaplotypeNode.VariantInfo>()
                    var consecVariantList = mutableListOf<HaplotypeNode.VariantInfo>()
                    val currentBedRegionChr = currentRange.lowerEndpoint().chromosome

                    // Keep track of insertion/deletion to determine number of ref bps covered
                    // for this haplotype.  Do we subtract for both?  The length of the sequence
                    // will be the actual assembly haplotype created.  SO the length already has
                    // the deletions counted (ie, they are not part of the length). So all we really
                    // need is to count the insertions, and subtract them from the length
                    var insertions = 0

                    val currentRefRangeStartPos = currentRange.lowerEndpoint().position
                    // Grab the last physical position.
                    val currentRefRangeEndPos = currentRange.upperEndpoint().position

                    if(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) > currentRange.lowerEndpoint().chromosome)) {
                        continue
                    }

                    if (currentRange.lowerEndpoint().chromosome == null || currentRange.lowerEndpoint().chromosome.name != currentChromosome) {
                        chromAnchorStartEnd = allIntervalRegions[currentRange.lowerEndpoint().chromosome.name]
                        prevChromosome = currentChromosome
                        currentChromosome = chromAnchorStartEnd?.asMapOfRanges()
                            ?.keys
                            ?.first()
                            ?.lowerEndpoint()
                            ?.chromosome
                            ?.name ?: ""

                        if (anchorSequences.size > 0) {
                            // prepare data to be added to the output metrics file
                            myLogger.info("\nStaging ${line} chrom ${prevChromosome} for DB uploading.")
                            myLogger.info("Time spent creating Sequences for  Chr:${currentChromosome} for Line: ${line} : ${(totalTimeCreatingSeq)/1E9}sec")
                            myLogger.info("Time spent creating GVCFSequence for  Chr:${currentChromosome} for Line: ${line} : ${(totalTimeCreatingObject)/1E9}sec")
                            totalTimeCreatingSeq = 0
                            totalTimeCreatingObject = 0

                            myLogger.info("Time spent creating Chr:${currentChromosome} for Line: ${line} : ${(System.nanoTime() - startTime)/1E9}sec")
                            startTime = System.nanoTime()
                            resultChannel.send(ChromosomeGVCFRecord(prevChromosome, line, anchorSequences))
                            anchorSequences = HashMap<Int, Pair<Int,Int>>() // these have been processed, clear the map, start fresh with next chrom
                        }
                    }

                    //Check to see if current range is fully behind GVCF range.  Chromosomes must be the same.  If chromosomes are different
                    // |---B---|
                    //            |--G----|
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name) && currentRefRangeEndPos < currentGVCFRecord.start ) {
//                        myLogger.info("Missing Node Here! Taxon:${line} bedRegion${currentBedRegionChr.name}:${currentRefRangeStartPos}-${currentRefRangeEndPos} " +
//                                "gvcfRegion:${currentGVCFRecord.contig}:${currentGVCFRecord.start}-${currentGVCFRecord.end}")
                        continue
                    }

                    //check to see if the GVCF should be added to the temp list
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name)
                        && checkRegionsOverlap(currentRefRangeStartPos,currentRefRangeEndPos,currentGVCFRecord.start, currentGVCFRecord.end)) {
                        val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                        //Check to see if newGVCFInfo cannot be added to consecVariantList
                        //If non-consecutive or variant, this will be false
                        if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                            //Make sure we have infos to merge.
                            // If we have consecutive SNPs this would be a problem without this check.
                            if(consecVariantList.size != 0) {
                                // always merging ref blocks for this plugin
                                val mergedInfo = mergeRefBlocks(consecVariantList, currentRefRangeStartPos, currentRefRangeEndPos)
                                variantTempList.add(mergedInfo)
                            }

                            consecVariantList = mutableListOf()

                            //If the newGVCFInfo is a variant, we just add it directly to the variantTempList
                            if(newGVCFInfo.isVariant || newGVCFInfo.isIndel) {

                                variantTempList.add(newGVCFInfo)
                                // if this variant was an insertion, count its length
                                val refLength = currentGVCFRecord.reference.length()
                                val altLength = currentGVCFRecord.getAlternateAllele(0).length()
                                if (refLength < altLength) {
                                    insertions += altLength - refLength
                                }
                            }
                            else {
                                consecVariantList.add(newGVCFInfo)
                            }
                        }
                        else {
                            //We can safely add it here.
                            consecVariantList.add(newGVCFInfo)
                        }
                    }

                    //Loop through the GVCF file until we catch up with the current Bed File range.
                    while(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) < currentRange.lowerEndpoint().chromosome) ) {
                        currentGVCFRecord = gvcfFileIterator.next()
                    }

                    //Check to see if the previous GVCF record fully covers the Bed range
                    //      |---B---|
                    // |---------G-------|
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name)
                        && currentRefRangeStartPos >= currentGVCFRecord.start && currentRefRangeEndPos >= currentGVCFRecord.start
                        && currentRefRangeStartPos <= currentGVCFRecord.end && currentRefRangeEndPos <= currentGVCFRecord.end) {

                        val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                        //Check to see if newGVCFInfo cannot be added to consecVariantList
                        //If non-consecutive or variant, this will be false
                        if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                            //Make sure we have infos to merge.
                            // If we have consecutive SNPs this would be a problem without this check.
                            if(consecVariantList.size != 0) {
                                val mergedInfo = mergeRefBlocks(consecVariantList, currentRefRangeStartPos, currentRefRangeEndPos)
                                variantTempList.add(mergedInfo)
                            }

                            consecVariantList = mutableListOf()

                            if (newGVCFInfo.isVariant()) {
                                // if this variant was an insertion, count its length
                                val refLength = currentGVCFRecord.reference.length()
                                val altLength = currentGVCFRecord.getAlternateAllele(0).length()
                                if (refLength < altLength) {
                                    insertions += altLength - refLength
                                }
                            }
                            //regardless if newGVCFInfo is ref or variant, add to the list for export.
                            variantTempList.add(newGVCFInfo)
                        }
                        else {
                            consecVariantList.add(newGVCFInfo)
                            val mergedInfo = mergeRefBlocks(consecVariantList, currentRefRangeStartPos, currentRefRangeEndPos)
                            variantTempList.add(mergedInfo)
                        }

                        val gsStartTime = System.nanoTime()
                        var gs = GVCFSequence.instance(referenceSequence, variantTempList, false)
                        totalTimeCreatingObject += (System.nanoTime()-gsStartTime)
                        //Extract out the fasta sequence:
                        var sequence = gs?.genotypeAsString(currentRange.lowerEndpoint().chromosome,
                            currentRefRangeStartPos,
                            currentRefRangeEndPos)

                        //nulling out to allow GC to release
                        gs = null

                        totalTimeCreatingSeq += (System.nanoTime()-gsStartTime)
                        //Add the sequences and the encoded GVCF to the datastructure
                        val mapEntry = chromAnchorStartEnd!!.getEntry(currentRange.lowerEndpoint())
                        val anchorId = mapEntry!!.value
                        // asmFileId is only for assemblies -send -1
                        //val aData = AnchorDataPHG(currentRange,"0", 0,0, currentGVCFFile, null, variantTempList, sequence,-1)

                        if (sequence != null) {
                            val numRefBpsCovered = sequence.length - insertions
                            anchorSequences[anchorId] = Pair<Int,Int>(sequence.length,numRefBpsCovered)
                        } else {
                            anchorSequences[anchorId] = Pair<Int,Int>(0,0)
                        }

                        continue
                    }

                    //If not, we should loop through the GVCF records until we either leave the BED region
                    while(gvcfFileIterator.hasNext()) {
                        //check to see if current VCF record is past range
                        currentGVCFRecord = gvcfFileIterator.next()
                        val currentVCFChr = chrNameToChrObject[Chromosome.instance(currentGVCFRecord?.contig).name]
                        val currentVCFStart = currentGVCFRecord?.start?:0
                        val currentVCFEnd = currentGVCFRecord?.end?:0

                        //Need to break out once the VCF has moved past the bed region
                        // |---B---|
                        //             |---G----|
                        if(currentVCFChr == null || currentVCFChr != currentBedRegionChr ||currentVCFStart > currentRefRangeEndPos) {
                            break
                        }

                        //If the GVCF End is still before the Bed region, it means that we need to just move to the next GVCF record
                        //                  |-----B----|
                        // |---G---|
                        if(currentVCFEnd < currentRefRangeStartPos) {
                            continue
                        }

                        //If it has passed, either GVCF overlaps BED region ends, GVCF fully covers BED file or GVCF is fully covered by the BED
                        if(currentGVCFRecord != null) {
                            val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                            //Check to see if newGVCFInfo cannot be added to consecVariantList
                            //If non-consecutive or variant, this will be false
                            if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                                if(consecVariantList.size != 0) {
                                    val mergedInfo = mergeRefBlocks(consecVariantList, currentRefRangeStartPos, currentRefRangeEndPos)
                                    variantTempList.add(mergedInfo)
                                }
                                consecVariantList = mutableListOf()

                                if(newGVCFInfo.isVariant) {
                                    // if this variant was an insertion, count its length
                                    val refLength = currentGVCFRecord.reference.length()
                                    val altLength = currentGVCFRecord.getAlternateAllele(0).length()
                                    if (refLength < altLength) {
                                        insertions += altLength - refLength
                                    }
                                    variantTempList.add(newGVCFInfo)
                                }
                                else {
                                    consecVariantList.add(newGVCFInfo)
                                }
                            }
                            else {
                                consecVariantList.add(newGVCFInfo)
                            }
                        }

                        //This is to fix if there is a GVCF record which spans multiple BedRegions
                        // |---B---|    |---B---|
                        //     |------------G------------|
                        //If we do not check this, we will move on to the next gvcf file incorrectly and have a chance to skip the second Bed region
                        if(currentRefRangeEndPos < currentVCFEnd) {
                            break
                        }
                    }

                    if(consecVariantList.size != 0 ) {
                        variantTempList.add(mergeRefBlocks(consecVariantList,currentRefRangeStartPos, currentRefRangeEndPos))
                    }

                    val gsStartTime = System.nanoTime()
                    var gs = GVCFSequence.instance(referenceSequence, variantTempList, false)
                    totalTimeCreatingObject += (System.nanoTime()-gsStartTime)
                    //Extract out the fasta sequence:
                    var sequence = gs?.genotypeAsString(currentRange.lowerEndpoint().chromosome,
                        currentRefRangeStartPos,
                        currentRefRangeEndPos)
                    //nulling out to allow GC to release
                    gs = null
                    totalTimeCreatingSeq += (System.nanoTime()-gsStartTime)
                    //Add the sequences and the encoded GVCF to the datastructure
                    val mapEntry = chromAnchorStartEnd!!.getEntry(currentRange.lowerEndpoint())
                    val anchorId = mapEntry!!.value

                    if (sequence != null) {
                        val numRefBpsCovered = sequence.length - insertions
                        anchorSequences[anchorId] = Pair<Int,Int>(sequence.length,numRefBpsCovered)
                    } else {
                        anchorSequences[anchorId] = Pair<Int,Int>(0,0)
                    }
                }

                if (anchorSequences.size > 0) {
                    // load the data on per-chrom basis
                    myLogger.info("\nStaging ${line} chrom $currentChromosome for DB Uploading")
                    myLogger.info("Time spent creating Chr:${currentChromosome} for Line: ${line} : ${(System.nanoTime() - startTime)/1E9}sec")

                    resultChannel.send(ChromosomeGVCFRecord(currentChromosome,line,anchorSequences))

                    anchorSequences = HashMap<Int, Pair<Int,Int>>()
                }
                //Send the results to the channel so the DB writer can process.
                myLogger.info("\nDone Processing ${line} Adding to the queue for writing metric file.")
            }
        }
    }

    // We start with a file of interval ranges, which should include the intervalId, chrom, start, end and interval len
    // To this we need to append columns that include taxaName_seqLen, taxaName_numRefBPsOverlap - so if we have 4 gvcf files,
    // then we end up adding 8 columns.
    private suspend fun processGVCFResultsToFile(refRangeMetricsList: MutableMap<Int,String>, resultChannel: Channel<ChromosomeGVCFRecord>) {

        // Create a map of taxon and all the anchorIds:
        // The Triple consists of <IntervalId,SeqLen,NumRefBpsCovered>
        val perTaxonAnchorCounts = mutableMapOf<String,MutableList<Triple<Int,Int,Int>>>()
        for(chrGVCFRecord in resultChannel) {

            val assembly = chrGVCFRecord.currentTaxon
            myLogger.info("processGVCFResultsToFIle: processing taxon: ${assembly}")
            val anchorIdLenCov = chrGVCFRecord.anchorDataMapping

            // the records are on a per-chrom basis and contain data for all reference
            // ranges represented on that chromosome. The chrom itself doesn't
            // matter as we are adding data based on anchorid/referenceRangeId.

            for ((key, value) in anchorIdLenCov) {
                if (!perTaxonAnchorCounts.containsKey(assembly)) {
                    perTaxonAnchorCounts[assembly] = mutableListOf<Triple<Int,Int,Int>>()
                }

                // "key" holds the intervalId, "value.first" has the seqLen, "value.second" has number
                // of ref base pairs covered
                perTaxonAnchorCounts[assembly]!!.add(Triple<Int,Int,Int>(key,value.first,value.second))
            }

        }
        myLogger.info("processGVCFResultsToFile: finished processing all chrGVCFRecords, start updating metricsList")

        // Finished processing all the records for each taxon.  Add this data to the initial results
        // Sorting the keys so taxon in metric file are always in the same order and easily searched
        val sortedKeys = perTaxonAnchorCounts.keys.sorted()
        for (key in sortedKeys) {
            val values = perTaxonAnchorCounts.get(key)
            myLogger.info("processGVCFResultsToFile: processing data for taxon $key")
            val newHeader = "${refRangeMetricsList.get(0)}\t${key}_seqLen\t ${key}_numRefBPs" // add this taxon to the header line
            refRangeMetricsList[0] = newHeader
            values!!.forEach { triple ->
                val id = triple.first
                val newData = "${refRangeMetricsList.get(id)}\t${triple.second}\t${triple.third}"
                refRangeMetricsList[id] = newData
            }

        }

        myLogger.info("processGVCFResultsToFile: finished updating the metrics list, write to file")
        writeMetricsFile(refRangeMetricsList)
        myLogger.info("Finished - file written to ${outputFile()}")

    }

    /**
     * Simple function to test to see if the bed region and the gvcf region are overlapping at all.
     */
    private fun checkRegionsOverlap(bedStart : Int, bedEnd : Int, gvcfStart : Int, gvcfEnd:Int) : Boolean {
        return (bedStart <= gvcfStart && gvcfStart <= bedEnd) ||
                (bedStart <= gvcfEnd && gvcfEnd <= bedEnd)
    }

    // This method writes the strings associated with each intervalId to a tab-delimited file
    private fun writeMetricsFile(data:Map<Int,String>) {
        val sortedKeys = data.keys.sorted()
        try {
            val writer = File(outputFile()).bufferedWriter()
            for (key in sortedKeys) {
                writer.write(data[key])
                writer.write("\n")
            }
            writer.close()
        } catch (exc:Exception) {
            throw IllegalStateException("writeMetricsFile: error writing metrics file")
        }
    }

    // This method creates a list<String> which are tab-delimited values for each bedFile interval,
    // the chrom, start, end, and Length.  To this, each assembly/taxon data will be added later
    //
    private fun createInitialData(focusRanges: Set<Range<Position>>, dbRanges: RangeMap<Position,Int>): MutableMap<Int,String> {

        val refDataList = mutableMapOf<Int,String>()

        myLogger.info("createInitialData:  begin ...")
        // Add header line to the file:
        val headerLine = "IntervalID\tChrom\tstart\tend\tInterval_len" // more will be added to this line
        refDataList.put(0,headerLine)
        // creating ranges for data in the user bedFile
        dbRanges.asMapOfRanges()
            .keys
            .forEach {
                // add entry to the initial output file data
                val intervalId = dbRanges.get(it.lowerEndpoint())
                val chr = it.lowerEndpoint().chromosome.name
                val start = it.lowerEndpoint().position
                val end = it.upperEndpoint().position
                val seqLen = end - start + 1
                // Method that adds to this will need to add in the \t before the taxa
                val dataLine = "${intervalId}\t${chr}\t${start}\t${end}\t${seqLen}"
                refDataList.put(intervalId!!,dataLine)
            }

        return refDataList
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = GVCFBedfileMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("GVCF bedfile coverage script")
    }

    override fun getToolTipText(): String {
        return("Takes GVCF file information with a bed file and computes coverage for the bed file intervals")
    }

    /**
     * Keyfile used to specify the inputs to the DB.  Must
     * have columns sample_name, files. Ordering of the columns
     * does not matter.
     * sample_name is the name of the taxon you are uploading.
     * files is a comma-separated list of file names(without
     * path) providing the names of the GVCF files to be uploaded.
     *
     * @return GVCF Key File
     */
    fun gVCFKeyFile(): String {
        return myGVCFKeyFile.value()
    }

    /**
     * Set GVCF Key File. Keyfile used to specify the inputs
     * to the DB.  Must have columns sample_name, files. Ordering
     * of the columns does not matter.
     * sample_name is the name of the taxon you are uploading.
     * files is a comma-separated list of file names(without
     * path) providing the names of the GVCF files to be uploaded.
     *
     * @param value Wgs Key File
     *
     * @return this plugin
     */
    fun gVCFKeyFile(value: String): GVCFBedfileMetricPlugin {
        myGVCFKeyFile = PluginParameter<String>(myGVCFKeyFile, value)
        return this
    }

    /**
     * Directory holding all the gvcf files.
     *
     * @return Gvcf Dir
     */
    fun gVCFDirectory(): String {
        return myGVCFDirectory.value()
    }

    /**
     * Set Gvcf Dir. Directory holding all the gvcf files.
     *
     * @param value Gvcf Dir
     *
     * @return this plugin
     */
    fun gVCFDirectory(value: String): GVCFBedfileMetricPlugin {
        myGVCFDirectory = PluginParameter<String>(myGVCFDirectory, value)
        return this
    }

    /**
     * File where metrics will be written.
     *
     * @return Output File
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output File. File where metrics will be written.
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): GVCFBedfileMetricPlugin {
        myOutputFile = PluginParameter<String>(myOutputFile, value)
        return this
    }

    /**
     * Input Reference Fasta
     *
     * @return Reference Fasta
     */
    fun reference(): String {
        return myReference.value()
    }

    /**
     * Set Reference Fasta. Input Reference Fasta
     *
     * @param value Reference Fasta
     *
     * @return this plugin
     */
    fun reference(value: String): GVCFBedfileMetricPlugin {
        myReference = PluginParameter<String>(myReference, value)
        return this
    }

    /**
     * File holding the Reference Range Information
     *
     * @return Bed File
     */
    fun bedFile(): String {
        return bedFile.value()
    }

    /**
     * Set Bed File. File holding the Reference Range Information
     *
     * @param value Bed File
     *
     * @return this plugin
     */
    fun bedFile(value: String): GVCFBedfileMetricPlugin {
        bedFile = PluginParameter<String>(bedFile, value)
        return this
    }

    /**
     * Number of threads used to upload
     *
     * @return Num Threads
     */
    fun numThreads() : Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to upload
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value : Int) : GVCFBedfileMetricPlugin {
        myNumThreads = PluginParameter(myNumThreads, value)
        return this
    }


}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(GVCFBedfileMetricPlugin::class.java)
}