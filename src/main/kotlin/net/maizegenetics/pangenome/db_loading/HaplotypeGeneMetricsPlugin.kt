package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyConsensusMetricPlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.DirectoryCrawler
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.nio.file.Paths
import javax.swing.ImageIcon

/**
 * Class to analyse haplotype coverage of genes.  Currently it takes a directory of
 * files, one per genotype, that contain the list of ref range ids that appear in the
 * haplotypes table for that genotype. This was created via the sql query:
 * (first get list of gamete_grp_ids for each genotype), then for each gamete_grp_id do:
 *
 * select haplotypes.ref_range_id from haplotypes
 * INNER JOIN reference_ranges on reference_ranges.ref_range_id = haplotypes.ref_range_id
 * INNER JOIN ref_range_ref_range_method on ref_range_ref_range_method.ref_range_id=reference_ranges.ref_range_id
 * INNER JOIN methods on ref_range_ref_range_method.method_id = methods.method_id
 *    AND methods.name = 'refRegionGroup' and haplotypes.gamete_grp_id=3;
 *
 * NOTE - the above query is no longer correct - it needs to use user-defined method
 * names instead of refRegionGroup to get the files needed.  But the code below that
 * analyzes these files stands.
 *
 * 2 tests to be run:
 *   kotlin/db_loading/HaplotypeGEneMetricsPluginTest has
 *      testHaplotypeGeneMetricsPlugin
 *      createFastaForBlast
 *  One creates list of refRangeIds/genes in ref but not in each assembly,
 *  the other creates a blast file of specified ref range sequences.
 *
 *  The run blastdb (db created from assembly fasta) and blastn (fastas of selected reference sequences
 *  blasted against the assembly blast db) to see if these ranges exist in the assemblies but were missed by
 *  mummer4/nucmer.
 */
class HaplotypeGeneMetricsPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = LogManager.getLogger(HaplotypeGeneMetricsPlugin::class.java)
    private var inputDir = PluginParameter.Builder("inputDir", null, String::class.java)
            .description("Directory with files of haplotypes reference ranges")
            .required(true)
            .inDir()
            .build()

    private var refName = PluginParameter.Builder("refName", null, String::class.java)
            .description("Name of reference haplotype ")
            .required(true)
            .build()

    private var refRangeFile = PluginParameter.Builder("refRangeFile", null, String::class.java)
            .description("File containing reference range information: csv with columns ref_range_id, chrom,range_start,range_end")
            .required(true)
            .inFile()
            .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("File for writing matrix of gene coverage")
            .required(true)
            .outFile()
            .build()

    override fun processData(input: DataSet?): DataSet? {
        val directoryFileNames = DirectoryCrawler.listPaths("glob:*{_genes.txt}", Paths.get(inputDir()))

        val hapNameList = mutableListOf<String>()
        var numRefRanges = 35677 // this should be determined by db, or via parameter
        // First, read each file, add to a list of list
        // List has haplotype name, then
        var refRangeHitCount = MutableList<Int>(numRefRanges){index -> 0}
        println("Before try")
        try {
            // FOr each file, read the genes into a list
            println("reading direcotry files")
            directoryFileNames.forEach {
                var fullName = it.fileName.toString()
                var lastSlash = fullName.lastIndexOf("/")
                var hapName = fullName.substring(lastSlash + 1, fullName.indexOf("_"))
                println("fullName: $fullName hapName: $hapName")
                hapNameList.add(hapName)
                Utils.getBufferedReader(it.toString()).readLines()
                        .forEach { line ->
                            val rangeId = line.toInt()
                            refRangeHitCount[rangeId-1] = refRangeHitCount[rangeId-1] +1
                        }
            }

            val headerString = "refRangeID\t${hapNameList.joinToString("\t")}\n"

            myLogger.info("Finished creating hapNameList")

            // read the reference range file:
            var refRangeMap = mutableMapOf<Int,String>()
            Utils.getBufferedReader(refRangeFile()).readLines()
                    .forEach { line ->

                        var cIndex1 = line.indexOf(",");
                        val rangeId = line.substring(0,cIndex1).toInt()
                        val data = line.substring(cIndex1+1)

                        refRangeMap.put(rangeId,data)
                    }

            // For now, just print gene lis and number of hits
            var geneCountHeader = "refRangeId\tchrom\tstart\tend\tlen\tTotalHits\n"
            // write file without header line
            var bw = Utils.getBufferedWriter(outputFile())
            bw.write(geneCountHeader)
            println("writing outputFile")
            for (rangeID in 1 .. numRefRanges) {
                var count = refRangeHitCount[rangeID-1]
                var refData = refRangeMap.get(rangeID)
                if (!refData.isNullOrBlank() ) {
                    var cIndex1 = refData.indexOf(",");
                    var cIndex2 = refData.indexOf(",",cIndex1+1);
                    var chrom = refData.substring(0,cIndex1)
                    var start =  refData.substring(cIndex1+1,cIndex2)
                    var end =  refData.substring(cIndex2+1)
                    var len = end.toInt() - start.toInt()
                    bw.write("$rangeID\t$chrom\t$start\t$end\t$len\t$count\n")
                }

            }
            bw.close()

        } catch (exc: Exception) {

            throw IllegalArgumentException("Error processing haplotypes files $exc", exc)
        }

        return null
    }

    override fun getToolTipText(): String {
        return ("Plugin to find ref ranges not covered by db haplotypes")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyConsensusMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return ("HaplotypeGeneMetricsPlugin")
    }

    /**
     * Directory with files of haplotypes reference ranges
     *
     * @return Input Dir
     */
    fun inputDir(): String {
        return inputDir.value()
    }

    /**
     * Set Input Dir. Directory with files of haplotypes reference
     * ranges
     *
     * @param value Input Dir
     *
     * @return this plugin
     */
    fun inputDir(value: String): HaplotypeGeneMetricsPlugin {
        inputDir = PluginParameter<String>(inputDir, value)
        return this
    }

    /**
     * Name of file containing reference genes
     *
     * @return Ref File Name
     */
    fun refName(): String {
        return refName.value()
    }

    /**
     * Set Ref File Name. Name of file containing reference
     * genes
     *
     * @param value Ref File Name
     *
     * @return this plugin
     */
    fun refName(value: String): HaplotypeGeneMetricsPlugin {
        refName = PluginParameter<String>(refName, value)
        return this
    }

    /**
     * File containing reference range information: csv with
     * columns ref_range_id, chrom,range_start,range_end
     *
     * @return Ref Range File
     */
    fun refRangeFile(): String {
        return refRangeFile.value()
    }

    /**
     * Set Ref Range File. File containing reference range
     * information: csv with columns ref_range_id, chrom,range_start,range_end
     *
     * @param value Ref Range File
     *
     * @return this plugin
     */
    fun refRangeFile(value: String): HaplotypeGeneMetricsPlugin {
        refRangeFile = PluginParameter<String>(refRangeFile, value)
        return this
    }

    /**
     * File for writing matrix of gene coverage
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. File for writing matrix of gene coverage
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): HaplotypeGeneMetricsPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(HaplotypeGeneMetricsPlugin::class.java)
}