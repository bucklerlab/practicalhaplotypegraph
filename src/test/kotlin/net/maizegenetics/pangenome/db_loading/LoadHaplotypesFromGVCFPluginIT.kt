package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll

import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import kotlin.test.assertFailsWith
import kotlin.test.fail
import kotlin.test.assertNotNull
import kotlin.test.assertEquals

/**
 * Unit test class to test uploading GVCF files created by SmallSeqTests using LoadHaplotypesFromGVCFPlugin
 *
 * It currently will test
 * -full upload of the SmallSeq GVCFs
 * -Checking column names for misspellings.
 */
@ExtendWith(SmallSeqExtension::class)
class LoadHaplotypesFromGVCFPluginIT {

    companion object {
        //get the current time for the method name
        val currentLocalDateTime = LocalDateTime.now()

        var hapMethodName = "GATK_KEYFILE_METHOD_${currentLocalDateTime.toString().replace(":","_").replace(",","_")}"
        val expectedNumberOfNodes = 60  //TODO this needs to be changed if

        //Global Mapping Taxon->Sequence for testing purposes:
        val gvcfSequenceMap = mutableMapOf<String, GenomeSequence>()

        //This list allows us to make and alter the column names in an automated fashion
        val keyFileHeaders = listOf("type","sample_name","sample_description","files","chrPhased","genePhased","phasingConf")

        @BeforeAll
        fun setup() {
            // NOTE: File operations in setup() cause all tests to fail
            //hapMethodName = "GATK_KEYFILE_METHOD_${currentLocalDateTime.toString().replace(":","_").replace(",","_")}"
            LoggingUtils.setupDebugLogging()
            createTestKeyfile(SmallSeqExtension.keyFilePath,keyFileHeaders,SmallSeqExtension.inputGVCFDir)
        }

        /**
         * Create a KeyFile based on the Small Seq tests
         */
        fun createTestKeyfile(fileName : String, headerColumnNames : List<String>, gvcfDirectory : String) {

            Utils.getBufferedWriter(fileName).use{output ->
                //Get the list of gvcf file names
                val gvcfFiles = File(gvcfDirectory).listFiles()
                    .filter { it.toString().endsWith("_filtered.g.vcf.gz") }
                    .map { it.name }

                //Keyfile format:
                //sample_name\tsample_description\tfiles\tchrPhased\tgenePhased\tphasingConf\n
                output.write(headerColumnNames.joinToString("\t")+"\n")
                gvcfFiles.forEach{
                    val taxonName = it.split("_")[0]
                    gvcfSequenceMap[taxonName] = GVCFSequence.instance(SmallSeqExtension.refFilePath,gvcfDirectory+it)
                    output.write("GVCF\t${taxonName}\t${taxonName}_FromGVCF\t${it}\ttrue\ttrue\t1.0\n")
                }
            }
        }
    }


    @Test
    fun testQueueSizeZeroOrBelow() {
        ParameterCache.load(SmallSeqExtension.configFilePath)
        val queueSizeExc = assertFailsWith<IllegalArgumentException> {
            LoadHaplotypesFromGVCFPlugin(null,false)
                .wGSKeyFile(SmallSeqExtension.keyFilePath)
                .bedFile(SmallSeqExtension.anchorBEDFilePath)
                .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
                .reference(SmallSeqExtension.refFilePath)
                .method(hapMethodName)
                .methodDescription("KeyFile Based Upload")
                .queueSize(-1)
                .processData(null)
        }
        assertNotNull(queueSizeExc.message)
        assertEquals(queueSizeExc!!.message, "Queue size is negative.  Please specify at least 0 for the size of the Multithreading Queue")
    }

    //Method to do a full upload using the keyfile
    @Test
    fun testGVCFFullUpload() {
        //Load the config file
        ParameterCache.load(SmallSeqExtension.configFilePath)

        //Run The plugin
        LoadHaplotypesFromGVCFPlugin(null,false)
                .wGSKeyFile(SmallSeqExtension.keyFilePath)
                .bedFile(SmallSeqExtension.anchorBEDFilePath)
                .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
                .reference(SmallSeqExtension.refFilePath)
                .method(hapMethodName)
                .methodDescription("KeyFile Based Upload")
                .performFunction(null)

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeSequences(true)
                .methods(hapMethodName)
                .configFile(SmallSeqExtension.configFilePath)
                .build()

        //Check to see if we have the right number of nodes
        assertTrue(graph.numberOfNodes()==expectedNumberOfNodes)
        val taxonToNodesMap = ArrayListMultimap.create<String,HaplotypeNode>()

        graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { node ->
                    node.taxaList().map { taxon -> taxon.name }
                            .map { taxon -> Pair(taxon,node) }
                }
                .forEach { taxonPair -> taxonToNodesMap.put(taxonPair.first,taxonPair.second) }

        //Check the sequences in the graph.
        verifySequenceInGraph(taxonToNodesMap)
    }

    /**
     * Function to test the key file column names are correct.
     *
     * Basically add a _1 to each column one at a time and make sure it throws an IllegalStateException
     */
    @Test
    fun verifyKeyFileCorrectness() {
        ParameterCache.load(SmallSeqExtension.configFilePath)

        keyFileHeaders.forEach { currentHeaderToReplace ->
            //Get rid of the sample_description as that one can be empty
            if(currentHeaderToReplace != "sample_description") {
                val newHeaders = keyFileHeaders.map { columnName ->
                    if (currentHeaderToReplace == columnName) {
                        columnName + "_1"
                    } else {
                        columnName
                    }
                }

                //Make a temp Key file
                val tempKeyFile = "${SmallSeqExtension.baseDir}keyfile_${currentHeaderToReplace}.txt"
                createTestKeyfile(tempKeyFile,newHeaders,SmallSeqExtension.inputGVCFDir)

                //Assert it fails
                assertFailsWith<IllegalStateException> {
                    println("Should Fail.")
                    LoadHaplotypesFromGVCFPlugin(null, false)
                            .wGSKeyFile(tempKeyFile)
                            .bedFile(SmallSeqExtension.anchorBEDFilePath)
                            .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
                            .reference(SmallSeqExtension.refFilePath)
                            .method(hapMethodName)
                            .methodDescription("KeyFile Based Upload")
                            .processData(null) //Use processData as we want it to throw an exception.
                }
                //Delete the file
                Files.delete(Paths.get(tempKeyFile))
            }
        }
    }

    //TODO Speed test between single and multithreaded version


    /**
     * Test to make sure the existing sequences in the DB actually match the gvcfs.
     * This is different than testGVCFFullUpload() where we try to upload then make sure our uploads are correct.
     */
    @Test
    fun verifyExistingSequence() {
        ParameterCache.load(SmallSeqExtension.configFilePath)

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeSequences(true)
                .methods("GATK_PIPELINE")
                .configFile(SmallSeqExtension.configFilePath)
                .build()


        val taxonToNodesMap = ArrayListMultimap.create<String,HaplotypeNode>()

        graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { node ->
                    node.taxaList().map { taxon -> taxon.name }
                            .map { taxon -> Pair(taxon,node) }
                }
                .forEach { taxonPair -> taxonToNodesMap.put(taxonPair.first,taxonPair.second) }

        verifySequenceInGraph(taxonToNodesMap)
    }

    @Test
    fun testASMVersion() {
        LoggingUtils.setupDebugLogging()

        val keyFileHeadersWithGenomeData = listOf("type","sample_name","sample_description","files","chrPhased","genePhased","phasingConf","gvcfServerPath","gvcfLocalPath")
        // Recreate the keyfile - createKeyFile from setup checks for different file extension than was used
        //  in this test.  There is also a gvcf keyfile created in smallSeq, but that keyFile has the
        //  docker path, and here we need non-docker path.

        val asmGVCFKeyFile = File(SmallSeqExtension.keyFilePath).parent + "/" + "asmGVCFKeyFile.txt"
        println("getting list of gvcf files, writing key file")
        Utils.getBufferedWriter(asmGVCFKeyFile).use { output ->
            //Get the list of gvcf file names
            val gvcfFiles = File(SmallSeqExtension.inputGVCFDir).listFiles()
                .filter { it.toString().endsWith("gvcf.gz") }
                .map { it.name }

            //Keyfile format:
            //type\tsample_name\tsample_description\tfiles\tchrPhased\tgenePhased\tphasingConf\n
            output.write(keyFileHeadersWithGenomeData.joinToString("\t") + "\n")
            gvcfFiles.forEach {
                //val taxonName = it.split("_")[0]
                val taxonName = it.substring(0,it.lastIndexOf(".")) // when _Ref is removed from name, use this
                gvcfSequenceMap[taxonName] = GVCFSequence.instance(SmallSeqExtension.refFilePath, SmallSeqExtension.inputGVCFDir + it)
                val serverDir = "irodsServerDir;//bldir/${taxonName}.fasta"
                val genomeFasta = SmallSeqExtension.assemblyFastaDir + "/${taxonName}.fa"
                output.write("GVCF\t${taxonName}\t${taxonName}_FromGVCF\t${it}\ttrue\ttrue\t1.0\t${serverDir}\t${genomeFasta}\n")
            }
        }

        // Use the files created above with LoadHaplotypesFromGVCFPlugin
        ParameterCache.load(SmallSeqExtension.configFilePath)

        val methodName = "test_anchorwave"
        //Run The plugin
        println("\ncalling LoadHaploytpesFromGVCFPlugin with methodName ${methodName}")
        LoadHaplotypesFromGVCFPlugin(null,false)
            .wGSKeyFile(asmGVCFKeyFile)
            //.wGSKeyFile(SmallSeqPaths.asmGVCFKeyFile)
            .bedFile(SmallSeqExtension.anchorBEDFilePath)
            .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
            .reference(SmallSeqExtension.refFilePath)
            .method(methodName)
            .methodDescription("KeyFile Based Upload")
            .performFunction(null)

        println("\ncalling HapotypeGraphBuilderPlugin")
        val graph = HaplotypeGraphBuilderPlugin(null,false)
            .includeSequences(true)
            .methods(methodName)
            .configFile(SmallSeqExtension.configFilePath)
            .build()

        // Number of nodes with methodName="test_anchorwave"
        // Should be 30 in all, including 10 for the ref with a different method
        val expectedHaplotypes = 20
        //Check to see if we have the right number of nodes
        assertEquals(expectedHaplotypes,graph.numberOfNodes(),"Number of nodes in graph is not correct")
        val taxonToNodesMap = ArrayListMultimap.create<String,HaplotypeNode>()
        graph.referenceRanges()
            .flatMap { graph.nodes(it) }
            .flatMap { node ->
                node.taxaList().map { taxon -> taxon.name }
                    .map { taxon -> Pair(taxon,node) }
            }
            .forEach { taxonPair -> taxonToNodesMap.put(taxonPair.first,taxonPair.second) }

        verifySequenceInGraph(taxonToNodesMap)
        // Currently, for smallseq, all strands should be "+"
        verifyStrandInGraph(taxonToNodesMap)

        // TODO: pull down and verify that the annotations match

    }



    /**
     * Function to verify that the sequences are all identical between GVCF and graph created sequences.
     * If this failed, it means something creating the sequences for the graph nodes has changed.
     */
    fun verifySequenceInGraph(taxonToNodesMap : Multimap<String, HaplotypeNode>) {
        taxonToNodesMap.keySet().forEach { taxonName ->
            taxonToNodesMap.get(taxonName)
                    .forEach { node ->
                        val refRange = node.referenceRange()
                        val chr = refRange.chromosome()
                        val start = refRange.start()
                        val end = refRange.end()

                        val graphSequence = node.haplotypeSequence().sequence()

                        val gvcfSequence = gvcfSequenceMap[taxonName]?.genotypeAsString(chr,start,end)?:""

                        assertEquals(gvcfSequence,graphSequence,"GVCF and Graph Sequence do not match: ${taxonName} ${chr.name}:${start}-${end}\nGVCFSequence:\n${gvcfSequence}\nGraph:\n${graphSequence}")
                    }
        }
    }

    fun verifyStrandInGraph(taxonToNodesMap : Multimap<String, HaplotypeNode>) {
        taxonToNodesMap.keySet().forEach { taxonName ->
            taxonToNodesMap.get(taxonName)
                .forEach { node ->
                    val refRange = node.referenceRange()
                    val strand = node.asmStrand()

                    assertEquals("+",strand, "Strand should be +: ${taxonName} strand ${strand}")
                }
        }
    }


    @Test
    fun parseKeyFileTestInvalidInputs() {
        ParameterCache.load(SmallSeqExtension.configFilePath)

        val invalidKeyFile = "${SmallSeqExtension.baseDir}/invalidKeyFile.txt"

        Utils.getBufferedWriter(invalidKeyFile).use { output ->
            output.write(
            """
            type	sample_name	sample_description	mafFile	files	chrPhased	genePhased	phasingConf	gvcfServerPath
            GVCF\tC33_DSC691_hap1\_Assembly\tC33_DSC691_hap1 description\t/workdir/mbw88/step3_build/mafs/C33_DSC691_hap1.maf\tC33_DSC691_hap1.gvcf.gz\ttrue\ttrue\t0.9\tmaizegenetics.net;/some/directory\n
            """.trimIndent())
        }
        val keyFileExc1 = assertFailsWith<java.lang.IllegalArgumentException> {
            LoadHaplotypesFromGVCFPlugin(null,false)
                .wGSKeyFile(invalidKeyFile)
                .bedFile(SmallSeqExtension.anchorBEDFilePath)
                .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
                .reference(SmallSeqExtension.refFilePath)
                .method(hapMethodName)
                .methodDescription("Test Invalid Inputs")
                .processData(null)
        }
        assertNotNull(keyFileExc1.message)
        assertEquals(keyFileExc1!!.message, "All entries must have the same number of elements as the header, whereas line 2 does not")

        Utils.getBufferedWriter(invalidKeyFile).use { output ->
            output.write(
                """
            type	sample_name	sample_description	mafFile	files	chrPhased	genePhased	phasingConf	gvcfServerPath
            GVCF	Sample	Description	/some/dir/foo.maf	LineA.gvcf.gz	true	true	spaghetti	maizegenetics.net;/some/directory
            """.trimIndent())
        }
        val keyFileExc2 = assertFailsWith<java.lang.IllegalArgumentException> {
            LoadHaplotypesFromGVCFPlugin(null, false)
                .wGSKeyFile(invalidKeyFile)
                .bedFile(SmallSeqExtension.anchorBEDFilePath)
                .gVCFDirectory(SmallSeqExtension.inputGVCFDir)
                .reference(SmallSeqExtension.refFilePath)
                .method(hapMethodName)
                .methodDescription("Test Invalid Inputs")
                .processData(null)
        }
        assertNotNull(keyFileExc2.message)
        assertEquals(keyFileExc2!!.message, "Column 'phasingConf' expects a Float, whereas you gave it 'spaghetti' on line 2")
    }
}