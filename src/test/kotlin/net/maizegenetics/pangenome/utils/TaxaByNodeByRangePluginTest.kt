package net.maizegenetics.pangenome.utils

import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TaxaByNodeByRangePluginTest {

    @Test
    fun testPlugin() {

        val graph = buildSimpleDummyGraph()

        val rangeIds = sortedSetOf(1, 3)

        // return type Map<rangeid: Int, Map<hapid: Int, taxa: List<String>>>
        val result = TaxaByNodeByRangePlugin()
            .rangeIds(rangeIds)
            .runPlugin(graph)

        assertEquals(2, result.size, "TaxaByNodeByRangePluginTest: result.size should be 2")

        val resultRange1 = result[1]

        assertEquals(3, resultRange1?.size, "TaxaByNodeByRangePluginTest: resultRange1?.size should be 3")
        assertEquals(
            "sample3",
            resultRange1?.get(2)?.get(0),
            "TaxaByNodeByRangePluginTest: hapid 2 of range id 1 should have taxa name sample3"
        )
        assertEquals(
            "sample1",
            resultRange1?.get(0)?.get(0),
            "TaxaByNodeByRangePluginTest: hapid 0 of range id 1 should have taxa name sample1"
        )
        assertEquals(
            "sample2",
            resultRange1?.get(1)?.get(0),
            "TaxaByNodeByRangePluginTest: hapid 1 of range id 1 should have taxa name sample2"
        )

    }

}