package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.File

@ExtendWith(SmallSeqExtension::class)
class ImportReadMappingToDBPluginIT {

    private val configDir = "${SmallSeqExtension.testTmpDir}/configs"
    private val keyFileDir = "${SmallSeqExtension.testTmpDir}/keyfiles"
    private val readMappingFileDir = "${SmallSeqExtension.testTmpDir}/readMappings"

    private val originalConfig = "$configDir/configOriginal.txt"
    private val originalName = "originaldb"

    private val finalConfig = "$configDir/configFinal.txt"
    private val finalName = "finaldb"

    private val keyFile = "$keyFileDir/keyFile.txt"

    private val numberOfDBs = 2 // Including the original database
    private val readMappingMap = mutableMapOf<Pair<String,String>,ByteArray>()

    private val testMergeMethod = "TestMergeMethod"
    private val testMergeDescription = "Method to test merging the DBs together"

    // TODO: This is potentially useful and should live somewhere general
    private fun createDatabase(dbToCreate: String) {
        // First create database using SmallSeq database as a template before restoring into it
        val createCommand = listOfNotNull(
            "createdb",
            "--host=${SmallSeqExtension.dbHost}",
            "--port=${SmallSeqExtension.dbPort}",
            "--username=postgres",
            "--template=${SmallSeqExtension.dbName}",
            dbToCreate
        )

        val createBuilder = ProcessBuilder(createCommand)
        createBuilder.environment().put("PGPASSWORD", "postgres")
        val createProcess = createBuilder.start()
        createProcess.waitFor()
        if (createProcess.exitValue() != 0) { throw RuntimeException(IOUtils.toString(createProcess.errorStream)) }
    }

    // TODO: This is potentially useful and should live somewhere general
    private fun dropDatabase(dbToDrop: String) {
        val dropCommand = listOfNotNull(
            "dropdb",
            "--host=${SmallSeqExtension.dbHost}",
            "--port=${SmallSeqExtension.dbPort}",
            "--username=postgres",
            dbToDrop
        )

        val dropBuilder = ProcessBuilder(dropCommand)
        dropBuilder.environment().put("PGPASSWORD", "postgres")
        val dropProcess = dropBuilder.start()
        dropProcess.waitFor()
        if (dropProcess.exitValue() != 0) { throw RuntimeException(IOUtils.toString(dropProcess.errorStream)) }
    }

    @BeforeEach
    fun before() {
        FileUtils.cleanDirectory(SmallSeqExtension.testTmpDirFile)
        File(configDir).mkdirs()
        File(keyFileDir).mkdirs()
        File(readMappingFileDir).mkdirs()

        // Create databases inside of Postgres
        createDatabase(originalName)
        createDatabase(finalName)
        (0 until numberOfDBs).forEach {
            createDatabase("importreadmappingtest$it")
        }

        writeConfig(originalConfig, originalName)
        writeConfig(finalConfig, finalName)

        Utils.getBufferedWriter(keyFile).use {
            it.write("""
                cultivar	flowcell_lane	filename	PlateID
                LineB1_wgs	wgsFlowcellFinal	LineB1_R1.fastq	wgs
                LineA_wgs	wgsFlowcellFinal	LineA_R1.fastq	wgs
            """.trimIndent())
        }

        ParameterCache.load(originalConfig)
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(originalConfig)
            .methods(SmallSeqExtension.haplotypeMethod)
            .includeSequences(false)
            .includeVariantContexts(false)
            .performFunction(null)
        FastqToMappingPlugin(null, false)
            .indexFile("${SmallSeqExtension.baseDir}/pangenome/pangenome_GATK_PIPELINE_k21w11I90G.mmi")
            .fastqDir(SmallSeqExtension.dataDir)
            .keyFile(keyFile)
            .methodName(testMergeMethod)
            .methodDescription(testMergeDescription)
            .outputDebugDir(readMappingFileDir)
            .performFunction(graph)

        // Make the configs for the dbs to be merged into the original one.
        (0 until numberOfDBs).forEach {
            val mergeConfig = "$configDir/config_${it}.txt"
            val keyFileName = "$keyFileDir/keyFile_${it}.txt"

            writeConfig(mergeConfig, "importreadmappingtest$it")

            Utils.getBufferedWriter(keyFileName).use { output ->
                output.write("""
                    cultivar	flowcell_lane	filename	PlateID
                    LineB1_wgs	wgsFlowcellMerge$it	LineB1_R1.fastq	wgs
                    LineA_wgs	wgsFlowcellMerge$it	LineA_R1.fastq	wgs
                """.trimIndent())
            }

            ParameterCache.load(mergeConfig)
            val graphMerge = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(mergeConfig)
                .methods(SmallSeqExtension.haplotypeMethod)
                .includeSequences(false)
                .includeVariantContexts(false)
                .performFunction(null)
            FastqToMappingPlugin(null, false)
                .indexFile("${SmallSeqExtension.baseDir}/pangenome/pangenome_GATK_PIPELINE_k21w11I90G.mmi")
                .fastqDir(SmallSeqExtension.dataDir)
                .keyFile(keyFileName)
                .methodName(testMergeMethod)
                .methodDescription(testMergeDescription)
                .outputDebugDir(readMappingFileDir)
                .performFunction(graphMerge)
        }
        verifyNumberFastqsAdded()
    }

    @AfterEach
    fun after() {
        // Remove databases created for tests
        dropDatabase(originalName)
        dropDatabase(finalName)
        (0 until numberOfDBs).forEach {
            dropDatabase("importreadmappingtest$it")
        }
    }

    /**
     * Function to check to see if we added the correct number of fastq mappings in the initialization code.
     */
    fun verifyNumberFastqsAdded() {
        try {
            val connection  = DBLoadingUtils.connection(originalConfig, false)
            val phg = PHGdbAccess(connection)

            //get the readMapping ids for this method name
            var mappings = phg.getReadMappingsForMethod(testMergeMethod)
            mappings.forEach {
                readMappingMap[Pair(it.genoName,it.fileGroupName)] = it.readMappings
            }
            assertEquals(2, mappings.size, "Original DB does not have the correct number of added readMappings")

            phg.close()
            connection.close()

            (0 until numberOfDBs).forEach {
                val connMerge  = DBLoadingUtils.connection("$configDir/config_$it.txt", false)
                val phgMerge = PHGdbAccess(connMerge)

                // Get the readMapping ids for this method name
                mappings = phgMerge.getReadMappingsForMethod(testMergeMethod)
                mappings.forEach {readMappingRecord ->
                    readMappingMap[Pair(readMappingRecord.genoName,readMappingRecord.fileGroupName)] = readMappingRecord.readMappings
                }
                assertEquals(2, mappings.size, "DB_$it does not have the correct number of added readMappings")

                phgMerge.close()
                connMerge.close()
            }

        }
        catch (exc: Exception) {
            fail("Verifying number of FASTQs added failed with: ${exc.toString()}")
        }
    }

    fun writeConfig(configFileName: String, dbName: String) {
        Utils.getBufferedWriter(configFileName).use { output ->
            output.write("""
                    host=${SmallSeqExtension.dbFull}
                    user=postgres
                    password=postgres
                    DB=$dbName
                    DBtype=postgres
                """.trimIndent())
        }
    }

    /**
     * Function to test if ImportReadMappingToDBPlugin will work.
     *
     * After merging it will check to see the number of readMappings for the method and then will also check to see if the mappings themselves are the same.
     *
     * TODO check the keyfile to make sure it is what is expected.
     */
    @Test
    fun testMergingDBs() {
        ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(finalConfig)
                .inputDir(configDir)
                .loadFromDB(true)
                .readMappingMethod(testMergeMethod)
                .outputPathFindingKeyFile(keyFile)
                .performFunction(null)

        val connection  = DBLoadingUtils.connection(finalConfig, false)
        val phg = PHGdbAccess(connection)

        val mappings = phg.getReadMappingsForMethod(testMergeMethod)

        assertEquals(2 + 2 * numberOfDBs, mappings.size, "MergedDB does not have the correct number of read mappings")

        mappings.forEach {
            val currentName = Pair(it.genoName,it.fileGroupName)
            assertArrayEquals(readMappingMap[currentName], it.readMappings, "MergedDB does not have the same array")
        }

        phg.close()
    }

    @Test
    fun testUploadingFiles() {
        val graph = HaplotypeGraphBuilderPlugin(null,false)
            .configFile(originalConfig)
            .methods(SmallSeqExtension.haplotypeMethod)
            .includeSequences(false)
            .includeVariantContexts(false)
            .performFunction(null)

        ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(finalConfig)
                .inputDir(readMappingFileDir)
                .loadFromDB(false)
                .readMappingMethod(testMergeMethod)
                .outputPathFindingKeyFile(keyFile)
                .performFunction(graph)

        val dbConnect = DBLoadingUtils.connection(finalConfig, false)
        val phg = PHGdbAccess(dbConnect)

        // Check to see if we have the correct number of read mappings and that they match what we imported
        val mappings = phg.getReadMappingsForMethod(testMergeMethod)

        assertEquals(2 + 2 * numberOfDBs, mappings.size, "MergedDB does not have the correct number of read mappings")

        mappings.forEach {
            val currentName = Pair(it.genoName,it.fileGroupName)
            assertArrayEquals(readMappingMap[currentName], it.readMappings, "MergedDB does not have the same array")
        }

        phg.close()
    }
}