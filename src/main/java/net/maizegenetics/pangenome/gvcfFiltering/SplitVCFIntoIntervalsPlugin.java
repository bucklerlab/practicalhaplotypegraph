package net.maizegenetics.pangenome.gvcfFiltering;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 * @author Terry Casstevens Created September 15, 2017
 */
public class SplitVCFIntoIntervalsPlugin extends AbstractPlugin {

    private static final Logger myLogger = LogManager.getLogger(SplitVCFIntoIntervalsPlugin.class);

    private PluginParameter<String> myInput = new PluginParameter.Builder<>("input", null, String.class)
            .description("Input VCF file")
            .required(true)
            .inFile()
            .build();

    private PluginParameter<String> myIntervalFile = new PluginParameter.Builder<>("intervals", null, String.class)
            .description("Intervals file")
            .required(true)
            .inFile()
            .build();

    public SplitVCFIntoIntervalsPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {

        try (BufferedReader reader = Utils.getBufferedReader(input());
             BufferedReader intervals = Utils.getBufferedReader(intervalFile())) {

            StringBuilder header = new StringBuilder();
            String vcfLine = reader.readLine();
            while (vcfLine.startsWith("#")) {
                header.append(vcfLine);
                header.append("\n");
                vcfLine = reader.readLine();
            }
            String headerStr = header.toString();

            String interval = intervals.readLine();
            while (interval != null) {

                String[] tokens = interval.split(":");
                String chr = tokens[0];
                String[] tokens1 = tokens[1].split("-");
                int start = Integer.parseInt(tokens1[0]);
                int end = Integer.parseInt(tokens1[1]);

                try (BufferedWriter writer = Utils.getBufferedWriter("vcfs/" + interval + ".vcf.gz")) {

                    writer.write(headerStr);

                    while (vcfLine != null) {
                        String vcfTokens[] = vcfLine.split("\t", 3);
                        String currentChr = vcfTokens[0];
                        int currentPos = Integer.parseInt(vcfTokens[1]);
                        if (currentChr.equals(chr) && currentPos >= start && currentPos <= end) {
                            writer.write(vcfLine);
                            writer.write("\n");
                        } else {
                            break;
                        }
                        vcfLine = reader.readLine();
                    }

                }

                interval = intervals.readLine();
            }

            return null;

        } catch (Exception e) {
            throw new IllegalStateException("SplitVCFIntoIntervalsPlugin: processData: problem splitting: " + input() + " with intervals: " + intervalFile());
        }

    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Split VCF into Intervals";
    }

    @Override
    public String getToolTipText() {
        return "Split VCF into Intervals";
    }

    /**
     * Input VCF file
     *
     * @return Input
     */
    public String input() {
        return myInput.value();
    }

    /**
     * Set Input. Input VCF file
     *
     * @param value Input
     *
     * @return this plugin
     */
    public SplitVCFIntoIntervalsPlugin input(String value) {
        myInput = new PluginParameter<>(myInput, value);
        return this;
    }

    /**
     * Intervals file
     *
     * @return Intervals
     */
    public String intervalFile() {
        return myIntervalFile.value();
    }

    /**
     * Set Intervals. Intervals file
     *
     * @param value Intervals
     *
     * @return this plugin
     */
    public SplitVCFIntoIntervalsPlugin intervalFile(String value) {
        myIntervalFile = new PluginParameter<>(myIntervalFile, value);
        return this;
    }

}
