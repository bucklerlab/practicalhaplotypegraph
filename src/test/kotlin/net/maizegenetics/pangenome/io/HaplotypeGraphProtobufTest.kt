package net.maizegenetics.pangenome.io

import net.maizegenetics.pangenome.api.VariantUtils
import net.maizegenetics.pangenome.api.buildSimpleConsensusGraph
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

class HaplotypeGraphProtobufTest {

    private val baseDir = System.getProperty("user.home")
    private val protobufDir = "${baseDir}/temp/protobuf"
    private val outputProtobufFile = "${protobufDir}/dummyGraph.proto"
    private val outputConsensusProtobufFile = "${protobufDir}/dummyConsensusGraph.proto"

    @BeforeEach
    fun before() {
        File(protobufDir).mkdirs()
        File(outputProtobufFile).delete()
        File(outputConsensusProtobufFile).delete()
    }

    @Test
    fun testExportingImportingGraphToProtobuf() {
        val graph = buildSimpleDummyGraph()
        toProtobuf(graph, outputProtobufFile, true, true, null)
        val gvcfMap1 = VariantUtils.getGvcfRemoteToLocalFiles()
        val graph2 = toGraph(outputProtobufFile)
        val gvcfMap2 = VariantUtils.getGvcfRemoteToLocalFiles()
        assert(compareGraphs(graph, graph2, gvcfMap1, gvcfMap2)) { "Graphs are not equal" }
    }

    @Test
    fun testProtobufWithConsensusGraph() {
        val graph = buildSimpleConsensusGraph(100, 100, 100)
        toProtobuf(graph, outputConsensusProtobufFile, true, true, null)
        val graph2 = toGraph(outputConsensusProtobufFile)
        assert(compareGraphs(graph, graph2)) { "Graphs are not equal" }
    }

}