
fasta_f=$1
kmer_ln=${2:-11}
outf_base=${3:-"$(dirname $fasta_f)/$(basename ${fasta_f%.*}).${kmer_ln}mer_count"}
nr_threads=${4:-40}
k_mem=${5:-5}
all_mem=${6:-20}

#jf_bin=$(which jellyfish)

$jf_bin count -m $kmer_ln -s ${k_mem}G --bf-size ${all_mem}G -t $nr_threads -o ${outf_base}.jf $fasta_f
$jf_bin dump -c -t -o ${outf_base}.tsv ${outf_base}.jf
