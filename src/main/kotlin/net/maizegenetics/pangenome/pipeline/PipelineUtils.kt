package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.liquibase.CheckDBVersionPlugin
import net.maizegenetics.pangenome.liquibase.LiquibaseUpdatePlugin
import net.maizegenetics.util.Utils
import java.io.File
import java.util.*
import kotlin.collections.HashMap

/**
 * This returns a String indicating all went well, or that the db needs updating.
 * This is a String vs boolean as we will in the future add a PHG version to run
 * if the user does not want to update the db.
 *
 * Change sets are defined with "–changeset AUTHOR:ID" lines.
 * We can add PHG Version at end of the id.
 * In the status file, when verbose is on, you get listing of changesets to be applied, e.g.:
 * 9 change sets have not been applied to null@jdbc:sqlite:/tempFileDir/outputDir/goodLiquibaseDocker.db
 *  /liquibase/changelogs/changesets/add_readMappingTables.sql::createReadMappingTable_sqlite::lcj34
 *  /liquibase/changelogs/changesets/add_readMappingTables.sql::create_read_mapping_unique_idx::lcj34
 *  /liquibase/changelogs/changesets/add_readMappingTables.sql::create_read_mapping_genoid_idx::lcj34
 *  /liquibase/changelogs/changesets/alterPathsTable_forReadMapping.sql::updatepathstable_sqlite::lcj34
 *   ...
 * In the above, "createReadMappingTable_sqlite" would become "createReadMappingTable_sqlite_0.0.18" where
 * 0.0.18 is the last PHG version that does NOT have this update.
 *
 * The only valid commands are "status" or "update".
 *
 * The return will look like  "NEEDS_UPDATING:<PHG VERSION>" or "UP_TO_DATE"
 */
fun runLiquibaseCommand(command: String,  outputDir: String, liquibaseDir:String?): String {

    // First run CheckDBVersionPlugin

    // db comes from the configFile, which is defined in the ParameterCache.
    // This must be set on the command line for the initial plugin that is called.
    CheckDBVersionPlugin(null, false)
            .outputDir(outputDir)
            .performFunction(null)

    // Verify the run_yes.txt file exists in the outputDir.  This was created via
    // CheckDBVersionPlugin run above
    var runYesFile = outputDir + "/run_yes.txt"
    var runYes = File(runYesFile).exists()

    if (!runYes) {
        throw IllegalStateException("ERROR:  CheckDBVersionPlugin indicates databse is too old to be updated.  You must either create a new database using current PHG docker image, or re-run this pipeline using the PHG docker with which your databse was created")
    }

    // Run requested Liquibase command
    if (liquibaseDir != null) {
        LiquibaseUpdatePlugin(null,false)
            .command(command)
            .outputDir(outputDir)
            .liquibaseDir(liquibaseDir)
            .performFunction(null)
    } else {
        LiquibaseUpdatePlugin(null,false)
            .command(command)
            .outputDir(outputDir)
            .performFunction(null)
    }

    // Check command output
    if (command.toLowerCase().equals("status")) {
        // read output file,
        // determine if db needs updating
        // Once implemented, will need to parse the ID to get the last PHG version that didn't contain these updates.
        //var liquibaseOutputFile = outputDir + "/liquibase_status_output.log"
        var liquibaseErrorFile = outputDir + "/liquibase_status_error.log"
        var linesInError = Utils.getBufferedReader(liquibaseErrorFile).readLines()

        // This is correct - 4.7.0: "executed successfully" is in the error log
        var success = linesInError
                .filter {it.contains("executed successfully")}
                .toList()
        if (success == null || success.size == 0) {
            var liquibaseErrorFile = outputDir + "/liquibase_status_error.log"
            throw IllegalStateException("ERROR!! - liquibase status command encountered an error.  See file $liquibaseErrorFile for details.")
        }

        // With liquibase 4.7.0, the "have not been applied" text shows up in the liquibase_status_output.log,
        // not the liquibase_status_error.log
        val liquibaseLogFile = outputDir + "/liquibase_status_output.log"
        var linesInOutput = Utils.getBufferedReader(liquibaseLogFile).readLines()
        val needsUpdating = linesInOutput
                .filter { it.contains("have not been applied") }
                .toList()

        if (needsUpdating != null && needsUpdating.size > 0) {
            //  determine PHG version to run from data stored in change_log ID field
            // This presumes going forward, the id coded by the developer will always
            // end with _<phgVersion>  where <phgVersion> is 3 dotted numbers, e.g. 0.0.18
            val changeSets = linesInOutput
                    .filter { it.contains("changesets")}
                    .map { it -> parseChangesetVersion(it)}
                    .toList()

            // We now have a list of PHG versions - find the lowest one
            val phgVersionId = findUserPHGVersion(changeSets)

            return "NEEDS_UPDATING:$phgVersionId"
        } else{
            return "UP_TO_DATE"
        }
    } else if (command.toLowerCase().equals("update")) {
        // call liquibase with update
        // check file for errors
        // return UP_TO_DATE if good
        // throw exception if failed
        var liquibaseOutputFile = outputDir + "/liquibase_update_output.log"
        var linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

        var success = linesInOutput
                .filter {it.contains("Update has been successful")}
                .toList()
        if (success == null || success.size == 0) {
            var liquibaseErrorFile = outputDir + "/liquibase_update_error.log"
            throw IllegalStateException("ERROR! Liquibase update command failed.  See file $liquibaseErrorFile for details")
        }
        val updateResults = linesInOutput
                .filter { it.contains("Update has been successful") }
                .toList()

        if (updateResults != null && updateResults.size > 0) {
            return "UP_TO_DATE" // successful
        }
        else {
            // we shouldn't get here.  Liquibase prints "Update has been successful " on all updates
            // that had no error - whether or not updates needed to be, or were, applied.
            throw IllegalStateException("ERROR!  Liquibase update output does not show successful.  See file $liquibaseOutputFile for details")
        }

    } else {
        // unexpected command - return failure
        throw IllegalArgumentException("ERROR - liquibase command must be either status or update")
    }
}

/**
 * This function finds the PHG version from a changeset ID string.
 * The version should be the last value following the last underscore.
 * If there are no underscores, look up the change set in the map.
 *
 * First identify the changeSet ID from the string, then find the PHG version
 * Use map if there is no version string
 */
fun parseChangesetVersion(changeset: String): Triple<Int,Int,Int> {
    // create map of exisiting change sets without versions:
    var idToVersionMap = HashMap<String,String>()
    idToVersionMap.put("createReadMappingTable_sqlite","0.0.10")
    idToVersionMap.put("createReadMappingTable_postgres","0.0.10")
    idToVersionMap.put("create_read_mapping_unique_idx","0.0.10")
    idToVersionMap.put("create_read_mapping_genoid_idx","0.0.10")

    idToVersionMap.put("updatepathstable_sqlite","0.0.11")
    idToVersionMap.put("updatepathstable_postgres","0.0.11")

    idToVersionMap.put("update_readtablemappings_postgres","0.0.11")
    idToVersionMap.put("update_readtablemappings_sqlite","0.0.11")

    idToVersionMap.put("drop_read_mapping_read_mapping_group","0.0.13")
    idToVersionMap.put("drop_read_mapping_group","0.0.13")
    idToVersionMap.put("create_read_mapping_paths_postgres","0.0.13")
    idToVersionMap.put("create_read_mapping_paths_sqlite","0.0.13")
    idToVersionMap.put("update_paths_table_with_genoid_sqlite","0.0.13")
    idToVersionMap.put("update_paths_table_with_genoid_postgres","0.0.13")


    // Get the changeset ID.  The line will look something like:
    //   /liquibase/changelogs/changesets/add_readMappingTables.sql::createReadMappingTable_sqlite::lcj34
    // The changeset ID is between the set of ::
    var changeSetID = changeset.substringBeforeLast("::").substringAfter("::")


    // The change set may or may not include a PHG version.  If it does, it will look like:
    //   createReadMappingTable_sqlite_0.0.13
    // Check the map.  If not there, parse the changeset ID for the values beyond the last underscore
    var phgVersion = idToVersionMap.get(changeSetID)
    if (phgVersion == null) {
        // not found, it should have a version attached
        // If it doesn't, then Lynn messed up creating the changeset
        val lastUnderscore = changeSetID.lastIndexOf("_")
        if (lastUnderscore < 0) {
            throw IllegalArgumentException("parseChangesetVersion: bad changeset value, missing underscore and not in map: $changeset")
        }
        phgVersion = changeSetID.substring(lastUnderscore+1)
    }

    val versionList = phgVersion.split(".")
    return Triple<Int,Int,Int>(versionList[0].toInt(), versionList[1].toInt(),versionList[2].toInt())
}

/**
 * This function finds the lowest PHG version from a list of PHG versions.
 * The return value should be of the format X.X.X
 */
fun findUserPHGVersion(versionList: List<Triple<Int,Int,Int>>): String {

    val comparatorTriple = ComparatorTriple()
    val sortedList = versionList.sortedWith(comparatorTriple)

    val phgVersion = sortedList[0].first.toString() + "." +  sortedList[0].second.toString() + "." + sortedList[0].third.toString()
    return phgVersion
}
// Class to compare PHG versions.  Because we have both 0.0.2 and 0.0.18, where the
// latter is a later version, we cannot do straight lexicographic ordering to get the
// earlist version.
class ComparatorTriple: Comparator<Triple<Int,Int,Int>> {

    override fun compare(o1: Triple<Int, Int, Int>, o2: Triple<Int, Int, Int>): Int {
        var choice = o1.first.compareTo(o2.first)
        if (choice == 0) {
            choice = o1.second.compareTo(o2.second)
        }
        if (choice == 0) {
            choice = o1.third.compareTo(o2.third)
        }
        return choice
    }
}

