@file:JvmName("SFTPConnection")
package net.maizegenetics.pangenome.io

import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.transport.verification.PromiscuousVerifier
import org.apache.logging.log4j.LogManager


/**
 * Simple SFTPConnection class wrapper. This allows you to see what files are at the remote directory and to download and upload files to the remote server.
 *
 * @author zrm22
 */
class SFTPConnection : AutoCloseable{

    // Late Initing this so its non-nullable
    lateinit var client : SSHClient

    private val myLogger = LogManager.getLogger(SFTPConnection::class.java)

    /**
     * Function to create a SSH client connection and setup the lateinit variable.
     *
     *
     * If requireVerification is set to true, it will look at the system's known_hosts file to make sure we can create a secure SSH connection.
     * If set to false, it will create the connection regardless of if the remote server is actually trusted or not.
     */
    fun createConnection(host: String, username: String, password: String, requireVerification : Boolean = true) : SSHClient {
        client = SSHClient()

        if (requireVerification) {
            client.loadKnownHosts()
        }
        else {
            client.addHostKeyVerifier(PromiscuousVerifier())
        }

        client.connect(host)
        client.authPassword(username, password)

        return client
    }

    /**
     * Function to list all of the file names on the remote server
     * This will simply return the file names not the full paths.
     * We assume that the client already knows of the path to download the files.
     */
    fun viewFiles(remoteFilePath: String) : List<String> {
        client.newSFTPClient().use {sftpClient ->
            return sftpClient.ls(remoteFilePath).map { it.name }
        }
    }

    /**
     * Function to upload a single file from the local directory to the remote directory.
     * This will close the sftp connection after the file is done uploading.
     */
    fun uploadFile(localFilePath : String, remoteFilePath: String, fileName: String) {
        client.newSFTPClient().use { sftpClient ->
            myLogger.info("Uploading file ${localFilePath}/${fileName} to remote path: ${remoteFilePath}")
            sftpClient.put("${localFilePath}/${fileName}", "${remoteFilePath}/${fileName}")
        }
    }

    /**
     * Function to upload a list of files to the remote directory.  Any file in fileNameList will be uploaded.
     */
    fun bulkFileUpload(localFilePath: String, remoteFilePath: String, fileNameList : List<String> ) {
        client.newSFTPClient().use { sftpClient ->
            for (fileName in fileNameList) {
                myLogger.info("Uploading file ${localFilePath}/${fileName} to remote path: ${remoteFilePath}")
                sftpClient.put("${localFilePath}/${fileName}", "${remoteFilePath}/${fileName}")
            }

            myLogger.info("Done uploading files")
        }
    }

    /**
     * Function to download a single file from the remote Directory.
     */
    fun downloadFile(localFilePath: String, remoteFilePath: String, fileName: String) {
        println("LCJ downloading file localFilePath:${localFilePath} remoteFilePath:${remoteFilePath} fileName:${fileName}")
        client.newSFTPClient().use { sftpClient ->
            sftpClient.get("${remoteFilePath}/${fileName}", "${localFilePath}/${fileName}")
        }
    }

    /**
     * Function to download a list of files from a remote directory.  The Connection object will be kept open until all the files are downloaded.
     */
    fun bulkFileDownload(localFilePath: String, remoteFilePath: String, fileNameList : List<String> ) {
        client.newSFTPClient().use { sftpClient ->
            for (fileName in fileNameList) {
                myLogger.info("Downloading file ${localFilePath}/${fileName} to remote path: ${remoteFilePath}")
                sftpClient.get("${remoteFilePath}/${fileName}", "${localFilePath}/${fileName}")
            }

            myLogger.info("Done downloading files")
        }
    }

    /**
     * Function to automatically close out this class so it can be used in a .use{} code block.
     */
    override fun close() {
        client.disconnect()
    }

}