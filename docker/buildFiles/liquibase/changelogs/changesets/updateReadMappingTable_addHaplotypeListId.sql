--liquibase formatted sql

--## add field haplotype_list_id to read_mapping table. This could be just 1 change set, but keeping
--## sqlite and postgres separate by convention.  NOTE: NO FK constrainst as -1 default will violote this!

--changeset lcj34:add_haplotype_list_id_to_readmapping_sqlite_0.0.32 dbms:sqlite
ALTER TABLE read_mapping ADD COLUMN haplotype_list_id INTEGER DEFAULT -1;

--changeset lcj34:add_haplotype_list_id_to_readmapping_postgres_0.0.32 dbms:postgresql
ALTER TABLE read_mapping ADD COLUMN haplotype_list_id INTEGER DEFAULT -1;