--liquibase formatted sql

--## create haplotype_list  tables for postgres and sqlite

--changeset lcj34:createHaplotypeListTable_sqlite_0.0.32 dbms:sqlite
DROP TABLE IF EXISTS haplotype_list;
CREATE TABLE haplotype_list(haplotype_list_id INTEGER PRIMARY KEY, list_hash TEXT UNIQUE, haplotype_list BLOB);


--changeset lcj34:createHaplotypeListTable_postgres_0.0.32 dbms:postgresql
DROP TABLE IF EXISTS haplotype_list;
CREATE TABLE haplotype_list(haplotype_list_id SERIAL PRIMARY KEY, list_hash TEXT UNIQUE, haplotype_list bytea);