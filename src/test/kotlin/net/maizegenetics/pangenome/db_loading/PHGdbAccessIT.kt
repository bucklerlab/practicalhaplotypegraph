package net.maizegenetics.pangenome.db_loading

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.assertNull
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertFailsWith

@ExtendWith(SmallSeqExtension::class)
class PHGdbAccessIT {

    @Test
    fun testAddTaxaGroup() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val lineA = "LineA"
        val lineA_Assembly = "LineA_Assembly"
        val lineB_Assembly = "LineB_Assembly"
        val testTaxaGroup = "Test_Taxa_Group"

        val existingTaxaList = phg.dbTaxaNames
        assertTrue(existingTaxaList.contains(lineA))
        assertTrue(existingTaxaList.contains(lineA_Assembly))
        assertFalse(existingTaxaList.contains("Nonexistent_Taxa"))

        val groupNamesBeforeAddition = phg.allTaxaGroupNames
        phg.putTaxaTaxaGroups(testTaxaGroup, listOf(lineA_Assembly, lineB_Assembly))

        val returnedList = phg.getTaxaForTaxaGroup(testTaxaGroup)
        assertTrue(returnedList.contains(lineA_Assembly))
        assertEquals(2, returnedList.size)

        val groupNamesAfterAddition = phg.allTaxaGroupNames
        assertEquals(
            groupNamesBeforeAddition.size + 1,
            groupNamesAfterAddition.size
        )
        assertTrue(groupNamesAfterAddition.contains(testTaxaGroup))

        phg.close()
    }

    @Test
    fun testGetReadMappingIdsForTaxaMethod() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val taxaList = mutableListOf<String>()

        val methodTestImpute = "SmallSeqTestImputeReadMethod"
        val methodGatk = "GATK_PIPELINE"

        // TODO: Additional methods to consider?

        val rmIds_1 = phg.getReadMappingIdsForTaxaMethod(taxaList, methodTestImpute)
        //Load in the keyFile and get the number of lines
        val keyFileLines = Utils.getBufferedReader(SmallSeqExtension.genotypingKeyFilePath).readLines()
        assertEquals(keyFileLines.size -1 , rmIds_1.size) // Need to subtract 1 for header.

        val rmIds_2 = phg.getReadMappingIdsForTaxaMethod(taxaList, methodGatk)
        assertEquals(0, rmIds_2.size)

        taxaList.add("LineA_wgs")
        taxaList.add("LineB1_wgs")
        taxaList.add("RecLineB1LineA1gco1_wgs")
        taxaList.add("RecRefA1LineA1gco4_wgs")
        val rmIds_3 = phg.getReadMappingIdsForTaxaMethod(taxaList, methodTestImpute)
        assertEquals(4, rmIds_3.size)

        val rmIds_4 = phg.getReadMappingIdsForTaxaMethod(taxaList, methodGatk)
        assertEquals(0, rmIds_4.size)

        taxaList.add("LineA")
        taxaList.add("LineB1")
        val rmIds_5 = phg.getReadMappingIdsForTaxaMethod(taxaList, methodTestImpute)
        assertEquals(4, rmIds_5.size)

        phg.close()
    }

    @Test
    fun testDeletePathsByMethodName() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val method1 = "B73Ref_method_PATH"
        val method2 = "anchorwave_assembly_PATH"
        val method3 = "GATK_PIPELINE_PATH"

        val allSmallSeqTaxa = listOf(
            "Ref_Assembly",
            "LineA_Assembly",
            "LineB_Assembly",
            "Ref",
            "RefA1",
            "LineB1",
            "LineA",
            "LineB",
            "LineA1",
        )

        // TODO: Is there a convenience method to get total number of paths?

        val methodPathMap1 = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method1)
        val numPaths1 = methodPathMap1.values.flatten().size

        val methodPathMap2 = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method2)
        val numPaths2 = methodPathMap2.values.flatten().size

        val methodPathMap3 = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method3)
        val numPaths3 = methodPathMap3.values.flatten().size

        val numAllPaths = numPaths1 + numPaths2 + numPaths3
        assertEquals(9, numAllPaths) // TODO: Alternative to magic number?

        phg.deletePaths(method1, null)
        val methodPathMap1AfterDelete = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method1)
        val numPaths1AfterDelete = methodPathMap1AfterDelete.values.flatten().size
        assertEquals(0, numPaths1AfterDelete)

        phg.deletePaths(method2, null)
        val methodPathMap2AfterDelete = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method1)
        val numPaths2AfterDelete = methodPathMap2AfterDelete.values.flatten().size
        assertEquals(0, numPaths2AfterDelete)

        phg.deletePaths(method3, null)
        val methodPathMap3AfterDelete = phg.getPathsForTaxonMethod(allSmallSeqTaxa, method1)
        val numPaths3AfterDelete = methodPathMap3AfterDelete.values.flatten().size
        assertEquals(0, numPaths3AfterDelete)

        phg.close()
    }

    @Test
    fun testDeletePathsByTaxaList() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val methodName = "GATK_PIPELINE_PATH"
        val taxaList = listOf("Ref", "RefA1", "LineA", "LineB", "LineA1", "LineB1")

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaList, methodName)
        assertEquals(6, pathsForMethodMapBefore.size)

        phg.deletePaths(null, taxaList)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaList, methodName)
        assertEquals(0, pathsForMethodMapAfter.size)

        assertTrue(phg.getMethodIdFromName(methodName) > 0)

        phg.close()
    }

    @Test
    fun testDeletePathsByMethodNameTaxaList() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val methodName = "GATK_PIPELINE_PATH"
        val taxaList = listOf("Ref", "RefA1", "LineA", "LineB", "LineA1", "LineB1")

        assertTrue(phg.getMethodIdFromName(methodName) is Int)
        // This should fail because method is still referenced in `paths` table
        assertFailsWith<IllegalArgumentException> { phg.deleteMethodByName(methodName) }

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaList, methodName)
        assertEquals(6, pathsForMethodMapBefore.size)

        phg.deletePaths(methodName, taxaList)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaList, methodName)
        assertEquals(0, pathsForMethodMapAfter.size)

        phg.deleteMethodByName(methodName)
        assertTrue(phg.getMethodIdFromName(methodName) < 1)

        phg.close()
    }

    @Test
    fun testDeleteMethodByName() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val methodName = "Test_Delete_By_Name"
        val methodDesc = HashMap<String,String>()
        methodDesc.put("foo", "bar")
        phg.putMethod(methodName, DBLoadingUtils.MethodType.PATHS, methodDesc);

        var methodId = phg.getMethodIdFromName(methodName)
        assertTrue(methodId > 0)

        phg.deleteMethodByName(methodName)
        methodId = phg.getMethodIdFromName(methodName)
        assertTrue(methodId < 1)

        phg.close()
    }

    @Test
    fun testDeleteReadMappingsCascade() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val (columns, data) = readInKeyFile(SmallSeqExtension.genotypingKeyFilePath)
        val taxaList = data.map { it[0] } // Assume `cultivar` is first column

        val readMappingIds_before = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(taxaList.size, readMappingIds_before.size)

        assertTrue(phg.deleteReadMappingsCascade(readMappingIds_before))

        val readMappingIds_after = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(0, readMappingIds_after.size)

        assertTrue(phg.getMethodIdFromName(SmallSeqExtension.imputeMethod) > 0)

        phg.close()
    }

    @Test
    fun testGetMethodDescriptionFromName() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false)
        val phg = PHGdbAccess(connection)

        val paramsMap = phg.getMethodDescriptionFromName("GATK_PIPELINE")
        assertEquals(11, paramsMap.keys.size)

        val paramsMapNonexistent = phg.getMethodDescriptionFromName("This_Method_Does_Not_Exist");
        assertNull(paramsMapNonexistent)
    }
}