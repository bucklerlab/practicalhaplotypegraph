package net.maizegenetics.pangenome.api

import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import kotlin.test.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

/**
 * ## Testing for rPHG methods
 * The general purpose of this class is to test methods from the
 * `net.maizegenetics.pangenome.api.RPHGMethodsKotlin` class.
 *
 * class.
 *
 * For these methods, a small-seq database is constructed using the method
 * `createSmallSeqForTest()` method. If starting from scratch, the tester
 * needs to accomplish the following prerequisites:
 *  1. Create directory `<user_home>/temp/smallseq_docker`
 *  2. From the master PHG repository branch, create a `phg.jar` file
 *  3. Add file `phg.jar` to `<user_home>/temp/smallseq_docker`
 *  4. Create file named `Dockerfile` and add it to `<user_home>/temp/smallseq_docker`
 *
 * The `Dockerfile` will need the following lines added:
 *
 * ```
 * FROM phgrepository_base:latest
 * ADD ./phg.jar /tassel-5-standalone/lib/
 * WORKDIR /
 * ```
 *
 * Once finished with initial setup, you are free to run these tests.
 *
 * @see RPHGMethodsKotlin
 */
@ExtendWith(SmallSeqExtension::class)
class RPHGMethodsKotlinIT {
    val rphgMethods = RPHGMethodsKotlin()

    // Get metadata for pulled node (used for verifying GVCF data)
    data class HapNodeStats(val chr: Int, val taxa: String, val start: Int, val end: Int)

    @Test
    fun testGetHapIdSeq() {
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .includeSequences(true)
            .methods("GATK_PIPELINE")
            .build()

        // 51 and 66 are valid, others are not
        val hapIdsForTesting = listOf(51, 66, 12345, 23435, 45678)

        // Generate expected sequence data from synthesized database
        val expectSeq = mutableListOf<String>()
        val hapNodeStats = mutableListOf<HapNodeStats>()
        graph.nodeStream().forEach { node ->
            if (node.id() == hapIdsForTesting[0]) {
                // Add expected sequence data
                expectSeq.add(node.haplotypeSequence().sequence()!!)

                // Add metadata (for pulling GVCF info)
                hapNodeStats.add(
                    HapNodeStats(
                        node.referenceRange().chromosome().chromosomeNumber,
                        node.taxaList().taxaName(0),
                        node.referenceRange().start(),
                        node.referenceRange().end()
                    )
                )
            }
        }

        // Verify that expectSeq variable contains one sequence when haplotype ID 51 is passed
        assertEquals(1, expectSeq.size)

        // Check if method will return valid haplotype ID
        val myIds1 = IntArray(1)
        myIds1[0] = hapIdsForTesting[0]
        val expectedReturn1 = RPHGMethodsKotlin.HaplotypeSequences(hapIdsForTesting[0], expectSeq[0])
        val testReturn1 = rphgMethods.getHapIdSeq(graph, myIds1)
        assertEquals(1, testReturn1.size)
        assertEquals(expectedReturn1, testReturn1[0])

        // Check GVCF directly
        val refPath = "${SmallSeqExtension.refDir}/Ref.fa"
        val gvcfPath = "${SmallSeqExtension.inputGVCFDir}/${hapNodeStats[0].taxa}_haplotype_caller_output_filtered.g.vcf.gz"
        val gvcfSequence = GVCFSequence.instance(refPath, gvcfPath)
        val expectGVCFSeq = gvcfSequence.genotypeAsString(
            Chromosome.instance(hapNodeStats[0].chr.toString()),
            hapNodeStats[0].start,
            hapNodeStats[0].end
        )
        assertEquals(expectGVCFSeq, testReturn1[0].sequence)

        // Check return methods
        assertEquals(hapIdsForTesting[0], testReturn1[0].hapId)
        assertEquals(expectSeq[0], testReturn1[0].sequence)

        // Check if method will return valid haplotype IDs
        val myIds2 = IntArray(3)
        myIds2[0] = hapIdsForTesting[0]
        myIds2[1] = hapIdsForTesting[1]
        myIds2[2] = hapIdsForTesting[2] // should not return
        val testReturn2 = rphgMethods.getHapIdSeq(graph, myIds2)
        assertEquals(2, testReturn2.size)

        // Check to see nothing will be returned if no valid IDs are passed
        val myIds3 = IntArray(3)
        myIds3[0] = hapIdsForTesting[2]
        myIds3[1] = hapIdsForTesting[3]
        myIds3[2] = hapIdsForTesting[4]
        val testReturn3 = rphgMethods.getHapIdSeq(graph, myIds3)
        assertEquals(0, testReturn3.size)
    }
}


