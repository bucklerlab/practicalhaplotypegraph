Evaluating alignment produced by Zack on 7/27/2017

Took the file - converted all gaps to N, and then examined whether there were non-B73 clusters
Comments on various trees:
* 10_45702962-45704205 - looks pretty good.
* 10_72345008-72358928 - looks like the old problem only B73 collapses
* 10_53540016-53540394 - too short to be meaningful

Many of the anchors with many Ns are very small.  Are these pseudogenes?
However, 10_3840240-3855309 is nearly 15kb.  And there are no taxa like B73.  This does not make sense.


