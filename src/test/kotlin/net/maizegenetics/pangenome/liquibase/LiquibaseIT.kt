package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.pipeline.ImputePipelinePlugin
import net.maizegenetics.pangenome.pipeline.PopulatePHGDBPipelinePlugin
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/*
    TODO: Work in progress

    NOTES FOR LOCAL DEVELOPMENT:
    1. Download Liquibase to ~/temp/phgSmallSeq/liquibase-release
       https://github.com/liquibase/liquibase/releases/tag/v4.7.0
    2. Copy docker/buildFiles/liquibase/changelogs directory into above
    3. Install Liquibase (if on Mac, use installer - same 4.7.0 version)
 */

@ExtendWith(SmallSeqExtension::class)
class LiquibaseIT {

    // TODO: Copied from CheckDBVersionPluginIT - remove code duplication
    companion object {

        val configWithSchema = "${SmallSeqExtension.liquibaseDir}/with_schema_config.txt"
        val configWithoutSchema = "${SmallSeqExtension.liquibaseDir}/without_schema_config.txt"

        val goodDB = "${SmallSeqExtension.liquibaseDir}/good.db"
        val badDB = "${SmallSeqExtension.liquibaseDir}/bad.db"

        val yesFile = File("${SmallSeqExtension.liquibaseDir}/run_yes.txt")
        val noFile = File("${SmallSeqExtension.liquibaseDir}/run_no.txt")
    }

    // TODO: Copied from CheckDBVersionPluginIT - remove code duplication
    @BeforeEach
    fun createData() {
        FileUtils.cleanDirectory(SmallSeqExtension.liquibaseDirFile)

        val withSchemaWriter = Utils.getBufferedWriter(configWithSchema)
        withSchemaWriter.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=$goodDB\nDBtype=sqlite\n")
        withSchemaWriter.close()

        val withoutSchemaWriter = Utils.getBufferedWriter(configWithoutSchema)
        withoutSchemaWriter.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=$badDB\nDBtype=sqlite\n")
        withoutSchemaWriter.close()

        createDBs(goodDB,true)
        createDBs(badDB, false)
    }

    @Test
    fun testLiquibaseDefaultDir() {
        // Test the correct default dir for IT testing is returjed from
        // defineDefaultLiquibaseDir() method.
        // This method is called from LiquibaseUpdatePlugin and MakeInitialPHGdbPlugin
        // when no value is sent for the liauivaseDir parameter.
        val expected1 = "${SmallSeqExtension.smallSeqDir}/liquibase-release"
        val expected2 = "/liquibase"
        val actual = defineDefaultLiquibaseDir()

        if (System.getenv("PHG_ENV") != null && System.getenv("PHG_ENV") == "docker") {
            assertEquals(expected2, actual)
        } else {
            assertEquals(expected1, actual)
        }

    }

    @Test
    fun testLiquibasePostgres() { statusAndUpdateCommand(SmallSeqExtension.configFilePath) }

    @Test
    fun testLiquibaseSQLite() { statusAndUpdateCommand(configWithSchema) }

    @Test
    fun testPopulatePHGDBPipelinePlugin_lb() {
        // if you run this without an assemblyKeyFile, and without WGS keyfile
        // and with no method for consensus, it will merely hit the liquibase
        // code and then stop.  The liquibase code is all we want to test here.

        // Write test config file.  We don't use the one in the SmallSeqExtension
        // because it contains the assemblyKeyFile and the WGS keyfile which we don't want.
        val outputDir="${SmallSeqExtension.liquibaseDir}"
        val pipelineConfigFile = "${SmallSeqExtension.smallSeqDir}/tesPipelineConfig.txt"
        writePipelineConfigFile(pipelineConfigFile)
        ParameterCache.load(pipelineConfigFile)
        PopulatePHGDBPipelinePlugin(null, false)
            .gvcfServerPath("dummyServerPath")
            .liquibaseOutputDir(outputDir)
            .performFunction(null)

        val statusOutput = "${SmallSeqExtension.liquibaseDir}/liquibase_status_output.log"

        val statusLines = Utils.getBufferedReader(statusOutput).readLines()
        assertTrue(statusLines[0].contains("up to date"))

    }

    @Test
    fun testImputePipelinePlugin() {
        // When given "config" as the target, the test will merely write
        // a config file but not impute anything.  But it will hit the liquibase
        // code before doing that.  This is what we are testing here - just the
        // liqibase code.
        // Note there are 2 config files here:  the one for the parameter cache which
        // holds db connection data, and the one the plugin will create
        val outputDir="${SmallSeqExtension.liquibaseDir}"
        val configToCreate = "${SmallSeqExtension.smallSeqDir}/testPipelineConfig.txt"

        // If it already exists, delete the config file we're asking the plugin to create,
        val path  = Paths.get(configToCreate)
        Files.deleteIfExists(path)

        val pipelineConfigFile = "${SmallSeqExtension.smallSeqDir}/pipelineConfig.txt"
        writePipelineConfigFile(pipelineConfigFile)
        ParameterCache.load(pipelineConfigFile)
        ImputePipelinePlugin(null, false)
            .imputeTarget(ImputePipelinePlugin.IMPUTE_TASK.config)
            .inputType(ImputePipelinePlugin.INPUT_TYPE.vcf)
            .configFile(configToCreate)
            .liquibaseOutputDir(outputDir)
            .readMethod("testRead")
            .localGVCFFolder(SmallSeqExtension.inputGVCFDir)
            .performFunction(null)

        val statusOutput = "${SmallSeqExtension.liquibaseDir}/liquibase_status_output.log"

        val statusLines = Utils.getBufferedReader(statusOutput).readLines()
        assertTrue(statusLines[0].contains("up to date"))
    }

    private fun writePipelineConfigFile(configFile:String) {
        // Delete the file if it  currently exists
        val path = Paths.get(configFile)
        Files.deleteIfExists(path)

        // write config file with only db connection info
        val writer = Utils.getBufferedWriter(configFile)
        writer.write("host=${SmallSeqExtension.dbFull}\nuser=postgres\npassword=postgres\nDB=${SmallSeqExtension.dbName}\nDBtype=postgres\n")
        writer.close()
    }

    private fun statusAndUpdateCommand(configFilePath: String) {
        val successSuffix = "is up to date"
        val statusOutput = "${SmallSeqExtension.liquibaseDir}/liquibase_status_output.log"
        val statusError = "${SmallSeqExtension.liquibaseDir}/liquibase_status_error.log"
        val updateError = "${SmallSeqExtension.liquibaseDir}/liquibase_update_error.log"

        ParameterCache.load(configFilePath)
        CheckDBVersionPlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .performFunction(null)

        assertTrue(yesFile.exists())

        // Part 1: Run the `status` command
        LiquibaseUpdatePlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .command("status")
            .performFunction(null)

        val outputLines1 = Utils.getBufferedReader(statusOutput).readLines()
        assertTrue(outputLines1.size == 1)
        assertEquals(outputLines1.last().takeLast(successSuffix.length), successSuffix)

        val errorLines1 = Utils.getBufferedReader(statusError).readLines()
        assertEquals(errorLines1.last(), "Liquibase command 'status' was executed successfully.")

        // Part 2: Run the `update` command
        LiquibaseUpdatePlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .command("update")
            .performFunction(null)

        val errorLines2 = Utils.getBufferedReader(updateError).readLines()
        assertEquals(errorLines2.last(), "Liquibase command 'update' was executed successfully.")

        // Part 3: Run the `status` command again
        LiquibaseUpdatePlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .command("status")
            .performFunction(null)

        val outputLines3 = Utils.getBufferedReader(statusOutput).readLines()
        assertTrue(outputLines3.size == 1)
        assertEquals(outputLines3.last().takeLast(successSuffix.length), successSuffix)

        val errorLines3 = Utils.getBufferedReader(statusError).readLines()
        assertEquals(errorLines3.last(), "Liquibase command 'status' was executed successfully.")

        // Part 4: Run the `update` command again
        LiquibaseUpdatePlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .command("update")
            .performFunction(null)

        val errorLines4 = Utils.getBufferedReader(updateError).readLines()
        assertEquals(errorLines4.last(), "Liquibase command 'update' was executed successfully.")
    }
}