#functions ----------------------------
plotrow <- function(x) {
  n = length(x)
  y = rep(1,n)
  prevx = -1
  for (i in 1:n) {
    if (x[i] == prevx) y[i] = y[i - 1] + 1 else prevx = x[i]
  }
  y
}

plot.ranges <- function(start, end, edges, vertices) {
  ndx.vert = which(vertices$range >= start & vertices$range <= end)
  ndx.vert.start = which(vertices$range >= start & vertices$range <= end-1)
  ndx.edge = which(edges$from %in% vertices$id[ndx.vert.start])
  g.vert = vertices[ndx.vert,]
  g.edge = edges[ndx.edge,]
  ndx2.vert = order(g.vert$range, -g.vert$ntaxa)
  g.vert = g.vert[ndx2.vert,]
  y = g.vert$range
  layout.matrix = cbind(y, plotrow(y))
  g = graph_from_data_frame(d = g.edge, vertices = g.vert)
  ndx = which(V(g)$ispath=='TRUE')
  g.color = rep('yellow', nrow(g.vert))
  g.color[g.vert$istarget=='TRUE'] = 'light green'
  g.color[g.vert$ispath=='TRUE'] = 'red'
  g1 = set_vertex_attr(graph = g, name = 'color', value = g.color)
  plot(g1, layout=layout.matrix, edge.arrow.size=0.3,edge.width=E(g)$prob*5)
  
}

#end functions ---------------------

#load igraph package
library("igraph")
#PHG
#phg.edges = read.delim('temp/phg/B97_outgraph_chr_1_edges.txt')
#phg.vertices = read.delim('temp/phg/B97_outgraph_chr_1_vertices.txt')

phg.edges = read.delim('Documents/projects/phg/training/w22_trainindb_chr_8_edges.txt')
phg.vertices = read.delim('Documents/projects/phg/training/w22_trainindb_chr_8_vertices.txt')
#View (RStudio command) the vertices sorted by range
View(phg.vertices[order(phg.vertices$range),])

#subgraph to display. plot.ranges function does the same thing
start=71
end=80
ndx.vert = which(phg.vertices$range >= start & phg.vertices$range <= end)
ndx.vert.start = which(phg.vertices$range >= start & phg.vertices$range <= end-1)
ndx.edge = which(phg.edges$from %in% phg.vertices$id[ndx.vert.start])
g.vert = phg.vertices[ndx.vert,]
g.edge = phg.edges[ndx.edge,]
ndx2.vert = order(g.vert$range, -g.vert$ntaxa)
g.vert = g.vert[ndx2.vert,]
y = g.vert$range
layout.matrix = cbind(y, plotrow(y))
g = graph_from_data_frame(d = g.edge, vertices = g.vert)
ndx = which(V(g)$ispath=='TRUE')
g.color = rep('yellow', nrow(g.vert))
g.color[g.vert$istarget=='TRUE'] = 'light green'
g.color[g.vert$ispath=='TRUE'] = 'red'
g1 = set_vertex_attr(graph = g, name = 'color', value = g.color)
plot(g1, layout=layout.matrix, edge.arrow.size=0.3,edge.width=E(g)$prob*5)

#with the function
plot.ranges(171, 190, phg.edges, phg.vertices)

#some summary functions
table.range = table(phg.vertices$range)
hist(table.range,breaks=21)
hist(table.range[table.range>20],breaks=21)
hist(table.range[table.range<=20],breaks=seq(from=-0.5, to=20.5, by = 1))

#histogram of ranges called incorrectly
wrong.ndx = which(phg.vert$istarget=='TRUE' & phg.vert$ispath=='FALSE')
hist(wrong.ndx, breaks=101)

#density plot of the same
count.wrong = rep(0,nrow(phg.vert))
count.wrong[wrong.ndx] = 1
cdf.wrong = cumsum(count.wrong)
x = seq(from=phg.vert$start[1], to=phg.vert$start[nrow(phg.vert)],length.out = 500)
ss = smooth.spline(phg.vert$start, cdf.wrong, spar=0.1)
plot(predict(ss, x, deriv=1), type='l')

#haplotypes per range
range.hapcount = tapply(phg.vert$id, phg.vert$range, length)
range.start = tapply(phg.vert$start, phg.vert$range, mean)
plot(range.start, range.hapcount, type='l')
plot(1:length(range.hapcount), range.hapcount, type='l', xlab='Range', ylab ='Number of Haplotypes')

#vertices at a range
View(phg.vertices[which(phg.vertices$range==182),])

#evaluate path vs target
sum(phg.vertices$ispath==T)
sum(phg.vertices$istarget==T)
sum(phg.vertices$ispath==T & phg.vertices$istarget==T)
sum(phg.vertices$ispath==T & phg.vertices$istarget==F)
conflited.ranges = which(phg.vertices$ispath==T & phg.vertices$istarget==F)
