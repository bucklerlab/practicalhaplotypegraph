package net.maizegenetics.junit

import net.maizegenetics.pangenome.pipeline.ImputePipelinePlugin
import net.maizegenetics.pangenome.pipeline.MakeInitialPHGDBPipelinePlugin
import net.maizegenetics.pangenome.pipeline.PopulatePHGDBPipelinePlugin
import net.maizegenetics.pangenome.smallseq.CreateSmallGenomesPlugin
import net.maizegenetics.plugindef.ParameterCache
import java.io.File
import java.nio.file.Paths
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.BeforeAllCallback
import net.maizegenetics.util.LoggingUtils
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeEachCallback

class SmallSeqExtension : BeforeEachCallback, AfterEachCallback, BeforeAllCallback {

    companion object {
        val smallSeqDir = "${System.getProperty("user.home")}/temp/phgSmallSeq"
        val rootDir = "$smallSeqDir/SmallSeqExtension"
        val testDataDir = "$smallSeqDir/testData"
        val testTmpDir = "$smallSeqDir/testTmp" // "Scratch" directory for unit test files
        val baseDir = "$rootDir/dockerBaseDir"
        val answerDir = "$rootDir/answer"
        val dumpDir = "$rootDir/dumps"
        val liquibaseDir = "$rootDir/liquibase"
        val liquibaseReleaseDir = if (System.getenv("DB_HOST") != null) "/liquibase" else "$smallSeqDir/liquibase-release"
        val inputGVCFDir = "$baseDir/inputDir/loadDB/gvcf/"
        val refDir = "$baseDir/inputDir/reference/"
        val assemblyFastaDir = "$baseDir/inputDir/assemblies/"
        val samFileDir = "$baseDir/samFiles/"
        val imputationFastqDir = "$baseDir/data/" // TODO: Same path as `dataDir`, consolidate
        val pangenomeDir = "$baseDir/pangenome/"
        val dataDir = "$baseDir/data/"
        val readMappingDir = "$baseDir/readMappingDebug/"  //No need to make this directory as it is made during the MakeDefaultDirectoryPlugin
        val readMappingDirKmers = "$baseDir/readMappingDebug_Kmers/"

        val configFilePath = "$baseDir/config.txt"
        val originalFilePath = "$answerDir/SmallSeq.vcf"
        val resultFilePath = "$baseDir/outputDir/output.vcf"
        val anchorBEDFilePath = "$answerDir/anchors.bed"
        val refFilePath = "$refDir/Ref.fa"
        val keyFilePath = "$baseDir/keyFile.txt"
        val genotypingKeyFilePath = "$baseDir/genotypingKeyFile.txt"
        val genotypingPathKeyFilePath = "$baseDir/genotypingKeyFile_pathKeyFile.txt"

        val dbName = "smallseq"
        val masterDump = "$dumpDir/$dbName.dump"

        val baseDirFile = File(baseDir)
        val answerDirFile = File(answerDir)
        val dumpDirFile = File(dumpDir)
        val liquibaseDirFile = File(liquibaseDir)
        val testDataDirFile = File(testDataDir)
        val testTmpDirFile = File(testTmpDir)

        val imputeMethod = "SmallSeqTestImputeReadMethod"
        val haplotypeMethod = "GATK_PIPELINE"
        val pathMethod = "GATK_PIPELINE_PATH_IMPUTED"

        val dbHost = System.getenv("DB_HOST") ?: "localhost"
        val dbPort = System.getenv("DB_PORT") ?: "5432"
        val dbFull = "$dbHost:$dbPort"
    }

    // Initializes SmallSeq
    override fun beforeAll(context: ExtensionContext) {
        LoggingUtils.setupDebugLogging()

        if (!File(masterDump).exists()) {
            baseDirFile.mkdirs()
            answerDirFile.mkdirs()
            dumpDirFile.mkdirs()
            liquibaseDirFile.mkdirs()
            testDataDirFile.mkdirs()
            testTmpDirFile.mkdirs()

            FileUtils.cleanDirectory(baseDirFile)
            FileUtils.cleanDirectory(answerDirFile)
            FileUtils.cleanDirectory(liquibaseDirFile)
            FileUtils.cleanDirectory(testTmpDirFile)

            CreateSmallGenomesPlugin()
                .geneLength(3000)
                .interGeneLength(3500)
                .numberOfGenes(10)
                .wgsDepth(10.0)
                .gbsDepth(0.05)
                .baseDirectory(rootDir)
                .performFunction(null)

            createConfig(configFilePath)

            ParameterCache.load(configFilePath)
            val make_db = MakeInitialPHGDBPipelinePlugin(null, false)
            make_db.setConfigParameters()
            make_db.localGVCFFolder(inputGVCFDir)
            make_db.performFunction(null)

            ParameterCache.load(configFilePath)
            val populate_db = PopulatePHGDBPipelinePlugin(null, false)
            populate_db.setConfigParameters()
            populate_db.performFunction(null)

            // NOTE: Config entries for these parameters are not picked up?
            ImputePipelinePlugin(null, false)
                .configFile(configFilePath)
                .skipLiquibaseCheck(true)
                .liquibaseOutputDir("$baseDir/tempFileDir")
                .readMethod(imputeMethod)
                .readMethodDescription("SmallSeq Impute Pipeline Read Method Description")
                .pathHaplotypeMethod(haplotypeMethod)
                .localGVCFFolder("$baseDir/localDownloadedGvcfs/")
                .inputType(ImputePipelinePlugin.INPUT_TYPE.fastq)
                .imputeTarget(ImputePipelinePlugin.IMPUTE_TASK.pathToVCF)
                .outVcfFile("$baseDir/outputDir/output.vcf")
                .pangenomeHaplotypeMethod(haplotypeMethod) //TODO get these tests working with Consensus
                .pangenomeDir(pangenomeDir)
                .performFunction(null)

            // Dump results of SmallSeq imputation to be used for each integration test
            dumpDatabase(masterDump)
        } else {
            val doCreate = dbAlreadyExists()
            restoreDatabase(doCreate)
        }
    }

    // Restore master database dump before each integration test method
    override fun beforeEach(context: ExtensionContext) {
        restoreDatabase()
    }

    // Dump database after each integration test method
    override fun afterEach(context: ExtensionContext) {
        val afterDumpFilename = "$dumpDir/after_${context.testMethod.get().name}.dump"

        dumpDatabase(afterDumpFilename)
    }

    private fun dumpDatabase(dumpOutput: String) {
        val dumpCommand = listOf(
            "pg_dump","-Fc",
            "--host=$dbHost",
            "--username=postgres",
            "--port=$dbPort",
            "--dbname=$dbName"
        )

        val dumpBuilder = ProcessBuilder(dumpCommand)
            .redirectOutput(File(dumpOutput))
        dumpBuilder.environment().put("PGPASSWORD", "postgres")
        val dumpProcess = dumpBuilder.start()
        dumpProcess.waitFor()
        handleProcessError(dumpProcess)
    }

    private fun restoreDatabase(doCreate: Boolean = false) {
        // NOTE: dbname is `postgres` during first restore because target db does not yet exist
        val restoreCommand = listOfNotNull(
            "pg_restore",
            "--no-owner",
            "--host=$dbHost",
            "--port=$dbPort",
            "--username=postgres",
            "--dbname=${if (doCreate) "postgres" else dbName}",
            if (doCreate) null else "--clean",
            if (doCreate) "--create" else null,
            masterDump
        )

        val restoreBuilder = ProcessBuilder(restoreCommand)
        restoreBuilder.environment().put("PGPASSWORD", "postgres")
        val restoreProcess = restoreBuilder.start()
        restoreProcess.waitFor()
        handleProcessError(restoreProcess)
    }

    private fun handleProcessError(proc: Process) {
        if (proc.exitValue() != 0) {
            throw RuntimeException(IOUtils.toString(proc.errorStream))
        }
    }

    private fun dbAlreadyExists(): Boolean {
        val psqlCommand = listOf(
            "psql",
            "--host=$dbHost",
            "--port=$dbPort",
            "--username=postgres",
            dbName, "-c", "''"
        )

        val psqlBuilder = ProcessBuilder(psqlCommand)
        psqlBuilder.environment().put("PGPASSWORD", "postgres")
        val psqlProcess = psqlBuilder.start()
        psqlProcess.waitFor()
        return psqlProcess.exitValue() != 1 // Code is 2 when nonexistent
    }

    private fun createConfig(config: String, configContents: String? = null) {
        // NOTE: host is 172.17.0.2 on CBSU but localhost in BitBucket Pipelines
        val configContents = configContents ?: """
            host=$dbFull
            user=postgres
            password=postgres
            DB=$dbName
            DBtype=postgres
            liquibaseOutdir=$liquibaseDir
            AssemblyMAFFromAnchorWavePlugin.outputDir=$baseDir/outputDir/align/
            AssemblyMAFFromAnchorWavePlugin.keyFile=$baseDir/asmMAF_keyFile.txt
            AssemblyMAFFromAnchorWavePlugin.gffFile=$baseDir/anchors.gff
            AssemblyMAFFromAnchorWavePlugin.refFasta=$baseDir/inputDir/reference/Ref.fa
            AssemblyMAFFromAnchorWavePlugin.threadsPerRun=2
            AssemblyMAFFromAnchorWavePlugin.numRuns=1
            bedFile=$baseDir/anchors.bed
            # Haplotype creation parameters
            referenceFasta=$baseDir/inputDir/reference/Ref.fa
            refServerPath=irods:/path/to/reference
            anchors=$baseDir/anchors.bed
            genomeData=$baseDir/inputDir/reference/Ref_Assembly_load_data.txt
            consensusMethodName=CONSENSUS
            asmMethodName=anchorwave
            asmKeyFile=$baseDir/asm_keyFile.txt
            AssemblyHaplotypesMultiThreadPlugin.isTestMethod=false
            outputDir=$baseDir/outputDir/align/
            gvcfOutputDir=$baseDir/outputDir/align/gvcfs/
            wgsMethodName=$haplotypeMethod
            haplotypeMethodName=$haplotypeMethod
            LoadHaplotypesFromGVCFPlugin.isTestMethod=false
            wgsKeyFile=$baseDir/keyFile.txt
            inputConsensusMethods=$haplotypeMethod
            gvcfDir=$baseDir/inputDir/loadDB/gvcf/
            PopulatePHGDBPipelinePlugin.gvcfServerPath=localhost;/remoteGvcfs/
            localGVCFFolder=$baseDir/localDownloadedGvcfs/
            isGvcfLocal=true
            extendedWindowSize=0
            GQ_min=50
            DP_poisson_min=.01
            DP_poisson_max=.99
            filterHets=true
            numThreads=5
            # HapCountBestPathToTextPlugin parameters
            maxNodesPerRange=30
            minTaxaPerRange=1
            minReads=0
            maxGBSReads=1000
            minTransitionProb=0.001
            probReadMappedCorrectly=0.99
            emissionMethod=allCounts
            splitTaxa=true
            # RunHapCollapsePipelinePlugin parameters
            minTaxa=2
            minSites=20
            clusteringMode=upgma_assembly
            rankingFile=$baseDir/ranking.txt
            includeVariants=true
            mxDiv=0.005
            maxError=0.2
            
            skipLiquibaseCheck=true
            liquibaseOutputDir=$liquibaseDir
            FastqToMappingPlugin.keyFile=$baseDir/genotypingKeyFile.txt
            FastqToMappingPlugin.fastqDir=$baseDir/data
            BestHaplotypePathPlugin.pathMethod=$pathMethod
            pathMethod=$pathMethod
            keyFile=$baseDir/genotypingKeyFile
            debugDir=$readMappingDir/
            
            # Groovy scripts
            picardPath=${ if (System.getenv("DB_HOST") != null) "/picard.jar" else "${Paths.get("").toAbsolutePath().toString()}/docker/buildFiles/picard.jar"}
            fastqFileDir=$baseDir/inputDir/loadDB/fastq/
            tempFileDir=$baseDir/inputDir/loadDB/temp/
            gvcfFileDir=$baseDir/inputDir/loadDB/gvcf/
            dedupedBamDir=$baseDir/inputDir/loadDB/bam/dedup/
            filteredBamDir=$baseDir/inputDir/loadDB/bam/mapqFiltered/
            gatkPath=${ if (System.getenv("DB_HOST") != null) "/gatk/gatk" else "$smallSeqDir/gatk/gatk"}
            tasselLocation=${ if (System.getenv("DB_HOST") != null) "/tassel-5-standalone/run_pipeline.pl" else "$smallSeqDir/tassel-5-standalone/run_pipeline.pl"}
        """.trimIndent()

        File(config).writeText(configContents)
    }
}
