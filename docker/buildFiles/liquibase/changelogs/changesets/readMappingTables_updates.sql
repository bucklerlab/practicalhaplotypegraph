--liquibase formatted sql

--## update paths table for postgres and sqlite

--changeset lcj34:update_readtablemappings_sqlite dbms:sqlite

DROP TABLE IF EXISTS read_mapping_group;
CREATE TABLE read_mapping_group (rm_group_id INTEGER PRIMARY KEY, name TEXT UNIQUE, description TEXT);

DROP TABLE IF EXISTS read_mapping_read_mapping_group;
CREATE TABLE read_mapping_read_mapping_group ( rm_group_id INTEGER, read_mapping_id INTEGER,UNIQUE(rm_group_id,read_mapping_id));

--changeset lcj34:update_readtablemappings_postgres dbms:postgresql

DROP TABLE IF EXISTS read_mapping_group;
CREATE TABLE read_mapping_group (rm_group_id SERIAL PRIMARY KEY, name TEXT UNIQUE, description TEXT);

DROP TABLE IF EXISTS read_mapping_read_mapping_group;
CREATE TABLE read_mapping_read_mapping_group ( rm_group_id INTEGER, read_mapping_id INTEGER,UNIQUE(rm_group_id,read_mapping_id));
