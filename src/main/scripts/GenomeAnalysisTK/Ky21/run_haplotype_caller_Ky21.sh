#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/Ky21_HB1JCADXX_CAGATC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Ky21_HL5WNCCXX_L3.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Ky21_HFFGKADXX_CAGATC.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o Ky21_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant Ky21_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o Ky21_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V Ky21_haplotype_caller_output.vcf \
   -o Ky21_haplotype_caller_output.fasta

