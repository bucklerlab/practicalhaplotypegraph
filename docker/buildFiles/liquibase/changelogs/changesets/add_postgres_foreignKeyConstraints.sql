--liquibase formatted sql

--## add postgres foreign key constraints.  SQLite has some already.  Adding additional foreigh key
--## constraints via liquibase is NOT supported for SQLITE
--## REMEMBER: all changset ids MUST end with the PHG release these will go into !!!
--## that is how our updating process works to tell user latest load that does not have these changes.

--changeset lcj34:FKsReadMapping_0.0.27 dbms:postgresql
ALTER TABLE read_mapping add constraint fk_method_id FOREIGN KEY (method_id) REFERENCES methods (method_id);

--changeset lcj34:FKsEdge_postgres_0.0.27 dbms:postgresql
ALTER TABLE edge add constraint fk_anchor_haplotype_id1 FOREIGN KEY (anchor_haplotype_id1) REFERENCES haplotypes (haplotypes_id);
ALTER TABLE edge add constraint fk_anchor_haplotype_id2 FOREIGN KEY (anchor_haplotype_id2) REFERENCES haplotypes (haplotypes_id);
ALTER TABLE edge add constraint fk_method_id FOREIGN KEY (method_id) REFERENCES methods (method_id);

--changeset lcj34:FKsGameteHaplotypes_postgres_0.0.27 dbms:postgresql
ALTER TABLE gamete_haplotypes add constraint fk_gamete_haplotypes_gameteid FOREIGN KEY (gameteid) REFERENCES gametes(gameteid);
ALTER TABLE gamete_haplotypes add constraint fk_gamete_haplotypes_gameate_grp_id FOREIGN KEY (gamete_grp_id) REFERENCES gamete_groups(gamete_grp_id);

--changeset lcj34:FKsGametes_postgres_0.0.27 dbms:postgresql
ALTER TABLE gametes add constraint fk_gametes_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);

--changeset lcj34:FKsRefRangeRefRangeMethod_postgres_0.0.27 dbms:postgres
ALTER TABLE ref_range_ref_range_method add constraint fk_ref_range_ref_range_method_method_id FOREIGN KEY (method_id) REFERENCES methods(method_id);
ALTER TABLE ref_range_ref_range_method add constraint fk_ref_range_ref_range_method_ref_range_id FOREIGN KEY (ref_range_id) REFERENCES reference_ranges(ref_range_id);

--changeset lcj34:FKsHaplotypes_postgres_0.0.27 dbms:postgresql
ALTER TABLE haplotypes add constraint fk_haplotypes_ref_range_id FOREIGN KEY (ref_range_id) REFERENCES reference_ranges(ref_range_id);
ALTER TABLE haplotypes add constraint fk_haplotypes_method_id FOREIGN KEY (method_id) REFERENCES methods(method_id);

--changeset lcj34:FKsGenomeFileData_postgres_0.0.27 dbms:postgresql
ALTER TABLE genome_file_data add constraint fk_genome_file_data_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);

--changeset lcj34:FKsVariants_postgres_0.0.27 dbms:postgresql
ALTER TABLE variants add constraint fk_variants_ref_allele_id FOREIGN KEY (ref_allele_id) REFERENCES alleles(allele_id);
ALTER TABLE variants add constraint fk_variants_alt_allele_id FOREIGN KEY (alt_allele_id) REFERENCES alleles(allele_id);

--changeset lcj34:FKsHaplotypeCounts_postgres_0.0.27 dbms:postgresql
ALTER TABLE haplotype_counts add constraint fk_haplotype_counts_method_id FOREIGN KEY (method_id) REFERENCES methods(method_id);
ALTER TABLE haplotype_counts add constraint fk_haplotype_counts_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);

--changeset lcj34:FKsReadMapping_postgres_0.0.27 dbms:postgresql
ALTER TABLE read_mapping add constraint fk_read_mapping_method_id FOREIGN KEY (method_id) REFERENCES methods(method_id);
ALTER TABLE read_mapping add constraint fk_read_mapping_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);

--changeset lcj34:FKsReadMappingPaths_postgres_0.0.27 dbms:postgresql
ALTER TABLE read_mapping_paths add constraint fk_read_mapping_paths_read_mapping_id FOREIGN KEY (read_mapping_id) REFERENCES read_mapping(read_mapping_id);
ALTER TABLE read_mapping_paths add constraint fk_read_mapping_paths_path_id FOREIGN KEY (path_id) REFERENCES paths(path_id);

--changeset lcj34:FKsPaths_postgres_0.0.27 dbms:postgresql
ALTER TABLE paths add constraint fk_paths_method_id FOREIGN KEY (method_id) REFERENCES methods(method_id);
ALTER TABLE paths add constraint fk_paths_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);

--changeset lcj34:FKsTaxaGroupsGenoid_postgres_0.0.27 dbms:postgresql
ALTER TABLE taxa_groups_genoid add constraint fk_taxa_groups_genoid_genoid FOREIGN KEY (genoid) REFERENCES genotypes(genoid);



