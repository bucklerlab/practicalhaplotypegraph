package net.maizegenetics.pangenome.gvcfFiltering;

import net.maizegenetics.pangenome.io.SFTPConnection;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Properties;

/**
 * This plugin is called from groovy script CreateHaplotypesFromBAM.groovy
 *
 * The gvcf remote file information will be uploaded to the db:genome_file_data table
 * when LoadHaplotypesFromGVCFPlugin is run.
 * So here we only need the docker/singularity specific path for storing the data.
 * If the user is to sftp the files, then isGvcfLocal will be false, and the
 * gvcfRemoteServer parameter will be a real server address and a path.
 */
public class FilterGVCFSingleFilePlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(FilterGVCFSingleFilePlugin.class);

    private PluginParameter<String> myInputFile = new PluginParameter.Builder<>("inputGVCFFile", null, String.class)
            .description("GVCF File to be filtered.")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myOutputFile = new PluginParameter.Builder<>("outputGVCFFile", null, String.class)
            .description("Output GVCF File Path and Name.")
            .outFile()
            .required(true)
            .build();

    private PluginParameter<String> myConfigFile = new PluginParameter.Builder<>("configFile",null, String.class)
            .description("Config folder containing the filtering parameters.")
            .inFile()
            .required(true)
            .build();


    public FilterGVCFSingleFilePlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }


    @Override
    public DataSet processData(DataSet input) {
        myLogger.info("FilterGVCFSingleFilePlugin:processData Loading up the Properties File.");
        //load up the properties object with whatever is in the config file
        Properties configProperties = new Properties();
        try {
            configProperties.load(Utils.getBufferedReader(configFile()));
        } catch (Exception e) {
            throw new IllegalStateException("FilterGVCFSingleFilePlugin Error loading config:",e);
        }
        myLogger.info("FilterGVCFSingleFilePlugin:processData Beginning the Filtering Process.");
        // This returns the gvcf bgzipped file name.
        String outputFileName = ComputeMedianGVCFAndFilter.filterGVCF(inputFile(),outputFile(),configProperties,false, "bcftools");

        myLogger.info("FilterGVCFSingleFilePlugin:processData Output File:"+outputFileName);

        return null;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Filter GVCF File";
    }

    @Override
    public String getToolTipText() {
        return "Filter GVCF File";
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // public static void main(String[] args) {
    //     GeneratePluginCode.generate(FilterGVCFSingleFilePlugin.class);
    // }

    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * GVCF File to be filtered.
     *
     * @return Input G V C F File
     */
    public String inputFile() {
        return myInputFile.value();
    }

    /**
     * Set Input G V C F File. GVCF File to be filtered.
     *
     * @param value Input G V C F File
     *
     * @return this plugin
     */
    public FilterGVCFSingleFilePlugin inputFile(String value) {
        myInputFile = new PluginParameter<>(myInputFile, value);
        return this;
    }

    /**
     * Output GVCF File Path and Name.
     *
     * @return Output G V C F File
     */
    public String outputFile() {
        return myOutputFile.value();
    }

    /**
     * Set Output G V C F File. Output GVCF File Path and
     * Name.
     *
     * @param value Output G V C F File
     *
     * @return this plugin
     */
    public FilterGVCFSingleFilePlugin outputFile(String value) {
        myOutputFile = new PluginParameter<>(myOutputFile, value);
        return this;
    }

    /**
     * Config folder containing the filtering parameters.
     *
     * @return Config File
     */
    public String configFile() {
        return myConfigFile.value();
    }

    /**
     * Set Config File. Config folder containing the filtering
     * parameters.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    public FilterGVCFSingleFilePlugin configFile(String value) {
        myConfigFile = new PluginParameter<>(myConfigFile, value);
        return this;
    }

}
