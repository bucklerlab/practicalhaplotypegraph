package net.maizegenetics.pangenome.fastaExtraction;

import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import junit.framework.Assert;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class GVCFGenotypeSequenceTest {

    private static GenomeSequence refSequence;
    private static final String refFileName = "data/B73_AGPv4_Chr8_first600bps.fa";

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeClass
    public static void setUp() {
        refSequence = GenomeSequenceBuilder.instance(refFileName);
    }

    @After
    public void tearDown() {
    }


    //#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	B73
    //8	1	.	T	<NON_REF>	.	.	END=37	GT:DP:GQ:MIN_DP:PL	0:10:99:1:0,45
    //8	38	.	G	A,<NON_REF>	.	.	.	GT:DP:GQ:MIN_DP:PL	1:5:99:1:0,45
    //8	39	.	A	<NON_REF>	.	.	END=40	GT:DP:GQ:MIN_DP:PL	0:5:99:1:0,45
    //8	41	.	A	AAA,<NON_REF>	.	.	.	GT:DP:GQ:MIN_DP:PL	1:7:99:1:0,45
    //8	42	.	C	<NON_REF>	.	.	END=45	GT:DP:GQ:MIN_DP:PL	0:7:99:1:0,45
    //8	46	.	TTCTT	T,<NON_REF>	.	.	.	GT:DP:GQ:MIN_DP:PL	1:5:99:1:0,45
    //8	51	.	G	<NON_REF>	.	.	END=55	GT:DP:GQ:MIN_DP:PL	0:10:99:1:0,45
    @Test
    public void testGenotypeSequence() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        //W22 is not in the sequence
        GenomeSequence gvcfSeq = GVCFGenotypeSequence.instance(refSequence,variants,false,"B73");

        //
        //        public String genotypeAsString(Chromosome chrom, int position)
        //        public String genotypeAsString(Chromosome chrom, Position positionObject)

        //Check some reference calls
        String gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),5);
        String expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),5);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 5 does not match expected",expectedRefCall,gvcfCallRef);

        gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),10);
        expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),10);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 10 does not match expected",expectedRefCall,gvcfCallRef);

        gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),15);
        expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),15);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 15 does not match expected",expectedRefCall,gvcfCallRef);
        //Check some SNPs
        String altCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),38);
        String expectedAltCall = "A/A";
        Assert.assertEquals("Alt call at position 38 should match:",expectedAltCall,altCall);

        //Check some INDELS
        String indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),41);
        String expectedIndelCall = "AAA/AAA";
        Assert.assertEquals("Insertion call at position 41 does not match:",expectedIndelCall,indelCall);


        indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),46);
        expectedIndelCall = "T/T";
        Assert.assertEquals("Insertion call at position 46 does not match:",expectedIndelCall,indelCall);

        reader.close();
    }

    @Test
    public void testGenotypeSequenceDiploid() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFileDiploid.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        //W22 is not in the sequence
        GenomeSequence gvcfSeq = GVCFGenotypeSequence.instance(refSequence,variants,false,"B73");

        //
        //        public String genotypeAsString(Chromosome chrom, int position)
        //        public String genotypeAsString(Chromosome chrom, Position positionObject)

        //Check some reference calls
        String gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),5);
        String expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),5);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 5 does not match expected",expectedRefCall,gvcfCallRef);

        gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),10);
        expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),10);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 10 does not match expected",expectedRefCall,gvcfCallRef);

        gvcfCallRef = gvcfSeq.genotypeAsString(Chromosome.instance(8),15);
        expectedRefCall = refSequence.genotypeAsString(Chromosome.instance(8),15);
        expectedRefCall = expectedRefCall + "/" + expectedRefCall;

        Assert.assertEquals("RefCall at position 15 does not match expected",expectedRefCall,gvcfCallRef);
        //Check some SNPs
        String altCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),38);
        String expectedAltCall = "A/A";
        Assert.assertEquals("Alt call at position 38 should match:",expectedAltCall,altCall);

        //Check some INDELS
        String indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),41);
        String expectedIndelCall = "AAA/AAA";
        Assert.assertEquals("Insertion call at position 41 does not match:",expectedIndelCall,indelCall);

        //Check a het
        indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),46);
        expectedIndelCall = "T/T";
        Assert.assertEquals("Deletion Het call at position 46 does not match:",expectedIndelCall,indelCall);
        indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),47);
        expectedIndelCall = "T/-";
        Assert.assertEquals("Deletion Het call at position 47 does not match:",expectedIndelCall,indelCall);

        indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),48);
        expectedIndelCall = "C/-";
        Assert.assertEquals("Deletion Het call at position 47 does not match:",expectedIndelCall,indelCall);

        //Check a het
        indelCall = gvcfSeq.genotypeAsString(Chromosome.instance(8),69);
        expectedIndelCall = "G/GGGG";
        Assert.assertEquals("Insertion call at position 69 does not match:",expectedIndelCall,indelCall);


        reader.close();
    }

    @Test
    public void testGenotypeSequenceErrors() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        //W22 is not in the sequence
        GenomeSequence gvcfSeq = GVCFGenotypeSequence.instance(refSequence,variants,false,"B73");


        //These methods should throw errors
//        public byte[] chromosomeSequence(Chromosome chrom)
//        public byte[] chromosomeSequence(Chromosome chrom, int startSite, int endSite)
//        public byte[] genomeSequence(long startSite, long lastSite)
//        public String genomeSequenceAsString(long startSite, long lastSite)
//        public byte genotype(Chromosome chrom, int position)
//        public byte genotype(Chromosome chrom, Position positionObject)
//        public String genotypeAsString(Chromosome chrom, int startSite, int endSite)
        try{
            gvcfSeq.chromosomeSequence(Chromosome.instance(8));
            Assert.fail("chromosomeSequence(Chromosome chrom) does not throw an error:");
        }
        catch(Exception e) {
            Assert.assertTrue("ChromosomeSequence should be throwing an error:",true);
        }

        try{
            gvcfSeq.chromosomeSequence(Chromosome.instance(8),1,10);
            Assert.fail("chromosomeSequence(Chromosome chrom, int startSite, int endSite) does not throw an error:");
        }
        catch(Exception e) {
            Assert.assertTrue("ChromosomeSequence should be throwing an error:",true);
        }

        try{
            gvcfSeq.genomeSequence(0,10);
            Assert.fail("genomeSequence(long startSite, long lastSite) should be throwing an error:");
        }
        catch(Exception e) {
            Assert.assertTrue("public byte[] genomeSequence(long startSite, long lastSite) should throw an error",true);
        }

        try{
            gvcfSeq.genomeSequenceAsString(0,10);
            Assert.fail("genomeSequenceAsString(long startSite, long lastSite) should be throwing an error:");
        }
        catch(Exception e) {
            Assert.assertTrue("genomeSequenceAsString(long startSite, long lastSite) should be throwing an error:",true);
        }

        try{
            gvcfSeq.genotype(Chromosome.instance(8),10);
            Assert.fail("genotype(Chromosome chrom, int position) should throw an error");
        }
        catch(Exception e) {
            Assert.assertTrue("genotype(Chromosome chrom, int position) should throw an error",true);
        }
        try {
            gvcfSeq.genotype(Chromosome.instance(8), Position.of(Chromosome.instance(8),3));
            Assert.fail("genotype(Chromosome chrom, Position positionObject) should throw an error");
        }
        catch(Exception e) {
            Assert.assertTrue("genotype(Chromosome chrom, Position positionObject) should throw an error",true);
        }

        try {
            gvcfSeq.genotypeAsString(Chromosome.instance(8), 0,12);
            Assert.fail(" genotypeAsString(Chromosome chrom, int startSite, int endSite) should throw an error.");
        }
        catch(Exception e) {
            Assert.assertTrue(" genotypeAsString(Chromosome chrom, int startSite, int endSite) should throw an error",true);
        }

        reader.close();
    }
}
