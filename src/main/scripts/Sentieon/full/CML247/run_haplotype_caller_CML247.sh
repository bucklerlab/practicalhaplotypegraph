#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/CML247/2220_7332_6928_H7F9YADXX_CML247_ACAGTG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/2220_7332_6928_H99W6ADXX_CML247_ACAGTG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/2220_7332_6928_H9C7FADXX_CML247_ACAGTG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/2220_7332_6928_H9CD2ADXX_CML247_ACAGTG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/2220_7332_6928_N_CML247_ACAGTG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/4881_6665_17269_HB1JCADXX_CML247_ATCACG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/4881_6665_17269_HFFGKADXX_CML247_ATCACG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML247/CML247_HL5WNCCXX_L4_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_all10_MergedPlus1000orGapDiffcoordinates.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 48 \
   --min_base_qual 21 \
   CML247_haplotype_caller_output.g.vcf

