/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.awt.Frame;
import java.sql.Connection;

import javax.swing.ImageIcon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;

/**
 * Plugin takes a configFile with db specifics and a boolean indicating whether
 * a new db shoudl be created.  Output is a db COnnection object.
 * 
 * INPUT:
 *   Config file containing these 5 lines (additional lines are ignored)
 *     host=<your host name>
 *     user=<username>
 *     password=<password>
 *     DB=<path to SQLITE db, or name of postgres db>
 *     DBtype=<sqlite or postgres>
 *   boolean: TRUE means create a new db if it doesn't exist, false means return null if db doesn't exist
 *      If db exists and boolean is TRUE, the db will be deleted and a new one of that name created.
 *      
 * OUTPUT:
 *   A db Connection object
 *   
 * @author lcj34
 *
 */
public class GetDBConnectionPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(GetDBConnectionPlugin.class);
    
 
    private PluginParameter<String> configFile = new PluginParameter.Builder<String>("config", null, String.class).guiName("DB Config Files").required(true).inFile()
            .description("File containing lines with data for host=, user=, password= and DB=, DBtype= used for db connection").build();
    private PluginParameter<Boolean> createNew = new PluginParameter.Builder<Boolean>("create", null, Boolean.class).guiName("Create New DB").required(true)
            .description(" True indicates a new DB of this name should be created, deleting any existing DB of this name.  False means return connection to existing DB or NULL")
            .build();
    
    public GetDBConnectionPlugin() {
        super(null, false);
    }

    public GetDBConnectionPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public GetDBConnectionPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
    
    
    @Override
    public DataSet processData(DataSet input) {
        Connection dbConnection = DBLoadingUtils.connection(configFile(), createNew());
        
        return new DataSet(new Datum("DB Connection", dbConnection, null), this);
    }
    
    public static void main(String[] args) {
        GeneratePluginCode.generate(GetDBConnectionPlugin.class);        
    }
    
    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {       
        return ("Get DB Connection");
    }

    @Override
    public String getToolTipText() {
        return ("Get DB Connection");
    }
    
    /**
     * File containing lines with data for host=, user=, password=
     * and DB=, DBtype= used for db connection
     *
     * @return DB Config Files
     */
    public String configFile() {
        return configFile.value();
    }

    /**
     * Set DB Config Files. File containing lines with data
     * for host=, user=, password= and DB=, DBtype= used for
     * db connection
     *
     * @param value DB Config Files
     *
     * @return this plugin
     */
    public GetDBConnectionPlugin configFile(String value) {
        configFile = new PluginParameter<>(configFile, value);
        return this;
    }

    /**
     *  True indicates a new DB of this name should be created,
     * deleting any existing DB of this name.  False means
     * return connection to existing DB or NULL
     *
     * @return Create New DB
     */
    public Boolean createNew() {
        return createNew.value();
    }

    /**
     * Set Create New DB.  True indicates a new DB of this
     * name should be created, deleting any existing DB of
     * this name.  False means return connection to existing
     * DB or NULL
     *
     * @param value Create New DB
     *
     * @return this plugin
     */
    public GetDBConnectionPlugin createNew(Boolean value) {
        createNew = new PluginParameter<>(createNew, value);
        return this;
    }

}
