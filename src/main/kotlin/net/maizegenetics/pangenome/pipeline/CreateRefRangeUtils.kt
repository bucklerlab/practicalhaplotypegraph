package net.maizegenetics.pangenome.pipeline

import com.google.common.collect.Range
import com.google.common.collect.RangeSet
import com.google.common.collect.TreeRangeSet
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.hapcollapse.DistanceCalculation
import net.maizegenetics.pangenome.hapcollapse.kmerDistanceMatrix
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.taxa.tree.TreeClusters
import net.maizegenetics.taxa.tree.UPGMATree
import net.maizegenetics.util.Utils
import org.biojava.nbio.genome.parsers.gff.GFF3Reader
import java.io.File
import java.io.PrintWriter
import kotlin.math.max
import kotlin.math.min

enum class GFFType {
    CDS,
    gene
}
object CreateRefRangeUtils {

    /**
     * Function to load in the Wiggle Files in a directory.  This works with both Coverage and Identity files.
     */
    fun loadInWiggleFiles(coverageDir : String) : Map<String,List<Int>> {
        val idMap = mutableMapOf<String,List<Int>>()

        File(coverageDir).walk()
            .filter { it.isFile }
            .filter { !it.isHidden }
            .forEach { addIdentityToMap(it.absolutePath,idMap) }

        return idMap
    }

    /**
     * Function to load in the identity/coverage into the full map.
     */
    fun addIdentityToMap(wiggleFile: String, chromMap : MutableMap<String, List<Int>>) {
        val reader = Utils.getBufferedReader(wiggleFile)
        val header = reader.readLine()

        val chrom = Chromosome.instance(header.split(" ")[1].split("=")[1]).name

        val identityList = mutableListOf<Int>()


        var currentLine = reader.readLine()
        while(currentLine != null) {
            val currentValue = currentLine.toInt()

            identityList.add(currentValue)

            currentLine = reader.readLine()
        }

        chromMap[chrom] = identityList
    }

    /**
     * This function will create Genic regions based on the input GFF file.
     *
     * First it groups the CDS regions by the transcript name
     * Then for each one we walk out from each end of the CDS until we can find a valid window where the coverage is high enough
     * Then we take all the resulting regions and merge the genes which are overlapping and will return this.
     */
    fun createGenicRegions(idMap: Map<String,List<Int>>,
                           gffFile : String,
                           minCovCount: Int,
                           windowSize: Int,
                           secondaryIdMap : Map<String,List<Int>> = mapOf(),
                           secondaryIdMapMinCovCount : Int = -1,
                           maximumSearchWindow: Int = 10000,
                           geneRangesOutput: String? = null,
                           gffIdentifier: GFFType = GFFType.CDS,
    ) : List<Triple<Position,Position,String>> {
        val featureList = GFF3Reader.read(gffFile)



        //If the gffIdentifier is CDS then we want to group by the Parent attribute. This will also not include the 5'
        // and 3' ends of the gene. Which allows us to find cut sites within those regions.
        //If the gffIdentifier is gene then we want to group by the gene name.  This will include the 5' and 3' ends
        // of the gene, and will find cut sites outside of the genic region
        val genicRegionsGrouped = featureList.selectByType(gffIdentifier.name)
            .groupBy { if(gffIdentifier == GFFType.CDS) {
                    it.getAttribute("Parent")
                }
                else {
                    it.group()
                }
            }

        println("Number of Genic Regions: ${genicRegionsGrouped.keys.size}")

        val geneWriter = if (geneRangesOutput == null) null else Utils.getBufferedWriter(geneRangesOutput)
        geneWriter?.write("chr\tstrand\toldStart\toldEnd\tnewStart\tnewEnd\ttotalExpansion\n")

        val genicRegions = genicRegionsGrouped
            .map {
                val currentCDS = it.value.sortedBy { it.location().bioStart() }

                //check the strand
                val strand = currentCDS.first().location().bioStrand()

                val revStrand = if(strand == '+') '-' else '+'

                val startPosition = if (strand == '+') {
                    currentCDS.first().location().bioStart() - 1
                } else {
                    currentCDS.last().location().bioEnd() + 1
                }

                val lastPostion = if (strand == '+') {
                    currentCDS.last().location().bioEnd() + 1
                } else {
                    currentCDS.first().location().bioStart()
                }


                val finalStartPosition = findEndPosition(
                    idMap,
                    currentCDS.first().seqname(),
                    startPosition,
                    revStrand,
                    minCovCount,
                    windowSize,
                    secondaryIdMap,
                    secondaryIdMapMinCovCount,
                    maximumSearchWindow)

                //find end Position
                val finalEndPosition = findEndPosition(
                    idMap,
                    currentCDS.first().seqname(),
                    lastPostion,
                    strand,
                    minCovCount,
                    windowSize,
                    secondaryIdMap,
                    secondaryIdMapMinCovCount,
                    maximumSearchWindow
                )

                if (finalStartPosition != -1 && finalEndPosition != -1) {
                    val newStart = Math.min(finalStartPosition, finalEndPosition)
                    val newEnd = Math.max(finalStartPosition, finalEndPosition)

                    val oldStart = Math.min(startPosition,lastPostion)
                    val oldEnd = Math.max(startPosition,lastPostion)

                    val totalExpansion = Math.abs(newStart - oldStart) + Math.abs(newEnd - oldEnd)

                    geneWriter?.write("${currentCDS.first().seqname()}\t${strand}\t${oldStart}\t${oldEnd}\t${newStart}\t${newEnd}\t${totalExpansion}\n")

                    Triple(
                        Position.of(currentCDS.first().seqname(), newStart),
                        Position.of(currentCDS.first().seqname(), newEnd),
                        "genic"
                    )
                }
                else {
                    val oldStart = Math.min(startPosition,lastPostion)
                    val oldEnd = Math.max(startPosition,lastPostion)
                    val newStart = if(checkCoverage(idMap, Position.of(currentCDS.first().seqname(),oldStart), minCovCount, windowSize, secondaryIdMap, secondaryIdMapMinCovCount)) oldStart else -1
                    val newEnd = if(checkCoverage(idMap, Position.of(currentCDS.first().seqname(),oldEnd), minCovCount, windowSize, secondaryIdMap, secondaryIdMapMinCovCount)) oldEnd else -1
                    geneWriter?.write("${currentCDS.first().seqname()}\t${strand}\t${oldStart}\t${oldEnd}\t${newStart}\t${newEnd}\t${-1}\n")
                    null
                }
            }
            .filterNotNull()
            .sortedBy { it.first }

        println("Number of genicRegions Before Merge: ${genicRegions.size}")
        geneWriter?.close()

        val mergedRegions = mergeGenicRegions(genicRegions)
        println("Number of genic Regions after merge: ${genicRegions.size}")

        return mergedRegions

    }

    /**
     * Function to check if the current window has high enough conservation.  It will check >= minCoverage
     */
    fun checkCoverage(idMap: Map<String, List<Int>>,
                      currentPosition: Position,
                      minCoverage: Int,
                      windowSize : Int,
                      secondaryIdMap : Map<String,List<Int>> = mapOf(),
                      secondaryIdMapMinCovCount : Int = -1) :Boolean {
        val originalSize = 2 * windowSize +1

        val size = (currentPosition.position - windowSize .. currentPosition.position + windowSize)
            .map { Position.of(currentPosition.chromosome,it) }
            .map {
                idMap[it.chromosome.name]?.get(it.position-1) ?: 0
            }
            .filter { it >= minCoverage }
            .size

        return (size == originalSize ) && (secondaryIdMap[currentPosition.chromosome.name]?.get(currentPosition.position-1) ?: 0) >= secondaryIdMapMinCovCount
    }

    /**
     * Simple function to merge the genic regions together if they overlap bps.
     */
    fun mergeGenicRegions(genicRegions: List<Triple<Position, Position, String>>): List<Triple<Position,Position,String>> {

        var currentRegionIdx = 1
        var currentRegion = genicRegions[0]
        val listOfNewRegions = mutableListOf<Triple<Position,Position, String>>()
        while(currentRegionIdx < genicRegions.size) {
            val regionToCheck = genicRegions[currentRegionIdx]

            if(isOverlap(currentRegion, regionToCheck)) {
                currentRegion = mergeTwoRegions(currentRegion,regionToCheck)
            }
            else {
                listOfNewRegions.add(currentRegion)
                currentRegion = regionToCheck
            }

            currentRegionIdx++
        }
        listOfNewRegions.add(currentRegion)
        return listOfNewRegions.toList()
    }

    /**
     * Function to check if there is an overlapping region.
     */
    private fun isOverlap(currentRegion : Triple<Position,Position,String>, regionToCheck : Triple<Position,Position,String>):Boolean {
        return (currentRegion.first.chromosome.name == regionToCheck.first.chromosome.name &&
                regionToCheck.first.position in currentRegion.first.position .. currentRegion.second.position)
    }

    /**
     * Simple functions to determine the merged start and end regions.
     */
    private fun mergeTwoRegions(currentRegion : Triple<Position,Position,String>, regionToCheck : Triple<Position,Position,String>) : Triple<Position,Position, String> {
        val chrom = currentRegion.first.chromosome
        val start = currentRegion.first.position
        val end = Math.max(currentRegion.second.position, regionToCheck.second.position)
        return Triple(Position.of(chrom,start), Position.of(chrom, end), currentRegion.third)
    }

    /**
     * Function to create the Intergenic regions.
     * For each one, we attempt to split up large regions based on the maximumIntergenicGapSize
     * We start from the middle of the Intergenic regions and walk out using the windowed coverage check
     */
    fun createIntergenicRegions(genicRegions: List<Triple<Position, Position, String>>,
                                maximumIntergenicGapSize: Int,
                                idMap: Map<String, List<Int>>,
                                minCovCount:Int,
                                windowSize: Int,
                                secondaryIdMap : Map<String,List<Int>> = mapOf(),
                                secondaryIdMapMinCovCount : Int = -1,
                                maximumSearchWindow: Int = 10000): List<Triple<Position,Position,String>> {
        val outputRegions = mutableListOf<Triple<Position,Position,String>>()


        for(idx in 0 until genicRegions.size-1) {
            val currentRange = genicRegions[idx]
            val nextRange = genicRegions[idx+1]

            outputRegions.add(currentRange)

            if(currentRange.first.chromosome.name != nextRange.first.chromosome.name) {
                continue
            }

            //Else we need to try to fill in the gap
            //Check size first
            if(nextRange.first.position - currentRange.second.position == 1) {
                continue
            }
            else if(nextRange.first.position - currentRange.second.position <= maximumIntergenicGapSize) {
                outputRegions.add(Triple(Position.of(currentRange.second.chromosome, currentRange.second.position+1),
                    Position.of(nextRange.first.chromosome, nextRange.first.position -1),
                    "intergenic"))
            }
            else {
                //If so it is big enough.  Try to determine if we can do a split
                outputRegions.addAll(splitIntergenicRegionsMiddleStart(currentRange.second.chromosome.name,
                    currentRange.second.position +1,
                    nextRange.first.position,
                    maximumIntergenicGapSize,
                    idMap,
                    minCovCount,
                    windowSize,
                    maximumSearchWindow,
                    secondaryIdMap,
                    secondaryIdMapMinCovCount
                    )
                )

            }

        }

        return outputRegions
    }

    /**
     * Function to split up the intergenic regions starting from the middle bp and walking out until we hit the boundaries of the region.
     */
    fun splitIntergenicRegionsMiddleStart(chrom: String,
                                          start:Int,
                                          end: Int,
                                          maximumIntergenicGapSize: Int,
                                          idMap: Map<String, List<Int>>,
                                          minCovCount: Int,
                                          windowSize: Int,maximumSearchWindow: Int = 10000,
                                          secondaryIdMap : Map<String,List<Int>> = mapOf(),
                                          secondaryIdMapMinCovCount : Int = -1,
                                          ): List<Triple<Position,Position,String>> {

        val cutSites = mutableListOf<Int>()

        val maximalSearchWindowSize = maximumIntergenicGapSize/2

        //Start at the middle bp
        val middleBP = (start + end)/2
        var currentPosition = middleBP - maximalSearchWindowSize
        //Walk back in steps of maximumIntergenicGapSize/2 until we hit the start site
        while(currentPosition > start + maximalSearchWindowSize) {
            val nextCutSite = findEndPosition(idMap,chrom, currentPosition, '-' ,minCovCount, windowSize, secondaryIdMap, secondaryIdMapMinCovCount, maximumSearchWindow)
            if(nextCutSite > start + maximalSearchWindowSize ) {
                cutSites.add(nextCutSite)
                currentPosition = nextCutSite - maximalSearchWindowSize - 1
            }
            else if(nextCutSite == -1) {
                currentPosition = currentPosition - maximalSearchWindowSize -1
            }
            else {
                break
            }
        }
        cutSites.add(start)

        //Reverse the cutSite list so the starting bp is at the start
        cutSites.reverse()
        //Walk out in steps of maximumIntergenicGapSize/2 until we find the last site
        currentPosition = middleBP + maximalSearchWindowSize
        while(currentPosition < end - maximalSearchWindowSize) {
            val nextCutSite = findEndPosition(idMap,chrom, currentPosition, '+' ,minCovCount, windowSize, secondaryIdMap, secondaryIdMapMinCovCount, maximumSearchWindow)
            if(nextCutSite in currentPosition .. end - maximalSearchWindowSize ) {
                cutSites.add(nextCutSite)
                currentPosition = nextCutSite + maximalSearchWindowSize + 1
            }
            else if(nextCutSite == -1) {
                currentPosition = currentPosition + maximalSearchWindowSize + 1
            }
            else {
                break
            }
        }

        //Add the last site
        cutSites.add(end)

        return cutSites.zipWithNext().map { Triple(Position.of(chrom,it.first), Position.of(chrom,it.second-1), "intergenic") }.toList()

    }

    /**
     * Function to find the End position starting from a start point.
     * This will walk out based on the strand until it hits a windowed region which passes the coverage check.
     */
    fun findEndPosition(idMap:Map<String,List<Int>>,
                        seqname: String,
                        lastPostion: Int,
                        strand: Char,
                        minCovCount: Int,
                        windowSize: Int,
                        secondaryIdMap : Map<String,List<Int>> = mapOf(),
                        secondaryIdMapMinCovCount : Int = -1,
                        maximumSearchWindow: Int = 10000 ) : Int {
        val chromName = Chromosome.instance(seqname).name
        var currentPosition = lastPostion

        if(!idMap.containsKey(chromName)) return -1
        val lengthOfChrom = idMap[chromName]?.size!!- (windowSize * 2)


        while(currentPosition in windowSize +1 .. lengthOfChrom && !checkCoverage(idMap, Position.of(seqname,currentPosition), minCovCount, windowSize,secondaryIdMap, secondaryIdMapMinCovCount)) {

            if(Math.abs(lastPostion - currentPosition) > maximumSearchWindow) {
                return -1
            }

            if(strand == '+') {
                currentPosition+=windowSize
            }
            else {
                currentPosition-=windowSize
            }
            if(currentPosition >= idMap[chromName]?.size!!- (windowSize * 2)  || currentPosition <= windowSize+1) {

                currentPosition = -1
                break
            }
        }

        return currentPosition
    }

    /**
     * Simple function to create a first and last entry for the intergenic regions before the first gene region and the last genic region.
     */
    fun createFirstAndLastBedIntergenic(bedRanges : List<Triple<Position,Position,String>>, idMap: Map<String, List<Int>>) : List<Triple<Position,Position,String>> {
        val outputList = mutableListOf<Triple<Position,Position,String>>()

        bedRanges.groupBy { it.first.chromosome }
            .map { Pair(it.key,it.value.sortedBy { range -> range.first.position }) }
            .forEach {
                outputList.add(Triple(Position.of(it.first,1),
                    Position.of(it.first,it.second.first().first.position-1),
                    "intergenic"))

                outputList.addAll(it.second)

                //Add in the last range
                outputList.add(Triple(Position.of(it.first, it.second.last().second.position+1),
                    Position.of(it.first, (idMap[it.first.name]?.size?:0)),
                    "intergenic"))
            }

        return outputList
    }

    /**
     * Simple function to output a bed file
     */
    fun genicRangesToBedFile(bedFile:String, bedRanges: List<Triple<Position,Position,String>>) {
        Utils.getBufferedWriter(bedFile).use { output ->
            for (range in bedRanges) {
                output.write("${range.first.chromosome.name}\t${range.first.position - 1}\t${range.second.position}\t${range.third}\n")
            }
        }
    }

    data class RegionInputData(val start: Position, val end: Position, val type: String, val variantContextMap: Map<Taxon, List<VariantContext>>)
    data class RegionHaplotypeCount(val start: Position, val end: Position, val count: Int, val type: String)

    /**
     * Function to split genic ranges based on haplotype number. As a by-product it prints a file with both the original
     * and new ranges along with their haplotype numbers. The result only contains the original intergenic ranges and the
     * new, subdivided genic ranges.[inputBed] is a list of the input range start position, end position, and type,
     * where type is genic or intergenic. [vcfdir] is a directory containing the gVCF files that will be used to
     * cluster haplotypes and calculate haplotype number. It is expected that these gVCF files will be used as the
     * source of haplotypes to build a PHG database. Because haplotype clustering is compute intensive the method is
     * multithreaded.
     */
    fun splitGenicReferenceRanges(inputBed: List<Triple<Position,Position,String>>, idMap: Map<String, List<Int>>,
                                  vcfdir: String, refGenome: GenomeSequence, minCoverage: Int = 25, windowSize: Int = 5,
                                  nThreads: Int, resultFile: String? = null,
                                  mxDiv: Double, minLength: Int, maxClusters: Int) : List<Triple<Position,Position,String>> {
        //inputBed is a List of 1-based closed regions as a triple of position, position, type

        //translate the idMap of sites into coveredRanges
        val conservedSites = idMap.entries.associate { (chr, cover) ->
            Pair(chr, coverageListToCoveredRanges(cover, windowSize, minCoverage))
        }

        //collect the results in a list
        val resultList = mutableListOf<RegionHaplotypeCount>()

        runBlocking {
            val sequenceMapChannel = Channel<RegionInputData>(10)

            //The result is a list of regions with their haplotype numbers
            //Since the size of the results is relatively small and needs to be sorted before writing to file
            //use an unbounded result channel and wait for the regions to be processed before writing the results
            val resultChannel = Channel<List<RegionHaplotypeCount>>(10)

            val jobList = mutableListOf<Job>()
            repeat(nThreads) {
                jobList.add(launch(Dispatchers.Default) { trySplitRegion(sequenceMapChannel, resultChannel, refGenome,
                    conservedSites, mxDiv, minLength, maxClusters)} )
            }

            println("Finished launching split jobs")

            val collectJob = launch(Dispatchers.Default){
                for (result in resultChannel) resultList.addAll(result)
            }

            println("Finished launching collector")

            //process regions
            val gvcfReaderMap = getVcfReaders(vcfdir)
            println("Finished creating vcf readers")
            var recordCount = 0
            inputBed.forEach { region ->
                val chrom = region.first.chromosome
                val start = region.first.position
                val end = region.second.position
                val type = region.third
                val vcMap = gvcfReaderMap.entries.associate { (taxon, reader) ->
                    Pair(taxon, reader.query(chrom.name, start, end).toList())
                }

                if (++recordCount % 1000 == 1) println("Processing bedfile record $recordCount, resultList has ${resultList.size} results")
                sequenceMapChannel.send(RegionInputData(Position.of(chrom, start), Position.of(chrom, end), type, vcMap))
            }
            sequenceMapChannel.close()

            println("Finished sending data to sequenceMapChannel")

            //this waits for the jobs to finish
            for (job in jobList) job.join()
            println("Finished processing ranges")
            resultChannel.close()
            collectJob.join()
            println("Finished collecting results")

        }

        //process the results
        resultList.sortBy { it.start }

        //regions are 1-based closed-closed. Do not adjust here, as this is not a bed file.
        if (resultFile != null) PrintWriter(resultFile).use { myPrinter ->
            //columns are chr, start, end, type, number of haplotypes
            myPrinter.println("chr\tstart\tend\ttype\tnhaps")
            for (hapcount in resultList)
                myPrinter.println("${hapcount.start.chromosome.name}\t${hapcount.start.position}\t${hapcount.end.position}\t${hapcount.type}\t${hapcount.count}")
            println("Genic region splitting results written to $resultFile")
        }

        //filter out genic-original now that the result file has been written.
        return resultList.map { Triple(Position.of(it.start.chromosome, it.start.position), it.end, it.type) }
            .filter { it.third !="genic-original" }
    }

    private fun getVcfReaders(vcfdir: String): Map<Taxon, VCFFileReader> {
        val dir = File(vcfdir)
        val fileList = dir.list { _, name -> name.endsWith("vcf.gz") }
            .map { name -> dir.resolve(name) }
        return fileList.associate { vcfFile ->
            val reader = VCFFileReader(vcfFile)
            val taxon = Taxon(reader.header.genotypeSamples[0])
            Pair(taxon, reader)
        }
    }

    /**
     * Converts [coverageList], which is a ordered list of coverage for individual positions, to a RangeSet of sites
     * meeting the coverage criterion specified by minCoverage and windowSize. All positions in the range
     * have coverage >= minCoverage and are bordered by a minimum of windowSize sites on either side that also meet
     * that are covered.
     */
    fun coverageListToCoveredRanges(coverageList: List<Int>, windowSize: Int, minCoverage: Int) : RangeSet<Int> {
        val coveredRanges = TreeRangeSet.create<Int>()
        val tempRanges = TreeRangeSet.create<Int>()
        var previousCoverage = 0
        var currentStart = -1
        coverageList.forEachIndexed { index, coverage ->
            if (coverage >= minCoverage) {
                if (previousCoverage < minCoverage) currentStart = index
            } else {
                if (previousCoverage >= minCoverage) tempRanges.add(Range.closed(currentStart, index - 1))
            }
            previousCoverage = coverage
        }

        //trim window off start and end of all ranges longer than 2 * windowSize + 1
        //since the range is closed-closed, the filter is end - start >= windowSize * 2
        val minRangeSize = 2 * windowSize
        tempRanges.asRanges().filter { it.hasLowerBound() && it.hasUpperBound()}
            .filter { it.upperEndpoint() - it.lowerEndpoint() >= minRangeSize }
            .forEach { coveredRanges.add(Range.closed(it.lowerEndpoint() + windowSize, it.upperEndpoint() - windowSize)) }
        return coveredRanges
    }

    /**
     * Function that, when possible, splits a range into subranges at conserved sites based on the number
     * of haplotype clusters.
     */
    suspend fun trySplitRegion(inputChannel: Channel<RegionInputData>, resultChannel: Channel<List<RegionHaplotypeCount>>,
                               refGenome: GenomeSequence, conservedSites: Map<String, RangeSet<Int>>,
                               mxDiv: Double, minLength: Int, maxClusters: Int) {
        val typeToSkip = "intergenic"
        val typeGenic = "genic"
        val typeOriginal = "genic-original"

        for (inputData in inputChannel) {
//            println("Processing ${inputData.start.chromosome.name} from ${inputData.start.position} to ${inputData.end.position}, length ${inputData.end.position -inputData.start.position + 1}, ${inputData.type}")
            //data class RegionCountInputData(val region: CollapseRegions.Region, val vcMap: Map<Taxon, List<VariantContext>>)
            //for the input region, count the number of haplotype clusters for a given mxDiv
            val inputNumberOfHaplotypes = countRegionHaplotypes(inputData.start, inputData.end, inputData.variantContextMap, refGenome, mxDiv)
            val skip = inputData.type == typeToSkip || inputNumberOfHaplotypes < maxClusters
            when  {
                skip -> {
                    val resultList = mutableListOf(RegionHaplotypeCount(inputData.start, inputData.end, inputNumberOfHaplotypes, inputData.type))
                    if (inputData.type == typeGenic) {
                        resultList.add(RegionHaplotypeCount(inputData.start, inputData.end, inputNumberOfHaplotypes, typeOriginal))
                    }
//                    println("finished (skip) ${inputData.start.chromosome.name}: ${inputData.start.position}")
                    resultChannel.send(resultList)
                }
                else -> {
                    val chrConservedSites = conservedSites[inputData.start.chromosome.name]
                    check(chrConservedSites != null) {"no conserved sites for chr ${inputData.start.chromosome.name}"}
                    val subRegions = splitRegionFromMiddle(inputData.start, inputData.end, chrConservedSites, minLength)
                    when (subRegions.size) {
                        1 -> {
                            val resultList = mutableListOf(RegionHaplotypeCount(subRegions[0].first, subRegions[0].second, inputNumberOfHaplotypes, inputData.type))
                            if (inputData.type == typeGenic) {
                                resultList.add(RegionHaplotypeCount(subRegions[0].first, subRegions[0].second, inputNumberOfHaplotypes, typeOriginal))
                            }
                            resultChannel.send(resultList)
//                            println("finished (subregion size = 1) ${inputData.start.chromosome.name}: ${inputData.start.position}")
                        }
                        else -> {
//                            println("${inputData.start.position} divided into ${subRegions.size} subregions")
                            //find the cluster number for each subregion
                            val regionsWithCounts = subRegions.map {
                                RegionHaplotypeCount(
                                    it.first,
                                    it.second,
                                    countRegionHaplotypes(it.first, it.second, inputData.variantContextMap, refGenome, mxDiv),
                                    inputData.type
                                )
                            }

                            //keep regions with haplotype count < number of haplotypes, remerge others
                            val finalRegionList = mutableListOf<RegionHaplotypeCount>()
                            val unreducedRegionList = mutableListOf<RegionHaplotypeCount>()

//                            println("${inputData.start.position} initialized region lists")

                            //for each region:
                            //   if the region has inputNumberOfHaplotypes or more add it to the unreduced region list
                            //   if the region has less than inputNumberOfHaplotypes merge any unreduced regions in the unreduced list
                            //   and add them to the final list, then add this region. Clear the unreduced list.
                            for (region in regionsWithCounts) {
                                if (region.count < inputNumberOfHaplotypes) {
                                    //if there are any unreduced regions, merge them and add the merged region before adding this region
                                    if (unreducedRegionList.size == 1) {
                                        finalRegionList.add(unreducedRegionList.first())
                                        unreducedRegionList.clear()
                                    } else if (unreducedRegionList.size > 1) {
                                        val startPos = unreducedRegionList.first().start
                                        val endPos = unreducedRegionList.last().end
                                        val mergedRegion = RegionHaplotypeCount(
                                            startPos,
                                            endPos,
                                            countRegionHaplotypes(startPos, endPos, inputData.variantContextMap, refGenome, mxDiv),
                                            inputData.type
                                        )
                                        finalRegionList.add(mergedRegion)
                                        unreducedRegionList.clear()
                                    }

                                    finalRegionList.add(region)
                                }
                                else {
                                    //add the region to the list of regions with haplotype number >= original haplotype number
                                    unreducedRegionList.add(region)
                                }
                            }
//                            println("${inputData.start.position} processed regions")
                            //add any remaining unreduced ranges
                            if (unreducedRegionList.size == 1) {
                                finalRegionList.add(unreducedRegionList.first())
                            } else if (unreducedRegionList.size > 1) {
                                val startPos = unreducedRegionList.first().start
                                val endPos = unreducedRegionList.last().end
                                val mergedRegion = RegionHaplotypeCount(
                                    startPos,
                                    endPos,
                                    countRegionHaplotypes(startPos, endPos, inputData.variantContextMap, refGenome, mxDiv),
                                    inputData.type
                                )
                                finalRegionList.add(mergedRegion)
                            }

                            //add the original interval to the list if the region is genic
                            if (inputData.type == typeGenic) {
                                finalRegionList.add(RegionHaplotypeCount(inputData.start, inputData.end, inputNumberOfHaplotypes, typeOriginal))
                            }
//                            println("finished (split region) ${inputData.start.chromosome.name}: ${inputData.start.position}")
                            resultChannel.send(finalRegionList)
                        }
                    }
                }
            }
        }
    }

    /**
     * Function that counts the number of haplotype clusters in a region
     */
    fun countRegionHaplotypes(start: Position, end: Position, taxonVcMap: Map<Taxon, List<VariantContext>>, refGenome: GenomeSequence, mxDiv: Double) : Int {
        //create a sequence map of Taxon -> sequence
        val sequenceMap = taxonVcMap.entries.associate { (taxon, vc) ->
            val seq = sequenceForRegion(start, end, vc, refGenome)
            Pair(taxon, seq)
        }

        val dm = kmerDistanceMatrix(sequenceMap, 7, DistanceCalculation.Euclidean)
        val myTree = UPGMATree(dm)
        val clusterMaker = TreeClusters(myTree)
        return clusterMaker.getGroups(mxDiv).distinct().count()
    }

    /**
     * Function that generates sequence from [vcIter], a list of VariantContexts, for the region
     * from [startPos] to [endPos].
     */
    fun sequenceForRegion(startPos: Position, endPos: Position, vcIter: List<VariantContext>, refSequence: GenomeSequence): String {
        val seqBuilder = StringBuilder()
        val myChr = startPos.chromosome.name
        val regionLength = endPos.position - startPos.position + 1
        for (vc in vcIter) {
            //Does vc overlap region? If not do not use it.
            if (vc.end < startPos.position || vc.start > endPos.position) continue

            if (vc.alleles[1].getDisplayString() == "<NON_REF>" && vc.hasAttribute("END")) {
                //this is a reference block
                //get the ref block sequence
                //if the start of the region or end of the region is internal to the ref block, extract the substring
                //corresponding to the overlap between the region and the ref block
                val refstart = max(vc.start, startPos.position)
                val refend = min(vc.end, endPos.position)
                check(refend >= refstart) {"refend less than refstart"}
                val seq = refSequence.genotypeAsString(Chromosome.instance(myChr), refstart, refend)
                seqBuilder.append(seq)
            } else if (vc.start < startPos.position && vc.end > endPos.position) {
                //the region is completely inside the variant context, so the ends of the vc sequence must be trimmed
                val leftAligned = isLeftAligned(vc)
                val seq = vc.genotypes[0].genotypeString
                if (leftAligned) {
                    val prefixLength = startPos.position - vc.start
                    if (seq.length > prefixLength) seqBuilder.append(seq.substring(prefixLength).take(regionLength))
                } else {
                    val suffixLength = vc.end - endPos.position
                    val remainderLength = seq.length - suffixLength
                    if (remainderLength > 0) seqBuilder.append(seq.take(remainderLength).takeLast(regionLength))
                }
            } else if (vc.start < startPos.position) {
                //vc.end <= region.end, otherwise previous condition would be true
                //not a reference block, region starts inside the vc
                val leftAligned = isLeftAligned(vc)
                val seq = vc.genotypes[0].genotypeString
                if (leftAligned) {
                    //remove the prefix not in the region
                    val prefixLength = startPos.position - vc.start
                    if (seq.length > prefixLength) seqBuilder.append(seq.removeRange(0 until prefixLength))
                } else {
                    seqBuilder.append(seq.takeLast(vc.end - startPos.position + 1))
                }
            } else if (vc.end > endPos.position) {
                //vc.start >= region.start
                val seq = vc.genotypes[0].genotypeString

                val leftAligned = isLeftAligned(vc)
                if (leftAligned) {
                    //take sequence from vc.start to region.end
                    seqBuilder.append(seq.take(endPos.position - vc.start + 1))
                } else {
                    //remove region.end to vc.end (exclusive, inclusive) from end of sequence
                    val keepLength = seq.length - (vc.end - endPos.position)
                    if (keepLength > 0) seqBuilder.append(seq.take(keepLength))
                }

            } else {
                val seq = vc.genotypes[0].genotypeString //because this is for single sample gvcfs
                seqBuilder.append(seq)
            }
        }

        return seqBuilder.toString()
    }

    /**
     * Function that splits a region in the middle, if that site is conserved and if the both subregions are at least
     * [minLength] long. Each new subregion is split in the middle again until no subregions can be split.
     */
    fun splitRegionFromMiddle(start: Position, end: Position, conservedSites: RangeSet<Int>, minLength: Int) : List<Pair<Position, Position>> {
        val splitRegionList = mutableListOf<Pair<Position, Position>>()
        val candidateRegionList = mutableListOf(Pair(start,end))
        val myChr = start.chromosome
        while (candidateRegionList.size > 0) {
            val regionToSplit = candidateRegionList.removeAt(0)
            val splitSite = splitRegionInTheMiddle(regionToSplit, conservedSites, minLength)
            if (splitSite == -1) splitRegionList.add(regionToSplit)
            else {
                candidateRegionList.add(Pair(regionToSplit.first, Position.of(myChr, splitSite)))
                candidateRegionList.add(Pair(Position.of(myChr, splitSite + 1), regionToSplit.second))
            }
        }

        return splitRegionList.sortedBy { it.first }
    }

    private fun splitRegionInTheMiddle(endpoints: Pair<Position,Position>, conservedSites: RangeSet<Int>, minLength: Int) : Int {
        //split the region as close to the middle as possible
        //return the split site (start of second region)
        //if either of the sub regions is smaller than minLength return -1
        // it is assumed that the region is a closed-open interval

        var split = (endpoints.second.position - endpoints.first.position) / 2 + endpoints.first.position

        //this will create two new closed-open intervals
        //the split is acceptable if the last site of the first new interval (split - 1) or the first site
        // of the second new interval (split) is conserved
        if (!conservedSites.contains(split) && !conservedSites.contains(split - 1)) {
            //split is not a conserved site, find the closest conserved site
            val nonConservedRange = conservedSites.complement().rangeContaining(split)
            when {
                nonConservedRange.hasUpperBound() && nonConservedRange.hasLowerBound() -> {
                    val ubnd = nonConservedRange.upperEndpoint()
                    val lbnd = nonConservedRange.lowerEndpoint()
                    if ((ubnd - split) <= lbnd - split) split = ubnd
                    else split = lbnd
                }
                nonConservedRange.hasUpperBound() -> {
                    split = nonConservedRange.upperEndpoint()
                }
                nonConservedRange.hasLowerBound() -> {
                    split = nonConservedRange.lowerEndpoint()
                }
                else -> return -1
            }
        }

        //If this region has no conserved sites, split will end up falling outside of region and either
        //startLength or endLength will be negative and this method will return -1
        val startLength = split - endpoints.first.position
        val endLength = endpoints.second.position - split
        return if (startLength >= minLength && endLength >= minLength) split else -1
    }

    private fun isLeftAligned(vc: VariantContext): Boolean {
        if (vc.start > 1) return true
        return false
    }

}