--liquibase formatted sql

--## remove the FK constraint that links read_mapping to haplotype_list table as it fails
--## when the haplotypes_list table has not been populated with a default.
--## Note there is no update for an sqlite table.  You cannot drop a constraint from an sqlite
--## db via alter table - you have to copy the table to something old, create new table without
--## the constraint, then copy data from the original table to the new one.
--## FK is not enabled in our sqlite, so we are ok, all defined FKs are ignored.

--changeset lcj34:remove_FK_haplotype_list_id_postgres_0.0.37 dbms:postgresql
ALTER TABLE read_mapping DROP CONSTRAINT IF EXISTS fk_read_mapping_haplotype_list_id;