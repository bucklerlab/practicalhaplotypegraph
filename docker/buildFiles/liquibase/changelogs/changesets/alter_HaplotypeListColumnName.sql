--liquibase formatted sql

--## fix haplotype_list  table - make column name consistent with what was coded in PHG

--changeset lcj34:alterHaplotypeListTableColumn_sqlite_0.0.34 dbms:sqlite
--preconditions onFail:MARK_RAN onError:MARK_RAN
--precondition-sql-check expectedResult:1 SELECT COUNT(*) AS CNTREC FROM pragma_table_info('haplotype_list') WHERE name='haplotype_list';
alter table haplotype_list rename haplotype_list to hapid_list;

--changeset lcj34:alterHaplotypeListTableColumn_postgres_0.0.34 dbms:postgresql
--preconditions onFail:MARK_RAN onError:MARK_RAN
--precondition-sql-check expectedResult:1 select count(*) from information_schema.columns where table_name='haplotype_list' and column_name='haplotype_list';
alter table haplotype_list rename haplotype_list to hapid_list;