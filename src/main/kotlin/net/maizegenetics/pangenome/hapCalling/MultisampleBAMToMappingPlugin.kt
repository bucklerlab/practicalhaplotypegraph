package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import htsjdk.samtools.SAMRecordIterator
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.LoadHaplotypesFromGVCFPlugin
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.DirectoryCrawler
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.nio.file.Paths
import java.sql.Connection
import java.util.HashMap
import javax.swing.ImageIcon

/**
 * Plugin to load in multisample BAM based readMappings into the DB. This is effectively the same result as FastqToMappingPlugin or SAMToMappingPlugin.
 *
 * The main difference is that this requires some additional architecture in order to correctly resplit up the BAM file into the correct alignments.
 *
 * This Plugin requires a fastqGroupingFile generated from MergeFastqPlugin in order to determine which reads match up to which samples.
 */
class MultisampleBAMToMappingPlugin (parentFrame: Frame?=null, isInteractive: Boolean=false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(MultisampleBAMToMappingPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, filename, and PlateID.  Optionally for paired end reads, filename2 is needed.  " +
                    "If filename2 is not supplied, Minimap2 will run in single end mode.  Otherwise will be paired.")
            .build()

    private var fastqGroupingFile = PluginParameter.Builder("fastqGroupingFile", null, String::class.java)
            .guiName("Fastq Grouping File")
            .inFile()
            .required(true)
            .description("Grouping file created by MergeFastqPlugin")
            .build()

    private var samDir = PluginParameter.Builder("samDir", null, String::class.java)
            .guiName("SAM/BAM dir to process")
            .inDir()
            .required(true)
            .description("Name of the SAM/BAM dir to process.")
            .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name to be stored in the DB.")
            .build()

    private var methodDescription = PluginParameter.Builder("methodDescription", null, String::class.java)
            .guiName("Method Description")
            .required(false)
            .description("Method description to be stored in the DB.")
            .build()

    private var outputDebugDir = PluginParameter.Builder("debugDir", "", String::class.java)
            .guiName("Debug Directory to write out read Mapping files.")
            .outDir()
            .required(false)
            .description("Directory to write out the read mapping files.  This is optional for debug purposes.")
            .build()

    private var outputSecondaryMappingStats = PluginParameter.Builder("outputSecondaryStats",false,Boolean::class.javaObjectType)
            .guiName("Output secondary mapping statistics.")
            .required(false)
            .description("Output Secondary Mapping Statistics such as total AS for each haplotype ID")
            .build()


    private var mappingStatDir = PluginParameter.Builder("secondaryStatsDir","./",String::class.java)
        .guiName("Output Secondary Mapping Stat Directory")
        .description("Directory to store the mapping stats.")
        .outDir()
        .required(false)
        .build()

    private var myNumThreads = PluginParameter.Builder("numThreads",3, Int::class.javaObjectType)
            .description("Number of threads used to upload.  The MultisampleBAM upload will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
            .required(false)
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()


    private var updateDB = PluginParameter.Builder("updateDB", true, Boolean::class.javaObjectType)
        .description("If set to true, the read mappings will be written to the db.  Otherwise nothing will be written.")
        .required(false)
        .build()


    private var ignoreWarnings = PluginParameter.Builder("ignoreWarnings", false, Boolean::class.javaObjectType)
        .guiName("Ignore Warnings")
        .description("Ignore Warnings when checking number of reads per unmerged file.  If this is enabled, this will write to the DB even though there may be errors.")
        .required(false)
        .build()

    private var runWithoutGraph = PluginParameter.Builder("runWithoutGraph", false, Boolean::class.javaObjectType)
        .description("If set to true, will require the input of a JSON file created by CreateHapIdMapsPlugin and will got require a Graph object input.")
        .required(false)
        .build()

    private var hapIdMapFile = PluginParameter.Builder("hapIdMapFile", null, String::class.java)
        .description("Location of the HapIdMapFile where the graph Information can be found.")
        .required(false)
        .dependentOnParameter(runWithoutGraph,true)
        .build()


    private var inputFile = PluginParameter.Builder("inputSAMFile", "", String::class.java)
        .description("Input Multisample SAM file to be run through.  If this is not set it will run everything in the inputDir")
        .required(false)
        .build()

    override fun processData(input: DataSet?): DataSet? {

        //These variables can come from either a JSON file hapIdMapFile() or from a graph.  Default behavior is to run with a graph.
        var hapIdToRefRangeMap:Map<Int,ReferenceRange>
        var refRangeToHapIdMap: Map<Int,Map<Int,Int>>
        var hapIdToLengthMap: Map<Int,Int>
        var hapListId:Int
        var phg:PHGdbAccess?
        if(runWithoutGraph()) {
            //Load in the JSON file
            val maps = deserializeGraphIdMaps(hapIdMapFile())
            hapIdToRefRangeMap = unwrapRefRanges(maps.hapIdToRefRangeMap)
            hapIdToLengthMap = maps.hapIdToLengthMap
            refRangeToHapIdMap = maps.refRangeToHapIdMap
            hapListId = maps.haplotypeListId
            phg= null
        }
        else {
            //Load in the graph to check reference Ranges

            val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
            if (temp.size != 1) {
                throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
            }
            val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph

            hapIdToRefRangeMap = getHapToRefRangeMap(graph)
            refRangeToHapIdMap = getRefRangeToHapidMap(graph)
            hapIdToLengthMap = getHapIdToSequenceLength(graph)

            var dbConnect = DBLoadingUtils.connection(false)
            phg = PHGdbAccess(dbConnect)
            hapListId = getHaplotypeListIdForGraph(graph, phg)

        }


        runMultithreadedReadMapping(keyFile(),fastqGroupingFile(), samDir(), hapIdToRefRangeMap, hapIdToLengthMap, maxRefRangeError(),
            methodName(), methodDescription(), pluginParameters(),outputDebugDir(), outputSecondaryMappingStats(), isTestMethod(),
            mappingStatDir(),phg,hapListId, refRangeToHapIdMap,inputFile())

        return null
    }

    /**
     * Function to run multithreaded Read Mapping.
     */
    private fun runMultithreadedReadMapping(keyFileName:String, fastqGroupingFileName: String, inputFileDir : String,
                                            hapIdToRefRangeMap: Map<Int, ReferenceRange>, hapIdToLengthMap : Map<Int,Int>,
                                            maxRefRangeError: Double,
                                            methodName : String, methodDescription: String?,
                                            pluginParams: Map<String,String>,
                                            outputDebugReadMappingDir : String,
                                            outputSecondaryMappingStats : Boolean = false,
                                            isTestMethod : Boolean = false,
                                            secondaryStatsDir : String,
                                            phg:PHGdbAccess?,
                                            hapListId: Int,
                                            refRangeToHapIdMap: Map<Int, Map<Int,Int>>,
                                            inputFileName :String = "") {

        try {

            //TODO when paired end works, remove this.
            val pairedEnd = false

            val fastqGroupLines = readInKeyFile(fastqGroupingFileName)
            val fastqColNameMap = fastqGroupLines.first
            val fastqLines = fastqGroupLines.second

            //    "originalFile\tnewFile\tBatch\tnumReads\n"
            val originalFileCol = fastqColNameMap["originalFile"] ?: -1
            val newFileCol = fastqColNameMap["newFile"] ?: -1
            val batchCol = fastqColNameMap["Batch"] ?: -1
            val numReadsCol = fastqColNameMap["numReads"] ?: -1

            check(originalFileCol != -1) {"Error processing Fastq Grouping File.  Must have originalFile column."}
            check(newFileCol != -1) {"Error processing Fastq Grouping File.  Must have newFile column."}
            check(batchCol != -1) {"Error processing Fastq Grouping File.  Must have Batch column."}
            check(numReadsCol != -1) {"Error processing Fastq Grouping File.  Must have numReads column."}


            val columnMappingAndLines = readInKeyFile(keyFileName)
            val keyFileColumnNameMap = columnMappingAndLines.first
            val keyFileLines = columnMappingAndLines.second

            //Get out the column indices as they will be consistent for the whole file
            val taxonCol = keyFileColumnNameMap["cultivar"] ?: -1
            val fileNameCol1 = keyFileColumnNameMap["filename"] ?: -1
            val fileNameCol2 = if (keyFileColumnNameMap.containsKey("filename2")) keyFileColumnNameMap["filename2"]
                    ?: -1 else -1
            val flowcellCol = keyFileColumnNameMap["flowcell_lane"] ?: -1

            //Check will automatically throw an IllegalStateException if the logic fails.
            check(taxonCol != -1) { "Error processing keyfile.  Must have cultivar column." }
            check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
            check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }

            val globString = "glob:*{.sam,.bam}"

            val directoryFilePaths = DirectoryCrawler.listPaths(globString, Paths.get(inputFileDir))
            val fileNameToPathMap = directoryFilePaths.associateBy { it.fileName.toString() }
            val directoryFileNames = fileNameToPathMap.keys.toHashSet()

            val keyFileRecordsToFileMap = mutableMapOf<KeyFileUniqueRecord, Pair<String, String>>()
            val keyFileRecordsToEntry = mutableMapOf<List<String>, KeyFileUniqueRecord>()
            val keyFileRecordsToMappingId = mutableMapOf<KeyFileUniqueRecord, Int>()
            val keyFileRecords = keyFileLines
                    .map {
                        val file1 = it[fileNameCol1]
                        val file2 = if (fileNameCol2 != -1) it[fileNameCol2] else ""
                        val taxon = it[taxonCol]
                        //fileGrpName = file names because sometimes user does not know plate and flow cell
                        val uniqueRecord = KeyFileUniqueRecord(methodName, taxon, it[flowcellCol])

                        keyFileRecordsToFileMap[uniqueRecord] = Pair(file1, file2)
                        keyFileRecordsToEntry[it] = uniqueRecord
                        uniqueRecord
                    }
                    .toList()

            verifyNoDuplicatesInKeyFile(keyFileRecords)

            //Set this up for debugging purposes
            val keyFileObj = File(keyFileName)
//            val keyFileNameWithoutExtension = keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
            val keyFileNameWithoutExtension = secondaryStatsDir + "/" + keyFileObj.nameWithoutExtension
            //This is setup this way in case there are 2 fastqs.  The merge Fastq plugin will need to be pair-aware to make this work correctly.
            val fileNameToKeyEntry = keyFileRecordsToFileMap.keys
                    .flatMap { listOf(Pair(keyFileRecordsToFileMap[it]?.first, it),Pair(keyFileRecordsToFileMap[it]?.second, it))  }
                    .toMap()



            //Need to sort the grouping file and organize them by batch
            val groupedLines = fastqLines.groupBy { it[batchCol] }
            //Pass them onto the worker threads through inputChannel


            runBlocking {
                val inputChannel = Channel<List<List<String>>>()
                val resultChannel = Channel<ReadMappingResult>()
                val numThreads = (numThreads() - 2).coerceAtLeast(1)

                launch {
                    myLogger.info("Adding entries to the inputChannel:")
                    groupedLines.forEach {
                        myLogger.info("Adding: ${it.key}")
                        inputChannel.send(it.value)
                    }
                    myLogger.info("Done Adding KeyFile entries to the inputChannel:")
                    inputChannel.close() //Need to close this here to show the workers that it is done adding more data
                }
                //For the number of threads on the machine, set up
                val workerThreads = (1..numThreads).map { threadNum ->
//                    launch { processKeyFileEntry(inputChannel, resultChannel, inputFileDir, fastqColNameMap, pairedEnd, hapIdToRefRangeMap, maxRefRangeError,fileNameToKeyEntry) }
                    launch { processKeyFileEntryVerifyReads(inputChannel, resultChannel, inputFileDir, fastqColNameMap, pairedEnd, hapIdToRefRangeMap, maxRefRangeError,fileNameToKeyEntry, inputFileName = inputFileName) }
                }

                launch {
                    processDBUploading(resultChannel, phg,outputSecondaryMappingStats, hapIdToLengthMap, methodName,
                        methodDescription, keyFileNameWithoutExtension, pluginParams, outputDebugReadMappingDir,
                        isTestMethod, secondaryStatsDir , hapListId, hapIdToRefRangeMap,refRangeToHapIdMap)
                    myLogger.info("Finished writing to the DB.")
                }


                //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
                //If this is not here, this will run forever.
                launch {
                    workerThreads.forEach { it.join() }
                    resultChannel.close()
                }

            }

            //phg is set to null if we are working on a server without access to the DB.
            if(phg != null) {
                outputKeyFiles(keyFileName, keyFileLines, flowcellCol, phg, taxonCol, methodName, keyFileColumnNameMap)

                phg.close()
            }
        } catch(exc : Exception) {
            throw IllegalStateException("Error writing to the DB: ", exc)
        }


    }

    @Deprecated("Use processKeyFileEntryVerifyReads instead.")
    private suspend fun processKeyFileEntry(inputChannel: Channel<List<List<String>>>, resultChannel: Channel<ReadMappingResult>,
                                            inputFileDir: String, fastqColNameMap : Map<String,Int>,
                                            pairedEnd : Boolean = false, hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                            maxRefRangeError: Double, fileNameToKeyEntry:Map<String?,KeyFileUniqueRecord>) = withContext(Dispatchers.Default) {
        for(channelEntry in inputChannel) {



            val firstEntry = channelEntry.first()

            //Try to figure out the BAM/SAM file that matches the grouping.
            val nameWithoutExtension = "${inputFileDir}${File(firstEntry[fastqColNameMap["newFile"]?:0]).nameWithoutExtension}"
            val currentMergedBAMFile = if(File("${nameWithoutExtension}.bam").exists()) {
                "${nameWithoutExtension}.bam"
            }
            else if(File("${nameWithoutExtension}.sam").exists()) {
                "${nameWithoutExtension}.sam"
            }
            else {
                throw java.lang.IllegalStateException("Unable to find corresponding SAM or BAM file for: ${firstEntry[fastqColNameMap["newFile"] ?: 0]}.  Please check your grouping file.")
            }

            val currentSAMReader: SAMRecordIterator = SamReaderFactory.makeDefault()
                                                                    .validationStringency(ValidationStringency.SILENT)
                                                                    .open(File(currentMergedBAMFile)).iterator()
            var samCounter = 0

            for(nonMergedFile in channelEntry) {
                //This object holds the best alignments for a given read.  It gets reset every time we switch to a new read.
                var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()

                //Need a new hapIdMultiset for each record in the key file.
                var hapIdMultiset = HapIdMultiset()
                var hapIdToStatMap = mutableMapOf<Int,HapIdStats>()


                var readCounter = 0
                var currentReadName = ""
                val readSet = mutableSetOf<String>()
                val keyFileRecord = fileNameToKeyEntry[nonMergedFile[fastqColNameMap["originalFile"]?:0]]
                checkNotNull(keyFileRecord) {"Key File entry for: ${nonMergedFile[fastqColNameMap["originalFile"]?:0]} is missing. "}
                while(readCounter < nonMergedFile[fastqColNameMap["numReads"]?:0].toInt()) {
                    samCounter++
                    val currentRecord = currentSAMReader!!.next()

                    //TODO Make this work with paired end.
                    if (filterRead(currentRecord, pairedEnd)) {
                        //Need to add in the name in case we filter out all reads.
                        if(currentReadName != currentRecord.readName) {
                            readCounter++
                            readSet.add(currentRecord.readName)
                            currentReadName = currentRecord.readName
                        }
                        continue
                    }

                    if(currentReadName == "") {
                        readCounter++
                        readSet.add(currentRecord.readName)
                        currentReadName = currentRecord.readName
                    }

                    if(currentReadName != currentRecord.readName) {
                        //Process the batch of alignments
                        addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)

                        readCounter++
                        readSet.add(currentRecord.readName)
                        //Set the new readName
                        currentReadName = currentRecord.readName
                        //Reset the best hit map
                        bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
                    }
                    //Try to add the Alignment to the BestReadMap.  If the alignment is sub-optimal it will be ignored
                    //If it is equal to the alignments in BestReadMap, it will be added
                    //If it is better it will reset BestReadMap and then add this alignment in.
                    attemptToAddSAMRecordToBestReadMap(pairedEnd, currentRecord, currentReadName, bestReadMap)

                }
                //Process the batch of alignments and increment the hapIdSet found in bestReadMap by 1.
                addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)
                //Add the results to the result channel for DB processing
                resultChannel.send(ReadMappingResult(keyFileRecord, hapIdMultiset, hapIdToStatMap))
            }

        }

    }



    private suspend fun processKeyFileEntryVerifyReads(inputChannel: Channel<List<List<String>>>, resultChannel: Channel<ReadMappingResult>,
                                            inputFileDir: String, fastqColNameMap : Map<String,Int>,
                                            pairedEnd : Boolean = false, hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                            maxRefRangeError: Double, fileNameToKeyEntry:Map<String?,KeyFileUniqueRecord>, inputFileName:String = "") = withContext(Dispatchers.Default) {
        for(channelEntry in inputChannel) {
            val firstEntry = channelEntry.first()


            // walk through the grouping file and determine what the start an end read bounds are

            val readIdToOriginalFileMap = if(fastqColNameMap.containsKey("startRead") && fastqColNameMap.containsKey("endRead")) {
                getReadMappingsBasedOnColumns(channelEntry,fastqColNameMap)
            }
            else {
                getReadMappingBasedOnCount(channelEntry, fastqColNameMap)
            }

            //Try to figure out the BAM/SAM file that matches the grouping.
            val nameWithoutExtension = "${inputFileDir}${File(firstEntry[fastqColNameMap["newFile"]?:0]).nameWithoutExtension}"
            val currentMergedBAMFile = if(File("${nameWithoutExtension}.bam").exists()) {
                "${nameWithoutExtension}.bam"
            }
            else if(File("${nameWithoutExtension}.sam").exists()) {
                "${nameWithoutExtension}.sam"
            }
            else {
                throw java.lang.IllegalStateException("Unable to find corresponding SAM or BAM file for: ${firstEntry[fastqColNameMap["newFile"] ?: 0]}.  Please check your grouping file.")
            }

            //Here we check to see if we are just processing a single merged bam file.
            //If so and this is not the current file to process we skip before any real work is done.
            if(inputFileName!="" && inputFileName != currentMergedBAMFile) {
                println("${inputFileName}\t${currentMergedBAMFile}")
                continue
            }

            val currentSAMReader: SAMRecordIterator = SamReaderFactory.makeDefault()
                .validationStringency(ValidationStringency.SILENT)
                .setOption(SamReaderFactory.Option.CACHE_FILE_BASED_INDEXES,false)
                .setOption(SamReaderFactory.Option.EAGERLY_DECODE,false)
                .setOption(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS,false)
                .setOption(SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS,false)
                .open(File(currentMergedBAMFile)).iterator()

            //Need a processed ReadCounter Map
            val originalFileCol = fastqColNameMap["originalFile"]
            check(originalFileCol != null) {"Error originalFile column is missing from the Grouping File"}
            val processedReadByOrigFileMap = channelEntry.map{ it[originalFileCol] }.filterNotNull().associate { Pair(it, 0) }.toMutableMap()

            //We also need a hapIdMultiset For each original file
            val hapIdMultisetByOrigFileMap = channelEntry.associate { Pair(it[originalFileCol], HapIdMultiset()) }

            //And we need a hapIdToStatMap for each original file
            val hapIdStatMapByOrigFileMap = channelEntry.associate { Pair(it[originalFileCol], mutableMapOf<Int,HapIdStats>()) }

            //This holds the best alignments for the current read.  It will reset every time we switch to a new read
            var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()

            var currentReadName = ""
            var counter = 0
            //This part is changed
            while(currentSAMReader.hasNext()) {

                val currentRecord = currentSAMReader!!.next()

                if(counter %100000 == 0) {
                    myLogger.info("Processing read : ${counter} in file, ${nameWithoutExtension}")
                }
                counter++
                //TODO Make this work with paired end.
                if (filterRead(currentRecord, pairedEnd)) {
                    //Need to add in the name in case we filter out all reads.
                    if(currentReadName != currentRecord.readName) {
                        if(currentReadName != "") {
                            //process the current read
                            getCorrectMapAndAddToHapIdMap(
                                readIdToOriginalFileMap,
                                currentReadName,
                                hapIdMultisetByOrigFileMap,
                                hapIdStatMapByOrigFileMap,
                                processedReadByOrigFileMap,
                                currentMergedBAMFile,
                                bestReadMap,
                                hapIdToRefRangeMap,
                                maxRefRangeError,
                                pairedEnd
                            )
                            bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
                        }
                        currentReadName = currentRecord.readName
                    }
                    continue
                }

                if(currentReadName == "") {
                    currentReadName = currentRecord.readName
                }

                if(currentReadName != currentRecord.readName) {
                    //Process the batch of alignments

                    getCorrectMapAndAddToHapIdMap(
                        readIdToOriginalFileMap,
                        currentReadName,
                        hapIdMultisetByOrigFileMap,
                        hapIdStatMapByOrigFileMap,
                        processedReadByOrigFileMap,
                        currentMergedBAMFile,
                        bestReadMap,
                        hapIdToRefRangeMap,
                        maxRefRangeError,
                        pairedEnd
                    )

                    //Set the new readName
                    currentReadName = currentRecord.readName
                    //Reset the best hit map
                    bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
                }
                //Try to add the Alignment to the BestReadMap.  If the alignment is sub-optimal it will be ignored
                //If it is equal to the alignments in BestReadMap, it will be added
                //If it is better it will reset BestReadMap and then add this alignment in.
                attemptToAddSAMRecordToBestReadMap(pairedEnd, currentRecord, currentReadName, bestReadMap)
            }
            //Process the lastbatch of alignments and increment the hapIdSet found in bestReadMap by 1.
            //Get the original file name
            getCorrectMapAndAddToHapIdMap(
                readIdToOriginalFileMap,
                currentReadName,
                hapIdMultisetByOrigFileMap,
                hapIdStatMapByOrigFileMap,
                processedReadByOrigFileMap,
                currentMergedBAMFile,
                bestReadMap,
                hapIdToRefRangeMap,
                maxRefRangeError,
                pairedEnd
            )

            //Once the sam file is done processing we need to go through all the original file maps and submit to the DB writer
            //first we should check to make sure we have processed the expected number of reads
            val readyForDBWrite = ( ignoreWarnings() || verifyAllReadsHaveBeenProcessed(readIdToOriginalFileMap,processedReadByOrigFileMap,currentMergedBAMFile)  )&&
                                    verifyKeyFileEntries(processedReadByOrigFileMap, fileNameToKeyEntry)

            currentSAMReader.close()

            if(readyForDBWrite) {
                for (originalFileName in hapIdMultisetByOrigFileMap.keys) {
                    val currentHapIdMultiset = hapIdMultisetByOrigFileMap[originalFileName]
                    val currentHapIdToStatMap = hapIdStatMapByOrigFileMap[originalFileName]
                    val keyFileRecord = fileNameToKeyEntry[originalFileName]

                    checkNotNull(currentHapIdMultiset) { "Submitting To DB Thread: Error in processing ${currentMergedBAMFile}.  Does not have a HapIdMultiset created for originalFile Name: ${originalFileName}" }
                    checkNotNull(currentHapIdToStatMap) { "Submitting To DB Thread: Error in processing ${currentMergedBAMFile}.  Does not have a hapIdToStatMap created for originalFile Name: ${originalFileName}" }
                    checkNotNull(keyFileRecord) { "Submitting To DB Thread: Error in processing ${currentMergedBAMFile}.  Does not have a key File Entry created for originalFile Name: ${originalFileName}" }

                    resultChannel.send(ReadMappingResult(keyFileRecord, currentHapIdMultiset, currentHapIdToStatMap))

                }
            }

        }

    }



    private fun verifyAllReadsHaveBeenProcessed(readIdToOriginalFileMap: RangeMap<Int, String>, processedReadByOrigFileMap: MutableMap<String, Int>, batchFileName: String) : Boolean {
        val mapOfRanges = readIdToOriginalFileMap.asMapOfRanges()
        var isGoodToWriteToDB = true
        for((range,fileName) in mapOfRanges) {
            val numReadsFromGroupingFile = range.upperEndpoint() - range.lowerEndpoint() + 1

            val processedNumReads = processedReadByOrigFileMap[fileName] ?:-1

            if(numReadsFromGroupingFile != processedNumReads) {
                myLogger.error("Incorrect number of counts for ${fileName}: GrpFile:${numReadsFromGroupingFile}, Found: ${processedNumReads}.  Please Rerun the alignment(Minimap2) for batch: ${batchFileName}")
                isGoodToWriteToDB = false
            }

        }

        if(!isGoodToWriteToDB) {
            myLogger.error("Please rerun the alignment for batch: ${batchFileName}")
        }

        return isGoodToWriteToDB
    }

    /**
     * Function to check to see if we have a key file entry for every file we tested.  Probably unnecessary to do, but a final check before we write to the DB.
     */
    private fun verifyKeyFileEntries(processedReadByOrigFileMap: MutableMap<String, Int>, fileNameToKeyEntry: Map<String?, KeyFileUniqueRecord>): Boolean {
        var isGoodToWriteToDB = true
        for(fileName in processedReadByOrigFileMap.keys) {
            if(!fileNameToKeyEntry.keys.contains(fileName)) {
                myLogger.error("Missing KeyFile entry for file: ${fileName}.  Please update your key file")

                isGoodToWriteToDB = false
            }
        }

        return isGoodToWriteToDB
    }

    private fun getCorrectMapAndAddToHapIdMap(
        readIdToOriginalFileMap: RangeMap<Int, String>,
        currentReadName: String,
        hapIdMultisetByOrigFileMap: Map<String, HapIdMultiset>,
        hapIdStatMapByOrigFileMap: Map<String, MutableMap<Int, HapIdStats>>,
        processedReadByOrigFileMap: MutableMap<String,Int>,
        currentMergedBAMFile: String,
        bestReadMap: MutableMap<Pair<String, Boolean>, BestAlignmentGroup>,
        hapIdToRefRangeMap: Map<Int, ReferenceRange>,
        maxRefRangeError: Double,
        pairedEnd: Boolean
    ) {
        //Get the original file name
        val originalFileName = readIdToOriginalFileMap[currentReadName.toInt()]
        val currentHapIdMultiset = hapIdMultisetByOrigFileMap[originalFileName]
        val currentHapIdToStatMap = hapIdStatMapByOrigFileMap[originalFileName]

        checkNotNull(originalFileName) {"Error in processing ${currentMergedBAMFile}.  Does not have an originalFile Name.  Read Name: ${currentReadName}"}
        checkNotNull(currentHapIdMultiset) { "Error in processing ${currentMergedBAMFile}.  Does not have a HapIdMultiset created for originalFile Name: ${originalFileName} Read Name: ${currentReadName}" }
        checkNotNull(currentHapIdToStatMap) { "Error in processing ${currentMergedBAMFile}.  Does not have a hapIdToStatMap created for originalFile Name: ${originalFileName} Read Name: ${currentReadName}" }

        addBestReadMapToHapIdMultiset(
            bestReadMap,
            hapIdToRefRangeMap,
            maxRefRangeError,
            pairedEnd,
            currentHapIdMultiset,
            currentHapIdToStatMap
        )
        //Increment the read counter for this file name
        processedReadByOrigFileMap[originalFileName] = (processedReadByOrigFileMap[originalFileName]?:0) + 1
    }


    /**
     * Function that will read the startRead, endRead and originalFile columns from the grouping file to determine which reads match which original file group.
     * This is to be used later to prevent any read mixup from messing up the mapping results.
     */
    private fun getReadMappingsBasedOnColumns(grouping: List<List<String>>, fastqColNameMap: Map<String, Int>): RangeMap<Int,String> {
        val mappingIdToRangeMap = TreeRangeMap.create<Int,String>()

        val startReadCol = fastqColNameMap["startRead"]
        val endReadCol = fastqColNameMap["endRead"]
        val originalFileCol = fastqColNameMap["originalFile"]

        check(startReadCol != null) { "Error startRead Column is missing from the Grouping File" }
        check(endReadCol != null) {"Error endRead Column is missing from the Grouping File" }
        check(originalFileCol != null) {"Error originalFile Column is missing from the Grouping File" }
        for(file in grouping) {
            val startReadId = file[startReadCol].toInt()
            val endReadId = file[endReadCol].toInt()
            val originalFile = file[originalFileCol]

            mappingIdToRangeMap.put(Range.closed(startReadId, endReadId), originalFile )
        }

        return mappingIdToRangeMap
    }


    private fun getReadMappingBasedOnCount(grouping: List<List<String>>, fastqColNameMap: Map<String, Int>): RangeMap<Int, String> {
        val mappingIdToRangeMap = TreeRangeMap.create<Int,String>()

        val numReadCol = fastqColNameMap["numReads"]
        val originalFileCol = fastqColNameMap["originalFile"]

        check(originalFileCol != null) {"Error originalFile Column is missing from the Grouping File" }
        check(numReadCol != null) {"Error numReads Column is missing from the Grouping File" }

        var readCounter = 0
        for(file in grouping) {
            val originalFile = file[originalFileCol]
            val numReads = file[numReadCol].toInt()

            val lastRead = readCounter + numReads -1 //Need to subtract 1 here because numReads is a size and we are 0 based

            mappingIdToRangeMap.put(Range.closed(readCounter, lastRead), originalFile)

            readCounter = lastRead + 1 //Add one as the next read is the first read for the next file
        }

        return mappingIdToRangeMap
    }

    data class ReadMappingResult(val keyFileRecord: KeyFileUniqueRecord, val hapIdMultiset : HapIdMultiset, val hapIdToStatMap: MutableMap<Int,HapIdStats>)

    private suspend fun processDBUploading(resultChannel: Channel<ReadMappingResult>, phg: PHGdbAccess?,
                                           outputSecondaryMappingStats: Boolean = false, hapIdToLengthMap: Map<Int, Int>,
                                           methodName: String, methodDescription: String?,
                                           keyFileNameWithoutExtension: String, pluginParams: Map<String, String>,
                                           outputDebugReadMappingDir: String, isTestMethod: Boolean,
                                           outputSecondaryStatsDir : String, hapListId: Int,
                                           hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                           refRangeToHapIdMap: Map<Int, Map<Int,Int>>) = withContext(Dispatchers.Default) {

        val keyFileRecordsToMappingId = mutableMapOf<KeyFileUniqueRecord, Int>()
        for (readMappingRecord in resultChannel) {
            val hapIdMultiset = readMappingRecord.hapIdMultiset
            val hapIdToStatMap = readMappingRecord.hapIdToStatMap
            val keyFileRecord = readMappingRecord.keyFileRecord
            val taxon = keyFileRecord.taxonName
            val fileGroupName = keyFileRecord.fileGroupName

            println("Taxon: ${taxon}")
            println("FileGrpName: ${fileGroupName}")


//            val outputDebugFile = if(outputSecondaryMappingStats) {"${outputSecondaryStatsDir}/${keyFileNameWithoutExtension}_${taxon}_${fileGroupName}_additionalMappingStats.txt" } else { "" }
            val outputDebugFile = if(outputSecondaryMappingStats) {"${keyFileNameWithoutExtension}_${taxon}_${fileGroupName}_additionalMappingStats.txt" } else { "" }
            exportHapIdStats(outputDebugFile, hapIdToStatMap, hapIdToLengthMap)

            val multisetMap = hapIdMultiset.getMap()
            val hapIdMapping = multisetMap.keys.map { Pair(it.toList(),multisetMap[it]?:0) }.toMap()

            // SOme of our cultivars have a / in their names, which is a problem
            // when the cultivar is used in the file name as below
            // This should not change the value of taxon
            var fileNameTaxon = taxon.replace("/","-")
            val outputFileName = "${outputDebugReadMappingDir}${fileNameTaxon}_${fileGroupName}_ReadMapping.txt"
            myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")

            val finalParamMap = pluginParams.toMutableMap()
            exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                    DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))

            //If phg is set to null we only want to output the files not write to the DB.
            if(phg != null) {
                loadReadMappingsToDB(
                    hapIdMapping, taxon, fileGroupName, pluginParams, methodDescription, phg, methodName,
                    keyFileRecordsToMappingId, keyFileRecord, outputDebugReadMappingDir, isTestMethod, hapListId,
                    hapIdToRefRangeMap, refRangeToHapIdMap
                )
            }

        }


    }



    override fun getIcon(): ImageIcon? {
        val imageURL = MultisampleBAMToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MultisampleBAMToMappingPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to take multisample BAM files and create readMapping counts for each sample. "
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will run in single end mode.
     *  Otherwise will be paired.
     *
     * @return keyFile
     */
    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will run in single end mode.
     *  Otherwise will be paired.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will run in single end mode.  Otherwise will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will run in single end mode.  Otherwise will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): MultisampleBAMToMappingPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Grouping file created by MergeFastqPlugin
     *
     * @return Fastq Grouping File
     */
    /**
     * Grouping file created by MergeFastqPlugin
     *
     * @return Fastq Grouping File
     */
    fun fastqGroupingFile(): String {
        return fastqGroupingFile.value()
    }

    /**
     * Set Fastq Grouping File. Grouping file created by MergeFastqPlugin
     *
     * @param value Fastq Grouping File
     *
     * @return this plugin
     */
    /**
     * Set Fastq Grouping File. Grouping file created by MergeFastqPlugin
     *
     * @param value Fastq Grouping File
     *
     * @return this plugin
     */
    fun fastqGroupingFile(value: String): MultisampleBAMToMappingPlugin {
        fastqGroupingFile = PluginParameter<String>(fastqGroupingFile, value)
        return this
    }

    /**
     * Name of the SAM/BAM dir to process.
     *
     * @return SAM/BAM dir to process
     */
    /**
     * Name of the SAM/BAM dir to process.
     *
     * @return SAM/BAM dir to process
     */
    fun samDir(): String {
        return samDir.value()
    }

    /**
     * Set SAM/BAM dir to process. Name of the SAM/BAM dir
     * to process.
     *
     * @param value SAM/BAM dir to process
     *
     * @return this plugin
     */
    /**
     * Set SAM/BAM dir to process. Name of the SAM/BAM dir
     * to process.
     *
     * @param value SAM/BAM dir to process
     *
     * @return this plugin
     */
    fun samDir(value: String): MultisampleBAMToMappingPlugin {
        samDir = PluginParameter<String>(samDir, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError(): Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double): MultisampleBAMToMappingPlugin {
        maxRefRangeError = PluginParameter<Double>(maxRefRangeError, value)
        return this
    }

    /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): MultisampleBAMToMappingPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    fun methodDescription(): String? {
        return methodDescription.value()
    }

    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value: String): MultisampleBAMToMappingPlugin {
        methodDescription = PluginParameter<String>(methodDescription, value)
        return this
    }

    /**
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @return Debug Directory to write out read Mapping files.
     */
    /**
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @return Debug Directory to write out read Mapping files.
     */
    fun outputDebugDir(): String {
        return outputDebugDir.value()
    }

    /**
     * Set Debug Directory to write out read Mapping files..
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @param value Debug Directory to write out read Mapping files.
     *
     * @return this plugin
     */
    /**
     * Set Debug Directory to write out read Mapping files..
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @param value Debug Directory to write out read Mapping files.
     *
     * @return this plugin
     */
    fun outputDebugDir(value: String): MultisampleBAMToMappingPlugin {
        outputDebugDir = PluginParameter<String>(outputDebugDir, value)
        return this
    }

    /**
     * Ouptput Secondary Mapping Statistics such as total
     * AS for each haplotype ID
     *
     * @return Output secondary mapping statistics.
     */
    /**
     * Ouptput Secondary Mapping Statistics such as total
     * AS for each haplotype ID
     *
     * @return Output secondary mapping statistics.
     */
    fun outputSecondaryMappingStats(): Boolean {
        return outputSecondaryMappingStats.value()
    }


    /**
     * Set Output secondary mapping statistics.. Ouptput Secondary
     * Mapping Statistics such as total AS for each haplotype
     * ID
     *
     * @param value Output secondary mapping statistics.
     *
     * @return this plugin
     */
    fun outputSecondaryMappingStats(value: Boolean): MultisampleBAMToMappingPlugin {
        outputSecondaryMappingStats = PluginParameter<Boolean>(outputSecondaryMappingStats, value)
        return this
    }

    /**
     * Directory to store the mapping stats.
     *
     * @return Output Secondary Mapping Stat Directory
     */
    fun mappingStatDir(): String {
        return mappingStatDir.value()
    }

    /**
     * Set Output Secondary Mapping Stat Directory. Directory
     * to store the mapping stats.
     *
     * @param value Output Secondary Mapping Stat Directory
     *
     * @return this plugin
     */
    fun mappingStatDir(value: String): MultisampleBAMToMappingPlugin {
        mappingStatDir = PluginParameter<String>(mappingStatDir, value)
        return this
    }

    /**

     * Number of threads used to upload
     *
     * @return Num Threads
     */
    fun numThreads() : Int {
        return myNumThreads.value()
    }


    /**
     * Set Num Threads. Number of threads used to upload
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value : Int) : MultisampleBAMToMappingPlugin {
        myNumThreads = PluginParameter(myNumThreads, value)
        return this
    }
     /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): MultisampleBAMToMappingPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }

    /**
     * If set to true, the read mappings will be written to
     * the db.  Otherwise nothing will be written.
     *
     * @return Update D B
     */
    fun updateDB(): Boolean {
        return updateDB.value()
    }

    /**
     * Set Update D B. If set to true, the read mappings will
     * be written to the db.  Otherwise nothing will be written.
     *
     * @param value Update D B
     *
     * @return this plugin
     */
    fun updateDB(value: Boolean): MultisampleBAMToMappingPlugin {
        updateDB = PluginParameter<Boolean>(updateDB, value)
        return this
    }
    /**
     * Ignore Warnings when checking number of reads per unmerged
     * file.  If this is enabled, this will write to the DB
     * even though there may be errors.
     *
     * @return Ignore Warnings
     */
    fun ignoreWarnings(): Boolean {
        return ignoreWarnings.value()
    }

    /**
     * Set Ignore Warnings. Ignore Warnings when checking
     * number of reads per unmerged file.  If this is enabled,
     * this will write to the DB even though there may be
     * errors.
     *
     * @param value Ignore Warnings
     *
     * @return this plugin
     */
    fun ignoreWarnings(value: Boolean): MultisampleBAMToMappingPlugin {
        ignoreWarnings = PluginParameter<Boolean>(ignoreWarnings, value)
        return this
    }

    /**
     * If set to true, will require the input of a JSON file
     * created by CreateHapIdMapsPlugin and will got require
     * a Graph object input.
     *
     * @return Run Without Graph
     */
    fun runWithoutGraph(): Boolean {
        return runWithoutGraph.value()
    }

    /**
     * Set Run Without Graph. If set to true, will require
     * the input of a JSON file created by CreateHapIdMapsPlugin
     * and will got require a Graph object input.
     *
     * @param value Run Without Graph
     *
     * @return this plugin
     */
    fun runWithoutGraph(value: Boolean): MultisampleBAMToMappingPlugin {
        runWithoutGraph = PluginParameter<Boolean>(runWithoutGraph, value)
        return this
    }

    /**
     * Location of the HapIdMapFile where the graph Information
     * can be found.
     *
     * @return Hap Id Map File
     */
    fun hapIdMapFile(): String {
        return hapIdMapFile.value()
    }

    /**
     * Set Hap Id Map File. Location of the HapIdMapFile where
     * the graph Information can be found.
     *
     * @param value Hap Id Map File
     *
     * @return this plugin
     */
    fun hapIdMapFile(value: String): MultisampleBAMToMappingPlugin {
        hapIdMapFile = PluginParameter<String>(hapIdMapFile, value)
        return this
    }

    /**
     * Input Multisample SAM file to be run through.  If this
     * is not set it will run everything in the inputDir
     *
     * @return Input S A M File
     */
    fun inputFile(): String {
        return inputFile.value()
    }

    /**
     * Set Input S A M File. Input Multisample SAM file to
     * be run through.  If this is not set it will run everything
     * in the inputDir
     *
     * @param value Input S A M File
     *
     * @return this plugin
     */
    fun inputFile(value: String): MultisampleBAMToMappingPlugin {
        inputFile = PluginParameter<String>(inputFile, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(MultisampleBAMToMappingPlugin::class.java)
}