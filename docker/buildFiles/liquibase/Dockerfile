FROM java

WORKDIR /

# add tassel and phg - needed to run the Plugin scripts that check the db and execute liquibase updates
#RUN apk add --no-cache -y git
RUN apt-get install -y git

RUN git clone https://bitbucket.org/tasseladmin/tassel-5-standalone.git

ENV PATH $PATH:/liquibase

ARG LIQUIBASE_VERSION=4.7.0

# mkdir liquibase directory, grab liquibase download, uncompress to the /liquibase directory

RUN mkdir -p /liquibase \
    && cd /liquibase \
    && wget -O liquibase-${LIQUIBASE_VERSION}.tar.gz "https://github.com/liquibase/liquibase/releases/download/v${LIQUIBASE_VERSION}/liquibase-${LIQUIBASE_VERSION}.tar.gz" \
    && tar -xzf liquibase-${LIQUIBASE_VERSION}.tar.gz \
    && rm liquibase-${LIQUIBASE_VERSION}.tar.gz

RUN mkdir /liquibase/changelogs
RUN mkdir /liquibase/changelogs/changesets

# Add changelogs
COPY ./changelogs/changesets/* /liquibase/changelogs/changesets/
ADD changelogs/db.changelog-0.0.xml /liquibase/changelogs
ADD changelogs/db.changelog-0.1.xml /liquibase/changelogs
ADD changelogs/db.changelog-master.xml /liquibase/changelogs

# Add scripts to process the updates against a database
ADD scripts/RunLiquibaseUpdates.sh /liquibase
RUN chmod +x /liquibase/RunLiquibaseUpdates.sh

# we need to be in liquibase directory for the commands to work as
# all changelog files/dirs are relative to the working directory
WORKDIR /liquibase

