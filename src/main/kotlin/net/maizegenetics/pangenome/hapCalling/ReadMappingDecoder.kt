package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.GZipCompression
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import java.util.*

/**
 * ReadMappingDecoder gets an encoded read mapping from a database based on a read mapping id and
 * decodes it to return counts of haplotype id sets as a map (Map<Int, Map<Int,Int>>). It correctly decodes either BitSet
 * encoded haplotype set counts or the older format without BitSet encoding.
 * The class has two constructors.
 * One of them takes a PHGdbAccess object and the other takes no arguments and instantiates a PHGdbAccess object based
 * on connection parameters stored in the ParameterCache.
 */
class ReadMappingDecoder(val phgAccess: PHGdbAccess) {

    /**
     * A cache of reference range to haplotype index maps used to decode BitSet encoded read mappings.
     * The map key is haplotype_list_ids.
     */
    private val haplotypeListMap = mutableMapOf<Int, Map<Int, Map<Int,Int>>>()

    constructor() : this(PHGdbAccess(DBLoadingUtils.connection(false)))

    fun getDecodedReadMappingForMappingId(readMappingId: Int) : Map<List<Int>, Int> {
        // get the mapping_data and the haplotype list id
        val mappingData = phgAccess.getReadMappingsForId(readMappingId)

        //if haplotype_list_id = -1, use the nonBitSetMethod from ReadMappingUtils [decodeHapIdMapping(encodedReadMapping : ByteArray)]
        if (mappingData.second == -1) return decodeHapIdMapping(mappingData.first)

        //getRefToHapidIndex, a map of ref_range_id -> map of index -> haplotype id
        val refToHapidIndex = getRefToHapidIndexMapFromListId(mappingData.second)

        //return decodeHapIdBitSets
        return decodeHapIdBitSets(mappingData.first, refToHapidIndex)
    }

    private fun getRefToHapidIndexMapFromListId(haplotypeListId: Int) : Map<Int, Map<Int,Int>> {
        //if the HapidIndexMap has been cached, it is returned, otherwise a new one is created (and added to the cache)
        return haplotypeListMap[haplotypeListId] ?: getRefToHapidIndexMapFromDB(haplotypeListId)
    }

    private fun getRefToHapidIndexMapFromDB( haplotypeListId: Int) :Map<Int, Map<Int,Int>> {
        //Get the individual haplotype ids that comprise the list
        val refrangeHapidPairs = phgAccess.getReferenceRangeHapidPairs(haplotypeListId)

        //create a map of ref_range_id -> list of haplotype ids
        val refRangeToUnorderedListMap =  refrangeHapidPairs.map { Pair(it[0], it[1]) }.groupBy({it.first}, {it.second})

        //convert each of the haplotype id lists into a map of index -> haplotype id
        // to return a map of ref_range_id -> (map of index -> haplotype id)
        val refToHapMap =  refRangeToUnorderedListMap.entries.associate { (refrange, hapidList) ->
            val hapidIndex = hapidList.sorted().mapIndexed { index, hapid ->  Pair(index,hapid)}.toMap()
            Pair(refrange, hapidIndex)
        }

        haplotypeListMap[haplotypeListId] = refToHapMap

        return refToHapMap
    }


    /**
     * Decodes a read mapping [mappingData] that has been encoded as a BitSet.
     * [refRangeToHapIdMap] is a map of refRangeId to a map of index to haplotype id,
     * where the index represents the order of the haplotype id in that ReferenceRange.
     * This data structure is used to decode the BitSet because it relates the set bits (the index) to haplotype ids.
     */
    private fun decodeHapIdBitSets(mappingData: ByteArray, refRangeToHapIdMap : Map<Int, Map<Int,Int>>)  : Map<List<Int>,Int> {
        //separators match those in encodeHapIdsAsBitSet
        val recordSeparator = "\n"
        val rangeidSeparator = ":"
        val setcountSeparator = ";"
        val numberSeparator = ","

        //HapidSetCounts for each ReferenceRange id were coded as
        // refRangeId:count, long, long, ...;count, long, long, ...;...

        val dataAsString = GZipCompression.decompress(mappingData)
        val rangeCounts = dataAsString.split(recordSeparator)
        return rangeCounts.map { rangeStr ->
            val rangeData = rangeStr.split(rangeidSeparator)
            val rangeid = rangeData[0].toInt()

            val hapidsInRefrange = refRangeToHapIdMap[rangeid]

            //rangeData[1] is a setcountSeparator separated list of HapidSetCounts (count,long,long...).
            // This next block converts each of the (count,long,long...) strings to a Pair(List<Int>, count)
            // To do that, the longs are converted to a LongArray, which is converted to a BitSet,
            // which is decoded to a list of haplotype ids
            rangeData[1].split(setcountSeparator).map { setCountStr ->
                val firstSplit = setCountStr.split(numberSeparator, limit = 2)
                val count = firstSplit[0].toInt()
                val longArray = firstSplit[1].split(numberSeparator).map{ it.toLong()}.toLongArray()
                val hapIdSet = if (hapidsInRefrange == null) listOf<Int>() else bitSetToIntList(BitSet.valueOf(longArray)).map { hapidsInRefrange[it] ?: -1}
                Pair(hapIdSet, count)
            }
        }.flatten().filter { it.first.isNotEmpty() }.toMap()
    }

}