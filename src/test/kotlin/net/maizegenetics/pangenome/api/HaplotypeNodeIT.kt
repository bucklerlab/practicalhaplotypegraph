package net.maizegenetics.pangenome.api

import java.util.stream.Collectors
import net.maizegenetics.pangenome.api.HaplotypeNode.VariantInfo
import net.maizegenetics.taxa.Taxon
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import net.maizegenetics.junit.SmallSeqExtension
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExtendWith(SmallSeqExtension::class)
class HaplotypeNodeIT {

    companion object {
        lateinit var hapGraph: HaplotypeGraph
        lateinit var refGraph: HaplotypeGraph

        private const val hapGraphMethod = "GATK_PIPELINE"
        private const val refGraphMethod = "B73Ref_method"

        @JvmStatic
        @BeforeAll
        fun setup() {
            hapGraph = HaplotypeGraphBuilderPlugin(null, false)
                    .configFile(SmallSeqExtension.configFilePath)
                    .methods(hapGraphMethod)
                    .includeVariantContexts(true)
                    .localGVCFFolder(SmallSeqExtension.inputGVCFDir)
                    .build()
            refGraph = HaplotypeGraphBuilderPlugin(null, false)
                    .configFile(SmallSeqExtension.configFilePath)
                    .methods(refGraphMethod)
                    .includeVariantContexts(true)
                    .localGVCFFolder(SmallSeqExtension.inputGVCFDir)
                    .build()
        }
    }
    
    @Test
    fun testRefVariants() {
        refGraph.nodeStream().filter { hn ->
            val range = hn.referenceRange()
            val length = range.end() - range.start() + 1
            length > 10
        }.forEach { hn ->
            val optList = hn.variantInfos()
            if (optList.isPresent) {
                val infoList = optList.get()
                val numberOfVariants = infoList.stream().filter{info -> info.isVariant}.count()
                assertEquals(0, numberOfVariants)
            }
        }
    }
    
    @Test
    fun testVariantInfoFirstRefRange() {
        assertEquals(1, hapGraph.numberOfChromosomes())
        assertEquals(10, hapGraph.numberOfRanges())
        assertEquals(6, hapGraph.totalNumberTaxa())

        val hapNodes = hapGraph.nodes(hapGraph.firstReferenceRange())
        assertEquals(6, hapNodes.size)

        val myNode = hapNodes[0]
        assertEquals("Ref", myNode.taxaList().stream().map(Taxon::getName).collect(Collectors.joining(",")))

        val taxonName = myNode.taxaList().taxaName(0)
        assertEquals("Ref", taxonName)

        val infoList = myNode.variantInfos().orElse(arrayListOf())

        val numberOfVariants = infoList.stream().filter(VariantInfo::isVariant).count()
        assertEquals(0, numberOfVariants)

        val numberOfIndels = infoList.stream().filter(VariantInfo::isIndel).count()
        assertEquals(0, numberOfIndels)

        val numberOfRefs = infoList.stream().filter{vi -> vi.genotypeString().equals(VariantInfo.Ref)}.count()
        assertEquals(0, numberOfRefs)

        val numberOfNonRefs = infoList.stream().filter{vi -> vi.genotypeString().equals(VariantInfo.NonRef)}.count()
        assertEquals(0, numberOfNonRefs)

        val numberOfMissing = infoList.stream().filter{vi -> vi.genotypeString().equals(VariantInfo.missing)}.count()
        assertEquals(0, numberOfMissing)
    }

    @Test
    fun areVariantsCalledCorrectly() {
        hapGraph.referenceRangeStream().forEach{ range ->
            val taxonName = "LineB"

            val hapNode = hapGraph.nodes(range).stream()
                .filter{ hn -> hn.taxaList().indexOf(taxonName) >= 0}.findAny().get()

            val otherSeq = hapNode.haplotypeSequence().sequence()
            val refSeq = refGraph.nodes(range)[0].haplotypeSequence().sequence()

            assertEquals(refSeq.length, otherSeq.length, "ref and alt sequences have different lengths at ${range.start()}")

            val variantInfoList = hapNode.variantInfos().get()

            var numberOfVar = 0
            var numberOfN = 0

            variantInfoList.forEach { varInfo ->
                if (varInfo.isVariant) {
                    if (!varInfo.altAlleleString().equals("N")) {
                        val relativePos = varInfo.start() - range.start()
                        val altString = otherSeq.substring(relativePos, relativePos + 1)
                        val refString = refSeq.substring(relativePos, relativePos + 1)
                        val test1 = varInfo.altAlleleString().equals(altString)
                        val test2 = varInfo.refAlleleString().equals(refString)

                        assertTrue(test1 || varInfo.isIndel, "other seq != altAllele at ${varInfo.start()}" )
                        assertTrue(test2 || varInfo.isIndel, "ref seq != refAllele at ${varInfo.start()}")

                        numberOfVar++
                    } else if (varInfo.altAlleleString().equals("N")) {
                        val relativePos = varInfo.start() - range.start()
                        val seqchar = otherSeq[relativePos]

                        assertEquals('N', otherSeq[relativePos], "alt allele = N but sequence = $seqchar at sequence position ${varInfo.start()}")

                        numberOfN++
                    }
                }
            }
        }
    }
}
