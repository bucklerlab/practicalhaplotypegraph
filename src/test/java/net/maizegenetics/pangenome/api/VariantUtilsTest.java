package net.maizegenetics.pangenome.api;

import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class VariantUtilsTest {

    private long encodeRefBlock(int pos, byte dp, int length) {
        long encoded = 1;

        encoded = (encoded <<23) | length;

        encoded = (encoded << 8) | dp;

        encoded = (encoded << 32) | pos;

        return encoded;
    }

    // int variantID = (int)((variantEncoded>>32)&0xFFFF);
    //        int refDepth = AlleleDepthUtil.depthByteToInt((byte)((variantEncoded>>24) & 0xFF));
    //        int altDepth = AlleleDepthUtil.depthByteToInt((byte)((variantEncoded>>16) & 0xFF));
    private long encodeVariant(int variantID, byte refDepth, byte altDepth) {
        long encoded = variantID;

        encoded = (encoded << 8) | refDepth;
        encoded = (encoded << 8) | altDepth;
        encoded = (encoded << 16);

        System.out.println(encoded);
        System.out.println(Long.toHexString(encoded));

        return encoded;
    }
}
