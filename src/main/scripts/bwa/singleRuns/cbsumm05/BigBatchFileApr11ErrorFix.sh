#!/bin/bash

LINE_ARRAY=(Ky21 Ky21 Ky21)
ID_ARRAY=(4881_6665_17275_HB1JCADXX_Ky21_CAGATC 4881_6665_17275_HFFGKADXX_Ky21_CAGATC Ky21_HL5WNCCXX_L4)
FIRST_FILE_NAME=(4881_6665_17275_HB1JCADXX_Ky21_CAGATC_R1.fastq.gz 4881_6665_17275_HFFGKADXX_Ky21_CAGATC_R1.fastq.gz Ky21_HL5WNCCXX_L4_1.clean.renamed.fq.gz)
SECOND_FILE_NAME=(4881_6665_17275_HB1JCADXX_Ky21_CAGATC_R2.fastq.gz 4881_6665_17275_HFFGKADXX_Ky21_CAGATC_R2.fastq.gz Ky21_HL5WNCCXX_L4_2.clean.renamed.fq.gz)
# to get the arrayLength, ${#ArrayName[@]}

#Set up the senteion variables and the reference path
license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02

SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
SENTIEONBWA='/programs/sentieon-genomics-201611.02/bin/bwa'
NUMBER_THREADS=20
REFERENCE='/SSD/zrm22/BAMFileGeneration/agpv4Ref/Zea_mays.AGPv4.dna.toplevel.fa.gz'

for((i=0;i<${#LINE_ARRAY[@]};i++));
do
    SAMPLE1=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${FIRST_FILE_NAME[i]}
    SAMPLE2=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${SECOND_FILE_NAME[i]}
    SORTED_BAM_1=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted.bam
    echo $SAMPLE1
    echo $SAMPLE2
    echo $SORTED_BAM_1
 
    time $SENTIEONBWA mem -M -R '@RG\tID:${ID_ARRAY[i]} \tSM:${LINE_ARRAY[i]} \tPL:ILLUMINA' -t $NUMBER_THREADS $REFERENCE $SAMPLE1 $SAMPLE2 | $SENTIEON util sort -o $SORTED_BAM_1 -t $NUMBER_THREADS --sam2bam -i -

    SCORE_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_score.txt
    DEDUP_METRIC_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedupMetric.txt
    DEDUP_BAM=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedup.bam

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo LocusCollector --fun score_info $SCORE_TXT

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo Dedup --rmdup --score_info $SCORE_TXT --metrics $DEDUP_METRIC_TXT $DEDUP_BAM
done
