#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/M162W_HBEN2ADXX_GTCCGC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/M162W_HL5WNCCXX_L5.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/M162W_HFFGKADXX_GTCCGC.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o M162W_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant M162W_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o M162W_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V M162W_haplotype_caller_output.vcf \
   -o M162W_haplotype_caller_output.fasta

