**Minimap is Heng Li newest aligner**
**Testing it for aligning assemblies with reference genomes**

Using Minimap2
https://github.com/lh3/minimap2

**Installation**
````sbtshell
git clone https://github.com/lh3/minimap2
cd minimap2 && make
mkdir out
./minimap2 -ax map10k test/MT-human.fa test/MT-orang.fa > out/test.sam

````

Testing of W22 against itself
````sbtshell
cp "/Users/edbuckler/Box Sync/MaizePHG/W22_alignment/W22_chr10.fa.gz" /Users/edbuckler/temp/assembly
cp "/Users/edbuckler/Box Sync/MaizePHG/W22_alignment/W22_chr10_contig.fa" /Users/edbuckler/temp/assembly
cp "/Users/edbuckler/Box Sync/MaizePHG/W22_alignment/W22_chr10_asContigs.fa.gz" /Users/edbuckler/temp/assembly
cp "/Users/edbuckler/Box Sync/MaizePHG/W22_alignment/B73_chr10.fa.gz" /Users/edbuckler/temp/assembly
cd /Users/edbuckler/temp/assembly
#uncompress files
gunzip W22_chr10.fa.gz
gunzip W22_chr10_asContigs.fa.gz
gunzip B73_chr10.fa.gz
#create an index for w22, using the assembly with 10% divergence
/Users/edbuckler/temp/minimap2/minimap2 -x asm10 -d w22chr10.mmi W22_chr10.fa
/Users/edbuckler/temp/minimap2/minimap2 -x asm10 -d B73_chr10.fa.mmi B73_chr10.fa
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" faidx B73_chr10.fa
#align one contig
/Users/edbuckler/temp/minimap2/minimap2 -ax asm10 w22chr10.mmi W22_chr10_contig.fa > testAsm10.sam
#align all contig
/Users/edbuckler/temp/minimap2/minimap2 -ax asm10 w22chr10.mmi W22_chr10_asContigs.fa > w22w22Asm10.sam
#create paf format output
/Users/edbuckler/temp/minimap2/minimap2 -x asm10 w22chr10.mmi W22_chr10_contig.fa > testAsm10.paf


````

```sbtshell
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -E -uf W22_chr10.fa testAsm10.sam > testAsm10.pileup
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -E -uf W22_chr10.fa testAsm10.sam > testAsm10.pileup
#not tried 
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" view -cg testAsm10.pileup > file.vcf

"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/sam2vcf" < testAsm10.pileup > out.vcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf W22_chr10.fa testAsm10.sam | "/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -vmO z -o study.vcf.gz
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf W22_chr10.fa testAsm10.sam > test1.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" convert -o study1.vcf test1.bcf

"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf W22_chr10.fa w22w22Asm10.sam > w22w22Asm10.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" convert -o w22w22Asm10.vcf w22w22Asm10.bcf
```

Preferred processing
```sbtshell
#Aligns using the existing index
/Users/edbuckler/temp/minimap2/minimap2 -ax asm10 w22chr10.mmi W22_chr10_asContigs.fa > w22w22Asm10.sam
#Create a bcf using mpileup
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf W22_chr10.fa w22w22Asm10.sam > w22w22Asm10.bcf
#convert bcf to vcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" convert -o w22w22Asm10.vcf w22w22Asm10.bcf
#convert bcf to gvcf
Fix bcf file for depth field
echo '##FORMAT=<ID=DP,Number=1,Type=Integer,Description=“Read Depth”>' > formatdp.txt
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" annotate -h formatdp.txt -o w22w22Asm10dp.bcf w22w22Asm10.bcf
Create gvcf - creates segmentation fault. Removing g.vcf works
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -m --ploidy 2 --gvcf 1 -o w22w22Asm10call.g.vcf w22w22Asm10dp.bcf

```

Testing with W22 Contigs versus B73
```sbtshell
#Aligns using the existing index
/Users/edbuckler/temp/minimap2/minimap2 -ax asm10 B73_chr10.fa.mmi W22_chr10_asContigs.fa > w22B73Asm10.sam
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" sort w22B73Asm10.sam w22B73Asm10.sorted.bam
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" index w22B73Asm10.sorted.bam

"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" view  -b -o w22B73Asm10.1_2M.sorted.bam -L region1_2M.bed w22B73Asm10.sorted.bam

#Create a bcf using mpileup
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf B73_chr10.fa w22B73Asm10.sam > w22B73Asm10.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf B73_chr10.fa w22B73Asm10.sorted.bam > w22B73Asm10.sorted.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" annotate -h formatdp.txt -o w22B73Asm10.dp.sorted.bcf w22B73Asm10.sorted.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -m --ploidy 1 -o w22B73Asm10call.sorted.vcf w22B73Asm10.sorted.bcf
0
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -m --ploidy 1 --gvcf 1 -o w22B73Asm10.dp.sorted.g.vcf w22B73Asm10.dp.sorted.bcf

"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup ­g ­f B73_chr10.fa w22B73Asm10.sorted.bam | bcftools call ­c ­ > A_J.vcf


"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf B73_chr10.fa w22B73Asm10.1_2M.sorted.bam > w22B73Asm10.1_2M.sorted.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -f B73_chr10.fa w22B73Asm10.1_2M.sorted.bam > w22B73Asm10.1_2M.sorted.vcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf B73_chr10.fa w22B73Asm10.1_2M.sorted.bam > w22B73Asm10.1_2M.sorted.vcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -m --ploidy 1 -o w22B73Asm10.1_2M.sorted.vcf w22B73Asm10.1_2M.sorted.bcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" index w22B73Asm10.1_2M.sorted.bam
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup ­m ­f B73_chr10.fa w22B73Asm10.1_2M.sorted.bam | "/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call ­c ­ > A_J.vcf

```

This is the current best approach to calling SNPs.  There are still regions with depth of two.  Two alignments?  
Need to check on what the minimap is doing on multiple alignment
```sbtshell
Reduce the bam to only the region
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" view  -b -o w22B73Asm10.1_2M.sorted.bam -L region1_2M.bed w22B73Asm10.sorted.bam
index bam
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" index w22B73Asm10.1_2M.sorted.bam
use the t option to export DP
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bin/samtools" mpileup -ugf B73_chr10.fa w22B73Asm10.1_2M.sorted.bam -t DP > w22B73Asm10.1_2M.sorted.bcf
call to gvcf
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools" call -m --ploidy 1 -g 0 -o w22B73Asm10.1_2M.sorted.vcf w22B73Asm10.1_2M.sorted.bcf

```



Installing bcftools
````sbtshell
Make sure Xz installed - lzma
brew install xz


````

On Sept 4 - tried looking through some of the remaining errors:
This regions fail the smell test

[Position	Chr:10	Pos:1493141	Name:S10_1493141	MAF:NaN	Ref:N..Position	Chr:10	Pos:1497522	Name:S10_1497522	MAF:NaN	Ref:N]
data=FILE:/Users/edbuckler/temp/minimap2Alignment/w22B73Asm10.1_2M.sorted.bam;index=FILE:/Users/edbuckler/temp/minimap2Alignment/w22B73Asm10.1_2M.sorted.bam.bai
1492889 length:266
targetRefPosition:	1493141	-> assemblyStart	1541795
ASSEM:CCACTCCTTGCCAAGATGAGCCTCGCCCTTTGCCTTCTTATAGTTCTTCCT
Genome FASTA character conversion: ACGTNacgtn to ACGTNacgtn
REF  :GACGGCGATGAGATAGATGTTGTTGATTACTTTTTTTTTTCTTTTCAATTG

Note there is no sequence similarity here.  If you look at the alignment there is massive hard clipping and only a tiny alignment.
It says on three mismatches, but I think there are many more.  

chr10:1541543:1632824
From: 1,492,889 U1,492,889 to 1,493,154 U1,493,154
Length: 266 U266 (3 mismatches)
Cigar: 66514H266M24502H
Read direction is FORWARD

>chr10:1541543:1632824
GAGCCGACGGAGCTGCGCGCGCTTGGCGGGGTGGACACCCGTGCCGCTGA
CGAGGTCGGAGACCGTTGCTGCGCCGCCGCGGCGGTGGATGGCGCCGGGG
ATGCCTAGTCCAACCGCGCACAGCAGCGCCGACGACTTCACGTGGTGAAG
GCCGTGGTGGTACAGCTCCACGTATGTTTGGTGGCGATGGTGGTCGTTGT
GGAGAGGCGGAGCGTTAGGCTCTTGCTGCTCTGCATGGGCACAAGGCGAC
ATGACGGCGATGAGAT
