package net.maizegenetics.pangenome.pipeline

import htsjdk.samtools.*
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

/**
 * Class to read a SAM file, then calculate and  print metrics on number of matched-BPs, unmatched-BPs(SNPs),
 * total-aligned bps, insertions, deletions, editDistance, and the total query length. THe total refLength
 * is also included per user request, but this information is not terribly useful.  While the query sequence
 * is included in the SAM file, allowing for the readLength to be determined by a seq.length call, the
 * reference sequence is not.  The refLen is inferred from the CIGAR string:  add all X,EQ,M, D values.
 * This means the refLen is always a total of the TotalBP-Aligned + Total_Deletions.
 *
 * Entries are filtered to include only primary alignments.
 *
 * Output:  A tab-delimited file with columns for each data point, and rows are the queryName/RNAME pair.
 *
 * @author lcj34
 */
class SAMAlignmentMetricsPlugin  (parentFrame: Frame?=null, isInteractive: Boolean=false) : AbstractPlugin(parentFrame, isInteractive){

    private val myLogger = LogManager.getLogger(SAMAlignmentMetricsPlugin::class.java)

    data class ReadNameData(val queryName:String, val refName:String, val totalEQ:Int, val totalSNP:Int,val totalM:Int,
                            val totalInserts:Int, val totalDeletes:Int, val editDistance:Int, val totalReadLength:Int,
                            val totalRefLength:Int)

    private var samFile = PluginParameter.Builder("samFile", null, String::class.java)
        .guiName("SAM/BAM  to process")
        .inFile()
        .required(true)
        .description("Name of the SAM/BAM file to process.")
        .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
        .guiName(" Output Metrics File.")
        .outFile()
        .required(true)
        .description("File where metrics will be written.")
        .build()


    override fun processData(input: DataSet?): DataSet? {

        // htsjdk ValidationStringency:
        //    "lenient" - emit warnings but keep going if possible
        //    "silent" - same as "lenient", but don't post warnings
        //    "strict" - throw an exception if something looks wrong
        val samRecordIterator: SAMRecordIterator = SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT)
            .open(File(samFile())).iterator()

        // Keep a list of data for each read
        val readDataList = mutableListOf<ReadNameData>()
        // This gives statistics on a per-QNAME/RNAME pair
        val readSet = mutableSetOf<String>()
        while (samRecordIterator.hasNext()) {

            val record = samRecordIterator!!.next()
            if (record.isSecondaryOrSupplementary) continue // only process primary records
            var totalSNP = 0
            var totalEQ = 0
            var totalInserts = 0
            var totalDeletes = 0
            var totalRefLen = 0
            var totalM = 0

            // Add both Query and Ref Names:
            var currentQueryName = record.readName
            var currentRefName = record.referenceName
            readSet.add(record.readName)

            // CigarElement: One component of a cigar string. The component comprises the operator,
            // and the number of bases to which the operator applies
            val cigarElements = record.cigar.cigarElements
            for (ce in cigarElements) {
                val cigarOp = ce.operator
                if (cigarOp == CigarOperator.X) {
                    totalSNP += ce.length
                    totalRefLen += ce.length
                } else if (cigarOp == CigarOperator.I) {
                    totalInserts += ce.length
                } else if (cigarOp == CigarOperator.D) {
                    totalDeletes += ce.length
                    totalRefLen += ce.length
                } else if (cigarOp == CigarOperator.EQ) {
                    totalEQ += ce.length
                    totalRefLen += ce.length
                } else if (cigarOp == CigarOperator.M) {
                    // This code initially written for Bethany, who I knew
                    // was using --eqx in her minimap2 run.  If this code
                    // is to be generic, it will need to factor in M vs EQ/X
                    totalM += ce.length
                    totalRefLen += ce.length
                } else if (cigarOp == CigarOperator.N) {
                    // https://davetang.org/wiki/tiki-index.php?page=SAM
                    // SKipped region from the reference
                    totalRefLen += ce.length
                }
            }

            // update stats for this query name (readName)
            val readLen = record.readLength
            totalM += totalSNP + totalEQ // whould have either M (totalled above) or EQ/X

            //println("queryRef=${queryPlusRef}: queryLen=${readLen} refLen=${totalRefLen}")

            // get editDistance
            var editDistance = record.getIntegerAttribute("NM")
            if (editDistance == null) editDistance = 0

            val data = ReadNameData(currentQueryName,currentRefName,totalEQ, totalSNP, totalM, totalInserts, totalDeletes,
                        editDistance, readLen, totalRefLen)
            readDataList.add(data)
        }

        myLogger.info("Number of Reads in SAM file: ${readSet.size} ")
        myLogger.info("Writing SNPS ...")
        File(outputFile()).bufferedWriter().use { writer ->

            writer.write("QueryName\tRefName\tNumBP-Match\tNumBP-SNP\tTotalBP-Aligned\tTotal_Insertions\tTotal_Deletions\tTotal_EditDistance\tTotalQueryLength_allReads\tTotalRefLen_allReads\n")
            for (row in readDataList) {
                val data = "${row.queryName}\t${row.refName}\t${row.totalEQ}\t${row.totalSNP}\t${row.totalM}\t${row.totalInserts}\t${row.totalDeletes}\t${row.editDistance}\t${row.totalReadLength}\t${row.totalRefLength}\n"
                writer.write(data)
            }

        }
        myLogger.info("\nMetrics written to file ${outputFile()}")

        return null
    }

    fun getReadCountFromSam(samFile: String) {
        SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT)
            .open(File(samFile)).use { reader ->
                val readSet = reader.asSequence().map { it.readName }.toSet()
                myLogger.info("\nNumber of Reads in SAM file: ${readSet.size}")
            }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = SAMAlignmentMetricsPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("SAM File Metrics plugin")
    }

    override fun getToolTipText(): String {
        return("Run this plugin to obtain basic metrics on SAM file data")
    }
    /**
     * Name of the SAM/BAM file to process.
     *
     * @return SAM/BAM  to process
     */
    fun samFile(): String {
        return samFile.value()
    }

    /**
     * Set SAM/BAM  to process. Name of the SAM/BAM file to
     * process.
     *
     * @param value SAM/BAM  to process
     *
     * @return this plugin
     */
    fun samFile(value: String): SAMAlignmentMetricsPlugin {
        samFile = PluginParameter<String>(samFile, value)
        return this
    }

    /**
     * File where metrics will be written.
     *
     * @return  Directory to write out statistics files.
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set  Directory to write out statistics files.. File
     * where metrics will be written.
     *
     * @param value  Directory to write out statistics files.
     *
     * @return this plugin
     */
    fun outputFile(value: String): SAMAlignmentMetricsPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(SAMAlignmentMetricsPlugin::class.java)
}