package net.maizegenetics.pangenome.hapCalling

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.ParameterCache
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.junit.jupiter.api.Test
import java.io.File
import net.maizegenetics.junit.SmallSeqExtension
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(SmallSeqExtension::class)
class CreateHapIdMapsPluginIT {

    @Test
    fun testJSONEncoding() {
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .includeVariantContexts(false)
            .includeSequences(false)
            .methods("CONSENSUS")
            .configFile(SmallSeqExtension.configFilePath)
            .performFunction(null)

        ParameterCache.load(SmallSeqExtension.configFilePath)

        val outputFile = "${SmallSeqExtension.baseDir}/consensus_json.txt"

        CreateHapIdMapsPlugin().outputMapFile(outputFile).performFunction(graph)

        // Load the file back in and compare the maps
        val mapper = jacksonObjectMapper()
        val maps = mapper.readValue(File(outputFile), GraphIdMaps::class.java)

        val fileHapIdToRefRangeMap = unwrapRefRanges(maps.hapIdToRefRangeMap)
        val fileHapIdToLengthMap = maps.hapIdToLengthMap
        val fileRefRangeToHapIdMap = maps.refRangeToHapIdMap
        val fileHaplotypeListId = maps.haplotypeListId
        val fileHapIdToTaxaList = maps.hapIdToTaxaList

        var dbConnect = DBLoadingUtils.connection(false)
        val phg = PHGdbAccess(dbConnect)
        val graphObject = graph.getData(0).data as HaplotypeGraph
        val originalHapIdToRefRangeMap = getHapToRefRangeMap(graphObject)
        val originalHapIdToLengthMap = getHapIdToSequenceLength(graphObject)
        val originalRefRangeToHapIdMap = getRefRangeToHapidMap(graphObject)
        val originalHaplotypeListId = getHaplotypeListIdForGraph(graphObject, phg)
        val originalHapIdToTaxaListMap = graphObject.referenceRanges()
            .flatMap { graphObject.nodes(it) }
            .map { Pair(it.id(), it.taxaList().map { taxon -> taxon.name }) }
            .toMap()

        assertEquals(originalHapIdToRefRangeMap.size, fileHapIdToRefRangeMap.size, "HapIdToRefRangeMap Different Sizes:")
        assertEquals(originalHapIdToLengthMap.size, fileHapIdToLengthMap.size, "HapIdToLengthMap Different Sizes:")
        assertEquals(originalRefRangeToHapIdMap.size, fileRefRangeToHapIdMap.size, "RefRangeToHapIdMap Different Sizes:")
        assertEquals(originalHapIdToTaxaListMap.size, fileHapIdToTaxaList.size, "HapIdToTaxaListMap Different Sizes:")
        assertEquals(originalHaplotypeListId, fileHaplotypeListId, "HaplotypeListId does not match:")

        // Make sure the HapIds -> Reference Range matches
        for (key in originalHapIdToRefRangeMap.keys) {
            val originalRefRange: ReferenceRange = originalHapIdToRefRangeMap[key]!!

            assertTrue(fileHapIdToRefRangeMap.containsKey(key), "Original Key Not found in file for HapIdToRefRangeMap")
            assertEquals(originalRefRange, fileHapIdToRefRangeMap[key], "Reference Range does not match:${key}")
        }

        for (key in originalHapIdToLengthMap.keys) {
            assertTrue(fileHapIdToLengthMap.containsKey(key), "Original Key Not found in file for HapIdToLengthMap")
            assertEquals(originalHapIdToLengthMap[key], fileHapIdToLengthMap[key], "Haplotype Lengths do not match:${key}")
        }

        for (refRangeKey in originalRefRangeToHapIdMap.keys) {
            assertTrue(fileRefRangeToHapIdMap.containsKey(refRangeKey), "Original Ref Range Key Not found in file for RefRangeToHapIdMap")

            for (hapIdKey in originalRefRangeToHapIdMap[refRangeKey]!!.keys) {
                assertTrue(fileRefRangeToHapIdMap[refRangeKey]!!.containsKey(hapIdKey), "Original hapId Key Not found in file for RefRangeToHapIdMap, RefRangeKey:${refRangeKey}")
                assertEquals(originalRefRangeToHapIdMap[refRangeKey]!![hapIdKey], fileRefRangeToHapIdMap[refRangeKey]!![hapIdKey], "HapIdValues do not match, refRangeKey:${refRangeKey}, hapIdKey:${hapIdKey}")
            }
        }

        for (hapIdKey in originalHapIdToTaxaListMap.keys) {
            assertTrue(fileHapIdToTaxaList.containsKey(hapIdKey), "Original HapIdKey Not found in file for HapIdToTaxaListMap")
            assertEquals(originalHapIdToTaxaListMap[hapIdKey]!!.size, fileHapIdToTaxaList[hapIdKey]!!.size, "TaxaList sizes do not match:")

            val originalTaxaNames = originalHapIdToTaxaListMap[hapIdKey]!!.toSet()

            for (taxaName in fileHapIdToTaxaList[hapIdKey]!!){
                assertTrue(originalTaxaNames.contains(taxaName), "Taxa: ${taxaName} not found in original List for HapId ${hapIdKey}:")
            }
        }
        phg.close()
    }
}