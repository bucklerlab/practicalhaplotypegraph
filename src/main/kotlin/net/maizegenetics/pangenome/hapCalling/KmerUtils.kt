@file:JvmName("KmerUtils")

package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.Arrays
import it.unimi.dsi.fastutil.Swapper
import it.unimi.dsi.fastutil.ints.IntComparator
import it.unimi.dsi.fastutil.longs.Long2IntMap
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2ObjectMap
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap
import net.maizegenetics.dna.BaseEncoder
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.io.FileInputStream

private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.hapCalling.KmerUtils")

/**
 * Function to first check that the kmer lengths match, then encode the Long from the sequence
 */
fun dnaToKmerLong(kmerAsDNA: String, kmerSize:Int): Long {
    if (kmerAsDNA.length != kmerSize) throw IllegalArgumentException("Kmer must equal initial length")
    return BaseEncoder.getLongFromSeq(kmerAsDNA)
}

/**
 * Function to check the kmer lengths an then encode the Long from the ByteSeq
 */
fun byteArrayToKmerLong(kmerAsByte: ByteArray, kmerSize: Int): Long {
    if (kmerAsByte.size != kmerSize) throw IllegalArgumentException("Kmer must equal initial length")
    return BaseEncoder.getLongSeqFromByteArray(kmerAsByte)
}

/**
 * Function to write out the Long2ObjectMap<IntArray> to a text file.  It is in the format:
 * kmerAsLong\tHapId1\tHapId2\tHapId3.....
 */
fun exportKmerToHapIdMapToTextFile(fileName : String, kmerMap : Long2ObjectMap<IntArray>) {
    Utils.getBufferedWriter(fileName)
            .use{out -> kmerMap.forEach {
                kmer, hapIdArray -> out.write("$kmer\t${hapIdArray.joinToString("\t")}\n")
            }}
}

/**
 * Import the KmerToHapIdMap from a text file.  This will read from a file with the format:
 * kmerAsLong\tHapId1\tHapId2\tHapId3.....
 */
fun importKmerToHapIdMapFromTextFile(fileName : String) : Long2ObjectMap<IntArray> {
    val kmerToHapIdMap: Long2ObjectMap<IntArray> = Long2ObjectOpenHashMap(500_000_000, 0.9f)
    Utils.getBufferedReader(fileName)
            .forEachLine{val parsedLine = parseKmerLine(it); kmerToHapIdMap[parsedLine.first] = parsedLine.second}
    return kmerToHapIdMap
}

/**
 * Function to export the kmer to Id map
 */
fun exportKmerToIntMap (fileName: String, kmerMap: Long2IntMap) {
    Utils.getBufferedWriter(fileName)
            .use { out -> kmerMap.forEach {
                kmer,dist -> out.write("$kmer\t$dist\n")
            } }
}

fun importKmerToIntMap(fileName: String) : Long2IntMap {
    val kmerToIntMap:Long2IntMap = Long2IntOpenHashMap(500_000_000,0.9f)
    Utils.getBufferedReader(fileName)
            .forEachLine { val parsedLine = it.split("\t");
            kmerToIntMap.put(parsedLine[0].toLong(),parsedLine[1].toInt())}

    return kmerToIntMap
}


fun exportPurgeArray(fileName: String, purgeArray: KmerToRefRangeIdToPurgeArray ) {
    val writer = Utils.getBufferedWriter(fileName)
    val kmerArray = purgeArray.kmerArray
    val refRangeIdArray = purgeArray.refRangeIdArray
    val markPurgeArray = purgeArray.purgeArray
    for( i in 0 until purgeArray.kmerArray.size) {
        writer.write("${kmerArray[i]}\t${refRangeIdArray[i]}\t${markPurgeArray[i]}\n")
    }
    writer.close()

}


/**
 * Function to parse the Kmer line.  This just splits the currentLine and then will convert the characters to integers and then add them to an array
 */
fun parseKmerLine(currentString : String) : Pair<Long,IntArray> {
    val currentLineSplit = currentString.split(delimiters = *charArrayOf('\t'))
    val hapIds = currentLineSplit.subList(1,currentLineSplit.size)
            .map{it.toInt()}
            .toIntArray()
    return Pair(currentLineSplit[0].toLong(), hapIds)
}

fun exportKmerToHapIdMapToBinaryFile(fileName: String, kmerMap: Long2ObjectMap<IntArray>) {
    //Need to write each entry as kmer | numberOfHapIds | hapId1 | hapId2 | ....
    Utils.getDataOutputStream(fileName,1024)
            .use { out ->
                out.writeInt(kmerMap.size)
                kmerMap.forEach {
                    out.writeLong(it.key)
                    out.writeInt(it.value.size)
                    it.value.forEach { hapId ->
                        out.writeInt(hapId)
                    }
                }

            }
}

fun importKmerToHapIdMapFromBinaryFile(fileName: String) : Long2ObjectMap<IntArray> {
    val kmerToHapIdMap: Long2ObjectMap<IntArray> = Long2ObjectOpenHashMap(500_000_000, 0.9f)
    DataInputStream(BufferedInputStream(FileInputStream(fileName)))
            .use { input ->
                val numberOfKmers = input.readInt()
                for(i in 0 until numberOfKmers) {
                    val kmer = input.readLong()
                    val numHapIds = input.readInt()
                    val hapIdArray = IntArray(numHapIds)
                    for (index in 0 until numHapIds) {
                        hapIdArray[index] = input.readInt()
                    }
                    kmerToHapIdMap[kmer] = hapIdArray
                }
            }

    return kmerToHapIdMap
}


/**
 * Method that will mark Kmers to purge using HammingDistance
 *
 * Inputs are the KmerMap where it maps kmer -> RefRangeId
 *
 * It will then export the Data Class KmerToRefRangeIdToPurgeArray which holds 3 primitive arrays
 * We create a swapper then do the hamming calculation twice one for the first half of the kmer one time for the second half
 */
fun markKmersForPurgeUsingHammingDistance(kmerToRefRange : KmerMap, kmerSize: Int, minAllowedHammingDist : Int, verboseLogging:Boolean = false) : KmerToRefRangeIdToPurgeArray {
    val kmerArray = kmerToRefRange.kmersAsLongArray()
    val refRangeIdArray = IntArray(kmerArray.size)
    val purgeArray = ByteArray(kmerArray.size)
    for(i in 0 until kmerArray.size) {
        refRangeIdArray[i] = kmerToRefRange[kmerArray[i]]
    }

    //We need a single swapper and a comparator for the mask
    //Build a swapper
    val kmerRefRangePurgeSwapper = Swapper { a, b ->
        val t1 : Long
        val t2 : Int
        val t3 : Byte

        t1 = kmerArray[a]
        kmerArray[a] = kmerArray[b]
        kmerArray[b] = t1

        t2 = refRangeIdArray[a]
        refRangeIdArray[a] = refRangeIdArray[b]
        refRangeIdArray[b] = t2

        t3 = purgeArray[a]
        purgeArray[a] = purgeArray[b]
        purgeArray[b] = t3
    }

    //We need to check the kmer size to see if it is odd.
    //If it is odd, we have to make the shift amount even so we do not split an allele's bits in half.
    val leftShiftAmount = 64  - if(kmerSize % 2 !=0 ) kmerSize - 1 else kmerSize
    //To get the low mask, shift 1 left by the shift amount and subtract 1 to turn all the right bits as 1s
    //Invert to get the high mask.
    val lowMask = (1L shl leftShiftAmount) - 1
    val highMask = lowMask.inv()

    //Compute kmer hamming distances sorted by first half of the Kmer
    compareKmerLoop(kmerArray = kmerArray, refRangeIdArray = refRangeIdArray,
            purgeArray = purgeArray, minAllowedHammingDist = minAllowedHammingDist, mask = highMask,
            kmerRefRangePurgeSwapper = kmerRefRangePurgeSwapper)

    //If verbose Logging is turned on, print out a message about what is being processed.  This will be very large with a lot of kmers.
    if(verboseLogging) {
        for (i in 0 until kmerArray.size) {
            val kmerString = BaseEncoder.getSequenceFromLong(kmerArray[i], 32.toByte())
            myLogger.info("${kmerArray[i]} ${kmerArray[i] and highMask} ${kmerArray[i] and lowMask} ${refRangeIdArray[i]} ${purgeArray[i]} ${kmerString.substring(0, 16)} ${kmerString.substring(16, kmerString.length)}")
        }

        myLogger.info("Number To Purge after first iteration: ${purgeArray.sum()}")
    }


    //Compute kmer hamming distances sorted by last half Of the kmer
    compareKmerLoop(kmerArray = kmerArray, refRangeIdArray = refRangeIdArray,
            purgeArray = purgeArray, minAllowedHammingDist = minAllowedHammingDist, mask = lowMask,
            kmerRefRangePurgeSwapper = kmerRefRangePurgeSwapper)

    //If verbose Logging is turned on, print out a message about what is being processed.  This will be very large with a lot of kmers.
    if(verboseLogging) {
        for (i in 0 until kmerArray.size) {
            val kmerString = BaseEncoder.getSequenceFromLong(kmerArray[i], 32.toByte())
            myLogger.info("${kmerArray[i]} ${kmerArray[i] and highMask} ${kmerArray[i] and lowMask} ${refRangeIdArray[i]} ${purgeArray[i]} ${kmerString.substring(0, 16)} ${kmerString.substring(16, kmerString.length)}")
        }
        myLogger.info("Number To Purge after second iteration: ${purgeArray.sum()}")
    }
    return KmerToRefRangeIdToPurgeArray(kmerArray,refRangeIdArray,purgeArray)
}


/**
 * Function to compute paired bit hamming distances for encoded kmers.
 */
fun hammingDistance(kmer1 : Long, kmer2 : Long) : Int {
    val xorLong = kmer1 xor kmer2
    val differences = (xorLong and 0x5555555555555555L) or ((xorLong ushr 1) and 0x5555555555555555L)
    return java.lang.Long.bitCount(differences)
}

/**
 * Function critical to run the hamming distance comparison.
 *
 * This will first create a Comparable and then sort the the arrays by the masked comparison
 * Then loop through each kmer
 *    Loop through the kmers that match the masked version and compare the hamming distance
 *    If the ref Range ids are not the same and they have a hamming distance below what is input, mark the arrays to be purged
 */
fun compareKmerLoop(kmerArray :LongArray, refRangeIdArray: IntArray, purgeArray: ByteArray, mask: Long, minAllowedHammingDist: Int, kmerRefRangePurgeSwapper : Swapper ) {

    //Build a Comparable for the current Mask
    val maskComparator = object : IntComparator {
        override fun compare(a: Int, b: Int): Int {
            //Pull off the mask the kmers and compare the masked values
            val aMasked = kmerArray[a] and mask
            val bMasked = kmerArray[b] and mask
            return if (aMasked === bMasked) 0 else if (aMasked < bMasked) -1 else 1
        }
    }

    //sort the arrays based on the Current Mask's comparator
    Arrays.quickSort(0,kmerArray.size,maskComparator, kmerRefRangePurgeSwapper)

    //Loop through all the kmers
    for (i in 0 until kmerArray.size) {
        //Mask the kmer
        val kmer1Masked = kmerArray[i] and mask

        //Loop through each of the kmers starting at i
        for (j in i until kmerArray.size) {
            //If they are the same index, we do not need to process.
            if (i == j) continue

            val kmer2Masked = kmerArray[j] and mask
            //Make sure that we are still processing the same masked kmer
            if(kmer1Masked != kmer2Masked) break  //you left the close diagonal
            //Check to make sure the hamming comparison is only done across different ref ranges
            //removed refRangeIdArray[i] >=0 && from this if statement as it creates inconsistent behavior
            if(refRangeIdArray[i]!=refRangeIdArray[j] &&
                    hammingDistance(kmerArray[i],kmerArray[j])<minAllowedHammingDist) {
                purgeArray[i]=1
                purgeArray[j]=1
            }
        }
    }
}

/**
 * Simple data class to hold the three arrays.
 */
data class KmerToRefRangeIdToPurgeArray(val kmerArray : LongArray, val refRangeIdArray: IntArray, val purgeArray: ByteArray)