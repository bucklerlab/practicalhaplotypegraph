package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * Plugin to take mapped reads to the pangenome in a SAM file and prepare and upload the ReadMappings to the DB.
 *
 * This plugin uses a Keyfile containing the following columns: cultivar, flowcell_lane, filename, and PlateID
 * It will then walk through each entry in the keyfile and will attempt to prepare the SAM file to create ReadMappings
 *
 * A number of filtering steps are done to improve the quality of the information stored in the db.
 *
 * Only equally optimal mappings(By Edit Distance(NM) are kept)
 *
 * Additional filtering is done to remove reads which are unmapped or clipped
 *
 * If running in paired mode, the reads must be on opposite strands and both must currently hit the same haplotype in a single reference range.
 *
 * Optimal mappings across reference ranges are processed, but reads are only assigned to haplotypes in the reference
 * range with the most hits(providing that at least 1- maxRefRangeError percentage hit that refRange)
 * Based on testing, this seems to be working the best to balance #of reads used with accuracy.
 */

class SAMToMappingPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(SAMToMappingPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, filename, and PlateID.  Optionally for paired end reads, filename2 is needed.  " +
                    "If filename2 is not supplied, Minimap2 will run in single end mode.  Otherwise will be paired.")
            .build()

    private var samDir = PluginParameter.Builder("samDir", null, String::class.java)
            .guiName("SAM/BAM dir to process")
            .inDir()
            .required(true)
            .description("Name of the SAM/BAM dir to process.")
            .build()


    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    private var lowMemMode = PluginParameter.Builder("lowMemMode", true, Boolean::class.javaObjectType)
            .required(false)
            .description("Run in low memory mode.")
            .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name to be stored in the DB.")
            .build()

    private var methodDescription = PluginParameter.Builder("methodDescription", null, String::class.java)
            .guiName("Method Description")
            .required(false)
            .description("Method description to be stored in the DB.")
            .build()

    private var outputDebugDir = PluginParameter.Builder("debugDir", "", String::class.java)
            .guiName("Debug Directory to write out read Mapping files.")
            .outDir()
            .required(false)
            .description("Directory to write out the read mapping files.  This is optional for debug purposes.")
            .build()

    private var outputSecondaryMappingStats = PluginParameter.Builder("outputSecondaryStats",false,Boolean::class.javaObjectType)
            .guiName("Output secondary mapping statistics.")
            .required(false)
            .description("Ouptput Secondary Mapping Statistics such as total AS for each haplotype ID")
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    private var updateDB = PluginParameter.Builder("updateDB", true, Boolean::class.javaObjectType)
        .description("If set to true, the read mappings will be written to the db.  Otherwise nothing will be written.")
        .required(false)
        .build()

    private var runWithoutGraph = PluginParameter.Builder("runWithoutGraph", false, Boolean::class.javaObjectType)
        .description("If set to true, will require the input of a JSON file created by CreateHapIdMapsPlugin and will got require a Graph object input.")
        .required(false)
        .build()

    private var hapIdMapFile = PluginParameter.Builder("hapIdMapFile", null, String::class.java)
        .description("Location of the HapIdMapFile where the graph Information can be found.")
        .required(false)
        .inFile()
        .dependentOnParameter(runWithoutGraph,true)
        .build()

    private var inputFile = PluginParameter.Builder("inputSAMFile", "", String::class.java)
        .description("Input SAM file to be run through.  If this is not set it will run everything in the inputDir")
        .required(false)
        .inFile()
        .build()

    override fun processData(input: DataSet?): DataSet? {

        if(runWithoutGraph()) {
            runGraphlessSamToMapping()
        }
        else {
            //Load in the graph to check reference Ranges
            val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("SAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
            if (temp.size != 1) {
                throw IllegalArgumentException("SAMToMappingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
            }
            val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph

            runGraphSamToMapping(graph)
        }
        return null

    }

    /**
     * This function will run the SAM -> ReadMapping File portion of the pipeline without needing DB access or a graph input.
     */
    fun runGraphlessSamToMapping() {
        //Load in the JSON file
        val maps = deserializeGraphIdMaps(hapIdMapFile())
        val hapIdToRefRangeMap = unwrapRefRanges(maps.hapIdToRefRangeMap)
        val hapIdToLengthMap = maps.hapIdToLengthMap
        val refRangeToHapIdMap = maps.refRangeToHapIdMap
        val haplotypeListId = maps.haplotypeListId

        runMinimapFromKeyFile(
            minimapLocation = "",
            keyFileName = keyFile(),
            inputFileDir = samDir(),
            referenceFile = "",
            graph = null,
            maxRefRangeError = maxRefRangeError(),
            methodName = methodName(),
            methodDescription = methodDescription(),
            pluginParams = pluginParameters(),
            outputDebugReadMappingDir = outputDebugDir(),
            outputSecondaryMappingStats = outputSecondaryMappingStats(),
            maxSecondary = 0,
            inputFileFormat = ReadMappingInputFileFormat.SAM,
            isTestMethod = isTestMethod(),
            updateDB = updateDB(),
            runWithoutGraph = runWithoutGraph(),
            hapIdToRefRangeMap = hapIdToRefRangeMap,
            hapIdToLengthMap = hapIdToLengthMap,
            refRangeToHapIdMap = refRangeToHapIdMap,
            inputFileName = inputFile()
        )
    }
    /**
     * This function will run the SAM -> ReadMapping File portion of the pipeline and will write to the DB.
     */
    fun runGraphSamToMapping(graph : HaplotypeGraph) {
        //create maps needed for BitSet encoding
        val hapIdToRefRangeMap = getHapToRefRangeMap(graph)
        val hapIdToLengthMap = getHapIdToSequenceLength(graph)
        val refRangeToHapIdMap = getRefRangeToHapidMap(graph)

        runMinimapFromKeyFile(
            minimapLocation = "",
            keyFileName = keyFile(),
            inputFileDir = samDir(),
            referenceFile = "",
            graph = graph,
            maxRefRangeError = maxRefRangeError(),
            methodName = methodName(),
            methodDescription = methodDescription(),
            pluginParams = pluginParameters(),
            outputDebugReadMappingDir = outputDebugDir(),
            outputSecondaryMappingStats = outputSecondaryMappingStats(),
            maxSecondary = 0,
            inputFileFormat = ReadMappingInputFileFormat.SAM,
            isTestMethod = isTestMethod(),
            updateDB = updateDB(),
            runWithoutGraph = runWithoutGraph(),
            hapIdToRefRangeMap = hapIdToRefRangeMap,
            hapIdToLengthMap = hapIdToLengthMap,
            refRangeToHapIdMap = refRangeToHapIdMap,
            inputFileName = inputFile()
        )
    }



    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "SAMToHapCountMinimapPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Convert a SAM file into ReadMappings"
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will run in single end mode.
     *  Otherwise will be paired.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will run in single end mode.  Otherwise will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): SAMToMappingPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Name of the SAM/BAM dir to process.
     *
     * @return SAM/BAM dir to process
     */
    fun samDir(): String {
        return samDir.value()
    }

    /**
     * Set SAM/BAM dir to process. Name of the SAM/BAM dir
     * to process.
     *
     * @param value SAM/BAM dir to process
     *
     * @return this plugin
     */
    fun samDir(value: String): SAMToMappingPlugin {
        samDir = PluginParameter<String>(samDir, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError(): Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double): SAMToMappingPlugin {
        maxRefRangeError = PluginParameter<Double>(maxRefRangeError, value)
        return this
    }

    /**
     * Run in low memory mode.
     *
     * @return Low Mem Mode
     */
    fun lowMemMode(): Boolean {
        return lowMemMode.value()
    }

    /**
     * Set Low Mem Mode. Run in low memory mode.
     *
     * @param value Low Mem Mode
     *
     * @return this plugin
     */
    fun lowMemMode(value: Boolean): SAMToMappingPlugin {
        lowMemMode = PluginParameter<Boolean>(lowMemMode, value)
        return this
    }

    /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): SAMToMappingPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    fun methodDescription(): String {
        return methodDescription.value()
    }

    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value: String): SAMToMappingPlugin {
        methodDescription = PluginParameter<String>(methodDescription, value)
        return this
    }

    /**
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @return Debug Directory to write out read Mapping files.
     */
    fun outputDebugDir(): String {
        return outputDebugDir.value()
    }

    /**
     * Set Debug Directory to write out read Mapping files..
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @param value Debug Directory to write out read Mapping files.
     *
     * @return this plugin
     */
    fun outputDebugDir(value: String): SAMToMappingPlugin {
        outputDebugDir = PluginParameter<String>(outputDebugDir, value)
        return this
    }

    /**
     * Ouptput Secondary Mapping Statistics such as total
     * AS for each haplotype ID
     *
     * @return Output secondary mapping statistics.
     */
    fun outputSecondaryMappingStats(): Boolean {
        return outputSecondaryMappingStats.value()
    }

    /**
     * Set Output secondary mapping statistics.. Ouptput Secondary
     * Mapping Statistics such as total AS for each haplotype
     * ID
     *
     * @param value Output secondary mapping statistics.
     *
     * @return this plugin
     */
    fun outputSecondaryMappingStats(value: Boolean): SAMToMappingPlugin {
        outputSecondaryMappingStats = PluginParameter<Boolean>(outputSecondaryMappingStats, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): SAMToMappingPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }

    /**
     * If set to true, the read mappings will be written to
     * the db.  Otherwise nothing will be written.
     *
     * @return Update D B
     */
    fun updateDB(): Boolean {
        return updateDB.value()
    }

    /**
     * Set Update D B. If set to true, the read mappings will
     * be written to the db.  Otherwise nothing will be written.
     *
     * @param value Update D B
     *
     * @return this plugin
     */
    fun updateDB(value: Boolean): SAMToMappingPlugin {
        updateDB = PluginParameter<Boolean>(updateDB, value)
        return this
    }

    /**
     * If set to true, will require the input of a JSON file
     * created by CreateHapIdMapsPlugin and will got require
     * a Graph object input.
     *
     * @return Run Without Graph
     */
    fun runWithoutGraph(): Boolean {
        return runWithoutGraph.value()
    }

    /**
     * Set Run Without Graph. If set to true, will require
     * the input of a JSON file created by CreateHapIdMapsPlugin
     * and will got require a Graph object input.
     *
     * @param value Run Without Graph
     *
     * @return this plugin
     */
    fun runWithoutGraph(value: Boolean): SAMToMappingPlugin {
        runWithoutGraph = PluginParameter<Boolean>(runWithoutGraph, value)
        return this
    }

    /**
     * Location of the HapIdMapFile where the graph Information
     * can be found.
     *
     * @return Hap Id Map File
     */
    fun hapIdMapFile(): String {
        return hapIdMapFile.value()
    }

    /**
     * Set Hap Id Map File. Location of the HapIdMapFile where
     * the graph Information can be found.
     *
     * @param value Hap Id Map File
     *
     * @return this plugin
     */
    fun hapIdMapFile(value: String): SAMToMappingPlugin {
        hapIdMapFile = PluginParameter<String>(hapIdMapFile, value)
        return this
    }
    /**
     * Input SAM file to be run through.  If this is not set
     * it will run everything in the inputDir
     *
     * @return Input S A M File
     */
    fun inputFile(): String {
        return inputFile.value()
    }

    /**
     * Set Input S A M File. Input SAM file to be run through.
     *  If this is not set it will run everything in the inputDir
     *
     * @param value Input S A M File
     *
     * @return this plugin
     */
    fun inputFile(value: String): SAMToMappingPlugin {
        inputFile = PluginParameter<String>(inputFile, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(SAMToMappingPlugin::class.java)
}