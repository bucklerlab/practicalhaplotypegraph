package net.maizegenetics.pangenome.pipeline

import junit.framework.Assert.assertEquals
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.LogManager
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Class to test the pipeline/PipelineUtils.kt:runLiquibaseCommand() function
 *
 * This class creates a PHG db that does not have the change sets stored for liquibase.
 * It then creates a new PHG docker, and runs 3 iterations of  LiquibaseCommandPlugin.
 *  - status, update, status
 *
 * LiquibaseCommandPlugin was created specifically to test the pipeline/PipelineUtils:runLiquibaseCommand()
 * method.  We have all this wrapper code as runLiquibaseCommand() needs to be called by a plugin that has
 * configuration values stored in a parameter cache.
 *
 * In addtion, in order to perform liquibase commands, currently a docker is involved for storing the
 * change logs and jdbc drivers needed by liquibase.
 *
 */

class PipelineUtilsTest {
    private val myLogger = LogManager.getLogger(PipelineUtilsTest::class.java)
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testParseChangesetVersion() {
        // This tests that we correctly parse the liquibase status output
        // THe previous output file used was in /Users/lcj34/notes_files/phg_2018/peter_pipeline/test/
        val userHome = System.getProperty("user.home")
        val testDir = "$userHome/temp/phgLiquibaseTest/"
        try {
            if (Files.exists(Paths.get(testDir))) FileUtils.deleteDirectory(File(testDir))
            Files.createDirectories(Paths.get(testDir))
        } catch (ioe: IOException) {
            throw IllegalStateException("testParseChangesetVersion:setup: error deleting/creating folders: " + ioe.message)
        }

        var liquibaseOutputFile = testDir + "/liquibase_status_output.log"
        createStatusFile(liquibaseOutputFile)
        var linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

        val changeSets = linesInOutput
                .filter { it.contains("changesets")}
                .map { it -> parseChangesetVersion(it)}
                .toList()

        println("Changsets recovered:")
        changeSets.stream().forEach{println(it)}
        assertEquals(10, changeSets.size)

        // Now find the lowest number - these are already sorted, but let's be sure
        val phgVersion = findUserPHGVersion(changeSets)
        println("phgVersion: $phgVersion")
        assertEquals("0.0.10", phgVersion)

    }

    fun createStatusFile(liquibaseOutputFile:String) {
        // create a file with the liquibase status output
        // THis has both changesets that did NOT have a PHG version, and one that does.
        // The data will be used to verify that both
        try {
            Utils.getBufferedWriter(liquibaseOutputFile).use { bw ->
                bw.write("Starting Liquibase at Fri, 29 May 2020 14:07:18 UTC (version 3.7.0 built at 2019-07-16 02:26:39)\n")
                bw.write("10 change sets have not been applied to null@jdbc:sqlite:/tempFileDir/outputDir/goodLiquibaseDocker.db\n")
                bw.write("     /liquibase/changelogs/changesets/add_readMappingTables.sql::createReadMappingTable_sqlite::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/add_readMappingTables.sql::create_read_mapping_unique_idx::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/add_readMappingTables.sql::create_read_mapping_genoid_idx::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/alterPathsTable_forReadMapping.sql::updatepathstable_sqlite::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates.sql::update_readtablemappings_sqlite::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates2.sql::drop_read_mapping_read_mapping_group::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates2.sql::drop_read_mapping_group::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates2.sql::create_read_mapping_paths_sqlite::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates2.sql::update_paths_table_with_genoid_sqlite::lcj34\n")
                bw.write("     /liquibase/changelogs/changesets/readMappingTables_updates2.sql::updateHaplotypesTable_addAsmData_0.0.24::lcj34\n")
                bw.write("Liquibase command 'status' was executed successfully.")
            }

        } catch (exc:Exception) {
            throw IllegalStateException(" createGFFKeyfile: error writing test gff key file: " + exc.message)
        }
    }
    @Test
    fun testFindUserPHGVersion() {
        var list = mutableListOf<Triple<Int,Int,Int>>()
        list.add(Triple<Int,Int,Int>(0,2,2))
        list.add(Triple<Int,Int,Int>(0,2,18))
        list.add(Triple<Int,Int,Int>(0,0,2))
        list.add(Triple<Int,Int,Int>(0,0,13))
        list.add(Triple<Int,Int,Int>(1,2,1))

        val phgVersion = findUserPHGVersion(list)
        val expected = "0.0.2"
        assertEquals(expected, phgVersion)
        println("phgVersion: $phgVersion")
    }
}