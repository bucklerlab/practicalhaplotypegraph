package net.maizegenetics.pangenome.api;

import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.pangenome.db_loading.GZipCompression;

import java.lang.ref.WeakReference;

/**
 * @author Terry Casstevens Created June 19, 2017
 * @author Zack Miller
 * @author Karl Kremling
 * @author Jacob Washburn
 */
public class HaplotypeSequence {

    private final byte[] mySequence;
    private WeakReference<String> mySequenceStr = null;
    private final int mySequenceLength;
    private final ReferenceRange mySourceReferenceRange;
    private final double myQualityScore;
    private final String mySeqHash; // md5 / sha1 hash for this sequence

    private HaplotypeSequence(byte[] sequence, int sequenceLength, ReferenceRange referenceRange, double qualityScore, String seqHash) {
        mySequence = sequence;
        mySequenceLength = sequenceLength;
        mySourceReferenceRange = referenceRange;
        myQualityScore = qualityScore;
        mySeqHash = seqHash;
    }

    /**
     * Factory method to create HaplotypeSequence This allows a copy of the HaplotypeSequence to be returned instead of
     * the original mutable object
     *
     * @param sequence
     * @param referenceRange
     * @param qualityScore
     * @param seqHash
     *
     * @return
     */
    public static HaplotypeSequence getInstance(String sequence, ReferenceRange referenceRange, double qualityScore, String seqHash) {
        return new HaplotypeSequence(GZipCompression.compress(sequence), sequence == null ? 0 : sequence.length(), referenceRange, qualityScore, seqHash);
    }

    /**
     * Factory method to create HaplotypeSequence This allows a copy of the HaplotypeSequence to be returned instead of
     * the original mutable object
     *
     * Haplotype sequences are always gzip compressed, stored as byte array. This signature used when creating sequence
     * from DB's byte array
     *
     * @param sequence
     * @param referenceRange
     * @param qualityScore
     * @param seqHash
     * @param seqLen
     *
     * @return
     */
    public static HaplotypeSequence getInstance(byte[] sequence, ReferenceRange referenceRange, double qualityScore, String seqHash, int seqLen) {
        return new HaplotypeSequence(sequence, seqLen, referenceRange, qualityScore, seqHash);
    }

    /**
     * Reference Range in (alternative)reference genome
     */
    public ReferenceRange referenceRange() {
        return mySourceReferenceRange;
    }

    /**
     * Basis of this score and units to be determined
     */
    public double qualityScore() {
        return myQualityScore;
    }

    /**
     * Sequence hash in md5
     */
    public String seqHash() {
        return mySeqHash;
    }

    @Override
    public String toString() {
        return "HaplotypeSequence{" +
                "mySourceReferenceRange=" + mySourceReferenceRange +
                ", mySequenceLength=" + mySequenceLength +
                ", mySequence=" + sequence() +
                ", myQualityScore=" + myQualityScore +
                ", mySeqHash=" + mySeqHash +
                '}';
    }

    /**
     * Converts string sequence to compressed array of bytes.
     *
     * @param sequence sequence
     *
     * @return compressed array of bytes
     */
    private static byte[] haplotypeSeqenceToHalfByte(String sequence) {

        if (sequence == null || sequence.isEmpty()) {
            return new byte[0];
        }

        int seqLen = sequence.length();
        int resultLen = (seqLen / 2) + (seqLen % 2);
        byte[] result = new byte[resultLen];
        int count = 0;
        for (int i = 1; i < sequence.length(); i += 2) {
            byte first = NucleotideAlignmentConstants.getNucleotideAlleleByte(sequence.charAt(i - 1));
            byte second = NucleotideAlignmentConstants.getNucleotideAlleleByte(sequence.charAt(i));
            result[count++] = (byte) (first << 4 | second);
        }

        if (seqLen % 2 != 0) {
            byte first = NucleotideAlignmentConstants.getNucleotideAlleleByte(sequence.charAt(seqLen - 1));
            result[count] = (byte) (first << 4 | NucleotideAlignmentConstants.UNDEFINED_ALLELE);
        }

        return result;

    }

    /**
     * String representation of this nucleotide sequence
     *
     * @return sequence
     */
    public String sequence() {

        String result = (mySequenceStr == null) ? null : mySequenceStr.get();
        if (result == null) {
            // If string is not compressed, decompress returns the original string
            result = GZipCompression.decompress(mySequence);
            mySequenceStr = new WeakReference<>(result);
        }
        return result;

    }

    public int length() {
        return mySequenceLength;
    }
}
