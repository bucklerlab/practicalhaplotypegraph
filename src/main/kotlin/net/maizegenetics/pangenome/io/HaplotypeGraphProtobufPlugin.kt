package net.maizegenetics.pangenome.io

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * Export / Import the HaplotypeGraph to / from a Protobuf file.
 * If the input file is specified, the graph will be imported from the file.
 * If the output file is specified, the graph will be exported to the file.
 */
class HaplotypeGraphProtobufPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) :
    AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(HaplotypeGraphProtobufPlugin::class.java)

    private var myGraph: HaplotypeGraph? = null

    private var myInputFile = PluginParameter.Builder("inputFile", null, String::class.java)
        .description("Input Protobuf file name. If not specified, will use the output file name.")
        .guiName("Input Protobuf File Name")
        .required(false)
        .inFile()
        .build()

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
        .description("Output Protobuf file name. If not specified, will use the input file name.")
        .guiName("Output Protobuf File Name")
        .required(false)
        .outFile()
        .build()

    private var myIncludeSequences = PluginParameter.Builder("includeSequences", true, Boolean::class.javaObjectType)
        .description("Include sequences in the protobuf file if available. Applies to import and export")
        .required(false)
        .build()

    private var myIncludeEdges = PluginParameter.Builder("includeEdges", true, Boolean::class.javaObjectType)
        .description("Include edges in the protobuf file if available. Applies to import and export")
        .required(false)
        .build()

    private var myLocalGVCFFolder = PluginParameter.Builder("localGVCFFolder", null, String::class.java)
        .description("Folder where gvcfs are stored.  This is required for VariantContexts.")
        .guiName("Local GVCF Folder")
        .inDir()
        .required(false)
        .build()

    override fun preProcessParameters(input: DataSet) {
        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size <= 1) { "HaplotypeGraphProtobufPlugin: preProcessParameters: must input at most one HaplotypeGraph" }
        if (temp.size == 1) myGraph = temp[0].data as HaplotypeGraph
    }

    override fun processData(input: DataSet?): DataSet? {

        if (myGraph != null) {
            require(myOutputFile.value() != null) { "HaplotypeGraphProtobufPlugin: processData: output file must be specified to export HaplotypeGraph" }
            myLogger.info("Writing graph to protobuf file: ${outputFile()}")
            toProtobuf(myGraph!!, outputFile(), includeSequences(), includeEdges(), localGVCFFolder())
            return input
        } else {
            require(myInputFile.value() != null) { "HaplotypeGraphProtobufPlugin: processData: input file must be specified to import HaplotypeGraph" }
            myLogger.info("Reading graph from protobuf file: ${inputFile()}")
            val result = toGraph(inputFile(), includeSequences(), includeEdges(), localGVCFFolder())
            return DataSet.getDataSet(result)
        }

    }

    /**
     * Input Protobuf file name. If not specified, will use
     * the output file name.
     *
     * @return Input Protobuf File Name
     */
    fun inputFile(): String {
        return myInputFile.value()
    }

    /**
     * Set Input Protobuf File Name. Input Protobuf file name.
     * If not specified, will use the output file name.
     *
     * @param value Input Protobuf File Name
     *
     * @return this plugin
     */
    fun inputFile(value: String): HaplotypeGraphProtobufPlugin {
        myInputFile = PluginParameter<String>(myInputFile, value)
        return this
    }

    /**
     * Output Protobuf file name. If not specified, will use
     * the input file name.
     *
     * @return Output Protobuf File Name
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output Protobuf File Name. Output Protobuf file
     * name. If not specified, will use the input file name.
     *
     * @param value Output Protobuf File Name
     *
     * @return this plugin
     */
    fun outputFile(value: String): HaplotypeGraphProtobufPlugin {
        myOutputFile = PluginParameter<String>(myOutputFile, value)
        return this
    }

    /**
     * Include sequences in the protobuf file if available.
     * Applies to import and export
     *
     * @return Include Sequences
     */
    fun includeSequences(): Boolean {
        return myIncludeSequences.value()
    }

    /**
     * Set Include Sequences. Include sequences in the protobuf
     * file if available. Applies to import and export
     *
     * @param value Include Sequences
     *
     * @return this plugin
     */
    fun includeSequences(value: Boolean): HaplotypeGraphProtobufPlugin {
        myIncludeSequences = PluginParameter<Boolean>(myIncludeSequences, value)
        return this
    }

    /**
     * Include edges in the protobuf file if available. Applies
     * to import and export
     *
     * @return Include Edges
     */
    fun includeEdges(): Boolean {
        return myIncludeEdges.value()
    }

    /**
     * Set Include Edges. Include edges in the protobuf file
     * if available. Applies to import and export
     *
     * @param value Include Edges
     *
     * @return this plugin
     */
    fun includeEdges(value: Boolean): HaplotypeGraphProtobufPlugin {
        myIncludeEdges = PluginParameter<Boolean>(myIncludeEdges, value)
        return this
    }

    /**
     * Folder where gvcfs are stored.  This is required for
     * VariantContexts.
     *
     * @return Local GVCF Folder
     */
    fun localGVCFFolder(): String? {
        return myLocalGVCFFolder.value()
    }

    /**
     * Set Local GVCF Folder. Folder where gvcfs are stored.
     *  This is required for VariantContexts.
     *
     * @param value Local GVCF Folder
     *
     * @return this plugin
     */
    fun localGVCFFolder(value: String): HaplotypeGraphProtobufPlugin {
        myLocalGVCFFolder = PluginParameter<String>(myLocalGVCFFolder, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "HaplotypeGraphProtobufPlugin"
    }

    override fun getToolTipText(): String {
        return "HaplotypeGraphProtobufPlugin"
    }

}