package net.maizegenetics.pangenome.api

import org.apache.logging.log4j.LogManager

/**
 * Class for holding static Kotlin methods to be used by the rPHG package.
 * The intention for this class is to:
 *   * house methods to be executed against a PHG graph object
 *   * house methods for returning data from BrAPI URLs:
 *     + URLs will be called from `BrapiCon` objects from the rPHG package
 *     + future optimization in data returns vs native R/HTTR package calls
 *   * additional helper methods that are not found in the API,
 *     but could be of use for rPHG returns.
 * @author Brandon Monier
 */
class RPHGMethodsKotlin {
    // Set up logging object ----
    private val myLogger = LogManager.getLogger(RPHGMethodsKotlin::class.java)

    /**
     * Data class for storing haplotype ID / sequence strings
     */
    data class HaplotypeSequences(val hapId: Int, val sequence: String)

    /**
     * Return sequence information from haplotype ID input
     * @param phgObj A PHG object
     * @param hapIds An array of haplotype ID values as integers
     * @return       A list of `HapSequences` data objects
     * @see          HaplotypeSequences
     */
    fun getHapIdSeq(phgObj: HaplotypeGraph, hapIds: IntArray): List<HaplotypeSequences> {
        myLogger.info("Running getHapIdSeq method...")
        val hapSeqs = mutableListOf<HaplotypeSequences>()

        val allHapIds = phgObj.nodeStream().map { it.id() }

        // Check each node in stream ----
        phgObj.nodeStream().forEach { node ->
            // If node ID from stream is found in array of give user IDs add to list object
            // as HapSequence object ----
            if (hapIds.contains(node.id())) {
                myLogger.info("Node found: ${node.id()}")
                hapSeqs.add(HaplotypeSequences(node.id(), node.haplotypeSequence().sequence()))
            }
        }

        return hapSeqs
    }
}





