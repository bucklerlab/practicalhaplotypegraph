package net.maizegenetics.pangenome.api

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.SimpleTableReport
import java.awt.Frame
import javax.swing.ImageIcon

class ReadMethodTablePlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .required(true)
            .description("Database configuration file")
            .inFile()
            .build()

    override fun processData(input: DataSet?): DataSet {
        val conn = DBLoadingUtils.connection(configFile.value(), false)
        val sqlstr = "select method_id, method_type, name, description from methods"
        val header = arrayOf<String>("method_id","method_type","type_name","method_name","description")
        val rowList = ArrayList<Array<Any?>>()

        val rsMethods = conn.createStatement().executeQuery(sqlstr)

        //map for converting method types to names
        val typeMap = HashMap<Int,String>()
        for (type in DBLoadingUtils.MethodType.values()) {
            typeMap.put(type.value, type.name)
        }

        while (rsMethods.next()) {
            val methodTypeId = rsMethods.getInt("method_type")
            val methodTypeName = typeMap.get(methodTypeId)
            rowList.add(arrayOf(rsMethods.getInt("method_id"),
                    methodTypeId,
                    methodTypeName,
                    rsMethods.getString("name"), rsMethods.getString("description")))
        }
        conn.close()

        val rows = Array(rowList.size) {i -> rowList[i]}
        val report = SimpleTableReport("Methods", header, rows)
        return DataSet.getDataSet(report);
    }

    override fun getToolTipText(): String {
        return "Get information from PHG DB methods table"
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "PHG methods"
    }

    fun configFile() : String {
        return configFile.value()
    }

    fun configFile(filename : String) : ReadMethodTablePlugin {
        configFile = PluginParameter(configFile, filename)
        return this
    }
}