#!/bin/bash -x

# This script will pull down the fasta from the Db based on what the user specifies in the method and will write out a fasta
# Once the fasta is created, it will run minimap2 indexing it to create the index file

#
# This script starts with fastq files and generates haplotype
# counts using a Haplotype Graph.  Those counts are then used
# with a HMM algorithm to find the best path through the
# haplotype graph for each taxon.  Those pathes are written
# to text files containing a list of the haplotype node ids.
#

MOUNT_DIR=/phg
OUTPUT_DIR=${MOUNT_DIR}/outputDir
DATA_DIR=${MOUNT_DIR}/data

PANGENOME_DIR=${MOUNT_DIR}/pangenome




if [ ! -d "$PANGENOME_DIR" ]; then
   mkdir $PANGENOME_DIR
fi

# Base name of the haplotype fasta (i.e. no .fa extension)
BASE_HAPLOTYPE_NAME=$1
# Filename of configFile for db connection - used in FastqToHapCountPlugin
CONFIGFILE=${MOUNT_DIR}/$2
CONFIGFILE_NAME=$2
# Consensus Method used to extract haplotypes in graph
HAPLOTYPE_METHOD=$3
NUM_BASES_LOADED=$4
MINIMIZER_SIZE=$5
WINDOW_SIZE=$6

# Haplotype Fasta file
HAPLOTYPE_FASTA="${PANGENOME_DIR}/${BASE_HAPLOTYPE_NAME}.fa"
# Haplotype Fasta Index file
HAPLOTYPE_INDEX="${PANGENOME_DIR}/${BASE_HAPLOTYPE_NAME}.mmi"

# Set maximum Java heap size if defined in config file
fullXmx=""
memoryString="`grep -m 1 '^Xmx=' ${CONFIGFILE} | cut -d'=' -f2`"
if [[ ${memoryString} != "" ]]
then
    fullXmx='-Xmx'${memoryString}
fi



#
# Check if Haplotype Fasta File exists and
# create if not.  The haplotype fasta file is
# generated from the haplotype graph.
#

if [[ -f ${HAPLOTYPE_FASTA} ]]; then
   echo "Haplotype Fasta File $HAPLOTYPE_FASTA exists."
else
   echo "Creating Haplotype Fasta File ${HAPLOTYPE_FASTA}."
   time /tassel-5-standalone/run_pipeline.pl $fullXmx -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIGFILE -methods $HAPLOTYPE_METHOD -includeVariantContexts false -endPlugin -WriteFastaFromGraphPlugin -outputFile $HAPLOTYPE_FASTA -endPlugin
fi

#
# Check if Haplotype Fasta Index File exists and
# create if not.  This is a bwa index needed by the
# next step in this pipeline.
#

if [[ -f ${HAPLOTYPE_INDEX} ]]; then
   echo "The Minimap2 Index File for ${HAPLOTYPE_FASTA} exists."
else
   echo "Creating Minimap2 Index for ${HAPLOTYPE_FASTA}."
   time /minimap2/minimap2 -d ${HAPLOTYPE_INDEX} -k ${MINIMIZER_SIZE} -I ${NUM_BASES_LOADED} -w ${WINDOW_SIZE} ${HAPLOTYPE_FASTA}
fi
