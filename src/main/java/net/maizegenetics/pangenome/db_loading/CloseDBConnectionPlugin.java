package net.maizegenetics.pangenome.db_loading;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.ImageIcon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;

/**
 * 
 */

/**
 * This method closes a DB connection.  The connection is expected as part of the
 * input DataSet parameter set.
 * 
 * @author lcj34
 *
 */
public class CloseDBConnectionPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(CloseDBConnectionPlugin.class);    
    
    public CloseDBConnectionPlugin() {
        super(null, false);
    }

    public CloseDBConnectionPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public CloseDBConnectionPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
    
    
    @Override
    public DataSet processData(DataSet input) {
        
        Connection dbConnect = (Connection)input.getData(0).getData();
        if (dbConnect == null) {
            myLogger.error("CloseDBConnection:  No DB CONNECTION supplied !");
            return null;
        }
        
        try {
            dbConnect.close();
        } catch (SQLException sqle) {
            myLogger.error("CloseDBConnection: error closing connection " + sqle.getMessage());           
        }
        return null;
    }
    
    public static void main(String[] args) {
        GeneratePluginCode.generate(CloseDBConnectionPlugin.class);        
    }
    
    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {       
        return ("Close DB Connection");
    }

    @Override
    public String getToolTipText() {
        return ("Close DB Connection");
    }

}
