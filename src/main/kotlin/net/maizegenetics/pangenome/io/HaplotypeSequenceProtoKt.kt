// Generated by the protocol buffer compiler. DO NOT EDIT!
// source: main/kotlin/net/maizegenetics/pangenome/io/haplotypegraph.proto

// Generated files should ignore deprecation warnings
@file:Suppress("DEPRECATION")
package net.maizegenetics.pangenome.io;

@kotlin.jvm.JvmName("-initializehaplotypeSequenceProto")
public inline fun haplotypeSequenceProto(block: net.maizegenetics.pangenome.io.HaplotypeSequenceProtoKt.Dsl.() -> kotlin.Unit): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto =
  net.maizegenetics.pangenome.io.HaplotypeSequenceProtoKt.Dsl._create(net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto.newBuilder()).apply { block() }._build()
/**
 * Protobuf type `net.maizegenetics.pangenome.io.HaplotypeSequenceProto`
 */
public object HaplotypeSequenceProtoKt {
  @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
  @com.google.protobuf.kotlin.ProtoDslMarker
  public class Dsl private constructor(
    private val _builder: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto.Builder
  ) {
    public companion object {
      @kotlin.jvm.JvmSynthetic
      @kotlin.PublishedApi
      internal fun _create(builder: net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto.Builder): Dsl = Dsl(builder)
    }

    @kotlin.jvm.JvmSynthetic
    @kotlin.PublishedApi
    internal fun _build(): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto = _builder.build()

    /**
     * `int32 length = 1;`
     */
    public var length: kotlin.Int
      @JvmName("getLength")
      get() = _builder.getLength()
      @JvmName("setLength")
      set(value) {
        _builder.setLength(value)
      }
    /**
     * `int32 length = 1;`
     */
    public fun clearLength() {
      _builder.clearLength()
    }

    /**
     * `int32 referenceRangeID = 2;`
     */
    public var referenceRangeID: kotlin.Int
      @JvmName("getReferenceRangeID")
      get() = _builder.getReferenceRangeID()
      @JvmName("setReferenceRangeID")
      set(value) {
        _builder.setReferenceRangeID(value)
      }
    /**
     * `int32 referenceRangeID = 2;`
     */
    public fun clearReferenceRangeID() {
      _builder.clearReferenceRangeID()
    }

    /**
     * `double qualityScore = 3;`
     */
    public var qualityScore: kotlin.Double
      @JvmName("getQualityScore")
      get() = _builder.getQualityScore()
      @JvmName("setQualityScore")
      set(value) {
        _builder.setQualityScore(value)
      }
    /**
     * `double qualityScore = 3;`
     */
    public fun clearQualityScore() {
      _builder.clearQualityScore()
    }

    /**
     * `string seqHash = 4;`
     */
    public var seqHash: kotlin.String
      @JvmName("getSeqHash")
      get() = _builder.getSeqHash()
      @JvmName("setSeqHash")
      set(value) {
        _builder.setSeqHash(value)
      }
    /**
     * `string seqHash = 4;`
     */
    public fun clearSeqHash() {
      _builder.clearSeqHash()
    }
  }
}
@kotlin.jvm.JvmSynthetic
@com.google.errorprone.annotations.CheckReturnValue
public inline fun net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto.copy(block: net.maizegenetics.pangenome.io.HaplotypeSequenceProtoKt.Dsl.() -> kotlin.Unit): net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeSequenceProto =
  net.maizegenetics.pangenome.io.HaplotypeSequenceProtoKt.Dsl._create(this.toBuilder()).apply { block() }._build()

