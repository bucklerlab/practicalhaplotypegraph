package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.*
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.util.*
import javax.swing.ImageIcon

/**
 * This plugin queries the PHG genome_file_data table and
 * returns a tab-delimited file with data for the specified
 * file types.
 *
 * input parameters:
 *   type:  indicates for which type of file, FASTA or GVCF, the user would like data returned.
 *          If this parameter is not present, or has a value other than FASTA or GVCF (case insensitive) data
 *          for all genome_file_data tables will be returned.
 *
 *   configFile:  file containing database connection information
 *
 *
 * Output Parameters:
 *   outputFile: full path to file where the tab-delimited output with columns "type" "GenomePath" "GenomeFile" "Checksum"
 *               will be written.
 *
 * @author lcj34
 */

class RetrieveGenomeFileDataPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(MAFToGVCFPlugin::class.java)

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
        .description("Config file with database connection information ")
        .required(true)
        .inFile()
        .build()

    private var type = PluginParameter.Builder("type", null, String::class.java)
        .description("Return information for only this type of file: options are GVCF or FASTA.  If this parameter is not present all data from the genome_file_data table will be returned")
        .required(false)
        .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
        .description("Path and name of file where tab-delimited output will be written")
        .required(true)
        .outFile()
        .build()

    override fun processData(input: DataSet?): DataSet? {

        // get db connection from configFile
        val connection = DBLoadingUtils.connection(configFile(), false)

        val querySB = StringBuilder()
        querySB.append("SELECT genome_path, genome_file, file_checksum, type from genome_file_data ")
        // If no type or invalid type is supplied, we grab all entries
        if (type() != null) {
            if (type()!!.uppercase(Locale.getDefault()) == DBLoadingUtils.GenomeFileType.GVCF.name) {
                querySB.append(" where type=${DBLoadingUtils.GenomeFileType.GVCF.value}")
            } else if (type()!!.uppercase(Locale.getDefault()) == DBLoadingUtils.GenomeFileType.FASTA.name) {
                querySB.append(" where type=${DBLoadingUtils.GenomeFileType.FASTA.value}")
            } else {
                myLogger.warn("invalid type parameter: ${type()}.  Value must be either GVCF or FASTA. Program will return all values from the genome_file_data table")
            }
        }

        val genomeData = mutableListOf<String>()
        try {

            var rs = connection.createStatement().executeQuery(querySB.toString())
            while (rs.next()) {
                var genomePath = rs.getString("genome_path") // haplotypes_id
                var genomeFile = rs.getString("genome_file") // ref_range_id
                var checkSum = rs.getString("file_checksum")
                var type = rs.getInt("type")

                DBLoadingUtils.GenomeFileType.GVCF.value
                // The columns are in the order of "type genomePath genomeFile checksum"
                var typeString = DBLoadingUtils.GenomeFileType.GVCF.name
                if (type == DBLoadingUtils.GenomeFileType.FASTA.value) {
                    typeString = DBLoadingUtils.GenomeFileType.FASTA.name
                }
                var fileData = StringBuilder()
                fileData.append(typeString).append("\t").append(genomePath).append("\t").append(genomeFile).append("\t").append(checkSum).append("\n")
                genomeData.add(fileData.toString())

            }

            myLogger.info("finished querying database, writing output to file ${outputFile()}")

            // Write the results
            val headerLine = "type\tGenomePath\tGenomeFile\tchecksum\n"
            File(outputFile()).bufferedWriter().use { writer ->

                writer.write(headerLine)
                for (line in genomeData) {
                    writer.write(line)
                }
            }
            myLogger.info("Genome data info written to file ${outputFile()}")

        } catch ( exc:Exception) {
            throw  IllegalStateException("AssemblyConsensusMetricPlugin: processData: Problem querying the database: ",exc);
        }
        return null

    }

    override fun getIcon(): ImageIcon ?{
        return(null)
    }

    override fun getButtonName(): String {
        return("Retrieve Gnome File Data")
    }

    override fun getToolTipText(): String {
        return("This plugin queries the specified database for information on the location of stored fasta and/or gvcf files used in the database creation.")
    }

    /**
     * Config file with database connection information
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Config file with database connection
     * information
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): RetrieveGenomeFileDataPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Return information for only this type of file: options
     * are GVCF or FASTA.  If this parameter is not present
     * all data will be returned
     *
     * @return Type
     */
    fun type(): String?{
        return type.value()
    }

    /**
     * Set Type. Return information for only this type of
     * file: options are GVCF or FASTA.  If this parameter
     * is not present all data will be returned
     *
     * @param value Type
     *
     * @return this plugin
     */
    fun type(value: String?): RetrieveGenomeFileDataPlugin {
        type = PluginParameter<String>(type, value)
        return this
    }

    /**
     * Path and name of file where tab-delimited output will
     * be written
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. Path and name of file where tab-delimited
     * output will be written
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): RetrieveGenomeFileDataPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

}
fun main() {
    GeneratePluginCode.generateKotlin(RetrieveGenomeFileDataPlugin::class.java)
}