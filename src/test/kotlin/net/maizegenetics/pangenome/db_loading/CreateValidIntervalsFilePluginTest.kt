package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.*
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class CreateValidIntervalsFilePluginTest {

    val userHome = System.getProperty("user.home")
    val outputDir = "$userHome/temp/CreateValidIntervalsFileData/"

    val refFile = outputDir + "testRefGenome.fa"
    val bed3colJustGenesNoMethod =  "${outputDir}bed3colJustGenesNoMethod.bed"
    val bed4colJustGenesWithMethod = "${outputDir}bed4colJustGenesWithMethod.bed"
    val bed3colAllIntervals = "${outputDir}bed3ColAllIntervals.bed"
    val bed4colAllIntervalsWithmethod = "${outputDir}bed4ColAllIntervalsWithMethod.bed"
    val bed4colOverlapDiffMethod = "${outputDir}bed4ColOverlapDiffMethod.bed"

    val bed3colJustGenesWithInteranchor = "${outputDir}bed3ColJustGenesWithInterAnchor.bed"
    val bed3colAllIntervalsWithInterAnchor = "${outputDir}bed3ColAllIntervalsWithInterAnchor.bed"

    var myRefSequence: GenomeSequence? = null

    @Before
    @Throws(Exception::class)
    fun setup() {
        LoggingUtils.setupDebugLogging()

        // Reset temp files
        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(File(outputDir))
            Files.createDirectories(Paths.get(outputDir))
        } catch (ioe: IOException) {
            throw IllegalStateException("GFFTests:setup: error deleting/creating folders: " + ioe.message)
        }
        createTestRefFasta(refFile)
        createBedFiles()
    }

    @Test
    fun testCreateValidIntervalsFilePluginTest() {
        LoggingUtils.setupDebugLogging()

        // Test the full plugin call, let group names default in plugin
        CreateValidIntervalsFilePlugin(null, false)
                .generatedFile(bed3colJustGenesWithInteranchor)
                .referenceFasta(refFile)
                .intervalsFile(bed3colJustGenesNoMethod)
                .mergeOverlaps(true)
                .performFunction(null)

        try {
            val bedLines = Utils.getBufferedReader(bed3colJustGenesWithInteranchor).readLines()
            assertEquals(23,bedLines.size)
            assertEquals("1\t0\t85\tFocusComplement",bedLines[1])
            assertEquals("2\t2000\t2240\tFocusComplement",bedLines[22])
        } catch (exc:Exception) {
            throw IllegalStateException("Error reading file ${bed3colJustGenesWithInteranchor}: ${exc.message}")
        }
    }

    @Test
    fun testVerifyAndMergeIntervalRanges() {
        val verifyAndMergeClass = CreateValidIntervalsFilePlugin(null,false)
        var rangesAndOverlaps = verifyAndMergeClass.verifyAndMergeIntervalRanges(bed3colJustGenesNoMethod)

        assertEquals(2,rangesAndOverlaps.getY().size) // there should be 2 overlaps
        // There are 12 entries in the bed file, 2 are overlaps which should have been merged,
        // hence 10 entries
        assertEquals(10,rangesAndOverlaps.getX().asMapOfRanges().entries.size)

        rangesAndOverlaps = verifyAndMergeClass.verifyAndMergeIntervalRanges(bed3colAllIntervals)
        assertEquals(0,rangesAndOverlaps.getY().size) // no overlaps this time
        assertEquals(21,rangesAndOverlaps.getX().asMapOfRanges().entries.size) // should have 20 entries

        // THe difference between this and bed3colJustGenesNoMethod is the bed file has a 4th column
        // that is a group name.  This test verifies that having a 4th column does not cause the
        // method to break
        rangesAndOverlaps = verifyAndMergeClass.verifyAndMergeIntervalRanges(bed4colJustGenesWithMethod)
        assertEquals(2,rangesAndOverlaps.getY().size) // there should be 2 overlaps
        // There are 12 entries in the bed file, 2 are overlaps which should have been merged, so 10
        assertEquals(10,rangesAndOverlaps.getX().asMapOfRanges().entries.size)

        // another test to verify method column isn't a problem
        rangesAndOverlaps = verifyAndMergeClass.verifyAndMergeIntervalRanges(bed4colAllIntervalsWithmethod)
        assertEquals(0,rangesAndOverlaps.getY().size) // no overlaps this time
        assertEquals(21,rangesAndOverlaps.getX().asMapOfRanges().entries.size) // should have 21 entries

        try {
            rangesAndOverlaps = verifyAndMergeClass.verifyAndMergeIntervalRanges(bed4colOverlapDiffMethod)
            fail("This test case should have failed  - overlaps have differing methods!!")
        } catch (exc:Exception) {
            // this should throw an exception - we have an overlap but the methods the overlapping
            // intervals are different
            println("test case with different methods on overlap passes!")
        }
    }

    @Test
    fun testCreateInterRegionRanges() {
        // Setting otherRegionsGroupName - must match what you have below for
        // val groupName = "focusComplement".  OTherwise, the writeIntervalsFile()
        // will not correctly adjust the ranges to be closed/closed.  When called from
        // inside the plugin, the writeIntervalsFile()  relies on the plugin parameter value.
        val createValidIntervals = CreateValidIntervalsFilePlugin(null,false)
        createValidIntervals.otherRegionsGroupName("focusComplement")
        // Test creating the inter-regions from a file that contains only genes.
        var rangesAndOverlaps = createValidIntervals.verifyAndMergeIntervalRanges(bed3colJustGenesNoMethod)
        var userRanges = rangesAndOverlaps.getX()
        val refInterRegionRanges: RangeSet<Position> = TreeRangeSet.create()

        // create the ref Genome - we need this to split the chromosomes
        val myRefSequence = GenomeSequenceBuilder.instance(refFile)

        // get interregions for testing
        var chromInterRegionRanges = createValidIntervals.createInterRegionRanges(userRanges, myRefSequence)

        assertEquals(12,chromInterRegionRanges.asRanges().size)
        // chr1/75 is a position below the first range in the bedfile.  Verify this was picked up in an interrange
        assertTrue(chromInterRegionRanges.contains(Position.of(Chromosome.instance("1"),75)))
        // chr2/2018 is a position after the last one in the bedfile.  Verify this was picked up as an inter-range
        assertTrue(chromInterRegionRanges.contains(Position.of(Chromosome.instance("2"),2018)))

        if (!chromInterRegionRanges.isEmpty()) {
            refInterRegionRanges.addAll(chromInterRegionRanges)
        }

        // Put the anchor and inter-anchor regions together
        var allRanges: RangeMap<Position,String> = TreeRangeMap.create()
        allRanges.putAll(userRanges)

        val groupName = "focusComplement"
        refInterRegionRanges.asRanges().forEach { range -> allRanges.put(range, groupName)}

        // Write output File - writeIntervalsFile() adjusts the bed ranges back to
        // inclusive/exclusive.  They were altered during RangeSet processing to prevent
        // collapsing of adjacent ranges.
        createValidIntervals.writeIntervalsFile(bed3colJustGenesWithInteranchor, allRanges)
        println("Intervals file written to ${bed3colJustGenesWithInteranchor}")

        // read the final file, check some values
        try {
            val bedLines = Utils.getBufferedReader(bed3colJustGenesWithInteranchor).readLines()
            assertEquals(23,bedLines.size)
            assertEquals("1\t0\t85\tfocusComplement",bedLines[1])
            assertEquals("2\t2000\t2240\tfocusComplement",bedLines[22])
        } catch (exc:Exception) {
            throw IllegalStateException("Error reading file ${bed3colJustGenesWithInteranchor}: ${exc.message}")
        }

    }

    @Test
    fun testCreateInterRegionRangesFromFullBedFile() {

        val createValidIntervals = CreateValidIntervalsFilePlugin(null,false)
        createValidIntervals.otherRegionsGroupName("focusComplement")
        // Test creating the inter-regions from a bed file that represents the full genome (not just genes)

        val refInterRegionRanges: RangeSet<Position> = TreeRangeSet.create()

        // create the ref Genome - we need this to split the chromosomes
        val myRefSequence = GenomeSequenceBuilder.instance(refFile)

        // Verify a bed file that already contains all the positions of the genome.
        // there should be no inter-region ranges created.
        var rangesAndOverlaps = createValidIntervals.verifyAndMergeIntervalRanges(bed3colAllIntervals)
        var userRanges = rangesAndOverlaps.getX()
        var chromInterRegionRanges = createValidIntervals.createInterRegionRanges(userRanges, myRefSequence)

        // More verification of refInterRegionRanges now
        // -check group name, check that certain ranges exist, perhaps read in the
        //  file with with all ranges and see if it matches.  Will need to have a group name
        // on all of them
        if (!chromInterRegionRanges.isEmpty()) {
            refInterRegionRanges.addAll(chromInterRegionRanges)
        }

        // we have refRegionRanges and refInterRegionRanges.  Merge the two, should be no overlaps
        // Using  Map vs Set as Set coalesces the ranges, giving us 1 big range.  I need them distinct.
        // These should be added in order.
        var allRanges: RangeMap<Position,String> = TreeRangeMap.create()
        allRanges.putAll(userRanges)

        val groupName = "focusComplement"
        refInterRegionRanges.asRanges().forEach { range -> allRanges.put(range, groupName)}

        // Write the file.  writeIntervalsFile() will adjust the ranges back to close/open.
        // They were changed to closed/closed for RangeSet processing by the previous functions
        createValidIntervals.writeIntervalsFile(bed3colAllIntervalsWithInterAnchor, allRanges)
        println("Intervals file written to ${bed3colAllIntervalsWithInterAnchor}")

        // Because the user specified ranges covering all positions, and the input file
        // had all ranges listed without a group name, they are all given the group name "FocusRegion"
        try {
            val bedLines = Utils.getBufferedReader(bed3colAllIntervalsWithInterAnchor).readLines()
            assertEquals(22,bedLines.size)
            assertEquals("1\t0\t85\tFocusRegion",bedLines[1])
            assertEquals("2\t2000\t2240\tFocusRegion",bedLines[21])
        } catch (exc:Exception) {
            throw IllegalStateException("Error reading file ${bed3colJustGenesWithInteranchor}: ${exc.message}")
        }

    }


    // create a series of bed files to be used for testing
    // THis bedfile will contain some overlapping ranges.  The values available
    // for chrom1 are 0-4959 and chrom2 are 0-2439
    fun createBedFiles() {
        // The test ref fasta has a single chromosome and sequence length of 4959 basepairs

        // Create bedfiles: These could all be in the same try/catch block, but are
        // separated to more easily tell which one failed if there is a failure
        //   bed3colJustGenesNoMethod
        try {
            Utils.getBufferedWriter(bed3colJustGenesNoMethod).use { bw ->
                bw.write("chr1\t85\t250\n")
                bw.write("chr1\t350\t500\n")
                bw.write("chr1\t800\t920\n")
                bw.write("chr1\t750\t850\n") // overlaps the previous range
                bw.write("chr1\t1000\t1250\n")
                bw.write("chr1\t1050\t1200\n") // embedded in the previous range
                bw.write("chr1\t2000\t2999\n")
                bw.write("chr1\t3500\t4000\n") // last genic range
                bw.write("chr2\t120\t200\n")
                bw.write("chr2\t220\t500\n")
                bw.write("chr2\t501\t1020\n") // no interrange between genes
                bw.write("chr2\t1050\t2000\n")
            }

        } catch (exc:Exception) {
            throw IllegalStateException("error creating bed3colJustGenesNoMethod: ${exc.message}")
        }

        //   bed4colJustGenesWithMethod
        try {
            Utils.getBufferedWriter(bed4colJustGenesWithMethod).use { bw ->
                bw.write("chr1\t85\t250\tgenic\n")
                bw.write("chr1\t350\t500\tgenic\n")
                bw.write("chr1\t800\t920\tgenic\n")
                bw.write("chr1\t750\t850\tgenic\n") // overlaps the previous range
                bw.write("chr1\t1000\t1250\tgenic\n")
                bw.write("chr1\t1050\t1200\tgenic\n") // embedded in the previous range
                bw.write("chr1\t2000\t2999\tgenic\n")
                bw.write("chr1\t3500\t4000\tgenic\n") // last genic range
                bw.write("chr2\t120\t200\tgenic\n")
                bw.write("chr2\t220\t500\tgenic\n")
                bw.write("chr2\t500\t1020\tgenic\n") // no interrange between this and gene above
                bw.write("chr2\t1050\t2000\tgenic\n")
            }
        } catch (exc:Exception) {
            throw IllegalStateException("error creating bed4colJustGenesWithMethod: ${exc.message}")
        }

        //   bed3colAllIntervals
        try {
            Utils.getBufferedWriter(bed3colAllIntervals).use { bw ->
                bw.write("chr1\t0\t85\n")
                bw.write("chr1\t85\t250\n")
                bw.write("chr1\t250\t350\n")
                bw.write("chr1\t350\t500\n")
                bw.write("chr1\t500\t750\n")
                bw.write("chr1\t750\t920\n")
                bw.write("chr1\t920\t1000\n")
                bw.write("chr1\t1000\t1250\n")
                bw.write("chr1\t1250\t2000\n")
                bw.write("chr1\t2000\t2999\n")
                bw.write("chr1\t2999\t3500\n")
                bw.write("chr1\t3500\t4000\n")
                bw.write("chr1\t4000\t4960\n") // will this one fail ???
                bw.write("chr2\t0\t120\n")
                bw.write("chr2\t120\t200\n")
                bw.write("chr2\t200\t220\n")
                bw.write("chr2\t220\t500\n")
                bw.write("chr2\t500\t1020\n")
                bw.write("chr2\t1020\t1050\n")
                bw.write("chr2\t1050\t2000\n")
                bw.write("chr2\t2000\t2240\n")
            }

        } catch (exc:Exception) {
            throw IllegalStateException("error creating bed3colAllIntervals: ${exc.message}")
        }

        //   bed4colAllIntervalsWithmethod
        try {
            Utils.getBufferedWriter(bed4colAllIntervalsWithmethod).use { bw ->
                bw.write("chr1\t0\t85\tintergenic\n")
                bw.write("chr1\t85\t250\tgenic\n")
                bw.write("chr1\t250\t350\tintergenic\n")
                bw.write("chr1\t350\t500\tgenic\n")
                bw.write("chr1\t500\t750\tgenic\n")
                bw.write("chr1\t750\t920\tgenic\n")
                bw.write("chr1\t920\t1000\tintergenic\n")
                bw.write("chr1\t1000\t1250\tgenic\n")
                bw.write("chr1\t1250\t2000\tintergenic\n")
                bw.write("chr1\t2000\t2999\tgenic\n")
                bw.write("chr1\t2999\t3500\tintergenic\n")
                bw.write("chr1\t3500\t4000\tgenic\n")
                bw.write("chr1\t4000\t4960\tintergenic\n") // will this one fail ???
                bw.write("chr2\t0\t120\tintergenic\n")
                bw.write("chr2\t120\t200\tgenic\n")
                bw.write("chr2\t200\t220\tintergenic\n")
                bw.write("chr2\t220\t500\tgenic\n")
                bw.write("chr2\t500\t1020\tgenic\n")
                bw.write("chr2\t1020\t1050\tintergenic\n")
                bw.write("chr2\t1050\t2000\tgenic\n")
                bw.write("chr2\t2000\t2240\tintergenic\n")
            }

        } catch (exc:Exception) {
            throw IllegalStateException("error creating bed4colAllIntervalsWithmethod: ${exc.message}")
        }

        try {
            Utils.getBufferedWriter(bed4colOverlapDiffMethod).use { bw ->
                bw.write("chr1\t85\t250\tgenic\n")
                bw.write("chr1\t350\t500\tgenic\n")
                bw.write("chr1\t800\t920\tgenic\n")
                bw.write("chr1\t750\t850\tgenicdiffmethod\n") // overlaps the previous range
                bw.write("chr1\t1000\t1250\tgenic\n")
                bw.write("chr1\t1050\t1200\tgenic\n") // embedded in the previous range
                bw.write("chr1\t2000\t2999\tgenic\n")
                bw.write("chr1\t3500\t4000\tgenic\n") // last genic range
                bw.write("chr2\t120\t200\tgenic\n")
                bw.write("chr2\t220\t500\tgenic\n")
                bw.write("chr2\t500\t1020\tgenic\n") // no interrange between this and gene above
                bw.write("chr2\t1050\t2000\tgenic\n")
            }
        } catch (exc:Exception) {
            throw IllegalStateException("error creating bed4colJustGenesWithMethod: ${exc.message}")
        }

    }
}

// The reference file is needed when creating the inter-region ranges - we need to know the
// size of the chromosomes
fun createTestRefFasta(refFasta: String?) {
    try {
        Utils.getBufferedWriter(refFasta).use { bw ->
            // Create the file we'll use for verifying the fasta
            bw.write(">chr1\n")
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n")
            bw.write("GTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAAACCATTTCTTGCTTAAAAACTCGTACGTGTTAGCCTTCG\n")
            bw.write("GTATTATTGAAAATGGTCATTCATGGCTATTTTTCGGCAAAATGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTC\n")
            bw.write("ATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAGAAA\n")
            bw.write("TCCGTAGGTGTTAGCCTTCGATATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCAT\n")
            bw.write("TGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGA\n")
            bw.write("GACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCATTCATGGCTATTTTCGACAAA\n")
            bw.write("AATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCAC\n")
            bw.write("ATTCTTGGATTTATGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCAT\n")
            bw.write("TCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCATAGGCTCATACACCTCACCCCACATAT\n")
            bw.write("GTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTCTTAGCCTCTA\n")
            bw.write("ATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTC\n")
            bw.write("ATAAATCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGACCATTTCTTGGTCAAAAA\n")
            bw.write("CTCGTACGTGTTAGCCTTTGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGGCAAAATGGGGGTTGTGTGGCCATT\n")
            bw.write("GATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTATCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAA\n")
            bw.write("ACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAA\n")
            bw.write("TGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACAT\n")
            bw.write("TCTTGAATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGAGTTAGCCTTCGATATTATTGAAAATGGTCGTTC\n")
            bw.write("ATGGCCATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGT\n")
            bw.write("TTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGT\n")
            bw.write("ATTATTGTAAATGGTCATTCATGGCTATTTTCGACAACAATGTGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCAT\n")
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATA\n")
            bw.write("CGTAGGTCTTAGCCTCTAATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTT\n")
            bw.write("ATCATTGACTAGAGGCTCATAAATCTCACCCCTCATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGA\n")
            bw.write("CCATTTCTTGGTCAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAAT\n")
            bw.write("GGGGGTTCTGTGGCCATTGATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATT\n")
            bw.write("CTTGGATTTCTGGTGGAGACCATTTCTTGGGCAAAAATCCGAATGTGTTAGCCTTTGGTATTTTTGAAAATGGTCATTCA\n")
            bw.write("TGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCTTCGACCAGAGGCTCATACACCTCACTACACATATGTT\n")
            bw.write("TCCTTGCCATAGATCACATTCTTGGATTTATGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCAGTG\n")
            bw.write("TCATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAAATTAGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCAT\n")
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATC\n")
            bw.write("CGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTG\n")
            bw.write("ATCATCGACCAGACGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTTGTGGAGA\n")
            bw.write("CCATTTCTTGGTCTAAAATCCGTAGGTGTTAGCCTCTAGTATTATTGAAAATGGTCGCTCATGGCTATTTTCAAGGTCGC\n")
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n")
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATC\n")
            bw.write("CGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTG\n")
            bw.write("ATCATCGACCAGACGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTTGTGGAGA\n")
            bw.write("CCATTTCTTGGTCTAAAATCCGTAGGTGTTAGCCTCTAGTATTATTGAAAATGGTCGCTCATGGCTATTTTCAAGGTCGC\n")
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n")
            bw.write("GTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCG\n")
            bw.write("GTATTATTGAAAATGGTCGTTCATGGCTATTTTCGGCAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCA\n")
            bw.write("TAAACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAAT\n")
            bw.write("CCGTACGTGTTTGCCTTTGGTATTTTTGAAAATGTTTATTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATT\n")
            bw.write("GATCTTCGACCAGAGGCTCATACACCTCACTACACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAG\n")
            bw.write("ACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCAGTGTCATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAA\n")
            bw.write("ATTAGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACA\n")
            bw.write("TTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTT\n")
            bw.write("CATGGCTATTTTCGACAAAAATGAGGGTTGTGTGGCCATTGATCATCGACCAGACGCTCATACACCTCACCCCACATATG\n")
            bw.write("TTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGTTCTAAAATCCGTAGGTGTTATCCTCTAG\n")
            bw.write("TATTATTGAAAATGGTCGTTCATGGCTATTTTGAAGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCA\n")
            bw.write("TTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATATGTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGG\n")
            bw.write("AAACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTTCGGCA\n")
            bw.write("AAATGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCA\n")
            bw.write("CATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGATATTATTGAAAATGGTCG\n")
            bw.write("TTCATGGCTATTTTCGGCAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATA\n")
            bw.write("TGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTC\n")
            bw.write("GGTATTATTGTAAATGGTCATTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCT\n")
            bw.write("CATACACCTCACCCCACATATGTTTCCTTGCCGTAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAA\n")
            bw.write("ATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTGCGACAAAAATTAGGGTTGTGTGGCCA\n")
            bw.write("TTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTG\n")
            bw.write("AGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAA\n")
            bw.write(">chr2\n")
            bw.write("TCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTCATAAACCTCACCCCACATAT\n")
            bw.write("GTTTCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAAACCATTTCTTGCTTAAAAACTCGTACGTGTTAGCCTTCG\n")
            bw.write("GTATTATTGAAAATGGTCATTCATGGCTATTTTTCGGCAAAATGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTC\n")
            bw.write("ATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAGAAA\n")
            bw.write("TCCGTAGGTGTTAGCCTTCGATATTATTGAAAATGGTCGTTCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCAT\n")
            bw.write("TGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGA\n")
            bw.write("GACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCATTCATGGCTATTTTCGACAAA\n")
            bw.write("AATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGCCATAGATCAC\n")
            bw.write("ATTCTTGGATTTATGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGTATTATTGTAAATGGTCAT\n")
            bw.write("TCATGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCATAGGCTCATACACCTCACCCCACATAT\n")
            bw.write("GTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTCTTAGCCTCTA\n")
            bw.write("ATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTTATCATCGACTAGAGGCTC\n")
            bw.write("ATAAATCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGACCATTTCTTGGTCAAAAA\n")
            bw.write("CTCGTACGTGTTAGCCTTTGGTATTATTGAAAATGGTCGTTCATGGCTATTTTCGGCAAAATGGGGGTTGTGTGGCCATT\n")
            bw.write("GATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTATCCTTGCCATAGATTACATTCTTGGATTTCTGGTGGAA\n")
            bw.write("ACCATTTCTTGGTTAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAA\n")
            bw.write("TGGGGGTTGTGTGGCCATTGATCGTCGACCAGAGGCTCATACACCTCACCCCACATATGTTTCCTTGTCGTAGATCACAT\n")
            bw.write("TCTTGAATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGAGTTAGCCTTCGATATTATTGAAAATGGTCGTTC\n")
            bw.write("ATGGCCATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCATACACCTCACCCCACATATGT\n")
            bw.write("TTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATCCGTAGGTGTTAGCCTTCGGT\n")
            bw.write("ATTATTGTAAATGGTCATTCATGGCTATTTTCGACAACAATGTGGGTTGTGTGGCCATTGATCATCGACCAGAGGCTCAT\n")
            bw.write("ACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATTCTTGGATTTCTGGTGGAGACCATTTCTTGGTCAAAAATA\n")
            bw.write("CGTAGGTCTTAGCCTCTAATATTATTGAAAATGGTCGCTCATGGCTATTTTCATAAAAAATGGGGGTTGTGTGGCCATTT\n")
            bw.write("ATCATTGACTAGAGGCTCATAAATCTCACCCCTCATATGTTTCCTTGCCATAGATCACATTCTTGGATTTTTGGTGGAGA\n")
            bw.write("CCATTTCTTGGTCAAAAACTCGTACGTGTTAGCCTTCGGTATTATTGAAAATGGTCATTCATGGCTATTTTCGGCAAAAT\n")
            bw.write("GGGGGTTCTGTGGCCATTGATCATCGACCAGAGGCTCGTACACCTCACCCCACATATGTTTCCTTGCCATAGATCACATT\n")
            bw.write("CTTGGATTTCTGGTGGAGACCATTTCTTGGGCAAAAATCCGAATGTGTTAGCCTTTGGTATTTTTGAAAATGGTCATTCA\n")
            bw.write("TGGCTATTTTCGACAAAAATGGGGGTTGTGTGGCCATTGATCTTCGACCAGAGGCTCATACACCTCACTACACATATGTT\n")
        }
    } catch (exc: Exception) {
        exc.printStackTrace()
        throw IllegalStateException("CreateValidIntervalsFilePluginTest: error writing test ref fasta: ${exc.message}")
    }
}


