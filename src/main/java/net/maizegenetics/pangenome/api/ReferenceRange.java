package net.maizegenetics.pangenome.api;

import com.google.common.collect.ImmutableSet;
import net.maizegenetics.dna.map.Chromosome;

import java.util.Set;

/**
 * @author Terry Casstevens Created June 19, 2017
 * @author Zack Miller
 * @author Karl Kremling
 * @author Jacob Washburn
 */
public class ReferenceRange implements Comparable<ReferenceRange> {

    private final String myReferenceName;
    private final Chromosome myChromosome;
    private final int myStart;
    private final int myEnd;
    private final int myID;
    private final ImmutableSet<String> myMethods;

    public ReferenceRange(String referenceName, Chromosome chromosome, int start, int end, int id) {
        this(referenceName, chromosome, start, end, id, ImmutableSet.of());
    }

    public ReferenceRange(String referenceName, Chromosome chromosome, int start, int end, int id, String method) {
        this(referenceName, chromosome, start, end, id, ImmutableSet.of(method));
    }

    public ReferenceRange(String referenceName, Chromosome chromosome, int start, int end, int id, ImmutableSet<String> methods) {
        myReferenceName = referenceName;
        myChromosome = chromosome;
        myStart = start;
        myEnd = end;
        myID = id;
        myMethods = methods;
    }

    /**
     * Chromosome in the reference genome from which this ReferenceRange originates
     */
    public Chromosome chromosome() {
        return myChromosome;
    }

    /**
     * Start position of reference range, inclusive
     */
    public int start() {
        return myStart;
    }

    /**
     * End position of reference range, inclusive
     */
    public int end() {
        return myEnd;
    }

    /**
     * Identifier for a unique reference range
     */
    public int id() {
        return myID;
    }

    /**
     * Returns group methods that this range belongs.
     *
     * @return group methods
     */
    public Set<String> groupMethods() {
        return myMethods;
    }

    /**
     * Returns whether this reference range is part of specifed group.
     *
     * @param groupMethod group method
     *
     * @return whether this reference range is part of specifed group.
     */
    public boolean isPartOf(String groupMethod) {
        return myMethods.contains(groupMethod);
    }

    /**
     * Reference genome (B73, CML247, EP1, etc) from which the reference range originates
     */
    public String referenceName() {
        return myReferenceName;
    }

    public String intervalString() {
        return myChromosome.getName() + ":" + myStart + "-" + myEnd;
    }

    @Override
    public String toString() {
        return "ReferenceRange{" +
                "myReferenceName='" + myReferenceName + '\'' +
                "myMethods='" + myMethods.toString() + '\'' +
                ", myChromosome=" + myChromosome +
                ", myStart=" + myStart +
                ", myEnd=" + myEnd +
                '}';
    }

    @Override
    public int compareTo(ReferenceRange o) {
        int result = myChromosome.compareTo(o.chromosome());
        if (result != 0) {
            return result;
        }
        return Integer.compare(myStart, o.start());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferenceRange that = (ReferenceRange) o;

        if (myStart != that.myStart) return false;
        if (myEnd != that.myEnd) return false;
        if (myID != that.myID) return false;
        if (!myMethods.equals(that.myMethods)) return false;
        if (!myReferenceName.equals(that.myReferenceName)) return false;
        return myChromosome.equals(that.myChromosome);
    }


    @Override
    public int hashCode() {
        int result = myReferenceName.hashCode();
        result = 31 * result + myChromosome.hashCode();
        result = 31 * result + myStart;
        result = 31 * result + myEnd;
        result = 31 * result + myID;
        result = 31 * result + myMethods.hashCode();
        return result;
    }
}
