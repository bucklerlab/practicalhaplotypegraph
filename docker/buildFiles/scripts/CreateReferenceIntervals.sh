#!/bin/bash

# check that we have awk installed, try to install if not
[ -z "$(which gawk)" ] && apt-get install gawk -y


#================================
# FUNCTION DEFINITIONS
#--------------------------------

#print instructions for script use when called
usage_h () {
cat <<EOF
SCRIPT PURPOSE:
  Generate reference intervals for the Practical Haplotype Graph (PHG)

NOTES: 
  This script assumes it is run inside the PHG Docker container with predefined I/O paths

REQUIRED PARAMETERS:
  -f <file name>
     name of fasta file containing the reference sequence
  -a <file name>
     name of genome annotation file in .gff format containing gene model annotation

OPTIONAL PARAMETERS:
  -k <integer>
     Length of kmer used for determining repetitive regions 
     Default: 11
  -e <integer>
     Number of bases by which to expand gene models for initial reference interval selection
     Default: 1000
  -m <integer>
     Distance (in bp) between genes below which gene models are merged
     Default: 100
  -p <double>
     Proportion of kmers to be considered repetitive. 
     This determines the high kmer count tail which is considered repetitive (e.g. the top 0.05 most frequent)
     Default: 0.1
  -n <integer> 
     Number of kmer copies (genome-wide) above which a kmer is considered repetitive. Overrides -p
     Default: none, -p is used by default
  -l <integer>
     The number of bases to consider when evaluating if a location in the genome is repetitive
     Default: 100
  -s <integer>
     The step size (in bp) by which to proceed outward from a gene model when evaluating flanking regions
     Default: 10

  -h This help message
EOF
}


#================================
# AUTOMATIC PARAMETERS
#--------------------------------

# I/O parameters
#---------------------

make_anchors_script="$(readlink -enq "/PHG_practical_reference_intervals/00_create_practical_reference_intervals.sh")"
[ -z "${make_anchors_script}" ] && unset make_anchors_script
: ${make_anchors_script?"ERROR: Could not find expected script /PHG_practical_reference_intervals/00_create_practical_reference_intervals.sh"}

data_d="$(readlink -enq "/tempFileDir/data")"
[ -z "${data_d}" ] && unset data_d
: ${data_d?"ERROR: Could not find data directory. Please make sure to mount your data directory as /tempFileDir/data"}


#================================
# ARGUMENT PARSING
#--------------------------------

#print usage if no options were given
if [ -z "$1" ]
then
    usage_h
    exit 0
fi

OPTIND='1'
options=':f:a:k:e:m:n:p:l:s:h'
while getopts $options option
do
    case $option in
	f  ) fasta_inf=$OPTARG;;
	a  ) annot_inf=$OPTARG;;
	k  ) mer_ln=$OPTARG;;
	e  ) gene_expand_bp=$OPTARG;;
	m  ) min_gene_dist=$OPTARG;;
	n  ) max_kmer_count=$OPTARG;;
	p  ) trim_topkmer_prop=$OPTARG;;
	l  ) mavg_ln=$OPTARG;;
	s  ) mavg_step=$OPTARG;;
	h  ) usage_h; exit 0;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done


#====================================
# REQUIRED PARAM ERRORS AND DEFAULTS
#------------------------------------

# Required I/O
input_d="${data_d}"
out_d="${data_d}"
fasta_inf=$(basename "${fasta_inf}")
annot_inf=$(basename "${annot_inf}")

# Optional parameters
: ${mer_ln:=11}
: ${gene_expand_bp:=1000}
: ${min_gene_dist:=100}
: ${mavg_ln:=100}
: ${mavg_step:=10}

# NOTE: $max_kmer_count and $trim_topkmer_prop are mutually exclusive
#       $max_kmer_count takes precedence
max_kmer_count_flag=''
if [ -z "${max_kmer_count}" ]
then
: ${trim_topkmer_prop:=0.1}
else
  max_kmer_count_flag=" -n ${max_kmer_count}"
fi

# Run the scripts with the provided parameters
bash $make_anchors_script -i ${input_d} -f ${fasta_inf} -a ${annot_inf} -o ${out_d} -k ${mer_ln} -e ${gene_expand_bp} -m ${min_gene_dist} -p ${trim_topkmer_prop} -l ${mavg_ln} -s ${mavg_step} ${max_kmer_count_flag}
