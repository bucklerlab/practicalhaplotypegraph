package net.maizegenetics.pangenome.hapCalling

/**
 * This plugin is required input when a user wants to run the PathsToVCFPlugin.
 * It creates and returns a matrix for all the paths for the specified path methods.
 * This maybe filtered by taxa if taxa are specified by the user.
 *
 * It takes as input a haplotype graph, and returns a DataSet containing both that
 * haplotype graph and the map of haplotypes paths.
 *
 * Note on multiple paths:
 *   The haplotype graph that is passed must be created with all haplotype methods used for each
 *   path method supplied as a parameter to this plugin.  But you cannot combine paths made from
 *   consensus haplotypes with paths made from non-consensus haplotypes that are included in
 *   the consensus haplotypes.  If you do, the taxa will be included twice - once as non-consensus
 *   haplotypes and once as part of the consensus haplotypes.  This results in the exception:
 *      java.lang.IllegalStateException: HaplotypeGraph: verifyGraph: taxon
 *   from HaplotypeGraph:verifyGraph() due to a taxan represented more than once for reference range
 *
 *   Allowed: Multiple paths that all use the same haplotype methods, or multiple paths that use haplotypes methods with
 *   no overlapping taxa, e.g. anchorwave_NAM_assembly_PATH, anchorwave_released_assembly_PATH
 */
import net.maizegenetics.pangenome.api.GraphUtils
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.Datum
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.Taxon
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*
import javax.swing.ImageIcon

class ImportDiploidPathPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(ImportDiploidPathPlugin::class.java)

    private var pathMethodName = PluginParameter.Builder("pathMethodName", null, String::class.java)
        .description("Common separated list of Path Method Names Stored in the DB")
        .required(true)
        .build()

    private var taxa = PluginParameter.Builder("taxa", null, TaxaList::class.java)
        .description("Optional list of taxa to include. This can be a comma separated list of taxa (no spaces unless surrounded by quotes), file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). By default, all taxa will be included.")
        .required(false)
        .build()

    override fun processData(input: DataSet?): DataSet? {

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
        require(temp?.size == 1) { "ImportDiploidPathPlugin: processData: must input one HaplotypeGraph." }
        val graphDatum = temp!![0]
        val graph = graphDatum.data as HaplotypeGraph

        val haplotypePaths = importPathsFromDB(graph, pathMethodName())

        return DataSet(arrayOf(graphDatum, Datum("Paths", haplotypePaths, null)), this)

    }

    /**
     * @param pathMethod    the name of the path method in the PHG DB
     *
     * @param pathMethod: a comma separated string of method names
     * @return a MatrixWithNames of haplotype ids with taxa name for row names and reference range id for the column name.
     * The method returns a matrix for all the paths for pathMethod.
     */
    private fun importPathsFromDB(graph: HaplotypeGraph, pathMethod: String): Map<String, List<List<HaplotypeNode>>> {

        DBLoadingUtils.connection(false).use { dbConn ->

            // get the paths from the db for the method name (taxa name, list of haplotype ids)
            // all paths for path method as pairs of line_name, list of haplotypes
            val pathList = LinkedHashMap<String, List<List<HaplotypeNode>>>()
            val pathQuerySB = StringBuilder()

            val paths = pathMethod.split(",")
            pathQuerySB.append("SELECT line_name, paths_data FROM paths, genotypes, methods ")
            pathQuerySB.append("WHERE paths.genoid=genotypes.genoid AND methods.method_id=paths.method_id AND methods.name IN ('")
            for ( pathName in paths) {
                pathQuerySB.append(pathName)
                pathQuerySB.append("','")
            }
            pathQuerySB.setLength(pathQuerySB.length-2) // remove the last ",'"
            pathQuerySB.append(")")

            val pathQuery = pathQuerySB.toString()
            myLogger.info("importPathsFromDB: query: $pathQuery")

            var pathResult: ResultSet? = null
            try {

                pathResult = dbConn.createStatement().executeQuery(pathQuery)

                while (pathResult.next()) {
                    val lineName = pathResult.getString(1)
                    if (taxa() == null || taxa()!!.contains(Taxon(lineName))) {
                        val haplotypeLists = DBLoadingUtils.decodePathsForMultipleLists(pathResult.getBytes(2))
                        pathList[lineName] = haplotypeLists
                            .map { hapids -> GraphUtils.nodes(graph, TreeSet<Int>(hapids)) }
                            .toList()
                    }
                }

            } catch (se: SQLException) {
                throw IllegalArgumentException("ImportDiploidPathPlugin: importPathsFromDB: Could not execute query: $pathQuery", se)
            } finally {
                pathResult?.close()
            }

            myLogger.info("importPathsFromDB: number of path list: ${pathList.size}")

            return pathList

        }

    }

    /**
     * Path Method Name Stored in the DB
     *
     * @return Path Method Name
     */
    fun pathMethodName(): String {
        return pathMethodName.value()
    }

    /**
     * Set Path Method Name. Path Method Name Stored in the
     * DB
     *
     * @param value Path Method Name
     *
     * @return this plugin
     */
    fun pathMethodName(value: String): ImportDiploidPathPlugin {
        pathMethodName = PluginParameter<String>(pathMethodName, value)
        return this
    }

    /**
     * Optional list of taxa to include. This can be a comma
     * separated list of taxa (no spaces unless surrounded
     * by quotes), file (.txt) with list of taxa names to
     * include, or a taxa list file (.json or .json.gz). By
     * default, all taxa will be included.
     *
     * @return Taxa
     */
    fun taxa(): TaxaList? {
        return taxa.value()
    }

    /**
     * Set Taxa. Optional list of taxa to include. This can
     * be a comma separated list of taxa (no spaces unless
     * surrounded by quotes), file (.txt) with list of taxa
     * names to include, or a taxa list file (.json or .json.gz).
     * By default, all taxa will be included.
     *
     * @param value Taxa
     *
     * @return this plugin
     */
    fun taxa(value: TaxaList): ImportDiploidPathPlugin {
        taxa = PluginParameter(taxa, value)
        return this
    }

    override fun getToolTipText(): String {
        return "Import Diploid Paths"
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Import Diploid Paths"
    }

}
