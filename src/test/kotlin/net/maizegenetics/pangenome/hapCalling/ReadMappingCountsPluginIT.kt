package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import java.io.File

@ExtendWith(SmallSeqExtension::class)
class ReadMappingCountsPluginIT {

    @Test
    fun writeReadCounts() {
        File("${SmallSeqExtension.baseDir}/outputDir/").mkdirs()
        val outputFile = "${SmallSeqExtension.baseDir}/outputDir/readCounts.txt"
        ParameterCache.load(SmallSeqExtension.configFilePath)
        val countsPlugin = ReadMappingCountsPlugin()
        countsPlugin.readMethod(SmallSeqExtension.imputeMethod)
        countsPlugin.outputFile(outputFile)
        countsPlugin.performFunction(null)
        val linesInFile = Utils.getBufferedReader(outputFile).use {
            it.lines().count()
        }

        //expect the output file to contain 29 lines
        val numberOfExpectedLines = 29L
        assertEquals(numberOfExpectedLines, linesInFile, "Number of lines in the read count file was not $numberOfExpectedLines.")
    }
}