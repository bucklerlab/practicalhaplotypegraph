package net.maizegenetics.pangenome.api;

import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

/**
 * Created on June 20, 2017
 *
 * @author Terry Casstevens
 * @author Karl Kremling
 */
public class HaplotypePath {

    private final List<HaplotypeEdge> myEdges;
    private final double myLikelihood;

    /**
     * Constructor for HaplotypePath.  Don't use directly.  Use {@link HaplotypeGraph}
     *
     * @param edges HaplotypeEdges that comprise this path
     */
    HaplotypePath(HaplotypeEdge[] edges) {
        this(Arrays.asList(edges));
    }

    HaplotypePath(List<HaplotypeEdge> edges) {

        if (edges == null || edges.isEmpty()) {
            throw new IllegalArgumentException("HaplotypePath: init: must specify at least one edge.");
        }

        if (edges instanceof ImmutableList) {
            myEdges = edges;
        } else {
            ImmutableList.Builder<HaplotypeEdge> builder = new ImmutableList.Builder<>();
            HaplotypeNode previous = null;
            for (HaplotypeEdge current : edges) {
                if (previous == null) {
                    previous = current.rightHapNode();
                } else {
                    if (previous != current.leftHapNode()) {
                        throw new IllegalStateException("HaplotypePath: init: Consecutive edges should share HaplotypeNodes.");
                    }
                    previous = current.rightHapNode();
                }
                builder.add(current);
            }
            myEdges = builder.build();
        }

        myLikelihood = caculateLikelihood();

    }

    /**
     * Haplotype Edges that create this HaplotypePath
     */
    public List<HaplotypeEdge> edges() {
        return myEdges;
    }

    public List<HaplotypeNode> nodes() {

        ImmutableList.Builder<HaplotypeNode> builder = new ImmutableList.Builder<>();
        builder.add(myEdges.get(0).leftHapNode());
        for (HaplotypeEdge edge : myEdges) {
            builder.add(edge.rightHapNode());
        }
        return builder.build();

    }

    /**
     * Likelihood of haplotype path calculated from all included haplotype nodes
     */
    public double likelihood() {
        return myLikelihood;
    }

    private double caculateLikelihood() {
        // TODO Calculate from HaplotypeEdge.edgeProbability()
        return 0.0;
    }

    /**
     * Sequence from nodes comprising this path.
     *
     * @return Sequence
     */
    public String sequence() {
        StringBuilder builder = new StringBuilder();
        builder.append(myEdges.get(0).leftHapNode().haplotypeSequence().sequence());
        for (HaplotypeEdge edge : myEdges) {
            builder.append(edge.rightHapNode().haplotypeSequence().sequence());
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (HaplotypeEdge edge : myEdges) {
            builder.append(edge.toString());
            builder.append("\n");
        }
        return builder.toString();
    }

}
