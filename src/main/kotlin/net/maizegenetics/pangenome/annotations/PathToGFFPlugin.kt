package net.maizegenetics.pangenome.annotations

import htsjdk.tribble.gff.Gff3Feature
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.db_updateDelete.DeletePathsPlugin
import net.maizegenetics.plugindef.*
import net.maizegenetics.taxa.TaxaList
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.util.*
import javax.swing.ImageIcon
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * THis class takes a keyfile with taxon to gff-path mapping, a taxon name and a method.
 * It creates a haplotype graph from the path obtained
 *
 * Purpose:  1.  We need the paths data from the phg: so need connection, taxon, path method: pull paths blob
 *           2.  From the paths, we need to build a haplotypes graph
 *           3.  From the graph, we order the hapids by reference range
 *           4.  For each Haplotype node, get asm_data
 *
 *           5.  The config file is needed to pull the graph and the paths
 *  input:
 *   taxon: list of taxa with paths for which the user wants a GFF created (taxon name must match line_name in genotypes table.)
 *      These should be taxa that have a path in the paths table.
 *   methods: method name.  If present, only pull specified taxa from the db when their method matches the method parameter.
 *   configFile:  for querying the db, and later making a graph from these taxa
 *   keyFile:  mapping of taxon name to existing user supplied GFF file for that assembly.
 *   gffOutputFile:  given if they want a gff file.
 *
 *   If user wants a fasta, that will be a different plugin
 */
class PathToGFFPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = LogManager.getLogger(PathToGFFPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .description("KeyFile file name.  Must be a tab separated file using the following headers:\n" +
                    "TaxonName\tGFF File Path\n" +
                    "TaxonName values must match taxon names stored in the genotypes table.\n" +
                    "GFF file path should be full path and file name of existing assembly GFF file for the specified taxon")
            .required(true)
            .inFile()
            .build()

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("Config file containing lines for host=,user=, password=, DB= and DBtype= where DBtype is either sqlite or postgres.")
            .inFile()
            .required(true)
            .build()

    private var taxa = PluginParameter.Builder("taxa", null, TaxaList::class.java)
            .required(true)
            .description("List of taxa for which paths will be pulled from the database.  " +
                    "  This can be a comma separated list of taxa (no spaces unless surrounded by quotes), " +
                    "file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). ")
            .build()

    private var method = PluginParameter.Builder("method", null, String::class.java)
            .description("A path method name. If present, only taxa from the taxa input list that were stored with this method will have their paths pulled from the db.")
            .required(false)
            .build()

    private var includeSequences = PluginParameter.Builder("includeSequences", false, Boolean::class.javaObjectType)
            .required(false)
            .description("Whether to include sequences in haplotype nodes.")
            .build()

    private var gffOutputDir = PluginParameter.Builder("gffOutputDir", null, String::class.java)
            .required(false)
            .outDir()
            .description("If present, gff files will be written to this directory.  ")
            .guiName("GFF Output Directory")
            .build()

    override fun processData(input: DataSet?): DataSet? {
        val time = System.nanoTime()

        // Get map of taxon to List<GFF3Features>
        val resultsTreeCenter = loadGFFsToGff3Feature(keyFile())

        // Get list of taxa names
        val taxonList = taxa().map{ it.name}.toList()

        // Get paths from db
        val dbConn = DBLoadingUtils.connection(configFile(), false)
        val phg = PHGdbAccess(dbConn)

        // getPaths returns Map<String,List<Integer>>
        myLogger.info("Getting paths for taxa: ${taxa()}")
        val paths = phg.getPathsForTaxonMethod(taxonList, method());

        myLogger.info("Size of paths is : ${paths.size}")
        // You could get the entire graph here, in which case we don't need to
        // create it multiple times.  But it will be very big - we don't know
        // which haplotypes we want
        // or you can get it based on hapids for each path

        val taxonGffMap = mutableMapOf<String,List<Set<Gff3Feature>>>()
        for ((taxon, pathList) in paths) {
            val numLists = pathList.size
            val gffLists = ArrayList<Set<Gff3Feature>>()

            // Iterator with index because we need the index to identify the hap number if we output a file
            val iterator = pathList.iterator()
            for ((index, path) in iterator.withIndex()) {
                val hapSet = path.toSortedSet()
                // make graph from the list of path hapids
                myLogger.info("Creating graph for taxon ${taxon} hapnumber ${index}")
                var graph =  HaplotypeGraphBuilderPlugin(null, false)
                        .configFile(configFile())
                        .hapids(hapSet)
                        .includeSequences(includeSequences())
                        .build()

                // If we get the graph once, for all paths on the graph, we would then
                // need to filter it based on hapids in the graph, and send each filtered
                // graph to makeGffFromPath()

                // Create the pseudo-genome GFF file.  This returns a map of taxon->pseudo-genome GFF
                val hapNumber = if (numLists == 1) "" else "_hap${index}" // only add hapNumber to file name if is not haploid
                val outFile = if (gffOutputDir() != null) "${gffOutputDir()}/${taxon}${hapNumber}_${method()}_pseudoGenome_GffFromPaths.gff3" else null
                myLogger.info("Calling makeGffFromPath for taxon ${taxon}")
                val time = System.nanoTime()
                val taxonPathGFF = makeGffFromPath(path, resultsTreeCenter, graph, outFile)
                val endingTime = (System.nanoTime() - time)/1e9
                myLogger.info("finished makeGffFromPath for taxon ${taxon} list ${index} in ${endingTime} seconds")
                gffLists.add(taxonPathGFF) // add to the list of Gff3Feature sets
            }
            taxonGffMap.put(taxon,gffLists) // add the List of FeatureSets to the taxon->GFF maps

        }

        // This will return a map of taxon to List<Set<Gff3Feature>>
        // The Set<Gff3Feature> can be printed via GFFUtils.writeGffFile(...)
        return DataSet(Datum("TaxonGFFmap", taxonGffMap, "Gff3Feature list for: " + taxa()), this)
    }


    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return("Get Gff for Paths")
    }

    override fun getToolTipText(): String {
        return("Creates a GFF file for the pseudo-genome represented by the taxon's path ")
    }

    /**
     * KeyFile file name.  Must be a tab separated file using
     * the following headers:
     * TaxonName	GFF File Path
     * TaxonName values must match taxon names stored in the
     * genotypes table.
     * GFF file path should be full path and file name of
     * GFF file for the specified taxon
     *
     * @return Key File
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set Key File. KeyFile file name.  Must be a tab separated
     * file using the following headers:
     * TaxonName	GFF File Path
     * TaxonName values must match taxon names stored in the
     * genotypes table.
     * GFF file path should be full path and file name of
     * GFF file for the specified taxon
     *
     * @param value Key File
     *
     * @return this plugin
     */
    fun keyFile(value: String): PathToGFFPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Config file containing lines for host=,user=, password=,
     * DB= and DBtype= where DBtype is either sqlite or postgres.
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Config file containing lines for host=,user=,
     * password=, DB= and DBtype= where DBtype is either sqlite
     * or postgres.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): PathToGFFPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * List of taxa for which paths will be pulled from the
     * database.    This can be a comma separated
     * list of taxa (no spaces unless surrounded by quotes),
     * file (.txt) with list of taxa names to include, or
     * a taxa list file (.json or .json.gz).
     *
     * @return Taxa
     */
    fun taxa(): TaxaList {
        return taxa.value()
    }

    /**
     * Set Taxa. List of taxa for which paths will be pulled from
     * the database.    This can be a comma
     * separated list of taxa (no spaces unless surrounded
     * by quotes), file (.txt) with list of taxa names to
     * include, or a taxa list file (.json or .json.gz).
     *
     * @param value Taxa
     *
     * @return this plugin
     */
    fun taxa(value: TaxaList): PathToGFFPlugin {
        taxa = PluginParameter<TaxaList>(taxa, value)
        return this
    }

    /**
     * Set Taxa.  Converts taxa to taxaList when the taxa comes in as a string
     * String should be a comma-separated list of taxa, no spaces!
     */
    fun taxa(value: String?): PathToGFFPlugin {
        taxa = PluginParameter<TaxaList>(taxa, convert(value, TaxaList::class.java))
        return this
    }

    /**
     * Path method name If present, only
     * taxa from the taxa input that were stored with this
     * method will have their paths pulled from the db.
     *
     * @return Methods
     */
    fun method(): String? {
        return method.value()
    }

    /**
     * Set Method
     * If present, only taxa from the taxa input that were
     * stored with this method will have their paths pulled
     * from the db.
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun method(value: String): PathToGFFPlugin {
        method = PluginParameter<String>(method, value)
        return this
    }

    /**
     * Whether to include sequences in haplotype nodes.
     *
     * @return Include Sequences
     */
    fun includeSequences(): Boolean {
        return includeSequences.value()
    }

    /**
     * Set Include Sequences. Whether to include sequences
     * in haplotype nodes.
     *
     * @param value Include Sequences
     *
     * @return this plugin
     */
    fun includeSequences(value: Boolean): PathToGFFPlugin {
        includeSequences = PluginParameter<Boolean>(includeSequences, value)
        return this
    }
    /**
     * If present, gff files will be written to this directory.
     *
     *
     * @return GFF Output Directory
     */
    fun gffOutputDir(): String? {
        return gffOutputDir.value()
    }

    /**
     * Set GFF Output Directory. If present, gff files will
     * be written to this directory.
     *
     * @param value GFF Output Directory
     *
     * @return this plugin
     */
    fun gffOutputDir(value: String?): PathToGFFPlugin {
        gffOutputDir = PluginParameter<String>(gffOutputDir, value)
        return this
    }
}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(PathToGFFPlugin::class.java)
}