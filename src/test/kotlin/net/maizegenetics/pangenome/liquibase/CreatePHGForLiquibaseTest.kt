package net.maizegenetics.pangenome.liquibase

import org.apache.logging.log4j.LogManager
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.liquibase.CraeteLiquibaseTestData")

fun createPHGDocker() {
    // Check if liquibase_initial and liquibase_phg images exist.
    var baseExists = false
    var testExists = false
    try {
        val cmd = "docker images"
        val start = Runtime.getRuntime().exec(cmd)
        val br = BufferedReader(
                InputStreamReader(start.inputStream))

        br.use{ bufferedReader ->
            while(true) {
                var line = bufferedReader.readLine()
                if (line.isNullOrBlank()) break // quit when buffer is empty
                if (line.contains("CREATED")) continue // skip header line
                val imageName = line.substring(0, line.indexOf(" ")) // up to first white space
                if (imageName == "phgrepository_base") {
                    baseExists = true
                }
                if (imageName == "phgrepository_test") {
                    testExists = true
                }
            }
        }
    } catch (exc: Exception) {
        throw IllegalStateException("createPHGDocker:main - error reading docker images: " + exc.message)
    }

    println("BaseExists: $baseExists testExists $testExists")

    // Base repository is created if it doesn't exist.  Test repository image
    // is deleted if it does exist.
    try {

        if (testExists) {
            // remove old docker liquibase_phg image
            val builder = ProcessBuilder(
                    "docker", "rmi", "phgrepository_test")

            builder.inheritIO()
            println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            //println("Command:" + builder.command().stream().collect<String, *>(Collectors.joining(" ")))
            val process = builder.start()
            val error = process.waitFor()
            if (error != 0) {
                // assume repository doesn't exist - this isn't real error
                println("Error removing phgrepository_test, assuming it doesn't exist, Error: $error")
            }
        }

        if (!baseExists) {
            // no liquibase_initial - create it
            // Setup variables for liquibase_initial, create base image
            val workingDir = System.getProperty("user.dir")
            println("WorkingDir: $workingDir")
            val dockerDir = "$workingDir/docker/buildFiles"

            println("Creating repository docker ")
            val builder = ProcessBuilder(
                    "docker", "build", "-t", "phgrepository_base", dockerDir)
            builder.inheritIO()
            println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")));

            val process = builder.start()
            process.waitFor()

        }

    } catch (exc: Exception) {
        throw IllegalStateException("Error deleting/creating docker images " + exc.message)
    }

    // Create test docker image based on phgrepository_base image.
    // I'm using the same direcotry as smallSeq - updated phg.jar should be placed here
    val testDockerDir = System.getProperty("user.home") + "/temp/smallseq_docker"
    println("Creating second docker on top of phgrepository_base image")

    try {
        val builder = ProcessBuilder(
                "docker", "build", "-t", "phgrepository_test", testDockerDir)
        builder.inheritIO()
        println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")))
        val process = builder.start()
        process.waitFor()
    } catch (e: Exception) {
        println("Creating second docker failed")
        throw IllegalStateException("Creating phgrepository_test docker image from phg repository failed ")
    }

}
