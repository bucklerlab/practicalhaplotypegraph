package net.maizegenetics.pangenome.db_loading;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.pangenome.api.CreateGraphUtils;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.TableReportBuilder;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Terry Casstevens
 * Created July 31, 2018
 */
public class DatabaseSummaryPlugin extends AbstractPlugin {

    private static final Logger myLogger = LogManager.getLogger(DatabaseSummaryPlugin.class);

    private Map<Integer, String> myMethodIdToName = new HashMap<>();

    private PluginParameter<String> configFile = new PluginParameter.Builder<String>("configFile", null, String.class)
            .description("Configuration file")
            .required(true)
            .inFile()
            .build();

    public DatabaseSummaryPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {

        try (Connection database = DBLoadingUtils.connection(configFile(), false)) {
            DatabaseMetaData meta = database.getMetaData();
            String databaseName = Utils.getFilename(meta.getURL());
            List<Datum> result = new ArrayList<>();
            result.add(methods(database, databaseName));
            result.add(taxa(database, databaseName));
            result.add(dbSummaryStats(database, databaseName));
            return new DataSet(result, this);
        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("DatabaseSummaryPlugin: processData: Problem getting summary: " + e.getMessage());
        }

    }

    private Datum methods(Connection database, String databaseName) {

        String query = "SELECT * FROM methods;";

        try (ResultSet rs = database.createStatement().executeQuery(query)) {

            String[] columnNames = new String[]{"method name", "method type", "description"};
            TableReportBuilder builder = TableReportBuilder.getInstance("Methods", columnNames);

            while (rs.next()) {
                Object[] row = new Object[3];
                int type = rs.getInt("method_type");
                for (DBLoadingUtils.MethodType current : DBLoadingUtils.MethodType.values()) {
                    if (current.value == type) {
                        row[1] = current.toString();
                    }
                }
                row[0] = rs.getString("name");
                row[2] = rs.getString("description");
                myMethodIdToName.put(rs.getInt("method_id"), (String) row[0]);
                builder.add(row);
            }

            return new Datum(databaseName + " Methods", builder.build(), null);

        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("DatabaseSummaryPlugin: methods: Problem querying the database: " + e.getMessage());
        }

    }

    private Datum taxa(Connection database, String databaseName) {

        String query = "SELECT DISTINCT line_name, line_data FROM genotypes ORDER BY line_name;";

        try (ResultSet rs = database.createStatement().executeQuery(query)) {

            String[] columnNames = new String[]{"line name", "description"};
            TableReportBuilder builder = TableReportBuilder.getInstance("Taxa", columnNames);

            while (rs.next()) {
                Object[] row = new Object[2];
                row[0] = rs.getString("line_name");
                row[1] = rs.getString("line_data");
                builder.add(row);
            }

            return new Datum(databaseName + " Taxa", builder.build(), null);

        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("DatabaseSummaryPlugin: taxa: Problem querying the database: " + e.getMessage());
        }

    }

    private Datum dbSummaryStats(Connection database, String databaseName) {

        String[] columnNames = new String[]{"stat type", "value"};
        TableReportBuilder builder = TableReportBuilder.getInstance("Stats", columnNames);

        refRangesPerChr(database, builder);
        builder.add(new Object[2]);
        haplotypesPerMethod(database, builder);

        return new Datum(databaseName + " Stats", builder.build(), null);

    }

    private void refRangesPerChr(Connection database, TableReportBuilder builder) {

        Map<Integer, ReferenceRange> ranges = CreateGraphUtils.referenceRangeMap(database);
        Map<Chromosome, Integer> numRangesPerChr = new TreeMap<>();
        for (ReferenceRange range : ranges.values()) {
            Chromosome chr = range.chromosome();
            Integer count = numRangesPerChr.get(chr);
            if (count == null) {
                numRangesPerChr.put(chr, 1);
            } else {
                numRangesPerChr.put(chr, count + 1);
            }
        }

        int totalRanges = 0;
        for (Map.Entry<Chromosome, Integer> current : numRangesPerChr.entrySet()) {
            Object[] row = new Object[2];
            row[0] = "Number of Reference Ranges for chromosome: " + current.getKey().getName();
            row[1] = current.getValue();
            builder.add(row);
            totalRanges += current.getValue();
        }
        builder.add(new Object[]{"Total Number of Reference Ranges", totalRanges});

    }

    private void haplotypesPerMethod(Connection database, TableReportBuilder builder) {

        String query = "SELECT method_id, COUNT(haplotypes_id) AS `num` FROM haplotypes GROUP BY method_id;";

        try (ResultSet rs = database.createStatement().executeQuery(query)) {

            int totalHaplotypes = 0;
            while (rs.next()) {
                Object[] row = new Object[2];
                row[0] = "Number of Haplotypes for Method: " + myMethodIdToName.get(rs.getInt("method_id"));
                int num = rs.getInt("num");
                row[1] = num;
                builder.add(row);
                totalHaplotypes += num;
            }
            builder.add(new Object[]{"Total Number of Haplotypes", totalHaplotypes});

        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("DatabaseSummaryPlugin: haplotypesPerMethod: Problem querying the database: " + e.getMessage());
        }

    }

    /**
     * Configuration file
     *
     * @return Config File
     */
    public String configFile() {
        return configFile.value();
    }

    /**
     * Set Config File. Configuration file
     *
     * @param value Config File
     *
     * @return this plugin
     */
    public DatabaseSummaryPlugin configFile(String value) {
        configFile = new PluginParameter<>(configFile, value);
        return this;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Database Summary";
    }

    @Override
    public String getToolTipText() {
        return "Database Summary";
    }
}
