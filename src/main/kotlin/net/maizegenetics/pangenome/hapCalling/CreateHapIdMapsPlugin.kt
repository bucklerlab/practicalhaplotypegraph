package net.maizegenetics.pangenome.hapCalling

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon


/**
 * Plugin to create the Haplotype Id Maps needed by the read mapping steps of the PHG.
 * This is very useful when running on a SLURM based system where you are unable to make SQL calls over the network.
 * By dumping the graph to a single JSON file read mapping can be run without needing the DB connection.
 */
class CreateHapIdMapsPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(CreateHapIdMapsPlugin::class.java)

    private var outputMapFile = PluginParameter.Builder("outputMapFile", null, String::class.java)
        .guiName("Output File Name")
        .outFile()
        .required(true)
        .description("Output File Name.  JSON file will be written")
        .build()

    private var outputTaxaNames = PluginParameter.Builder("outputTaxaNames", true, Boolean::class.javaObjectType)
        .guiName("Output Taxa Names")
        .required(false)
        .description("Output the Hapid -> TaxaName list Map.")
        .build()

    override fun processData(input: DataSet?): DataSet? {
        //Load in the graph to check reference Ranges

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph


        var dbConnect = DBLoadingUtils.connection(false)
        val phg = PHGdbAccess(dbConnect)

        //Build the haplotype id maps needed
        val hapIdToRefRangeMap = getHapToRefRangeMap(graph)

        val hapIdToLengthMap = getHapIdToSequenceLength(graph)
        val refRangeToHapIdMap = getRefRangeToHapidMap(graph)
        val haplotypeListId = getHaplotypeListIdForGraph(graph, phg)

        val hapIdToTaxaListMap = if(outputTaxaNames()) {
            graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .map { Pair(it.id(), it.taxaList().map { taxon -> taxon.name }) }
                .toMap()
        }
        else {
            mapOf()
        }


        createAndWriteHapIdMaps(hapIdToRefRangeMap, hapIdToLengthMap, refRangeToHapIdMap, haplotypeListId, hapIdToTaxaListMap ,outputMapFile())


        phg.close()

        return null
    }

    fun createAndWriteHapIdMaps(
        hapIdToRefRangeMap: Map<Int, ReferenceRange>,
        hapIdToLengthMap: Map<Int, Int>,
        refRangeToHapIdMap: Map<Int, Map<Int, Int>>,
        haplotypeListId: Int,
        hapIdToTaxaListMap: Map<Int, List<String>>,
        outputFileName: String
    ) {
        val hapIdToRefRangeWrapper = hapIdToRefRangeMap.map {
            val refRange = it.value
            val refRangeWrapped = RefRangeWrapper(refRange.referenceName(), refRange.chromosome().name, refRange.start(), refRange.end(),refRange.id(), refRange.groupMethods())
            Pair(it.key, refRangeWrapped) }.toMap()
        //Wrap the maps into the Serializable object
        val maps = GraphIdMaps(hapIdToRefRangeWrapper, hapIdToLengthMap, refRangeToHapIdMap, haplotypeListId, hapIdToTaxaListMap)

        //Create the JacksonMapper and write out to file.
        val mapper = jacksonObjectMapper()
        mapper.writerWithDefaultPrettyPrinter().writeValue(File(outputFileName), maps)
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = CreateHapIdMapsPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "CreateHapIdMapsPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to pull out the required maps for use in ReadMapping."
    }

    /**
     * Output File Name.  JSON file will be written
     *
     * @return Output File Name
     */
    fun outputMapFile(): String {
        return outputMapFile.value()
    }

    /**
     * Set Output File Name. Output File Name.  JSON file
     * will be written
     *
     * @param value Output File Name
     *
     * @return this plugin
     */
    fun outputMapFile(value: String): CreateHapIdMapsPlugin {
        outputMapFile = PluginParameter<String>(outputMapFile, value)
        return this
    }

    /**
     * Output the Hapid -> TaxaName list Map.
     *
     * @return Output Taxa Names
     */
    fun outputTaxaNames(): Boolean {
        return outputTaxaNames.value()
    }

    /**
     * Set Output Taxa Names. Output the Hapid -> TaxaName
     * list Map.
     *
     * @param value Output Taxa Names
     *
     * @return this plugin
     */
    fun outputTaxaNames(value: Boolean): CreateHapIdMapsPlugin {
        outputTaxaNames = PluginParameter<Boolean>(outputTaxaNames, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(CreateHapIdMapsPlugin::class.java)
}