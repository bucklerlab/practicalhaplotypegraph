# PHG Testing
## Unit Tests
To run the PHG unit tests, execute the following Maven command:

```
mvn test
```

This can be done from a terminal within the repository directory, or through IntelliJ via double-tapping the Control key on your keyboard.

To run only the tests for a specific test class, provide its name as a `-Dtest` parameter, like so:

```
mvn test -Dtest=HaplotypeNodeTest
```

## Integration Tests
To run the PHG integration tests, execute the following Maven command:

```
mvn post-integration-test -DskipUnitTests
```

Similar to the unit tests, this may be done in a terminal or via IntelliJ's "Run Anything" prompt. Ensure that you have Docker running, as the integration tests require database access. After the tests run, whether they pass or fail, the Postgres container will be stopped and removed.

To run only a specific integration test, the process is similar to the unit tests except provide a value for `-Dit.test` instead of `-Dtest` like so:

```
mvn post-integration-test -Dit.test=SmallSeqIT -DskipUnitTests
```

If you want to inspect the database tables after the tests are run, use `integration-test` instead of `post-integration-test`, which will not trigger the Docker container to be torn down. You must remember to stop and remove the container yourself, however, otherwise on repeated runs you will encounter an error saying that the database port is already in use.

### Requirements
In order to run the integration tests, the software tools that the PHG uses must be available to system (*i.e.* in the `PATH`).

Install the following software tools:

- [AnchorWave](https://github.com/baoxingsong/AnchorWave)
- [HTSlib, SAMtools, and BCFtools](http://www.htslib.org/download/)
- [Burrow-Wheeler Aligner (BWA)](https://github.com/lh3/bwa)
- [Groovy](https://groovy-lang.org/install.html) which can be installed via [SDKMAN](https://sdkman.io/) or [Homebrew](https://brew.sh/)
- [Minimap2](https://github.com/lh3/minimap2)
- Postgres utilities: `psql`, `pg_dump`, `pg_restore`, `createdb`, and `deletedb`
    - Install via Homebrew: `brew install postgresql@11 && brew link postgresql@11`
- [Docker](https://docs.docker.com/engine/install/) - the Desktop version is suggested

Additionally, the following software must be downloaded and their directories moved to the `~/temp/phgSmallSeq` directory:

- [TASSEL 5](https://tassel.bitbucket.io/)
    - Get the most recent version from the [downloads page](https://bitbucket.org/tasseladmin/tassel-5-standalone/downloads/?tab=tags)
    - Its directory should be `~/temp/phgSmallSeq/tassel-5-standalone`
- [GATK](https://gatk.broadinstitute.org/hc/en-us)
    - Get the most recent version from the [releases page](https://github.com/broadinstitute/gatk/releases)
    - Its directory should be `~/temp/phgSmallSeq/gatk`
- [Liquibase](https://www.liquibase.org/)
    - Get the most 4.7.0 version (as a `.tar.gz` archive) from the [releases page](https://github.com/liquibase/liquibase/releases/tag/v4.7.0)
    - Its directory should be `~/temp/phgSmallSeq/liquibase-release`
    - Copy the `docker-buildFiles/liquibase/changelogs` directory within the PHG repository into `~/temp/phgSmallSeq/liquibase-release`

Lastly, ensure [Picard](https://broadinstitute.github.io/picard/)'s `picard.jar` is within the `docker/buildFiles` directory of your PHG repository.

## Miscellaneous Notes
### Maven
It is expected that you have already installed [Maven](https://maven.apache.org/), which can be done via the Homebrew command:

```
brew install maven
```

### AnchorWave and ARM/Apple M-series chips
If installing AnchorWave from source, you must:

1. Clone the AnchorWave repository
2. Use the ARM-specific CMake file: `mv CMakeLists_arm.txt CMakeLists.txt`
3. Get the `sse2neon.h` header [from here](https://github.com/DLTcollab/sse2neon) and copy it into the `src/` directory within the AnchorWave repository
