#!/bin/bash -x

#
# This script starts with fastq files and generates haplotype
# counts using a Haplotype Graph.  Those counts are then used
# with a HMM algorithm to find the best path through the
# haplotype graph for each taxon.  Those paths are written
# to text files containing a list of the haplotype node ids.
#

MOUNT_DIR=/phg
OUTPUT_DIR=${MOUNT_DIR}/outputDir/
DATA_DIR=${MOUNT_DIR}/data
INPUT_DIR=${MOUNT_DIR}/inputDir
REFERENCE_DIR=${INPUT_DIR}/reference
FASTQ_DIR=${DATA_DIR}
PANGENOME_DIR=${MOUNT_DIR}/pangenome

FASTQ_HAP_COUNT_DIR=${OUTPUT_DIR}/fastq_hap_count/
if [ ! -d "$FASTQ_HAP_COUNT_DIR" ]; then
   mkdir $FASTQ_HAP_COUNT_DIR
fi

HAP_COUNT_BEST_PATH_DIR=${OUTPUT_DIR}/hap_count_best_path
if [ ! -d "$HAP_COUNT_BEST_PATH_DIR" ]; then
   mkdir $HAP_COUNT_BEST_PATH_DIR
fi

if [ ! -d "$PANGENOME_DIR" ]; then
   mkdir $PANGENOME_DIR
fi

# Base name of the haplotype fasta (i.e. no .fa extension)
BASE_HAPLOTYPE_NAME=$1
# Filename of configFile for db connection - used in FastqToHapCountPlugin
CONFIGFILE=${MOUNT_DIR}/$2
CONFIGFILE_NAME=$2
# Haplotype Method used to create haplotypes in graph
HAPLOTYPE_METHOD=$3
# Reference FASTA file (i.e. Zea_mays.AGPv4.dna.toplevel.fa)
HAPLOTYPE_METHOD_FIND_PATH=$4
# Name of method used to create hap counts
HAP_COUNT_METHOD=$5
# Name of method to be used to create paths through the graph
PATH_METHOD=$6
KEY_FILE=$7
PATH_KEY_FILE=$8

# The name of the file containing the reference ranges to keep
REF_RANGE_FILE='';
if [ ! -z "${9}" ]
then
   REF_RANGE_FILE=${DATA_DIR}/${9}
fi

# Haplotype Fasta file
HAPLOTYPE_FASTA="${PANGENOME_DIR}/${BASE_HAPLOTYPE_NAME}.fa"
# Haplotype Fasta Index file
HAPLOTYPE_INDEX="${PANGENOME_DIR}/${BASE_HAPLOTYPE_NAME}.mmi"

# Set maximum Java heap size if defined in config file
fullXmx=""
memoryString="`grep -m 1 '^Xmx=' ${CONFIGFILE} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
fi

#Extract out the taxa names to filter out of the graph for genotyping.
TAXA_LIST_FLAG=''
filterTaxaList="`grep -m 1 '^filterTaxaList=' ${CONFIGFILE} | cut -d'=' -f2`"
if [ ${filterTaxaList} != "" ]
then
   TAXA_LIST_FLAG="-taxa ${filterTaxaList}"
fi

REF_RANGE_FILE_FLAG='';
if [ ! -z "$REF_RANGE_FILE" ]
then
   REF_RANGE_FILE_FLAG="-refRangeFile ${f}"
fi


#
# Use FastqToHapCountPlugin to generate counts of haplotypes from given
# fastq files and Haplotype Graph
#



time /tassel-5-standalone/run_pipeline.pl -debug ${fullXmx} -configParameters ${CONFIGFILE} -HaplotypeGraphBuilderPlugin -configFile ${CONFIGFILE} -methods ${HAPLOTYPE_METHOD} -includeVariantContexts false -includeSequences false  -endPlugin -FastqToMappingPlugin -minimap2IndexFile ${HAPLOTYPE_INDEX} -keyFile ${KEY_FILE} -fastqDir ${FASTQ_DIR}/ -methodName ${HAP_COUNT_METHOD} -methodDescription READ_MAPPING_DESCRIPTION -debugDir $OUTPUT_DIR -endPlugin


time /tassel-5-standalone/run_pipeline.pl -debug ${fullXmx} -configParameters ${CONFIGFILE} \
                                                            -HaplotypeGraphBuilderPlugin \
                                                              -configFile ${CONFIGFILE} \
                                                              -methods ${HAPLOTYPE_METHOD_FIND_PATH} \
                                                              -includeVariantContexts false \
                                                              -includeSequences false -endPlugin \
                                                            -BestHaplotypePathPlugin \
                                                              -keyFile ${PATH_KEY_FILE} \
                                                              -outDir ${HAP_COUNT_BEST_PATH_DIR} \
                                                              -readMethod ${HAP_COUNT_METHOD} \
                                                              -pathMethod ${PATH_METHOD} -endPlugin


