package net.maizegenetics.pangenome.hapcollapse;

import com.google.common.collect.*;
import net.maizegenetics.analysis.data.SortTaxaAlphabeticallyPlugin;
import net.maizegenetics.analysis.distance.IBSDistanceMatrix;
import net.maizegenetics.analysis.distance.KinshipPlugin;
import net.maizegenetics.analysis.distance.RemoveNaNFromDistanceMatrixPlugin;
import net.maizegenetics.analysis.numericaltransform.kNearestNeighbors;
import net.maizegenetics.analysis.tree.CreateTreePlugin;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.snp.*;
import net.maizegenetics.plugindef.*;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import net.maizegenetics.taxa.tree.TreeClusters;
import net.maizegenetics.taxa.tree.UPGMATree;
import net.maizegenetics.util.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Processes a multi-haplotype VCF file from one region, and identifies haplotype clusters to be
 * collapsed in future steps into a consensus haplotype.
 */
@Deprecated
public class FindHaplotypeClustersPlugin extends AbstractPlugin {
    public static enum CLUSTER_METHOD {coverage, upgma};
    private static final Logger myLogger = LogManager.getLogger(FindHaplotypeClustersPlugin.class);
    private PluginParameter<String> vcfDir = new PluginParameter.Builder<>("i", null, String.class).guiName("Target directory").inDir().required(false)
            .description("Input genotypes to generate haplotypes from. Usually best to use all available samples from a species. Accepts all file types supported by TASSEL5.").build();
    private PluginParameter<String> outFile = new PluginParameter.Builder<>("o", null, String.class).guiName("Donor dir/file basename").outDir().required(false)
            .description("Output file directory name, or new directory path; "
                    + "Directory will be created, if doesn't exist. Outfiles will be placed in the directory").build();
    private PluginParameter<Double> maxDistFromFounder = new PluginParameter.Builder<>("mxDiv", 0.01, Double.class).guiName("Max divergence from founder")
            .description("Maximum genetic divergence from founder haplotype to cluster sequences").build();
    private PluginParameter<Integer> maxNumberOfClusters = new PluginParameter.Builder<>("maxClust", 500, Integer.class)
            .description("The maximum number of clusters allowed by the tree cluster method. This value overrides mxDiv if it results in more clusters.").build();
    private PluginParameter<Double> seqErrorRate = new PluginParameter.Builder<>("seqErr", 0.01, Double.class).guiName("Sequencing error rate")
            .description("Error rate used to merge alleles call hets versus homozygous").build();
    private PluginParameter<Integer> minSiteForComp = new PluginParameter.Builder<>("minSites", 5, Integer.class).guiName("Min sites to cluster")
            .description("The minimum number of sites present in two taxa to compare genetic distance to evaluate similarity for clustering").build();
    private PluginParameter<Double> minTaxaCoverage = new PluginParameter.Builder<>("minTaxaCoverage", 0.5, Double.class).guiName("Min taxa coverage")
            .description("The minimum proportion of sites present in a taxa to go into clustering").build();
    private PluginParameter<Integer> minTaxaInGroup = new PluginParameter.Builder<>("minTaxa", 2, Integer.class).guiName("Min taxa to generate a haplotype")
            .description("Minimum number of taxa to generate a haplotype").build();
    private PluginParameter<Double> maxNan = new PluginParameter.Builder<>("maxNan", 0.5, Double.class)
            .description("Maximum proportion of Nans allowed in a row of the relationship matrix").build();
    private PluginParameter<GenomeSequence> referenceSequence = new PluginParameter.Builder<>("ref",null,GenomeSequence.class).guiName("Reference Genome Sequence").required(false)
            .description("Reference Genome Sequence to use to extract the fasta").build();
    private PluginParameter<String> sequenceOutDir = new PluginParameter.Builder<>("seqOutDir",null, String.class).guiName("Sequence Output Directory").outDir().required(false)
            .description("Output Directory For storing the sequence files").build();
    private PluginParameter<String> intervalFile = new PluginParameter.Builder<>("intervalFile",null,String.class).guiName("Interval File").inFile().required(false)
            .description("Interval File used to create the VCF file").build();
    private PluginParameter<CLUSTER_METHOD> clusterMethod = new PluginParameter.Builder<>("method", CLUSTER_METHOD.upgma, CLUSTER_METHOD.class)
            .guiName("Cluster Method")
            .description("The method used to cluster taxa. Coverage seeds the first cluster with the highest coverage taxon. UPGMA builds a UPGMA tree then cuts it at maxDistance.")
            .required(false)
            .build();

    private PluginParameter<Double> maxError = new PluginParameter.Builder<>("maxError",0.2,Double.class)
            .guiName("Maximum error")
            .description("Maximum error allowed to create a homozygous call.  If the error rate is above this value N or Major allele will be exported for that site")
            .required(false)
            .build();
    private PluginParameter<Boolean> replaceNsWithMajor = new PluginParameter.Builder<>("replaceNsWithMajor",true,Boolean.class)
            .guiName("Replace N calls with Major Allele")
            .description("Boolean flag to replace any N calls with a Major Homozygous Diploid Value")
            .required(false)
            .build();

    private PluginParameter<Boolean> useDepthForCalls = new PluginParameter.Builder<>("useDepthForCalls",false,Boolean.class)
            .guiName("Use Depth For Calls")
            .description("Boolean flag to have the clustering algorithm use depth information instead of allele counts")
            .required(false)
            .build();

    public FindHaplotypeClustersPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    /**
     * Method to create a consensus haplotype GenotypeTable object
     *
     * Method will now just create and return the GenotypeTable instead of doing additional work to create the fasta sequence.
     * @param input
     * @return
     */
    @Override
    public DataSet processData(DataSet input) {

        List<Datum> alignInList = input.getDataOfType(GenotypeTable.class);
        myLogger.info("Number of GenotypeTables: " + alignInList.size());
        List<Datum> result = alignInList.stream()
                .map(datum -> (GenotypeTable) datum.getData())
                .map(alignment -> {
                    //Sort the genotype table coming by taxa.  If the taxa ordering is inconsistent, the distance calculation is also inconsistent
                    DataSet sortedGT = new SortTaxaAlphabeticallyPlugin(null,false).performFunction(new DataSet(new Datum("inputAlignment",alignment,"Genotype Table"),null));
                    return (GenotypeTable)sortedGT.getDataOfType(GenotypeTable.class).get(0).getData();
                })
                .flatMap(alignment -> {
                    myLogger.info("NumberOfSites:" + alignment.numberOfSites());
                    //TODO filter by coverage
                    Multimap<Taxon, Taxon> clusters = null;
                    if (clusterMethod.value() == CLUSTER_METHOD.coverage) {
                        clusters = findHapClusterHighCoverage(alignment, minSiteForComp(), maxDistFromFounder());
                        myLogger.info("findhHapClusterHighCoverage num clusters returned: " + clusters.keySet().size());
                    }
                    else if (clusterMethod.value() == CLUSTER_METHOD.upgma) {
                        clusters = findHapClustersFromTree(alignment);
                        myLogger.info("findhHapClusterFromTree num clusters returned: " + clusters.keySet().size());
                    }

                    System.out.println(clusters);

                    List<GenotypeTable> genotypeTables = clusters.asMap().entrySet().stream()
                            .filter(taxonCluster -> taxonCluster.getValue().size() >= minTaxaInGroup())
                            .map(taxonCluster -> consensusGameteCalls(alignment, taxonCluster.getValue(),maxError(),useDepthForCalls(),replaceNsWithMajor()))
                            .collect(Collectors.toList());

                    myLogger.info("findHapClusterFromTree: number of GenotypeTables at end " + genotypeTables.size());
                    return genotypeTables.stream();
                })
                .map(genotypeTable -> new Datum(getDatumName(genotypeTable),genotypeTable,null))
                .collect(Collectors.toList());

        return new DataSet(result, this);
    }

    /**
     * Method to create a datum name for that given consensus haplotype which is unique.
     * @param genotypeTable
     * @return
     */
    private String getDatumName(GenotypeTable genotypeTable) {
        //Build up a name for each datum.
        //Do chr1_stPos100_firstTaxaName
        StringBuilder stringBuilder = new StringBuilder();

        Position firstPos = genotypeTable.positions().get(0);

        String firstTaxon = genotypeTable.taxaName(0);

        return stringBuilder.append("chr").append(firstPos.getChromosome().getName()).append("_stPos").append(firstPos.getPosition()).append("_").append(firstTaxon).toString();

    }


    /**
     * Clusters taxa using high coverage samples as the nucleation points.
     *
     * @param baseAlign   - Alignment without indel
     * @param minSites    - minimum number of polymorphic sites to calculate identity
     * @param maxDistance - maximum distance from each taxon to cluster
     * @return
     */
    protected static Multimap<Taxon, Taxon> findHapClusterHighCoverage(GenotypeTable baseAlign, int minSites, double maxDistance) {
//        GenotypeTable inAlign = GenotypeTableBuilder.getGenotypeCopyInstance(GenotypeTableBuilder.getInstanceMaskIndels(baseAlign));
        //TODO remove the invariant sites as well. We did this originally, but then we lose all the reference sites from the resulting vcf.
        GenotypeTable inAlign = baseAlign;
        //int positionRangeOfVarSites=inAlign.chromosomalPosition(inAlign.numberOfSites()-1)-inAlign.chromosomalPosition(0);
        TaxaList taxa = inAlign.taxa();
        if(baseAlign.positions().size()==0) {
            myLogger.debug("GenotypeTable does not have any positions.");
        }
        else {
            myLogger.debug("Alignment Locus:" + baseAlign.chromosome(0) + " StartPos:" + baseAlign.chromosomalPosition(0) + " Filtered taxa:" + inAlign.numberOfTaxa() + " Filtered sites:" + inAlign.numberOfSites());
        }

        //get the current ranking of the sites
        List<Tuple<Taxon, Integer>> presentRanking = createPresentRanking(inAlign, minSites);
        myLogger.debug("\tTaxa with sufficient coverage:" + presentRanking.size());
        Multimap<Taxon, Taxon> mergeSets = HashMultimap.create();
        Set<Taxon> unmatched = presentRanking.stream().map(Tuple::getX).collect(Collectors.toSet());
        for (Tuple<Taxon, Integer> e : presentRanking) {
            final Taxon taxon1 = e.getX();
            if (unmatched.contains(taxon1) == false) continue;//already included in another group
            unmatched.remove(taxon1);
            mergeSets.put(taxon1, taxon1);
            List<Taxon> hits = unmatched.stream()
                    .filter(taxon2 -> {
                        double[] dist = IBSDistanceMatrix.computeHetBitDistances(inAlign, taxa.indexOf(taxon1), taxa.indexOf(taxon2), minSites);
                        //double scaledDistance=dist[0]*inAlign.numberOfSites()/positionRangeOfVarSites;
//                        System.out.printf("%s\t%s\t%g\t%g\t%g\n",taxon1.getName(), taxon2.getName(),dist[0],scaledDistance,(maxDistance*inAlign.numberOfSites()/positionRangeOfVarSites));
                        return ((!Double.isNaN(dist[0])) && dist[0] < maxDistance);
                    })
                    .collect(Collectors.toList());
            mergeSets.putAll(taxon1, hits);
            unmatched.removeAll(hits);
        }
        myLogger.debug("Found clusters:");
        return mergeSets;
    }

    private Multimap<Taxon,Taxon> findHapClustersFromTree(GenotypeTable baseAlign) {

        Multimap<Taxon,Taxon> groupMap = HashMultimap.create();
        GenotypeTable inAlign = removeIndels(baseAlign);
        TaxaList taxa = inAlign.taxa();
        if(baseAlign.positions().size()==0) {
            myLogger.debug("GenotypeTable does not have any positions.");
        }
        else {
            myLogger.debug("Alignment Locus:" + baseAlign.chromosome(0) + " StartPos:" + baseAlign.chromosomalPosition(0) + " Filtered taxa:" + inAlign.numberOfTaxa() + " Filtered sites:" + inAlign.numberOfSites());
        }

        myLogger.debug(String.format("After removing indels range starting at chr %s, pos %d has %d sites and %d taxa%n", inAlign.chromosome(0).getName(), inAlign.chromosomalPosition(0), inAlign.numberOfSites(), inAlign.numberOfTaxa()));

        int ntaxa = baseAlign.numberOfTaxa();
        if (ntaxa == 1) groupMap.put(baseAlign.taxa().get(0), baseAlign.taxa().get(0));
        if (ntaxa < 2) return groupMap;

        DistanceMatrix dm = IBSDistanceMatrix.getInstance(baseAlign);
        UPGMATree myTree = new UPGMATree(dm);

        TreeClusters myClusters = new TreeClusters(myTree);
        int[] taxaGroups = myClusters.getGroups(maxDistFromFounder());

        OptionalInt optMaxGroup = Arrays.stream(taxaGroups).max();
        myLogger.debug("taxa groups : " + Arrays.stream(taxaGroups).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
        myLogger.debug("groups fit using dist = " + optMaxGroup.orElse(-1));
        if (optMaxGroup.isPresent() && optMaxGroup.getAsInt() >= maxNumberOfClusters()) {
            taxaGroups = myClusters.getGroups(maxNumberOfClusters());
            myLogger.debug("groups fit using maxClust = " + Arrays.stream(taxaGroups).max().orElse(-1));
        }

        //taxaGroups is an array with one element for each taxon in the tree
        //the ordering of taxa is arbitrary and corresponds to that returned by myTree.getExternalNode()
        //The method processData() which calls this method expects the cluster membership to be returned as a Multimap<Taxon,Taxon>
        //because that is the way it is supplied by the clustering method findHapClusterHighCoverage
        //The value multimap key Taxon is not used once the groups are formed.
        //As a result the only requirement is that each group have a unique Taxon key.
        
        //get a list of taxa in the same order as the tree external nodes
        List<Taxon> taxaList = Arrays.stream(taxaGroups)
                .mapToObj(t -> myTree.getExternalNode(t).getIdentifier())
                .collect(Collectors.toList());
        
        //choose the first taxon in each group as the key for that group
        Map<Integer,Taxon> groupTaxon = new HashMap<>();
        for (int t = 0; t < taxaGroups.length; t++) {
            if (!groupTaxon.containsKey(taxaGroups[t])) groupTaxon.put(taxaGroups[t], taxaList.get(t));
        }
        
        //create the multimap
        //groupTaxon.get(taxaGroups[t]) returns the Taxon that is the first member of the group containing Taxon t
        //and serves as the key for that group
        
        for (int t = 0; t < taxaGroups.length; t++) {
            Taxon taxon = myTree.getExternalNode(t).getIdentifier();
            groupMap.put(groupTaxon.get(taxaGroups[t]), taxon);
        }
        return groupMap;
    }

    private static DistanceMatrix imputeNaNs(DistanceMatrix dm, int numberOfNeighbors) {
        double[][] imputedMatrix = kNearestNeighbors.impute(dm.getDistances(), numberOfNeighbors, true, false);

        //a distance matrix must be symmetric
        //if imputedMatrix is not, make it symmetric by averaging values in upper and lower triangular matrix
        int n = imputedMatrix.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (imputedMatrix[i][j] != imputedMatrix[j][i]) {
                    double mean = (imputedMatrix[i][j] + imputedMatrix[j][i]) / 2.0;
                    imputedMatrix[i][j] = imputedMatrix[j][i] = mean;
                }
            }
        }

        return new DistanceMatrix(imputedMatrix, dm.getTaxaList(), dm.annotations());
    }

    /**
     * Remove indels from the GenotypeTable
     * @param baseAlign
     * @return
     */
    private static GenotypeTable removeIndels(GenotypeTable baseAlign) {
        GenotypeTableBuilder gtb = GenotypeTableBuilder.getSiteIncremental(baseAlign.taxa());
        PositionList positions = baseAlign.positions();
        for(int positionIndex = 0; positionIndex < positions.size(); positionIndex++) {
            byte[] positionForGenotypes = baseAlign.genotypeAllTaxa(positionIndex);
            if(isNonIndel(positionForGenotypes)) {
                gtb.addSite(positions.get(positionIndex),positionForGenotypes);
            }
        }
        return gtb.build();
    }

    /**
     * Check to see if it is not an indel. Originally this was checking for invariants.
     * Eventually we would like to have it check for invariants as well.
     * @param calls
     * @return
     */
    private static boolean isNonIndel(byte[] calls) {
        boolean isNonIndel = true;
        if(calls.length == 0) {
            return false;
        }
        byte firstCall = calls[0];
        for (int i = 1; i < calls.length; i++) {
            if(calls[i] != firstCall) {
                //TODO remove invariant sites as well
                isNonIndel = true;
            }

            if(calls[i] == NucleotideAlignmentConstants.INSERT_DIPLOID_ALLELE || calls[i] == NucleotideAlignmentConstants.GAP_DIPLOID_ALLELE) {
                return false;
            }
        }

        return isNonIndel;
    }

    /**
     * Method to compute the list of taxa ordered by how many calls are missing.  Also filters the taxa to be required to have at least minSites
     *
     * @param inAlign
     * @param minSites
     * @return
     */
    private static List<Tuple<Taxon, Integer>> createPresentRanking(GenotypeTable inAlign, int minSites) {
        List<Tuple<Taxon, Integer>> taxaCoverageList = IntStream.range(0, inAlign.numberOfTaxa())
                .mapToObj(taxaIndex -> new Tuple<>(inAlign.taxa().get(taxaIndex), inAlign.totalGametesNonMissingForTaxon(taxaIndex)))
                .filter(taxonSites -> taxonSites.getY() > minSites)
                .collect(Collectors.toList());
        taxaCoverageList.sort(Comparator.comparing(Tuple::getY, Comparator.reverseOrder()));
        myLogger.debug("found present ranking");
        return taxaCoverageList;
    }

    /**
     * Method to create consensus Gamete Calls for a collection of taxa
     * This method will loop through each site and attempt to create a consensus call.  If depth is requested, it will export the summed depth for the site
     * @param a
     * @param taxa
     * @param maxError
     * @param useDepth
     * @param setToMajor
     * @return
     */
    private GenotypeTable consensusGameteCalls(GenotypeTable a, Collection<Taxon> taxa, double maxError, boolean useDepth, boolean setToMajor) {
        GenotypeTableBuilder gtb = GenotypeTableBuilder.getTaxaIncremental(a.positions());
        Taxon consensusTaxon = new Taxon(createConsensusTaxonName(taxa));
        List<Integer> taxaIndicies = taxa.stream().map(taxon -> a.taxa().indexOf(taxon)).collect(Collectors.toList());

        byte[] calls = new byte[a.positions().size()];
        int[][] depths = new int[6][a.positions().size()];

        for(int site = 0; site < a.positions().size(); site++) {
            CallAndAlleleDepth callsAndDepths = consensusGameteCallAndCounts(a, taxaIndicies, site, maxError, useDepth, setToMajor);

            calls[site] = callsAndDepths.call;
            // For now, depth is not supported.  Code below is maintained in the event we reverse
            // this decision and add code to support depth.
//            if(useDepth) {
//                int[] currentSiteDepths = callsAndDepths.getDepths();
//                depths[0][site] = currentSiteDepths[0];
//                depths[1][site] = currentSiteDepths[1];
//                depths[2][site] = currentSiteDepths[2];
//                depths[3][site] = currentSiteDepths[3];
//                depths[4][site] = currentSiteDepths[4];
//                depths[5][site] = currentSiteDepths[5];
//            }
        }
        

        gtb.addTaxon(consensusTaxon,calls);
//        if(useDepth) {
//            gtb.addTaxon(consensusTaxon,depths,calls);
//        }
//        else {
//            gtb.addTaxon(consensusTaxon,calls);
//        }

        return gtb.build();
    }

    /**
     * Method that will create a consensus Gamete Call for a given site.  If useDepth = true, depth will be used.  If not, the genotype calls will be counted
     * After depth or calls are counted, find the highest count and divide the rest of the counts by the total number to get an error rate.
     *
     * If this errorRate is below maxError, select the top counted gamete, if not if setToMajor is true, return the global major genotype, else return N
     * @param gt
     * @param taxaIndicies
     * @param site
     * @param maxError
     * @param useDepth
     * @param setToMajor
     * @return
     */
    private CallAndAlleleDepth consensusGameteCallAndCounts(GenotypeTable gt, List<Integer> taxaIndicies, int site, double maxError, boolean useDepth, boolean setToMajor) {
        byte mergedCall = GenotypeTable.UNKNOWN_DIPLOID_ALLELE;

        int[] countArray = new int[6];//This holds the counts/depths for each allele encoded by the byte value
        for (int taxonIndex = 0; taxonIndex < taxaIndicies.size(); taxonIndex++) {
            //Count one for each allele
            byte[] alleles = gt.genotypeArray(taxaIndicies.get(taxonIndex),site);
            if(alleles[0]!=GenotypeTable.UNKNOWN_ALLELE) {
                countArray[alleles[0]]+=1;
            }
            if(alleles[1]!=GenotypeTable.UNKNOWN_ALLELE) {
                countArray[alleles[1]]+=1;
            }
            // Not supporting depth for consensus when using the variantId as allele-call method
            // Code is maintained but commented out to allow its return when/if depths are supported
//            if(useDepth) {
//                //Sum the depths as if they were counts
//                int[] alleleDepths = gt.depthForAlleles(taxaIndicies.get(taxonIndex),site);
//
//                countArray[0] += alleleDepths[0];
//                countArray[1] += alleleDepths[1];
//                countArray[2] += alleleDepths[2];
//                countArray[3] += alleleDepths[3];
//                countArray[4] += alleleDepths[4];
//                countArray[5] += alleleDepths[5];
//
//            }
//            else {
//                //Count one for each allele
//                byte[] alleles = gt.genotypeArray(taxaIndicies.get(taxonIndex),site);
//                if(alleles[0]!=GenotypeTable.UNKNOWN_ALLELE) {
//                    countArray[alleles[0]]+=1;
//                }
//                if(alleles[1]!=GenotypeTable.UNKNOWN_ALLELE) {
//                    countArray[alleles[1]]+=1;
//                }
//            }
        }

        byte majorAllele = GenotypeTable.UNKNOWN_ALLELE;

        int majorCount = 0;

        //Do a quick pass over the counts(6 elements always) and get the total and the max
        int totalCnt = 0;
        for(int alleleIndex = 0; alleleIndex < countArray.length; alleleIndex++) {
            totalCnt+=countArray[alleleIndex];
            if(countArray[alleleIndex]>majorCount) {
                majorCount = countArray[alleleIndex];
                majorAllele = (byte)alleleIndex;
            }

        }

        //Compute the error rate
        double errorRate = 1.0 - ((double)majorCount/(double)totalCnt);
        //If the error rate is below user specified maxError return the major diploid
        if(errorRate < maxError) {
            byte majorDiploid= GenotypeTableUtils.getUnphasedDiploidValue(majorAllele,majorAllele);
            mergedCall = majorDiploid;
        }

        //If mergedCall is still unknown and setToMajor is true, we set merged call to the global major allele
        if(mergedCall==GenotypeTable.UNKNOWN_DIPLOID_ALLELE && setToMajor) {
            byte globalMajorAllele = gt.majorAllele(site);
            mergedCall = GenotypeTableUtils.getUnphasedDiploidValue(globalMajorAllele,globalMajorAllele);
        }

        return new CallAndAlleleDepth(mergedCall,countArray);

    }

    /**
     * Simple private class to hold the primitive byte call and depth array.  Would have used a Multimap, but that will not use the primitives
     */
    private class CallAndAlleleDepth {
        private final byte call;
        private final int[] depths;

        private CallAndAlleleDepth() {
            throw new IllegalStateException("Cannot initialize CallAndAlleleDepth with Default Constructor");
        }

        public CallAndAlleleDepth(byte call, int[] depths) {
            this.call = call;
            this.depths = depths;
        }

        public byte getCall() {
            return call;
        }

        public int[] getDepths() {
            return depths;
        }
    }

    /**
     * Method to create a consensusTaxonName separated by ':' characters\
     * TODO We will need to alter this when we do hybrids
     *
     * @param taxa
     * @return
     */
    private String createConsensusTaxonName(Collection<Taxon> taxa) {
        return taxa.stream().map(taxon -> taxon.getName()+"_0").sorted().collect(Collectors.joining(":"));
    }

    @Override
    public String pluginUserManualURL() {
        //TODO
        return "https://bitbucket.org/tasseladmin/tassel-5-source/wiki/UserManual/Kinship/Missing";
    }

    @Override
    public ImageIcon getIcon() {
        URL imageURL = KinshipPlugin.class.getResource("/net/maizegenetics/analysis/images/missing.gif");
        if (imageURL == null) {
            return null;
        } else {
            return new ImageIcon(imageURL);
        }
    }

    @Override
    public String getButtonName() {
        return "PHG Cluster Haplotypes";
    }

    @Override
    public String getToolTipText() {
        return "PHG Cluster Haplotypes";
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
     public static void main(String[] args) {
         GeneratePluginCode.generate(FindHaplotypeClustersPlugin.class);
     }

    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * Input genotypes to generate haplotypes from. Usually
     * best to use all available samples from a species. Accepts
     * all file types supported by TASSEL5.
     *
     * @return Target directory
     */
    public String vcfDir() {
        return vcfDir.value();
    }

    /**
     * Set Target directory. Input genotypes to generate haplotypes
     * from. Usually best to use all available samples from
     * a species. Accepts all file types supported by TASSEL5.
     *
     * @param value Target directory
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin vcfDir(String value) {
        vcfDir = new PluginParameter<>(vcfDir, value);
        return this;
    }

    /**
     * Output file directory name, or new directory path;
     * Directory will be created, if doesn't exist. Outfiles
     * will be placed in the directory
     *
     * @return Donor dir/file basename
     */
    public String outFile() {
        return outFile.value();
    }

    /**
     * Set Donor dir/file basename. Output file directory
     * name, or new directory path; Directory will be created,
     * if doesn't exist. Outfiles will be placed in the directory
     *
     * @param value Donor dir/file basename
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin outFile(String value) {
        outFile = new PluginParameter<>(outFile, value);
        return this;
    }

    /**
     * Maximum genetic divergence from founder haplotype to
     * cluster sequences
     *
     * @return Max divergence from founder
     */
    public Double maxDistFromFounder() {
        return maxDistFromFounder.value();
    }

    /**
     * Set Max divergence from founder. Maximum genetic divergence
     * from founder haplotype to cluster sequences
     *
     * @param value Max divergence from founder
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin maxDistFromFounder(Double value) {
        maxDistFromFounder = new PluginParameter<>(maxDistFromFounder, value);
        return this;
    }

    /**
     * The maximum number of clusters allowed. This value
     * overrides mxDiv if it results in more clusters.
     *
     * @return Max Clust
     */
    public Integer maxNumberOfClusters() {
        return maxNumberOfClusters.value();
    }

    /**
     * Set Max Clust. The maximum number of clusters allowed.
     * This value overrides mxDiv if it results in more clusters.
     *
     * @param value Max Clust
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin maxNumberOfClusters(Integer value) {
        maxNumberOfClusters = new PluginParameter<>(maxNumberOfClusters, value);
        return this;
    }

    /**
     * Error rate used to merge alleles call hets versus homozygous
     *
     * @return Sequencing error rate
     */
    public Double seqErrorRate() {
        return seqErrorRate.value();
    }

    /**
     * Set Sequencing error rate. Error rate used to merge
     * alleles call hets versus homozygous
     *
     * @param value Sequencing error rate
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin seqErrorRate(Double value) {
        seqErrorRate = new PluginParameter<>(seqErrorRate, value);
        return this;
    }

    /**
     * The minimum number of sites present in two taxa to
     * compare genetic distance to evaluate similarity for
     * clustering
     *
     * @return Min sites to cluster
     */
    public Integer minSiteForComp() {
        return minSiteForComp.value();
    }

    /**
     * Set Min sites to cluster. The minimum number of sites
     * present in two taxa to compare genetic distance to
     * evaluate similarity for clustering
     *
     * @param value Min sites to cluster
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin minSiteForComp(Integer value) {
        minSiteForComp = new PluginParameter<>(minSiteForComp, value);
        return this;
    }

    /**
     * The minimum proportion of sites present in a taxa to
     * go into clustering
     *
     * @return Min taxa coverage
     */
    public Double minTaxaCoverage() {
        return minTaxaCoverage.value();
    }

    /**
     * Set Min taxa coverage. The minimum proportion of sites
     * present in a taxa to go into clustering
     *
     * @param value Min taxa coverage
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin minTaxaCoverage(Double value) {
        minTaxaCoverage = new PluginParameter<>(minTaxaCoverage, value);
        return this;
    }

    /**
     * Minimum number of taxa to generate a haplotype
     *
     * @return Min taxa to generate a haplotype
     */
    public Integer minTaxaInGroup() {
        return minTaxaInGroup.value();
    }

    /**
     * Set Min taxa to generate a haplotype. Minimum number
     * of taxa to generate a haplotype
     *
     * @param value Min taxa to generate a haplotype
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin minTaxaInGroup(Integer value) {
        minTaxaInGroup = new PluginParameter<>(minTaxaInGroup, value);
        return this;
    }

    /**
     * Maximum proportion of Nans allowed in a row of the
     * relationship matrix
     *
     * @return Max Nan
     */
    public Double maxNan() {
        return maxNan.value();
    }

    /**
     * Set Max Nan. Maximum proportion of Nans allowed in
     * a row of the relationship matrix
     *
     * @param value Max Nan
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin maxNan(Double value) {
        maxNan = new PluginParameter<>(maxNan, value);
        return this;
    }

    /**
     * Reference Genome Sequence to use to extract the fasta
     *
     * @return Reference Genome Sequence
     */
    public GenomeSequence referenceSequence() {
        return referenceSequence.value();
    }

    /**
     * Set Reference Genome Sequence. Reference Genome Sequence
     * to use to extract the fasta
     *
     * @param value Reference Genome Sequence
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin referenceSequence(GenomeSequence value) {
        referenceSequence = new PluginParameter<>(referenceSequence, value);
        return this;
    }

    /**
     * Output Directory For storing the sequence files
     *
     * @return Sequence Output Directory
     */
    public String sequenceOutDir() {
        return sequenceOutDir.value();
    }

    /**
     * Set Sequence Output Directory. Output Directory For
     * storing the sequence files
     *
     * @param value Sequence Output Directory
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin sequenceOutDir(String value) {
        sequenceOutDir = new PluginParameter<>(sequenceOutDir, value);
        return this;
    }

    /**
     * Interval File used to create the VCF file
     *
     * @return Interval File
     */
    public String intervalFile() {
        return intervalFile.value();
    }

    /**
     * Set Interval File. Interval File used to create the
     * VCF file
     *
     * @param value Interval File
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin intervalFile(String value) {
        intervalFile = new PluginParameter<>(intervalFile, value);
        return this;
    }

    /**
     * The method used to cluster taxa. Coverage seeds the
     * first cluster with the highest coverage taxon. UPGMA
     * builds a UPGMA tree then cuts it at maxDistance.
     *
     * @return Cluster Method
     */
    public CLUSTER_METHOD clusterMethod() {
        return clusterMethod.value();
    }

    /**
     * Set Cluster Method. The method used to cluster taxa.
     * Coverage seeds the first cluster with the highest coverage
     * taxon. UPGMA builds a UPGMA tree then cuts it at maxDistance.
     *
     * @param value Cluster Method
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin clusterMethod(CLUSTER_METHOD value) {
        clusterMethod = new PluginParameter<>(clusterMethod, value);
        return this;
    }

    /**
     * Maximum error allowed to create a homozygous call.
     *  If the error rate is above this value N or Major allele
     * will be exported for that site
     *
     * @return Maximum error
     */
    public Double maxError() {
        return maxError.value();
    }

    /**
     * Set Maximum error. Maximum error allowed to create
     * a homozygous call.  If the error rate is above this
     * value N or Major allele will be exported for that site
     *
     * @param value Maximum error
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin maxError(Double value) {
        maxError = new PluginParameter<>(maxError, value);
        return this;
    }

    /**
     * Boolean flag to replace any N calls with a Major Homozygous
     * Diploid Value
     *
     * @return Replace N calls with Major Allele
     */
    public Boolean replaceNsWithMajor() {
        return replaceNsWithMajor.value();
    }

    /**
     * Set Replace N calls with Major Allele. Boolean flag
     * to replace any N calls with a Major Homozygous Diploid
     * Value
     *
     * @param value Replace N calls with Major Allele
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin replaceNsWithMajor(Boolean value) {
        replaceNsWithMajor = new PluginParameter<>(replaceNsWithMajor, value);
        return this;
    }

    /**
     * Boolean flag to have the clustering algorithm use depth
     * information instead of allele counts
     *
     * @return Use Depth For Calls
     */
    public Boolean useDepthForCalls() {
        return useDepthForCalls.value();
    }

    /**
     * Set Use Depth For Calls. Boolean flag to have the clustering
     * algorithm use depth information instead of allele counts
     *
     * @param value Use Depth For Calls
     *
     * @return this plugin
     */
    public FindHaplotypeClustersPlugin useDepthForCalls(Boolean value) {
        useDepthForCalls = new PluginParameter<>(useDepthForCalls, value);
        return this;
    }

}
