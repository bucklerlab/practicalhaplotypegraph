package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.WriteFastaFromGraphPlugin
import net.maizegenetics.pangenome.hapCalling.*
import net.maizegenetics.pangenome.liquibase.defineDefaultLiquibaseDir
import net.maizegenetics.plugindef.*
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.swing.ImageIcon

/*
* This plugin runs all the steps required to impute variants from sequence or SNPs.
* It checks for the existence of a pangenome index (.mmi) and creates it if necessary. It then creates a read mapping
* and finds paths from that read mapping.
* */
class ImputePipelinePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(ImputePipelinePlugin::class.java)

    enum class IMPUTE_TASK { config, pangenome, map, path, diploidPath, pathToVCF, diploidPathToVCF }
    enum class INPUT_TYPE { fastq, sam, vcf }

    private var imputeTarget = PluginParameter.Builder("imputeTarget", IMPUTE_TASK.path, IMPUTE_TASK::class.java)
            .description("Specifies the target or desired endpoint when running the plugin. For any given endpoint, " +
                    "the plugin will check that all required previous step have been completed and run those as needed.")
            .range(IMPUTE_TASK.values())
            .build()

    private var inputType = PluginParameter.Builder("inputType", null, INPUT_TYPE::class.java)
            .description("The type of data to be used for imputation: options are fastq, sam, or vcf.")
            .range(INPUT_TYPE.values())
            .build()

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("The config file name used when writing the default config file. When imputeTarget is not config, the -configParameters flag should be used to set the configFile")
            //.inFile() // this is not an input file, it is a config file that is written by the plugin
            .build()

    private var pangenomeHaplotypeMethod = PluginParameter.Builder("pangenomeHaplotypeMethod", null, String::class.java)
            .description("The haplotype method used to create the pangenome.fa and pangenome.mmi.")
            .build()

    private var pathHaplotypeMethod = PluginParameter.Builder("pathHaplotypeMethod", null, String::class.java)
            .description("The pathHaplotypeMethod defines the set of haplotypes to be used for pathing. It must be the same as the pangenomeHaplotypeMethod or a subset of it.")
            .build()

    private var pangenomeDir = PluginParameter.Builder("pangenomeDir", null, String::class.java)
            .description("The directory of the pangenome file and index.")
            .inDir()
            .build()

    private var pangenomeIndexName = PluginParameter.Builder("pangenomeIndexName", null, String::class.java)
            .description("WARNING: By default the plugin will construct pangenome file and index names from the pangenomeHaplotypeMethod. " +
                    "Only use this parameter to over-ride that name with a different one. Be sure the pangenomeHaplotypeMethod matches the index.")
            .build()

    //minimap parameters -k kmer length [15], -w window size [2/3 k], -I number of bases to load [4G], [] is the minimap2 default
    private var indexKmerLength = PluginParameter.Builder("indexKmerLength", 21, Int::class.javaObjectType)
            .description("Minimap2 index parameter k, the kmer length of the minimizers, which is used to index the pangenome.")
            .build()

    private var indexWindowSize = PluginParameter.Builder("indexWindowSize", 11, Int::class.javaObjectType)
            .description("Minimap2 index parameter w, the minimizer window size, which is used to index the pangenome.")
            .build()

    private var indexNumberBases = PluginParameter.Builder("indexNumberBases", "90G", String::class.java)
            .description("Minimap2 index parameter I, the maximum number of bases loaded into memory, which is used to index the pangenome." +
                    " This must be large enough to hold the entire pangenome in memory.")
            .build()

    private var minimapLocation = PluginParameter.Builder("minimapLocation", "minimap2", String::class.java)
            .guiName("Location of Minimap2 Executable")
            .description("Location of Minimap2 on file system.  This defaults to use minimap2 if it is on the PATH environment variable.")
            .build()

    private var readMethod = PluginParameter.Builder("readMethod", null, String::class.java)
            .description("The read method name to used for the read mapping data stored in the DB.")
            .required(true)
            .build()

    private var readMethodDescription = PluginParameter.Builder("readMethodDescription", null, String::class.java)
            .description("An additional description that will be stored with the read method name, if desired.")
            .build()

    private var outVcfFile = PluginParameter.Builder("outVcfFile", null, String::class.java)
            .description("The name of the file to be output by the PathsToVCFPlugin.")
            .build()

    //liquibase parameters
    private var myForceDBUpdate = PluginParameter.Builder("forceDBUpdate",false,Boolean::class.javaObjectType)
            .description("Parameter to force the DB to update.  If this is set to true and the DB is incompatible with the current PHG code, this will automatically update the DB if it can.")
            .required(false)
            .build()

    private var liquibaseDir = PluginParameter.Builder("liquibaseDir", null, String::class.java)
        .description("Liquibase release directory.  Holds liquibase executable and change logs.")
        .required(false)
        .inDir()
        .build()

    private var liquibaseOutputDir = PluginParameter.Builder("liquibaseOutdir", null, String::class.java)
            .description("The directory to which liquibase output files will be written.")
            .inDir()
            .required(true)
            .build()

    private var skipLiquibaseCheck = PluginParameter.Builder("skipLiquibaseCheck", false, Boolean::class.javaObjectType)
            .description("Should the liquibase check be skipped when liquibase is missing? " +
                    "Only set to true if the PHG DB version is known to match the software version.")
            .build()

    private var localGVCFFolder = PluginParameter.Builder("localGVCFFolder", null, String::class.java)
        .description("Folder where gvcfs will be downloaded and stored")
        .guiName("Local GVCF Folder")
        .inDir()
        .required(true)
        .build()

    //TODO add overwrite parameters
    override fun postProcessParameters() {
        //the following parameters are required unless imputeTarget=config
        //inputType, pangenomeHaplotypeMethod
        if (imputeTarget() != IMPUTE_TASK.config) {
            if (inputType() == null) throw IllegalArgumentException("inputType is null. A value for inputType must be supplied to run the impute pipeline.")
            if (pangenomeHaplotypeMethod() == null) throw IllegalArgumentException("pangenomeHaplotypeMethod is null. A value for pangenomeHaplotypeMethod must be supplied to run the impute pipeline.")
            //if input type is fastq, pangenomeDir is required
            if (inputType() == INPUT_TYPE.fastq && pangenomeDir() == null) throw IllegalArgumentException("pangenomeDir is null. A value must be supplied for pangenomeDir when the input type is fastq.")
        }

        // if no liquibase directory is specified, set the value from
        // defineDefaultLiquibaseDir()
        if (liquibaseDir.value() == null) {
            // This may still return null if liquibase is not found in either of the 2 default locations
            liquibaseDir(defineDefaultLiquibaseDir()) // function defined in LiquibaseUpdatePlugin
        }
    }

    override fun processData(input: DataSet?): DataSet? {
        //Check if liquibase needs to be updated.
        //check if liquibaseDir exists.
        myLogger.info("Checking if Liquibase can be run.")
        if (liquibaseDir.value() == null || !Files.exists(Paths.get(liquibaseDir()))) {
            if (skipLiquibaseCheck()) {
                myLogger.warn("Liquibase changelogs not present. Liquibase check was skipped.")
                runImputationPipeline()
            }
            else throw IllegalArgumentException("Liquibase not found: You must run liquibase manually or from the phg_liquibase docker.")

            return null
        }

        //First, run LiquibaseCommandPlugin with 'status' as the command.
        val status = runLiquibaseCommand("status",liquibaseOutputDir(),liquibaseDir())
        if(status == "UP_TO_DATE") {
            // If it returns back 'UP_TO_DATE', execute the pipeline.
            myLogger.info("PHG DB is up to date.  Proceeding with Populating the PHG DB.")
            runImputationPipeline()
        }
        else if(status.startsWith("NEEDS_UPDATING")) {
            // If it returns back NEEDS_UPDATING,
            // if 'force' command
            if(myForceDBUpdate.value()) {
                // run LiquibaseCommandPlugin with an 'update' command and log that we are updating.
                runLiquibaseCommand("update",liquibaseOutputDir(),liquibaseDir())
                //Now okay to run the pipeline
                runImputationPipeline()
            }
            else  {
                throw IllegalStateException("The Current PHG System is incompatible with the provided PHG Database.\n" +
                        "To Run with the existing DB, please use PHG/docker version: ${status.substringAfter("NEEDS_UPDATING:")}\n" +
                        "If you would like the PHG DB to be automatically updated please rerun this Plugin with -forceDBUpdate\n")
            }
        }
        else {
            myLogger.error("Unknown status from liquibase.  Status Message: ${status}")
            throw IllegalStateException("Unknown status from liquibase.  Status Message: ${status}")
        }

        return null;
    }

    // TODO: Finish updating to Kmer-based mapping
    private fun runImputationPipeline() {
        val myInputType = inputType()
        val myImputeTarget = imputeTarget()
        if (imputeTarget() == IMPUTE_TASK.config) {
            //write a config file with default parameter values
            writeDefaultConfigFile()

        } else if (imputeTarget() == IMPUTE_TASK.pangenome) {
            //assume here that the user would not be running the pangenome task if the pangenome already exists
            //as a result it is not necessary to check whether it already exists
            //if it does it exist it will be overwritten
            val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
            graphBuilder.setConfigParameters()
            val graphDataSet = graphBuilder
                    .methods(pangenomeHaplotypeMethod())
                    .performFunction(null)
            createPangenome(graphDataSet)
            //createKmerMap()
        } else {
            if (myInputType == INPUT_TYPE.fastq) {
                //create the pangenome and map reads use this PHG
                val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
                graphBuilder.setConfigParameters()
                val graphDataSet = graphBuilder
                        .methods(pangenomeHaplotypeMethod())
                        .performFunction(null)

                //does the pangenome index exist
                if (!verifyPangenomeIndex()) {
                    //if not create and index a pangenome
                    createPangenome(graphDataSet)
                    indexPangenome()
                }

                if (myImputeTarget == IMPUTE_TASK.map ||
                        myImputeTarget == IMPUTE_TASK.path ||
                        myImputeTarget == IMPUTE_TASK.diploidPath ||
                        myImputeTarget == IMPUTE_TASK.pathToVCF ||
                        myImputeTarget == IMPUTE_TASK.diploidPathToVCF) {
                    val index = if (pangenomeIndexName() == null) "${pangenomeDir()}/${pangenomeIndexNameFromMethod()}"
                    else "${pangenomeDir()}/${pangenomeIndexName()}"
                    val mappingPlugin = FastqToMappingPlugin(null, false)
                    mappingPlugin.setConfigParameters()
                    val methodDesc = readMethodDescription()
                    if (methodDesc != null) mappingPlugin.methodDescription(methodDesc)
                    mappingPlugin.methodName(readMethod())
                            .indexFile(index!!)
                            .performFunction(graphDataSet)
                    ////create the kmerMap if it does not exist
                    //if (!File(kmerMapPath()).exists()) createKmerMap()
                    //val mappingPlugin = KmerReadMapperPlugin()
                    //mappingPlugin.setConfigParameters()
                    //val methodDesc = readMethodDescription()
                    //if (methodDesc != null) mappingPlugin.methodDescription(methodDesc)
                    //mappingPlugin.readMethod(readMethod())
                    //    .kmerMapFilePath(kmerMapPath())
                    //    .performFunction(null)
                }
            } else if (myInputType == INPUT_TYPE.sam) {
                //build a PHG using the pangenomeHaplotypeMethod
                val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
                graphBuilder.setConfigParameters()
                val graphDataSet = graphBuilder
                        .methods(pangenomeHaplotypeMethod())
                        .localGVCFFolder(localGVCFFolder())
                        .performFunction(null)

                if (myImputeTarget == IMPUTE_TASK.map ||
                        myImputeTarget == IMPUTE_TASK.path ||
                        myImputeTarget == IMPUTE_TASK.diploidPath ||
                        myImputeTarget == IMPUTE_TASK.pathToVCF ||
                        myImputeTarget == IMPUTE_TASK.diploidPathToVCF) {

                    //map reads
                    val mappingPlugin = SAMToMappingPlugin(null, false)
                    mappingPlugin.setConfigParameters()
                    val methodDesc = readMethodDescription()
                    if (methodDesc != null) mappingPlugin.methodDescription(methodDesc)
                    mappingPlugin.methodName(readMethod())
                            .performFunction(graphDataSet)

                }

            } else if (myInputType == INPUT_TYPE.vcf) {
                //set the index output file name to indexFile, which is the parameter used by for that file
                //index the snp positions to haplotypes
                val indexer = IndexHaplotypesBySNPPlugin(null, false)
                indexer.setConfigParameters()

                val cacheValue = ParameterCache.value("indexFile")
                require(cacheValue.isPresent) {"parameter indexFile missing from config"}
                indexer.outputIndexFile(cacheValue.get())

                indexer.methods(pangenomeHaplotypeMethod())
                indexer.performFunction(null)

                //map reads
                //SNPToReadMappingPlugin does not automatically write a keyfile
                //that needs to supplied using outputPathKeyFile(value: String)
                val readMapper = SNPToReadMappingPlugin(null, false)
                readMapper.setConfigParameters()
                readMapper.methodName(readMethod())
                readMapper.outputPathKeyFile(pathKeyFileName())
                val methodDesc = readMethodDescription()
                if (methodDesc != null) readMapper.methodDescription(methodDesc)
                readMapper.performFunction(null)
            }

            //find paths: after reads are mapped, path finding is the same for all input types
            if (myImputeTarget == IMPUTE_TASK.path ||
                    myImputeTarget == IMPUTE_TASK.pathToVCF) findHaploidPaths()

            if (myImputeTarget == IMPUTE_TASK.diploidPath ||
                    myImputeTarget == IMPUTE_TASK.diploidPathToVCF) findDiploidPaths()

            //write vcf
            if (myImputeTarget == IMPUTE_TASK.pathToVCF ||
                    myImputeTarget == IMPUTE_TASK.diploidPathToVCF) writePathsToVCF()
        }
    }

    /**
     * Runs BestHaplotypePathPlugin for all records in the pathKeyFile
     */
    private fun findHaploidPaths() {
        //find haploid paths
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods(pathHaplotypeMethod())
                .includeSequences(false)
                .performFunction(null)

        //this method has to get the path key file name from key file name, which is used by read mapping
        val pathFinder = BestHaplotypePathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(pathKeyFileName())
                .readMethodName(readMethod())
                .performFunction(graphDataSetForPath)
    }

    /**
     * Runs DiploidPathPlugin for all entries in the pathKeyFile
     */
    private fun findDiploidPaths() {
        //find diploid paths
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods(pathHaplotypeMethod())
                .includeSequences(false)
                .performFunction(null)

        //this method has to get the path key file name from key file name, which is used by read mapping
        val pathFinder = DiploidPathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(pathKeyFileName())
                .readMethodName(readMethod())
                .performFunction(graphDataSetForPath)
    }

    /**
     * Create a VCF for all of paths in the database that have method = pathMethod()
     */
    private fun writePathsToVCF() {
        //the pathMethod is the method name for the paths. The pathHaplotypeMethod is the haplotypeMethod used for path finding.
        val pathMethod = ParameterCache.value("pathMethod").get()
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods(pathHaplotypeMethod())
                .includeSequences(false)
                .includeVariantContexts(true)
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
                .pathMethodName(pathMethod)
                .performFunction(graphDataSetForPath)
        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(outVcfFile())
                .performFunction(paths)

    }

    /**
     * Assigns a name to the pangenome fasta file
     */
    private fun pangenomeNameFromMethod() : String {
        return "pangenome_${pangenomeHaplotypeMethod()}.fa"
    }

    private fun kmerMapPath() : String {
        val filename = pangenomeIndexName() ?: "kmerMap_${pangenomeHaplotypeMethod()}.txt.gz"
        return "${pangenomeDir()}/$filename"
    }

    /**
     * Assigns a name to the pangenome index file
     */
    private fun pangenomeIndexNameFromMethod() : String {
        return "pangenome_${pangenomeHaplotypeMethod()}_k${indexKmerLength()}w${indexWindowSize()}I${indexNumberBases()}.mmi"
    }

    /**
     * Derives a pathKeyFile name from the keyFile name used for the reads
     */
    private fun pathKeyFileName() : String {
        //Create the outputFileNames
        val keyFileName = ParameterCache.value("keyFile").orElse("NA")
        val keyFileObj = File(keyFileName)

        //Check to see if the folder path is included.
        //If it is not included, keyFileObj.parent will return a null so we can just preappend ./ to the filename.
        val keyFileNameWithoutExtension = if(keyFileObj.parent != null) {
            keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
        }
        else {
            "./" + keyFileObj.nameWithoutExtension
        }

        return "${keyFileNameWithoutExtension}_pathKeyFile.txt"
    }

    /**
     * Checks for the existence of a pangenome and checks whether the pangenome specified by a config file exists.
     * Return true if the pangenome index exists.
     * If the pangenome fasta exists but the index does not, creates the index and returns true.
     * If neither the index or the fasta exist returns false.
     */
    private fun verifyPangenomeIndex() : Boolean {
        //is there a pangenome index name?
        //  if yes, check for the existence of pangenomeDir/pangenomeIndexName
        //    if that does not exist, stop and exit
        //    if the pangenome index exists return true
        //if pangenome index name is null
        //  check for the existence of a pangenome index based on method
        //    if it exists return true
        //    if it does not exist, check for the existence of .fa based method
        //      if the pangenome.fa exists, create the index then return true
        //      if the pangenome.fa does not exist, return false

        if (pangenomeIndexName() != null) {
            val pangenomeIndexFile = File("${pangenomeDir()}/${pangenomeIndexName()}")
            if (pangenomeIndexFile.exists()) return true
            throw IllegalArgumentException("${pangenomeIndexFile.path} does not exist")
        } else {
            val pangenomeIndexFile = File("${pangenomeDir()}/${pangenomeIndexNameFromMethod()}")
            if (pangenomeIndexFile.exists()) return true
            val pangenomeFile = File("${pangenomeDir()}/${pangenomeNameFromMethod()}")
            if (pangenomeFile.exists()) {
                indexPangenome()
                return true
            }
            return false
        }

    }

    /**
     * Runs WriteFastaFromGraphPlugin
     */
    private fun createPangenome(graphDataSet : DataSet) {
        WriteFastaFromGraphPlugin(null, false)
                .outputFile("${pangenomeDir()}/${pangenomeNameFromMethod()}")
                .performFunction(graphDataSet)
    }

    private fun createKmerMap() {
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSet = graphBuilder
            .methods(pangenomeHaplotypeMethod())
            .performFunction(null)

        KmerHashMapFromGraphPlugin()
            .saveFileName(kmerMapPath())
            .performFunction(graphDataSet)
    }

    /**
     * Uses minimap2 to index the pangenome
     */
    private fun indexPangenome() {
        //use minimap2 to index pangenome
        myLogger.info("Indexing pangenome starting at ${DateTimeFormatter.ISO_LOCAL_TIME.format(LocalTime.now())}.")
        val indexName = "${pangenomeDir()}/${pangenomeIndexNameFromMethod()}"
        val fastaName = "${pangenomeDir()}/${pangenomeNameFromMethod()}"
        val minimapProcessBuilder = ProcessBuilder(minimapLocation(), "-d", indexName, "-k${indexKmerLength()}",
                "-I${indexNumberBases()}", "-w${indexWindowSize()}", fastaName)
        minimapProcessBuilder.redirectError(ProcessBuilder.Redirect.INHERIT)
        minimapProcessBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        val minimapProcess = minimapProcessBuilder.start()
        minimapProcess.waitFor()
        myLogger.info("Finished indexing pangenome at ${DateTimeFormatter.ISO_LOCAL_TIME.format(LocalTime.now())}.")
    }

    /**
     * Writes the default config file. There are different config files depending on the input data type.
     * There is one specific to fastq or sam files and another for vcf files
     */
    private fun writeDefaultConfigFile() {
        //test for existence of config file
        //if it exists throw an exception. Overwriting existing config files is a bad idea.
        val configFile = File(configFile())
        if (configFile.exists()) throw IllegalArgumentException("${configFile()} already exists and will not be overwritten.")
        val myInputType = inputType()
        if (myInputType == INPUT_TYPE.sam || myInputType == INPUT_TYPE.fastq) writeFastqSamConfigFile()
        else if (myInputType == INPUT_TYPE.vcf) writeVcfConfigFile()
        else throw IllegalArgumentException("Input type ${myInputType} is unknown.")
    }

    private fun writeFastqSamConfigFile() {
        PrintWriter(configFile()).use {pw ->
            pw.println("# Imputation Pipeline parameters for fastq or SAM files\n" +
                    "\n" +
                    "#!!! Required Parameters !!!\n" +
                    "#--- Database ---\n" +
                    "host=localHost\n" +
                    "user=sqlite\n" +
                    "password=sqlite\n" +
                    "DB=**UNASSIGNED**\n" +
                    "DBtype=sqlite\n" +
                    "\n" +
                    "#--- Used for writing a pangenome reference fasta(not needed when inputType=vcf) ---\n" +
                    "pangenomeHaplotypeMethod=**UNASSIGNED**\n" +
                    "pangenomeDir=/phg/outputDir/pangenome\n" +
                    "indexKmerLength=21\n" +
                    "indexWindowSize=11\n" +
                    "indexNumberBases=90G\n" +
                    "\n" +
                    "#--- Used for mapping reads\n" +
                    "inputType=fastq\n" +
                    "readMethod=**UNASSIGNED**\n" +
                    "keyFile=/phg/readMapping_key_file.txt\n" +
                    "fastqDir=/phg/inputDir/imputation/fastq/\n" +
                    "samDir=/phg/inputDir/imputation/sam/\n" +
                    "lowMemMode=true\n" +
                    "maxRefRangeErr=0.25\n" +
                    "outputSecondaryStats=false\n" +
                    "maxSecondary=20\n" +
                    "fParameter=f1000,5000\n" +
                    "minimapLocation=minimap2\n" +
                    "\n" +
                    "#--- Used for path finding\n" +
                    "pathHaplotypeMethod=**UNASSIGNED*\n" +
                    "pathMethod=**UNASSIGNED*\n" +
                    "maxNodes=1000\n" +
                    "maxReads=10000\n" +
                    "minReads=1\n" +
                    "minTaxa=20\n" +
                    "minTransitionProb=0.001\n" +
                    "numThreads=3\n" +
                    "probCorrect=0.99\n" +
                    "removeEqual=true\n" +
                    "splitNodes=true\n" +
                    "splitProb=0.99\n" +
                    "usebf=false\n" +
                    "#   used by haploid path finding only\n" +
                    "usebf=false\n" +
                    "minP=0.8\n" +
                    "#   used by diploid path finding only\n" +
                    "maxHap=11\n" +
                    "maxReadsKB=100\n" +
                    "algorithmType=efficient\n" +
                    "\n" +
                    "#--- Used to output a vcf file for pathMethod\n" +
                    "outVcfFile=**UNASSIGNED**\n" +
                    "#~~~ Optional Parameters ~~~\n" +
                    "pangenomeIndexName=**OPTIONAL**\n" +
                    "#readMethodDescription=**OPTIONAL**\n" +
                    "#pathMethodDescription=**OPTIONAL**\n" +
                    "#debugDir=**OPTIONAL**\n" +
                    "#bfInfoFile=**OPTIONAL**")
        }
    }

    private fun writeVcfConfigFile() {
        PrintWriter(configFile()).use {pw ->
            pw.println("# Imputation Pipeline parameters for VCF files\n" +
                    "\n" +
                    "#!!! Required Parameters !!!\n" +
                    "#--- Database ---\n" +
                    "host=localHost\n" +
                    "user=sqlite\n" +
                    "password=sqlite\n" +
                    "DB=**UNASSIGNED**\n" +
                    "DBtype=sqlite\n" +
                    "\n" +
                    "#--- Used for indexing SNP positions ---\n" +
                    "pangenomeHaplotypeMethod=\n" +
                    "indexFile=/phg/outputDir/vcfIndexFile\n" +
                    "vcfFile=/phg/inputDir/imputation/vcf/yourFilenameHere.vcf\n" +
                    "\n" +
                    "#--- Used for mapping reads\n" +
                    "inputType=vcf\n" +
                    "indexFile=**UNASSIGNED**\n" +
                    "keyFile=/phg/readMapping_key_file.txt\n" +
                    "readMethod=**UNASSIGNED**\n" +
                    "outputDir=/phg/outputDir/\n" +
                    "vcfDir=/phg/inputDir/imputation/vcf/\n" +
                    "fastaOutFile=**UNASSIGNED**\n" +
                    "countAlleleDepths=true\n" +
                    "\n" +
                    "#--- Used for path finding\n" +
                    "pathHaplotypeMethod=**UNASSIGNED*\n" +
                    "pathMethod=**UNASSIGNED*\n" +
                    "maxNodes=1000\n" +
                    "maxReads=10000\n" +
                    "minReads=1\n" +
                    "minTaxa=20\n" +
                    "minTransitionProb=0.001\n" +
                    "numThreads=3\n" +
                    "probCorrect=0.99\n" +
                    "removeEqual=true\n" +
                    "splitNodes=true\n" +
                    "splitProb=0.99\n" +
                    "usebf=false\n" +
                    "#   used by haploid path finding only\n" +
                    "usebf=false\n" +
                    "minP=0.8\n" +
                    "#   used by diploid path finding only\n" +
                    "maxHap=11\n" +
                    "maxReadsKB=100\n" +
                    "algorithmType=efficient\n" +
                    "\n" +
                    "#--- Used to output a vcf file for pathMethod\n" +
                    "outVcfFile=**UNASSIGNED**\n" +
                    "#~~~ Optional Parameters ~~~\n" +
                    "#readMethodDescription=**OPTIONAL**\n" +
                    "#pathMethodDescription=**OPTIONAL**\n" +
                    "#bfInfoFile=**OPTIONAL**")
        }
    }

    override fun getToolTipText(): String {
        return "Run the impute pipeline"
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Impute Pipeline"
    }

    // The following getters and setters were auto-generated.

    /**
     * Specifies the target or desired endpoint when running
     * the plugin. For any given endpoint, the plugin will
     * check that all required previous step have been completed
     * and run those as needed.
     *
     * @return Impute Target
     */
    fun imputeTarget(): IMPUTE_TASK {
        return imputeTarget.value()
    }

    /**
     * Set Impute Target. Specifies the target or desired
     * endpoint when running the plugin. For any given endpoint,
     * the plugin will check that all required previous step
     * have been completed and run those as needed.
     *
     * @param value Impute Target
     *
     * @return this plugin
     */
    fun imputeTarget(value: IMPUTE_TASK): ImputePipelinePlugin {
        imputeTarget = PluginParameter<IMPUTE_TASK>(imputeTarget, value)
        return this
    }

    /**
     * The type of data to be used for imputation
     *
     * @return Input Type
     */
    fun inputType(): INPUT_TYPE {
        return inputType.value()
    }

    /**
     * Set Input Type. The type of data to be used for imputation
     *
     * @param value Input Type
     *
     * @return this plugin
     */
    fun inputType(value: INPUT_TYPE): ImputePipelinePlugin {
        inputType = PluginParameter<INPUT_TYPE>(inputType, value)
        return this
    }

    /**
     * The config file name.
     *
     * @return Config File
     */
    fun configFile(): String? {
        return configFile.value()
    }

    /**
     * Set Config File. The config file name.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): ImputePipelinePlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * The haplotype method used to create the pangenome.fa
     * and pangenome.mmi.
     *
     * @return Pangenome Haplotype Method
     */
    fun pangenomeHaplotypeMethod(): String {
        return pangenomeHaplotypeMethod.value()
    }

    /**
     * Set Pangenome Haplotype Method. The haplotype method
     * used to create the pangenome.fa and pangenome.mmi.
     *
     * @param value Pangenome Haplotype Method
     *
     * @return this plugin
     */
    fun pangenomeHaplotypeMethod(value: String): ImputePipelinePlugin {
        pangenomeHaplotypeMethod = PluginParameter<String>(pangenomeHaplotypeMethod, value)
        return this
    }

    /**
     * The pathHaplotypeMethod defines the set of haplotypes
     * to be used for pathing. It must be the same as the
     * pangenomeHaplotypeMethod or a subset of it.
     *
     * @return Path Haplotype Method
     */
    fun pathHaplotypeMethod(): String? {
        return pathHaplotypeMethod.value()
    }

    /**
     * Set Path Haplotype Method. The pathHaplotypeMethod
     * defines the set of haplotypes to be used for pathing.
     * It must be the same as the pangenomeHaplotypeMethod
     * or a subset of it.
     *
     * @param value Path Haplotype Method
     *
     * @return this plugin
     */
    fun pathHaplotypeMethod(value: String): ImputePipelinePlugin {
        pathHaplotypeMethod = PluginParameter(pathHaplotypeMethod, value)
        return this
    }

    /**
     * The directory of the pangenome file and index.
     *
     * @return Pangenome Dir
     */
    fun pangenomeDir(): String {
        return pangenomeDir.value()
    }

    /**
     * Set Pangenome Dir. The directory of the pangenome file
     * and index.
     *
     * @param value Pangenome Dir
     *
     * @return this plugin
     */
    fun pangenomeDir(value: String): ImputePipelinePlugin {
        pangenomeDir = PluginParameter<String>(pangenomeDir, value)
        return this
    }

    /**
     * WARNING: By default the plugin will construct pangenome
     * file and index names from the pangenomeHaplotypeMethod.
     * Only use this parameter to over-ride that name with
     * a different one. Be sure the pangenomeHaplotypeMethod
     * matches the index.
     *
     * @return Pangenome Index Name
     */
    fun pangenomeIndexName(): String? {
        return pangenomeIndexName.value()
    }

    /**
     * Set Pangenome Index Name. WARNING: By default the plugin
     * will construct pangenome file and index names from
     * the pangenomeHaplotypeMethod. Only use this parameter
     * to over-ride that name with a different one. Be sure
     * the pangenomeHaplotypeMethod matches the index.
     *
     * @param value Pangenome Index Name
     *
     * @return this plugin
     */
    fun pangenomeIndexName(value: String): ImputePipelinePlugin {
        pangenomeIndexName = PluginParameter<String>(pangenomeIndexName, value)
        return this
    }

    /**
     * Minimap2 index parameter k, the kmer length of the
     * minimizers, which is used to index the pangenome.
     *
     * @return Index Kmer Length
     */
    fun indexKmerLength(): Int {
        return indexKmerLength.value()
    }

    /**
     * Set Index Kmer Length. Minimap2 index parameter k,
     * the kmer length of the minimizers, which is used to
     * index the pangenome.
     *
     * @param value Index Kmer Length
     *
     * @return this plugin
     */
    fun indexKmerLength(value: Int): ImputePipelinePlugin {
        indexKmerLength = PluginParameter(indexKmerLength, value)
        return this
    }

    /**
     * Minimap2 index parameter w, the minimizer window size,
     * which is used to index the pangenome.
     *
     * @return Index Window Size
     */
    fun indexWindowSize(): Int {
        return indexWindowSize.value()
    }

    /**
     * Set Index Window Size. Minimap2 index parameter w,
     * the minimizer window size, which is used to index the
     * pangenome.
     *
     * @param value Index Window Size
     *
     * @return this plugin
     */
    fun indexWindowSize(value: Int): ImputePipelinePlugin {
        indexWindowSize = PluginParameter(indexWindowSize, value)
        return this
    }

    /**
     * Minimap2 index parameter I, the maximum number of bases
     * loaded into memory, which is used to index the pangenome.
     * This must be large enough to hold the entire pangenome
     * in memory.
     *
     * @return Index Number Bases
     */
    fun indexNumberBases(): String {
        return indexNumberBases.value()
    }

    /**
     * Set Index Number Bases. Minimap2 index parameter I,
     * the maximum number of bases loaded into memory, which
     * is used to index the pangenome. This must be large
     * enough to hold the entire pangenome in memory.
     *
     * @param value Index Number Bases
     *
     * @return this plugin
     */
    fun indexNumberBases(value: String): ImputePipelinePlugin {
        indexNumberBases = PluginParameter<String>(indexNumberBases, value)
        return this
    }

    /**
     * Location of Minimap2 on file system.  This defaults
     * to use minimap2 if it is on the PATH environment variable.
     *
     * @return Location of Minimap2 Executable
     */
    fun minimapLocation(): String {
        return minimapLocation.value()
    }

    /**
     * Set Location of Minimap2 Executable. Location of Minimap2
     * on file system.  This defaults to use minimap2 if it
     * is on the PATH environment variable.
     *
     * @param value Location of Minimap2 Executable
     *
     * @return this plugin
     */
    fun minimapLocation(value: String): ImputePipelinePlugin {
        minimapLocation = PluginParameter<String>(minimapLocation, value)
        return this
    }

    /**
     * The read method name to used for the read mapping data
     * stored in the DB.
     *
     * @return Read Method
     */
    fun readMethod(): String {
        return readMethod.value()
    }

    /**
     * Set Read Method. The read method name to used for the
     * read mapping data stored in the DB.
     *
     * @param value Read Method
     *
     * @return this plugin
     */
    fun readMethod(value: String): ImputePipelinePlugin {
        readMethod = PluginParameter<String>(readMethod, value)
        return this
    }

    /**
     * An additional description that will be stored with
     * the read method name, if desired.
     *
     * @return Read Method Description
     */
    fun readMethodDescription(): String? {
        return readMethodDescription.value()
    }

    /**
     * Set Read Method Description. An additional description
     * that will be stored with the read method name, if desired.
     *
     * @param value Read Method Description
     *
     * @return this plugin
     */
    fun readMethodDescription(value: String): ImputePipelinePlugin {
        readMethodDescription = PluginParameter<String>(readMethodDescription, value)
        return this
    }


    /**
     * The name of the file to be output by the PathsToVCFPlugin.
     *
     * @return Out Vcf File
     */
    fun outVcfFile(): String {
        return outVcfFile.value()
    }

    /**
     * Set Out Vcf File. The name of the file to be output
     * by the PathsToVCFPlugin.
     *
     * @param value Out Vcf File
     *
     * @return this plugin
     */
    fun outVcfFile(value: String): ImputePipelinePlugin {
        outVcfFile = PluginParameter<String>(outVcfFile, value)
        return this
    }

    /**
     * Parameter to force the DB to update.  If this is set
     * to true and the DB is incompatible with the current
     * PHG code, this will automatically update the DB if
     * it can.
     *
     * @return Force D B Update
     */
    fun forceDBUpdate(): Boolean {
        return myForceDBUpdate.value()
    }

    /**
     * Set Force D B Update. Parameter to force the DB to
     * update.  If this is set to true and the DB is incompatible
     * with the current PHG code, this will automatically
     * update the DB if it can.
     *
     * @param value Force D B Update
     *
     * @return this plugin
     */
    fun forceDBUpdate(value: Boolean): ImputePipelinePlugin {
        myForceDBUpdate = PluginParameter<Boolean>(myForceDBUpdate, value)
        return this
    }

    /**
     * Liquibase release directory.  HOlds liquibase executable
     * and change logs.
     *
     * @return Liquibase Dir
     */
    fun liquibaseDir(): String {
        return liquibaseDir.value()
    }

    /**
     * Set Liquibase Dir. Liquibase release directory.  HOlds
     * liquibase executable and change logs.
     *
     * @param value Liquibase Dir
     *
     * @return this plugin
     */
    fun liquibaseDir(value: String?): ImputePipelinePlugin {
        liquibaseDir = PluginParameter<String>(liquibaseDir, value)
        return this
    }

    /**
     * The directory to which liquibase output files will
     * be written.
     *
     * @return Liquibase Outdir
     */
    fun liquibaseOutputDir(): String {
        return liquibaseOutputDir.value()
    }

    /**
     * Set Liquibase Outdir. The directory to which liquibase
     * output files will be written.
     *
     * @param value Liquibase Outdir
     *
     * @return this plugin
     */
    fun liquibaseOutputDir(value: String): ImputePipelinePlugin {
        liquibaseOutputDir = PluginParameter<String>(liquibaseOutputDir, value)
        return this
    }

    /**
     * Should the liquibase check be skipped? Do not set to
     * true if running inside a Docker. Then, only set to
     * true if the PHG DB version is known to match the software
     * version.
     *
     * @return Skip Liquibase Check
     */
    fun skipLiquibaseCheck(): Boolean {
        return skipLiquibaseCheck.value()
    }

    /**
     * Set Skip Liquibase Check. Should the liquibase check
     * be skipped? Do not set to true if running inside a
     * Docker. Then, only set to true if the PHG DB version
     * is known to match the software version.
     *
     * @param value Skip Liquibase Check
     *
     * @return this plugin
     */
    fun skipLiquibaseCheck(value: Boolean): ImputePipelinePlugin {
        skipLiquibaseCheck = PluginParameter<Boolean>(skipLiquibaseCheck, value)
        return this
    }

    /**
     * Folder where gvcfs will be downloaded and stored
     *
     * @return Local GVCF Folder
     */
    fun localGVCFFolder(): String {
        return localGVCFFolder.value()
    }

    /**
     * Set Local GVCF Folder. Folder where gvcfs will be downloaded
     * and stored
     *
     * @param value Local GVCF Folder
     *
     * @return this plugin
     */
    fun localGVCFFolder(value: String): ImputePipelinePlugin {
        localGVCFFolder = PluginParameter<String>(localGVCFFolder, value)
        return this
    }


}

fun main() {
    GeneratePluginCode.generateKotlin(ImputePipelinePlugin::class.java)
}


