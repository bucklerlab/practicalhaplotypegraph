**DB Loading Methods**

The best practices for Loading  maize inbred line anchor and inter-anchor data to a PHG database 
is described in this document.

Details:

**Reference anchors:**
* Run the InbredHaplotyperPipeline to get anchor coordinates file.
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/LoadAnchorsToPHGdb.java 
    * takes as input the anchor coordinates file from InbredHaplotyperPipeline
    * output: creates the db, populates entries in the following tables:
        * genotypes
        * haplotypes
        * methods
        * anchor_versions
        * anchors
        * anchor_sequences
        * anchor_haplotypes ( links anchor and anchor sequences to the reference haplotype)
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/CreateRef_InterAnchorCSV.java 
    * use the db created in previous step
    * Grabs anchors from db, sorts based on chrom/pos
    * Using reference genome, grabs coordinates for start/end positions not included in anchor db
    * output:  CSV file of inter-anchor coordinates
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/LoadRefInterAnchorsToPHGdb.java 
    * use CSV created in previous step.  
    * Processes CSV from previous step, identifying regions with no inter-anchor
    * output:  populates entries in the following tables:
        * anchors (is_anchor field identities anchor as an inter-anchor)
        * anchor_sequences
        * anchor_haplotypes (links inter-anchor and inter-anchor sequences to the reference haplotype)

**Assembly Anchors:**
NOTE:  If the trimmed assembly fastas have already been created from the trimmed reference fasta, then use those fastas for loading and skip the "CreatePHGAssemblyAnchors_justBlast.java" step below
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/CreatePHGAssemblyAnchors_justBlast.java 
    * Grab anchors from DB created above
    * Blast the anchor regions against the assembly
    * Filters out based on matching chrom, length, % id, and quality
    * fasta file created
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/LoadHapSequencesToPHGdb.java
    * takes fasta from previous step and loads to db
    * output: populates entries in the following tables:
        * genotypes
        * haplotypes
        * methods
        * anchors
        * anchor_sequences
        * anchor_haplotypes ( links anchor and anchor sequences to the assembly)
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/AddAssembly_unknownSequences.java
    * This method must be run if loading assemblies made before code was added to include an entry for every anchor. It adds "UNKNOWN" as a sequence for all missing anchorids for the specified assembly.
    * it takes as input the following:
        * assembly line name
        * assembly hap number
        * assembly method name
        * relLine name
        * the db from which to pull and load data.
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/CreateAssembly_InterAnchorCSV.java
    * grab assembly anchors and coordinates from the db
    * create a CSV file of assembly anchor positions
    * The new CSV is processed, merging overlapping assemblies and associating inter-regions to ref inter-anchors
    * output: a fasta file ready for loading
* Java Program: src/main/java/net/maizegenetics/pangenome/db_loading/LoadHapInterAnchorsToPHGdb 
    * use fasta created in previous step
    * output: populates entries in the following tables:
        * genotypes
        * haplotypes
        * methods
        * anchors
        * anchor_sequences
        * anchor_haplotypes ( links inter-anchor and inter-anchor sequences to the assembly)    

**September 2017**
* Still load anchors from csv file from June hackathon.  Use method LoadGenomeIntervalsToPHGdbPlugin with these variables (on laptop)  Note that now there is a loading file for the REF as there has always been for loading haplotypes:

       String anchorsFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/interAnchors_June/trimmedAnchorsToLoad_B73Ref_withGenes.txt";
        String db = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/testRemovedFiles.db";
        String genomeDataFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/B73Ref_load_data.txt";

* Create RefInteranchors from laptop using method CreateRef_InterAnchorCSV.java using these parameters:

       String anchorsFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/interAnchors_June/trimmedAnchorsToLoad_B73Ref_withGenes.txt";
        String db = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/testRemovedFiles.db";
        String genomeDataFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/B73Ref_load_data.txt";
        String outputFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/refInterAnchorsFromDB_usingRangeMap.csv";
        
* load ref inter-anchors from laptop using LoadRefInterRegionsPlugin.java.  Note the db name changed for loading anchors, creating inter-anchors and loading ref-interanchors.  All were loaded to the same db, but its name changed.  Here are the parameters used for loading ref interanchors: (Testing verified the inter-anchor regions created with the new plugin were an exact copy of those created previously and stored to refInterAnchorsFromDB.csv)

        String refGenome = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zea_mays.AGPv4.dna.toplevel.fa";
        String anchorsFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/august2017_grantFiles/refInterAnchorsFromDB.csv";
        String db = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/testNewSchema_21august2017_PLUGIN_AnchorPHG.db";
        String line = "B73Ref";
        int hapNumber = 0;
        String method_name = "v4_gffGenes_plus1000_trimmed";
        String version_name = "B73v4_gffGenes_plus1000_trimmed";
        
* Zack creates the haplotypes from his gvcf filer code.  I then created shell scripts to load batches of them.  The scripts are created from db_loading/ShellScript_createLoadHaplotypes.java

* The consensus anchor fastas are also created by Zack.  I load them via shell scripts created from db_loading/ShellScript_createLoadConsensusAnchor.java

* Creating anchor regions from assembly:
    * First take the assembly genomes and run them through pangenome.processAssemblyGenomes.CreateFastqFromAssemblyGenomePlugin.java to create contig fastas
    * on cbsudc01, ran this command
nohup java -Xms200g -Xmx225g -jar jars/CreateFastqFromAssemblyGenomePlugin.jar maize_assemblies/W22__Ver12.genome.fasta W22 agpv4_phg_files/asembly_fastqFasta_contigs_hackathon2017/ > agpv4_phg_files/asembly_fastqFasta_contigs_hackathon2017/w22_output.txt &
    * Then run the contig fastas through processAssemblyGenomes.Minimap2PipelinePlugin.java.  On cbsudc01 I ran this command:
nohup java -Xms200g -Xmx225g -jar jars/Minimap2PipelinePlugin.jar maize_assemblies/Zea_mays.AGPv4.dna.toplevel.fa agpv4_phg_files/asembly_fastqFasta_contigs_hackathon2017/W22_asContigs.fa W22 agpv4_phg_files/minimapPipeline/W22_output/ /workdir/lcj34/Practical_haplotype_graph/WGS_pipeline/jars/minimap2 /programs/bin/samtools/samtools /programs/bin/samtools/bcftools >  agpv4_phg_files/minimapPipeline/W22_output/w22_pipeline_output.txt &
 
* creating inter-regions from assembly: (branch PHG-33)
    * Still working on this.  Initial code is in src/test/java/.../pangenome/minimap2/RunMinimap2Pipeline.java.  it runs the pipeline then extracts the bams, makes the gvcf for for Zack.  Then Reads the BAMs created earlier in the minimap2 pipeline to find assembly start/end  positions, grabbing sequence from the genome.
    * still need to look for overlapping regions among both anchor and inter-anchor regions
    
** Sept 10, 2017 **
Updated methods.  Everything is being turned into a real plugin.  To run PHG plugins from the TASSEL run_pipelin.pl code do the following:

1. From  your favorite place (my IDE), create a PHG jar file by right clicking on the PracticalHaplotypeGraph project, select export -> jar file (NOT runnable jar).  This creates a jar file of the whole project.
2.  download tassel-5-standalone to cbsu machine.
3.  Add the PHG.jar file you created above to the tassel-5-standalone/lib folder.

You should now be able to execute  ./run_pipeline.pl and it sees your plugin.

To load many at once, create a shell script and run it via:
nohup ./assembly_loadScript_asPlugin_first7.sh &

To create the assembly fasta of contigs, run: (from /workdir/lcj34/Practical_haplotype_graph/WGS_pipeline on cbsudc01)

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -CreateContigFastaFromAssemblyGenomePlugin -genomeFile maize_assemblies/W22__Ver12.genome.fasta -assembly W22 -o agpv4_phg_files/assembly_fasta_contigs_hackathon2017/ > agpv4_phg_files/assembly_fasta_contigs_hackathon2017/w22_output.txt &

To get assembly data into a fasta file run Minimap2PipelinePlugin: (This is running from /workdir/lcj34/Practical_haplotype_graph/WGS_Pipeline on cbsudc01)

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -Minimap2PipelinePlugin -ref maize_assemblies/Zea_mays.AGPv4.dna.toplevel.fa -assembly agpv4_phg_files/assembly_fasta_contigs_hackathon2017/B104_asContigs.fa -name B104 -intervals /workdir/lcj34/Practical_haplotype_graph/project_pangenome_files/intervals_files/hackathon_trimmed_intervals.bed -minimap2Path jars/minimap2 -o agpv4_phg_files/minimapPipeline/ > agpv4_phg_files/minimapPipeline/b104_pipeline_output.txt &

NOTE: for above, the folders agpv4_phg_files/minimapPipeline/Fastas and agpv4_phg_files/minimapPipeline/Filtered must exist before running the pipeline.

Running LoadHapSequencesToDBPlugin:
- this method used for loading fastas creatied either from the assembly methods (createContigFastaFromAssemblyGenomePlugin and Minimap2PipelinePlugin) or GATK haplotyes fasta into the db.  Here is example:

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -LoadHapSequencesToDBPlugin -db agpv4_phg_dbs/aug2017_308haps_NoConsensusWithAssemblies.db -fasta agpv4_phg_files/minimapPipeline/Fastas/F7ToRef.sorted_filtered.fa -genomeData agpv4_phg_files/f7_load_data.txt -gvcf agpv4_phg_files/minimapPipeline/Filtered/  > load_gvcfAssemblies_output/f7_gvcfLoadPlugin_output.txt &

When there is a large directory of haplotypes to load from Zack, it is easier to create a shell
script for loading.  Create a list of the files to be loaded (l -1 > files.out).  remove "files.out"
from the file list.  GIve this list to a java program to create the shell script.  I've been using
ShellScript_createLoadHaplotypes.java, but it is setup for "main", not for the plugin as a "plugin",
so it needs altering.

Load the consensus fastas via LoadConsensusAnchorSequencesPlugin:
- note the "\" before the quotes surrounding the method details.  WIthout the quotes, the run_pipeline.pl
script counts each space-separated word as a new argument and complains. Does the method description need to
be in a separate file?

ALSO _ concatenate all the gvcfs for each chromsome into 1 file per chrom or this will be very
slow. Zack gives 1 per anchor, db insertion is too slow unless you concat to 1 file per chromosome.

The command below was run on cbsudc01 from /workdir/lcj34/Practical_haplotype_graph/WGS_pipeline directory.

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -LoadConsensusAnchorSequencesPlugin -db agpv4_phg_dbs/aug2017_308haps_NoConsensusWithAssemblies.db -inputFile zack_combinedConsensus_withGVCFname -version B73v4_gffGenes_plus1000_trimmed -vcfDir zack_combinedConsensus_withGVCFname_VCFFiles/VCFFiles/ -cmethod collapse_method_1 -method_details \"gvcftyper with max error .001, mininum taxa 2 and minimum site 20\" > agpv4_phg_files/collapse_anchors_output/collapse_anchors_output_PLUGINtest.txt &

To load consensus for the 37804 anchors on cbsudc01 (large memory machine) it took 5205.129124214 seconds
(86.75 minutes).  THis was loading into a db with 308 haplotypes, size 58G (no assemblies)

**DBs currently stored**

* current databases are stored on cbsublfs1 in /data1/PHG/phg_dbs
* Look at the README.txt file in /data1/PHG/phg_dbs to see what is in each db
* copy these DB and add data.  Do not overwrite these dbs unless you are fixing an error.

Example shell scripts for running minimap2pipeline and loading haplotypes can be seen on cbsudc01 in /workdir/lcj34/Practical_haplotype_graph/WGS_pipeline in 
* minimapFirst7Assemblies.sh
* assembly_loadScript_asPlugin_first7.sh

Commands to load the DB with ref anchor and interanchor, run as Plugins:  Note the csv files
needed for loading are stored in PHG repository in
* refInterAnchorsFromDB.csv
* trimmedAnchorsToLoad_B73Ref_withGenes.csv

This run on cbsudc01 from /workdir/lcj34/Practical_haplotype_graph/WGS_pipeline directory:

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -LoadGenomeIntervalsToPHGdbPlugin -ref maize_assemblies/Zea_mays.AGPv4.dna.toplevel.fa -db agpv4_phg_dbs/testLoadingAnchors_viaPlugin.db -anchors agpv4_phg_files/trimmedAnchorsToLoad_B73Ref_withGenes.csv -genomeData agpv4_phg_files/B73Ref_load_data.txt > testPLUGIN_loaddb.txt &

nohup /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -LoadRefInterRegionsPlugin -db agpv4_phg_dbs/testLoadingAnchors_viaPlugin.db -ref maize_assemblies/Zea_mays.AGPv4.dna.toplevel.fa -anchors agpv4_phg_files/refInterAnchorsFromDB.csv -line B73Ref -hapNumber 0 -method v4_gffGenes_plus1000_trimmed -version B73v4_gffGenes_plus1000_trimmed > testInterAnchorLoad.txt &


    
    

