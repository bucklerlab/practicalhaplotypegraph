package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.junit.SmallSeqExtension
import org.junit.jupiter.api.Test
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.util.LoggingUtils
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import java.io.File
import java.lang.IllegalArgumentException
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

@ExtendWith(SmallSeqExtension::class)
class AddRefRangeGroupIT {

    companion object {
        val currentLocalDateTime = LocalDateTime.now()
        private const val rangeLimit = 10
        val methodName = "FocusRegion2_${currentLocalDateTime}" // Method must not already exist
        val methodDetails = "$methodName - AddRefRangeGroupIT"

        @JvmStatic
        @BeforeAll
        fun setup() {
            // NOTE: File operations in setup() cause all tests to fail
            LoggingUtils.setupDebugLogging()

        }
    }

    @Test
    fun testAddRefRangeGroupGoodData() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false)

        val goodRangesFile = "${SmallSeqExtension.baseDir}/goodRanges.txt"

        val  refRangeSet = CreateGraphUtils.referenceRanges(connection)
        File(goodRangesFile).bufferedWriter().use {
            var indexCounter = 0
            var rangeContents = ""
            for (refRange in refRangeSet) {
                // BED file is 0-based and exclusive on the end
                rangeContents = "${rangeContents}${refRange.chromosome().name}\t${refRange.start() - 1}\t${refRange.end()}\n"
                indexCounter++
                if (indexCounter == rangeLimit) break
            }
            it.write(rangeContents)
        }

        AddRefRangeGroupPlugin()
            .methodName("good_${methodName}")
            .methodDetails(methodDetails)
            .configFile(SmallSeqExtension.configFilePath)
            .ranges(goodRangesFile)
            .performFunction(null)

        val methodId = CreateGraphUtils.methodId(connection, "good_${methodName}")
        assertTrue(methodId > 0)

        val rs = connection.createStatement().executeQuery("select count(*) as group_count from ref_range_ref_range_method where method_id = $methodId")

        if (rs.next()) assertEquals(rangeLimit, rs.getInt("group_count"))
        else           fail("No count for methodName: \"good_${methodName}\"")

        // Run again, using the same method.  This should fail due to method already existing
        assertFailsWith<IllegalArgumentException> {
            AddRefRangeGroupPlugin()
                .methodName("good_${methodName}")
                .methodDetails(methodDetails)
                .configFile(SmallSeqExtension.configFilePath)
                .ranges(goodRangesFile)
                .processData(null) //Need to call processData() directly as preformFunction will catch the error and just print out the message to stdout
        }

        connection.close()
    }
    
    @Test
    fun testAddRefRangeGroupBadRangeData() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false)

        val badRangesFile = "${SmallSeqExtension.baseDir}/badRanges.txt"

        val  refRangeSet = CreateGraphUtils.referenceRanges(connection)
        File(badRangesFile).bufferedWriter().use {
            var indexCounter = 0
            var rangeContents = ""
            for (refRange in refRangeSet) {
                // BED file is 0-based, but purposefully don't decrement to create a bad range
                rangeContents = "${rangeContents}${refRange.chromosome().name}\t${refRange.start()}\t${refRange.end()}\n"
                indexCounter++
                if (indexCounter == rangeLimit) break
            }
            it.write(rangeContents)
        }

        // This should throw an exception: bad input file ref range data
        assertFailsWith<IllegalArgumentException> {
            AddRefRangeGroupPlugin()
                .methodName("bad_${methodName}")
                .methodDetails(methodDetails)
                .configFile(SmallSeqExtension.configFilePath)
                .ranges(badRangesFile)
                .processData(null) //Need to call processData() directly as preformFunction will catch the error and just print out the message to stdout
        }//        }

        connection.close()
    }
}
