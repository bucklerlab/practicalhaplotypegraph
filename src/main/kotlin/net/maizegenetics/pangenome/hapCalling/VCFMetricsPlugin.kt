@file:Suppress("UnstableApiUsage")

package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.DiscreteDomain
import com.google.common.collect.Range
import com.google.common.collect.TreeRangeSet
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.BufferedWriter
import java.io.File
import javax.swing.ImageIcon
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.util.Utils
import org.apache.commons.math3.stat.descriptive.rank.Percentile
import kotlin.math.abs


/**
 * This plugin calculates metrics for gvcf files generated from MAFToVCFPlugin.
 * Summary statistics are written to a tab-delimited file, with one row for the full-gvcf summary, and subsequent rows
 * for each chromosome. Metrics include:
 * Number of SNPs, insertions and deletions
 * Number of bases inserted and deleted
 * Total bases
 * Percentage of bases aligned relative to reference - reference blocks and snps are considered to be "aligned"
 * Percentage of bases covered relative to reference - reference, SNPs, and indels are considered to be "covered"
 * (Unmapped ranges are not considered to be "covered")
 * Percentage of reference length inserted and deleted
 * indel sizes mean, quartiles, and largest
 * insertion and deletion sizes mean, quartiles, and largest
 *
 * Optionally, an additional file can be written containing the sizes of the indels in each gvcf - again separated by chromosome
 * Each line starts with the sample and chromosome name, and then contains a tab-delimited list of indel sizes
 *
 * Assumptions:
 * gvcf files end with .gvcf, or else are gzipped and end with .gvcf.gz
 *
 * @author ahb232 March 6, 2023
 */
class VCFMetricsPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(VCFMetricsPlugin::class.java)

    private var vcfFile = PluginParameter.Builder("vcfFile", null, String::class.java)
        .guiName("gvcf  to process")
        .required(false)
        .description("Name of the gvcf file to process.")
        .build()

    private var vcfDir = PluginParameter.Builder("vcfDir", null, String::class.java)
        .guiName("Directory containing gvcf file(s) to process")
        .inDir()
        .required(true)
        .description("Name of the directory containing gvcf file(s) to process")
        .build()

    private var outFile = PluginParameter.Builder("outFile", null, String::class.java)
        .guiName("output tab-delimited table")
        .outFile()
        .required(true)
        .description("Name of the file to write tab-delimited output")
        .build()

    private var indelFile = PluginParameter.Builder("indelFile", "", String::class.java)
        .guiName("output list of indel sizes to file")
        .outFile()
        .required(false)
        .description("Name of the file to write list of indel sizes")
        .build()

    /**
     * Data class containing summary statistics for a particular file or contig/chromosome
     */
    data class VCFSummary(val name: String, val numSNPs: Int, val numIndels: Int,
                          val basesAligned: Int, val basesInserted: Int, val basesDeleted: Int, val basesGapped: Int,
                          val totalBases: Long,
                          val indelSizes: List<Int>)

    /**
     * Data class containing summary information about indel distribution (number of insertions/deletions, average sizes, etc.)
     * toString() is overridden in order to make printing to tab-delimited summary file easier
     */
    data class IndelDistributions(val numIndels: Int, val meanIndel: Double, val indelLowerQuartile: Double, val indelMedian: Double, val indelUpperQuartile: Double, val indelLargest: Int,
                                  val numIns: Int, val meanIns: Double, val insLowerQuartile: Double, val insMedian: Double, val insUpperQuartile: Double, val insLargest: Int,
                                  val numDel: Int, val meanDel: Double, val delLowerQuartile: Double, val delMedian: Double, val delUpperQuartile: Double, val delLargest: Int) {
        override fun toString(): String {
            return "$meanIndel\t$indelLowerQuartile\t$indelMedian\t$indelUpperQuartile\t$indelLargest\t" +
                    "$meanIns\t$insLowerQuartile\t$insMedian\t$insUpperQuartile\t$insLargest\t" +
                    "$meanDel\t$delLowerQuartile\t$delMedian\t$delUpperQuartile\t$delLargest\t"
        }
    }

    override fun processData(input: DataSet?): DataSet? {

        // get list of files to process
        val vcfList = if (vcfFile() == null) {
            File(vcfDir()).walk().filter { it.isFile }.filter { !it.isHidden }.filter { it.name.endsWith("gvcf.gz") || it.name.endsWith(".gvcf") }.toList()
        } else {
            listOf(File("${vcfDir()}/${vcfFile()}"))
        }

        Utils.getBufferedWriter(outFile()).use{summaryWriter ->
            val indelWriter = if (indelFile() != "") { File(indelFile()).bufferedWriter() } else { null }

            // write header
            summaryWriter.write("name\tnumSNPs\tnumIndels\tnumIns\tnumDel\t" +
                    "numBasesInserted\tnumBasesDeleted\tnumBasesInGaps\ttotalBases\t" +
                    "percentRefAligned\tpercentRefMapped\tpercentRefDeleted\tpercentRefInserted\t" +
                    "meanIndelSize\tlowerQuartileIndelSize\tmedianIndelSize\tupperQuartileIndelSize\tlargestIndel\t" +
                    "meanInsSize\tlowerQuartileInsSize\tmedianInsSize\tupperQuartileInsSize\tlargestInsSize\t" +
                    "meanDelSize\tlowerQuartileDelSize\tmedianDelSize\tupperQuartileDelSize\tlargestDelSize\n")

            vcfList.forEach{file ->
                try {
                    val summaries = getVCFStats(file)

                    val allSummary = getAllChrVCFStats(summaries)
                    myLogger.info("Writing summary metrics to file.")
                    writeSummaryToFile(allSummary, summaryWriter)
                    summaries.forEach { writeSummaryToFile(it.value, summaryWriter) }
                    if (indelWriter != null) {
                        myLogger.info("Writing indel sizes to file.")
                        //writeIndelsToFile(allSummary, indelWriter)
                        summaries.forEach{sm -> writeIndelsToFile(sm.value, indelWriter)}
                    }
                }
                catch(e: Exception) {
                    myLogger.info(e.message)
                    myLogger.info("Error encountered processing file ${file.name}")
                    myLogger.info("This file will be skipped.")
                }

            }
            indelWriter?.close()
        }

        return null
    }

    /**
     *  given a vcfsummary object, calculate and write stats to file (see class description for list of stats)
     */
    fun writeSummaryToFile(summary: VCFSummary, writer: BufferedWriter) {
        // get sorted lists of absolute indel sizes, insertion sizes, and deletion sizes
        val indelDistributions = getIndelSizeStats(summary.indelSizes)

        val percentAligned = summary.basesAligned.toDouble()/summary.totalBases
        val percentMapped = (summary.basesAligned + summary.basesDeleted).toDouble()/summary.totalBases
        val percentDeleted = summary.basesDeleted.toDouble()/summary.totalBases
        val percentInserted = summary.basesInserted.toDouble()/summary.totalBases

        // first, write basic information: number of snps, indels
        writer.write("${summary.name}\t${summary.numSNPs}\t${summary.numIndels}\t${indelDistributions.numIns}\t${indelDistributions.numDel}\t")

        // then coverage

        writer.write("${summary.basesInserted}\t${summary.basesDeleted}\t${summary.basesGapped}\t${summary.totalBases}\t")
        writer.write("$percentAligned\t$percentMapped\t")
        writer.write("$percentDeleted\t$percentInserted\t")

        // then indel size distribution stats
        writer.write(indelDistributions.toString())

        writer.write("\n")
    }


    /**
     * write the lengths of indels to file. Deletions represented as negative numbers, insertions positive
     */
    fun writeIndelsToFile(summary: VCFSummary, writer: BufferedWriter) {
        writer.write("${summary.name}\t${summary.indelSizes.joinToString("\t")}\n")
    }

    /**
     * Given list of indel sizes, return number, mean length, quantiles length, and longest for indels, insertions, and deletions
     */
    fun getIndelSizeStats(indelSizes: List<Int>): IndelDistributions {
        // get sorted lists of absolute indel sizes, insertion sizes, and deletion sizes
        val absIndelSizes = indelSizes.map{ abs(it)}.sorted()
        val insertionSizes = indelSizes.filter{it > 0}.sorted()
        val deletionSizes = indelSizes.filter{it < 0}.map{ it * -1 }.sorted()

        val indelQuantiles = quantiles(absIndelSizes)
        val insQuantiles = quantiles(insertionSizes)
        val delQuantiles = quantiles(deletionSizes)

        return IndelDistributions(
            absIndelSizes.size,
            absIndelSizes.sum()/absIndelSizes.size.toDouble(),
            indelQuantiles["Q1"]!!,
            indelQuantiles["Q2"]!!,
            indelQuantiles["Q3"]!!,
            if(absIndelSizes.isNotEmpty()) {absIndelSizes.last()} else { 0 },
            insertionSizes.size,
            insertionSizes.sum()/insertionSizes.size.toDouble(),
            insQuantiles["Q1"]!!,
            insQuantiles["Q2"]!!,
            insQuantiles["Q3"]!!,
            if(insertionSizes.isNotEmpty()) {insertionSizes.last()} else { 0 },
            deletionSizes.size,
            deletionSizes.sum()/deletionSizes.size.toDouble(),
            delQuantiles["Q1"]!!,
            delQuantiles["Q2"]!!,
            delQuantiles["Q3"]!!,
            if (deletionSizes.isNotEmpty()) {deletionSizes.last()} else { 0 })

    }

    /**
     * Calculates summary statistics for the given gvcf file and returns them as a map
     */
    fun getVCFStats(file: File): Map<String, VCFSummary> {
        myLogger.info("Processing file ${file.name}")

        // file.nameWithoutExtension only removes .gz from .gvcf.gz, so we can handle this case explicitly
        val baseName = if (file.name.endsWith(".gvcf.gz")) {
            file.name.dropLast(8)
        } else {
            file.nameWithoutExtension
        }

        myLogger.info("Getting chromosome lengths.")
        // get iterator for gvcf file
        val reader = VCFFileReader(file, false)

        // extract lengths of the reference chromosomes
        val refChrLengths = reader.fileHeader
            .contigLines
            .filter { it.genericFields["ID"] != null && it.genericFields["length"] != null }.associate {
                Pair(
                    it.genericFields["ID"]!!,
                    Range.closed(1, it.genericFields["length"]!!.toInt()).canonical(DiscreteDomain.integers())
                )
            }

        val iterator = reader.iterator()

        // these keep track of the indels and SNPs identified on each chromosome (relative to reference)
        val indelSizes = mutableMapOf<String, MutableList<Int>>()
        val numSNPs = mutableMapOf<String, Int>()

        // for each contig we need to separately keep track of what positions are covered
        val refRangesCovered = mutableMapOf<String, TreeRangeSet<Int>>()

        // loop through all records in gvcf file
        while (iterator.hasNext()) {
            val record = iterator.next()

            // chromosomes
            val refChrom = record.contig

            // asm start and end positions, mostly so that we don't have to type the long call multiple times
            val asmEnd = record.getAttributeAsInt("ASM_End", -1)
            val asmStart = record.getAttributeAsInt("ASM_Start", -1)

            // length of ref segment and ASM segment
            // take absolute value of asmLen because if it is on the negative strand asmStart > asmEnd
            val refLen = record.end - record.start + 1
            val asmLen = abs(asmEnd - asmStart) + 1

            /* COVERAGE */

            // if we haven't encountered these chromosomes yet, add them to the map
            if (!refRangesCovered.containsKey(refChrom)) {
                //myLogger.info("New chromosome ID - $refChrom")
                refRangesCovered[refChrom] = TreeRangeSet.create<Int>()
                indelSizes[refChrom] = mutableListOf<Int>()
                numSNPs[refChrom] = 0
            }

            // calculate the difference in length between the ref and asm blocks
            // positive if insertion, negative if deletion, 0 if SNP or reference block
            val lenDif = asmLen - refLen

            // add the ranges that are covered to the appropriate lists
            val refRange = if (lenDif >= 0) {
                    getRange(record.start, record.end, 0)
                } else {
                    getRange(record.start, record.end, lenDif * -1)
                }

            // ranges should not overlap unless the aligner aligned one sequence to multiple locations
            // if that is the case, print out a warning
            if (refRangesCovered[refChrom]!!.intersects(refRange)) {
                myLogger.info("WARNING: Overlapping alignments found in chromosome $refChrom at position ${refRange.lowerEndpoint()}, ${refRange.upperEndpoint()}")
            }

            // then, add ranges to their respective sets
            refRangesCovered[refChrom]?.add(refRange)

            /* INDEL AND SNP COUNTING */

            // each listed allele can be reference, alt, or symbolic
            // records that only contain reference and symbolic alleles can be ignored
            // there should be at most one alt allele, though strictly speaking we can handle multiple
            // and each will count as its own SNP/indel
            for (allele in record.alleles) {
                if (!allele.isSymbolic && !allele.isReference) {
                    val queryLen = allele.length()

                    val indelSize = queryLen - refLen

                    // if alt allele is the same length as ref allele, it is a SNP
                    // else, it is an indel
                    if (indelSize != 0) {
                        indelSizes[refChrom]?.add(indelSize)
                    } else {
                        numSNPs[refChrom] = numSNPs[refChrom]!! + 1
                    }
                }
            }
        }

        myLogger.info(("Calculating coverage."))

        // a list of all chromosome names that had at least one record in the gvcf
        // we use this to get rid of scaffolding contigs that weren't part of the alignment but still made it into the gvcf header
        val allChromosmes = refRangesCovered.keys

        indelSizes.forEach {it.value.sort()}

        // get the ranges not covered by the alignment
        // in this context, "uncovered" means both gaps and indels (but not snps)
        val refRangesUncovered = refRangesCovered.map {
            return@map if (refChrLengths.containsKey(it.key)) {
                Pair(it.key, it.value.complement().subRangeSet(refChrLengths[it.key]!!))
            } else {
                null
            }
        }.filterNotNull().toMap()


        // get the number of bases covered/uncovered
        val sumRefCovered = refRangesCovered.map { chrRange ->
            Pair(chrRange.key, chrRange.value.asRanges().map {it.upperEndpoint() - it.lowerEndpoint()}.sum())
        }.toMap()

        val sumRefUncovered = refRangesUncovered.map { chrRange ->
            Pair(chrRange.key, chrRange.value.asRanges().map {it.canonical(DiscreteDomain.integers()).upperEndpoint() -
                    it.canonical(DiscreteDomain.integers()).lowerEndpoint()}.sum())
        }.toMap()

        val sumInserted = indelSizes.map{ entry ->
            Pair(entry.key,
                entry.value.filter{it > 0}.sum()
            )
        }.toMap()

        val sumDeleted = indelSizes.map{ entry ->
            Pair(entry.key,
                entry.value.filter{it < 0}.sum() * -1
            )
        }.toMap()

        val summaries = allChromosmes.map{
            Pair(it,
                VCFSummary(
                    "${baseName}_$it", // name
                    numSNPs[it]?:0, //numSNPs
                    indelSizes[it]?.size?:0, // numIndels
                    sumRefCovered[it]?:0, // basesAligned
                    sumInserted[it]?:0, //basesInserted
                    sumDeleted[it]?:0, //basesDeleted
                    (sumRefUncovered[it]?:0) - (sumDeleted[it]?:0), //basesGapped
                    (refChrLengths[it]?.upperEndpoint()?.toLong()?:0) - 1, //totalBases
                    indelSizes[it]?:listOf<Int>() //indelSizes
                )) }.toMap()

        return summaries
    }

    fun getAllChrVCFStats(summaries: Map<String, VCFSummary>): VCFSummary {
        myLogger.info("Calculating metrics across whole genome.")

        val first = summaries.keys.first()

        val name = "${summaries[first]!!.name.dropLast(first.length)}ALL"
        val numSNPs = summaries.map{it.value.numSNPs}.sum()
        val sumRefCovered = summaries.map{it.value.basesAligned}.sum()
        val basesInserted = summaries.map{it.value.basesInserted}.sum()
        val basesDeleted = summaries.map{it.value.basesDeleted}.sum()
        val basesGapped = summaries.map{it.value.basesGapped}.sum()
        val totalBases = summaries.map{it.value.totalBases}.sum()
        val indelSizes = summaries.map{it.value.indelSizes}.flatten().sorted()

        return VCFSummary (
            name,
            numSNPs,
            indelSizes.size,
            sumRefCovered,
            basesInserted,
            basesDeleted,
            basesGapped,
            totalBases,
            indelSizes)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = VCFMetricsPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "VCFMetricsPlugin"
    }

    override fun getToolTipText(): String {
        return "VCFMetricsPlugin"
    }

    /**
     * A single .gvcf file (optionally gzipped) to process.
     * Do not include file path, file is assumed to reside within vcfDir
     *
     * @return vcf file
     */
    fun vcfFile(): String? {
        return vcfFile.value()
    }

    /**
     * Set vcf file
     * A single .gvcf file (optionally gzipped) to process.
     * Do not include file path, file is assumed to reside within vcfDir
     *
     * @param value vcf file
     * @return this plugin
     */
    fun vcfFile(value: String): VCFMetricsPlugin {
        vcfFile = PluginParameter<String>(vcfFile, value)
        return this
    }

    /**
     * vcf directory. Directory containing one or more gvcf files to process
     * It is assumed that files end with .gvcf or .gvcf.gz (if gzipped)
     * All other files in this directory will be ignored.
     *
     * @return vcf directory
     */
    fun vcfDir(): String? {
        return vcfDir.value()
    }

    /**
     * Set vcf directory.
     * vcf directory. Directory containing one or more gvcf files to process
     * It is assumed that files end with .gvcf or .gvcf.gz (if gzipped)
     * All other files in this directory will be ignored.
     *
     * @param value vcf directory
     * @return this plugin
     */
    fun vcfDir(value: String): VCFMetricsPlugin {
        vcfDir = PluginParameter<String>(vcfDir, value)
        return this
    }

    /**
     * output metrics file. The tab-delimited file of summary statistcs calcualted from one or more gvcf files
     * Each file has a summary line in the table, as well as a line for each contig/chromosome
     *
     * @return output metrics file
     */
    fun outFile(): String {
        return outFile.value()
    }

    /**
     * Set output metrics file
     * output metrics file. The tab-delimited file of summary statistcs calcualted from one or more gvcf files
     * Each file has a summary line in the table, as well as a line for each contig/chromosome
     *
     * @param value output metrics file
     * @return this plugin
     */
    fun outFile(value: String): VCFMetricsPlugin {
        outFile = PluginParameter<String>(outFile, value)
        return this
    }

    /**
     * Optional file to write all indel lengths to
     * If not provided, indel lengths will not be written
     * Each line begins with the gvcf and chromosome name (separated by underscore)
     * And then the indel sizes, separated by tabs
     *
     * @return indel file
     */
    fun indelFile(): String {
        return indelFile.value()
    }

    /**
     * Set indel file
     * Optional file to write all indel lengths to
     * If not provided, indel lengths will not be written
     * Each line begins with the gvcf and chromosome name (separated by underscore)
     * And then the indel sizes, separated by tabs
     *
     * @param value indel file
     * @return this plugin
     */
    fun indelFile(value: String): VCFMetricsPlugin {
        indelFile = PluginParameter<String>(indelFile, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(VCFMetricsPlugin::class.java)
}

/**
 * Standardizes the format of the ranges: closed on lower edge, open on top
 * This is mostly to make sure that concatenating two adjacent ranges
 * Produces one large range.
 * Also handles trimming some amount off of the end of the range
 * And flipping ranges where start > end
 */
fun getRange(start: Int, end: Int, trim: Int): Range<Int> {
    return if(start > end) {
        Range.closed(end, start-trim).canonical(DiscreteDomain.integers())
    } else {
        Range.closed(start, end-trim).canonical(DiscreteDomain.integers())
    }
}

/**
 * Given a list, return the lower, median, and upper quartile values
 * Return NaN for all if list is empty
 */
fun quantiles(list: List<Int>): Map<String, Double> {

    if (list.isEmpty()) {
        return listOf(
            Pair("Q1", Double.NaN),
            Pair("Q2", Double.NaN),
            Pair("Q3", Double.NaN)
        ).toMap()

    }

    val percentile = Percentile()
    percentile.data = list.map{it.toDouble()}.toDoubleArray()

    return listOf(
        Pair("Q1", percentile.evaluate(25.0)),
        Pair("Q2", percentile.evaluate(50.0)),
        Pair("Q3", percentile.evaluate(75.0))
    ).toMap()


}