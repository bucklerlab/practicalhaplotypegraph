package net.maizegenetics.pangenome.io

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.BufferedWriter
import javax.swing.ImageIcon

/**
 * This plugin exports a PHG Graph to GFAv1 format.
 * PHG Paths can also be included if "Walk Lines" are wanted in the file.
 * http://gfa-spec.github.io/GFA-spec/GFA1.html
 */
class PHGtoGFAv1Plugin(parentFrame: Frame? = null, isInteractive: Boolean = false) :
    AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(PHGtoGFAv1Plugin::class.java)

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
        .description("Output filename")
        .required(true)
        .build()

    private var myFullyConnected = PluginParameter.Builder("fullyConnected", false, Boolean::class.javaObjectType)
        .description("Set to true to have adjacent reference ranges fully connected")
        .build()

    private lateinit var myGraph: HaplotypeGraph

    override fun preProcessParameters(input: DataSet) {
        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size == 1) { "PHGtoGFAv1Plugin: preProcessParameters: must input one HaplotypeGraph" }
        myGraph = temp[0].data as HaplotypeGraph
    }

    override fun processData(input: DataSet): DataSet? {

        var temp = input.getDataOfType(Map::class.java)
        when (temp.size) {
            0 -> {
                writeGraphToGFA(myGraph)
            }
            1 -> { // Use paths provided in input
                val taxaToPaths = temp[0].data as Map<String, List<List<HaplotypeNode>>>
                writeGraphToGFA(myGraph, taxaToPaths)
            }
            else -> {
                throw IllegalArgumentException("PHGtoGFAv1Plugin: processData: At most one paths map can be supplied as input.")
            }

        }

        return null

    }

    private fun writeGraphToGFA(graph: HaplotypeGraph, paths: Map<String, List<List<HaplotypeNode>>>? = null) {

        val gfaFilename = Utils.addSuffixIfNeeded(outputFile(), ".gfa")

        Utils.getBufferedWriter(gfaFilename).use { writer ->

            if (fullyConnected()) {

                var leftNodes: List<HaplotypeNode>? = null

                // Header with version number
                writer.write("H\tVN:Z:1.0\n")
                for (refRange in graph.referenceRanges()) {
                    if (leftNodes == null) {
                        leftNodes = graph.nodes(refRange)
                        leftNodes.forEach { writer.write("S\t${it.id()}\t${it.haplotypeSequence().sequence()}\n") }
                    } else {
                        val rightNodes = graph.nodes(refRange)
                        leftNodes.forEach { leftNode ->
                            rightNodes.forEach { rightNode ->
                                writer.write("L\t${leftNode.id()}\t+\t${rightNode.id()}\t+\t0M\n")
                            }
                        }

                        leftNodes = rightNodes
                        leftNodes.forEach { writer.write("S\t${it.id()}\t${it.haplotypeSequence().sequence()}\n") }
                    }
                }

            } else {

                // Header with version number
                writer.write("H\tVN:Z:1.0\n")
                for (refRange in graph.referenceRanges()) {
                    for (node in graph.nodes(refRange)) {
                        writer.write("S\t${node.id()}\t${node.haplotypeSequence().sequence()}\n")
                        for (edge in graph.leftEdges(node)) {
                            writer.write("L\t${edge.leftHapNode().id()}\t+\t${edge.rightHapNode().id()}\t+\t0M\n")
                        }
                        for (edge in graph.rightEdges(node)) {
                            writer.write("L\t${edge.leftHapNode().id()}\t+\t${edge.rightHapNode().id()}\t+\t0M\n")
                        }
                    }
                }

            }

            // W	NA12878	1	chr1	0	11	>s11<s12>s13
            paths?.forEach { (taxon, paths) ->
                val numPathsForTaxon = paths.size
                paths.forEachIndexed { index, nodes ->

                    val nodesSorted = nodes.sorted()
                    var currentChr = nodesSorted.first().referenceRange().chromosome().name
                    var currentStart = nodesSorted.first().referenceRange().start() - 1
                    var currentEnd = nodesSorted.first().referenceRange().end()

                    var walkStr = StringBuilder()
                    nodesSorted.forEach { node ->

                        if (currentChr != node.referenceRange().chromosome().name) {
                            writeWalkLine(
                                writer,
                                taxon,
                                index,
                                numPathsForTaxon,
                                currentChr,
                                currentStart,
                                currentEnd,
                                walkStr.toString()
                            )
                            currentChr = node.referenceRange().chromosome().name
                            currentStart = nodesSorted.first().referenceRange().start() - 1
                            walkStr = StringBuilder()
                        }

                        currentEnd = node.referenceRange().end()
                        walkStr.append(">${node.id()}")

                    }

                    writeWalkLine(
                        writer,
                        taxon,
                        index,
                        numPathsForTaxon,
                        currentChr,
                        currentStart,
                        currentEnd,
                        walkStr.toString()
                    )

                }

            }

        }

    }

    private fun writeWalkLine(
        writer: BufferedWriter,
        taxon: String,
        index: Int,
        numPathsForTaxon: Int,
        chr: String,
        start: Int,
        end: Int,
        walk: String
    ) {
        val hapIndex = if (numPathsForTaxon == 1) 0 else index + 1
        writer.write("W\t${taxon}\t${hapIndex}\t${chr}\t${start}\t${end}\t${walk}\n")
    }

    /**
     * Output filename
     *
     * @return Output File
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output File. Output filename
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): PHGtoGFAv1Plugin {
        myOutputFile = PluginParameter(myOutputFile, value)
        return this
    }

    /**
     * Set to true to have adjacent reference ranges fully
     * connected
     *
     * @return Fully Connected
     */
    fun fullyConnected(): Boolean {
        return myFullyConnected.value()
    }

    /**
     * Set Fully Connected. Set to true to have adjacent reference
     * ranges fully connected
     *
     * @param value Fully Connected
     *
     * @return this plugin
     */
    fun fullyConnected(value: Boolean): PHGtoGFAv1Plugin {
        myFullyConnected = PluginParameter(myFullyConnected, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "PHG to GFAv1 Format"
    }

    override fun getToolTipText(): String {
        return "PHG to GFAv1 Format"
    }

}
