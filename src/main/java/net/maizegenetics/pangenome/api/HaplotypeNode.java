package net.maizegenetics.pangenome.api;

import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.pangenome.db_loading.VCFFile;
import net.maizegenetics.taxa.TaxaList;

import java.util.*;

/**
 * @author Terry Casstevens Created June 19, 2017
 * @author Zack Miller
 * @author Karl Kremling
 * @author Jacob Washburn
 *
 * Class for representing haplotype nodes (HaplotypNode) , also known as anchor or inner anchor haplotypes. Haplotype
 * nodes contain are the basic unit of the graph and represent sequence blocks of a given single haplotype.  They are
 * derived from a single chromosome from a single line/genotype/variety. In many cases, a haplotype node may also use
 * the consensus sequence of very similar haplotypes from multiple lines/genotypes/varieties.
 */
public class HaplotypeNode implements Comparable<HaplotypeNode> {

    private static final Optional<List<VariantInfo>> EMPTY_VARIANT_INFOS = Optional.empty();

    private final HaplotypeSequence myHaplotypeSequence;
    private final TaxaList myTaxaList;
    private final int myHapId;
    private final int myGenomeFileID;
    private final int myGVCFFileID;
    private final String myASMContig;
    private final int myASMStartCoordinate;
    private final int myASMEndCoordinate;
    private final String myASMStrand;

    /**
     * Constructor
     *
     * @param haplotypeSequence sequence
     * @param taxaList taxa list
     * @param id hapid from database
     * @param asmContig assembly contig / chromosome
     * @param asmStart assembly start coordinate
     * @param asmEnd assembly end coordinate
     * @param genomeFileID genome file ID
     * @param gvcfFileID gvcf file ID
     */
    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, String asmContig, int asmStart, int asmEnd, String asmStrand, int genomeFileID, int gvcfFileID) {

        myHaplotypeSequence = haplotypeSequence;
        if (taxaList == null || taxaList.isEmpty()) {
            throw new IllegalArgumentException("HaplotypeNode: init: taxa list must contain at least one taxon.");
        }
        myTaxaList = taxaList;
        myHapId = id;

        myASMContig = asmContig;
        myASMStartCoordinate = asmStart;
        myASMEndCoordinate = asmEnd;
        myASMStrand = asmStrand;

        myGenomeFileID = genomeFileID;
        myGVCFFileID = gvcfFileID;

    }

    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id) {
        this(haplotypeSequence, taxaList, id, "0", 0, 0, ".", -1, -1);
    }

    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList) {
        this(haplotypeSequence, taxaList, -1, "0", 0, 0, ".", -1, -1);
    }

    /**
     * HaplotypeSequence object containing the nucleotide sequence and other sequence related information for a given
     * HaplotypeNode object.
     */
    public HaplotypeSequence haplotypeSequence() {
        return myHaplotypeSequence;
    }

    /**
     * List of taxa used when the consensus sequence was created or a single taxon.
     */
    public TaxaList taxaList() {
        return myTaxaList;
    }

    public int id() {
        return myHapId;
    }

    public String asmContig() {
        return myASMContig;
    }

    public int asmStart() {
        return myASMStartCoordinate;
    }

    public int asmEnd() {
        return myASMEndCoordinate;
    }

    public String asmStrand() { return myASMStrand;}

    public int genomeFileID() {
        return myGenomeFileID;
    }

    public int gvcfFileID() {
        return myGVCFFileID;
    }

    /**
     * Object containing the range of genomic coordinate values associated with a given HaplotypeNode and other details
     * associated with that range of values. These values correspond to the reference genome being used (Generally B73
     * in Maize).
     */
    public ReferenceRange referenceRange() {
        return myHaplotypeSequence.referenceRange();
    }

    /**
     * Number of taxa used when consensus sequence was created or one if this is an original sequence.
     *
     * @return number of taxa
     */
    public int numTaxa() {
        return myTaxaList.numberOfTaxa();
    }

    @Override
    public String toString() {
        return "HaplotypeNode{" +
                "myHaplotypeSequence=" + myHaplotypeSequence +
                ", myTaxaList=" + Arrays.deepToString(myTaxaList.toArray()) +
                ", myReferenceRange=" + referenceRange() +
                '}';
    }

    /**
     * These correspond to the VariantContext but holds less information for memory efficiency.
     * For reference blocks, isVariant will be false and refAllele may equal "REF" rather than
     * the actual allele for the first base pair of the block.
     * For that reason, converting VariantInfo to VariantContext will require
     * a copy of the reference genome sequence to get the actual refAllele values.
     *
     * @return variant infos
     */
    public Optional<List<VariantInfo>> variantInfos() {

        if (myGVCFFileID == -1) return EMPTY_VARIANT_INFOS;

        // Here - we need to read the gvcf file, then grab
        // all the variants based on start/end of the range
        // This code assumes that myGvcfIdToFilePath has the local file
        // path for the gvcf file, and that the user has downloaded all
        // required gvcfs, including the corresponding tabix files

        String chrom = myHaplotypeSequence.referenceRange().chromosome().getName();
        int startPos = myHaplotypeSequence.referenceRange().start();
        int endPos = myHaplotypeSequence.referenceRange().end();

        String gvcfFilePath = VariantUtils.gvcfFilePath(myGVCFFileID);
        VCFFileReader variantReader;
        try {
            // Get reader from cache
            variantReader = VCFFile.vcfFileReader(gvcfFilePath, null);
        } catch (Exception e) {
            throw new IllegalStateException("HaplotypeNode: variantInfos: error getting VCFFileReader for: " + gvcfFilePath, e);
        }

        // Read just that part of the VCF file that overlaps the chrom/range for
        // the reference range associated with this haplotype node.
        CloseableIterator<VariantContext> iterator = variantReader.query(chrom, startPos, endPos);

        ArrayList<VariantInfo> variantInfoList = new ArrayList<>();
        while (iterator.hasNext()) {
            VariantContext currentRecord = iterator.next();
            VariantInfo vi = ConvertVariantContextToVariantInfo.convertContextToInfo(currentRecord, 0);
            variantInfoList.add(vi);
        }
        iterator.close();

        // Return reader for this file to the cache
        VCFFile.vcfFileReader(gvcfFilePath, variantReader);

        return Optional.of(variantInfoList);
    }

    @Override
    public int compareTo(HaplotypeNode o) {
        int result = myHaplotypeSequence.referenceRange().compareTo(o.haplotypeSequence().referenceRange());
        if (result != 0) {
            return result;
        }
        return myTaxaList.taxaName(0).compareTo(o.taxaList().taxaName(0));
    }

    public Optional<List<VariantContext>> variantContexts() {
        throw new UnsupportedOperationException("variantContexts are no longer available from HaplotypeNode");
    }

    /**
     * These will be instantiated only when requested.
     */
    public static class VariantInfo {

        private final String chromosome;
        private final int start;
        private final int end;
        private final String genotypeString;
        private final String refAlleleString;
        private final String altAlleleString;
        private final boolean isVariant;
        private final long variantLong;

        private final int[] alleleDepths;

        private final String asmChr;
        private final int asmStart;
        private final int asmEnd;
        private final String asmStrand;

        //fields with package level access
        public static final String Ref = "REF";
        public static final String NonRef = "NON_REF";
        public static final String missing = "N";

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, long varLong) {
            this(chr, startPos, endPos, genotype, refAllele, altAllele, isVariant, null, varLong, "0", 0, 0, ".");

        }

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, long varLong,
                           String asmChr, int asmStart, int asmEnd, String asmStrand) {
            this(chr, startPos, endPos, genotype, refAllele, altAllele, isVariant, null, varLong, asmChr, asmStart, asmEnd, asmStrand);
        }

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, int[] alleleDepths) {
            this(chr, startPos, endPos, genotype, refAllele, altAllele, isVariant, alleleDepths, 0l, "0", 0, 0, ".");
        }

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, int[] alleleDepths,
                           String asmChr, int asmStart, int asmEnd, String asmStrand) {
            this(chr, startPos, endPos, genotype, refAllele, altAllele, isVariant, alleleDepths, 0l, asmChr, asmStart, asmEnd, asmStrand);
        }

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, int[] alleleDepths, long varLong,
                           String asmChr, int asmStart, int asmEnd, String asmStrand) {
            chromosome = chr;
            start = startPos;
            end = endPos;
            genotypeString = genotype;
            refAlleleString = refAllele;
            altAlleleString = altAllele;

            this.isVariant = isVariant;
            this.alleleDepths = alleleDepths;
            this.variantLong = varLong;

            this.asmChr = asmChr;
            this.asmStart = asmStart;
            this.asmEnd = asmEnd;
            this.asmStrand = asmStrand;
        }

        public String chromosome() {
            return chromosome;
        }

        public int start() {
            return start;
        }

        public int end() {
            return end;
        }

        public String genotypeString() {
            return genotypeString;
        }

        public String refAlleleString() {
            return refAlleleString;
        }

        public String altAlleleString() {
            return altAlleleString;
        }

        public boolean isVariant() {
            return isVariant;
        }

        public boolean isIndel() {
            return isVariant && ((start != end) || (altAlleleString.length() > 1));
        }

        public boolean isSymbolic() {
            return genotypeString.startsWith("<");
        }

        public int length() {
            return end - start + 1;
        }

        /**
         * @return an int array of ref depth, alt depth
         */
        public int[] depth() {
            return alleleDepths;
        }

        public long toLong() {
            if (variantLong == 0l) {
                throw new IllegalStateException("VariantInfo.toLong(): variantLong is not set.");
            }
            return variantLong;
        }

        public String asmChr() {
            return asmChr;
        }

        public int asmStart() {
            return asmStart;
        }

        public int asmEnd() {
            return asmEnd;
        }

        public String asmStrand() {
            return asmStrand;
        }
    }

}
