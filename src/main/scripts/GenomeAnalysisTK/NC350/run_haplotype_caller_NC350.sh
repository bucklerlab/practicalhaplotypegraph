#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/NC350_HL5WNCCXX_L4.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/NC350_HFFGKADXX_TGACCA.bam \
   -I /SSD/terry/haplotype_caller/bam_files/NC350_HB1JCADXX_TGACCA.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o NC350_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant NC350_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o NC350_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V NC350_haplotype_caller_output.vcf \
   -o NC350_haplotype_caller_output.fasta

