--liquibase formatted sql

--## create read_mapping and read_mapping_group tables for postgres and sqlite

--changeset lcj34:createReadMappingTable_sqlite dbms:sqlite
DROP TABLE IF EXISTS read_mapping;
CREATE TABLE read_mapping(read_mapping_id INTEGER PRIMARY KEY, genoid INTEGER, file_group_name TEXT, method_id INTEGER, mapping_data BLOB);

DROP TABLE IF EXISTS read_mapping_group;
CREATE TABLE read_mapping_group(rm_group_id INTEGER PRIMARY KEY, read_mapping_id INTEGER, name TEXT, description TEXT);


--changeset lcj34:createReadMappingTable_postgres dbms:postgresql
DROP TABLE IF EXISTS read_mapping;
CREATE TABLE read_mapping(read_mapping_id SERIAL PRIMARY KEY, genoid INTEGER, file_group_name TEXT, method_id INTEGER, mapping_data bytea);

DROP TABLE IF EXISTS read_mapping_group;
CREATE TABLE read_mapping_group(rm_group_id SERIAL PRIMARY KEY, read_mapping_id INTEGER, name TEXT, description TEXT);

--## index commands are the same for both databases
--changeset lcj34:create_read_mapping_unique_idx
CREATE UNIQUE INDEX read_mapping_unique_idx on read_mapping (genoid, method_id, file_group_name);

--changeset lcj34:create_read_mapping_genoid_idx
CREATE INDEX read_mapping_genoid_idx on read_mapping (genoid);
