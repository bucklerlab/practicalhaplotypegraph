package net.maizegenetics.pangenome.hapcollapse;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.Taxon;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ConsensusProcessingUtilsTest {
    @Test
    public void testFindTaxonRefCoverageRefBlocks() {
        //Unit Testing
        // RefAltData findTaxonRefCoverage(Position startPos, int maxRefLen, TaxaList tList, Map<Taxon, RangeMap<Position, RefAltData>> taxonToPosDataMap, double maxError)

        //Test RefBlock
        TaxaList tl = new TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0:Mo17_0")).build();
        int maxRefLength = 50;

        Map<Taxon, RangeMap<Position, RefAltData>> variantMapping = new HashMap<>();

        //RefAltData(int variantId, String chr, int pos, int length, int refDepth, int altDepth)
        RangeMap<Position, RefAltData> b73Map = TreeRangeMap.create();
        b73Map.put(Range.closed(Position.of(1,25),Position.of(1,165)), new RefAltData(-1,"1",25,141,5,0));

        RangeMap<Position, RefAltData> cml247Map = TreeRangeMap.create();
        cml247Map.put(Range.closed(Position.of(1,40),Position.of(1,160)), new RefAltData(-1,"1",40,121,5,0));


        RangeMap<Position, RefAltData> mo17Map = TreeRangeMap.create();
        mo17Map.put(Range.closed(Position.of(1,30),Position.of(1,100)), new RefAltData(-1,"1",30,71,5,0));
        mo17Map.put(Range.closed(Position.of(1,110),Position.of(1,160)), new RefAltData(-1,"1",110,55,5,0));


        variantMapping.put(new Taxon("B73"),b73Map);
        variantMapping.put(new Taxon("CML247"),cml247Map);
        variantMapping.put(new Taxon("Mo17"),mo17Map);

        Position startPos = Position.of(1,100);
        RefAltData refData1 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, maxRefLength, tl, variantMapping, .32, .2);

        startPos = Position.of(1,101);
        RefAltData refData2 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150 - 101, tl, variantMapping, .32, .2);


        Assert.assertEquals("Error FirstRefRange variantIds do not match:",-1,refData1.getVariantId());
        Assert.assertEquals("Error FirstRefRange chromosomes do not match:","1",refData1.getChrom());
        Assert.assertEquals("Error FirstRefRange position do not match:",100,refData1.getPosition());
        Assert.assertEquals("Error FirstRefRange length do not match:",1,refData1.getLen());


        Assert.assertEquals("Error SecondRefRange variantIds do not match:",-1,refData2.getVariantId());
        Assert.assertEquals("Error SecondRefRange chromosomes do not match:","1",refData2.getChrom());
        Assert.assertEquals("Error SecondRefRange position do not match:",101,refData2.getPosition());
        Assert.assertEquals("Error SecondRefRange length do not match:",49,refData2.getLen());

    }

    @Test
    public void testFindTaxonRefCoverageRefBlocksLowCoverage() {
        //Unit Testing
        // RefAltData findTaxonRefCoverage(Position startPos, int maxRefLen, TaxaList tList, Map<Taxon, RangeMap<Position, RefAltData>> taxonToPosDataMap, double maxError)

        //Test RefBlock
        TaxaList tl = new TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0:Mo17_0")).build();


        Map<Taxon, RangeMap<Position, RefAltData>> variantMapping = new HashMap<>();

        //RefAltData(int variantId, String chr, int pos, int length, int refDepth, int altDepth)
        RangeMap<Position, RefAltData> b73Map = TreeRangeMap.create();
        b73Map.put(Range.closed(Position.of(1,25),Position.of(1,165)), new RefAltData(-1,"1",25,141,5,0));

        RangeMap<Position, RefAltData> cml247Map = TreeRangeMap.create();
        cml247Map.put(Range.closed(Position.of(1,40),Position.of(1,160)), new RefAltData(-1,"1",40,121,5,0));


        RangeMap<Position, RefAltData> mo17Map = TreeRangeMap.create();
        mo17Map.put(Range.closed(Position.of(1,30),Position.of(1,100)), new RefAltData(-1,"1",30,71,5,0));
        mo17Map.put(Range.closed(Position.of(1,105),Position.of(1,160)), new RefAltData(-1,"1",105,56,5,0));


        variantMapping.put(new Taxon("B73"),b73Map);
        variantMapping.put(new Taxon("CML247"),cml247Map);
        variantMapping.put(new Taxon("Mo17"),mo17Map);

        int maxRefLength = 50;
        Position startPos = Position.of(1,100);
        RefAltData refData1 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, maxRefLength, tl, variantMapping, .32, .67);

        startPos = Position.of(1,101);
        RefAltData refData2 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-101, tl, variantMapping, .32, .67);

        startPos = Position.of(1,102);
        RefAltData refData3 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-102, tl, variantMapping, .32, .67);

        startPos = Position.of(1,103);
        RefAltData refData4 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-103, tl, variantMapping, .32, .67);

        startPos = Position.of(1,104);
        RefAltData refData5 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-104, tl, variantMapping, .32, .67);

        startPos = Position.of(1,105);
        RefAltData refData6 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-105, tl, variantMapping, .32, .67);



        Assert.assertEquals("Error FirstRefRange variantIds do not match:",-1,refData1.getVariantId());
        Assert.assertEquals("Error FirstRefRange chromosomes do not match:","1",refData1.getChrom());
        Assert.assertEquals("Error FirstRefRange position do not match:",100,refData1.getPosition());
        Assert.assertEquals("Error FirstRefRange length do not match:",1,refData1.getLen());

        Assert.assertEquals("Error SecondRefRange variantIds do not match:",0,refData2.getVariantId());
        Assert.assertEquals("Error SecondRefRange chromosomes do not match:","1",refData2.getChrom());
        Assert.assertEquals("Error SecondRefRange position do not match:",101,refData2.getPosition());
        Assert.assertEquals("Error SecondRefRange length do not match:",1,refData2.getLen());

        Assert.assertEquals("Error ThirdRefRange variantIds do not match:",0,refData3.getVariantId());
        Assert.assertEquals("Error ThirdRefRange chromosomes do not match:","1",refData3.getChrom());
        Assert.assertEquals("Error ThirdRefRange position do not match:",102,refData3.getPosition());
        Assert.assertEquals("Error ThirdRefRange length do not match:",1,refData3.getLen());

        Assert.assertEquals("Error FourthRefRange variantIds do not match:",0,refData4.getVariantId());
        Assert.assertEquals("Error FourthRefRange chromosomes do not match:","1",refData4.getChrom());
        Assert.assertEquals("Error FourthRefRange position do not match:",103,refData4.getPosition());
        Assert.assertEquals("Error FourthRefRange length do not match:",1,refData4.getLen());

        Assert.assertEquals("Error FifthRefRange variantIds do not match:",0,refData5.getVariantId());
        Assert.assertEquals("Error FifthRefRange chromosomes do not match:","1",refData5.getChrom());
        Assert.assertEquals("Error FifthRefRange position do not match:",104,refData5.getPosition());
        Assert.assertEquals("Error FifthRefRange length do not match:",1,refData5.getLen());

        Assert.assertEquals("Error SixthRefRange variantIds do not match:",-1,refData6.getVariantId());
        Assert.assertEquals("Error SixthRefRange chromosomes do not match:","1",refData6.getChrom());
        Assert.assertEquals("Error SixthRefRange position do not match:",105,refData6.getPosition());
        Assert.assertEquals("Error SixthRefRange length do not match:",45,refData6.getLen());
    }

    @Test
    public void testFindTaxonRefCoverageSNPs() {
        //Test RefBlock and SNP
        TaxaList tl = new TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0:Mo17_0")).build();

        Map<Taxon, RangeMap<Position, RefAltData>> variantMapping = new HashMap<>();

        //RefAltData(int variantId, String chr, int pos, int length, int refDepth, int altDepth)
        RangeMap<Position, RefAltData> b73Map = TreeRangeMap.create();
        b73Map.put(Range.closed(Position.of(1,25),Position.of(1,120)), new RefAltData(-1,"1",25,96,5,0));
        b73Map.put(Range.closed(Position.of(1,122),Position.of(1,160)), new RefAltData(-1,"1",122,39,5,0));

        RangeMap<Position, RefAltData> cml247Map = TreeRangeMap.create();
        cml247Map.put(Range.closed(Position.of(1,40),Position.of(1,120)), new RefAltData(-1,"1",40,81,5,0));
        cml247Map.put(Range.closed(Position.of(1,121),Position.of(1,121)), new RefAltData(20,"1",121,1,0,6));
        cml247Map.put(Range.closed(Position.of(1,122),Position.of(1,160)), new RefAltData(-1,"1",122,39,5,0));


        RangeMap<Position, RefAltData> mo17Map = TreeRangeMap.create();
        mo17Map.put(Range.closed(Position.of(1,30),Position.of(1,105)), new RefAltData(-1,"1",30,76,5,0));
        mo17Map.put(Range.closed(Position.of(1,106),Position.of(1,106)), new RefAltData(10,"1",106,1,0,5));
        mo17Map.put(Range.closed(Position.of(1,107),Position.of(1,120)), new RefAltData(-1,"1",107,14,5,0));
        mo17Map.put(Range.closed(Position.of(1,121),Position.of(1,121)), new RefAltData(20,"1",121,1,0,5));
        mo17Map.put(Range.closed(Position.of(1,122),Position.of(1,160)), new RefAltData(-1,"1",122,39,5,0));



        variantMapping.put(new Taxon("B73"),b73Map);
        variantMapping.put(new Taxon("CML247"),cml247Map);
        variantMapping.put(new Taxon("Mo17"),mo17Map);

        int maxRefLength = 50;
        Position startPos = Position.of(1,100);
        RefAltData refData1 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-100, tl, variantMapping, .32, .2);

        startPos = Position.of(1,106);
        RefAltData refData2 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150-106, tl, variantMapping, .32, .2);

        startPos = Position.of(1,107);
        RefAltData refData3 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150 - 107, tl, variantMapping, .32, .2);

        startPos = Position.of(1,121);
        RefAltData refData4 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150 - 121, tl, variantMapping, .32, .2);

        startPos = Position.of(1,122);
        RefAltData refData5 = ConsensusProcessingUtils.findTaxonRefCoverage(startPos, 150 - 122, tl, variantMapping, .32, .2);

        Assert.assertEquals("Error FirstRefRange variantIds do not match:",-1,refData1.getVariantId());
        Assert.assertEquals("Error FirstRefRange chromosomes do not match:","1",refData1.getChrom());
        Assert.assertEquals("Error FirstRefRange position do not match:",100,refData1.getPosition());
        Assert.assertEquals("Error FirstRefRange length do not match:",6,refData1.getLen());


        Assert.assertEquals("Error SecondRefRange variantIds do not match:",0,refData2.getVariantId());
        Assert.assertEquals("Error SecondRefRange chromosomes do not match:","1",refData2.getChrom());
        Assert.assertEquals("Error SecondRefRange position do not match:",106,refData2.getPosition());
        Assert.assertEquals("Error SecondRefRange length do not match:",1,refData2.getLen());


        Assert.assertEquals("Error ThirdRefRange variantIds do not match:",-1,refData3.getVariantId());
        Assert.assertEquals("Error ThirdRefRange chromosomes do not match:","1",refData3.getChrom());
        Assert.assertEquals("Error ThirdRefRange position do not match:",107,refData3.getPosition());
        Assert.assertEquals("Error ThirdRefRange length do not match:",14,refData3.getLen());


        Assert.assertEquals("Error FourthRefRange variantIds do not match:",20,refData4.getVariantId());
        Assert.assertEquals("Error FourthRefRange chromosomes do not match:","1",refData4.getChrom());
        Assert.assertEquals("Error FourthRefRange position do not match:",121,refData4.getPosition());
        Assert.assertEquals("Error FourthRefRange length do not match:",1,refData4.getLen());

        Assert.assertEquals("Error FifthRefRange variantIds do not match:",-1,refData5.getVariantId());
        Assert.assertEquals("Error FifthRefRange chromosomes do not match:","1",refData5.getChrom());
        Assert.assertEquals("Error FifthRefRange position do not match:",122,refData5.getPosition());
        Assert.assertEquals("Error FifthRefRange length do not match:",28,refData5.getLen());

    }
}
