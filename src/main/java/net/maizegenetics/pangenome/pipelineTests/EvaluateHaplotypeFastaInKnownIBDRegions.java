package net.maizegenetics.pangenome.pipelineTests;

import com.google.common.base.CharMatcher;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.dna.snp.io.ReadBedfile;
import net.maizegenetics.dna.snp.io.ReadBedfile.BedFileRange;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Unit test to evaluate fasta file generated from GVCF files for anchors in IBD regions to B73
 * Get all the anchorIDs in the IBD region of chromosome 10
 * Read B73 reference sequences for IBD anchors
 * Read fasta file obtained with the practical haplotype pipeline (alternative).
 * Power: how many of the anchor-fragments that are in IBD perfectly match the B73 ref?
 * Question: how frequently PHG call variants in regions that should be identical?
 * Created by mm2842 on 6/22/17.
 */
public class EvaluateHaplotypeFastaInKnownIBDRegions {

    public static final String workdir = "/Users/edbuckler/Box Sync/Hackhaton_files_June_2017/W22_unit_test_files/";
   // public static final String workdir = "/Users/mm2842/Documents/Regulatory_Regions_Project/PHG_Hackhaton/";
    public static final String anchorIDFile = workdir + "onehundred_IBD.txt";//"anchorID_IBD_W22_B73.txt";
    public static final String fastaAlternatives = workdir+"chrom10_anchors_W22.fasta";
    public static final String resultsfile = workdir + "Results_anchorID_IBD_W22_B73a.txt";
    private static String referenceGenomeFile = workdir+"Zea_mays.AGPv4.dna.toplevel.fa.gz";
    private static String bedFileAnchors = workdir + "chrom10_anchor_genome_position.bed";
    private static GenomeSequence refGenomeSequence;


    /**
     * Reads in list of anchor region numbers that are IBD
     */
    public static List<String> readAnchorIds() throws IOException {
        List<String> anchorIDs = Files.readAllLines(Paths.get(anchorIDFile)).stream().limit(100).collect(Collectors.toList());
        //List<String> anchorIDs = Files.readAllLines(Paths.get(anchorIDFile)).stream().collect(Collectors.toList());
        return anchorIDs;
    }


    public static Optional<Boolean> isAltSeqSameAsRefExcludingNs(String referenceSeq, String alternativeSeq){
        //clean leading and training N
        String regexSearch=alternativeSeq
                .replaceAll("^N+", "")
                .replaceAll("N+$", "")
                .replaceAll("N+","[ACGT]+");
        if(CharMatcher.is('[').countIn(regexSearch)>4) {return Optional.empty();}
        if(regexSearch.length()<1000) return Optional.empty();
        Pattern pattern = Pattern.compile(regexSearch);
        Matcher matcher = pattern.matcher(referenceSeq);
        boolean found = matcher.find();
        return Optional.of(found);
    }

    public static Map<String, String> getMapFromFasta(String IBDsequences) throws IOException {
        List<String> fastaFile = Files.readAllLines(Paths.get(IBDsequences));
        Map<String, String>  MapIBDs = new TreeMap<>();
        String header = "";
        for (String line: fastaFile){
            if (line.startsWith(">")) {
                header = line.replace(">","");
            } else {
                MapIBDs.put(header, line);
            }
        }
        return MapIBDs;
    }

    public static void main(String[] args) {
        String refSeq   ="ACCCCCGGGGGGGGG";
        String altSeqIBD="NNNACCCCNNGGGGGGGG";
        String altSeqNon="ACCCCNNGGGGTGGG";
        System.out.println("NNNNNCCCCCCCC".replaceAll("^N+", ""));
        System.out.println(isAltSeqSameAsRefExcludingNs(refSeq,altSeqIBD));
        System.out.println(isAltSeqSameAsRefExcludingNs(refSeq,altSeqNon));

        //System.exit(1);
        Map<String, BedFileRange> anchorMap=ReadBedfile.getRanges(bedFileAnchors).stream()
                .sorted(Comparator.comparing(BedFileRange::start))
                .collect(Collectors.toMap(BedFileRange::name, ar -> ar));
        refGenomeSequence= GenomeSequenceBuilder.instance(referenceGenomeFile);


        boolean appendToFile = false;
        BufferedWriter reporter = null;
        int perfectMatch=0, noMatch=0, poorSequence=0;
        int perfectBp=0, noMatchBp=0;
        try {
            reporter = new BufferedWriter(new FileWriter(resultsfile, appendToFile));
            Map<String, String> mapIBDanchorsW22 = getMapFromFasta(fastaAlternatives); //before alignment
            System.out.println("anchorID\tMatch\tQuerySeqLength");
            //for(String anchorID : readAnchorIds() ){
            for(String anchorID : anchorMap.keySet()){
                String reference = NucleotideAlignmentConstants.nucleotideBytetoString(
                        refGenomeSequence.chromosomeSequence(Chromosome.instance("10"),
                        anchorMap.get(anchorID).start()-1000, anchorMap.get(anchorID).end()+1000));
                String altAnchor = mapIBDanchorsW22.get(anchorID);
                Optional<Boolean> result=isAltSeqSameAsRefExcludingNs(reference,altAnchor);
                if(result.isPresent()) {
                    if(result.get()) {perfectMatch++; perfectBp+=altAnchor.length();}
                    else {noMatch++; noMatchBp+=altAnchor.length();}
                    System.out.println(anchorID+"\t"+result.get()+"\t"+altAnchor.length());
                } else {
                    poorSequence++;
                }
            }
            reporter.flush();
            reporter.close();
        } catch(IOException ioe){
            ioe.printStackTrace();
        }
        System.out.println("perfectMatch = " + perfectMatch);
        System.out.println("noMatch = " + noMatch);
        System.out.println("poorSequence = " + poorSequence);
        System.out.println("perfectBp = " + perfectBp);
        System.out.println("noMatchBp = " + noMatchBp);
        System.out.println("Perfect MatchRate = "+(double)perfectMatch/(perfectMatch+noMatch));
        System.out.println("PoorSequence Rate = "+(double)(perfectMatch+noMatch)/(perfectMatch+noMatch+poorSequence));
    }
}
