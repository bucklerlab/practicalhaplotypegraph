package net.maizegenetics.pangenome.smallseq

import com.google.common.collect.Range
import com.google.common.collect.TreeRangeSet
import net.maizegenetics.dna.snp.ImportUtils
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.dna.map.Position
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import java.io.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.hapCalling.DiploidPathPlugin
import net.maizegenetics.pangenome.hapCalling.FastqToMappingPlugin
import net.maizegenetics.plugindef.ParameterCache
import org.junit.jupiter.api.extension.ExtendWith
import java.nio.file.Files
import java.nio.file.Paths
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.hapCalling.BestHaplotypePathPlugin
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.assertDoesNotThrow
import java.sql.Connection
import kotlin.io.path.pathString

@ExtendWith(SmallSeqExtension::class)
class SmallSeqIT {

    @Test
    fun testNumberOfSites() {
        val resultGT = ImportUtils.read(SmallSeqExtension.resultFilePath)
        val resultNumSites = resultGT.numberOfSites()
        val resultSites = resultGT.positions()

        val anchorBedFile = Utils.getBufferedReader(SmallSeqExtension.anchorBEDFilePath).readLines()
        //build a Guava rangeSet of the anchor positions
        val rangeSet = TreeRangeSet.create<Position>()
        anchorBedFile.forEach {
            val split = it.split("\t")
            val chr = split[0]
            val start = split[1].toInt() +1 //Because BED is 0 based inclusive-exclusive
            val end = split[2].toInt()
            rangeSet.add(Range.closed(Position.of(chr, start), Position.of(chr, end)))
        }

        val originalSites = File(SmallSeqExtension.inputGVCFDir).walk().filter { it.isFile }
            .filter { it.name.endsWith("_filtered.g.vcf.gz") }
            .flatMap {
                VCFFileReader(it, false)
                    .use { reader -> reader.iterator().asSequence().filter { variantContext -> variantContext.alternateAlleles.size>1 }
                        .map { variantContext ->  Position.of(variantContext.contig,variantContext.start) } }
            }
            .toSet()

        assertEquals(originalSites.size, resultNumSites, "Total number of sites between SmallSeq.vcf and imputation output does not match")

        for (pos in resultSites) {
            assertTrue(originalSites.contains(pos), "Site $pos in imputation output was not found in original SmallSeq.vcf")
            assertTrue(rangeSet.contains(pos), "Site $pos in imputation output was not found in anchor BED file")
        }

    }

    @Test
    fun testContainsAllTaxa() {
        val originalGT = ImportUtils.read(SmallSeqExtension.originalFilePath)
        val resultGT = ImportUtils.read(SmallSeqExtension.resultFilePath)

        //The expected number of taxa is now originalGT.numberOfTaxa() * 2 because we are now including the WGS and GBS samples
        assertEquals(originalGT.numberOfTaxa() * 2, resultGT.numberOfTaxa(), "Number of taxa between original SmallSeq.vcf and output differ")

        resultGT.taxa().forEach {
            val originalTaxonName: String = it.name
                .replace(".gbs", "")
                .replace("_gbs","")
                .replace("_wgs","")
                .replace("_multimap.txt", "")
                .replace("_R1","")
            assertTrue(originalGT.taxa().contains(Taxon(originalTaxonName)), "Taxa $originalTaxonName in result VCF was not found in original SmallSeq.vcf")
        }
    }

    @Test
    fun testDiploidPaths() {
        val readMappingMethod = "READ_MAP_TEST"
        val pathMethod = "PATH_DIPLOID_TEST"
        val lineA = "LineA"
        val lineB = "LineB"
        val lineList = listOf(lineA, lineB) // Used in assertions
        val directory = "${SmallSeqExtension.baseDir}/diploidPathTest/"
        File(directory).mkdirs()
        val lineAFile = "${SmallSeqExtension.dataDir}/LineA_R1.fastq" //these need to stay at SmallSeqExtension.dataDir as they are merged later
        val lineBFile = "${SmallSeqExtension.dataDir}/LineB_R1.fastq"

        // Concatenate LineA_R1.fastq and LineB_R1.fastq to create LineA_LineB.fastq
        val concatAB = "LineA_LineB.fastq"
        val concatABPath = "${directory}/$concatAB"
        val cultivar = "A_B_het"
        File(concatABPath).bufferedWriter().use { myWriter ->
            Files.newBufferedReader(Paths.get(lineAFile)).useLines { lines ->
                lines.forEach { myWriter.write("$it\n") }
            }
            Files.newBufferedReader(Paths.get(lineBFile)).useLines { lines ->
                lines.forEach { myWriter.write("$it\n") }
            }
        }

        // Create keyfile
        val keyfileReads = "${directory}/diploidPathTest_keyfile_reads.txt"
        File(keyfileReads).bufferedWriter().use {
            it.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
            it.write("$cultivar\tmy_lane\t$concatAB\tmy_plate\n")
        }

        ParameterCache.load(SmallSeqExtension.configFilePath)
        val graphData = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .methods("GATK_PIPELINE")
            .performFunction(null)

        FastqToMappingPlugin(null, false)
            .keyFile(keyfileReads)
            .indexFile("${SmallSeqExtension.pangenomeDir}/pangenome_GATK_PIPELINE_k21w11I90G.mmi")
//            .fastqDir(SmallSeqExtension.dataDir)
            .fastqDir(directory)
            .methodName(readMappingMethod)
            .performFunction(graphData)

        DiploidPathPlugin(null, false)
            .keyFile("${directory}/diploidPathTest_keyfile_reads_pathKeyFile.txt")
            .readMethodName(readMappingMethod)
            .pathMethodName(pathMethod)
            .minTaxaPerRange(3)
            .splitNodes(false)
            .splitTransitionProb(0.9)
            .minTransitionProbability(0.5)
            .inbreedingCoef(0.1)
            .performFunction(graphData)

        // Test diploid path finding
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false)
        val phg = PHGdbAccess(connection)

        val query = "SELECT path_id, line_name, paths_data FROM paths, genotypes, methods WHERE paths.genoid=genotypes.genoid " +
                     "AND line_name='$cultivar' AND paths.method_id=methods.method_id AND methods.name='$pathMethod'"
        val sqlResult = connection.createStatement().executeQuery(query)
        sqlResult.next()
        val pathList = DBLoadingUtils.decodePathsForMultipleLists(sqlResult.getBytes("paths_data"))
        sqlResult.close()

        assertEquals(2, pathList.size) // Two paths because diploid: LineA and LineB

        // TODO: More and better asserts - use methods from PHGdbAccess?
        for (path in pathList) {
            val hapidStr = path.joinToString(",", "(", ")")
            val hapidQuery = "SELECT line_name, chrom FROM haplotypes h, reference_ranges r, " +
                    "genotypes g, gamete_haplotypes gh, gametes gam WHERE h.ref_range_id=r.ref_range_id AND g.genoid=gam.genoid " +
                    "AND gam.gameteid=gh.gameteid AND gh.gamete_grp_id=h.gamete_grp_id AND haplotypes_id in $hapidStr"
            val hapidResult = connection.createStatement().executeQuery(hapidQuery)
            while (hapidResult.next()) {
                val lineName = hapidResult.getString("line_name")
                assertTrue(lineList.contains(lineName))

                val chromNumber = hapidResult.getInt("chrom")
                assertEquals(1, chromNumber)
            }
            hapidResult.close()
        }

        //Test whether imputing paths a second time skips all the paths
        //Rerunning path finding should not add any paths and should not throw an error
        val startingNumberOfPaths = numberOfPathsForMethod(pathMethod, connection)
        assertDoesNotThrow {
            DiploidPathPlugin(null, false)
            .keyFile("${directory}/diploidPathTest_keyfile_reads_pathKeyFile.txt")
            .readMethodName(readMappingMethod)
            .pathMethodName(pathMethod)
            .minTaxaPerRange(3)
            .splitNodes(false)
            .splitTransitionProb(0.9)
            .minTransitionProbability(0.5)
            .inbreedingCoef(0.1)
            .performFunction(graphData)
        }
        val endingNumberOfPaths = numberOfPathsForMethod(pathMethod, connection)
        val numberOfPathsAdded = endingNumberOfPaths - startingNumberOfPaths
        assertEquals(0, numberOfPathsAdded, "Duplicate paths added to the database.")

        phg.close()
        connection.close()
    }

    private fun numberOfPathsForMethod(pathMethod: String, dbConn: Connection): Int {
        val query = "SELECT count(*) FROM paths, methods WHERE paths.method_id=methods.method_id AND methods.name='$pathMethod'"
        return dbConn.createStatement().use {
            val resultSet = it.executeQuery(query)
            if (resultSet.next()) {
                resultSet.getInt(1)
            } else 0
        }
    }

    @Test
    // TODO: Check each readMapping instead of just `LineA_wgs_wgsFlowcell_ReadMapping.txt` to make this more complete
    //       When doing this, keep in mind that the recombinant readMappings should have 2 taxa for their paths
    fun testBestHaplotypePathPlugin() {
        // Clean temporary directory - TODO: Make this unnecessary
        SmallSeqExtension.testTmpDirFile.mkdirs()
        FileUtils.cleanDirectory(SmallSeqExtension.testTmpDirFile)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        val graphData = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .methods(SmallSeqExtension.haplotypeMethod)
            .performFunction(null)

        val graph = graphData.getData(0).data as HaplotypeGraph

        BestHaplotypePathPlugin(null, false)
            .keyFile("${SmallSeqExtension.baseDir}/genotypingKeyFile_pathKeyFile.txt")
            .readMethodName(SmallSeqExtension.imputeMethod)
            .pathMethodName(SmallSeqExtension.pathMethod)
            .readMapFilename("${SmallSeqExtension.readMappingDir}/LineA_wgs_wgsFlowcell_ReadMapping.txt")
            .pathOutDirectory(SmallSeqExtension.testTmpDir)
            .splitConsensusNodes(true)
            .minTaxaPerRange(2)
            .removeRangesWithEqualCounts(false)
            .performFunction(graphData)

        // Create a hapid -> node map in order to get taxaList
        val hapIdToNodeMap = HashMap<Int, HaplotypeNode>()
        graph.nodeStream().forEach { hapIdToNodeMap.put(it.id(), it) }

        Files.list(SmallSeqExtension.testTmpDirFile.toPath()).forEach { tmpFile ->
            File(tmpFile.pathString).bufferedReader().use {
                it.lines().filter { line -> !(line.startsWith("#")) }
                    .map { line -> line.toInt()}
                    .map { line -> hapIdToNodeMap.get(line)?.taxaList() }
                    .forEach { line -> if (line != null) assertEquals("LineA", line.joinToString(",")) }
            }
        }
    }

    @Test
    fun testBuildGraphFromHapids() {
        val db = DBLoadingUtils.connection(false)
        val query = "SELECT haplotype_list_id FROM haplotype_list"
        val haplotypeListId = db.createStatement().use {
            val resultSet = it.executeQuery(query)
            resultSet.next()
            resultSet.getInt(1)
        }
        val haplotypeSet = PHGdbAccess(db).getHaplotypeList(haplotypeListId).toSortedSet()
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graph = graphBuilder
            .hapids(haplotypeSet)
            .includeSequences(false)
            .includeVariantContexts(false)
            .build()

        val expectedNumberOfNodes = haplotypeSet.size
        assertEquals(expectedNumberOfNodes, graph.numberOfNodes(),
            "Number of graph nodes does not equal number of haplotypes used to build it.")

        val methodQuery = "SELECT name FROM haplotypes, methods WHERE haplotypes.method_id=methods.method_id AND haplotypes_id=${haplotypeSet.first()}"
        val methodName = db.createStatement().use {
            val resultSet = it.executeQuery(methodQuery)
            resultSet.next()
            resultSet.getString(1)
        }
        val graphFromMethod = graphBuilder.methods(methodName).build()

        assertEquals(expectedNumberOfNodes, graphFromMethod.numberOfNodes(),
            "When method is specified, number of graph nodes does not equal number of haplotypes used to build it.")

    }

}
