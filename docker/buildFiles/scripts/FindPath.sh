#!/bin/bash -x

#
# This script starts with fastq files and generates haplotype
# counts using a Haplotype Graph.  Those counts are then used
# with a HMM algorithm to find the best path through the
# haplotype graph for each taxon.  Those pathes are written
# to text files containing a list of the haplotype node ids.
#

TEMP_DIR=/tempFileDir
OUTPUT_DIR=${TEMP_DIR}/outputDir
DATA_DIR=${TEMP_DIR}/data
REFERENCE_DIR=${DATA_DIR}/reference
FASTQ_DIR=${DATA_DIR}/fastq
PANGENOME_DIR=${OUTPUT_DIR}/pangenome

FASTQ_HAP_COUNT_DIR=${OUTPUT_DIR}/fastq_hap_count
if [ ! -d "$FASTQ_HAP_COUNT_DIR" ]; then
   mkdir $FASTQ_HAP_COUNT_DIR
fi

HAP_COUNT_BEST_PATH_DIR=${OUTPUT_DIR}/hap_count_best_path
if [ ! -d "$HAP_COUNT_BEST_PATH_DIR" ]; then
   mkdir $HAP_COUNT_BEST_PATH_DIR
fi

if [ ! -d "$PANGENOME_DIR" ]; then
   mkdir $PANGENOME_DIR
fi

# Base name of the haplotype fasta (i.e. no .fa extension)
BASE_HAPLOTYPE_NAME=$1
# Filename of configFile for db connection - used in FastqToHapCountPlugin
CONFIGFILE=${DATA_DIR}/$2
CONFIGFILE_NAME=$2
# Consensus Method used to create haplotypes in graph
CONSENSUS_METHOD=$3
# Reference FASTA file (i.e. Zea_mays.AGPv4.dna.toplevel.fa)
REFERENCE_FILE=${REFERENCE_DIR}/$4
# Name of method used to create hap counts
HAP_COUNT_METHOD=$5
# Name of method to be used to create paths through the graph
PATH_METHOD=$6

# The name of the file containing the reference ranges to keep
REF_RANGE_FILE='';
if [ ! -z "${7}" ]
then
   REF_RANGE_FILE=${DATA_DIR}/${8}
fi

# Haplotype Fasta file
HAPLOTYPE_FASTA="${PANGENOME_DIR}/${BASE_HAPLOTYPE_NAME}.fa"
# Haplotype Fasta Index file
HAPLOTYPE_INDEX="${PANGENOME_DIR}/{BASE_HAPLOTYPE_NAME}.fa.amb"

# Set maximum Java heap size if defined in config file
fullXmx=""
memoryString="`grep -m 1 '^Xmx=' ${CONFIGFILE} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
fi

#Extract out the taxa names to filter out of the graph for genotyping.
TAXA_LIST_FLAG=''
filterTaxaList="`grep -m 1 '^filterTaxaList=' ${CONFIGFILE} | cut -d'=' -f2`"
if [ ${filterTaxaList} != "" ]
then
   TAXA_LIST_FLAG="-taxa ${filterTaxaList}"
fi



#
# Check if Haplotype Fasta File exists and
# create if not.  The haplotype fasta file is
# generated from the haplotype graph.
#

if [ -f $HAPLOTYPE_FASTA ]; then
   echo "Haplotype Fasta File $HAPLOTYPE_FASTA exists."
else
   echo "Creating Haplotype Fasta File ${HAPLOTYPE_FASTA}."
   time /tassel-5-standalone/run_pipeline.pl $fullXmx -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIGFILE -methods $CONSENSUS_METHOD -includeVariantContexts false -endPlugin -WriteFastaFromGraphPlugin -outputFile $HAPLOTYPE_FASTA -endPlugin
fi

#
# Check if Haplotype Fasta Index File exists and
# create if not.  This is a bwa index needed by the
# next step in this pipeline.
#

if [ -f $HAPLOTYPE_INDEX ]; then
   echo "Haplotype Fasta Index File for $HAPLOTYPE_FASTA exists."
else
   echo "Creating Haplotype Fasta Index for ${HAPLOTYPE_FASTA}."
   time bwa index $HAPLOTYPE_FASTA
fi

#
# Use FastqToHapCountPlugin to generate counts of haplotypes from given
# fastq files and Haplotype Graph
#

cd $FASTQ_DIR
fastqfiles="`ls *.fastq | sed 's/.fastq//'`"
cd -

for file in $fastqfiles
do

echo $file

#
# These are fastq files, the taxon may not be in the DB.
# For the smallSeq tests, use $file (which has the extension stripped) as the taxon name
#

time /tassel-5-standalone/run_pipeline.pl $fullXmx -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIGFILE -methods $CONSENSUS_METHOD -includeVariantContexts true -endPlugin -FastqToHapCountPlugin -taxon $file -configFile $CONFIGFILE -haplotypeFile $HAPLOTYPE_FASTA -method $HAP_COUNT_METHOD -refFile $REFERENCE_FILE -rawReadFile ${FASTQ_DIR}/${file}.fastq -exportHaploFile ${FASTQ_HAP_COUNT_DIR}/${file}.txt -endPlugin

done

#
# Use HapCountBestPathToTextPlugin to find best path (HMM algorithm)
# through Haplotype Graph given counts from previous step.
# The paths are written to text files in $HAP_COUNT_BEST_PATH_DIR
#

REF_RANGE_FILE_FLAG='';
if [ ! -z "$REF_RANGE_FILE" ]
then
   REF_RANGE_FILE_FLAG="-refRangeFile ${f}"
fi

time /tassel-5-standalone/run_pipeline.pl $fullXmx -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIGFILE -methods $CONSENSUS_METHOD -includeVariantContexts true -endPlugin -HapCountBestPathToTextPlugin -configFile ${CONFIGFILE} ${REF_RANGE_FILE_FLAG} ${TAXA_LIST_FLAG} -outputDir $HAP_COUNT_BEST_PATH_DIR -hapCountMethod $HAP_COUNT_METHOD -pMethod $PATH_METHOD -endPlugin

