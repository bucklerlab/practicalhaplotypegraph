package net.maizegenetics.pangenome.api

import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import java.util.*

fun buildSimpleDummyGraph(): HaplotypeGraph {

    val gvcfIdToFile = mutableMapOf<Int, String>()
    val gvcfDir = "${System.getProperty("user.home")}/temp/remoteGvcfs/"
    val numOfGVCFFiles = 10
    0.until(numOfGVCFFiles).forEach { gvcfIdToFile[it] = "${gvcfDir}gvcf_${it}.gvcf.gz" }
    VariantUtils.setGvcfRemoteToLocalFiles(gvcfIdToFile)

    val alleles = listOf("A", "T", "G", "C")
    //create 9 sequences of length 100
    val random = Random()
    val sequences = (0 until 9).map {
        (0 until 100)
            .joinToString("") { alleles[random.nextInt(4)] }
    }

    // ReferenceRange(String referenceName, Chromosome chromosome, int start, int end, int id)
    val ranges = listOf(
        ReferenceRange("refName", Chromosome.instance("1"), 1, 100, 1, "genic"),
        ReferenceRange("refName", Chromosome.instance("1"), 300, 400, 2, "genic"),
        ReferenceRange("refName", Chromosome.instance("1"), 600, 800, 3, "genic")
    )

    val hapSequences = sequences.mapIndexed { index, s -> HaplotypeSequence.getInstance(s, ranges[index / 3], 1.0, "") }

    val taxa = listOf<TaxaList>(
        TaxaListBuilder().add("sample1").build(),
        TaxaListBuilder().add("sample2").build(),
        TaxaListBuilder().add("sample3").build()
    )

    // turn hapSequences into a node
    // public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, String asmContig,
    // int asmStart, int asmEnd, String asmStrand, int genomeFileID, int gvcfFileID)
    val nodes = hapSequences.mapIndexed { index, sequence ->
        HaplotypeNode(
            sequence, taxa[index % 3], index,
            "0", -1, -1, ".", -1, random.nextInt(numOfGVCFFiles)
        )
    }

    return buildEdgesAndGraph(nodes, ranges)

}

fun buildSimpleConsensusGraph(seqLength: Int = 100, numTaxa: Int = 10, numRanges: Int = 2): HaplotypeGraph {

    val seqs = (0 until numTaxa).map { "${"A".repeat(it)}${"T".repeat(seqLength - it)}" }

    val taxa = (0 until numTaxa).map { TaxaListBuilder().add("sample_${it}").build() }

    val ranges = (0 until numRanges).map {
        ReferenceRange("refName", Chromosome.instance("1"), it * seqLength, (it + 1) * seqLength, it, "genic")
    }

    val hapSequences = mutableListOf<HaplotypeSequence>()

    for (i in ranges.indices) {
        hapSequences.addAll(seqs.mapIndexed { index, s -> HaplotypeSequence.getInstance(s, ranges[i], 1.0, "") })
    }

    //turn hapSequences into a node
    val nodes = hapSequences.mapIndexed { index, sequence -> HaplotypeNode(sequence, taxa[index % numTaxa], index) }

    return buildEdgesAndGraph(nodes, ranges)

}

private fun buildEdgesAndGraph(
    nodes: List<HaplotypeNode>,
    ranges: List<ReferenceRange>
): HaplotypeGraph {

    val nodesGroupedByRange = nodes.groupBy { it.referenceRange() }.toSortedMap()

    // create Edges between nodes of consecutive ranges
    val edges = ranges.map { nodesGroupedByRange[it] }.zipWithNext().map {
        val nodes1 = it.first
        val nodes2 = it.second
        nodes1!!.map { node1 ->
            nodes2!!.map { node2 ->
                // compute the edge weights.  If the taxon is shared between the node assign weight .99.
                // If it's not we take .01 and divide by the number of nodes in the next range
                val prob = if (node1.taxaList() == node2.taxaList()) .99 else (0.01) / (nodes2.size - 1)
                HaplotypeEdge(node1, node2, prob)
            }
        }
    }.flatten().flatten()

    return HaplotypeGraph(edges)

}