package net.maizegenetics.pangenome.api;

import com.google.common.collect.Range;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.taxa.TaxaList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.SortedSet;

/**
 * @author Terry Casstevens Created August 29, 2017
 */
public class FilterGraphPlugin extends AbstractPlugin {

    private static final Logger myLogger = LogManager.getLogger(FilterGraphPlugin.class);

    private PluginParameter<TaxaList> myTaxaList = new PluginParameter.Builder<>("taxaList", null, TaxaList.class)
            .description("Optional list of taxa to include. This can be a comma separated list of taxa (no spaces unless surrounded by quotes), file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). By default, all taxa will be included.")
            .build();

    private PluginParameter<Integer> myMinCountTaxa = new PluginParameter.Builder<>("minCountTaxa", 0, Integer.class)
            .description("Minimun number of taxa represented in reference range for reference range to remain in graph")
            .range(Range.atLeast(0))
            .build();

    private PluginParameter<Double> myMinPercentTaxa = new PluginParameter.Builder<>("minPercentTaxa", null, Double.class)
            .description("Minimum percent of taxa represented in reference range for reference range to remain in graph.")
            .range(Range.closed(0.0, 1.0))
            .build();

    private PluginParameter<List<ReferenceRange>> myRefRanges = new PluginParameter.Builder("referenceRanges", null, List.class)
            .description("Reference range list to remove from graph")
            .build();

    private PluginParameter<SortedSet<Integer>> myHapids = new PluginParameter.Builder("haplotypeIds", null, SortedSet.class)
            .description("List of haplotype ids to include in the graph. If not specified, all ids are included.")
            .required(false)
            .build();

    public FilterGraphPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {

        List<Datum> graphs = input.getDataOfType(HaplotypeGraph.class);
        if (graphs.size() != 1) {
            throw new IllegalArgumentException("FilterGraphPlugin: processData: input one HaplotypeGraph.");
        }
        HaplotypeGraph graph = (HaplotypeGraph) graphs.get(0).getData();

        // Filter by minimum count taxa if specified
        if (minCountTaxa() != null && minCountTaxa() > 0) {
            myLogger.debug("Filter graph on minCountTaxa.");
            graph = CreateGraphUtils.removeRefRanges(graph, minCountTaxa());
        }

        // Filter by minimum percent taxa if specified
        if (minPercentTaxa() != null && minPercentTaxa() > 0.0) {
            myLogger.debug("Filter graph on minPercentTaxa.");
            graph = CreateGraphUtils.removeRefRanges(graph, minPercentTaxa());
        }

        if (taxaList() != null) {
            myLogger.debug("Filter graph on taxaList.");
            graph = CreateGraphUtils.subsetGraph(graph, taxaList());
        }

        if (refRanges() != null && !refRanges().isEmpty()) {
            myLogger.debug("Filter graph on refRanges.");
            graph = CreateGraphUtils.removeRefRanges(graph, refRanges());
        }

        // Filter by haplotype ids if specified
        if (hapids() != null && !hapids().isEmpty()) {
            myLogger.debug("Filter graph to keep only specified hapids");
            graph = CreateGraphUtils.keepHapIDs(graph, hapids());
        }

        return new DataSet(new Datum("Filtered Graph", graph, null), this);

    }

    public HaplotypeGraph filter(HaplotypeGraph graph) {
        return (HaplotypeGraph) processData(DataSet.getDataSet(graph)).getData(0).getData();
    }

    /**
     * Optional list of taxa to include.
     *
     * @return Taxa List
     */
    public TaxaList taxaList() {
        return myTaxaList.value();
    }

    /**
     * Set Taxa List.
     *
     * @param value Taxa List
     *
     * @return this plugin
     */
    public FilterGraphPlugin taxaList(TaxaList value) {
        myTaxaList = new PluginParameter<>(myTaxaList, value);
        return this;
    }

    /**
     * Set Taxa List. Optional list of taxa to include. This
     * can be a comma separated list of taxa (no spaces unless
     * surrounded by quotes), file (.txt) with list of taxa
     * names to include, or a taxa list file (.json or .json.gz).
     * By default, all taxa will be included.
     *
     * @param value Taxa List
     *
     * @return this plugin
     */
    public FilterGraphPlugin taxaList(String value) {
        myTaxaList = new PluginParameter<>(myTaxaList, convert(value, TaxaList.class));
        return this;
    }

    /**
     * Minimun number of taxa represented in reference range for reference range to remain in graph
     *
     * @return Min Count Taxa
     */
    public Integer minCountTaxa() {
        return myMinCountTaxa.value();
    }

    /**
     * Set Min Count Taxa. Minimun number of taxa represented in reference range for reference range to remain in graph
     *
     * @param value Min Count Taxa
     *
     * @return this plugin
     */
    public FilterGraphPlugin minCountTaxa(Integer value) {
        myMinCountTaxa = new PluginParameter<>(myMinCountTaxa, value);
        return this;
    }

    /**
     * Minimum percent of taxa represented in reference range for reference range to remain in graph.
     *
     * @return Min Percent Taxa
     */
    public Double minPercentTaxa() {
        return myMinPercentTaxa.value();
    }

    /**
     * Set Min Percent Taxa. Minimum percent of taxa represented in reference range for reference range to remain in
     * graph.
     *
     * @param value Min Percent Taxa
     *
     * @return this plugin
     */
    public FilterGraphPlugin minPercentTaxa(Double value) {
        myMinPercentTaxa = new PluginParameter<>(myMinPercentTaxa, value);
        return this;
    }

    /**
     * Reference range list to remove from graph
     *
     * @return Reference Ranges
     */
    public List<ReferenceRange> refRanges() {
        return myRefRanges.value();
    }

    /**
     * Set Reference Ranges. Reference range list to remove from graph
     *
     * @param value Reference Ranges
     *
     * @return this plugin
     */
    public FilterGraphPlugin refRanges(List<ReferenceRange> value) {
        myRefRanges = new PluginParameter<>(myRefRanges, value);
        return this;
    }

    /**
     * List of haplotype ids to include in the graph. If not specified, all ids are included.
     *
     * @return Haplotype Ids
     */
    public SortedSet<Integer> hapids() {
        return myHapids.value();
    }

    /**
     * Set Haplotype Ids. List of haplotype ids to include in the graph. If not specified, all ids are included.
     *
     * @param value Haplotype Ids
     *
     * @return this plugin
     */
    public FilterGraphPlugin hapids(SortedSet<Integer> value) {
        myHapids = new PluginParameter<>(myHapids, value);
        return this;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Filter Graph";
    }

    @Override
    public String getToolTipText() {
        return "Filter Graph";
    }
}
