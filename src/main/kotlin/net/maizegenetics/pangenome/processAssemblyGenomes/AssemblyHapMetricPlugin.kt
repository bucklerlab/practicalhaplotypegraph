package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.HashMultimap
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.*
import net.maizegenetics.util.Tuple
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import org.jetbrains.kotlinx.dataframe.*
import org.jetbrains.kotlinx.dataframe.api.dataFrameOf
import org.jetbrains.kotlinx.dataframe.api.toDataFrame
import org.jetbrains.kotlinx.dataframe.io.readCSV
import org.junit.experimental.ParallelComputer
import java.awt.Frame
import java.sql.Connection
import java.sql.SQLException
import java.util.*
import javax.swing.ImageIcon


/**
 * This class creates a csv file of each reference range with the chr/start/end details, and
 * a column for each taxon.  In each taxon's column is the length of the haplotype asociated with
 * that reference range.  A 0 length means the haplotypes was not present for that taxon at that
 * reference range.
 *
 * The taxons included are based on gamete_grp_id from the haplotypes table.  It includes all
 * gamete_grp_ids for which there is only 1 taxon/line_name associated.
 *
 * On a large database, this will run slow unless given enough memory to process results.
 * It is suggested it be run on CBSU with the java -Xmx flag set to a value appropriate for
 * your database size.  With a db size of 63G, and 4.5 million haplotype tables entries
 *
 * 2/17/23 - update to take a graph which removes the need to access a database to
 *   get the taxa, ref ranges, etc.  All can be pulled from the graph.
 *
 * We return a dataframe that is created by reading the csv file that
 *  was exported.  This dataframe can then be manipulated in a jupyter notebook by running
 *  functions against the Kotlin dataframe
 *
 */
class AssemblyHapMetricPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = LogManager.getLogger(AssemblyHapMetricPlugin::class.java)
    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("File with information for database access, required if a graph is not passed to the plugin")
            .required(false)
            .inFile()
            .build()

    private var methods = PluginParameter.Builder("methods", null, String::class.java)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" +
    "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
            .required(false)
            .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("File for writing matrix of haplotype coverage")
            .required(true)
            .outFile()
            .build()

    override fun processData(input: DataSet?): DataSet? {

        // If a graph was sent in, use that.  We won't then need a db connection
        var graph:HaplotypeGraph? = null
        if (input != null) {
            val graphData = input.getDataOfType(HaplotypeGraph::class.java)
            if (graphData.size >  0) {
                myLogger.info("Graph supplied from input DataSet")
                graph = graphData[0].data as HaplotypeGraph
            }
        }
        if (graph == null && ( configFile() == null || methods() == null)) {
            throw IllegalArgumentException("Must supply either a config file with db connection information AND a list of methods, or a haplotype graph as part of the plugin's Dataset Input")
        }

        if (graph == null) {
            myLogger.info("Getting graph from config file db  ...")

            graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile())
                .includeVariantContexts(false)
                .includeSequences(false)
                .methods(methods())
                .build()
        }


        // This needed as we need to grab reference ranges based on refRangeID
        val referenceRanges = mutableMapOf<Int,ReferenceRange>()
        val refRangeToGameteTuple = HashMultimap.create<Int, Tuple<String, Int>>()
        val taxaNameSet = mutableSetOf<String>()

        // Make list of reference ranges for each assembly - do this by traversing the nodes
        // in the graph

        // Works - taxaName must be Set, not List - no duplicates!
        // This is for non-consensus haplotypes, so only pick 1 taxon per gamete_grp_id
        graph!!.nodeStream().forEach{ node ->
            val refRangeId = node.referenceRange().id()
            val taxon = node.taxaList()[0].name
            val seqLen = node.haplotypeSequence().length()
            refRangeToGameteTuple.put(refRangeId,Tuple(taxon,seqLen))
            taxaNameSet.add(taxon)
            referenceRanges.put(refRangeId,node.referenceRange())
        }

        myLogger.info(" size of referenceRanges keyset: ${referenceRanges.keys.size}")
        myLogger.info(" size of taxaNameSet: ${taxaNameSet}")
        var size = refRangeToGameteTuple.keys().size
        myLogger.info("Calling writeOutFile with refRangeToGameteTUple map keyset size: $size")

        // Due to the difficulty of creating a dataframe before the number and names of taxa are known
        // and the inability to append rows (only columns) to a Kotlin Dataframe, we write the output
        // first, then read that as a CSV file to create the dataframe that will be returned.
        val df = writeMetricsFile(taxaNameSet, referenceRanges, refRangeToGameteTuple, outputFile())

        return DataSet(Datum("DataFrame", df, "kotlin DataFrame from csv file ${outputFile()}" ), this)
    }

    // This function takes taxon names, a map of refRanges to <taxonName,seqLen>
    // and an output file.  There is code left in here tha attempts to create a dataframe
    // at the same time as creating lines for the output file.  That dataframe code is
    // not working properly, but I have kept it in for future debugging use.  The dataframe
    // that is created as the return value is created via a readCSV command, which works
    // well to create Kotlin dataframes.
    fun writeMetricsFile(taxonNameList:Set<String>, refRangeMap:MutableMap<Int, ReferenceRange>,
                         rangesForGamete: HashMultimap<Int, Tuple<String, Int>>, outputFile: String):DataFrame<*> {

        // Create the header line, add the taxa on sorted order
        val headerLineSB = StringBuilder()
        headerLineSB.append("refRangeID,group_type,chr,start,end,refRangeLen")

        // Create dataframe column names
        val dfNames = mutableListOf<String>()
        dfNames.add("refRangeID")
        dfNames.add("group_type")
        dfNames.add("chr")
        dfNames.add("start")
        dfNames.add("end")
        dfNames.add("refRangeLength")

        // Create dataframe column data list
        val dfData = mutableListOf<List<Any>>()

        val sortedTaxa = taxonNameList.sorted()
        for (name in sortedTaxa) {
            headerLineSB.append(",").append(name)
            dfNames.add(name)
        }
        headerLineSB.append("\n")

        // Create empty kotlin dataframe with column names from dfNames
        val dfFinal = dataFrameOf(dfNames)

        var bw = Utils.getBufferedWriter(outputFile)
        val rangeList = ArrayList(rangesForGamete.keySet())
        rangeList.sort()
        myLogger.info("writeOutFile: Size of rangeList ${rangeList.size}")

        try {
            bw.write(headerLineSB.toString())
            for ( rangeId in rangeList) {
                val dfDataList = mutableListOf<String>()
                // this gets as the taxa data for this ref range
                var rrGameteData = rangesForGamete.get(rangeId)
                var line = StringBuilder()
                var rrData = refRangeMap.get(rangeId)

                // We shouldn't get null, as the graph only pulled data for
                // the methods and range types the user requested.
                if (rrData == null) {
                    myLogger.warn("NO data for ref range ID ${rangeId} - skipping!!")
                    continue //  skip this line, we don't want this range
                }
                val rangeLen = rrData.end() - rrData.start() + 1

                // this is for the column that shows genic/intergenic/etc range methods for each range
                line.append(rangeId).append(",").append(rrData.groupMethods()).append(",")
                dfDataList.add(rrData.groupMethods().joinToString())


                // THen append chrom, start,end for this row
                line.append(rrData.chromosome().name).append(",")
                line.append(rrData.start()).append(",").append(rrData.end()).append(",").append(rangeLen)
                dfDataList.add(rrData.start().toString())
                dfDataList.add(rrData.end().toString())

                dfDataList.add(rangeLen.toString())

                // need the rest of the data for each line
                // create list of gameteid to seqlen, 0 if not present
                //var gameteIdToLen= ArrayList<Int>(28);

                // These need to be sorted lexicographically to match
                // the column headers.
                //var gameteIdToLenMap = mutableMapOf<String, Int>()

                // here are getting the length for each gamete in the list for this reference range
                // translate the set to a map with the taxa as the key, length as the value
                val gameteIdToLenMap = rrGameteData.associate{ it.getX() to it.getY() }

                // add remaining gametes in the sortedTaxa list to StringBuilder
                for (name in sortedTaxa) {
                    var gameteLen = gameteIdToLenMap.get(name)
                    if (gameteLen != null) {
                        line.append(",").append(gameteLen)
                        dfDataList.add(gameteLen.toString())
                    } else {
                        line.append(",").append(0)
                        dfDataList.add("0")
                    }
                }
                line.append("\n")
                var finalString = line.toString()
                bw.write(finalString)

                // dfDataList is all the data for that row.  So
                // dfData is a list of row data, not a list of list,
                // but rather a list<String>, comma-separated string
                dfData.add(dfDataList.toList())

            }
            bw.close()

            // create a Kotlinx dataframe from the dfNames and dfData
            //First, change dfData from a list columns list to a list of rows list
            // THis fails as well
            //val dfDataCols = dfData.toDataFrame()
            // println("\nNumber of df columns = ${dfDataCols.columnNames().size}")
            //println("Number of df rows = ${dfDataCols.rowsCount()}")

            val dfFromCSV = DataFrame.readCSV(outputFile())
            myLogger.info("\nNumber of dfFromCSV columns = ${dfFromCSV.columnNames().size}")
            myLogger.info("Number of dfFromCSV rows = ${dfFromCSV.rowsCount()}")
            return dfFromCSV

        } catch (exc: Exception) {
            throw IllegalStateException("Error writing output file", exc)
        }


    }

    override fun getToolTipText(): String {
        return ("Pulls data on haplotype size per reference range for each non-consensus taxon in the data base")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyHapMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return ("AssemblyHapMetricPlugin")
    }

    /**
     * FIle with infor for database access
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. FIle with infor for database access
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): AssemblyHapMetricPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Commas separated list of mathod names
     *
     * @return Methods
     */
    fun methods(): String {
        return methods.value()
    }

    /**
     * Set Methods. Commas separated list of mathod names
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): AssemblyHapMetricPlugin {
        methods = PluginParameter<String>(methods, value)
        return this
    }

    /**
     * File for writing matrix of gene coverage
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. File for writing matrix of gene coverage
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): AssemblyHapMetricPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyHapMetricPlugin::class.java)
}