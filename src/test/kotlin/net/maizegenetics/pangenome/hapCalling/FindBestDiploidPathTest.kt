package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Test
import org.junit.Assert.*
import java.io.*
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

val minimap2location = "/Users/peterbradbury/git/minimap2/minimap2"

class FindBestDiploidPathTest {

    @Test
    fun testPairToLong() {
        val pairLong: Long = intArrayToLong(intArrayOf(5,10))
        val backToInts = longToIntArray(pairLong)
        assertEquals(5, backToInts[0])
        assertEquals(5, backToInts[0])
        assertEquals(10, backToInts[1])
    }

    @Test
    fun testCountsFromHapidList() {
        val idList = listOf(HapIdSetCount(setOf(1,2,3,4), 3), HapIdSetCount(setOf(1,3,4), 4), HapIdSetCount(setOf(2,4), 2))

        val countMap : Map<Long,Int> = countsFromHapidList(idList)
        assertEquals("count map size wrong",11, countMap.size)
        assertEquals(3, countMap.getOrDefault(intArrayToLong(intArrayOf(1,2)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,3)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,4)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,1)), 0))
        assertEquals(5, countMap.getOrDefault(intArrayToLong(intArrayOf(2,4)), 0))
        assertEquals(5, countMap.getOrDefault(intArrayToLong(intArrayOf(2,2)), 0))

        //test total count
        assertEquals(9, countMap.get(-1))
    }

    @Test
    fun testNodePairs() {
        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }
        val pairs = nodePairs(nodeList)
        for (i in 0..8) {
            val a: Int = i / 3 + 1
            val b: Int = i % 3 + 1
            assertEquals("testing first value of pair $i", a, pairs[i].first.id())
            assertEquals("testing second value of pair $i", b, pairs[i].second.id())
        }
    }

    @Test
    fun testStartProbabilities() {
        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val probs = startProbabilities(nodeList)

        probs.forEach { print(java.lang.String.format("%1.3e ", it)) }
        println()

        //# of successes
        val expectedProb = 1.0 / 9.0;
        for (i in 0..8) {
            assertEquals("Testing probability $i", expectedProb, probs[i], 0.00001)
        }
    }

    @Test
    fun testEmissionProbability() {
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        for (i in 4..6) {
            nodeList2.add(HaplotypeNode(null, taxonList, i))
        }
        rangeToNodeMap.put(refRange2, nodeList2)

        val reads : Multimap<ReferenceRange, HapIdSetCount> = HashMultimap.create()
        reads.put(refRange, HapIdSetCount(setOf(1,2), 3))
        reads.put(refRange, HapIdSetCount(setOf(1), 3))
        reads.put(refRange, HapIdSetCount(setOf(1,2,3), 1))

        val dipEmissionProb = DiploidEmissionProbability(rangeToNodeMap,reads,0.9)

        (0..8).map { dipEmissionProb.getProbObsGivenState(it,0) }.forEach { println(it) }

        //expected values were calculated using R function dmultinom()
        //state 0 (1,1): dbinom(7,7,0.9)
        assertEquals("testing prob11", 0.4782969, dipEmissionProb.getProbObsGivenState(0,0), 1e-6)
        //state 1 (1,2): sum over n = 0 to 4, dmultinom(x = c(3 + n, 4 - n, 0),prob = c(0.45,0.45,0.1))
        assertEquals("testing prob12", 0.3699328, dipEmissionProb.getProbObsGivenState(1,0), 1e-6)
        //state 2 (1,3): sum over n = 0 to 1, dmultinom(x = c(6 + n, 1 - n, 0),prob = c(0.45,0.45,0.1))
        val prob = dipEmissionProb.getProbObsGivenState(2,0)
        assertEquals("testing prob13", 0.02989356, prob, 1e-6)

        //state 4 (2,2): dbinom(4,7,0.9)
        assertEquals("testing prob22", 0.0229635, dipEmissionProb.getProbObsGivenState(4,0), 1e-6)

        //state 3 (2,1): same as (1,2)
        assertEquals("testing prob21", 0.3699328, dipEmissionProb.getProbObsGivenState(3,0), 1e-6)

        //test range with no counts
        assertEquals("testing empty range",1.0/9.0, dipEmissionProb.getProbObsGivenState(0,1), 0.00001)
    }

    @Test
    fun compareEmissionProbabilities() {
        //TODO, update this test with easier to compute probs
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        for (i in 4..6) {
            nodeList2.add(HaplotypeNode(null, taxonList, i))
        }
        rangeToNodeMap.put(refRange2, nodeList2)

        val reads : Multimap<ReferenceRange, HapIdSetCount> = HashMultimap.create()
        reads.put(refRange, HapIdSetCount(setOf(1,2), 14))
        reads.put(refRange, HapIdSetCount(setOf(1), 62))
        reads.put(refRange, HapIdSetCount(setOf(2), 11))
//        reads.put(refRange, HapIdSetCount(setOf(1,2,3), 5))
        reads.put(refRange, HapIdSetCount(setOf(3), 3))

        val dipEmissionProb = DiploidEmissionProbability(rangeToNodeMap,reads,0.95)
        (0..8).map { dipEmissionProb.getProbObsGivenState(it,0) }.forEach { println(it) }
        assertEquals("testing prob11", 1e-4,dipEmissionProb.getProbObsGivenState(0,0), 1e-1)
        assertEquals("testing prob12", 7.7e-6,dipEmissionProb.getProbObsGivenState(1,0), 1e-1)
        assertEquals("testing prob13", 4.6e-22,dipEmissionProb.getProbObsGivenState(2,0), 1e-1)
        assertEquals("testing prob21", 7.7e-6,dipEmissionProb.getProbObsGivenState(1,0), 1e-1)
        assertEquals("testing prob22", 8.7e-64,dipEmissionProb.getProbObsGivenState(4,0), 1e-1)
        assertEquals("testing prob23", 9.7e-64,dipEmissionProb.getProbObsGivenState(5,0), 1e-1)
        assertEquals("testing prob31", 4.6e-22,dipEmissionProb.getProbObsGivenState(2,0), 1e-1)
        assertEquals("testing prob32", 9.7e-64,dipEmissionProb.getProbObsGivenState(5,0), 1e-1)
        assertEquals("testing prob33", 6.5e-109,dipEmissionProb.getProbObsGivenState(8,0), 1e-1)
    }

    @Test
    fun testTransitionProbability() {
        //build a graph with two refRanges and 3 nodes each
        //create some edges
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        for (i in 1..3) {
            val myTaxa = TaxaListBuilder().add("t${i * 3 - 2}").add("t${i * 3 - 1}").add("t${i * 3}").build()
            nodeList.add(HaplotypeNode(HaplotypeSequence.getInstance("", refRange, 0.0, ""), myTaxa, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val hapSequence = HaplotypeSequence.getInstance("", refRange2, 0.0, "")
        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t1").add("t2").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t3").add("t4").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t5").add("t6").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t7").add("t8").add("t9").build(),
                4))

        rangeToNodeMap.put(refRange2, nodeList2)

        val edges = CreateGraphUtils.createEdges(rangeToNodeMap)
        val myGraph = HaplotypeGraph(edges)

        val transProb = DiploidTransitionProbability(myGraph, ArrayList(rangeToNodeMap.values), 0.01)

        transProb.setNode(1)
        assertEquals(16, transProb.numberOfStates)
        assertEquals("", 4.0/9.0, transProb.getTransitionProbability(0,0), 1e-6)
        //state 0 = (0,0); state 1 = (0,1); 0->0 p=2/3;0->1 p=1/3
        assertEquals("", 2.0/9.0, transProb.getTransitionProbability(0,1), 1e-6)
        //state 0 = (0,0); state 2 = (0,2); 0->0 p=2/3;0->2 p=0.01
        assertEquals("", 0.01 * 2.0 / 3.0, transProb.getTransitionProbability(0,2), 1e-6)
        //state 1 = (0,1); state 1 = (0,1);0->0 p=2/3; 0->1 p=1/3
        assertEquals("", 2.0/9.0, transProb.getTransitionProbability(1,1), 1e-6)
        //state 1 = (0,1); state 2 = (0,2); 0->0 p=2/3; 1->2 p=2/3
        assertEquals("", 4.0/9.0, transProb.getTransitionProbability(1,2), 1e-6)

    }

    @Test
    fun testFactorialApproximation() {
        var logFactorial: Double = 0.0;
        for (n in 1..15) {
            val N = n.toDouble()
            val stirling1 = logFactorial(n)
            val stirling2 = n * Math.log(N) - N
            logFactorial += Math.log(N)

            assertEquals("Logfactorial $n does not match stirling1", logFactorial, stirling1, 0.01)

            //Note not checking striling2 because it is not as accurate as stirling1  delta is around 2 at n=15
            println("$n log factorial: $logFactorial, $stirling1, $stirling2")
        }
    }
}
