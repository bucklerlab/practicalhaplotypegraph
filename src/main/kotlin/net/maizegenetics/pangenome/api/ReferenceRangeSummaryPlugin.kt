package net.maizegenetics.pangenome.api

import net.maizegenetics.dna.map.Position
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.util.*
import javax.swing.ImageIcon

/**
 * @author Terry Casstevens
 * Created January 29, 2019
 */
class ReferenceRangeSummaryPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(ReferenceRangeSummaryPlugin::class.java)

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .required(true)
            .description("Output summary filename")
            .build()

    override fun processData(input: DataSet?): DataSet? {

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
        if (temp?.size != 1) {
            throw IllegalArgumentException("ReferenceRangeSummaryPlugin: processData: must input a graph")
        }
        val graph = input.getData(0).data as HaplotypeGraph

        File(outputFile()).bufferedWriter().use { out ->

            out.write("Haplotype_ID\tRefRange_ID\tRange\tUnique_SNPs\tTaxa\tNum_Taxa\tSNPs\tMissing\tSeq_Length\n")

            graph.referenceRangeStream().forEach { range ->

                val result = TreeSet<Position>()

                val nodes = graph.nodes(range)
                val numNodes = nodes.size

                val snpsPerNode = IntArray(numNodes)

                nodes.forEachIndexed { index, node ->

                    node.variantInfos().get()?.let {
                        it.filter { it.isVariant && !it.isIndel }.forEach {
                            result.add(Position.of(it.chromosome(), it.start()))
                            snpsPerNode[index]++
                        }
                    }

                }

                nodes.forEachIndexed { index, node ->
                    out.write("${node.id()}")
                    out.write("\t")
                    out.write(range.id().toString())
                    out.write("\t")
                    out.write(range.intervalString())
                    out.write("\t")
                    out.write(result.size.toString())
                    out.write("\t")
                    out.write(node.taxaList().joinToString())
                    out.write("\t")
                    out.write(node.taxaList().size.toString())
                    out.write("\t")
                    out.write(snpsPerNode[index].toString())
                    out.write("\t")
                    out.write(node.haplotypeSequence().sequence().count { it == 'N' }.toString())
                    out.write("\t")
                    out.write(node.haplotypeSequence().length().toString())
                    out.write("\n")
                }

            }

        }

        return null

    }

    /**
     * Output summary filename
     *
     * @return Output File
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output File. Output summary filename
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): ReferenceRangeSummaryPlugin {
        myOutputFile = PluginParameter(myOutputFile, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Reference Range Summary"
    }

    override fun getToolTipText(): String {
        return "Reference Range Summary"
    }

}
