/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Utils;

import static org.junit.Assert.*;

/**
 * Tests the plugin CreateContigFastaFromAssemblyGenomePlugin
 * from a short test fasta. 
 * 
 * A short fasta with 2 chromosomes and sequences that contain Ns is created.
 * This file is sent as a parameter to the CreateContigFastaFromAssemblyGenomePlugin.
 * The junit verifies CreateContigFastaFromAssemblyGenomePlugin correctly split the
 * fasta at the N's.
 *
 * @author lcj34
 *
 */
public class CreateContigFastaFromAssemblyGenomePluginTest {
    
    public static final String userHome = System.getProperty("user.home");
    public static final String outputDir = userHome + "/temp/CreateContigFastaFromAssemblyGenomePlugin/";
    public static final String dataDir = outputDir+"data/";
    
    public static final String junitDataDir = userHome + "/temp/CreateContigFastaFromAssemblyGenomePlugin/";
    @Test
    public  void testCreateContigs() {
        //fail("Fix this broken test.");
        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(new File(outputDir));
            Files.createDirectories(Paths.get(outputDir));
            Files.createDirectories(Paths.get(dataDir));

        } catch (IOException ioe) {
            throw new IllegalStateException("testCreateContigs: error deleting/creating folders: " + ioe.getMessage());
        }
 
        String genomeFile = dataDir + "testGenomeForContigFasta.fa";
        String assembly = "Test";

        createTestFile(genomeFile);
        
        new CreateContigFastaFromAssemblyGenomePlugin()
        .assembly(assembly)
        .genomeFile(genomeFile)
        .outputDir(outputDir)
        .performFunction(null);
        
        // A small assembly was created with sequence containing N's.
        // CreateContigFatasFromAssemblyGenomePlugin splits it where N's occur.
 
        String outputContigs = outputDir + assembly + "_asContigs.fa";
        Map<String,String> nameToSequence = new HashMap<String,String>();
        
        try (BufferedReader br = Utils.getBufferedReader(outputContigs)) {
            String line;
            StringBuilder sb = new StringBuilder();
            String curContig = "none";
            while ((line = br.readLine()) != null) {                
                if (line.startsWith(">")) {  
                    if (sb.length() > 0) {
                        nameToSequence.put(curContig, sb.toString());
                        sb.setLength(0);
                    }
                    curContig = line;
                } else {
                    sb.append(line);
                }
            }
            if (sb.length() > 0) {
                nameToSequence.put(curContig, sb.toString());
            }
        } catch (Exception exc) {
            exc.printStackTrace();
            throw new IllegalStateException("Error reading output contigs file " + exc.getMessage());
        }
        // Verify the initial header lines for each chrom, and the contig sequences that should
        // have been created.  Verify both chromosomes are processed as well as sequence past last N
        assertEquals(nameToSequence.keySet().size(),8); // splitting by N's should leave us with 8 contigs
        assertTrue(nameToSequence.containsKey(">chr13:1:86")); // 86 alleles before first N
        assertTrue(nameToSequence.containsValue("CCCAAGTCAACCGGATATTCCAAAGCAAGGGTTATGAATCAAAAGAGAAGTGCCAGCTGGATCTCTGGTTCAAAGTTCAGGGCATG"));
        assertTrue(nameToSequence.containsValue("GAAAAGGAAATCCCATTCGTCAACCTAAATGGAAGGGCAGGTAAGGGAAGGCCAGCAGGAAATTCAGTAAGT"));
        assertTrue(nameToSequence.containsValue("AAAAGGTATGTATGAACCAACAAAGGTCAAGTAGACGAACACATATCCACTCCACTAGAGAAAGCGTCGAACCAGGG"));
        assertTrue(nameToSequence.containsValue("CTTTCGAGCTTCTTATTCGATTTCTTAAATGACTTCTTCGG"));
        
        assertTrue(nameToSequence.containsKey(">chr14:1:45")); // 45 alleles before first N
        assertTrue(nameToSequence.containsValue("CGGCGACTGGATGACAGAATGGTTTTATGTAAAAAATGACCTGAA"));
        assertTrue(nameToSequence.containsValue("ATCTGTTCTTTTATTGGAACGAGAGATTTGGTACAAGAGCATATTGCCTTCAGGGTATGGCCACT"));
        assertTrue(nameToSequence.containsValue("TTGAG"));
        assertTrue(nameToSequence.containsValue("CTATCGTTATCCCATTCGAGGGCAGAAAAGAAAAAACACAAC"));
                       
        System.out.println("testCreateContigs Successfully Done !!");
    }

    public void createTestFile(String genomeFile) {

        // Write a small test file fasta that will be split by N's
        try (BufferedWriter bw = Utils.getBufferedWriter(genomeFile)) {
            bw.write(">chr13\n");
            bw.write("CCCAAGTCAACCGGATATTCCAAAGCAAGGGTTATGAATCAAAAGAGAAGTGCCAGCTGGATCTCTGGTTCAAAGTTCAGGGCATGNNN");
            bw.write("GAAAAGGAAATCCCATTCGTCAACCTAAATGGAAGGGCAGGTAAGGGAAGGCCAGCAGGAAATTCAGTAAGTN");
            bw.write("AAAAGGTATGTATGAACCAACAAAGGTCAAGTAGACGAACACATATCCACTCCACTAGAGAAAGCGTCGAACCAGGGNN");
            bw.write("CTTTCGAGCTTCTTATTCGATTTCTTAAATGACTTCTTCGG\n");
            // write chrom 14 data
            bw.write(">chr14\n");
            bw.write("CGGCGACTGGATGACAGAATGGTTTTATGTAAAAAATGACCTGAAN");
            bw.write("ATCTGTTCTTTTATTGGAACGAGAGATTTGGTACAAGAGCATATTGCCTTCAGGGTATGGCCACTNNN");
            bw.write("TTGAGN");
            bw.write("CTATCGTTATCCCATTCGAGGGCAGAAAAGAAAAAACACAAC\n");

        } catch (Exception exc) {
            System.out.println("createTestFile: error writing test genome file with N's");
            throw new IllegalStateException(" createTestFile threw this error: " + exc.getMessage());
        }
    }
}
