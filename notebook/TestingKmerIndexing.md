##Testing IndexHaplotypeKmersPlugin
###Notebook denoting steps taken to create and tune IndexHaplotypeKmersPlugin


Main Test Class is package net.maizegenetics.pangenome.hapCalling.HapCountBestPathPluginTest.java

Analysis June 22, 2018
Large numbers of errors introduced by creating the consensus haplotype and then extracting haplotypes. Every N created
in the consensus, was dropped from the kmer.

~6% error when SNP every 256 bp within a haplotype group.

3 to 0% error when SNP every 10,000bp within a haplotype group.

Random error rate would be 65%.  So a lot better than random but 3% is still problematic.
I was suprised the error didn't drop to near zero with 1 in 10,000bp intrahaplotype mutations