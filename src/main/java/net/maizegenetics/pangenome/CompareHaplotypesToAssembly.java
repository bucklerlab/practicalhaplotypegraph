package net.maizegenetics.pangenome;

import net.maizegenetics.util.Utils;

import java.io.BufferedReader;

/**
 * Created by terry on 3/21/17.
 */
public class CompareHaplotypesToAssembly {

    public static void main(String[] args) {

        String haplotypes = args[0];
        String assembly = args[1];
        String map = args[2];

        try (BufferedReader readHaplotypes = Utils.getBufferedReader(haplotypes);
             BufferedReader readMap = Utils.getBufferedReader(map)) {

            String samLine = readMap.readLine();
            while (samLine.startsWith("@")) {
                samLine = readMap.readLine();
            }

            String hapLine = readHaplotypes.readLine();
            while (samLine != null) {

                if (!hapLine.startsWith(">")) {
                    throw new IllegalStateException("hapLine should start with >");
                }
                String header = hapLine;
                String[] hapHeader = hapLine.split(" ");
                String hapName = hapHeader[0].substring(1);

                StringBuilder hapSeq = new StringBuilder();
                hapLine = readHaplotypes.readLine();
                while (hapLine != null && !hapLine.startsWith(">")) {
                    hapSeq.append(hapLine);
                    hapLine = readHaplotypes.readLine();
                }

                int samIndex;
                boolean notDone = true;
                do {
                    samIndex = 0;
                    int numTabs = 0;
                    StringBuilder samNameBuilder = new StringBuilder();
                    StringBuilder chrBuilder = new StringBuilder();
                    while (numTabs < 9) {
                        if (samLine.charAt(samIndex) == '\t') {
                            numTabs++;
                        }
                        if (numTabs == 0) {
                            samNameBuilder.append(samLine.charAt(samIndex));
                        } else if (numTabs == 2) {
                            chrBuilder.append(samLine.charAt(samIndex));
                        }
                        samIndex++;
                    }
                    if (hapName.equals(samNameBuilder.toString())) {
                        notDone = false;
                        System.out.print(chrBuilder.toString().trim() + " ");
                    } else {
                        samLine = readMap.readLine();
                    }
                } while (notDone);

                StringBuilder samSeq = new StringBuilder();
                while (samLine.charAt(samIndex) != '\t') {
                    samSeq.append(samLine.charAt(samIndex));
                    samIndex++;
                }

                if (hapSeq.length() != samSeq.length()) {
                    System.out.print("hapSeq length: " + hapSeq.length() + "  samSeq length: " + samSeq.length() + " ");
                }

                int minLen = Math.min(hapSeq.length(), samSeq.length());
                int numSame = 0;
                for (int i = 0; i < minLen; i++) {
                    if (hapSeq.charAt(i) == samSeq.charAt(i)) {
                        numSame++;
                    }
                }

                System.out.println(header + ": percent same: " + ((double) numSame / (double) Math.max(hapSeq.length(), samSeq.length())) + " hapSeq len: " + hapSeq.length() + " samSeq len: " + samSeq.length());

                samLine = readMap.readLine();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
