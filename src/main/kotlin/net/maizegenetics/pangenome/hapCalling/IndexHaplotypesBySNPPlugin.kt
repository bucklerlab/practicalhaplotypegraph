package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.util.stream.Collectors
import javax.swing.ImageIcon


/**
 * Class to index the PHG by the SNP values contained within.
 * The output is a file in the form:
 *  refRangeId  chr	position	refAllele	altAllele	refHapIds	altHapIds
    1   1	24	A	T	102,104	103
    1   1	95	C	T	103,104	102
    1   1	101	G	T	103,104	102

 * This index is then used in the SNPToReadMappingPlugin step to compare a VCF file with the index and create readMappings.
 *
 */
class IndexHaplotypesBySNPPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(IndexHaplotypesBySNPPlugin::class.java)


    private var vcfFile = PluginParameter.Builder("vcfFile", null, String::class.java)
            .guiName("VCF File")
            .inFile()
            .required(true)
            .description("Name of the VCF file which holds the initial mappings to a single reference.")
            .build()


    private var outputIndexFile = PluginParameter.Builder("outputIndexFile", null, String::class.java)
            .guiName("Index File Output")
            .outFile()
            .required(true)
            .description("Index File Output.")
            .build()

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("Database configuration file")
            .guiName("Config File")
            .required(true)
            .inFile()
            .build()

    private var myMethods = PluginParameter.Builder("methods", null, String::class.java)
            .required(true)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" + "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
            .build()

    private var myLocalGVCFDir = PluginParameter.Builder("localGVCFDir", null, String::class.java)
            .description("Local directory to store GVCF files")
            .required(true)
            .inDir()
            .build()


    /**
     * Method to index the PHG for each SNP specified in the vcfFile
     *
     * This makes use of the graph stream so it is RAM efficient even though we are loading in the Variants
     *
     * Steps:
     * 1.  Get a RangeMap of position to reference range Ids (used for fast lookup for what haplotypes to pull out for each SNP in VCF
     * 2.  Associate each SNP with a reference range and add to a map
     * 3.  Get the graphStream with Variants
     * 4.  For each Reference range, go through each Haplotype's SNP list and check to see if they are one of the SNPs to score
     *      If so, add to either the reference or alternate list
     * 5.  Collect the results from the graphStream
     * 6.  Output the results to the index file.
     */
    override fun processData(input: DataSet?): DataSet? {
        var startTime = System.nanoTime()
        val snpMap = getSNPsFromVCF(vcfFile())

        //Create a Position -> RefRangeId range map for easy lookup of SNP position to reference range.
        myLogger.info("Done loading in SNPs from the vcfFile: Time ${(System.nanoTime() - startTime)/1E9} secs.")

        val graphStreamNoVar = HaplotypeGraphStreamBuilderPlugin(null,false)
                .configFile(configFile())
                .methods(methods())
                .includeSequences(false)
                .includeVariantContexts(false)
                .localGVCFFolder(localGVCFDir())
                .build()

        val positionPairs = graphStreamNoVar.parallel().map { refRangeToNodePair -> Pair(Range.closed(Position.of(refRangeToNodePair.first.chromosome(),refRangeToNodePair.first.start()),
                Position.of(refRangeToNodePair.first.chromosome(),refRangeToNodePair.first.end())),refRangeToNodePair.first.id())}.collect(Collectors.toList())

        myLogger.info("Done getting Position Pairs:")
        val positionToRefRangeMap = TreeRangeMap.create<Position, Int>()

        positionPairs.forEach {
            positionToRefRangeMap.put(it.first,it.second)
        }

        myLogger.info("Done setting up position To Ref Range Map:")
        //Associate SNPs with specific reference ranges to make things easier
        val refRangeToSNPMap = snpMap.keys.map { Pair(it,positionToRefRangeMap[it]) }
                .groupBy ({ it.second }, { it.first })

        val positionToHapIdsMap = PositionToRefAndAltHaplotypes()

        myLogger.info("Walking through the graph to associate SNPs with haplotypes")
        val graphStreamWithVar = HaplotypeGraphStreamBuilderPlugin(null,false)
                .configFile(configFile())
                .methods(methods())
                .includeSequences(false)
                .includeVariantContexts(true)
                .localGVCFFolder(localGVCFDir())
                .build()

        //Get the SNP to HapId Mappings for each reference range in the graph.
        val listOfSNPtoHapIdLists = graphStreamWithVar.parallel().map { refRangeToHapPair ->
            val snpsInRefRange = refRangeToSNPMap[refRangeToHapPair.first.id()]?:listOf<Position>()
            getSNPToHapIdMapping(refRangeToHapPair,snpsInRefRange,snpMap)
        }
        .collect(Collectors.toList())

        //Add them all to the final datastructure
        listOfSNPtoHapIdLists.forEach { positionToHapIdsMap.addAll(it) }
        val finalHapIdMap = positionToHapIdsMap.getMap()

        myLogger.info("Exporting index to File:")
        //Export to file
        exportMapToFile(outputIndexFile(), finalHapIdMap, snpMap,positionToRefRangeMap)
        myLogger.info("Done exporting file:")

        return null

    }

    /**
     * Method to retrieve the SNPs from the VCF file. We need to do this in order to map SNPs to haplotype nodes.
     */
    fun getSNPsFromVCF(vcfFile: String) : Map<Position,Pair<String,String>> {
        val snpMap = mutableMapOf<Position,Pair<String,String>>()
        Utils.getBufferedReader(vcfFile).use { reader ->
            var currentLine:String? = ""
            while(currentLine != null) {
                currentLine = reader.readLine()
                if(currentLine == null) {
                    break
                }
                //filter out the header
                if(!currentLine.startsWith("#")) {
                    val vcfRecordSplit = currentLine.split("\t",limit = 6) //Only need the first 5, but the rest of the line is put into the last element.  So We actually need a list of size 6
                    val currentPosition = Position.of(vcfRecordSplit[0],vcfRecordSplit[1].toInt())
                    val refAllele = vcfRecordSplit[3]
                    val firstAlt = if(vcfRecordSplit[4] != "") {
                        vcfRecordSplit[4].split(",", limit=2)[0] //Only need first allele, but need to set limit to 2 otherwise it will merge all the alts into a single string.

                    }
                    else {
                        "N"
                    }

                    if(refAllele.length == 1 && firstAlt.length == 1) {
                        snpMap[currentPosition] = Pair(refAllele, firstAlt)
                    }
                }
            }
        }

        return snpMap
    }

    /**
     * Method to get Mappings from SNP -> HapId Lists
     * We only support the first non-reference allele for simplicity
     */
    fun getSNPToHapIdMapping(refAndHapPair: Pair<ReferenceRange,Set<HaplotypeNode>>, snpsInRefRange: List<Position>, snpMap: Map<Position,Pair<String,String>>): PositionToRefAndAltHaplotypes {
        val positionToHapIdsMap = PositionToRefAndAltHaplotypes()

        refAndHapPair.second.filter { node -> node.variantInfos().isPresent }.forEach { node ->
            //Walk through snpsInRefRange and nodes variantInfos
            val variantInfosForNode = node.variantInfos().get()

            var infoCounter = 0
            var snpCounter = 0

            while(infoCounter < variantInfosForNode.size && snpCounter < snpsInRefRange.size) {
                val currentSNPPosition = snpsInRefRange[snpCounter]
                val currentSNP = snpMap[currentSNPPosition]
                val currentVariantInfo = variantInfosForNode[infoCounter]


                if(currentSNPPosition.position<= currentVariantInfo.end() && currentSNPPosition.position>= currentVariantInfo.start()) {
                    //this means the SNP Position falls within the Info so we can do a comparison
                    if(!currentVariantInfo.isVariant) {
                        //Means we have a REF Call for this SNP position.  Add this hapId to the reference side of the final map
                        positionToHapIdsMap.addToRef(currentSNPPosition,node.id())
                    }
                    else if(currentSNP?.second?:"" == currentVariantInfo.altAlleleString() ){
                        //Means we have an ALT call for this SNP position.  Add this hapId to the alt side of the final map
                        positionToHapIdsMap.addToAlt(currentSNPPosition,node.id())
                    }

                }

                //If SNP position is before variantInfoEnd
                if(currentSNPPosition.position<= currentVariantInfo.end()) {
                    //Slide up snpCounter
                    snpCounter++
                }
                else {
                    //If  End is before SNP position
                    //Slide up infoCounter
                    infoCounter++
                }
            }
        }

        return positionToHapIdsMap
    }

    /**
     * Simple export to write the index to a file on the harddrive.
     */
    fun exportMapToFile(outputFile : String, finalHapIdMap : Map<Position, Pair<List<Int>, List<Int>>>, snpMap : Map<Position,Pair<String,String>> , positionToRefRangeMap : RangeMap<Position, Int>) {
//        refRangeID\tchr\tposition\trefAllele\taltAllele\trefHapIds/taltHapIds/n
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write("refRangeId\tchr\tposition\trefAllele\taltAllele\trefHapIds\taltHapIds\n")

            finalHapIdMap.keys.sorted().forEach {
                output.write("${positionToRefRangeMap[it]}\t${it.chromosome.name}\t${it.position}\t${snpMap[it]?.first}\t${snpMap[it]?.second}\t" +
                        "${finalHapIdMap[it]?.first?.sorted()?.joinToString(",")}\t${finalHapIdMap[it]?.second?.sorted()?.joinToString(",")}\n")
            }
        }

    }

    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

        override fun getButtonName(): String {
        return "SNPToHaplotypePlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Create Read Mappings given a SNP file"
    }


    /**
     * Name of the VCF file which holds the initial mappings
     * to a single reference.
     *
     * @return VCF File
     */
    fun vcfFile(): String {
        return vcfFile.value()
    }

    /**
     * Set VCF File. Name of the VCF file which holds the
     * initial mappings to a single reference.
     *
     * @param value VCF File
     *
     * @return this plugin
     */
    fun vcfFile(value: String): IndexHaplotypesBySNPPlugin {
        vcfFile = PluginParameter<String>(vcfFile, value)
        return this
    }

    /**
     * Index File Output.
     *
     * @return Index File Output
     */
    fun outputIndexFile(): String {
        return outputIndexFile.value()
    }

    /**
     * Set Index File Output. Index File Output.
     *
     * @param value Index File Output
     *
     * @return this plugin
     */
    fun outputIndexFile(value: String): IndexHaplotypesBySNPPlugin {
        outputIndexFile = PluginParameter<String>(outputIndexFile, value)
        return this
    }

    /**
     * Database configuration file
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Database configuration file
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): IndexHaplotypesBySNPPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Pairs of methods (haplotype method name and range group
     * method name). Method pair separated by a comma, and
     * pairs separated by colon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>:<haplotype
     * method name2>,<range group name2>:<haplotype method
     * name3>
     *
     * @return Methods
     */
    fun methods(): String {
        return myMethods.value()
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name
     * and range group method name). Method pair separated
     * by a comma, and pairs separated by colon. The range
     * group is optional
     * Usage: <haplotype method name1>,<range group name1>:<haplotype
     * method name2>,<range group name2>:<haplotype method
     * name3>
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): IndexHaplotypesBySNPPlugin {
        myMethods = PluginParameter<String>(myMethods, value)
        return this
    }
    /**
     * Local directory to store GVCF files
     *
     * @return Local G V C F Dir
     */
    fun localGVCFDir(): String {
        return myLocalGVCFDir.value()
    }

    /**
     * Set Local G V C F Dir. Local directory to store GVCF
     * files
     *
     * @param value Local G V C F Dir
     *
     * @return this plugin
     */
    fun localGVCFDir(value: String): IndexHaplotypesBySNPPlugin {
        myLocalGVCFDir = PluginParameter<String>(myLocalGVCFDir, value)
        return this
    }

}

class PositionToRefAndAltHaplotypes {
    var positionToRefAndAltIdsMap = mutableMapOf<Position,Pair<MutableList<Int>,MutableList<Int>>>()

    fun addToRef(position: Position,hapIdInt: Int) {
        if(!positionToRefAndAltIdsMap.containsKey(position)) {
            positionToRefAndAltIdsMap[position] = Pair(mutableListOf(), mutableListOf())
        }

        positionToRefAndAltIdsMap[position]?.first?.add(hapIdInt)
    }


    fun addToAlt(position: Position,hapIdInt: Int) {
        if(!positionToRefAndAltIdsMap.containsKey(position)) {
            positionToRefAndAltIdsMap[position] = Pair(mutableListOf(), mutableListOf())
        }

        positionToRefAndAltIdsMap[position]?.second?.add(hapIdInt)
    }

    fun getHapIdsForPos(position: Position) : Pair<List<Int>,List<Int>> {
        return positionToRefAndAltIdsMap[position]?:Pair(listOf(),listOf())
    }

    fun getMap() : Map<Position, Pair<List<Int>, List<Int>>> {
        return positionToRefAndAltIdsMap
    }

    fun addAll(otherMap : PositionToRefAndAltHaplotypes) {
        val mapObject = otherMap.getMap()
        mapObject.keys.forEach {
            val refHaps = mapObject[it]?.first
            val altHaps = mapObject[it]?.second

            refHaps?.forEach { hapId ->
                addToRef(it,hapId)
            }

            altHaps?.forEach { hapId ->
                addToAlt(it,hapId)
            }

        }
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(IndexHaplotypesBySNPPlugin::class.java)
}