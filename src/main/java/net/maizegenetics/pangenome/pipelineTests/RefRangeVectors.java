/**
 * 
 */
package net.maizegenetics.pangenome.pipelineTests;

/**
 * This class creates a range of vectors holding data for PHG ReferenceRange objects
 * 
 * @author lcj34
 *
 */
public class RefRangeVectors {
    public int[] refRangeId;
    public String[] chromosomes;
    public int[] startPos;
    public int[] endPos;       
    public String[] refLineName;
    public int[] numberOfNodes;

    public RefRangeVectors(int[] refRangeIds, String[] chromosomes, int[] startPos, int[] endPos,  String[] refLineName, int[] numberOfNodes) {
        this.refRangeId = refRangeIds;
        this.chromosomes = chromosomes;
        this.startPos = startPos;
        this.endPos = endPos;
        this.refLineName = refLineName;
        this.numberOfNodes = numberOfNodes;
    }
}
