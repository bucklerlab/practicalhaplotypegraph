@file:JvmName("CompareHaplotypeGraphs")

package net.maizegenetics.pangenome.io

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeSequence
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * Compare two haplotype graphs
 *
 * @param graph1 first graph
 * @param graph2 second graph
 * @return true if graphs are equal
 */
fun compareGraphs(
    graph1: HaplotypeGraph,
    graph2: HaplotypeGraph,
    gvcfMap1: Map<Int, String>? = null,
    gvcfMap2: Map<Int, String>? = null
): Boolean {

    gvcfMap1?.let { gvcfMap2?.let { compareGVCFIDToFileMaps(gvcfMap1, gvcfMap2) } }

    compareReferenceRanges(graph1, graph2)

    compareHaplotypeNodes(graph1, graph2)

    compareEdges(graph1, graph2)

    return true

}

private fun compareReferenceRanges(graph1: HaplotypeGraph, graph2: HaplotypeGraph): Boolean {

    assertEquals(graph1.referenceRanges().size, graph2.referenceRanges().size, "Number of reference ranges is not equal")

    val ranges1 = graph1.referenceRangeList()
    val ranges2 = graph2.referenceRangeList()

    for (i in 0 until graph1.referenceRanges().size) {
        val range1 = ranges1[i]
        val range2 = ranges2[i]
        assertEquals(range1.id(), range2.id(), "Graphs have different reference range ids for reference range $i")
        assertEquals(range1.referenceName(), range2.referenceName(), "Graphs have different reference names for reference range $i")
        assertEquals(range1.start(), range2.start(), "Graphs have different start positions for reference range $i")
        assertEquals(range1.end(), range2.end(), "Graphs have different end positions for reference range $i")
        assertEquals(range1.chromosome(), range2.chromosome(), "Graphs have different chromosomes for reference range $i")
        assertEquals(range1.groupMethods(), range2.groupMethods(), "Graphs have different group methods for reference range $i")
    }

    return true

}

private fun compareHaplotypeNodes(graph1: HaplotypeGraph, graph2: HaplotypeGraph): Boolean {

    assertEquals(graph1.numberOfNodes(), graph2.numberOfNodes(), "Graphs have different number of haplotype nodes")

    graph1.referenceRanges().forEach { range ->
        assertEquals(graph1.nodes(range).size, graph2.nodes(range).size, "Graphs have different number of haplotype nodes for reference range ${range.id()}")
        val nodes1 = graph1.nodes(range).toMutableList()
        nodes1.sortBy { it.id() }
        val nodes2 = graph2.nodes(range).toMutableList()
        nodes2.sortBy { it.id() }
        for (i in 0 until nodes1.size) {
            val node1 = nodes1[i]
            val node2 = nodes2[i]
            assertEquals(node1.id(), node2.id(), "Graphs have different haplotype node ids for haplotype node $i")
            assertTrue(
                compareSequences(
                    node1.haplotypeSequence(),
                    node2.haplotypeSequence()
                ),
                "Graphs have different sequences for haplotype node $i"
            )
            assertEquals(node1.numTaxa(), node2.numTaxa(), "Graphs have different number of taxa for haplotype node $i")
            val taxa1 = node1.taxaList()
            val taxa2 = node2.taxaList()
            for (j in 0 until node1.numTaxa()) {
                assertEquals(taxa1[j], taxa2[j], "Graphs have different taxa for haplotype node $i")
            }
            assertEquals(node1.genomeFileID(), node2.genomeFileID(), "Graphs have different genome file ids for haplotype node $i")
            assertEquals(node1.gvcfFileID(), node2.gvcfFileID(), "Graphs have different gvcf file ids for haplotype node $i")
            assertEquals(node1.asmContig(), node2.asmContig(), "Graphs have different asm contigs for haplotype node $i")
            assertEquals(node1.asmStart(), node2.asmStart(), "Graphs have different asm starts for haplotype node $i")
            assertEquals(node1.asmEnd(), node2.asmEnd(), "Graphs have different asm ends for haplotype node $i")
            assertEquals(node1.asmStrand(), node2.asmStrand(), "Graphs have different asm strands for haplotype node $i")
        }
    }

    return true

}

private fun compareSequences(sequence1: HaplotypeSequence, sequence2: HaplotypeSequence): Boolean {

    assertEquals(sequence1.length(), sequence2.length(), "Sequences have different lengths")
    assertEquals(sequence1.sequence(), sequence2.sequence(), "Sequences have different sequences")
    assertEquals(sequence1.qualityScore(), sequence2.qualityScore(), "Sequences have different quality scores")
    assertEquals(sequence1.seqHash(), sequence2.seqHash(), "Sequences have different sequence hashes")
    assertEquals(sequence1.referenceRange().id(), sequence2.referenceRange().id(), "Sequences have different reference range ids")

    return true

}

private fun compareGVCFIDToFileMaps(map1: Map<Int, String>, map2: Map<Int, String>): Boolean {

    assertEquals(map1.size, map2.size, "GVCF ID to file maps have different sizes")

    map1.forEach { (key, value) ->
        assertTrue(map2.containsKey(key), "GVCF ID to file maps have different keys")
        assertEquals(map2[key], value, "GVCF ID to file maps have different values for key $key")
    }

    return true

}

private fun compareEdges(graph1: HaplotypeGraph, graph2: HaplotypeGraph): Boolean {

    graph1.referenceRanges().forEach { range ->

        val nodes1 = graph1.nodes(range).toMutableList()
        nodes1.sortBy { it.id() }
        val nodes2 = graph2.nodes(range).toMutableList()
        nodes2.sortBy { it.id() }

        assertEquals(nodes1.size, nodes2.size, "Graphs have different number of haplotype nodes for reference range ${range.id()}")

        for (i in 0 until nodes1.size) {
            val node1 = nodes1[i]
            val node2 = nodes2[i]
            val edges1 = graph1.rightEdges(node1).toMutableList()
            edges1.sortedWith(compareBy({ it.leftHapNode().id() }, { it.rightHapNode().id() }))
            val edges2 = graph2.rightEdges(node2).toMutableList()
            edges2.sortedWith(compareBy({ it.leftHapNode().id() }, { it.rightHapNode().id() }))
            assertEquals(edges1.size, edges2.size, "Graphs have different number of edges for haplotype node $i")
            for (j in 0 until edges1.size) {
                val edge1 = edges1[j]
                val edge2 = edges2[j]
                assertEquals(edge1.leftHapNode().id(), edge2.leftHapNode().id(), "Graphs have different left haplotype node ids for edge $j")
                assertEquals(edge1.rightHapNode().id(), edge2.rightHapNode().id(), "Graphs have different right haplotype node ids for edge $j")
                assertEquals(edge1.edgeProbability(), edge2.edgeProbability(), "Graphs have different edge probabilities for edge $j")
            }
        }

    }

    return true

}