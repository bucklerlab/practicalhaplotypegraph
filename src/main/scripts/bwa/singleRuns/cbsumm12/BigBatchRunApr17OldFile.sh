#!/bin/bash

LINE_ARRAY=(B73 CML247 CML247 CML247 CML247 CML247)
ID_ARRAY=(471_3807_3653_N_B73_ACAGTG 2220_7332_6928_H9CD2ADXX_CML247_ACAGTG 2220_7332_6928_H9C7FADXX_CML247_ACAGTG 2220_7332_6928_H99W6ADXX_CML247_ACAGTG 2220_7332_6928_H7F9YADXX_CML247_ACAGTG 2220_7332_6928_N_CML247_ACAGTG)
FIRST_FILE_NAME=(471_3807_3653_N_B73_ACAGTG_R1.fastq.gz 2220_7332_6928_H9CD2ADXX_CML247_ACAGTG_R1.fastq.gz 2220_7332_6928_H9C7FADXX_CML247_ACAGTG_R1.fastq.gz 2220_7332_6928_H99W6ADXX_CML247_ACAGTG_R1.fastq.gz 2220_7332_6928_H7F9YADXX_CML247_ACAGTG_R1.fastq.gz 2220_7332_6928_N_CML247_ACAGTG_R1.fastq.gz)
SECOND_FILE_NAME=(471_3807_3653_N_B73_ACAGTG_R2.fastq.gz 2220_7332_6928_H9CD2ADXX_CML247_ACAGTG_R2.fastq.gz 2220_7332_6928_H9C7FADXX_CML247_ACAGTG_R2.fastq.gz 2220_7332_6928_H99W6ADXX_CML247_ACAGTG_R2.fastq.gz 2220_7332_6928_H7F9YADXX_CML247_ACAGTG_R2.fastq.gz 2220_7332_6928_N_CML247_ACAGTG_R2.fastq.gz)
# to get the arrayLength, ${#ArrayName[@]}

#Set up the senteion variables and the reference path
license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02

SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
SENTIEONBWA='/programs/sentieon-genomics-201611.02/bin/bwa'
NUMBER_THREADS=20
REFERENCE='/SSD/zrm22/BAMFileGeneration/agpv4Ref/Zea_mays.AGPv4.dna.toplevel.fa.gz'

for((i=0;i<${#LINE_ARRAY[@]};i++));
do
    SAMPLE1=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${FIRST_FILE_NAME[i]}
    SAMPLE2=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${SECOND_FILE_NAME[i]}
    SORTED_BAM_1=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_RGFixed.bam
 
    time $SENTIEONBWA mem -M -R '@RG\tID:'${ID_ARRAY[i]}'\tSM:'${LINE_ARRAY[i]}'\tPL:ILLUMINA' -t $NUMBER_THREADS $REFERENCE $SAMPLE1 $SAMPLE2 | $SENTIEON util sort -o $SORTED_BAM_1 -t $NUMBER_THREADS --sam2bam -i -

    SCORE_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_RGFixed_score.txt
    DEDUP_METRIC_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_RGFixed_dedupMetric.txt
    DEDUP_BAM=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_RGFixed_dedup.bam

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo LocusCollector --fun score_info $SCORE_TXT

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo Dedup --rmdup --score_info $SCORE_TXT --metrics $DEDUP_METRIC_TXT $DEDUP_BAM
done
