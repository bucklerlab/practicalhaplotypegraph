package net.maizegenetics.pangenome.hapCalling

import htsjdk.samtools.SAMFileWriterFactory
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import htsjdk.variant.variantcontext.GenotypesContext
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.junit.SmallSeqExtension

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess

import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import java.io.File
import java.time.LocalDateTime
import kotlin.system.measureNanoTime

/**
 * This class tests the MultisampleBAMTOMappingPlugin.
 *
 * For these test cases to run successfully, the user must install minimmap2, and have
 * it available on their path.
 */
@ExtendWith(SmallSeqExtension::class)
class MultisampleBAMToMappingPluginIT {
    companion object {
        //get the current time for the method name
        val currentLocalDateTime = LocalDateTime.now()
        val multisampleBamTestDir = "${SmallSeqExtension.smallSeqDir}/multisampleBamTest/"
        val outputGroupingFile = "${multisampleBamTestDir}/groupingFile.txt"
        val outputMergedDir = "${multisampleBamTestDir}/fastq/"
        val outputMergedBAMDir = "${multisampleBamTestDir}/bam/"
        val outputMergedBAMDirMixed = "${multisampleBamTestDir}/bamMixed/"
        val imputationKeyFile = "${multisampleBamTestDir}/genotypingKeyFile.txt"
        val imputationKeyFileOriginal = "${SmallSeqExtension.rootDir}/dockerBaseDir/genotypingKeyFile.txt"
        val pathImputationKeyFile = "${multisampleBamTestDir}/genotypingKeyFile_pathKeyFile.txt"
        val outputVCFFile = "${multisampleBamTestDir}/multisampleBAMSNPs.vcf"
        val outputDebugDir = "${multisampleBamTestDir}/debug/"
        val outputDebugDirNoGraph = "${multisampleBamTestDir}/debug_noGraph/"
        val outputDebugDirNoGraphSingleFile = "${multisampleBamTestDir}/debug_noGraph_singleFile/"
        val outputDebugDirMixed = "${multisampleBamTestDir}/debugMixed/"


        @JvmStatic
        @BeforeAll
        fun setup() {
            // NOTE: File operations in setup() cause all tests to fail
            LoggingUtils.setupDebugLogging()

            //copy the key file
            File(imputationKeyFileOriginal).copyTo(File(imputationKeyFile), true)

            //Merge together the fastqs from the SmallSeqTests
            //We can use the old keyfile

            val scriptTemplate = "${multisampleBamTestDir}/sampleScript.sh"
            val referenceFile =
                "${SmallSeqExtension.baseDir}/pangenome/pangenome_GATK_PIPELINE_k21w11I90G.mmi" //TODO make it work with CONSENSUS
            File(outputMergedDir).deleteRecursively()
            File(outputMergedBAMDir).deleteRecursively()
            File(outputDebugDir).deleteRecursively()
            File(outputMergedBAMDirMixed).deleteRecursively()
            File(outputDebugDirMixed).deleteRecursively()
            File(outputDebugDirNoGraph).deleteRecursively()
            File(outputDebugDirNoGraphSingleFile).deleteRecursively()
            File(outputMergedDir).mkdirs()
            File(outputMergedBAMDir).mkdirs()
            File(outputDebugDir).mkdirs()
            File(outputMergedBAMDirMixed).mkdirs()
            File(outputDebugDirMixed).mkdirs()
            File(outputDebugDirNoGraph).mkdirs()
            File(outputDebugDirNoGraphSingleFile).mkdirs()

            MergeFastqPlugin()
                .fastqDir(SmallSeqExtension.imputationFastqDir)
                .outputBAMDir(outputMergedBAMDir)
                .outputMergedFastqDir(outputMergedDir)
                .outputGroupingFile(outputGroupingFile)
                .numberOfFastqsPerMerge(3)
                //                .numberOfFastqsPerMerge(2)
                .useOriginalReadNames(false)
                .scriptTemplate(scriptTemplate)
                .keyFile(imputationKeyFile)
                .performFunction(null)

            //align the merged fastqs to the phg made by small seq tests
            File(outputMergedDir).walk()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .forEach {
                    println(it)
                    val command = arrayOf(
                        "minimap2",
                        "-ax",
                        "sr",
                        "-t",
                        "4",
                        "--secondary=yes",
                        "-N20",
                        "--eqx",
                        referenceFile,
                        it.toString()
                    )

                    val minimap2Process = ProcessBuilder(*command)
                        .redirectOutput(File("$outputMergedBAMDir/${it.nameWithoutExtension}.sam"))
                        .redirectError(ProcessBuilder.Redirect.INHERIT)
                        .start()
                    minimap2Process.waitFor()
                }
        }
    }


    @Test
    fun testLoadingSmallSeq() {
        LoggingUtils.setupDebugLogging()
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .includeVariantContexts(false)
                .includeSequences(false)
                .methods("GATK_PIPELINE")
                .configFile(SmallSeqExtension.configFilePath)
                .performFunction(null)

        ParameterCache.load(SmallSeqExtension.configFilePath)

        val time = measureNanoTime {
            MultisampleBAMToMappingPlugin().keyFile(imputationKeyFile)
                    .fastqGroupingFile(outputGroupingFile)
                    .samDir(outputMergedBAMDir)
                    .methodName("multisampleBAMImpute_${currentLocalDateTime}")
                    .methodDescription("Using Multisample BAM read mapping")
                    .outputDebugDir(outputDebugDir)
                    .outputSecondaryMappingStats(false)
                    .numThreads(5)
                    .performFunction(graph)
        }
        println("Time spent uploading SAMs: ${time/1E9} seconds.")


        // The read mapping decoder needs a graph
        val datasetResult = graph?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (datasetResult.size != 1) {
            throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + datasetResult.size)
        }
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph
        verifyReadMappings(outputDebugDir,mappingGraph)

        println("Done verifying Read Mappings")

        BestHaplotypePathPlugin(null, false)
                .keyFile(pathImputationKeyFile)
                .readMethodName("multisampleBAMImpute_${currentLocalDateTime}")
                .pathMethodName("multisampleBAMImputePath_${currentLocalDateTime}")
                .maxNodesPerRange(30)
                .minReads(0)
                .minTransitionProb(0.001)
                .probReadMappedCorrectly(0.99)
                .minTaxaPerRange(1)
                .splitConsensusNodes(true)
                .performFunction(graph)


        println("Done finding paths")

        val graphDataSetForPath = HaplotypeGraphBuilderPlugin(null, false)
                .methods("GATK_PIPELINE")
                .includeSequences(false)
                .includeVariantContexts(true)
                .configFile(SmallSeqExtension.configFilePath)
                .localGVCFFolder(SmallSeqExtension.inputGVCFDir)
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
                .pathMethodName("multisampleBAMImputePath_${currentLocalDateTime}")
                .performFunction(graphDataSetForPath)

        println("Creating VCFs")
        PathsToVCFPlugin(null, false)
                .outputFile(outputVCFFile)
                .performFunction(paths)

        println("Done Creating VCFS.")
        //Uncomment if you would like to see the old non-exact comparison of the vcf files.
//        RunSmallSeqTestsDockerTest.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", outputVCFFile)

        compareVCFs(SmallSeqExtension.resultFilePath, outputVCFFile)
    }

    @Test
    fun testCreatingReadMappingsWithoutDB() {
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .includeVariantContexts(false)
            .includeSequences(false)
            .methods("GATK_PIPELINE")
            .configFile(SmallSeqExtension.configFilePath)
            .performFunction(null)
        ParameterCache.load(SmallSeqExtension.configFilePath)

        val hapIdMapsFile = "${SmallSeqExtension.baseDir}/hapIdMaps.json"

        CreateHapIdMapsPlugin().outputMapFile(hapIdMapsFile).performFunction(graph)

        val time = measureNanoTime {
            MultisampleBAMToMappingPlugin().keyFile(imputationKeyFile)
                .fastqGroupingFile(outputGroupingFile)
                .samDir(outputMergedBAMDir)
                .methodName("multisampleBAMImpute_${currentLocalDateTime}")
                .methodDescription("Using Multisample BAM read mapping")
                .outputDebugDir(outputDebugDirNoGraph)
                .outputSecondaryMappingStats(false)
                .numThreads(5)
                .runWithoutGraph(true)
                .hapIdMapFile(hapIdMapsFile)
                .performFunction(null)
        }
        println("Time spent uploading SAMs: ${time/1E9} seconds.")

        // The read mapping decoder needs a graph
        val datasetResult = graph?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (datasetResult.size != 1) {
            throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + datasetResult.size)
        }
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph
        verifyReadMappings(outputDebugDirNoGraph,mappingGraph)
    }

    @Test
    fun testCreatingReadMappingsWithoutDBSingleFile() {
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .includeVariantContexts(false)
            .includeSequences(false)
            .methods("GATK_PIPELINE")
            .configFile(SmallSeqExtension.configFilePath)
            .performFunction(null)
        ParameterCache.load(SmallSeqExtension.configFilePath)

        val hapIdMapsFile = "${SmallSeqExtension.baseDir}/hapIdMaps.json"

        CreateHapIdMapsPlugin().outputMapFile(hapIdMapsFile).performFunction(graph)

        val time = measureNanoTime {
            MultisampleBAMToMappingPlugin().keyFile(imputationKeyFile)
                .fastqGroupingFile(outputGroupingFile)
                .samDir(outputMergedBAMDir)
                .methodName("multisampleBAMImpute_${currentLocalDateTime}")
                .methodDescription("Using Multisample BAM read mapping")
                .outputDebugDir(outputDebugDirNoGraphSingleFile)
                .outputSecondaryMappingStats(false)
                .numThreads(5)
                .runWithoutGraph(true)
                .hapIdMapFile(hapIdMapsFile)
                .inputFile("${outputMergedBAMDir}mergedFastq_batch_5.sam")
                .performFunction(null)
        }
        println("Time spent uploading SAMs: ${time/1E9} seconds.")


        // The read mapping decoder needs a graph
        val datasetResult = graph?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (datasetResult.size != 1) {
            throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + datasetResult.size)
        }
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph
        verifyReadMappings(outputDebugDirNoGraphSingleFile,mappingGraph)
    }

    @Test
    fun testMixingBatches() {
        mixupTheBams(outputMergedBAMDir, outputMergedBAMDirMixed)

        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .includeVariantContexts(false)
            .includeSequences(false)
            .methods("GATK_PIPELINE")
            .configFile(SmallSeqExtension.configFilePath)
            .performFunction(null)

        ParameterCache.load(SmallSeqExtension.configFilePath)

        val time = measureNanoTime {
            MultisampleBAMToMappingPlugin().keyFile(imputationKeyFile)
                .fastqGroupingFile(outputGroupingFile)
                .samDir(outputMergedBAMDirMixed)
                .methodName("multisampleBAMImpute_${currentLocalDateTime}_mixed")
                .methodDescription("Using Multisample BAM read mapping")
                .outputDebugDir(outputDebugDirMixed)
                .outputSecondaryMappingStats(false)
                .numThreads(5)
                .performFunction(graph)
        }
        println("Time spent uploading SAMs: ${time/1E9} seconds.")

        // The read mapping decoder needs a graph
        val datasetResult = graph?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (datasetResult.size != 1) {
            throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + datasetResult.size)
        }
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph
        verifyReadMappings(outputDebugDirMixed,mappingGraph)
    }

    fun mixupTheBams(originalBamDir : String, mixedBAMDir : String) {
        File(originalBamDir).walkTopDown()
            .filter { it.isFile }
            .filter { !it.isHidden }
            .forEach { samFile ->

                val currentSAMReader = SamReaderFactory.makeDefault()
                    .validationStringency(ValidationStringency.SILENT)
                    .setOption(SamReaderFactory.Option.CACHE_FILE_BASED_INDEXES,false)
                    .setOption(SamReaderFactory.Option.EAGERLY_DECODE,false)
                    .setOption(SamReaderFactory.Option.INCLUDE_SOURCE_IN_RECORDS,false)
                    .setOption(SamReaderFactory.Option.VALIDATE_CRC_CHECKSUMS,false)
                    .open(samFile)

                val currentSAMReaderIter = currentSAMReader.iterator()
                val currentSAMReaderHeader = currentSAMReader.fileHeader

                val samAlignmentList = mutableListOf<SAMRecord>()
                while(currentSAMReaderIter.hasNext()) {
                    samAlignmentList.add(currentSAMReaderIter.next())
                }

                val recordsSortedByRead = samAlignmentList.groupBy { it.readName }

                val samWriter = SAMFileWriterFactory().makeSAMWriter(currentSAMReaderHeader, false, File("${mixedBAMDir}/${samFile.name}"))

                for ((read,records) in recordsSortedByRead.toList().shuffled()) {
                    for( record in records) {
                        samWriter.addAlignment(record)
                    }
                }

                currentSAMReader.close()
                samWriter.close()
            }
    }

    private fun verifyReadMappings(debugDir : String, graph: HaplotypeGraph) {
        //Get out the old readMappings

        var dbConnect = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false)
        val phgdb = PHGdbAccess(dbConnect)
        val readMappingDecoder = ReadMappingDecoder(phgdb)
        val mappingIdFile = "${SmallSeqExtension.baseDir}/genotypingKeyFile_withMappingIds.txt"
        val mappingNewFileNameToReadMap = Utils.getBufferedReader(mappingIdFile).readLines().filter { !it.startsWith("cultivar") }.map { it.split("\t") }
            .map { Pair("${it[0]}_${it[1]}_ReadMapping.txt", readMappingDecoder.getDecodedReadMappingForMappingId(it[4].toInt()) ) }
            .toMap()
        //Compare the two sets
        File(debugDir).walkTopDown()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .forEach { newFile ->
                    val name = newFile.name
                    assertTrue( mappingNewFileNameToReadMap.containsKey(name), "Unable to find new file name ${name}.")
                    val oldMappingSet = mappingNewFileNameToReadMap[name]

                    val newMappingSet = Utils.getBufferedReader(newFile.toString()).readLines()
                            .filter { !it.startsWith("#") }
                            .filter { !it.startsWith("HapIds") }
                            .map { it.split("\t") }
                            .map { Pair(it[0].split(",").map { number -> number.toInt() }.toList(), it[1].toInt()) }
                            .toMap()
                    for (mapping in newMappingSet) {
                        //Check to make sure we have all the values
                        assertTrue( oldMappingSet!!.containsKey(mapping.key), "Old Mapping missing ${name}  new set: ${mapping.key}")

                        //Check the values as well.
                        assertEquals(mapping.value, oldMappingSet[mapping.key], "Counts do not match. ${name}: Key: ${mapping.key}: New method: ${mapping.value}, Old Method: ${oldMappingSet[mapping.key]}")
                    }

                    for (oldMapping in oldMappingSet!!) {
                        //Check to make sure we have all the values
                        assertTrue(newMappingSet.containsKey(oldMapping.key), "New Mapping missing ${name}  old set: ${oldMapping.key}")

                        //Check the values as well.
                        assertEquals(newMappingSet[oldMapping.key], oldMapping.value, "Old Counts do not match. ${name}: Key: ${oldMapping.key}:  New method: ${newMappingSet[oldMapping.key]}, Old Method: ${oldMapping.value}")
                    }

                }

        phgdb.close()
    }

    /**
     * Quick test function to exactly compare two VCF files.
     */
    fun compareVCFs(originalFile: String, newFile: String) {
        assertTrue(File(originalFile).exists(), "Original file does not exist.")
        assertTrue(File(newFile).exists(), "New file does not exist.")
        assertTrue(originalFile != newFile, "Files are incorrectly the same.")

        val originalVCF = VCFFileReader(File(originalFile), false)
        val newVCF = VCFFileReader(File(newFile), false)

        val originalVCFIter = originalVCF.iterator()
        val newVCFIter = newVCF.iterator()

        while(originalVCFIter.hasNext()) {
            val originalRecord = originalVCFIter.next()
            val newRecord = newVCFIter.next()

            assertEquals(originalRecord.contig, newRecord.contig, "Chromosomes do not match.")
            assertEquals(originalRecord.start, newRecord.start, "Starts do not match.")
            assertEquals(originalRecord.end, newRecord.end, "Ends do not match.")
            assertEquals(originalRecord.reference, newRecord.reference, "References do not match.")
            assertEquals(originalRecord.alternateAlleles, newRecord.alternateAlleles, "Alternate alleles do not match.")
            compareGenotypes(originalRecord.genotypes,newRecord.genotypes)
        }

        originalVCF.close()
        newVCF.close()
    }

    /**
     * Simple function to compare the Genotypes found with in 2 exactly matching VCF records.
     */
    fun compareGenotypes(originalGenotypes: GenotypesContext, newGenotypes: GenotypesContext) {
        for (sample in originalGenotypes.sampleNames) {
            val originalGenotype = originalGenotypes.get(sample)
            val newGenotype = newGenotypes.get(sample)
            assertEquals(originalGenotype.alleles, newGenotype.alleles, "Genotypes do not match for sample ${sample}.")
        }

        for (sample in newGenotypes.sampleNames) {
            val originalGenotype = originalGenotypes.get(sample)
            val newGenotype = newGenotypes.get(sample)
            assertEquals(originalGenotype.alleles, newGenotype.alleles, "Genotypes do not match for sample ${sample}.")
        }
    }
}
