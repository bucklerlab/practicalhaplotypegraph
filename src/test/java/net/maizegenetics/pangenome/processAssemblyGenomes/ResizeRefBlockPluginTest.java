package net.maizegenetics.pangenome.processAssemblyGenomes;

import htsjdk.samtools.reference.FastaReferenceWriter;
import htsjdk.samtools.reference.FastaReferenceWriterBuilder;
import htsjdk.samtools.reference.FastaSequenceIndexCreator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.fail;

public class ResizeRefBlockPluginTest {
    String directory = System.getProperty("user.home")+"/temp/resizeRefBlockTests/";

    @Before
    public void setup() {
        LoggingUtils.setupDebugLogging();
        File dir = new File(directory);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    @Test
    public void testResizingRefBlocks() {
        //make a new vcf file with a single insertion
        String correctedVCF = directory + "testResizingRefBlocks_truth.vcf";
        try(BufferedWriter writer = Utils.getBufferedWriter(correctedVCF)) {
            writer.write("##fileformat=VCFv4.2\n");
            writer.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB73\n");
            writer.write("8\t1\t.\tG\tA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:10:99:1:0,45\n");
            writer.write("8\t38\t.\tG\tA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:5:99:1:0,45\n");
            writer.write("8\t39\t.\tA\t<NON_REF>\t.\t.\tEND=40\tGT:DP:GQ:MIN_DP:PL\t0:5:99:1:0,45\n");
            writer.write("8\t41\t.\tA\tAAA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:7:99:1:0,45\n");
            writer.write("8\t42\t.\tC\t<NON_REF>\t.\t.\tEND=45\tGT:DP:GQ:MIN_DP:PL\t0:7:99:1:0,45\n");
            writer.write("8\t46\t.\tTTCTT\tT,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:5:99:1:0,45\n");
            writer.write("8\t51\t.\tG\t<NON_REF>\t.\t.\tEND=55\tGT:DP:GQ:MIN_DP:PL\t0:10:99:1:0,45\n");
            writer.write("8\t60\t.\tC\t<NON_REF>\t.\t.\tEND=65\tGT:DP:GQ:MIN_DP:PL\t0:7:99:1:0,45\n");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        String incorrectVcfFile = directory + "testResizingRefBlocks_indels.vcf";
        String incorrectVcfFileAfterCorrection = directory + "testResizingRefBlocks_pluginCorrected.vcf";
        try(BufferedWriter writer = Utils.getBufferedWriter(incorrectVcfFile)) {
            writer.write("##fileformat=VCFv4.2\n");
            writer.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB73\n");
            writer.write("8\t1\t.\tG\tA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:10:99:1:0,45\n");
            writer.write("8\t38\t.\tG\tA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:5:99:1:0,45\n");
            writer.write("8\t39\t.\tA\t<NON_REF>\t.\t.\tEND=40\tGT:DP:GQ:MIN_DP:PL\t0:5:99:1:0,45\n");
            writer.write("8\t41\t.\tA\tAAA,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:7:99:1:0,45\n");
            writer.write("8\t42\t.\tC\t<NON_REF>\t.\t.\tEND=47\tGT:DP:GQ:MIN_DP:PL\t0:7:99:1:0,45\n");
            writer.write("8\t46\t.\tTTCTT\tT,<NON_REF>\t.\t.\t.\tGT:DP:GQ:MIN_DP:PL\t1:5:99:1:0,45\n");
            writer.write("8\t51\t.\tG\t<NON_REF>\t.\t.\tEND=55\tGT:DP:GQ:MIN_DP:PL\t0:10:99:1:0,45\n");
            writer.write("8\t60\t.\tC\t<NON_REF>\t.\t.\tEND=65\tGT:DP:GQ:MIN_DP:PL\t0:7:99:1:0,45\n");
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        //Build the Reference as well
        String referenceNoExt = directory + "ref";
        String reference = referenceNoExt + ".fa";
        FastaReferenceWriterBuilder builder = new FastaReferenceWriterBuilder()
                .setFastaFile(Paths.get(reference))
                .setDictFile(Paths.get(referenceNoExt + ".dict"));
        try(FastaReferenceWriter writer = builder.build()) {
            String bases = "TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGGAGACCATTTCTTGGTCAAAAAT" +
                    "CCGTAGATGTTAGCATTCAGTATTATTTAAAATGCTCATTCTTGCTATTTTGGACAAAAA";
             writer.startSequence("8").appendBases(bases);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //Index the reference
        try {
            FastaSequenceIndexCreator.create(Paths.get(reference), true);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        new ResizeRefBlockPlugin(null,false)
                .vCFFile(incorrectVcfFile)
                .vCFFileCorrected(incorrectVcfFileAfterCorrection)
                .ref(reference)
                .taxon("B73")
                .processData(null);


        //Now check the output
        VCFFileReader truthReader = new VCFFileReader(new File(correctedVCF), false);
        VCFFileReader correctedReader = new VCFFileReader(new File(incorrectVcfFileAfterCorrection), false);

        List<VariantContext> truthList = new ArrayList<>();
        Iterator<VariantContext> truthIterator = truthReader.iterator();
        while(truthIterator.hasNext()) {
            truthList.add(truthIterator.next());
        }
        List<VariantContext> corrected = new ArrayList<>();
        Iterator<VariantContext> correctedIterator = correctedReader.iterator();
        while(correctedIterator.hasNext()) {
            corrected.add(correctedIterator.next());
        }

        Assert.assertEquals("Sizes of the corrected and truths do not match.",truthList.size(), corrected.size());

        for(int i = 0; i < truthList.size(); i++) {
            VariantContext truth = truthList.get(i);
            VariantContext correctedVariant = corrected.get(i);

            //check the chromosomes
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getContig(), correctedVariant.getContig());
            //check the start and end positions
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getStart(), correctedVariant.getStart());
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getEnd(), correctedVariant.getEnd());
            //check the ref and alt alleles
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getReference(), correctedVariant.getReference());
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getAlternateAlleles(), correctedVariant.getAlternateAlleles());
            //check the genotype
            Assert.assertEquals("The corrected variant does not match the truth.", truth.getGenotype("B73").getGenotypeString(), correctedVariant.getGenotype("B73").getGenotypeString());
        }
    }
}
