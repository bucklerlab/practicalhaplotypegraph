package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import htsjdk.samtools.DefaultSAMRecordFactory
import htsjdk.samtools.SAMFileHeader
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.test.assertEquals

class Minimap2UtilsTest {
    lateinit var graph : HaplotypeGraph

    val testingDir = System.getProperty("user.home") + "/temp/Minimap2Utils/"
    val fastqDir = testingDir + "fastq/"
    val singleKeyFile = testingDir + "genotypingKeyFile.txt"
    val singleDuplicateKeyFile = testingDir + "genotypingKeyFile_Duplicates.txt"
    val pairedEndKeyFile = testingDir + "genotypingKeyFile_diploid.txt"
    val pairedEndDuplicateKeyFile = testingDir + "genotypingKeyFile_diploid_Duplicates.txt"
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
        graph = buildSimpleDummyGraph()
        File(testingDir).mkdirs()
        File(fastqDir).mkdirs()
        buildSimpleGenotypingKeyFile(singleKeyFile)
        buildSimpleGenotypingKeyFile(singleDuplicateKeyFile, duplicates = true)
        buildSimpleGenotypingKeyFile(pairedEndKeyFile,true)
        buildSimpleGenotypingKeyFile(pairedEndDuplicateKeyFile, pairedEnd = true, duplicates = true)
        val fileNames = listOf("${fastqDir}RecLineB1RefA1gco4_R1_1.fastq","${fastqDir}RecLineB1RefA1gco4_R1_2.fastq",
                "${fastqDir}RecLineB1RefA1gco4_R1_gbs_1.fastq","${fastqDir}RecLineB1RefA1gco4_R1_gbs_2.fastq",
                "${fastqDir}RecRefA1LineBgco6_R1_1.fastq","${fastqDir}RecRefA1LineBgco6_R1_2.fastq",
                "${fastqDir}RecRefA1LineBgco6_R1_gbs_1.fastq","${fastqDir}RecRefA1LineBgco6_R1_gbs_2.fastq")
        buildSimpleFastqFiles(fileNames)
    }

    @After
    fun tearDown() {
        File(testingDir).deleteRecursively()
        File(fastqDir).deleteRecursively()
    }

    private fun buildSimpleGenotypingKeyFile(fileName: String, pairedEnd: Boolean = false, duplicates : Boolean = false) {
        Utils.getBufferedWriter(fileName).use { writer ->
            writer.write("cultivar\tflowcell_lane\tfilename${if(pairedEnd) "\tfilename2" else ""}\tPlateID\n")
            writer.write("RecLineB1RefA1gco4_wgs\twgsFlowcell\tRecLineB1RefA1gco4_R1_1.fastq${if(pairedEnd) "\tRecLineB1RefA1gco4_R1_2.fastq" else ""}\twgs\n")
            writer.write("RecLineB1RefA1gco4_gbs\tgbsFlowcell\tRecLineB1RefA1gco4_R1_gbs_1.fastq${if(pairedEnd) "\tRecLineB1RefA1gco4_R1_gbs_2.fastq" else ""}\tgbs\n")

            if(duplicates) {
                writer.write("RecLineB1RefA1gco4_gbs\tgbsFlowcell\tRecLineB1RefA1gco4_R1_gbs_1.fastq${if(pairedEnd) "\tRecLineB1RefA1gco4_R1_gbs_2.fastq" else ""}\tgbs\n")
                writer.write("RecRefA1LineBgco6_wgs\twgsFlowcell\tRecRefA1LineBgco6_R1_1.fastq${if(pairedEnd) "\tRecRefA1LineBgco6_R1_2.fastq" else ""}\twgs\n")
            }

            writer.write("RecRefA1LineBgco6_wgs\twgsFlowcell\tRecRefA1LineBgco6_R1_1.fastq${if(pairedEnd) "\tRecRefA1LineBgco6_R1_2.fastq" else ""}\twgs\n")
            writer.write("RecRefA1LineBgco6_gbs\tgbsFlowcell\tRecRefA1LineBgco6_R1_gbs_1.fastq${if(pairedEnd) "\tRecRefA1LineBgco6_R1_gbs_2.fastq" else ""}\tgbs\n")
        }
    }

    fun buildSimpleFastqFiles(fileNames: List<String>) {
        //for each file, make a new fastq file of 1000 reads of 100 random bases each
        val rand = Random(12345)
        fileNames.forEach {
            Utils.getBufferedWriter(it).use { writer ->
                for (i in 1..1000) {
                    writer.write("@${it}_$i\n")
                    val seq=  "${(0..99).map { rand.nextInt(4) }.joinToString("") { "ACGT"[it].toString() }}"
                    writer.write("$seq\n")
                    writer.write("+\n")
                    writer.write("${(0..99).joinToString("") { "A" }}\n")
                }
            }
        }

    }


    /**
     * Function to test the getHapToRefRangeMap function in Minimap2Utils using a dummy graph structure.
     */
    @Test
    fun testGetHapToRefRangeMap() {
        Assert.assertEquals("Error building the graph, number of nodes does not match",9, graph.numberOfNodes())
        val hapIdMap = getHapToRefRangeMap(graph)

        Assert.assertEquals("Error building HapIdToRefRangeMap.  Incorrect number of Nodes in map",9, hapIdMap.size)

        //loop through the hapIdMapKeys and check to make sure the values are correct
        hapIdMap.forEach {
            Assert.assertEquals("Error building HapIdToRefRangeMap.  Incorrect value for Node Id ${it.key}",((it.key)/3)+1,it.value.id())
        }
    }

    /**
     * Function to test the getHapIdToSequenceLength function in Minimap2Utils using a dummy graph structure.
     */
    @Test
    fun testGetHapIdToSequenceLength() {
        Assert.assertEquals("Error building the graph, number of nodes does not match",9, graph.numberOfNodes())
        val hapIdToSequenceLength = getHapIdToSequenceLength(graph)

        Assert.assertEquals("Error building HapIdToSequenceLength.  Incorrect number of Nodes in map",9, hapIdToSequenceLength.size)

        //loop through the hapIdMapKeys and check to make sure the values are correct
        hapIdToSequenceLength.forEach {
            Assert.assertEquals("Error building HapIdToSequenceLength.  Incorrect value for Node Id ${it.key}",100,it.value)
        }
    }

    /**
     * Function to test the getRefRangeToHapidMap function in Minimap2Utils using a dummy graph structure.
     */
    @Test
    fun testGetRefRangeToHapidMap() {
        Assert.assertEquals("Error building the graph, number of nodes does not match",9, graph.numberOfNodes())
        val refRangeToHapidMap = getRefRangeToHapidMap(graph)

        Assert.assertEquals("Error building RefRangeToHapidMap.  Incorrect number of Nodes in map",3, refRangeToHapidMap.size)

        //loop through the hapIdMapKeys and check to make sure the values are correct
        refRangeToHapidMap.forEach {
            Assert.assertEquals("Error building RefRangeToHapidMap.  Incorrect value for Node Id ${it.key}",3,it.value.size)

            val map = it.value
            map.forEach{mapVal ->
                Assert.assertEquals("Error building RefRangeToHapidMap.  Incorrect Index value for Node Id ${it.key}",((mapVal.key)/3)+1,it.key)
            }
        }
    }

    @Test
    fun testFilterSamRecord() {
        //Make a dummy SAMRecord
        val samRecordFactory = DefaultSAMRecordFactory()

        //Make a dummy sequence of A, C, G, or Ts 100 base pairs in length
        val alleles = listOf("A","T","G","C")
        val random = Random(1234)
        val sequence = (0 until 100)
            .joinToString("") { alleles[random.nextInt(4)] }

        val simpleSAM = samRecordFactory.createSAMRecord(SAMFileHeader())
        simpleSAM.setReadName("testRead")
        simpleSAM.setReferenceName("refName")
        simpleSAM.setAlignmentStart(1)
        simpleSAM.setCigarString("100M")

        simpleSAM.setReadString(sequence)
        simpleSAM.setAttribute("NM",0)
        Assert.assertFalse("Error filtering SAMRecord.  Should be False", filterRead(simpleSAM,false,false))

        //Set simpleSam to have clipping
        simpleSAM.cigarString = "10S90M"
        Assert.assertFalse("Error filtering SAMRecord, CIGAR SoftClip at start, filter off.  Should be False", filterRead(simpleSAM,false,false))
        Assert.assertTrue("Error filtering SAMRecord, CIGAR SoftClip at start, filter on.  Should be True", filterRead(simpleSAM,false,true))
        simpleSAM.cigarString = "90M10S"
        Assert.assertFalse("Error filtering SAMRecord, CIGAR SoftClip at end, filter off.  Should be False", filterRead(simpleSAM,false,false))
        Assert.assertTrue("Error filtering SAMRecord, CIGAR SoftClip at end, filter on.  Should be True", filterRead(simpleSAM,false,true))

        //Set cigar back to normal
        simpleSAM.cigarString = "100M"

        //Set simpleSam to be unmapped
        simpleSAM.readUnmappedFlag = true
        Assert.assertTrue("Error filtering SAMRecord Unmapped Read.  Should be True", filterRead(simpleSAM,false,false))
        simpleSAM.readUnmappedFlag = false

        //set simpleSam's mate paired
        simpleSAM.readPairedFlag = true
        simpleSAM.setMateUnmappedFlag(true)
        Assert.assertTrue("Error filtering SAMRecord Unmapped Mate with Paired end.  Should be True", filterRead(simpleSAM,true,false))

        simpleSAM.mateNegativeStrandFlag = true
        simpleSAM.readNegativeStrandFlag = false
        Assert.assertFalse("Error filtering SAMRecord Checking Strands both different.  Should be False", filterRead(simpleSAM,true,false))

        simpleSAM.mateNegativeStrandFlag = true
        simpleSAM.readNegativeStrandFlag = true
        Assert.assertTrue("Error filtering SAMRecord Checking strands both same.  Should be True", filterRead(simpleSAM,true,false))

    }

    /**
     * Simple test to test the reading in of a simple single end key file
     */
    @Test
    fun testReadingKeyFile() {
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "PlateID" to 3)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","gbs")
        )
        val (headerMap, lines) = readInKeyFile(singleKeyFile)
        //"cultivar\tflowcell_lane\tfilename${if(diploid) "\tfilename2" else ""}\tPlateID\n"

        Assert.assertEquals("Error Reading Simple key file.  Header map does not match",headerTruth,headerMap)
        Assert.assertEquals("Error Reading Simple key file.  Lines do not match",linesTruth,lines)
    }

    /**
     * Simple test to verify that we do not do any duplicate checking when when we read in a Simple single end key file
     */
    @Test
    fun testReadingKeyFileDuplicates() {
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "PlateID" to 3)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","gbs")
        )
        val (headerMap, lines) = readInKeyFile(singleDuplicateKeyFile)
        //"cultivar\tflowcell_lane\tfilename${if(diploid) "\tfilename2" else ""}\tPlateID\n"

        Assert.assertEquals("Error Reading Simple key file.  Header map does not match",headerTruth,headerMap)
        Assert.assertEquals("Error Reading Simple key file.  Lines do not match",linesTruth,lines)
    }

    @Test
    fun testReadingPairedEndKeyFile() {
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "filename2" to 3, "PlateID" to 4)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq","gbs")
        )
        val (headerMap, lines) = readInKeyFile(pairedEndKeyFile)
        //"cultivar\tflowcell_lane\tfilename${if(diploid) "\tfilename2" else ""}\tPlateID\n"

        Assert.assertEquals("Error Reading Simple key file.  Header map does not match",headerTruth,headerMap)
        Assert.assertEquals("Error Reading Simple key file.  Lines do not match",linesTruth,lines)
    }

    @Test
    fun testReadingPairedEndKeyFileDuplicates() {
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "filename2" to 3, "PlateID" to 4)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq","gbs")
        )
        val (headerMap, lines) = readInKeyFile(pairedEndDuplicateKeyFile)
        //"cultivar\tflowcell_lane\tfilename${if(diploid) "\tfilename2" else ""}\tPlateID\n"

        Assert.assertEquals("Error Reading Simple key file.  Header map does not match",headerTruth,headerMap)
        Assert.assertEquals("Error Reading Simple key file.  Lines do not match",linesTruth,lines)
    }

    @Test
    fun testFullParsingInKeyFileFastq() {
        //data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
        val keyFileRecordsToFileMapTruth = mapOf(
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_wgs","wgsFlowcell") to Pair("RecLineB1RefA1gco4_R1_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_gbs","gbsFlowcell") to Pair("RecLineB1RefA1gco4_R1_gbs_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_wgs","wgsFlowcell") to Pair("RecRefA1LineBgco6_R1_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_gbs","gbsFlowcell") to Pair("RecRefA1LineBgco6_R1_gbs_1.fastq","")
        )

        val fileNameToPathMap = mapOf<String,Path>("RecLineB1RefA1gco4_R1_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_1.fastq"),
            "RecLineB1RefA1gco4_R1_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_2.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_1.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            "RecRefA1LineBgco6_R1_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_1.fastq"),
            "RecRefA1LineBgco6_R1_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_2.fastq"),
            "RecRefA1LineBgco6_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_1.fastq"),
            "RecRefA1LineBgco6_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_2.fastq")
            )
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "PlateID" to 3)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","gbs")
        )
        val parsedKeyFile = loadInReadMappingKeyFile(singleKeyFile, ReadMappingInputFileFormat.FASTQ, fastqDir, "testFullParsingInKeyFileFastq")

        Assert.assertEquals("Error parsing Simple key file. KeyFileRecordsToFileMap does not match", keyFileRecordsToFileMapTruth, parsedKeyFile.keyFileRecordsToFileMap)
        Assert.assertEquals("Error parsing Simple key file.  fileNameToPathMap does not match",fileNameToPathMap,parsedKeyFile.fileNameToPathMap)
        Assert.assertEquals("Error parsing Simple key file.  Header map does not match",headerTruth,parsedKeyFile.keyFileColumnNameMap)
        Assert.assertEquals("Error parsing Simple key file.  Lines do not match",linesTruth,parsedKeyFile.keyFileLines)

    }

    @Test
    fun testFullParsingInPairedEndKeyFileFastq() {
        //data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
        val keyFileRecordsToFileMapTruth = mapOf(
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_wgs","wgsFlowcell") to Pair("RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_gbs","gbsFlowcell") to Pair("RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_wgs","wgsFlowcell") to Pair("RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_gbs","gbsFlowcell") to Pair("RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq")
        )

        val fileNameToPathMap = mapOf<String,Path>("RecLineB1RefA1gco4_R1_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_1.fastq"),
            "RecLineB1RefA1gco4_R1_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_2.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_1.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            "RecRefA1LineBgco6_R1_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_1.fastq"),
            "RecRefA1LineBgco6_R1_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_2.fastq"),
            "RecRefA1LineBgco6_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_1.fastq"),
            "RecRefA1LineBgco6_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_2.fastq")
        )
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "filename2" to 3, "PlateID" to 4)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq","gbs")
        )
        val parsedKeyFile = loadInReadMappingKeyFile(pairedEndKeyFile, ReadMappingInputFileFormat.FASTQ, fastqDir, "testFullParsingInKeyFileFastq")

        Assert.assertEquals("Error parsing Simple key file. KeyFileRecordsToFileMap does not match", keyFileRecordsToFileMapTruth, parsedKeyFile.keyFileRecordsToFileMap)
        Assert.assertEquals("Error parsing Simple key file.  fileNameToPathMap does not match",fileNameToPathMap,parsedKeyFile.fileNameToPathMap)
        Assert.assertEquals("Error parsing Simple key file.  Header map does not match",headerTruth,parsedKeyFile.keyFileColumnNameMap)
        Assert.assertEquals("Error parsing Simple key file.  Lines do not match",linesTruth,parsedKeyFile.keyFileLines)

    }

    @Test
    fun testFullParsingInKeyFileFastqWithDuplicates() {
        //data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
        val keyFileRecordsToFileMapTruth = mapOf(
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_wgs","wgsFlowcell") to Pair("RecLineB1RefA1gco4_R1_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_gbs","gbsFlowcell") to Pair("RecLineB1RefA1gco4_R1_gbs_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_wgs","wgsFlowcell") to Pair("RecRefA1LineBgco6_R1_1.fastq",""),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_gbs","gbsFlowcell") to Pair("RecRefA1LineBgco6_R1_gbs_1.fastq","")
        )

        val fileNameToPathMap = mapOf<String,Path>("RecLineB1RefA1gco4_R1_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_1.fastq"),
            "RecLineB1RefA1gco4_R1_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_2.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_1.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            "RecRefA1LineBgco6_R1_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_1.fastq"),
            "RecRefA1LineBgco6_R1_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_2.fastq"),
            "RecRefA1LineBgco6_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_1.fastq"),
            "RecRefA1LineBgco6_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_2.fastq")
        )
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "PlateID" to 3)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","gbs")
        )
        val parsedKeyFile = loadInReadMappingKeyFile(singleDuplicateKeyFile, ReadMappingInputFileFormat.FASTQ, fastqDir, "testFullParsingInKeyFileFastq")

        Assert.assertEquals("Error parsing Simple key file. KeyFileRecordsToFileMap does not match", keyFileRecordsToFileMapTruth, parsedKeyFile.keyFileRecordsToFileMap)
        Assert.assertEquals("Error parsing Simple key file.  fileNameToPathMap does not match",fileNameToPathMap,parsedKeyFile.fileNameToPathMap)
        Assert.assertEquals("Error parsing Simple key file.  Header map does not match",headerTruth,parsedKeyFile.keyFileColumnNameMap)
        Assert.assertEquals("Error parsing Simple key file.  Lines do not match",linesTruth,parsedKeyFile.keyFileLines)

    }

    @Test
    fun testFullParsingInPairedEndKeyFileWithDuplicates() {
        //data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
        val keyFileRecordsToFileMapTruth = mapOf(
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_wgs","wgsFlowcell") to Pair("RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecLineB1RefA1gco4_gbs","gbsFlowcell") to Pair("RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_wgs","wgsFlowcell") to Pair("RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq"),
            KeyFileUniqueRecord("testFullParsingInKeyFileFastq","RecRefA1LineBgco6_gbs","gbsFlowcell") to Pair("RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq")
        )

        val fileNameToPathMap = mapOf<String,Path>("RecLineB1RefA1gco4_R1_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_1.fastq"),
            "RecLineB1RefA1gco4_R1_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_2.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_1.fastq"),
            "RecLineB1RefA1gco4_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecLineB1RefA1gco4_R1_gbs_2.fastq"),
            "RecRefA1LineBgco6_R1_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_1.fastq"),
            "RecRefA1LineBgco6_R1_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_2.fastq"),
            "RecRefA1LineBgco6_R1_gbs_1.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_1.fastq"),
            "RecRefA1LineBgco6_R1_gbs_2.fastq" to Paths.get("${fastqDir}/RecRefA1LineBgco6_R1_gbs_2.fastq")
        )
        val headerTruth = mapOf("cultivar" to 0, "flowcell_lane" to 1, "filename" to 2, "filename2" to 3, "PlateID" to 4)
        val linesTruth = listOf(
            listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq","wgs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecLineB1RefA1gco4_gbs", "gbsFlowcell", "RecLineB1RefA1gco4_R1_gbs_1.fastq","RecLineB1RefA1gco4_R1_gbs_2.fastq","gbs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_wgs", "wgsFlowcell", "RecRefA1LineBgco6_R1_1.fastq","RecRefA1LineBgco6_R1_2.fastq","wgs"),
            listOf("RecRefA1LineBgco6_gbs", "gbsFlowcell", "RecRefA1LineBgco6_R1_gbs_1.fastq","RecRefA1LineBgco6_R1_gbs_2.fastq","gbs")
        )
        val parsedKeyFile = loadInReadMappingKeyFile(pairedEndDuplicateKeyFile, ReadMappingInputFileFormat.FASTQ, fastqDir, "testFullParsingInKeyFileFastq")

        Assert.assertEquals("Error parsing Simple key file. KeyFileRecordsToFileMap does not match", keyFileRecordsToFileMapTruth, parsedKeyFile.keyFileRecordsToFileMap)
        Assert.assertEquals("Error parsing Simple key file.  fileNameToPathMap does not match",fileNameToPathMap,parsedKeyFile.fileNameToPathMap)
        Assert.assertEquals("Error parsing Simple key file.  Header map does not match",headerTruth,parsedKeyFile.keyFileColumnNameMap)
        Assert.assertEquals("Error parsing Simple key file.  Lines do not match",linesTruth,parsedKeyFile.keyFileLines)

    }

    @Test
    fun testIsKeyEntryInDirectory() {

        //get files in dir
        val filesInDir = File(fastqDir).walk().map { it.name }.filterNotNull().toHashSet()

        val currentSingleEndKeyRecord = listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","wgs")
        Assert.assertTrue("Error testing isKeyEntryInDir.  Single end key record should be in directory", isKeyEntryInDir(filesInDir, currentSingleEndKeyRecord, 2, -1))


        //Test file not found
        val currentSingleEndKeyRecordNotFound = listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1_100.fastq","wgs")
        Assert.assertFalse("Error testing isKeyEntryInDir.  Not found Single end key record should not be in directory", isKeyEntryInDir(filesInDir, currentSingleEndKeyRecordNotFound, 2, -1))


        //Test paired end
        val currentPairedEndKeyRecord = listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2.fastq","wgs")
        Assert.assertTrue("Error testing isKeyEntryInDir.  Paired end key record should be in directory", isKeyEntryInDir(filesInDir, currentPairedEndKeyRecord, 2, 3))

        //Test paired end not found
        val currentPairedEndKeyRecordNotFound = listOf("RecLineB1RefA1gco4_wgs", "wgsFlowcell", "RecLineB1RefA1gco4_R1_1.fastq","RecLineB1RefA1gco4_R1_2_100.fastq","wgs")
        Assert.assertFalse("Error testing isKeyEntryInDir.  Not found Paired end key record should not be in directory", isKeyEntryInDir(filesInDir, currentPairedEndKeyRecordNotFound, 2, 3))
    }

    @Test
    fun testSpanningSingleRefRange() {
        //Test spanning single ref range
        val refRanges = listOf(ReferenceRange("B73", Chromosome.instance(1),100,200,1),
                                ReferenceRange("B73", Chromosome.instance(1),400,500,2),
                                ReferenceRange("B73", Chromosome.instance(1),600,700,3))

        val hapIdToRangeMap = (0 ..8).associateWith { refRanges[(it / 3)] }

        val currentMappingSingleRefRange = AbstractMap.SimpleEntry(Pair("read1", true), BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2), mutableMapOf()))

        Assert.assertTrue("Error testing spanningSingleRefRange.  Entry only covers one refRange.  Should be true", spansSingleRefRange(currentMappingSingleRefRange, hapIdToRangeMap))

        val currentMapping2RefRanges = AbstractMap.SimpleEntry(Pair("read1", true), BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2,3), mutableMapOf()))
        Assert.assertFalse("Error testing spanningSingleRefRange.  Entry covers 2 refRanges.  Should be false", spansSingleRefRange(currentMapping2RefRanges, hapIdToRangeMap))
    }
    @Test
    fun testFindBestRefRange() {
        val refRangeToIdMapping = HashMultimap.create<Int, Int>()
        refRangeToIdMapping.put(1,1)
        refRangeToIdMapping.put(1,2)
        refRangeToIdMapping.put(1,3)

        Assert.assertEquals("Error testing findBestRefRange.  Only hitting a single refRange.  Should be 1", 1, findBestRefRange(refRangeToIdMapping, 0.25))

        //Add in a refRange2
        refRangeToIdMapping.put(2,4)
        Assert.assertEquals("Error testing findBestRefRange.  Only hitting a single refRange.  Should be 1", 1, findBestRefRange(refRangeToIdMapping, 0.33))
        Assert.assertEquals("Error testing findBestRefRange.  Only hitting a single refRange.  Should be 1", -1, findBestRefRange(refRangeToIdMapping, 0.13))

        //Add in another refRange2
        refRangeToIdMapping.put(2,5)
        Assert.assertEquals("Error testing findBestRefRange.  Only hitting a single refRange.  Should be 1", 1, findBestRefRange(refRangeToIdMapping, 0.5))
        Assert.assertEquals("Error testing findBestRefRange.  Only hitting a single refRange.  Should be 1", -1, findBestRefRange(refRangeToIdMapping, 0.2))
    }
    @Test
    fun testRemoveExtraRefRangeHits() {
        //Test spanning single ref range
        val refRanges = listOf(ReferenceRange("B73", Chromosome.instance(1),100,200,1),
                ReferenceRange("B73", Chromosome.instance(1),400,500,2),
                ReferenceRange("B73", Chromosome.instance(1),600,700,3))

        val hapIdToRangeMap = (0 ..8).associateWith { refRanges[(it / 3)] }

        val currentMappingSingleRefRange = AbstractMap.SimpleEntry(Pair("read1", true), BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2), mutableMapOf()))

        Assert.assertEquals("Error testing removeExtraRefRangeHits.  Entry only covers one refRange.  Should be true",3, removeExtraRefRangeHits(currentMappingSingleRefRange, hapIdToRangeMap,.25).second.listOfHapIds.size)

        val currentMapping2RefRanges = AbstractMap.SimpleEntry(Pair("read1", true), BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2,3), mutableMapOf()))
        Assert.assertEquals("Error testing removeExtraRefRangeHits.  Entry covers 2 refRanges.  Should have 3 hapIds",3, removeExtraRefRangeHits(currentMapping2RefRanges, hapIdToRangeMap,.33).second.listOfHapIds.size)
        Assert.assertEquals("Error testing removeExtraRefRangeHits.  Entry covers 2 refRanges.  Should have 3 hapIds",setOf(0,1,2), removeExtraRefRangeHits(currentMapping2RefRanges, hapIdToRangeMap,.33).second.listOfHapIds)

        //Change the maxRefRange Param
        Assert.assertEquals("Error testing removeExtraRefRangeHits.  Entry covers 2 refRanges.  Should have 0 hapIds",0, removeExtraRefRangeHits(currentMapping2RefRanges, hapIdToRangeMap,.1).second.listOfHapIds.size)
    }

    /**
     * fun keepHapIdsForSingleRefRange(bestHitMap: Map<Pair<String,Boolean>, BestAlignmentGroup>, hapIdToRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double) : Map<Pair<String,Boolean>, BestAlignmentGroup> {
     */

    @Test
    fun testKeepHapIdsForSingleRefRange() {
        //Test spanning single ref range
        val refRanges = listOf(ReferenceRange("B73", Chromosome.instance(1),100,200,1),
                ReferenceRange("B73", Chromosome.instance(1),400,500,2),
                ReferenceRange("B73", Chromosome.instance(1),600,700,3))

        val hapIdToRangeMap = (0 ..8).associateWith { refRanges[(it / 3)] }

        val currentMappingSingleRefRange = mapOf(Pair("read1", true) to BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2), mutableMapOf()))

        Assert.assertEquals("Error testing keepHapIdsForSingleRefRange.  Entry only covers one refRange.  Should be true",1, keepHapIdsForSingleRefRange(currentMappingSingleRefRange, hapIdToRangeMap,.25).size)

        //test paired end
        val currentMappingSingleRefRangePairedEnd = mapOf(Pair("read1", true) to BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2), mutableMapOf()),
                Pair("read1", false) to BestAlignmentGroup("read1", false, 0, 100, mutableSetOf(0,1), mutableMapOf()))
        Assert.assertEquals("Error testing keepHapIdsForSingleRefRange.  Entry only covers one refRange.  Should be true",2, keepHapIdsForSingleRefRange(currentMappingSingleRefRangePairedEnd, hapIdToRangeMap,.25).size)

        //Test it filtering out all refRanges.
        val currentMappingSingleRefRangeFail = mapOf(Pair("read1", true) to BestAlignmentGroup("read1", true, 0, 100, mutableSetOf(0,1,2,3,4), mutableMapOf()))
        Assert.assertEquals("Error testing keepHapIdsForSingleRefRange.  Entry only covers one refRange.  Should be true",0, keepHapIdsForSingleRefRange(currentMappingSingleRefRangeFail, hapIdToRangeMap,.25).size)
    }

    @Test
    fun testWritePathFindingKeyFile() {
        val methodName = "myMethod"
        val parsedKeyFile = loadInReadMappingKeyFile(singleDuplicateKeyFile, ReadMappingInputFileFormat.FASTQ, fastqDir, methodName)

        //generate some read mapping ids
        val idMap = parsedKeyFile.keyFileRecordsToFileMap.keys.mapIndexed { index, keyFileUniqueRecord ->
            Pair(keyFileUniqueRecord, index)
        }.toMap()

        //this next code was modified from Minimap2Utils.outputKeyFiles to avoid having to get a mapping id from a database

        val keyFileLinesWithId = parsedKeyFile.keyFileLines.mapIndexed { id, keyFileRow ->
            val uniqueRecord = KeyFileUniqueRecord(methodName, keyFileRow[0], keyFileRow[1])
            Pair(keyFileRow, idMap[uniqueRecord] ?: -1)
        }.filter { it.second > -1 }

        //the single end key file with duplicates contains
        //"RecLineB1RefA1gco4_gbs", "gbsFlowcell" twice
        //"RecLineB1RefA1gco4_wgs", "wgsFlowcell" once
        //"RecRefA1LineBgco6_gbs", "gbsFlowcell" once
        //"RecRefA1LineBgco6_wgs", "wgsFlowcell" twice
        //if taxonToHapIds is correct, it will only contain each read mapping id once

        val taxonCol = 0
        //the following line of code is from Minimap2Utils.outputKeyFiles
        //this tests whether it correctly eliminates duplicate read mapping id values
        val taxonToHapIds = keyFileLinesWithId.groupBy({ it.first[taxonCol] }, { it.second })
            .mapValues { (_, ids) -> ids.toSet() }

        println(taxonToHapIds)
        Assert.assertEquals("Expected read mapping ids", listOf(0,1,2,3), taxonToHapIds.values.flatten())
    }

}