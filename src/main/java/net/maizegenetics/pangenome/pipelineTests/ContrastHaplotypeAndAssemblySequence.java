package net.maizegenetics.pangenome.pipelineTests;

import com.google.common.base.CharMatcher;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.dna.snp.io.ReadBedfile;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Integration test to evaluate fasta file generated from GVCF files for the whole genome against assembly fasta
 * Assembly fasta
 * Read B73 reference sequences for IBD anchors
 * Read fasta file obtained with the practical haplotype pipeline (W22).
 * Read fasta file obtained from assembly (W22).
 * Power: how many of the anchor perfectly match the assembly reference?
 * Question: how frequently assembly and anchor region diverge?
 * Created by mm2842 on 6/23/17.
 */
public class ContrastHaplotypeAndAssemblySequence {

    //public static final String workdir = "/Users/edbuckler/Box Sync/Hackhaton_files_June_2017/W22_unit_test_files/";
    public static final String workdir = "/Users/mm2842/Documents/Regulatory_Regions_Project/PHG_Hackhaton/";
    public static final String alternativeSeqFile = workdir+"chrom8_anchors_W22.fasta";
    public static final String resultsfile = workdir + "Results_anchorID_W22_W22_assembly_chrom8.txt";
    private static String referenceGenomeFile = workdir+"W22_Ver12.genome.fasta";
    private static String bedFileAnchors = workdir + "chrom8_anchor_genome_position.bed";
    private static GenomeSequence refGenomeSequence = GenomeSequenceBuilder.instance(referenceGenomeFile);
    
    /**
     * Load a fasta file in a map, headers are keys and sequences are values
     * assumes a fasta file in which sequences are not breaked between lines
     */
    public static Map<String, String> getMapFromFasta(String alternativeSeqFile) throws IOException {
        List<String> fastaFile = Files.readAllLines(Paths.get(alternativeSeqFile));
        Map<String, String>  MapFasta = new TreeMap<>();
        String header = "";
        for (String line: fastaFile){
            if (line.startsWith(">")) {
                header = line.replace(">","");
            } else {
                MapFasta.put(header, line);
            }
        }
        return MapFasta;
    }


    /**
     * Check the match between anchors and reference sequence using regular expression
     * have the method here to use InterruptTimerTask class
     * because some of the anchors were taking forever to run and I want to skip them
     */
    private static Optional<Boolean> isAltSeqSameAsRefExcludingNs(String referenceSeq, String alternativeSeq){
        long startTime = System.currentTimeMillis();
        String regexSearch=alternativeSeq
                    .replaceAll("^N+", "")
                    .replaceAll("N+$", "")
                    .replaceAll("N+","[ACGT]+");
        System.out.println(regexSearch);
        if(CharMatcher.is('[').countIn(regexSearch)>4) {return Optional.empty();}
        if(regexSearch.length()<1000) {return Optional.empty();}
        Pattern pattern = Pattern.compile(regexSearch);
        Matcher matcher = pattern.matcher(referenceSeq);
        boolean found = matcher.find();
        if (found) {
            System.out.print("8:" + matcher.start() + "-" + matcher.end()+"\t");
        }
        return Optional.of(found);
    }

    /**
     * Evaluate the proportion of alternative sequence that matchs reference sequence.
     * It looks for matching fragments after rejecting too short ones
     */
    private static Optional<Boolean> almostAltSeqMatchRefSeq(String referenceSeq, String alternativeSeq) {
        int lengthAnchor = alternativeSeq.replaceAll("N","").length();
        int fragmentLimitSize = 75; //arbitrary value
        List<String> altFragments =  Arrays.asList(alternativeSeq.split("N+")).stream().
                map( frag -> frag.replace("N","") ).
                collect( Collectors.toList() ); //split and clean Ns
        altFragments.removeAll(Collections.singleton(null)); //remove singletons characters derived from inner Ns in close proximity //ACCAAACAAGANCNTN
        int poorseq = altFragments.stream().
                filter(fragment -> fragment.length() < fragmentLimitSize).
                mapToInt(fragment -> fragment.length()).
                sum(); //get the sum of bp of short fragments
        if ( 100* ((double) poorseq/lengthAnchor) >= 50 ) {return Optional.empty();}

        int goodcalls = altFragments.stream().
                filter(fragment -> fragment.length() >= fragmentLimitSize).
                filter(fragment -> referenceSeq.contains(fragment)).
                mapToInt(fragment -> fragment.length()).
                sum(); //get the sum of bp that are contained in  referenceSeq

        double percent = 100* ((double)goodcalls/lengthAnchor);
        return Optional.of(percent >= 99.0);
    }

    public static void main(String[] args) {
        String header = "Assembly_Coords\tanchorID\tMatch\tQuerySeqLength";
        Map<String, ReadBedfile.BedFileRange> anchorMap=ReadBedfile.getRanges(bedFileAnchors).stream()
                .sorted(Comparator.comparing(ReadBedfile.BedFileRange::start))
                .collect(Collectors.toMap(ReadBedfile.BedFileRange::name, ar -> ar));

        boolean appendToFile = false;
        BufferedWriter reporter = null;
        int perfectMatch=0, noMatch=0, poorSequence=0;
        int perfectBp=0, noMatchBp=0, allSequenceBp=0;
        for (int intChrom = 8; intChrom <=8;intChrom++) {
            String reference = "";
            try {
                int refChromLength = refGenomeSequence.chromosomeSequence(Chromosome.instance(Integer.toString(intChrom))).length;
                reference = NucleotideAlignmentConstants.nucleotideBytetoString(refGenomeSequence.chromosomeSequence(Chromosome.instance(Integer.toString(intChrom)),
                        1,  refChromLength)); //W22 assembly, searching the chromosome 8 for anchors to chromosome 8 of assembly
            } catch (Exception exc) {
                System.err.println(exc.getMessage());
                System.out.println("main: EXCEPTION chromosome " + intChrom + " is not in the genome file, continuing ...");
                continue; //System.exit(1);
            }
            try {
                Map<String, String> mapAnchorsW22 = getMapFromFasta(alternativeSeqFile);
                reporter = new BufferedWriter(new FileWriter(resultsfile, appendToFile));
                System.out.println(header);
                reporter.write(header);
                reporter.newLine();
                System.out.println("main: Ready to search anchors");
                long startTime = System.currentTimeMillis();
                for(Integer anchorID : anchorMap.keySet().stream().map(Integer::parseInt).sorted().collect(Collectors.toList()) ){
                    String altAnchor = mapAnchorsW22.get(anchorID.toString());
                    //Optional<Boolean> result = almostAltSeqMatchRefSeq(reference,altAnchor);
                    Optional<Boolean> result = isAltSeqSameAsRefExcludingNs(reference,altAnchor);
                    allSequenceBp+=altAnchor.length();
                    if(result.isPresent()) {
                        reporter.write(anchorID+"\t"+result.get()+"\t"+altAnchor.length());
                        reporter.newLine();
                        if(result.get()) {
                            perfectMatch++; perfectBp+=altAnchor.length();
                            System.out.println(anchorID+"\t"+result.get()+"\t"+altAnchor.length());
                        } else {
                            noMatch++; noMatchBp+=altAnchor.length();
                            System.out.println("NA\t"+anchorID+"\t"+result.get()+"\t"+altAnchor.length());
                        }
                    } else {
                        poorSequence++; //counting here those sequence that are taking too long and might not be poor sequences
                        reporter.write(anchorID+"\t"+"NA"+"\t"+altAnchor.length());
                        reporter.newLine();
                    }
                }
                reporter.newLine();
                int totalSeqs = (perfectMatch+noMatch+poorSequence);
                reporter.write("perfectMatch = " + perfectMatch + "\n");
                reporter.write("noMatch = " + noMatch + "\n");
                reporter.write("poorSequence = " + poorSequence + "\n");
                reporter.write("total=" + totalSeqs + "\n");
                reporter.write("perfectBp = " + perfectBp + "\n");
                reporter.write("noMatchBp = " + noMatchBp + "\n");
                reporter.write("totalBp = " + allSequenceBp + "\n");
                reporter.write("Perfect Match Rate = "+(double)perfectMatch/(perfectMatch+noMatch+poorSequence) + "\n");
                reporter.write("No-match Rate = "+(double)noMatch/(perfectMatch+noMatch+poorSequence) + "\n");
                reporter.write("PoorSequence Rate = "+(double)( totalSeqs - (perfectMatch+noMatch) )/ totalSeqs + "\n");
                reporter.flush();
                reporter.close();
                long endTime = System.currentTimeMillis();
                System.out.println(" took "+ (endTime-startTime)/1000 + " seconds" );
            } catch(IOException ioe){
                ioe.printStackTrace();
            }
        }
    }
}
