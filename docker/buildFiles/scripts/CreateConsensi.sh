#!/usr/bin/env bash


#Run Haplotype Collapse Pipeline

#Steps
#1. Load GVCFs stored for each haplotype in the db split by intervals
#2. For each anchor's haplotypes, Merge all the GVCFs into a single GenotypeTable using MergeGVCFPlugin
#3. Then remove all positions where there exists a indel for easier clustering
#4. Cluster the taxon together using FindHaplotypesClusterPlugin and export a GenotypeTable for each cluster
#5. Attempt to reintroduce the base pairs which were removed as indels, if the indel is consistent across all taxon in the cluster, introduce it back in.  Otherwise use the indel merge rule.
#6. Convert each genotype table into an AnchorPHG object by extracting out the fasta sequence.
#7. Collect up to 1000 consensus haplotypes and then upload them to the PHG db.

# All these steps are integrated into RunHapCollapsePipelinePlugin
#Parameters that need to be passed in
dbConfigFile=$1
REFERENCE_FILE=/phg/inputDir/reference/$2
HAPLOTYPE_METHOD=$3
COLLAPSE_METHOD=$4


#Extract out of the config the heap space, whether or not to includeVariants and the indel merge rule
memoryString="`grep -m 1 '^Xmx=' ${dbConfigFile} | cut -d'=' -f2`"
includeVariantsString="`grep -m 1 '^includeVariants=' ${dbConfigFile} | cut -d'=' -f2`"
minAlleleFrequency="`grep -m 1 '^minFreq=' ${dbConfigFile} | cut -d'=' -f2`"
rankingFile="`grep -m 1 '^rankingFile=' ${dbConfigFile} | cut -d'=' -f2`"

if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
fi

#includeVariants has to be true, if it is set to false in the config file consensus will probably fail
if [ ${includeVariantsString} != "" ]
then
    includeVariants=${includeVariantsString}
else
    includeVariants=true
fi


#Default minAlleleFrequency is 0.5
if [ ${minAlleleFrequency} != "" ]
then
    minFreq=${minAlleleFrequency}
else
    minFreq=0.5
fi

#Execute the Create Consensus Pipeline with out ranking file if ranking file is not set
#Default rankingFile is null
if [ ${rankingFile} != "" ]
then
    perl /tassel-5-standalone/run_pipeline.pl $fullXmx -configParameters $dbConfigFile -debug -HaplotypeGraphBuilderPlugin -configFile $dbConfigFile -methods ${HAPLOTYPE_METHOD} -includeVariantContexts ${includeVariants} -endPlugin \
                                                -RunHapConsensusPipelinePlugin -referenceFasta ${REFERENCE_FILE} \
                                                                                -dbConfigFile ${dbConfigFile} \
                                                                                -collapseMethod ${COLLAPSE_METHOD} \
                                                                                -collapseMethodDetails "\"${COLLAPSE_METHOD} for creating Consensus\"" \
                                                                                -minFreq ${minFreq} \
                                                                                -rankingFile ${rankingFile} \
                                                                                -endPlugin

else
    perl /tassel-5-standalone/run_pipeline.pl $fullXmx -configParameters $dbConfigFile -debug -HaplotypeGraphBuilderPlugin -configFile $dbConfigFile -methods ${HAPLOTYPE_METHOD} -includeVariantContexts ${includeVariants} -endPlugin \
                                                -RunHapConsensusPipelinePlugin -referenceFasta ${REFERENCE_FILE} \
                                                                                -dbConfigFile ${dbConfigFile} \
                                                                                -collapseMethod ${COLLAPSE_METHOD} \
                                                                                -collapseMethodDetails "\"${COLLAPSE_METHOD} for creating Consensus\"" \
                                                                                -minFreq ${minFreq} \
                                                                                -endPlugin
fi

