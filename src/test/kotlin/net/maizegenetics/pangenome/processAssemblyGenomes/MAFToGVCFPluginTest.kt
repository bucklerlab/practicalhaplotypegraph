package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.Range
import com.google.common.collect.Sets
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File
import kotlin.test.fail


class MAFToGVCFPluginTest {


    val sampleName = "B97"
    val workDir = SmallSeqPaths.baseDir
    val testingDir = "${workDir}/MAFToGVCFTests/"
    val refFile = "${testingDir}/B73Test.fa"
    val diploidRefFile = "${testingDir}/CML103DiploidTest.fa"
    val mafFile = "${testingDir}/B97.maf"
    val mafFileInverted = "${testingDir}/B97_inverted.maf"
    val diploidMafFile = "${testingDir}/B97diploid.maf"

    val truthGVCFFile = "${testingDir}/B97_truth.gvcf"
    val truthGVCFFileInverted = "${testingDir}/B97_truth_inverted.gvcf"
    val truthGVCFFile2 = "${testingDir}/B97_truth2.gvcf"
//    val outputFile = "${testingDir}/asdf.gvcf"
    val outputFile = "${testingDir}/B97.gvcf"
    val outputFileInverted = "${testingDir}/B97_inverted.gvcf"
    val diploidOutputFile1 = "${testingDir}/B97_1.gvcf"
    val diploidOutputFile2 = "${testingDir}/B97_2.gvcf"

    val cml103DiploidMafFile = "${testingDir}/CML103diploid.maf"
    val cml103_outputFile = "${testingDir}/CML103.gvcf"
    val diploidCML103File1 = "${testingDir}/CML103_1.gvcf"
    val diploidCML103File2 = "${testingDir}/CML103_2.gvcf"

    val truthCML103gvcfFile1 = "${testingDir}/CML103_truth1.gvcf"
    val truthCML103gvcfFile2 = "${testingDir}/CML103_truth2.gvcf"

    val mafDir = "${testingDir}/maf_dir_test"
    val mafDirDoesNotExist = "${testingDir}/maf_dir_does_not_exist"
    val gvcfDir = "${testingDir}/gvcf_dir_test"
    val gvcfDirDoesNotExist = "${testingDir}/gvcf_dir_does_not_exist"
    val dirTestMafFiles = listOf(
        "test1.maf",
        "test2.maf",
        "test3.maf",
    )


    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        File(testingDir).deleteRecursively()

        //Make the dir first
        File(testingDir).mkdirs()

        // Create subdirectories for testing dir parameter handling
        File(mafDir).mkdir()
        File(mafDirDoesNotExist).mkdir()
        File(gvcfDir).mkdir()
        File(gvcfDirDoesNotExist).mkdir()
        // Populate mafDir with MAFs for testing
        dirTestMafFiles.forEach{ createMAFFile("${mafDir}/${it}") }

        //Create the ref File:
        createSimpleRef(refFile)
        //Create the MAF file:
        createMAFFile(mafFile)
        //create the diploid maf file
        createDiploidMAFFile(diploidMafFile)
        //Create the known GVCF file:
        createTruthGVCFFile(truthGVCFFile)

        createInvertedMAFFile(mafFileInverted)
        createTruthInversionGVCFFile(truthGVCFFileInverted)

        // Create the known overlapping GVCF truth file for 1st diploid
        createTruthGVCFCML103_1Genome(truthCML103gvcfFile1)
        // Create the known overlapping GVCF truth file for 2nd diploid
        createTruthGVCFCML103_2Genome(truthCML103gvcfFile2)
    }


    @Test
    fun testSimpleMAFs() {
        MAFToGVCFPlugin().reference(refFile)
            .mAFFile(mafFile)
            .sampleName(sampleName)
            .gVCFOutput(outputFile)
            .fillGaps(false)
            .bgzipAndIndex(false)
            .performFunction(null)

        //Load in the output GVCF  and the truth GVCF and verify that the output is correct
        val truthVariantIterator = VCFFileReader(File(truthGVCFFile),false).iterator()
        val truthVariants = mutableListOf<VariantContext>()
        while(truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        val truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        val outputVariantIterator = VCFFileReader(File(outputFile), false).iterator()
        val outputVariants = mutableListOf<VariantContext>()
        while(outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:",truthVariants.size, outputVariants.size)

        for(variant in outputVariants) {
            if(!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals("End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end)

            //Check alleles
            Assert.assertArrayEquals("Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.alleles.toTypedArray(), variant.alleles.toTypedArray())
            //Check GT
            Assert.assertEquals("GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).genotypeString,variant.getGenotype(0).genotypeString)
            //Check AD
            Assert.assertArrayEquals("AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).ad, variant.getGenotype(0).ad)
            //Check ASM Contig
            Assert.assertEquals("ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Chr"), variant.getAttribute("ASM_Chr"))
            //Check ASM Start
            Assert.assertEquals("ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Start"), variant.getAttribute("ASM_Start"))
            //Check ASM END
            Assert.assertEquals("ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_End"), variant.getAttribute("ASM_End"))
            //Check ASM Strand
            Assert.assertEquals("ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Strand"), variant.getAttribute("ASM_Strand"))

        }
    }

    @Test
    fun testInversion() {
        //truthGVCFFileInverted
        MAFToGVCFPlugin().reference(refFile)
            .mAFFile(mafFileInverted)
            .sampleName(sampleName)
            .gVCFOutput(outputFileInverted)
            .fillGaps(false)
            .bgzipAndIndex(false)
            .performFunction(null)

        //Load in the output GVCF  and the truth GVCF and verify that the output is correct
        val truthVariantIterator = VCFFileReader(File(truthGVCFFileInverted),false).iterator()
        val truthVariants = mutableListOf<VariantContext>()
        while(truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        val truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        val outputVariantIterator = VCFFileReader(File(outputFileInverted), false).iterator()
        val outputVariants = mutableListOf<VariantContext>()
        while(outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:",truthVariants.size, outputVariants.size)

        for(variant in outputVariants) {
            if(!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals("End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                    "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end)

            //Check alleles
            Assert.assertArrayEquals("Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.alleles.toTypedArray(), variant.alleles.toTypedArray())
            //Check GT
            Assert.assertEquals("GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).genotypeString,variant.getGenotype(0).genotypeString)
            //Check AD
            Assert.assertArrayEquals("AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).ad, variant.getGenotype(0).ad)
            //Check ASM Contig
            Assert.assertEquals("ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Chr"), variant.getAttribute("ASM_Chr"))
            //Check ASM Start
            Assert.assertEquals("ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Start"), variant.getAttribute("ASM_Start"))
            //Check ASM END
            Assert.assertEquals("ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_End"), variant.getAttribute("ASM_End"))
            //Check ASM Strand
            Assert.assertEquals("ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Strand"), variant.getAttribute("ASM_Strand"))

        }
    }

    @Test
    fun testSimpleDiploidMaf() {
        // This uses the same REF as was used for the testSimpleMaf() case, and it
        // has all the same sequence - they are merely repeated entries.
        createDiploidMAFFile(diploidMafFile)
        MAFToGVCFPlugin().reference(refFile).mAFFile(diploidMafFile)
            .sampleName(sampleName).gVCFOutput(outputFile).twoGvcfs(true)
            .fillGaps(false)
            .bgzipAndIndex(false)
            .performFunction(null)


        // verify the first gvcf file - the truth file was created during setup
        //Load in the output GVCF  and the truth GVCF and verify that the output is correct
        var truthVariantIterator = VCFFileReader(File(truthGVCFFile), false).iterator()
        var truthVariants = mutableListOf<VariantContext>()
        while (truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        var truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        var outputVariantIterator = VCFFileReader(File(diploidOutputFile1), false).iterator()
        var outputVariants = mutableListOf<VariantContext>()
        while (outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:", truthVariants.size, outputVariants.size)

        for (variant in outputVariants) {
            if (!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals(
                "End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end
            )

            //Check alleles
            Assert.assertArrayEquals(
                "Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.alleles.toTypedArray(),
                variant.alleles.toTypedArray()
            )
            //Check GT
            Assert.assertEquals(
                "GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).genotypeString,
                variant.getGenotype(0).genotypeString
            )
            //Check AD
            Assert.assertArrayEquals(
                "AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).ad,
                variant.getGenotype(0).ad
            )
            //Check ASM Contig
            Assert.assertEquals(
                "ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Chr"),
                variant.getAttribute("ASM_Chr")
            )
            //Check ASM Start
            Assert.assertEquals(
                "ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Start"),
                variant.getAttribute("ASM_Start")
            )
            //Check ASM END
            Assert.assertEquals(
                "ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_End"),
                variant.getAttribute("ASM_End")
            )
            //Check ASM Strand
            Assert.assertEquals(
                "ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Strand"),
                variant.getAttribute("ASM_Strand")
            )
        }

        // verify the second vcf - note we are using truthGVCFFile2 and dipolodOutputFile2
        createTruthGVCFFileSecondGenome(truthGVCFFile2)
        truthVariantIterator = VCFFileReader(File(truthGVCFFile2), false).iterator()
        truthVariants = mutableListOf<VariantContext>()
        while (truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        outputVariantIterator = VCFFileReader(File(diploidOutputFile2), false).iterator()
        outputVariants = mutableListOf<VariantContext>()
        while (outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:", truthVariants.size, outputVariants.size)

        for (variant in outputVariants) {
            if (!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals(
                "End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end
            )

            //Check alleles
            Assert.assertArrayEquals(
                "Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.alleles.toTypedArray(),
                variant.alleles.toTypedArray()
            )
            //Check GT
            Assert.assertEquals(
                "GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).genotypeString,
                variant.getGenotype(0).genotypeString
            )
            //Check AD
            Assert.assertArrayEquals(
                "AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).ad,
                variant.getGenotype(0).ad
            )
            //Check ASM Contig
            Assert.assertEquals(
                "ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Chr"),
                variant.getAttribute("ASM_Chr")
            )
            //Check ASM Start
            Assert.assertEquals(
                "ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Start"),
                variant.getAttribute("ASM_Start")
            )
            //Check ASM END
            Assert.assertEquals(
                "ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_End"),
                variant.getAttribute("ASM_End")
            )
            //Check ASM Strand
            Assert.assertEquals(
                "ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Strand"),
                variant.getAttribute("ASM_Strand")
            )
        }

    }

    @Test
    fun testCMLOverlappingDiploidMaf() {
        createOverlappingCML103DiploidMAF(cml103DiploidMafFile)

        // NOTE - this needs a different ref file than is needed for the simpleMaf
        // and simpleDiploid MAF test cases.
        createOverlappingDiploidRef(diploidRefFile)
        MAFToGVCFPlugin().reference(diploidRefFile).mAFFile(cml103DiploidMafFile)
            .sampleName("CML103").gVCFOutput(cml103_outputFile).twoGvcfs(true)
            .fillGaps(false)
            .bgzipAndIndex(false)
            .performFunction(null)

        // verify the first gvcf file - the truth file was created during setup
        //Load in the output GVCF  and the truth GVCF and verify that the output is correct
        var truthVariantIterator = VCFFileReader(File(truthCML103gvcfFile1), false).iterator()
        var truthVariants = mutableListOf<VariantContext>()
        while (truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        var truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        var outputVariantIterator = VCFFileReader(File(diploidCML103File1), false).iterator()
        var outputVariants = mutableListOf<VariantContext>()
        while (outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:", truthVariants.size, outputVariants.size)

        for (variant in outputVariants) {
            if (!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals(
                "End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end
            )

            //Check alleles
            Assert.assertArrayEquals(
                "Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.alleles.toTypedArray(),
                variant.alleles.toTypedArray()
            )
            //Check GT
            Assert.assertEquals(
                "GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).genotypeString,
                variant.getGenotype(0).genotypeString
            )
            //Check AD
            Assert.assertArrayEquals(
                "AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).ad,
                variant.getGenotype(0).ad
            )
            //Check ASM Contig
            Assert.assertEquals(
                "ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Chr"),
                variant.getAttribute("ASM_Chr")
            )
            //Check ASM Start
            Assert.assertEquals(
                "ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Start"),
                variant.getAttribute("ASM_Start")
            )
            //Check ASM END
            Assert.assertEquals(
                "ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_End"),
                variant.getAttribute("ASM_End")
            )
            //Check ASM Strand
            Assert.assertEquals(
                "ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Strand"),
                variant.getAttribute("ASM_Strand")
            )
        }

        // verify the second vcf - note we are using truthGVCFFile2 and dipolodOutputFile2
        truthVariantIterator = VCFFileReader(File(truthCML103gvcfFile2), false).iterator()
        truthVariants = mutableListOf<VariantContext>()
        while (truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }

        outputVariantIterator = VCFFileReader(File(diploidCML103File2), false).iterator()
        outputVariants = mutableListOf<VariantContext>()
        while (outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:", truthVariants.size, outputVariants.size)

        for (variant in outputVariants) {
            if (!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals(
                "End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end
            )

            //Check alleles
            Assert.assertArrayEquals(
                "Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.alleles.toTypedArray(),
                variant.alleles.toTypedArray()
            )
            //Check GT
            Assert.assertEquals(
                "GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).genotypeString,
                variant.getGenotype(0).genotypeString
            )
            //Check AD
            Assert.assertArrayEquals(
                "AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getGenotype(0).ad,
                variant.getGenotype(0).ad
            )
            //Check ASM Contig
            Assert.assertEquals(
                "ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Chr"),
                variant.getAttribute("ASM_Chr")
            )
            //Check ASM Start
            Assert.assertEquals(
                "ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Start"),
                variant.getAttribute("ASM_Start")
            )
            //Check ASM END
            Assert.assertEquals(
                "ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_End"),
                variant.getAttribute("ASM_End")
            )
            //Check ASM Strand
            Assert.assertEquals(
                "ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}",
                matchingTruth.getAttribute("ASM_Strand"),
                variant.getAttribute("ASM_Strand")
            )
        }


    }

    /**
     * Function to create a reference file which will be used with the MAF file to create the GVCF
     * NOTE: the ref create here is used for the non-diploid test cases.  DO not change.
     */
    private fun createSimpleRef(outputFile : String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write(">chr7\n")
            output.write("${(0 until 12).map { "A" }.joinToString("")}")
            output.write("AAAGGGAATGTTAACCAAATGAATTGTCTCTTACGGTG")
            output.write("${(0 until 400).map { "A" }.joinToString("")}")
            output.write("TAAAGATGGGT\n")

            output.write(">chr1\n")
            output.write("GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")

            // added chr10 to test sorting
            output.write(">chr10\n")
            output.write("GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")

        }
    }

    /**
     * Function to create a reference file which will be used with the MAF file to create the GVCF
     * NOTE: the ref create here is used for the diploid overlapping test cases.  DO not change.
     */
    private fun createOverlappingDiploidRef(outputFile : String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write(">chr1\n")
            //output.write("GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n") // lcj - original. copied from simpleRef
            output.write("AGGCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACTAAAAAATAAAGATGGGT\n") // matches the MAF until the AAAAAA...

            output.write(">chr7\n")
            output.write("${(0 until 12).map { "T" }.joinToString("")}")
            output.write("AAAGGGAATGTTAACCAAATGAATTGTCTCTTACGGTGCACACTTGTA") // this matches the MAF CML103 diploid MAF file
            output.write("${(0 until 390).map { "T" }.joinToString("")}")
            output.write("TAAAGATGGGT\n")



            // added chr10 to test sorting
            output.write(">chr10\n")
            output.write("AGGCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACTGGGCCTACTAAAAAAAA\n")
        }
    }

    /**
     * Simple function to create a simple MAF file used for testing.  This covers most of the edge cases we have run into.
     */
    private fun createMAFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use {output ->
            output.write("##maf version=1 scoring=Tba.v8\n\n")

            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t158545518\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tB97.chr4\t81344243\t40\t+\t187371129\t-AA-GGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t6\t+\t158545518\tTAAAGAT---GGGT\n")
            output.write("s\tB97.chr4\t81444246\t6\t+\t 187371129\tTAAGGATCCC---T\n\n")

            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t40\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            // we need a chr10 in here to test sorting the maf records
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t40\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

        }
    }

    private fun createInvertedMAFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use {output ->
            output.write("##maf version=1 scoring=Tba.v8\n\n")

            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t158545518\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tB97.chr4\t81344243\t40\t+\t187371129\t-AA-GGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t6\t+\t158545518\tTAAAGAT---GGGT\n")
            output.write("s\tB97.chr4\t81444246\t6\t+\t 187371129\tTAAGGATCCC---T\n\n")

            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t - 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            // we need a chr10 in here to test sorting the maf records
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

        }
    }

    /**
     * Simple function to create a simple MAF file used for testing.  This covers most of the edge cases we have run into.
     */
    private fun createDiploidMAFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use {output ->
            output.write("##maf version=1 scoring=Tba.v8\n\n")

            //first genome
            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t158545518\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tB97.chr4\t81344243\t40\t+\t187371129\t-AA-GGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t11\t+\t158545518\tTAAAGAT---GGGT\n")
            output.write("s\tB97.chr4\t81444246\t11\t+\t 187371129\tTAAGGATCCC---T\n\n")

            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            // we need a chr10 in here to test sorting the maf records
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            //second genome, same as the first
            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t158545518\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tB97.chr4\t81344243\t40\t+\t187371129\t-AA-GGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t11\t+\t158545518\tTAAAGAT---GGGT\n")
            output.write("s\tB97.chr4\t81444246\t11\t+\t 187371129\tTAAGGATCCC---T\n\n")

            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            // we need a chr10 in here to test sorting the maf records
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

        }
    }

    /**
     * Simple function to create a simple MAF file used for testing.  This covers most of the edge cases we have run into.
     *
     */
    private fun createOverlappingCML103DiploidMAF(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use {output ->
            output.write("##maf version=1 scoring=Tba.v8\n\n")

            // NO overlapping entries for this one, so initial gvcf 2 will not have any chr1 entries.
            // All entries should be added when we process the gaps.
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t42\t+\t59\tAG---GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tCML103.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t461\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tCML103.chr4\t81344243\t41\t+\t187371129\t-AATGGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            // overlaps chr9 alignment above.  Note the corresponding REF sequence matches where the positions overlap
            // IE - the ref sequence is consistent in the 2 MAF entries where the positions overlap
            output.write("a\tscore=23260.0\n")
            output.write("s\tB73.chr7\t20\t40\t+\t461\tTGTTAACCAAATGA---ATTGTCTCTTACGGTGCACACTTGTA\n")
            output.write("s\tCML103.chr4\t81344243\t41\t+\t187371129\tTGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG--CACTTGTA\n\n")

            // nothing overlaps this one
            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t11\t+\t461\tTAAAGAT---GGGT\n")
            output.write("s\tCML103.chr4\t81444246\t11\t+\t187371129\tTAAGGATCCCG--T\n\n")


            // we need a chr10 in here to test sorting the maf records. Note the corresponding REF sequence matches where the positions overla
            // IE - the ref sequence is consistent in the 2 MAF entries where the positions overlap
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t0\t42\t+\t59\tAG---GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tCML103.chr6\t53310097\t45\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n\n")

            // this one overlaps chr10 entry above. Note the corresponding REF sequence matches where the positions overla
            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr10\t13\t38\t+\t59\tCAGTCAATCTTACACACTTGGGGCCTACTGGGCCTACT\n")
            output.write("s\tCML103.chr6\t436789\t38\t + 151104725\tCACTGAAAATATCAATCTTACACACTTGGGGCCTATCT\n\n")

        }
    }

    /**
     * Function to create the truth GVCF file
     */
    private fun createTruthGVCFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write("##fileformat=VCFv4.2\n" +
                    "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                    "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                    "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                    "##INFO=<ID=ASM_Chr,Number=1,Type=String,Description=\"Assembly chromosome\">\n" +
                    "##INFO=<ID=ASM_End,Number=1,Type=Integer,Description=\"Assembly end position\">\n" +
                    "##INFO=<ID=ASM_Start,Number=1,Type=Integer,Description=\"Assembly start position\">\n" +
                    "##INFO=<ID=ASM_Strand,Number=1,Type=String,Description=\"Assembly strand\">\n" +
                    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                    "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                    "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                    "##contig=<ID=7,length=461>\n" +
                    "##contig=<ID=1,length=100>\n" +
                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB97\n" +
                    "1\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "1\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344248;ASM_Start=81344244;ASM_Strand=+;END=18\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344249;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344252;ASM_Start=81344250;ASM_Strand=+;END=22\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344253;ASM_Start=81344253;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344256;ASM_Start=81344254;ASM_Strand=+;END=26\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344257;ASM_Start=81344257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344258;ASM_Start=81344258;ASM_Strand=+;END=28\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344259;ASM_Start=81344259;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344263;ASM_Start=81344260;ASM_Strand=+;END=33\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344267;ASM_Start=81344264;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344268;ASM_Start=81344268;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344269;ASM_Strand=+;END=43\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344277;ASM_Start=81344277;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344278;ASM_Start=81344278;ASM_Strand=+;END=45\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344279;ASM_Start=81344279;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344280;ASM_Start=81344280;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344283;ASM_Start=81344281;ASM_Strand=+;END=50\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t457\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444253;ASM_Start=81444253;ASM_Strand=+;END=457\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t458\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444254;ASM_Start=81444254;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t459\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444255;ASM_Start=81444255;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t460\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444256;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+;END=461\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n")

        }
    }


    private fun createTruthInversionGVCFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write("##fileformat=VCFv4.2\n" +
                    "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                    "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                    "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                    "##INFO=<ID=ASM_Chr,Number=1,Type=String,Description=\"Assembly chromosome\">\n" +
                    "##INFO=<ID=ASM_End,Number=1,Type=Integer,Description=\"Assembly end position\">\n" +
                    "##INFO=<ID=ASM_Start,Number=1,Type=Integer,Description=\"Assembly start position\">\n" +
                    "##INFO=<ID=ASM_Strand,Number=1,Type=String,Description=\"Assembly strand\">\n" +
                    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                    "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                    "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                    "##contig=<ID=7,length=461>\n" +
                    "##contig=<ID=1,length=100>\n" +
                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB97\n" +


                    "1\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310137;ASM_Start=53310142;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310136;ASM_Start=53310136;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310135;ASM_Start=53310135;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310134;ASM_Start=53310134;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310133;ASM_Start=53310133;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310132;ASM_Start=53310132;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310131;ASM_Start=53310131;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310130;ASM_Start=53310130;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310127;ASM_Start=53310129;ASM_Strand=-;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "1\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310126;ASM_Start=53310126;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310125;ASM_Start=53310125;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310124;ASM_Start=53310124;ASM_Strand=-\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310098;ASM_Start=53310123;ASM_Strand=-;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344248;ASM_Start=81344244;ASM_Strand=+;END=18\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344249;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344252;ASM_Start=81344250;ASM_Strand=+;END=22\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344253;ASM_Start=81344253;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344256;ASM_Start=81344254;ASM_Strand=+;END=26\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344257;ASM_Start=81344257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344258;ASM_Start=81344258;ASM_Strand=+;END=28\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344259;ASM_Start=81344259;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344263;ASM_Start=81344260;ASM_Strand=+;END=33\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344267;ASM_Start=81344264;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344268;ASM_Start=81344268;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344269;ASM_Strand=+;END=43\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344277;ASM_Start=81344277;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344278;ASM_Start=81344278;ASM_Strand=+;END=45\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344279;ASM_Start=81344279;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344280;ASM_Start=81344280;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344283;ASM_Start=81344281;ASM_Strand=+;END=50\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t457\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444253;ASM_Start=81444253;ASM_Strand=+;END=457\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t458\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444254;ASM_Start=81444254;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t459\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444255;ASM_Start=81444255;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t460\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444256;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+;END=461\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n")

        }
    }

    /**
     * Function to create the truth GVCF for the 2nd CML103 overlap file
     */
    private fun createTruthGVCFFileSecondGenome(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write("##fileformat=VCFv4.2\n" +
                    "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                    "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                    "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                    "##INFO=<ID=ASM_Chr,Number=1,Type=String,Description=\"Assembly chromosome\">\n" +
                    "##INFO=<ID=ASM_End,Number=1,Type=Integer,Description=\"Assembly end position\">\n" +
                    "##INFO=<ID=ASM_Start,Number=1,Type=Integer,Description=\"Assembly start position\">\n" +
                    "##INFO=<ID=ASM_Strand,Number=1,Type=String,Description=\"Assembly strand\">\n" +
                    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                    "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                    "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                    "##contig=<ID=7,length=461>\n" +
                    "##contig=<ID=10, length=59>\n" +
                    "##contig=<ID=1,length=100>\n" +
                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tCML103_2\n" +
                    "1\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "1\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "1\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344248;ASM_Start=81344244;ASM_Strand=+;END=18\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344249;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344252;ASM_Start=81344250;ASM_Strand=+;END=22\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344253;ASM_Start=81344253;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344256;ASM_Start=81344254;ASM_Strand=+;END=26\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344257;ASM_Start=81344257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344258;ASM_Start=81344258;ASM_Strand=+;END=28\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344259;ASM_Start=81344259;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344263;ASM_Start=81344260;ASM_Strand=+;END=33\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344267;ASM_Start=81344264;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344268;ASM_Start=81344268;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344269;ASM_Strand=+;END=43\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344277;ASM_Start=81344277;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344278;ASM_Start=81344278;ASM_Strand=+;END=45\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344279;ASM_Start=81344279;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344280;ASM_Start=81344280;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344283;ASM_Start=81344281;ASM_Strand=+;END=50\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t457\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444253;ASM_Start=81444253;ASM_Strand=+;END=457\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "7\t458\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444254;ASM_Start=81444254;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t459\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444255;ASM_Start=81444255;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t460\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444256;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+;END=461\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                    "10\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                    "10\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n")

        }
    }

    /**
     * Function to create the truth GVCF file for CML103 overlapping gvcf 1
     */
    private fun createTruthGVCFCML103_1Genome(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write(
                "##fileformat=VCFv4.2\n" +
                        "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                        "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                        "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                        "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                        "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                        "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                        "##INFO=<ID=ASM_Chr,Number=1,Type=String,Description=\"Assembly chromosome\">\n" +
                        "##INFO=<ID=ASM_End,Number=1,Type=Integer,Description=\"Assembly end position\">\n" +
                        "##INFO=<ID=ASM_Start,Number=1,Type=Integer,Description=\"Assembly start position\">\n" +
                        "##INFO=<ID=ASM_Strand,Number=1,Type=String,Description=\"Assembly strand\">\n" +
                        "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                        "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                        "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                        "##contig=<ID=7,length=461>\n" +
                        "##contig=<ID=10,length=59>\n" +
                        "##contig=<ID=1,length=100>\n" +
                        "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB97\n" +
                        "1\t1\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310098;ASM_Start=53310098;ASM_Strand=+;END=1\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t2\t.\tG\tAAAA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310102;ASM_Start=53310099;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t3\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310103;ASM_Strand=+;END=3\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t4\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t5\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t6\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t7\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t8\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t9\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t10\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t11\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=13\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t14\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t15\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t16\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t17\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=42\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344244;ASM_Start=81344244;ASM_Strand=+;END=14\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t15\t.\tA\tAT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344246;ASM_Start=81344245;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t16\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344247;ASM_Strand=+;END=18\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344250;ASM_Start=81344250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344253;ASM_Start=81344251;ASM_Strand=+;END=22\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344254;ASM_Start=81344254;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344257;ASM_Start=81344255;ASM_Strand=+;END=26\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344258;ASM_Start=81344258;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344259;ASM_Start=81344259;ASM_Strand=+;END=28\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344260;ASM_Start=81344260;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344264;ASM_Start=81344261;ASM_Strand=+;END=33\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344268;ASM_Start=81344265;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344269;ASM_Start=81344269;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344277;ASM_Start=81344270;ASM_Strand=+;END=43\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344278;ASM_Start=81344278;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344279;ASM_Start=81344279;ASM_Strand=+;END=45\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344280;ASM_Start=81344280;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344281;ASM_Start=81344281;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344284;ASM_Start=81344282;ASM_Strand=+;END=50\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t50\t.\tCCA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344276;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t53\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344284;ASM_Start=81344277;ASM_Strand=+;END=60\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t457\t.\tT\tTCCC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444253;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t458\t.\tGGG\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444258;ASM_Start=81444258;ASM_Strand=+;END=461\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t1\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310098;ASM_Start=53310098;ASM_Strand=+;END=1\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t2\t.\tG\tAAAA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310102;ASM_Start=53310099;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t3\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310103;ASM_Strand=+;END=3\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t4\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t5\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t6\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t7\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t8\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t9\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t10\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t11\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=13\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t14\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t15\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t16\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t17\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=42\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t43\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436820;ASM_Start=436819;ASM_Strand=+;END=44\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t45\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436821;ASM_Start=436821;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t46\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436822;ASM_Start=436822;ASM_Strand=+;END=46\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t47\t.\tC\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436823;ASM_Start=436823;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t48\t.\tT\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436824;ASM_Start=436824;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t49\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436825;ASM_Start=436825;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t50\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436827;ASM_Start=436826;ASM_Strand=+;END=51\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n"
            )
        }
    }

    /**
     * Function to create the truth GVCF file for CML103 overlapping gvcf 2
     */
    private fun createTruthGVCFCML103_2Genome(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write(
                "##fileformat=VCFv4.2\n" +
                        "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                        "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                        "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                        "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                        "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                        "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                        "##INFO=<ID=ASM_Chr,Number=1,Type=String,Description=\"Assembly chromosome\">\n" +
                        "##INFO=<ID=ASM_End,Number=1,Type=Integer,Description=\"Assembly end position\">\n" +
                        "##INFO=<ID=ASM_Start,Number=1,Type=Integer,Description=\"Assembly start position\">\n" +
                        "##INFO=<ID=ASM_Strand,Number=1,Type=String,Description=\"Assembly strand\">\n" +
                        "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                        "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                        "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                        "##contig=<ID=7,length=461>\n" +
                        "##contig=<ID=10,length=59>\n" +
                        "##contig=<ID=1,length=100>\n" +
                        "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB97\n" +
                        "1\t1\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310098;ASM_Start=53310098;ASM_Strand=+;END=1\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t2\t.\tG\tAAAA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310102;ASM_Start=53310099;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t3\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310103;ASM_Strand=+;END=3\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t4\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t5\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t6\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t7\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t8\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t9\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t10\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t11\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=13\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "1\t14\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t15\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t16\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "1\t17\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=42\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344244;ASM_Start=81344244;ASM_Strand=+;END=14\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t15\t.\tA\tAT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344246;ASM_Start=81344245;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t16\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344247;ASM_Strand=+;END=18\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344250;ASM_Start=81344250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344251;ASM_Start=81344251;ASM_Strand=+;END=20\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t21\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344245;ASM_Start=81344244;ASM_Strand=+;END=22\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344246;ASM_Start=81344246;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344247;ASM_Strand=+;END=26\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344250;ASM_Start=81344250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344251;ASM_Start=81344251;ASM_Strand=+;END=28\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344252;ASM_Start=81344252;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344256;ASM_Start=81344253;ASM_Strand=+;END=33\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344260;ASM_Start=81344257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344261;ASM_Start=81344261;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344269;ASM_Start=81344262;ASM_Strand=+;END=43\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344270;ASM_Start=81344270;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344271;ASM_Start=81344271;ASM_Strand=+;END=45\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344272;ASM_Start=81344272;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344273;ASM_Start=81344273;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344275;ASM_Start=81344274;ASM_Strand=+;END=49\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t50\t.\tGCA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344276;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t53\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344284;ASM_Start=81344277;ASM_Strand=+;END=60\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "7\t457\t.\tT\tTCCC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444253;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t458\t.\tGGG\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444258;ASM_Start=81444258;ASM_Strand=+;END=461\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t1\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310098;ASM_Start=53310098;ASM_Strand=+;END=1\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t2\t.\tG\tAAAA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310102;ASM_Start=53310099;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t3\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310103;ASM_Strand=+;END=3\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t4\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t5\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t6\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t7\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t8\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t9\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t10\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t11\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=13\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t14\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436791;ASM_Start=436790;ASM_Strand=+;END=15\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t16\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436792;ASM_Start=436792;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t17\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436793;ASM_Start=436793;ASM_Strand=+;END=17\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t18\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436794;ASM_Start=436794;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t19\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436796;ASM_Start=436795;ASM_Strand=+;END=20\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t21\t.\tT\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436797;ASM_Start=436797;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t22\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436798;ASM_Start=436798;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t23\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436799;ASM_Start=436799;ASM_Strand=+;END=23\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t24\t.\tT\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436800;ASM_Start=436800;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t25\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436801;ASM_Start=436801;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t26\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436803;ASM_Start=436802;ASM_Strand=+;END=27\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t28\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436804;ASM_Start=436804;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t29\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436805;ASM_Start=436805;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t30\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436808;ASM_Start=436806;ASM_Strand=+;END=32\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t33\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436809;ASM_Start=436809;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t34\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436810;ASM_Start=436810;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t35\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436811;ASM_Start=436811;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t36\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436812;ASM_Start=436812;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t37\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436813;ASM_Start=436813;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t38\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436815;ASM_Start=436814;ASM_Strand=+;END=39\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t40\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436816;ASM_Start=436816;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t41\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436817;ASM_Start=436817;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t42\t.\tT\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436818;ASM_Start=436818;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t43\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436820;ASM_Start=436819;ASM_Strand=+;END=44\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t45\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436821;ASM_Start=436821;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t46\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436822;ASM_Start=436822;ASM_Strand=+;END=46\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n" +
                        "10\t47\t.\tC\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436823;ASM_Start=436823;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t48\t.\tT\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436824;ASM_Start=436824;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t49\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436825;ASM_Start=436825;ASM_Strand=+\tGT:AD:DP:PL\t1:0,30,0:30:90,0,90\n" +
                        "10\t50\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=436827;ASM_Start=436826;ASM_Strand=+;END=51\tGT:AD:DP:PL\t0:30,0:30:0,90,90\n")
        }
    }
}