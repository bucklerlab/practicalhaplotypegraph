package net.maizegenetics.pangenome.hapCalling

import htsjdk.variant.variantcontext.Allele
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.VariantContextBuilder
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.convertContextToInfo
import org.junit.jupiter.api.Test
import kotlin.test.*

class PathsToVCFPluginTest {

    @Test
    fun testCreateVariantContext() {
        val taxa1 = "taxa1"
        val taxa2 = "taxa2"

        val chromosome = "1"
        val position = 100
        val taxaToInfos = mutableMapOf<String, List<HaplotypeNode.VariantInfo?>>()
        taxaToInfos[taxa1] = listOf(null)
        taxaToInfos[taxa2] = listOf(null)

        val vc = PathsToVCFPlugin()
            .createVariantContext(chromosome = chromosome, position = position, taxaToInfos = taxaToInfos)

        assertNotNull(vc, "Variant Context should not be null")

        assertEquals(2, vc.nSamples)
        assertEquals(2, vc.sampleNames.size)
        assertTrue(vc.sampleNames.contains(taxa1))
        assertTrue(vc.sampleNames.contains(taxa2))

        assertTrue(vc.attributes.isEmpty())
        assertTrue(vc.commonInfo.attributes.isEmpty())

        assertEquals(".", vc.commonInfo.name) // Does the `.` mean it is unpopulated?

        assertEquals(0, vc.calledChrCount)
        assertTrue(vc.reference.isReference)
        assertFalse(vc.reference.isNonReference)
        assertEquals("N", vc.reference.displayString)
        assertEquals("N", vc.reference.baseString)

        assertEquals(2, vc.genotypes.size)
        assertEquals(2, vc.genotypes.sampleNames.size)
        assertTrue(vc.genotypes.sampleNames.contains(taxa1))
        assertTrue(vc.genotypes.sampleNames.contains(taxa2))

        assertEquals(1, vc.alleles.size)
        assertEquals("N", vc.alleles[0].baseString)
        assertEquals("N", vc.alleles[0].displayString)

        assertTrue(vc.alleles[0].isCalled)
        assertFalse(vc.alleles[0].isBreakpoint)
        assertFalse(vc.alleles[0].isNoCall)
        assertTrue(vc.alleles[0].isReference)
        assertFalse(vc.alleles[0].isNonRefAllele)
        assertFalse(vc.alleles[0].isNonReference)

        assertEquals(0, vc.alternateAlleles.size)
    }

    @Test
    fun testVariantContextWithMultipleAltAlleles() {
        //create a VariantContext with two alt alleles, the second of which is an indel
        //first make some genotypes
        val alleleAref = Allele.create("A", true)
        val alleleC = Allele.create("C", false)
        val alleleN = Allele.create("N", false)
        val alleleIns = Allele.create("ATTT", false)
        val alleleNonref = Allele.create("<NON-REF>", false)

        val genoA = GenotypeBuilder("taxonA", listOf(alleleAref)).make()
        val genoC = GenotypeBuilder("taxonC", listOf(alleleC)).make()
        val genoIns = GenotypeBuilder("taxonIns", listOf(alleleIns)).make()
        val genoNR = GenotypeBuilder("taxonNref", listOf(alleleNonref)).make()
        val genoN = GenotypeBuilder("taxonN", listOf(alleleN)).make()

        val vcA = VariantContextBuilder()
            .alleles(listOf(alleleAref, alleleC, alleleIns))
            .genotypes(genoA)
            .loc("chr1", 100,100).make()
        val vcC = VariantContextBuilder()
            .alleles(listOf(alleleAref, alleleC, alleleIns))
            .genotypes(genoC)
            .loc("chr1", 100,100).make()
        val vcIns = VariantContextBuilder()
            .alleles(listOf(alleleAref, alleleC, alleleIns))
            .genotypes(genoIns)
            .loc("chr1", 100,100).make()
        val vcNR = VariantContextBuilder()
            .alleles(listOf(alleleAref, alleleC, alleleNonref))
            .genotypes(genoNR)
            .loc("chr1", 100,100).make()
        val vcN = VariantContextBuilder()
            .alleles(listOf(alleleAref, alleleN, alleleNonref))
            .genotypes(genoN)
            .loc("chr1", 100,100).make()

        //create a map of taxaname to variant info list
        val taxaToInfos = mutableMapOf("taxon1" to listOf(convertContextToInfo(vcA, 0)),
            "taxon2" to listOf(convertContextToInfo(vcC, 0)),
            "taxon3" to listOf(convertContextToInfo(vcIns, 0)),
            "taxon4" to listOf(convertContextToInfo(vcNR, 0)),
            "taxon5" to listOf(convertContextToInfo(vcN, 0)),
            "taxon6" to listOf(convertContextToInfo(vcA, 0), convertContextToInfo(vcC, 0)),
            "taxon7" to listOf(convertContextToInfo(vcA, 0), convertContextToInfo(vcIns, 0)),
            "taxon8" to listOf(convertContextToInfo(vcA, 0), convertContextToInfo(vcNR, 0)),
            "taxon9" to listOf(convertContextToInfo(vcA, 0), convertContextToInfo(vcN, 0))
        )


        val pathsToVcf = PathsToVCFPlugin()

        val resultVC = pathsToVcf
            .createVariantContext(chromosome = "chr1", position = 100, taxaToInfos = taxaToInfos)

        //haploid path
        assertEquals("A/A", resultVC.getGenotype(0).genotypeString)
        assertEquals("C/C", resultVC.getGenotype(1).genotypeString)
        //insertion
        assertEquals("./.", resultVC.getGenotype(2).genotypeString)
        //nonref
        assertEquals("./.",  resultVC.getGenotype(3).genotypeString)
        //N
        assertEquals("./.",  resultVC.getGenotype(4).genotypeString)

        //diploid path
        assertEquals("A/C",  resultVC.getGenotype(5).genotypeString)
        //A/Insertion
        assertEquals("A/.",  resultVC.getGenotype(6).genotypeString)
        //A/NonRef
        assertEquals("A/.",  resultVC.getGenotype(7).genotypeString)
        //A/N
        assertEquals("A/.",  resultVC.getGenotype(8).genotypeString)

        pathsToVcf.symbolicToN(true)
        pathsToVcf.symbolicOutput(false)

        val resultVCtoN = pathsToVcf
            .createVariantContext(chromosome = "chr1", position = 100, taxaToInfos = taxaToInfos)

        //haploid path
        assertEquals("A/A", resultVCtoN.getGenotype(0).genotypeString)
        assertEquals("C/C", resultVCtoN.getGenotype(1).genotypeString)
        //insertion
        assertEquals("N/N", resultVCtoN.getGenotype(2).genotypeString)
        //nonref
        assertEquals("N/N",  resultVCtoN.getGenotype(3).genotypeString)
        //N
        assertEquals("./.",  resultVCtoN.getGenotype(4).genotypeString)

        //diploid path
        assertEquals("A/C",  resultVCtoN.getGenotype(5).genotypeString)
        //A/Insertion
        assertEquals("A/N",  resultVCtoN.getGenotype(6).genotypeString)
        //A/NonRef
        assertEquals("A/N",  resultVCtoN.getGenotype(7).genotypeString)
        //A/N
        assertEquals("A/.",  resultVCtoN.getGenotype(8).genotypeString)

        pathsToVcf.symbolicToN(false)
        pathsToVcf.symbolicOutput(true)

        val resultVCsymbolic = pathsToVcf
            .createVariantContext(chromosome = "chr1", position = 100, taxaToInfos = taxaToInfos)

        //haploid path
        assertEquals("A/A", resultVCsymbolic.getGenotype(0).genotypeString)
        assertEquals("C/C", resultVCsymbolic.getGenotype(1).genotypeString)
        //insertion (because symbolic alleles return an empty string)
        assertEquals("/", resultVCsymbolic.getGenotype(2).genotypeString)
        assertEquals("<INS>", resultVCsymbolic.getGenotype(2).alleles[0].displayString)
        //nonref
        assertEquals("/",  resultVCsymbolic.getGenotype(3).genotypeString)
        assertEquals("<NON-REF>", resultVCsymbolic.getGenotype(3).alleles[0].displayString)
        //N
        assertEquals("./.",  resultVCsymbolic.getGenotype(4).genotypeString)

        //diploid path
        assertEquals("A/C",  resultVCsymbolic.getGenotype(5).genotypeString)
        //A/Insertion
        assertEquals("A/",  resultVCsymbolic.getGenotype(6).genotypeString)
        assertEquals("<INS>", resultVCsymbolic.getGenotype(6).alleles[1].displayString)
        //A/NonRef
        assertEquals("A/",  resultVCsymbolic.getGenotype(7).genotypeString)
        assertEquals("<NON-REF>", resultVCsymbolic.getGenotype(7).alleles[1].displayString)
        //A/N
        assertEquals("A/.",  resultVCsymbolic.getGenotype(8).genotypeString)

    }
}