package net.maizegenetics.pangenome.db_loading

import kotlin.test.*
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

/**
 * This class tests loading data to the haplotype_list table
 * It works on a database created from the smallSeq program but would
 * work on any PHG db that includes the haplotype_list table
 *
 * Before each run of this test, the haplotype_list table entries
 * should be deleted.
 */
@ExtendWith(SmallSeqExtension::class)
class HaplotypeListTableIT {

    @Test
    fun testHaplotypeListTableFromPHGdbAccess() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)
        // Table has: id, list_hash, and a blob
        val hapids1 = listOf(12, 1, 71, 65)
        val hapids2 = listOf(34, 1, 71, 85)
        // THis is the same list as hapids1, but in a different order
        // It should result in the same hash value getting created
        val hapids3 = listOf(1,65,12,71)

        // verify the same hash is created for different
        val hashString1 = hapids1.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_1 = AnchorDataPHG.getChecksumForString(hashString1,"MD5")

        val hashString3 = hapids3.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_3 = AnchorDataPHG.getChecksumForString(hashString3,"MD5")

        assertEquals(hashMD5_1, hashMD5_3)

        // verify this entry does not yet exist
        val testId = phg.getHaplotypeListIDfromHash(hashMD5_1)
        assertEquals(0, testId)

        // Should return 1 - unless you've already added/deleted, then
        // it returns the next number in line, even though there may only
        // be 1 entry in the db.  That is postgres.
        val haplotypeId1 = phg.putHalotypeListData(hapids1)
        assertEquals(2, haplotypeId1) //Updating this to be 2 as SmallSeqExtension will create a haplotypeListEntry first

        val haplotypeId2 = phg.putHalotypeListData(hapids2)
        assertEquals(3, haplotypeId2) //Updating this to be 3 as SmallSeqExtension will create a haplotypeListEntry first

        // repeat haplotypeId insert for 1 - generatedKeys should return
        // the correct value (ie 2)
        val haplotypeId1Again = phg.putHalotypeListData(hapids1)
        assertEquals(2, haplotypeId1Again)

        phg.close()
    }

    @Test
    fun testGetReadMappingsAndHaplotypeListForId() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val mappingsForId10 = phg.getReadMappingsForId(10)
        val haplotypeListId10 = mappingsForId10.second
        val hapids = phg.getHaplotypeList(haplotypeListId10)

        //Based on how SmallSeq is setup this this should be the same as what is in the GATK_PIPELINE graph
        //build GATK_PIPELINE graph
        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .methods("GATK_PIPELINE")
            .includeSequences(false)
            .build()

        val graphHapIds = graph.referenceRanges().flatMap { graph.nodes(it) }.map { it.id() }.toSet()

        assertEquals(graphHapIds.size, hapids.size, "Number of hapIds in the graph do not match hapids for the readMappings")
        for(hapId in hapids) {
            assertTrue(graphHapIds.contains(hapId), "HapId $hapId is not in the graph")
        }

        val mappingsForId1 = phg.getReadMappingsForId(0)
        assertEquals(null, mappingsForId1, "Read mapping for id 0 should be null")

        phg.close()
    }
}