package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Range
import com.google.common.collect.TreeRangeMap
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import java.io.File
import java.time.LocalDateTime
import kotlin.test.fail


data class IndexRow(val refRangeId: Int,val chr:String, val position: Int, val refAllele:String,val altAllele: String, val refHapIds:List<Int>,val altHapIds:List<Int>)

@ExtendWith(SmallSeqExtension::class)
class IndexHaplotypesBySNPPluginIT {

    companion object {
        //get the current time for the method name
        val currentLocalDateTime = LocalDateTime.now()

        val indexHaplotypesBySNPDir = "${SmallSeqExtension.smallSeqDir}/indexHaplotypesBySNPPlugin/"

        @JvmStatic
        @BeforeAll
        fun setup() {
            // NOTE: File operations in setup() cause all tests to fail
            LoggingUtils.setupDebugLogging()

            File(indexHaplotypesBySNPDir).mkdirs()
        }
    }


    @Test
    fun testVCFUploadFull() {
        val vcfFile = SmallSeqExtension.resultFilePath
        val graphConfig = SmallSeqExtension.configFilePath
        val outputIndexFile = "${indexHaplotypesBySNPDir}/testVCFIndex_Streaming.txt"


        IndexHaplotypesBySNPPlugin()
            .vcfFile(vcfFile)
            .outputIndexFile(outputIndexFile)
            .methods("GATK_PIPELINE")
            .configFile(graphConfig)
            .localGVCFDir(SmallSeqExtension.inputGVCFDir)
            .performFunction(null)

        //load in the index file into a hashMap
        val indexLines = Utils.getBufferedReader(outputIndexFile).readLines().filter { !it.startsWith("refRangeId") }.map { it.split("\t") }
            .map { IndexRow(it[0].toInt(),it[1],it[2].toInt(),it[3],it[4],it[5].split(",").map { it.toInt() },it[6].split(",").map { it.toInt() }) }

        //need to loop through the indexLines and create a Map<HapId,List<Triple<chr,pos, allele>>>
        val hapIdToAlleleList = mutableMapOf<Int,List<Triple<String,Int,String>>>()

        indexLines.forEach { indexRow ->
            indexRow.refHapIds.forEach { hapId ->
                hapIdToAlleleList.getOrPut(hapId) { listOf() }.plus(Triple(indexRow.chr,indexRow.position,indexRow.refAllele))
            }
            indexRow.altHapIds.forEach { hapId ->
                hapIdToAlleleList.getOrPut(hapId) { listOf() }.plus(Triple(indexRow.chr,indexRow.position,indexRow.altAllele))
            }
        }


        //load in the graph and check that the snps for each haplotype match the index
        val graph = HaplotypeGraphBuilderPlugin(null,false)
            .methods("GATK_PIPELINE")
            .configFile(graphConfig)
            .includeSequences(false)
            .includeVariantContexts(true)
            .localGVCFFolder(SmallSeqExtension.inputGVCFDir)
            .build()

        graph.referenceRanges().flatMap { graph.nodes(it) }.forEach { checkSNPsForHaplotype(it,hapIdToAlleleList) }
    }

    fun checkSNPsForHaplotype(haplotype: HaplotypeNode, hapIdToAlleleList: Map<Int,List<Triple<String,Int,String>>>){
        val hapId = haplotype.id()
        val snps = haplotype.variantInfos().get()


        val indexSNPList = hapIdToAlleleList[hapId] ?: fail("No snps found in index for haplotype $hapId")

        val treeRangeMap = TreeRangeMap.create<Int,String>()
        snps.forEach { treeRangeMap.put(Range.closed(it.start(),it.end()),it.genotypeString()) }

        for(indexSNP in indexSNPList){
            val snpRange = treeRangeMap.get(indexSNP.second) ?: fail("No SNP found for haplotype $hapId at position ${indexSNP.second}")
            assertEquals(indexSNP.third,snpRange,"SNP found for haplotype $hapId at position ${indexSNP.second} does not match expected allele ${indexSNP.third} (found $snpRange)")
        }
    }
}