package net.maizegenetics.pangenome.api

/**
 * Function to check to see if a new variantInfo can be merged to the list of refBlocks.
 *
 * If the variantInfo is a variant or indel or if it is at least 2 bps away from the end of the previous it will return false
 *
 * Only if it is a consecutive reference block
 */
fun canVariantInfoBeAddedToRefBlockList(refBlocks: List<HaplotypeNode.VariantInfo>, newVariantInfo: HaplotypeNode.VariantInfo): Boolean {
    //If a variant or an indel, we should not add to the list.
    if (newVariantInfo.isVariant || newVariantInfo.isIndel) {
        return false
    }
    //if it is empty we can always add it
    if (refBlocks.isEmpty()) {
        return true
    }

    return areVariantInfosConsecutive(refBlocks.last(),newVariantInfo)
}

/**
 * Function to merge the reference blocks
 */
fun mergeRefBlocks(refBlocks: List<HaplotypeNode.VariantInfo>, refRangeStart:Int, refRangeEnd:Int): HaplotypeNode.VariantInfo {
    //Verify that the refBlocks are all able to be merged.
    check(canVariantInfosBeMerged(refBlocks)) {"Error merging Consecutive Reference VariantInfos.  "}
    val firstRefBlock = refBlocks.first()
    val secondRefBlock = refBlocks.last()

    val avgDepth = computeDepths(refBlocks)

    val asmInfo = determineASMInfo(refBlocks, refRangeStart, refRangeEnd)

    return HaplotypeNode.VariantInfo(firstRefBlock.chromosome(), firstRefBlock.start(), secondRefBlock.end(),
        firstRefBlock.genotypeString(), firstRefBlock.refAlleleString(), firstRefBlock.altAlleleString(),
        firstRefBlock.isVariant, intArrayOf(avgDepth.toInt()),
        asmInfo.asmChr, asmInfo.asmStart,asmInfo.asmEnd,asmInfo.asmStrand)
}

/**
 * Method to determine if the list is able to be merged together.
 */
fun canVariantInfosBeMerged(variantInfoList: List<HaplotypeNode.VariantInfo>) : Boolean {
    val variantInfosWithoutVariants = variantInfoList.filter { !(it.isVariant || it.isIndel) }

    //Make sure we do not have any SNPs or indels
    if(variantInfosWithoutVariants.size != variantInfoList.size) {
        return false
    }

    //If any are not able to be merged, this will throw a false
    return variantInfosWithoutVariants.zipWithNext()
            .all { areVariantInfosConsecutive(it.first,it.second) }
}

/**
 * Simple method to check chromosome and positions to see if the infos are consective.
 */
fun areVariantInfosConsecutive(variantInfo1: HaplotypeNode.VariantInfo, variantInfo2 : HaplotypeNode.VariantInfo) : Boolean {
    //Check the chromosome and position.  Using >= as the refBlocks might be overlapping.
    return variantInfo1.chromosome() == variantInfo2.chromosome() && variantInfo1.end() + 1 >= variantInfo2.start()
}

fun computeDepths(variants: List<HaplotypeNode.VariantInfo>): Double {
    return variants.map { it.depth()[0] }.average()
}
