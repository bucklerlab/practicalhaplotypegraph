package net.maizegenetics.pangenome.gui;

import com.google.common.collect.Multimap;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.hapCalling.ImportHaplotypePathFilePlugin;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 *
 * This plugin uses Graph Stream to visually display a PHG graph.  It has controls
 * to navigate through the Reference Ranges and show labels on the nodes.
 * 
 * @author Terry Casstevens
 *
 * Created March 17, 2018
 */
public class ViewGraphPlugin extends AbstractPlugin {

    private PluginParameter<String> myPathsDir = new PluginParameter.Builder<>("pathsDir", null, String.class)
            .description("Paths directory")
            .inDir()
            .required(false)
            .build();

    private PluginParameter<String> myPathMethodName = new PluginParameter.Builder<>("pathMethodName", null, String.class)
            .description("Path Method Name Stored in the DB")
            .required(false)
            .build();

    public ViewGraphPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    protected void preProcessParameters(DataSet input) {
        List<Datum> temp = input.getDataOfType(HaplotypeGraph.class);
        if (temp.size() != 1) {
            throw new IllegalArgumentException("ViewGraphPlugin: preProcessParameters: must select one HaplotypeGraph: " + temp.size());
        }
    }

    @Override
    public DataSet processData(DataSet input) {

        List<Datum> temp = input.getDataOfType(HaplotypeGraph.class);
        if (temp.size() != 1) {
            throw new IllegalArgumentException("ViewGraphPlugin: processData: must input one HaplotypeGraph: " + temp.size());
        }

        HaplotypeGraph haplotypeGraph = (HaplotypeGraph) temp.get(0).getData();

        if (pathMethodName() != null) {
            ImportHaplotypePathFilePlugin importPlugin = new ImportHaplotypePathFilePlugin(getParentFrame(), isInteractive())
                    .pathMethodName(pathMethodName());
            Multimap<String, HaplotypeNode> paths = (Multimap<String, HaplotypeNode>) importPlugin.performFunction(input).getDataOfType(Multimap.class).get(0).getData();
            CreateStreamGraph.view(haplotypeGraph, paths);
        } else if (pathsDir() != null) {
            ImportHaplotypePathFilePlugin importPlugin = new ImportHaplotypePathFilePlugin(getParentFrame(), isInteractive())
                    .inputFileDirectory(pathsDir());
            Multimap<String, HaplotypeNode> paths = (Multimap<String, HaplotypeNode>) importPlugin.performFunction(input).getDataOfType(Multimap.class).get(0).getData();
            CreateStreamGraph.view(haplotypeGraph, paths);
        } else {
            CreateStreamGraph.view(haplotypeGraph);
        }

        return null;

    }

    /**
     * Paths directory
     *
     * @return Paths Directory
     */
    public String pathsDir() {
        return myPathsDir.value();
    }

    /**
     * Set Paths Directory.
     *
     * @param value Paths directory
     *
     * @return this plugin
     */
    public ViewGraphPlugin pathsDir(String value) {
        myPathsDir = new PluginParameter<>(myPathsDir, value);
        return this;
    }

    /**
     * Path Method Name Stored in the DB
     *
     * @return Path Method Name
     */
    public String pathMethodName() {
        return myPathMethodName.value();
    }

    /**
     * Set Path Method Name. Path Method Name Stored in the DB
     *
     * @param value Path Method Name
     *
     * @return this plugin
     */
    public ViewGraphPlugin pathMethodName(String value) {
        myPathMethodName = new PluginParameter<>(myPathMethodName, value);
        return this;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "View Graph";
    }

    @Override
    public String getToolTipText() {
        return "View Graph";
    }
}
