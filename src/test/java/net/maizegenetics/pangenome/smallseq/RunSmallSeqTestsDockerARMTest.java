package net.maizegenetics.pangenome.smallseq;

import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableUtils;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence;
import net.maizegenetics.util.LoggingUtils;

import java.io.*;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.atomic.LongAdder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.maizegenetics.dna.snp.NucleotideAlignmentConstants.getNucleotideIUPAC;
import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.fail;

/**
 * Authors: Ed Buckler, Lynn Johnson, Zack Miller, Terry Casstevens, Ana Berthel
 * <p>
 * This version of the method is modified to work on ARM architectures (e.g. Apple Silicon).
 * For x86_64 architectures see RunSmallSeqTestsDockerTest.java
 * The base image is built using ARMDockerfile instead of Dockerfile, and the default platform is linux/arm64.
 * A separate docker image is currently required to run anchorwave, and is created using AnchorwaveDockerfile.
 * When anchorwave fails to run in the base image, the user can manually execute the same command in the
 * anchorwave image to produce the desired output files. More details are provided on line 98 of this file.
 * <p>
 * PreRequisites to run this method:
 * <p>
 * Initial testing without changes:
 * 1. Create directory <user_home>/temp/smallseq_docker
 * 2. From the master PHG repository branch, create a phg.jar file
 * 3. Add file phg.jar to <user_home>/temp/smallseq_docker
 * 4. Create file named Dockerfile and add it to <user_home>/temp/smallseq_docker
 * <p>
 * This Dockerfile should contain the following default lines:
 * <p>
 * #Use the repository docker created in:
 * FROM phgrepository_base:latest
 * ADD ./phg.jar /tassel-5-standalone/lib/
 * WORKDIR /
 * <p>
 * 4.a Depending on how your IDE is setup, you may have to copy the files from docker/ into your working directory.
 * To Figure out what your work directory is for the IDE, make a test class and print out the following: System.getProperty("user.dir");
 * Basically you need to have all the scripts in a scripts/ directory as well as the Dockerfile from the repository
 * <p>
 * 5.  You now are setup to run tests based on Master without any changes.
 * <p>
 * Setup to test new code/scripts:
 * 6.  Replace the phg.jar in <user_home>/temp/smallseq_docker with a phg.jar
 * containing any changes/additions to code that you want to test.
 * 7.  Add additional files or scripts to <user_home>/temp/smallseq_docker needed for your tests.
 * 8.  Add additional lines to the smallseq_docker/Docker file to load additional scripts
 * files, or install applications you need for testing.  This is the docker to which
 * containers are attached when running this test pipeline.
 * <p>
 * Method Details:
 * This method checks for existence of a base docker image called phgrepository_base.
 * If it doesn't exist, it creates one using information from data in the
 * docker/buildFiles PHG repository folder.  It then creates a docker image called
 * phgrepository_test using the Dockerfile and other artifacts from the
 * <user_home>/temp/smallseq_docker folder on the executing machine
 * <p>
 * Changing Parameters:
 * The CreateTestGenomes variables are now plugin parameters to CreateSmallGenomePlugin.
 * This plugin has NO required parameters - everything defaults.  To change the defaults,
 * invoke CeateSmallGenomePlugin with the parameter you wish to change.
 * <p>
 * NOTES:
 * If it exists,  the phgrepository_test docker image is deleted at the beginning of the tests.
 * The phgrepository_base image created from master is NOT deleted.  If master has changed, you
 * may want to delete the old phgrepository_base image and allow this method to recreate it for
 * you with the latest code.
 * <p>
 * ERRORS:
 * If this fails when attempting to run the docker command from inside your IDE, try replacing
 * "docker" with the full path, e.g. "/usr/bin/local/bin/docker".
 * <p>
 * Output:  Builds a Haplotype Graph into a SQLite database using the dockerized steps.
 * <p>
 * Steps: All but the first two steps are dockerized
 * Create a set of test genomes and read files,
 * Create a set of anchor intervals,
 * Upload the reference genome to the DB
 * Upload the assembly sequences to the DB
 * Align the reads to the reference,
 * Run GATK HaplotypeCaller
 * Filter the GVCFs and upload to DB
 * Run Consensus finder and upload to DB
 * Run HMM to get SNPs
 * Output the SNPs to VCF
 *
 * IF ANCHORWAVE DOES NOT WORK IN MAIN DOCKER IMAGE, FOLLOW THESE STEPS
 *         1. Run RunSmallSeqTestsDockerARM
 *         2. Search for "Error: could not execute anchorwave command. Run anchorwave manually and retry."
 *              in populateDB_output.log. The line above should contain a command beginning with "anchorwave gff2seq"
 *         3. Run the anchorwave command in an anchorwave_test docker container. Use the flag --platform linux/amd64 and
 *              mount the same volumes as for phgrepository_base
 *         4. comment out resetSmallSeqFiles(); and makeDB(); on lines 405-406. We do not want to reset the files during this
 *              process, and the makeDB() step should have completed successfully and does not need to be performed again.
 *         5. Run RunSmallSeqTestsDockerARM again. Because we provided the first anchorwave output file and didn't reset the files,
 *              it should get further through the populateDB() step this time.
 *         6. Search for "Error: could not execute anchorwave command. Run anchorwave manually and retry."
 *              in populateDB_output.log again. The first occurrence should be the same as in step 2, and can be ignored.
 *              There should now also be more occurrences later in the file. The lines above those should contain commands
 *              beginning with "anchorwave proali"
 *         7. Run the anchorwave proali commands in an anchorwave_test docker container. Use the flag --platform linux/amd64
 *              and mount the same volumes as for phgrepository_base. The anchorwave gff2seq command does not need to be re-run here.
 *         8. Run RunSmallSeqTestsDockerARM again. You should get the desired output now.
 */
@Deprecated
public class RunSmallSeqTestsDockerARMTest {
    public static void main(String[] args) {

        LoggingUtils.setupDebugLogging();


        // Check if phgrepository_base and phgrepository_test images exist.
        Boolean[] imageArray = checkImagesExist();
        System.out.println("BaseExists: " + imageArray[0] + " testExists " + imageArray[1]);

        // Base repository is created if it doesn't exist.  Test repository image
        // is deleted if it does exist.
        try {

            if (imageArray[1]) {
                // remove old docker phgrepository_test image
                removeRepositoryTestImage();
            }

            if (!imageArray[0]) {
                // no phgrepository_base - create it
                // Setup variables for phgrepository_base, create base image
                createBaseImage();
            }

            if (!imageArray[3]) {
                // no anchorwave image - create it
                createAnchorwaveImage();
            }
            //System.exit(0);
            // Create test docker image based on phgrepository_base image
            createTestImage();
            // Check for postgres image
            if (imageArray[2]) {
                // Sometimes the container exists, if not, recreate it
                System.out.println("\nPostgres image exists, check for the container");
                // This actually delete a container if it exists. That is because sometimes
                // a container exists, but has status of "exited".  It is easiest to
                // drop any existing container and start a fresh one.
                boolean containerExists = checkPostgresContainer();
                if (!containerExists) {
                    createPostgresContainer();
                }
            }

            if (!imageArray[2]) {
                // We don't need to clean out the postgres data directory as
                // making the db for this test case runs LoadAllIntervalsToPHGPlugin,
                // which drops the db (if it exists), then re-creates it.  The instance remains up,
                // only the db drops.
                createPostgresImage();

                // create the container
                createPostgresContainer();
            }

        } catch (Exception exc) {
            throw new IllegalStateException("Error deleting/creating docker images " + exc.getMessage());
        }

        // To run faster, comment out one of the db sections below.
        // Code below runs test for both sqlite and postgres

        // Create/test sqlite db
        System.out.println("\nRUNNING TESTS FOR SQLITE DB\n");
        runSmallSeq(dbDockerConfigFileDockerPath, true);

//        System.out.println("\nRUNNING TESTS FOR POSTGRES DB\n");
//        runSmallSeq(dbDockerPostgresConfigFileDockerPath, false);

    }

    // Checks for presense of the docker images needed by this test.  It
    // returns a 3-element boolean array, where the elements in order represent
    // the presense of the base, test and postgres images (elements 0,1,2)
    public static Boolean[] checkImagesExist() {

        Boolean[] imageArray = new Boolean[4];
        Arrays.fill(imageArray, Boolean.FALSE);
        try {
            String cmd = "docker images";
            Process start = Runtime.getRuntime().exec(cmd);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(start.getInputStream()));
            String line = null;

            while ((line = br.readLine()) != null) {
                if (line.contains("CREATED")) continue; // skip header line
                String imageName = line.substring(0, line.indexOf(" ")); // up to first white space
                if (imageName.equals("phgrepository_base")) {
                    imageArray[0] = true;
                }
                if (imageName.equals("phgrepository_test")) {
                    imageArray[1] = true;
                }
                if (imageName.equals("phgpostgres_docker")) {
                    imageArray[2] = true;
                    //  container presence is checked below
                }
                if (imageName.equals("anchorwave_test")) {
                    imageArray[3] = true;
                }
            }
            return imageArray;
        } catch (Exception exc) {
            throw new IllegalStateException("RunSmallSeqTests:main - error reading docker images: " + exc.getMessage());
        }

    }

    public static void removeRepositoryTestImage() {
        try {
            // remove old docker phgrepository_test image
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "rmi", "phgrepository_test");

            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            int error = process.waitFor();
            if (error != 0) {
                // assume repository doesn't exist - this isn't real error
                System.out.println("Error removing phgrepository_test, assuming it doesn't exist, Error: " + error);
            }
        } catch (Exception exc) {
            throw new IllegalStateException("Error deleting phgrepository test docker images " + exc.getMessage());
        }
    }

    public static void createBaseImage() {
        try {
            // Setup variables for phgrepository_base, create base image
            String workingDir = System.getProperty("user.dir");
            System.out.println("WorkingDir: " + workingDir);
            String dockerDir = workingDir + "/docker/buildFiles";

            System.out.println("Creating repository docker ");
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "buildx", "build", "-f", "ARMDockerFile", "-t", "phgrepository_base", dockerDir);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        } catch (Exception exc) {
            throw new IllegalStateException("Error creating phgrepository base docker images " + exc.getMessage());
        }
    }

    public static void createAnchorwaveImage() {
        System.out.println("Creating a docker for anchorwave");
        try {
            // Setup variables for phgrepository_base, create base image
            String workingDir = System.getProperty("user.dir");
            System.out.println("WorkingDir: " + workingDir);
            String dockerDir = workingDir + "/docker/buildFiles";

            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "buildx", "build", "--platform", "linux/amd64", "-f", dockerDir + "/AnchorwaveDockerfile", "-t", "anchorwave_test", dockerDir);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();

        } catch (Exception exc) {
            System.out.println("Creating anchorwave docker failed");
            throw new IllegalStateException("Creating anchorwave docker image from phg repository failed ");
        }
    }

    public static void createTestImage() {
        String testDockerDir = System.getProperty("user.home") + "/temp/smallseq_docker";
        System.out.println("Creating second docker on top of phgrepository_base image");
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "buildx", "build",  "--no-cache=true", "-t", "phgrepository_test", testDockerDir);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();

        } catch (Exception exc) {
            System.out.println("Creating second docker failed");
            throw new IllegalStateException("Creating test docker image from phg repository failed ");
        }
    }

    public static void createPostgresImage() {
        try {
            System.out.println("\npostgres docker image not present, creating the image ...");
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "buildx", "build", "--no-cache=true", "-t", postgresImageName, postgresDockerDir);
//                    "docker", "buildx", "build", "--no-cache=true", "--platform", "linux/amd64", "-t", postgresImageName, postgresDockerDir);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        } catch (Exception exc) {
            throw new IllegalStateException("Creating postgres docker image failed ");
        }
    }
    public static boolean checkPostgresContainer() {
        // image is present, check for container

        try {
            String cmd = "docker ps -a";
            Process start = Runtime.getRuntime().exec(cmd);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(start.getInputStream()));
            String line = null;

            while ((line = br.readLine()) != null) {
                if (line.contains("CREATED")) continue; // skip header line
                // get to first white space
                String temp = line.substring(line.indexOf(" "));
                // get to first non-white space
                Pattern p = Pattern.compile("([^\\s]+)?(\\s)+");
                final Matcher matcher = p.matcher(temp);
                matcher.find();
                // This gets us the string starting with the image name
                temp = temp.substring(matcher.end());
                // Now get just up to the first white space
                String imageName = temp.substring(0, temp.indexOf(" "));

                System.out.println("checkPostgresContainer: test imageName is:" + imageName);
                System.out.println("checkPostgresContainer: postgresImageName is:" + postgresImageName);
                // Expecting only 1 container against the postgres docker image
                // Note this checks the image name, not the container name
                // If a container exists, we drop and re-create.  This handles a
                // case where the container exists but is in exited state.  It is
                // easier and quicker to just remake the container for smallseq.
                if (imageName.equals(postgresImageName)) {
                    System.out.println("checkPostgresContainer: image names match, stopping/dropping old container!!");
                    String containerID = line.substring(0,line.indexOf(" "));
                    ProcessBuilder builder = new ProcessBuilder(
                            "docker", "stop", containerID);
                    builder.inheritIO();
                    System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
                    Process process = builder.start();
                    process.waitFor();

                    // Run second command to remove it
                    System.out.println("Removing postgres container");
                     builder = new ProcessBuilder(
                            "docker", "rm", containerID);
                    builder.inheritIO();
                    System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
                    process = builder.start();
                    process.waitFor();

                    return false; // because it will now always be false
                }
            }
            System.out.println("checkPostgresContainer:  returning FALSE!!");
            return false;
        } catch (Exception exc) {
            throw new IllegalStateException("RunSmallSeqTests:main - error reading docker images: " + exc.getMessage());
        }
    }

    public static void createPostgresContainer() {
        System.out.println("Creating the postgres docker container ...");
        String mountline = postgresDataDir + ":/var/lib/postgresql/data/";
        String portmount = postgresPort + ":5432";

        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--detach", "--name",postgresContainer,"-e", "POSTGRES_PASSWORD=postgres","-v",mountline,"-p",portmount,postgresImageName);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();

            // Now start it: docker start $DOCKER_CONTAINER_NAME
            builder = new ProcessBuilder("docker","start",postgresContainer);
            builder.inheritIO();
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            process = builder.start();
            process.waitFor();
        } catch (Exception exc) {
            throw new IllegalStateException("ERROR - failed to create postgres container: " + exc.getMessage());
        }
    }


    public static void resetSmallSeqFiles() {
        // The plugin is called with defaults
        // Change any parameter value by adding it to the plugin invocation below.
        // For assembly data to be included, you need to include a refInterGeneDelete value.  0.2 works with
        // this data set
        new CreateSmallGenomesPlugin()
                .geneLength(3000)
                .interGeneLength(3500)
                .numberOfGenes(10)
//                .refInterGeneDelete(0.2)
                //.refGeneInsert(0.2)
                .wgsDepth(10.0)
                .gbsDepth(0.05)
                .performFunction(null);
    }

    public static void runSmallSeq(String configFile, boolean isSQLite) {
        //resetSmallSeqFiles();
        //makeDB(configFile);
        populateDB(configFile);
        imputeFromFastq(isSQLite);
        try {
            compareResultAndAnswer(false, answerDir + "SmallSeq.vcf",outputDir+"output.vcf");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID");
        }
    }

    /**
     * Function to compare the phg result Genotype Table with the original file.  This is not a true unit test as it does not make any assertions.
     *
     * If the phg does work, the power should be high and the error rate should be low.
     * @param verboseErrors
     * @param originalFile
     * @param resultFile
     */
    public static void compareResultAndAnswer(boolean verboseErrors, String originalFile, String resultFile) {
        GenotypeTable originalGT = ImportUtils.read(originalFile);
        GenotypeTable resultGT=ImportUtils.read(resultFile); //To Compare to original non dockerized output uncomment this line
        System.out.println(originalGT.numberOfSites());
        System.out.println(resultGT.numberOfSites());

        System.out.printf("%s\t%s\t%s\t%s\t%s\t%s\n", "Taxon", "#Sites", "Power", "ErrorRate", "#Errors", "#Hets");
        resultGT.taxa().stream().sorted().forEach(resultTaxon -> {
            String origTaxon = resultTaxon.getName().replace(".gbs", "").replace("_gbs","").replace("_wgs","").replace("_multimap.txt", "").replace("_R1","");
            int origTaxonIndex = originalGT.taxa().indexOf(origTaxon);
            int resultTaxonIndex = resultGT.taxa().indexOf(resultTaxon);

            LongAdder unknownCnt = new LongAdder();
            LongAdder matchCnt = new LongAdder();
            LongAdder hetCnt = new LongAdder();  //partially correct
            LongAdder errorCnt = new LongAdder();
            IntStream.range(0, resultGT.numberOfSites())
                    .forEach(resultSite -> {
                        byte resultCall = resultGT.genotype(resultTaxonIndex, resultSite);
                        int origSite = originalGT.positions().indexOf(resultGT.positions().get(resultSite));
                        byte origCall = originalGT.genotype(origTaxonIndex, origSite);
                        if (verboseErrors && !GenotypeTableUtils.isEqual(resultCall, origCall)) {
                            System.out.printf("%d\t%s\t%s%n", resultSite, getNucleotideIUPAC(origCall), getNucleotideIUPAC(resultCall));
                        }
                        if (resultCall == GenotypeTable.UNKNOWN_DIPLOID_ALLELE) {
                            unknownCnt.increment();
                        } else if (!GenotypeTableUtils.isPartiallyEqual(resultCall, origCall)) {
                            errorCnt.increment();
                        } else if (GenotypeTableUtils.isEqual(resultCall, origCall)) {
                            matchCnt.increment();
                        } else {
                            hetCnt.increment();
                        }
                    });
            double totalSites = unknownCnt.intValue() + matchCnt.intValue() + hetCnt.intValue() + errorCnt.intValue();
            double power = (totalSites - unknownCnt.intValue()) / totalSites;
            double errorRate = (errorCnt.doubleValue() + 0.5 * hetCnt.intValue()) / totalSites;
            System.out.printf("%s\t%g\t%g\t%g\t%g\t%g%n", resultTaxon.getName(), totalSites, power, errorRate, errorCnt.doubleValue(), hetCnt.doubleValue());
        });
    }

    /**
     * Currently this test fails, as the first and last 50bp are not called correctly.
     */
    @Test
    @Disabled
    public void compareGVCFfastaWithOriginal() {
        taxaList.forEach(taxon -> {
            String taxonName = taxon.getName();
            System.out.println("Comparing GVCF sequence for:" + taxon.getName());
            GenomeSequence answerSeq = GenomeSequenceBuilder.instance(answerDir + taxonName + ".fa");
            GenomeSequence gvcfSeq = GVCFSequence.instance(refGenomePath, alignDir + taxonName + ".g.vcf");
            String answerSeqStr = answerSeq.genomeSequenceAsString(0, (int) answerSeq.genomeSize());
            String gvcfSeqStr = gvcfSeq.genomeSequenceAsString(0, (int) gvcfSeq.genomeSize());
            System.out.println(answerSeqStr);
            System.out.println(gvcfSeqStr);
            int errors = (int) IntStream.range(0, answerSeqStr.length())
                    .filter(site -> site >= gvcfSeqStr.length() || answerSeqStr.charAt(site) != gvcfSeqStr.charAt(site))
                    .count();
            System.out.println(errors);
            //Assert.assertEquals("Full Sequence does not match expected", answerSeqStr, gvcfSeqStr);
        });

    }

    /**
     * Indexes the reference files all the ways needed by BWA, SamTools, and GATK
     */
    private static void createRefGenomeIndex(String refGenomePath) {
        try {
            ProcessBuilder builder = new ProcessBuilder(bwaPath, "index", refGenomePath);
            Process process = builder.start();
            process.waitFor();
            builder = new ProcessBuilder(samToolsPath, "faidx", refGenomePath);
            process = builder.start();
            process.waitFor();
            builder = new ProcessBuilder("java", "-jar",
                    picardPath, "CreateSequenceDictionary",
                    "REFERENCE=" + refGenomePath,
                    "OUTPUT=" + refGenomePath.replace(fastaSuffix, ".dict"));
            System.out.println("Command:" + builder.command().toString());
            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            System.out.println(runCommand);
            process = builder.start();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makeDB(String configFile) {
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--name", "small_seq_test_container", "--rm",
                    "-v", baseDir + ":/phg/",
                    "-v", baseGvcfDir + ":/remoteGvcfs/", // stored as baseGVCFDirDocker in SmallSeqPaths
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G" ,"-debug", "-configParameters", configFile,
                    "-MakeInitialPHGDBPipelinePlugin", "-endPlugin");
            String redirectOutput = baseDir + "loadGenomeIntervals_output.log";
            String redirectError = baseDir + "loadGenomeIntervals_error.log";
            builder.redirectOutput(new File(redirectOutput));
            builder.redirectError(new File(redirectError));
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void populateDB(String configFile) {
        try {
            // If you want to test with mummer4, change the asmAliger parameter below
            // to "mummer4"
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--name", "small_seq_test_container", "--rm",
                    "-v", baseDir + ":/phg/",
                    "-v", baseGvcfDir + ":/remoteGvcfs/", // stored as baseGVCFDirDocker in SmallSeqPaths
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G" ,"-debug", "-configParameters", configFile,
                    "-PopulatePHGDBPipelinePlugin", "-endPlugin");
            String redirectOutput = baseDir + "populateDB_output.log";
            String redirectError = baseDir + "populateDB_error.log";
            builder.redirectOutput(new File(redirectOutput));
            builder.redirectError(new File(redirectError));
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error populating the DB:",e);
        }
    }


    // This was used by LoadHaplotypesFromGVCFPluginTest:testASMVersion()
    // It tested loading ASM from GVCF before the changes were made to PopulatePHGDBPipelinePlugin
    public static void createMAFfromAnchorwave(String configFile) {
        System.out.println("Begin - createMAFFromAnchorWave with configFile " + configFile);
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--name", "small_seq_test_container", "--rm", //"--platform", "linux/amd64",
                    "-v", baseDir + ":/phg/",
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G" ,"-debug", "-configParameters", configFile,
                    "-AssemblyMAFFromAnchorWavePlugin", "-endPlugin");
            String redirectOutput = baseDir + "asemblyMAFanchorwave_output.log";
            String redirectError = baseDir + "assemblyMAFanchorwave_error.log";
            builder.redirectOutput(new File(redirectOutput));
            builder.redirectError(new File(redirectError));
            System.out.println("\nCommand:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        }
        catch(Exception exc) {
            exc.printStackTrace();
            throw new IllegalStateException("Error creating MAF from Anchorwave:",exc);
        }
    }

    // This was used by LoadHaplotypesFromGVCFPluginTest:testASMVersion()
    // It tested loaded before changes were made to PopulatePHGDBPipelinePlugin
    public static void createGVCFfromMAF(String configFile) {
        System.out.println("Begin - createGVCFfromMAF with configFile " + configFile);
        // Just "alignDir", not docker, as this is outside the docker
        File dir = new File(alignDir);
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".maf");
            }
        });

        for (File file : files) {

            String mafFileName = file.getName().toString();
            //String sampleName = mafFileName.substring(0,mafFileName.lastIndexOf("_"));
            String fileNameOnly = mafFileName.substring(0,mafFileName.lastIndexOf("."));
            String mafFilePath = alignDirDocker + "/" + mafFileName;
            // This one is alignDirDocker as the command below is run from within the docker
            String gvcfFile = gvcfDirDocker + "/" + fileNameOnly + ".gvcf";
            try {
                ProcessBuilder builder = new ProcessBuilder(
                        "docker", "run", "--name", "small_seq_test_container", "--rm", //"--platform", "linux/amd64",
                        "-v", baseDir + ":/phg/",
                        "-t", dockerImageName,
                        "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G" ,"-debug", "-configParameters", configFile,
                        "-MAFToGVCFPlugin", "-mafFile", mafFilePath, "-sampleName", fileNameOnly, "-referenceFasta", refGenomePathDocker, "-gvcfOutput",gvcfFile, "-endPlugin");
                String redirectOutput = baseDir + "MAFtoGVCF_output.log";
                String redirectError = baseDir + "MAFtoGVCF_error.log";
                builder.redirectOutput(new File(redirectOutput));
                builder.redirectError(new File(redirectError));
                System.out.println("\nCommand:" + builder.command().stream().collect(Collectors.joining(" ")));
                Process process = builder.start();
                process.waitFor();
            }
            catch(Exception exc) {
                exc.printStackTrace();
                throw new IllegalStateException("Error creating GVCF from MAF:",exc);
            }
        }
    }

    private static void imputeFromFastq(boolean isSqlite) {

        String configName = baseDir + "imputeConfig.txt";

        // create SQLITE db data string
        String sqliteDBData = "host=localHost\nuser=sqlite\npassword=sqlite\nDB=/phg/phgSmallSeq.db\nDBtype=sqlite\n\n";

        // Get the host id address for postgres
        String ipAddr = CreateTestGenomes.getHostIpAddr();

        // Create postgres db data string
        StringBuilder postgresDBData = new StringBuilder();
        postgresDBData.append("host=").append(ipAddr).append(":5433\n"); // postgres docker container has port 5433 mapped to 5432
        postgresDBData.append("user=").append(postgresUser).append("\n");
        postgresDBData.append("password=").append(postgresUserPwd).append("\n");
        postgresDBData.append("DB=").append(phgDBNamePostgres).append("\n");
        postgresDBData.append("DBtype=postgres\n");

        //first write the config file
        try (PrintWriter pw = new PrintWriter(configName)) {
            pw.println("#config file for imputation, small seq test");
            pw.println("#!!! Required Parameters !!!\n");
            if (isSqlite) {
                pw.println(sqliteDBData);
            } else {
                pw.println(postgresDBData.toString());
            }
            pw.println("#--- Used by liquibase to check DB version ---\n" +
                    "liquibaseOutdir=/phg/outputDir\n\n" +
                    "#--- Used for writing and indexing a pangenome ---\n" +
                    "pangenomeHaplotypeMethod=CONSENSUS\n" +
                    "pangenomeDir=/phg/pangenome/\n" +
                    "indexKmerLength=21\n" +
                    "indexWindowSize=11\n" +
                    "indexNumberBases=90G\n" +
                    "\n" +
                    "#--- Used for all that need to upload/download gvcfs \n" +
                    "localGVCFFolder=" + localGvcfDirDocker + "\n" +
                    "isGvcfLocal=true\n" +
                    "#--- Used for mapping reads\n" +
                    "inputType=fastq\n" +
                    "readMethod=READS_FASTQ\n" +
                    "keyFile=" + genotypingKeyFileDocker + "\n" +
                    "fastqDir=" + genotypingFastqDirDocker + "\n" +
                    "debugDir=" + genotypingDebugDirDocker + "\n" +
                    "samDir=**UNASSIGNED**\n" +
                    "lowMemMode=true\n" +
                    "maxRefRangeErr=0.25\n" +
                    "outputSecondaryStats=false\n" +
                    "maxSecondary=20\n" +
                    "minimapLocation=minimap2\n" +
                    "\n" +
                    "#--- Used for path finding\n" +
                    "pathHaplotypeMethod=CONSENSUS\n" +
                    "pathMethod=PATH_METHOD\n" +
                    "maxNodes=1000\n" +
                    "maxReads=10000\n" +
                    "minReads=0\n" +
                    "minTaxa=2\n" +
                    "minTransitionProb=0.01\n" +
                    "numThreads=3\n" +
                    "probCorrect=0.99\n" +
                    "removeEqual=true\n" +
                    "splitNodes=true\n" +
                    "splitProb=0.99\n" +
                    "usebf=false\n" +
                    "#   used by haploid path finding only\n" +
                    "usebf=false\n" +
                    "minP=0.8\n" +
                    "#   used by diploid path finding only\n" +
                    "maxHap=11\n" +
                    "maxReadsKB=100\n" +
                    "algorithmType=classic\n" +
                    "outVcfFile=" + outputDirDocker + "output.vcf\n")
            ;

        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to write to " + configName, e);
        }

        //next call the impute pipeline plugin
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--name", "small_seq_test_container", "--rm",
                    "-v", baseDir + ":/phg/",
                    "-v", baseGvcfDir + ":/remoteGvcfs/", // stored as baseGVCFDirDocker in SmallSeqPaths
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G" ,"-debug", "-configParameters", "/phg/imputeConfig.txt",
                    "-ImputePipelinePlugin","-imputeTarget", "pathToVCF", "-endPlugin");
            String redirectOutput = baseDir + "imputeFromFastq_output.log";
            String redirectError = baseDir + "imputeFromFastq_error.log";
            builder.redirectOutput(new File(redirectOutput));
            builder.redirectError(new File(redirectError));
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error populating the DB:",e);
        }

    }

}
