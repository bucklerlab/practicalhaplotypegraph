/*
 * GenerateHaplotypeCallerScripts
 */
package net.maizegenetics.pangenome;

import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terry
 */
public class GenerateHaplotypeCallerScripts {

    /*
    
    #!/bin/bash

    time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
       --num_cpu_threads_per_data_thread 20 \
       -R maize.fa \
       -T HaplotypeCaller \
       -I bam_files/B73_HBEN2ADXX_GTTTCG.bam \
       -I bam_files/B73_HFFGKADXX_GTTTCG.bam \
       -I bam_files/B73_HL5WNCCXX_L5.clean_srt_dedup.bam \
       --emitRefConfidence GVCF \
       -L fullOverlapBalancedWindowWithTrimmingThreshold2.intervals \
       -o B73_haplotype_caller_threshold2_output.g.vcf

    time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
       -T GenotypeGVCFs \
       -R maize.fa \
       --variant B73_haplotype_caller_threshold2_output.g.vcf \
       -stand_call_conf 30 \
       -o B73_haplotype_caller_threshold2_output.vcf

    time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
       -T FastaAlternateReferenceMaker \
       -R maize.fa \
       -o B73_haplotype_caller_threshold2_output.fasta \
       -L fullOverlapBalancedWindowWithTrimmingThreshold2.intervals \
       -V B73_haplotype_caller_threshold2_output.vcf
    
     */
    public static void old(String[] args) {

        String results = "results";
        String referenceFile = "../../maize.fa";
        String intervalsFile = "../../overlapWindowsWithTrimmingThreshold2.intervals";

        File resultsDir = new File(results);
        if (resultsDir.exists()) {
            throw new IllegalStateException("results directory already exists.");
        }
        resultsDir.mkdir();

        Map<String, List<String>> bamFiles = new HashMap<>();

        try {

            File temp = new File("bam_files");
            for (File current : temp.listFiles()) {
                String name = current.getCanonicalPath();
                if (name.endsWith(".bam")) {
                    String taxon = Utils.getFilename(name).split("_")[0];
                    List<String> files = bamFiles.get(taxon);
                    if (files == null) {
                        files = new ArrayList<>();
                        bamFiles.put(taxon, files);
                        new File(results + "/" + taxon).mkdir();
                    }
                    files.add(name);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (BufferedWriter exec = Utils.getBufferedWriter("run_haplotype_caller.sh")) {

            exec.write("#!/bin/bash\n\n");

            for (String taxon : bamFiles.keySet()) {

                List<String> files = bamFiles.get(taxon);
                String scriptDir = results + "/" + taxon;
                String script = "run_haplotype_caller_" + taxon + ".sh";

                exec.write("cd /SSD/terry/haplotype_caller/" + scriptDir + "\n");
                exec.write("./" + script + " > " + taxon + ".log 2>&1\n");

                try (BufferedWriter writer = Utils.getBufferedWriter(scriptDir + "/" + script)) {

                    writer.write("#!/bin/bash\n\n");

                    writer.write("time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \\\n");
                    writer.write("   --num_cpu_threads_per_data_thread 20 \\\n");
                    writer.write("   -T HaplotypeCaller \\\n");
                    writer.write("   -R ");
                    writer.write(referenceFile);
                    writer.write(" \\\n");
                    for (String file : files) {
                        writer.write("   -I ");
                        writer.write(file);
                        writer.write(" \\\n");
                    }
                    writer.write("   --emitRefConfidence GVCF \\\n");
                    writer.write("   -L ");
                    writer.write(intervalsFile);
                    writer.write(" \\\n");
                    writer.write("   -o ");
                    writer.write(taxon);
                    writer.write("_haplotype_caller_output.g.vcf\n\n");

                    writer.write("time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \\\n");
                    writer.write("   -T GenotypeGVCFs \\\n");
                    writer.write("   -R ");
                    writer.write(referenceFile);
                    writer.write(" \\\n");
                    writer.write("   --variant ");
                    writer.write(taxon);
                    writer.write("_haplotype_caller_output.g.vcf \\\n");
                    writer.write("   -stand_call_conf 30 \\\n");
                    writer.write("   -o ");
                    writer.write(taxon);
                    writer.write("_haplotype_caller_output.vcf\n\n");

                    writer.write("time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \\\n");
                    writer.write("   -T FastaAlternateReferenceMaker \\\n");
                    writer.write("   -R ");
                    writer.write(referenceFile);
                    writer.write(" \\\n");
                    writer.write("   -L ");
                    writer.write(intervalsFile);
                    writer.write(" \\\n");
                    writer.write("   -V ");
                    writer.write(taxon);
                    writer.write("_haplotype_caller_output.vcf \\\n");
                    writer.write("   -o ");
                    writer.write(taxon);
                    writer.write("_haplotype_caller_output.fasta\n\n");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                new File(scriptDir + "/" + script).setExecutable(true);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        new File("run_haplotype_caller.sh").setExecutable(true);

    }

    /*

    #!/bin/bash
    license=cbsulogin2.tc.cornell.edu:8990
    export SENTIEON_LICENSE=$license
    RELEASE=/programs/sentieon-genomics-201611.02

    SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
    NUMBER_THREADS=20
    REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'

    time $SENTIEON driver \
            -t $NUMBER_THREADS \
            -r $REFERENCE \
            -i /workdir/terry/BAMFiles/W22/5509_6665_23535_HGCJCBGXX_W22_GCCAAT_sorted_dedup.bam \
            -i /workdir/terry/BAMFiles/W22/W22_HVMFTCCXX_L8_sorted_dedup.bam \
            -i /workdir/terry/BAMFiles/W22/W22HVMTCCXXL7_sorted_dedup.bam \
            --interval /workdir/terry/overlapWindowsWithTrimmingThreshold2.intervals \
            --algo Haplotyper \
            --ploidy 1 \
            --emit_mode gvcf \
            W22_haplotype_caller_output.g.vcf

    */

    public static void main(String[] args) {

        LoggingUtils.setupDebugLogging();

        String top = "/workdir/zrm22";
        String results = "results_nam";
        // we used this
        String referenceFile = top + "/agpv4/Zea_mays.AGPv4.dna.toplevel.fa";
        // Robert used this
        //String referenceFile = top + "/maize_AGPv4/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa";
        String intervalsFile = top + "/anchorsFile_all10_MergedPlus1000orGapDiffcoordinates_fixedOverlaps.intervals";
        String bamFilesDir = "/workdir/zrm22/BAMFiles";

        File resultsDir = new File(results);
        if (resultsDir.exists()) {
            throw new IllegalStateException("results directory already exists.");
        }
        resultsDir.mkdir();

        Map<String, List<String>> bamFiles = new HashMap<>();

        try {
            File directories = new File(bamFilesDir);
            for (File dir : directories.listFiles()) {
                if (dir.isDirectory()) {
                    String taxon = Utils.getFilename(dir.getCanonicalPath());
                    if (!taxon.equals("CIMMYTLines")) {
                        System.out.println("taxon: " + taxon);
                        for (File current : dir.listFiles()) {
                            String name = current.getCanonicalPath();
                            if (name.endsWith("dedup.bam")) {
                                //String taxon = Utils.getFilename(name).split("_")[0];
                                List<String> files = bamFiles.get(taxon);
                                if (files == null) {
                                    files = new ArrayList<>();
                                    bamFiles.put(taxon, files);
                                    new File(results + "/" + taxon).mkdir();
                                }
                                System.out.println("file: " + name);
                                files.add(name);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> parameters = new ArrayList<>();

        // defaults
        parameters.add("");

        //for (int m = 40; m < 51; m++) {
        //    for (int b = 20; b < 31; b++) {
        //        parameters.add("--min_map_qual " + m + " --min_base_qual " + b);
        //    }
        //}

        // --min_base_qual (default 10)
        //parameters.add("--min_base_qual 20");
        //parameters.add("--min_base_qual 30");
        //parameters.add("--min_base_qual 40");

        // --emit_conf (default 30)
        //parameters.add("--emit_conf 20");
        //parameters.add("--emit_conf 40");
        //parameters.add("--emit_conf 50");

        // --call_conf (default 30)
        //parameters.add("--call_conf 20");
        //parameters.add("--call_conf 40");
        //parameters.add("--call_conf 50");

        // --prune_factor (default 2)
        //parameters.add("--prune_factor 1");
        //parameters.add("--prune_factor 3");

        // --min_map_qual
        //parameters.add("--min_map_qual 10");
        //parameters.add("--min_map_qual 20");
        //parameters.add("--min_map_qual 30");
        //parameters.add("--min_map_qual 40");
        //parameters.add("--min_map_qual 50");
        //parameters.add("--min_map_qual 60");
        //parameters.add("--min_map_qual 70");
        //parameters.add("--min_map_qual 80");
        //parameters.add("--min_map_qual 90");

        // parameters.add("--contamination_fraction_to_filter 0.1");
        // parameters.add("--minReadsPerAlignmentStart 3");
        // parameters.add("--minReadsPerAlignmentStart 5");
        // parameters.add("--minReadsPerAlignmentStart 7");
        // parameters.add("--standard_min_confidence_threshold_for_calling 20");
        // parameters.add("--standard_min_confidence_threshold_for_calling 30");
        // parameters.add("--useNewAFCalculator TRUE");
        // parameters.add("--allowNonUniqueKmersInRef TRUE");

        try (BufferedWriter exec = Utils.getBufferedWriter("run_haplotype_caller.sh")) {

            exec.write("#!/bin/bash\n\n");


            for (String taxon : bamFiles.keySet()) {

                for (String parameter : parameters) {

                    String key;
                    if (parameter.isEmpty()) {
                        key = taxon;
                    } else {
                        key = taxon + "_" + parameter.replaceAll("\\s", "_").replaceAll("-", "");
                    }

                    List<String> files = bamFiles.get(taxon);
                    String scriptDir = results + "/" + taxon;
                    String script = "run_haplotype_caller_" + key + ".sh";

                    exec.write("cd " + top + "/" + scriptDir + "\n");
                    exec.write("./" + script + " > " + key + ".log 2>&1\n");

                    try (BufferedWriter writer = Utils.getBufferedWriter(scriptDir + "/" + script)) {

                        writer.write("#!/bin/bash\n\n");

                        writer.write("license=cbsulogin2.tc.cornell.edu:8990\n");
                        writer.write("export SENTIEON_LICENSE=$license\n");
                        writer.write("RELEASE=/programs/sentieon-genomics-201611.02\n");

                        writer.write("SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'\n");
                        writer.write("NUMBER_THREADS=20\n");
                        writer.write("REFERENCE='");
                        writer.write(referenceFile);
                        writer.write("'\n");

                        writer.write("time $SENTIEON driver \\\n");
                        writer.write("   -t $NUMBER_THREADS \\\n");
                        writer.write("   -r $REFERENCE \\\n");
                        for (String file : files) {
                            writer.write("   -i ");
                            writer.write(file);
                            writer.write(" \\\n");
                        }
                        writer.write("   --interval ");
                        writer.write(intervalsFile);
                        writer.write(" \\\n");
                        writer.write("   --algo Haplotyper \\\n");
                        writer.write("   --ploidy 1 \\\n");
                        writer.write("   --emit_mode gvcf \\\n");
                        writer.write("   --min_map_qual 48 \\\n");
                        writer.write("   --min_base_qual 21 \\\n");
                        //writer.write("   ");
                        //writer.write(parameter);
                        //writer.write(" \\\n");
                        writer.write("   ");
                        writer.write(key);
                        writer.write("_haplotype_caller_output.g.vcf\n\n");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    new File(scriptDir + "/" + script).setExecutable(true);

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        new File("run_haplotype_caller.sh").setExecutable(true);

    }
}
