/**
 * 
 */
package net.maizegenetics.pangenome.pipelineTests;

import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.Utils;

/**
 * Code to provide metrics on PHG haplotypes based on user supplied method.
 * 
 * Input: 
 *      1.  config file indicating db that contains the  data
 *      2.  name of method to be used when pulling the graph
 *      3.  name of output file to be written containing consensus metrics
 *      
 * Output:  tab-delimited file with columns:
 *   refRangeID  chrom  hapid numberOfTaxa taxalist
 *   
 * There may be multiple hapids per refRangeID when a consensus method is specified.
 *
 * @author lcj34
 *
 */
public class HaplotypesDataMetricsPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(HaplotypesDataMetricsPlugin.class);

    private PluginParameter<String> configFile = new PluginParameter.Builder<>("configFile", null, String.class).required(true)
            .inFile()
            .description(" Config file that specifies database connection parameters")
            .build();

    private PluginParameter<String> outputFile = new PluginParameter.Builder<>("outputFile", null, String.class).required(true)
            .outFile()
            .description("FUll path to output file created by this plugin .")
            .build();

    private PluginParameter<String> methods = new PluginParameter.Builder<>("methods", null, String.class)
            .required(true)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by semicolon. The range group is optional \n" +
                    "Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>")
            .build();
    
    public HaplotypesDataMetricsPlugin() {
        super(null, false);
    }

    public HaplotypesDataMetricsPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }
    public HaplotypesDataMetricsPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        //Load in the Graph for the specified method
        HaplotypeGraphBuilderPlugin graphBuilderPlugin = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile())
                .methods(methods())
                .includeVariantContexts(false);

        HaplotypeGraph graph = graphBuilderPlugin.build();

        myLogger.info("Nodes: "+graph.numberOfNodes());
        myLogger.info("Ranges: "+graph.numberOfRanges());
       
        StringBuilder outSB = new StringBuilder();
        // Create header line
        outSB.append("refRangeID\tchrom\tstartPosition\tendPosition\thapid\tnumberOfTaxa\ttaxaList\n" );
        // Process data on per-refRange basis
        List<ReferenceRange> allRefRanges= graph.referenceRangeList();
        
        for (ReferenceRange refRange : allRefRanges) {
            String chrom = refRange.chromosome().getName();           
            List<HaplotypeNode> hapNodeList = graph.nodes(refRange);
            
            StringBuilder baseSB = new StringBuilder();
            baseSB.append(refRange.id()).append("\t").append(chrom).append("\t");
            baseSB.append(refRange.start()).append("\t").append(refRange.end()).append("\t");

            for (HaplotypeNode hapNode : hapNodeList) {
                int hapID = hapNode.id();
                int numTaxa = hapNode.numTaxa();
                List<Taxon> taxaList = hapNode.taxaList();
                StringBuilder taxaSB = new StringBuilder();
                taxaSB.append(taxaList.get(0).getName());
                
                // LCJ _ Uncomment these lines when CraeteGraphUtils.TAXON_HAP_NUMBER_KEY has been merged to master
                
//                String[] hapNums = taxaList.get(0).getAnnotation().getTextAnnotation(CreateGraphUtils.TAXON_HAP_NUMBER_KEY);
//                if (hapNums.length > 0) {
//                    taxaSB.append("-").append(hapNums[0]);
//                }
                for (int idx = 1; idx < taxaList.size(); idx++) {
                    taxaSB.append(",");
                    taxaSB.append(taxaList.get(idx).getName());
//                    hapNums = taxaList.get(idx).getAnnotation().getTextAnnotation(CreateGraphUtils.TAXON_HAP_NUMBER_KEY);
//                    if (hapNums.length > 0) {
//                        taxaSB.append("-").append(hapNums[0]);
//                    }                    
                }
                
                outSB.append(baseSB.toString()).append(hapID).append("\t");
                outSB.append(numTaxa).append("\t");
                outSB.append(taxaSB.toString()).append("\n");
            }
        }
        
        myLogger.info("Printing data to file " + outputFile());
        try (BufferedWriter bw = Utils.getBufferedWriter(outputFile())) {
            bw.write(outSB.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        myLogger.info("FInished !!");
        
        return null;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() { 
        return ("HaplotypesDataMetrics");
    }

    @Override
    public String getToolTipText() {
        return ("Write tab-delimited file containing data on consensus intervals from the specified PHG database");
    }
    
    /**
     *  Config file that specifies database connection parameters
     *
     * @return Config File
     */
    public String configFile() {
        return configFile.value();
    }

    /**
     * Set Config File.  Config file that specifies database
     * connection parameters
     *
     * @param value Config File
     *
     * @return this plugin
     */
    public HaplotypesDataMetricsPlugin configFile(String value) {
        configFile = new PluginParameter<>(configFile, value);
        return this;
    }

    /**
     * FUll path to output file created by this plugin .
     *
     * @return Output File
     */
    public String outputFile() {
        return outputFile.value();
    }

    /**
     * Set Output File. FUll path to output file created by
     * this plugin .
     *
     * @param value Output File
     *
     * @return this plugin
     */
    public HaplotypesDataMetricsPlugin outputFile(String value) {
        outputFile = new PluginParameter<>(outputFile, value);
        return this;
    }

    /**
     * Pairs of methods (haplotype method name and range group
     * method name). Method pair separated by a comma, and
     * pairs separated by semicolon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>;<haplotype
     * method name2>,<range group name2>;<haplotype method
     * name3>
     *
     * @return Methods
     */
    public String methods() {
        return methods.value();
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name
     * and range group method name). Method pair separated
     * by a comma, and pairs separated by semicolon. The range
     * group is optional
     * Usage: <haplotype method name1>,<range group name1>;<haplotype
     * method name2>,<range group name2>;<haplotype method
     * name3>
     *
     * @param value Methods
     *
     * @return this plugin
     */
    public HaplotypesDataMetricsPlugin methods(String value) {
        methods = new PluginParameter<>(methods, value);
        return this;
    }
    
}
