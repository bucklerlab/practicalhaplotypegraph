package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.commons.math3.stat.descriptive.rank.Percentile
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

/**
 * Plugin to extract metrics from SAM file(s) related to alignment quality.
 * Summary statistics are written to a tab-delimited file with one row for each input SAM file,
 * and include: number of mapped and unmapped reads, mean and median alignment edit distances.
 * it also writes one file per input SAM containing the best (lowest) edit distances for each read,
 * separated by new lines
 *
 * Assumptions: sam/bam files have the optional field "NM", containing the alignment edit distance.
 *
 * Required input:
 * samDir - directory containing one or more .sam or .bam files
 * outDir - directory in which we will write output files
 *
 * Optional input:
 * samFile - if only one sam or bam file is to be processed, the name of the file. It should reside in samDir
 *
 * Output:
 * SAM_summary_statistics.txt - tab-delimited table of summary statistcs
 * SAMPLE_editDistances.txt - file containing the lowest edit distances for each read in SAMPLE.sam. One per sam processed.
 *
 * @author ahb232 March 6, 2023
 */
class SAMMetricsPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) :
    AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(SAMMetricsPlugin::class.java)

    private var samFile = PluginParameter.Builder("samFile", "", String::class.java)
        .guiName("sam  to process")
        .required(false)
        .description("Name of the sam file to process.")
        .build()

    private var samDir = PluginParameter.Builder("samDir", null, String::class.java)
        .guiName("sam directory")
        .inDir()
        .required(true)
        .description("Directory containing sam files")
        .build()

    private var outDir = PluginParameter.Builder("outDir", null, String::class.java)
        .guiName("output directory")
        .outDir()
        .required(true)
        .description("output directory for summary statistics")
        .build()

    /**
     * data class containing summary of alignment statstics for one file:
     * mappedReads: number of mapped reads
     * unmappedReads: number of unmapped reads
     * minEditDistances: list of the lowest edit distance for the alignments of each mapped read
     * Instead of printing the entire editDistances list, toString() calculates and prints mean and median
     */
    data class AlignmentSummary(val mappedReads: Int, val unmappedReads: Int, val minEditDistances: List<Int>) {
        override fun toString(): String {
            val mean = minEditDistances.sum().toDouble()/minEditDistances.size
            val percentile = Percentile()
            percentile.data = minEditDistances.sorted().map{it.toDouble()}.toDoubleArray()
            val median = percentile.evaluate(50.0)
            return "$mappedReads\t$unmappedReads\t$mean\t$median" }
    }

    override fun processData(input: DataSet?): DataSet? {

        // get list of files to process
        // if inferring files from directory, only use files ending in .sam or .bam
        val samFiles = if (samFile() != "") {
            listOf(File("${samDir()}/${samFile()}"))
        } else {
            File(samDir()).walk().filter{it.isFile}.filterNot { it.isHidden }.filter{it.name.endsWith(".sam") || it.name.endsWith(".bam")}.toList()
        }


        File("${outDir()}/SAM_summary_statistics.txt").bufferedWriter().use {writer ->
            writer.write("fileName\tmappedReads\tunmappedReads\tmeanMinEditDistance\tmedianMinEditDistance\n")
            samFiles.forEach {
                val stats = getSAMStats(it)
                writer.write("${it.nameWithoutExtension}\t$stats\n")
                writeEditDistancesToFile(stats.minEditDistances, "${outDir()}/${it.nameWithoutExtension}_editDistances.txt")
            }
        }
        
        return null
    }


    /**
     * Writes list of ints (edit distances) to a file. Each int gets its own line
     */
    fun writeEditDistancesToFile(distances: List<Int>, fileName: String) {
        File(fileName).bufferedWriter().use {writer ->
            distances.forEach { writer.write("$it\n") }
        }
    }

    /**
     * Given a sam or bam file, return the following as an AlignmentSummary object:
     * number of aligned reads
     * number of unaligned reads
     * list of the minimum edit distance for each aligned read
     * When the sam file contains multiple alignments (i.e. secondary alignments), we get the edit distance for each
     * alignment and then choose the lowest to add to the list.
     */
    fun getSAMStats(samFile: File): AlignmentSummary {
        val reader = loadSAMFileIntoSAMReader(samFile.absolutePath)
        val iterator = reader.iterator()

        var alignedReads = 0
        var unalignedReads = 0
        var editDistanceList = mutableListOf<Int>()

        var currentReadName = ""
        var currentEditDistanceList = mutableListOf<Int>()
        var isCurrentReadMapped = false

        while (iterator.hasNext()) {
            var currentRecord = iterator.next()

            if (currentRecord.readName == currentReadName) {
                if(!currentRecord.readUnmappedFlag) {
                    isCurrentReadMapped = true
                    currentEditDistanceList.add(currentRecord.getIntegerAttribute("NM"))
                }
            } else {
                if (isCurrentReadMapped) {
                    alignedReads += 1
                    currentEditDistanceList.minOrNull()?.let { editDistanceList.add(it) }

                } else if (currentReadName != "") {
                    unalignedReads += 1
                }

                currentReadName = currentRecord.readName
                isCurrentReadMapped = !currentRecord.readUnmappedFlag
                currentEditDistanceList = mutableListOf<Int>()
                if (isCurrentReadMapped) { currentEditDistanceList.add(currentRecord.getIntegerAttribute("NM")) }
            }
        }

        if (isCurrentReadMapped) {
            alignedReads += 1
            currentEditDistanceList.minOrNull()?.let { editDistanceList.add(it) }

        } else if (currentReadName != "") {
            unalignedReads += 1
        }

        return AlignmentSummary(alignedReads, unalignedReads, editDistanceList)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = SAMMetricsPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "SAMMetricsPlugin"
    }

    override fun getToolTipText(): String {
        return "SAMMetricsPlugin"
    }

    /**
     * Name of singular sam or bam file to process. Does not include path.
     * @return sam file
     */
    private fun samFile(): String {
        return samFile.value()
    }

    /**
     * Set sam file. Name of singular sam or bam file to process. Does not include path.
     * @param value sam file
     * @return this plugin
     */
    fun samFile(value: String):SAMMetricsPlugin {
        samFile = PluginParameter<String>(samFile, value)
        return this
    }

    /**
     * Path to a directory containing one or more sam/bam files.
     * To be recognized by this plugin, files must end with .sam or .bam
     * All other files in the directory will be ignored.
     * @return sam directory
     */
    private fun samDir(): String {
        return samDir.value()
    }

    /**
     * Set sam directory.
     * Path to a directory containing one or more sam/bam files.
     * To be recognized by this plugin, files must end with .sam or .bam
     * All other files in the directory will be ignored.
     * @param value sam directory
     * @return this plugin
     */
    fun samDir(value: String): SAMMetricsPlugin {
        samDir = PluginParameter<String>(samDir, value)
        return this
    }

    /**
     * A directory to which output text files will be written.
     * This includes the summary statistics table as well as a file of minumum edit distances
     * for each sam/bam file processed.
     * @return output directory
     */
    fun outDir(): String {
        return outDir.value()
    }

    /**
     * Set the output directory.
      * A directory to which output text files will be written.
     * This includes the summary statistics table as well as a file of minumum edit distances
     * for each sam/bam file processed.
     * @param value output directory
     * @return this plugin
     */
    fun outDir(value: String): SAMMetricsPlugin {
        outDir = PluginParameter<String>(outDir, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(SAMMetricsPlugin::class.java)
}