package net.maizegenetics.pangenome.gvcfFiltering;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.stream.Collectors;

/** TODO Separate Filter from FastaExtraction
 * Created by zrm22 on 7/18/17.
 *
 * This class will filter a GVCF file based on user specified criteria and will export both the filtered GVCF and the fasta setquence that the DB loader expects.
 *
 * The user can provide the specified criteria using a config file(similar to db configs and the class will filter.
 * Alternatively, the user can provide an upper and lower bound for poisson-based depth filtering.
 *
 * The following options are currently valid for filtering:
 *
 * exclusionString = String used for bcftools filtering.  If this is specified, no additional terms are added to exclude
 * DP_poisson_min = minimum poisson bound used for filtering(absolute minimum is 0.0)
 * DP_poisson_max = maximum poisson bound used for filtering(absolute maximum is 1.0)
 * DP_min = minimum DP bound
 * DP_max = maximum DP bound
 * GQ_min = minimum GQ bound
 * GQ_max = maximum GQ bound
 * QUAL_min = base quality minimum bound
 * QUAL_max = base quality maximum bound
 * filterHets = true/false or t/f, if true will filter sites where 2 or more alleles have above 0 AD
 */
public class ComputeMedianGVCFAndFilter {

    private static final Logger myLogger = LogManager.getLogger(ComputeMedianGVCFAndFilter.class);

    /**
     * Method to filter the gvcf and extract the fasta using the config file.
     * @param gvcfFile GVCF file to be filtered
     * @param outputDirectory Directory to store the gvcfs and fastas
     * @param refSeq Reference genome sequence
     * @param intervalFile  interval bed file holding the intervals we need to export for the fastas
     * @param filterProperties  Properties object using the config
     * @param isAssembly Boolean saying if the gvcf is an assembly or not.  If an assembly, we only exclude based on depth
     * @param bcfToolsPath Path
     */
    public static void filterAndExtractFasta(Path gvcfFile, String outputDirectory, GenomeSequence refSeq, String intervalFile, Properties filterProperties, boolean isAssembly, String bcfToolsPath) {
        String exclusionString;
        if(isAssembly) {
            exclusionString = "FORMAT/DP!=1";
        }
        else {
            //Check to see if there is an exclusion string in the config file
            if(!filterProperties.getProperty("exclusionString","").equals("")) {
                exclusionString = filterProperties.getProperty("exclusionString");
            }
            else {
                //we need to build up the exclusion string based on other parameters
                exclusionString = buildExclusionString(gvcfFile.toString(),filterProperties);
            }
        }

        myLogger.debug("Exclusion String: "+exclusionString);

        String outputGVCFFilteredFile = getFilteredOutputFileName(gvcfFile.toString(),outputDirectory+"Filtered/");
        myLogger.debug("OutputFilteredGVCFFile: "+outputGVCFFilteredFile);
        //Run the filtering and indexing process for the gvcf file
        String outputGVCFFilteredGzipped = filterAndCompressGVCFFile(gvcfFile,bcfToolsPath,exclusionString,outputGVCFFilteredFile);

        myLogger.debug("Extracting the Fasta file");
        //Here you can take the output and extract the fasta files
        //Load up the gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,outputGVCFFilteredGzipped);
        //Create an export file
        String outputFastaFileName = getFastaOutputFilename(outputGVCFFilteredFile,outputDirectory+"Fastas/");
        //Then loop through the interval file and extract the fasta
        BufferedReader reader = Utils.getBufferedReader(intervalFile);

        //write out the fasta
        try {
            writeFasta(reader, outputFastaFileName, gvcfSequence, outputGVCFFilteredGzipped);
        }
        catch(Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("ComputeMedianGVCFAndFilter: filterAndExtractFasta: Error writing fasta:",e);
        }
    }

    /**
     * Old method using a poissonProbability tuple to filter based on depth.  This should be deprecated once we tune the pipeline
     * TODO deprecate this method once the pipeline is tuned.
     * @param gvcfFile
     * @param outputDirectory
     * @param refSeq
     * @param intervalFile
     * @param poissonProbabliltyBounds
     * @param isAssembly
     * @param bcfToolsPath
     */
    public static void filterAndExtractFasta(Path gvcfFile, String outputDirectory, GenomeSequence refSeq, String intervalFile,
            Tuple<Double,Double> poissonProbabliltyBounds, boolean isAssembly, String bcfToolsPath) {

        myLogger.debug("GVCFFile: "+gvcfFile.toString());

        String exclusionString;

        if (isAssembly) {
            exclusionString = "FORMAT/DP!=1";
        } else { // haplotypes from haplotype_caller
            //check to see what the input bounds are, if they are [0,1] we do not want to filter on depth
            if(poissonProbabliltyBounds.getX()<=0.00001 && poissonProbabliltyBounds.getY()>=.999999) {
                exclusionString = "FORMAT/GQ<50 || QUAL<200 || (FORMAT/AD[*:0]>0 && FORMAT/AD[*:1]>0) || (FORMAT/AD[*:0]>0 && FORMAT/AD[*:2]>0) || (FORMAT/AD[*:1]>0 && FORMAT/AD[*:2]>0)";
            }
            else {
                double median = ComputeMedianAnnotation.getMedianDepth(gvcfFile.toString());
                //if median is equal to zero we need to set it to one.  Otherwise the poisson will not be created
                if (median == 0) {
                    median = 1;
                }

                Tuple<Integer, Integer> lowerAndUpperBound = computeBounds(median, poissonProbabliltyBounds);
                System.out.println("File: " + gvcfFile);
                System.out.println("Median: " + median + ", Bounds: " + lowerAndUpperBound);
                exclusionString = "FORMAT/DP>" + lowerAndUpperBound.getY() + " || FORMAT/DP<" + lowerAndUpperBound.getX() + " || FORMAT/GQ<50 || QUAL<200 || (FORMAT/AD[*:0]>0 && FORMAT/AD[*:1]>0) || (FORMAT/AD[*:0]>0 && FORMAT/AD[*:2]>0) || (FORMAT/AD[*:1]>0 && FORMAT/AD[*:2]>0)";
            }
        }
 
        String outputGVCFFilteredFile = getFilteredOutputFileName(gvcfFile.toString(),outputDirectory+"Filtered/");
        myLogger.debug("OutputGVCFFilteredFile: "+outputGVCFFilteredFile);

        //Run the filtering and indexing process for the gvcf file
        String outputGVCFFilteredGzipped = filterAndCompressGVCFFile(gvcfFile,bcfToolsPath,exclusionString,outputGVCFFilteredFile);

        try {
            myLogger.debug("Extracting the Fasta file");
            //Here you can take the output and extract the fasta files
            //Load up the gvcfSequence
            GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,outputGVCFFilteredGzipped);
            //Create an export file
            String outputFastaFileName = getFastaOutputFilename(outputGVCFFilteredFile,outputDirectory+"Fastas/");
            //Then loop through the interval file and extract the fasta
            BufferedReader reader = Utils.getBufferedReader(intervalFile);

            try {
                writeFasta(reader, outputFastaFileName, gvcfSequence, outputGVCFFilteredGzipped);
            }
            catch(Exception e) {
                myLogger.debug(e.getMessage(), e);
                throw new IllegalStateException("ComputeMedianGVCFAndFilter: filterAndExtractFasta: Error writing fasta:",e);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to read properties from the properties object and will build a new exclusion string
     * @param gvcfFile
     * @param filterProperties
     * @return
     */
    private static String buildExclusionString(String gvcfFile, Properties filterProperties) {
        ArrayList<String> exclusionTerms = new ArrayList<>();

        //Check the properties to look for poisson bounds
        if (filterProperties.containsKey("DP_poisson_min") || filterProperties.containsKey("DP_poisson_max")) {
            //If it has one of these, we need to get the median Depth
            double median = ComputeMedianAnnotation.getMedianDepth(gvcfFile);
            //if median is equal to zero we need to set it to one.  Otherwise the poisson will not be created
            if (median == 0) {
                median = 1;
            }
            //get the bounds out of the properties
            Tuple<Double,Double> poissonProbabilityBounds = new Tuple<>(Double.parseDouble(filterProperties.getProperty("DP_poisson_min","0.0")) ,
                                                                        Double.parseDouble(filterProperties.getProperty("DP_poisson_max","1.0")));

            Tuple<Integer, Integer> lowerAndUpperBound = computeBounds(median, poissonProbabilityBounds);

            exclusionTerms.add("FORMAT/DP<" + lowerAndUpperBound.getX());
            exclusionTerms.add("FORMAT/DP>" + lowerAndUpperBound.getY());

        }
        else {
            //set depth based on the properties
            if(filterProperties.containsKey("DP_min")) {
                exclusionTerms.add("FORMAT/DP<" + filterProperties.getProperty("DP_min"));
            }
            if(filterProperties.containsKey("DP_max")) {
                exclusionTerms.add("FORMAT/DP>" + filterProperties.getProperty("DP_max"));
            }
        }

        //Get the rest of the filter criterion
        //Check GQ
        if(filterProperties.containsKey("GQ_min")) {
            exclusionTerms.add("FORMAT/GQ<"+filterProperties.getProperty("GQ_min"));
        }
        if(filterProperties.containsKey("GQ_max")) {
            exclusionTerms.add("FORMAT/GQ>"+filterProperties.getProperty("GQ_max"));
        }
        //Check QUAL
        if(filterProperties.containsKey("QUAL_min")) {
            exclusionTerms.add("QUAL<"+filterProperties.getProperty("QUAL_min"));
        }
        if(filterProperties.containsKey("QUAL_max")) {
            exclusionTerms.add("QUAL>"+filterProperties.getProperty("QUAL_max"));
        }

        if(filterProperties.containsKey("filterHets")) {
            //If we have filter hets set to true,
            if(filterProperties.getProperty("filterHets").equals("true") || filterProperties.getProperty("filterHets").equals("t")) {
                exclusionTerms.add("(FORMAT/AD[*:0]>0 && FORMAT/AD[*:1]>0) || (FORMAT/AD[*:0]>0 && FORMAT/AD[*:2]>0) || (FORMAT/AD[*:1]>0 && FORMAT/AD[*:2]>0)");
//                exclusionTerms.add("(FORMAT/AD[0]>0 && FORMAT/AD[1]>0) || (FORMAT/AD[0]>0 && FORMAT/AD[2]>0) || (FORMAT/AD[1]>0 && FORMAT/AD[2]>0)");
            }
        }



        return exclusionTerms.stream().collect(Collectors.joining("||"));
    }


    /**
     * Method to filter and compress the GVCF file using bcftools
     * @param gvcfFile
     * @param bcfToolsPath
     * @param exclusionString
     * @param outputGVCFFilteredFile
     */
    private static String filterAndCompressGVCFFile(Path gvcfFile, String bcfToolsPath, String exclusionString, String outputGVCFFilteredFile) {
        try {
            ProcessBuilder builder;

            if(exclusionString.length() > 0) {
                builder = new ProcessBuilder(
                        bcfToolsPath, "filter", gvcfFile.toString(),
                        "-e", exclusionString,
                        "-o",outputGVCFFilteredFile
                );
            } else {
                builder = new ProcessBuilder(
                        bcfToolsPath, "filter", gvcfFile.toString(),
                        "-o",outputGVCFFilteredFile
                );
                myLogger.warn("No exclusion criteria have been selected for bcftools filter. All records will automatically pass.");
            }

            String runCommand = builder.command().stream().collect(Collectors.joining(" "));
            myLogger.debug("bcftools filter command: "+runCommand);
            Process process = builder.start();
            process.waitFor();

            // bgzip the file - needed to create index
            myLogger.debug("bgzip the file");
            String outputGVCFFilteredGzipped = outputGVCFFilteredFile + ".gz";
            builder = new ProcessBuilder(
                    "bgzip",  outputGVCFFilteredFile

            );
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            myLogger.debug(runCommand);
            process = builder.start();
            int error = process.waitFor();
            if (error != 0) myLogger.error("\nERROR " + error + " creating bgzipped  version of file: " + outputGVCFFilteredFile);

            // index the file
            myLogger.debug("tabix the bgzipped file");
            builder = new ProcessBuilder("tabix",outputGVCFFilteredGzipped);
            runCommand = builder.command().stream().collect(Collectors.joining(" "));
            myLogger.debug(runCommand);
            process = builder.start();
            error = process.waitFor();
            if (error != 0) myLogger.error("\nERROR " + error + " creating tabix  version of file: " + outputGVCFFilteredGzipped);

            return outputGVCFFilteredGzipped;
        }
        catch(Exception e) {
            myLogger.info("ComputeMedianGVCFAndFilter: filterAndIndexGVCFFile:"+e.toString());
            throw new IllegalStateException("Error filtering GVCFs.");
        }
    }

    /**
     * Method to write the Fasta for a given gvcf genome sequence.
     * It will write a fasta file containing sequence for each interval specified in the bed file.
     * @param reader
     * @param outputFastaFileName
     * @param gvcfSequence
     * @param filteredGVCF
     * @throws Exception
     */
    private static void writeFasta(BufferedReader reader, String outputFastaFileName, GenomeSequence gvcfSequence, String filteredGVCF) throws Exception{
        BufferedWriter writer = Utils.getBufferedWriter(outputFastaFileName);
        String currentLine = "";

        //Get the list of intervals and multithread the extractor
        ArrayList<Tuple<String,ArrayList<Integer>>> intervals = new ArrayList<>();

        int lastSlash = filteredGVCF.lastIndexOf("/");
        String gvcfNameOnly = filteredGVCF.substring(lastSlash+1);
        while((currentLine = reader.readLine())!=null) {
            Tuple<String,ArrayList<Integer>> interval = parseInterval(currentLine);
            intervals.add(interval);

        }
        ArrayList<String> outputStrings = (ArrayList<String>)intervals.parallelStream().map(currentInterval -> getOutputFastaString(currentInterval,gvcfNameOnly,gvcfSequence)).collect(Collectors.toList());

        for(String outputLine : outputStrings) {
            writer.write(outputLine);
        }
        writer.close();
    }

    /**
     * Method to get the ID line for each fasta record.  It is of the form:
     * >chr:stPos:endPos gvcfFilename
     *
     * We need the gvcfFile name so the DB upload knows where to find the correct gvcf file
     * @param currentInterval
     * @param gvcfFile
     * @param gvcfSequence
     * @return
     */
    private static String getOutputFastaString(Tuple<String, ArrayList<Integer>> currentInterval,String gvcfFile,GenomeSequence gvcfSequence) {
        // Fasta ID Line contains:
        //   >refChrom:refStartPos:refEndPos filteredGVCFFileName
        StringBuilder outputBuilder = new StringBuilder();
        outputBuilder.append(">");
        outputBuilder.append(currentInterval.getX());
        outputBuilder.append(":");
        outputBuilder.append(currentInterval.getY().get(0));
        outputBuilder.append(":");
        outputBuilder.append(currentInterval.getY().get(1));
        outputBuilder.append(" ").append(gvcfFile);
        outputBuilder.append("\n");
        outputBuilder.append(gvcfSequence.genotypeAsString(Chromosome.instance(currentInterval.getX()), currentInterval.getY().get(0),currentInterval.getY().get(1)));
//        outputBuilder.append(gvcfSequence.genotypeSequenceAsString(currentInterval.getX(),currentInterval.getY().get(0),currentInterval.getY().get(1)));
        outputBuilder.append("\n");
        return outputBuilder.toString();
    }

    /**
     * Method to parse the bed file record to get the start and end positions.
     * @param currentLine
     * @return
     */
    private static Tuple<String,ArrayList<Integer>> parseInterval(String currentLine) {
        ArrayList<Integer> startAndEndPosition = new ArrayList<>();
        String[] splitOnTab = currentLine.split("\\t");
        
        startAndEndPosition.add(Integer.parseInt(splitOnTab[1])+1); // bed files are 0-based, add 1 to make physical position
        startAndEndPosition.add(Integer.parseInt(splitOnTab[2])); // bed file end positions is open, so no adjustment needed for physical position mapping
        return new Tuple<>(splitOnTab[0],startAndEndPosition);
    }

    /**
     * Method to figure out the name of the filtered GVCF file
     * @param gvcfFile
     * @param directoryPath
     * @return
     */
    private static String getFilteredOutputFileName(String gvcfFile, String directoryPath) {
        int indexOfLastSlash = gvcfFile.lastIndexOf("/");
        String fileNameOnly = gvcfFile.substring(indexOfLastSlash);
        //Split off the extension
        return directoryPath + fileNameOnly.substring(0,fileNameOnly.length()-6) + "_filtered.g.vcf";
    }

    /**
     * Method to figure out the name for the output fasta file
     * @param gvcfFile
     * @param directoryPath
     * @return
     */
    private static String getFastaOutputFilename(String gvcfFile, String directoryPath) {
        int indexOfLastSlash = gvcfFile.lastIndexOf("/");
        String fileNameOnly = gvcfFile.substring(indexOfLastSlash);
        return directoryPath + fileNameOnly.substring(0,fileNameOnly.length()-6) + ".fa";
    }


    /**
     * Method to compute the depth bounds based on the poisson probability imports
     * @param median
     * @param poissonProbabilityBounds
     * @return
     */
    private static Tuple<Integer,Integer> computeBounds(double median,Tuple<Double,Double> poissonProbabilityBounds) {
        PoissonDistribution poissonDistribution = new PoissonDistribution(median);
        int lowerBound = -1;
        int counter = 2;
        while(true) {
            if(poissonDistribution.cumulativeProbability(counter)>poissonProbabilityBounds.getX()) {
                lowerBound = counter;
                break;
            }
            counter++;
        }
        counter = (int)median;
        int upperBound = -1;
        while(true) {
            if(poissonDistribution.cumulativeProbability(counter)>poissonProbabilityBounds.getY()) {
                upperBound = counter-1;
                break;
            }
            counter++;
        }

        return new Tuple<>(lowerBound,upperBound);
    }

    public static String filterGVCF(String inputFile, String outputFile, Properties configProperties, boolean isAssembly, String bcftools) {
        String exclusionString;
        if(isAssembly) {
            exclusionString = "FORMAT/DP!=1";
        }
        else {
            //Check to see if there is an exclusion string in the config file
            if(!configProperties.getProperty("exclusionString","").equals("")) {
                exclusionString = configProperties.getProperty("exclusionString");
            }
            else {
                //we need to build up the exclusion string based on other parameters
                exclusionString = buildExclusionString(inputFile,configProperties);
            }
        }

        return filterAndCompressGVCFFile(Paths.get(inputFile),bcftools,exclusionString,outputFile);
    }
}
