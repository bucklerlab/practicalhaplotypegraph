package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap
import it.unimi.dsi.fastutil.longs.LongOpenHashSet
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.lang.IllegalStateException
import java.util.*
import javax.swing.ImageIcon
import kotlin.math.ceil
import kotlin.math.min

fun main() {
    GeneratePluginCode.generateKotlin(KmerHashMapFromGraphPlugin::class.java)

}

/**
 * Creates a Map of 32-mer hash -> hapid list for the haplotypes in a HaplotypeGraph. Only hashes observed
 * in exactly one reference range will be kept. Also, hashes will be retained only if they map to at
 * most [maxHaplotypeProportion] * the number of haplotyes in a reference range. Only hashes that pass the filter
 * ((hashValue and [hashMask]) == [hashFilterValue]) will be considered. For example, setting [hashMask] = 3u and [hashFilterValue] = 1u
 * only uses hashes from kmers ending in C. To filter on the final two positions set [hashMask] = 16u (0b1111).
 * [hashFilterValue] is based on the two bit encoding of nucleotides: A -> 0, C -> 1, G -> 2, T -> 3.
 * For example, the [hashFilterValue] for CG is 6u (0b0110).
 *
 * To use KmerHashMapFromGraph, instantiate the class with a [HaplotypeGraph] containing the desired taxa and haplotypes
 * then call processGraphKmers() to generate keep and discard lists. Call createHashToHapidMapFromKeepList()
 * to map the kmers in the keep list to hapids. The resulting mapping is stored as the public field kmerHashToHapidMap
 * and can be used directly or written to a file by calling saveKmerToHapidMap(filePath: String).
 */
class KmerHashMapFromGraph(val graph: HaplotypeGraph,
                           val maxHaplotypeProportion: Double = 0.75,
                           val hashMask: Long = 3L,
                           val hashFilterValue: Long = 1L) {
    val myLogger = LogManager.getLogger(KmerHashMapFromGraph::class.java)
    private val keepSet = LongOpenHashSet()

    //3L masks all but the last (least significant) position, 1L is C
    //15L masks all but the last two positions, CG is 6L (0b0110)
    //15L masks all but the last two positions, GC is 9L (0b1001)

    /**
     * Finds the set of kmers meeting the conditions set by [maxHaplotypeProportion], the maximum proportion of
     * haplotypes in a reference range, and [hashMask] and [hashFilterValue], which determine which nucleotide(s)
     * are required to be in the final postion(s) of a kmer.
     *
     * This returns a HashMap of hash -> hapid list for all the kmers in the keep set.
     * Which allows the export to not need to do a second pass over the sequences to get the set of hapIds which contain the unique kmers.
     */
    fun processGraphKmersSinglePass() : Long2ObjectOpenHashMap<Set<Int>>{
        val keepMap = Long2ObjectOpenHashMap<Set<Int>>()
        val discardSet = LongOpenHashSet()
        var rangeCount = 0
        val startTime = System.nanoTime()
        for (refrange in graph.referenceRanges()) {
            if (rangeCount++ % 5000 == 0) {
                myLogger.debug("Processing range $rangeCount, keep set size = ${keepMap.size}, discard set size = ${discardSet.size}, elapse time ${(System.nanoTime() - startTime)/1e9} sec")
            }
            val nodes = graph.nodes(refrange)
            val maxHaplotypes = ceil(nodes.size * maxHaplotypeProportion)

            //create a map of hash -> count of occurrences for all the haplotypes in this reference range
            val (kmerHashCounts, longToHapIdMap) = countKmerHashesForHaplotypesAndHapIds(graph.nodes(refrange))

            for (hashCount in kmerHashCounts.entries) {
                val hashValue = hashCount.key

                //if the hash is in the discard set skip it
                if (discardSet.contains(hashValue)) continue

                when {
                    //if hash count >= numberOfHaplotype add it to the discard set
                    hashCount.value >= maxHaplotypes -> discardSet.add(hashValue)
                    //if the hash is already in the keepSet, it has been seen in a previous reference range
                    // so, remove it from the keep set and add it to the discard set
                    keepMap.containsKey(hashValue) -> {
                        keepMap.remove(hashValue)
                        discardSet.add(hashValue)
                    }
                    else -> {
                        keepMap[hashValue] = longToHapIdMap[hashValue]
                    }
                }
            }

        }
        myLogger.debug("Finished building kmer keep set, keep set size = ${keepMap.size}, discard set size = ${discardSet.size}, elapse time ${(System.nanoTime() - startTime)/1e9} sec")
        return keepMap
    }

    /**
     * Function to count the kmerHashes for a single reference range's haplotype nodes.
     * This gets put in 2 sets of maps.
     * One for the hash counts and one for a hash to a list of hapIds which contain that hash.
     */
    private fun countKmerHashesForHaplotypesAndHapIds(nodes: List<HaplotypeNode>) : Pair<Map<Long,Int>,Map<Long,Set<Int>>> {
        //start by splitting sequence into subsequences without N's
        val mapOfHashes = Long2IntOpenHashMap()
        val mapOfHapIds = Long2ObjectOpenHashMap<MutableSet<Int>>()

        for (node in nodes) {
            //split sequence on N's then filter on length > 31
            val splitList = node.haplotypeSequence().sequence()
                .split("N+".toRegex())
                .filter{it.length > 31}
            for (sequence in splitList) {
                var previousHash = Pair(0L, 0L)

                //for first 31 nucleotides just update the hash
                for (nucleotide in sequence.subSequence(0..30)) {
                    previousHash = updateKmerHashAndReverseCompliment(previousHash, nucleotide)
                }

                //start using kmers starting with the 32nd nucleotide
                for (nucleotide in sequence.subSequence(31 until sequence.length)) {
                    previousHash = updateKmerHashAndReverseCompliment(previousHash, nucleotide)
                    val minHash = min(previousHash.first, previousHash.second)
                    // Use only kmers with a specific ending nucleotide(s)
                    // hashMask determines how many positions will be used
                    // hashFilterValue determines which nucleotide will be kept at that position
                    //at this point the hash value, which is a ULong, must be converted to a Long in order to make
                    //use of the fastutils Long2IntOpenHashMap
                    if ((minHash and hashMask) == hashFilterValue) {
                        mapOfHashes.addTo(minHash,1)
                        if(mapOfHapIds.containsKey(minHash)) {
                            mapOfHapIds[minHash].add(node.id())
                        }
                        else {
                            mapOfHapIds[minHash] = mutableSetOf(node.id())
                        }
                    }
                }
            }
        }
        return Pair(mapOfHashes,mapOfHapIds)
    }

    /**
     * Function to save the kmer hash map to the specified file.  If writeToDB is set to true it will also update the haplotype list ID.
     */
    fun saveKmerHashesAndHapids(filePath: String, kmerMapToHapIds: Long2ObjectOpenHashMap<Set<Int>>, writeToDb: Boolean = true) {
        val refRangeToHapidMap = getRefRangeToHapidMap(graph)
        val hapIdToRefRangeMap = graph.referenceRanges().flatMap { graph.nodes(it) }.map { Pair(it.id(),it.referenceRange().id()) }.toMap()

        //Walk through the map and associate kmerLongs with specific referenceRanges
        val refRangeToKmerSetMap = mutableMapOf<Int,MutableSet<Long>>()
        for(kmerMap in kmerMapToHapIds.long2ObjectEntrySet()) {
            val kmer = kmerMap.longKey
            val currentRefRange = hapIdToRefRangeMap[kmerMap.value.first()]!!
            if(refRangeToKmerSetMap.containsKey(currentRefRange)) {
                refRangeToKmerSetMap[currentRefRange]?.add(kmer)
            }
            else {
                refRangeToKmerSetMap[currentRefRange] = mutableSetOf(kmer)
            }
        }


        val haplotypeListId = if(writeToDb) {
            PHGdbAccess(DBLoadingUtils.connection(false)).use { phgdb ->
                getHaplotypeListIdForGraph(graph, phgdb)
            }
        }
        else {
            -1
        }


        var rangeCount = 0
        val startTime = System.nanoTime()
        var seqscanTime = 0L

        Utils.getBufferedWriter(filePath).use { myWriter ->
            //start with haplistid header
            myWriter.write("#haplotypeListId=$haplotypeListId\n")

            for(refrangeId in refRangeToKmerSetMap.keys) {
                if(rangeCount%1000 == 0) {
                    myLogger.info("Time Spent Processing output: ${(System.nanoTime() - startTime)/1e9} seconds.  Processed ${rangeCount} Ranges.")
                }
                rangeCount++
                val kmers = refRangeToKmerSetMap[refrangeId]?:throw IllegalStateException("Error no kmers associated with this refRange ID: ${refrangeId}")

                val mapForRange = kmers.map { Pair(it, kmerMapToHapIds[it]) }.toMap()

                //generate map of hapidSet -> kmer hash list
                val hapidKmerHashMap = mutableMapOf<MutableSet<Int>, MutableList<Long>>()
                for (entry in mapForRange.entries) {
                    val kmerHashList = hapidKmerHashMap[entry.value]
                    if (kmerHashList == null) hapidKmerHashMap.put(entry.value.toMutableSet(), mutableListOf(entry.key))
                    else kmerHashList.add(entry.key)
                }

                val numberOfHaplotypesInRange = refRangeToHapidMap[refrangeId]?.size?:throw IllegalStateException("Error no haplotypes associated with this refRange.")
                val numberOfHapSets = hapidKmerHashMap.size
                if (numberOfHapSets == 0) continue  //skip to next refrange because there are no kmers here
                val numberOfBitsNeeded = numberOfHaplotypesInRange * numberOfHapSets
                //encodedHapSets is a BitSet containing all of the Hapsets seen in this reference range
                //each hapset is encoded with a series of bits equal to the number of haplotypes in the range
                //with the bits in this hapset set
                //the offset for each kmer is the offset for its associated hapset
                val encodedHapSets = BitSet(numberOfBitsNeeded)
                //make pairs of kmerHash, offset
                val kmerHashOffsets = mutableListOf<Pair<Long,Int>>()

                var offset = 0
                val hapidIndex = refRangeToHapidMap[refrangeId]
                check(hapidIndex != null) {"hapidIndex null for $refrangeId"}

                hapidKmerHashMap.entries.forEachIndexed { index, entry ->
                    //this line encodes a haplotype set (hapset)
                    for (hapid in entry.key) encodedHapSets.set(offset + hapidIndex[hapid]!!)
                    //this line stores a pair of kmerHash, offset for each kmer mapping to this haplotype set
                    for (kmerHash in entry.value) kmerHashOffsets.add(Pair(kmerHash,offset))
                    offset += numberOfHaplotypesInRange
                }

                //write the results of the range to a file
                //line 1: >rangeid
                myWriter.write(">${refrangeId}\n")
                //line 2: long1,long2,...,longn (range has hapid sets encoded into a bitSet, which can be stored as longs)
                myWriter.write("${encodedHapSets.toLongArray().joinToString(",")}\n")
                //line 3: hash1,offset1,hash2,offset2,...,hashn,offsetn
                myWriter.write("${kmerHashOffsets.map { "${it.first}@${it.second}" }.joinToString(",")}\n")

            }
        }

        myLogger.info("Saved kmer mapping to $filePath, elapsed time ${(System.nanoTime() - startTime)/1e9} sec, seqscan time ${seqscanTime/1e9} sec")
    }


    companion object {
        //Coding for nucleotides is A -> 0, C -> 1, G -> 2, T -> 3
        //Generated by nuc_char shr 1 and 3
        val Avalue: Long = 0L
        val Cvalue: Long = 1L
        val Gvalue: Long = 2L
        val Tvalue: Long = 3L
        val revCompA = Tvalue shl 62
        val revCompC = Gvalue shl 62
        val revCompG = Cvalue shl 62
        val revCompT = Avalue shl 62

        /**
         * Updates hashes with the next nucleotide for both the kmer sequence and its reverse compliment.
         */
        fun updateKmerHashAndReverseCompliment(hashes: Pair<Long,Long>, nucleotide: Char): Pair<Long,Long> {
            return when (nucleotide) {
                'A' -> Pair((hashes.first shl 2) or Avalue, (hashes.second ushr 2) or revCompA)
                'T' -> Pair((hashes.first shl 2) or Tvalue, (hashes.second ushr 2) or revCompT)
                'G' -> Pair((hashes.first shl 2) or Gvalue, (hashes.second ushr 2) or revCompG)
                'C' -> Pair((hashes.first shl 2) or Cvalue, (hashes.second ushr 2) or revCompC)
                else -> throw java.lang.IllegalArgumentException("Attempted to update kmer hash with an invalid nucleotide character($nucleotide). Must be one of A,G,C,T")
            }
        }

        /**
         * data class to hold the KmerMap information.
         */
        data class KmerMapData(val haplotypeListId: Int, val rangeToBitSetMap: Map<Int, BitSet>, val kmerHashToLongMap: Long2LongOpenHashMap)

        /**
         * Loads a kmer hash map file to memory for use in mapping reads.
         */
        fun loadKmerMaps(filename: String): KmerMapData {
            //Load the contents of the file into
            //rangeHapidMap: a map of refRangeId to an IntArray of the haplotype ids in the ReferenceRange
            // and the BitSet of all hapid sets
            //kmerHashmap: a map of kmer hash to reference range and offset into its BitSet, encoded as a long
            //These data structures all the reference range and haplotype set to be looked up for a kmer has

            val rangeToBitSetMap = mutableMapOf<Int, BitSet>()
            val kmerHashMap = Long2LongOpenHashMap()
            var lineCount = 0
            var totalLineCount = 0
            var refrangeId = 0
            var haplotypeListId = -1

            Utils.getBufferedReader(filename).useLines {
                it.forEach { inputStr ->
                    totalLineCount++

                    lineCount = when(inputStr.first()) {
                        '>' -> 1
                        '#' -> 4
                        else -> lineCount + 1
                    }

                    when (lineCount) {
                        1 -> {
                            refrangeId = inputStr.substring(1).toInt()                  }
                        2-> {
                            try {
                                val parsedLine = inputStr.split(",")
                                val myBitset = BitSet.valueOf(parsedLine.map { it.toLong() }.toLongArray())
                                rangeToBitSetMap[refrangeId] = myBitset
                            } catch(e: Exception) {
                                println("error at line $totalLineCount for input = $inputStr")
                                throw java.lang.IllegalArgumentException(e)
                            }
                        }
                        3 -> {
                            val refrangeLong = (refrangeId.toLong()) shl 32
                            val parsedLine = inputStr.split(",")
                            //values are pairs of hash, offset (long,int)
                            //add the pairs of entries to kmerHashMap
                            for (datapair in parsedLine) {
                                val hashOffset = datapair.split("@")

                                if (hashOffset.size < 2) {
                                    println("improperly formatted datapair at line $totalLineCount")
                                }

                                val hash = hashOffset[0].toLong()
                                val offset = refrangeLong or hashOffset[1].toLong()
                                kmerHashMap.put(hash, offset)
                            }
                        }
                        4 -> {
                            if (inputStr.startsWith("#haplotypeListId="))
                                haplotypeListId = inputStr.substringAfter('=', "-1").toInt()

                        }
                    }
                }
            }

            return KmerMapData(haplotypeListId, rangeToBitSetMap, kmerHashMap)
        }
    }
}

class KmerHashMapFromGraphPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(KmerHashMapFromGraphPlugin::class.java)


    private var maxHaplotypes = PluginParameter.Builder("maxHaplotypes", 0.75, Double::class.javaObjectType)
        .description("If a kmer is in more than (maxHaplotypes * number of haplotypes) in a reference range it will not be used.")
        .build()

    private var hashMask = PluginParameter.Builder("hashMask", 3, Int::class.javaObjectType)
        .description("The value used to mask the positions of a kmer hash used to pre-filter hashes. A value of 3 uses " +
                "the last position of the kmer, and a value of 15 uses the last two positionns")
        .build()

    private var hashFilter = PluginParameter.Builder("hashFilter", 1, Int::class.javaObjectType)
        .description("A kmer is used when its masked value equals hashFilter, where 0 = A, 1 = C, 2 = G, and 3 = T.")
        .build()

    private var saveFileName = PluginParameter.Builder("saveFile", null, String::class.java)
        .description("The name and path of the file to which the map will be saved.")
        .outFile()
        .build()

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
        .description("A config file containing database connection parameters. Either this value or a value for -configParameters is required.")
        .inFile()
        .build()

    override fun pluginDescription(): String {
        return "This plugin creates a kmer hash map with k = 32 and, optionally, saves it to a file. The kmer map is a map" +
                " of kmer hash to hapid list that associates each kmer hash with with haplotypes it occurs in." +
                " The first step in creating the map is to find a set of kmers that are observed in only one reference range" +
                " and that occur in at most maxHaplotypes of them, so that the kmers can discriminate between haplotypes." +
                " Once a diagnostic kmer set is found, a map is created of kmer hash value to the list of haplotypes in" +
                " which they occur. The map is then saved to a file for later use in mapping reads for imputation."
    }

    override fun processData(input: DataSet?): DataSet? {
        require(input != null) {"Must provide a HaplotypeGraph as input."}
        val graphDataList = input.getDataOfType(HaplotypeGraph::class.java)
        require(graphDataList.size == 1) {"Must provide exactly one HaplotypeGraph as input."}
        val graph = graphDataList[0].data as HaplotypeGraph
        val startTime = System.nanoTime()
        val kmerMapFromGraph = KmerHashMapFromGraph(graph, maxHaplotypes(), hashMask().toLong(), hashFilter().toLong())

        val outputMap = kmerMapFromGraph.processGraphKmersSinglePass()
        val endTime = System.nanoTime()
        myLogger.info("Time spent processing kmers: ${(endTime - startTime)/1E9}seconds.")
        val saveFile = saveFileName()
        val saveStart = System.nanoTime()
        if (saveFile != null) kmerMapFromGraph.saveKmerHashesAndHapids(saveFile, outputMap)
        val saveEnd = System.nanoTime()
        myLogger.info("Time spent saving out the index: ${(saveEnd - saveStart)/1E9} seconds.")
        return null
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Kmer Hash"
    }

    override fun getToolTipText(): String {
        return "generates a mapping of kmer hashes to hapid to use for read mapping"
    }
    /**
     * If a kmer is in more than (maxHaplotypes * number of
     * haplotypes) in a reference range it will not be used.
     *
     * @return Max Haplotypes
     */
    fun maxHaplotypes(): Double {
        return maxHaplotypes.value()
    }

    /**
     * Set Max Haplotypes. If a kmer is in more than (maxHaplotypes
     * * number of haplotypes) in a reference range it will
     * not be used.
     *
     * @param value Max Haplotypes
     *
     * @return this plugin
     */
    fun maxHaplotypes(value: Double): KmerHashMapFromGraphPlugin {
        maxHaplotypes = PluginParameter<Double>(maxHaplotypes, value)
        return this
    }

    /**
     * The value used to mask the positions of a kmer hash
     * used to pre-filter hashes. A value of 3 uses the last
     * position of the kmer, and a value of 15 uses the last
     * two positionns
     *
     * @return Hash Mask
     */
    fun hashMask(): Int {
        return hashMask.value()
    }

    /**
     * Set Hash Mask. The value used to mask the positions
     * of a kmer hash used to pre-filter hashes. A value of
     * 3 uses the last position of the kmer, and a value of
     * 15 uses the last two positionns
     *
     * @param value Hash Mask
     *
     * @return this plugin
     */
    fun hashMask(value: Int): KmerHashMapFromGraphPlugin {
        hashMask = PluginParameter<Int>(hashMask, value)
        return this
    }

    /**
     * A kmer is used when its masked value equals hashFilter,
     * where 0 = A, 1 = C, 2 = G, and 3 = T.
     *
     * @return Hash Filter
     */
    fun hashFilter(): Int {
        return hashFilter.value()
    }

    /**
     * Set Hash Filter. A kmer is used when its masked value
     * equals hashFilter, where 0 = A, 1 = C, 2 = G, and 3
     * = T.
     *
     * @param value Hash Filter
     *
     * @return this plugin
     */
    fun hashFilter(value: Int): KmerHashMapFromGraphPlugin {
        hashFilter = PluginParameter<Int>(hashFilter, value)
        return this
    }

    /**
     * The name and path of the file to which the map will
     * be saved.
     *
     * @return Save File
     */
    fun saveFileName(): String? {
        return saveFileName.value()
    }

    /**
     * Set Save File. The name and path of the file to which
     * the map will be saved.
     *
     * @param value Save File
     *
     * @return this plugin
     */
    fun saveFileName(value: String): KmerHashMapFromGraphPlugin {
        saveFileName = PluginParameter<String>(saveFileName, value)
        return this
    }

    /**
     * A config file containing database connection parameters.
     * Either this value or a value for -configParameters
     * is required.
     *
     * @return Config File
     */
    fun configFile(): String? {
        return configFile.value()
    }

    /**
     * Set Config File. A config file containing database
     * connection parameters. Either this value or a value
     * for -configParameters is required.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): KmerHashMapFromGraphPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

}
