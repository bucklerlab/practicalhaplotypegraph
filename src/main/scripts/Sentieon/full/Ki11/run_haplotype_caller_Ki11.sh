#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/Ki11/4882_6665_17304_HBEN2ADXX_Ki11_CTTGTA_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/Ki11/4882_6665_17304_HFFGKADXX_Ki11_CTTGTA_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/Ki11/Ki11_HL5WNCCXX_L5_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_all10_MergedPlus1000orGapDiffcoordinates.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 48 \
   --min_base_qual 21 \
   Ki11_haplotype_caller_output.g.vcf

