#!/bin/bash -x

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201704.01
SENTIEON='/programs/sentieon-genomics-201704.01/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'

BAMFilesInDir='FilteredBAMFiles'
HaplotyperOutputDir='HaplotyperOutput'

cd $BAMFilesInDir
taxaNames="`ls *.bam | cut -d- -f1 | sort -u`"
cd ..

for taxon in $taxaNames
do

echo "Taxon: " $taxon

input_list='';
  for f in $BAMFilesInDir/${taxon}*.bam
  do
   input_list="${input_list} -i ${f}"
  done

time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   ${input_list} \
   --interval hackathon_trimmed.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   ${HaplotyperOutputDir}/${taxon}_haplotype_caller_output.g.vcf

done
