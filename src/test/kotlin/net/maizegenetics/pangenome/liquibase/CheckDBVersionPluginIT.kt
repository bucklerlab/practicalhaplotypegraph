package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.File
import kotlin.test.assertTrue

@ExtendWith(SmallSeqExtension::class)
class CheckDBVersionPluginIT {

    companion object {

        val configWithSchema = "${SmallSeqExtension.liquibaseDir}/with_schema_config.txt"
        val configWithoutSchema = "${SmallSeqExtension.liquibaseDir}/without_schema_config.txt"

        val goodDB = "${SmallSeqExtension.liquibaseDir}/good.db"
        val badDB = "${SmallSeqExtension.liquibaseDir}/bad.db"

        val yesFile = File("${SmallSeqExtension.liquibaseDir}/run_yes.txt")
        val noFile = File("${SmallSeqExtension.liquibaseDir}/run_no.txt")
    }

    @BeforeEach
    fun createData() {
        FileUtils.cleanDirectory(SmallSeqExtension.liquibaseDirFile)

        val withSchemaWriter = Utils.getBufferedWriter(configWithSchema)
        withSchemaWriter.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=$goodDB\nDBtype=sqlite\n")
        withSchemaWriter.close()

        val withoutSchemaWriter = Utils.getBufferedWriter(configWithoutSchema)
        withoutSchemaWriter.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=$badDB\nDBtype=sqlite\n")
        withoutSchemaWriter.close()

        createDBs(goodDB,true)
        createDBs(badDB, false)
    }

    @Test
    fun dbWithVariantTablePostgres() {
        ParameterCache.load(SmallSeqExtension.configFilePath)
        CheckDBVersionPlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .performFunction(null)

        assertTrue(yesFile.exists())
        assertTrue(!noFile.exists())
    }

    @Test
    fun dbWithVariantTableSQLite() {
        ParameterCache.load(configWithSchema)
        CheckDBVersionPlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .performFunction(null)

        assertTrue(yesFile.exists())
        assertTrue(!noFile.exists())
    }

    @Test
    fun dbVWithoutVariantTableSQLite() {
        ParameterCache.load(configWithoutSchema)
        CheckDBVersionPlugin(null, false)
            .outputDir(SmallSeqExtension.liquibaseDir)
            .performFunction(null)

        assertTrue(noFile.exists())
    }

}