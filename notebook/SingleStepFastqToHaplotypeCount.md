**Single step GBS Fastq to Haplotype Counts**

Branch: PHG-36-gbsfastq-to-haplocnt

Steps:
1) Setting up JNI BWA
https://github.com/lindenb/jbwa
2) Create plugin with these steps:
* Pass in GraphDB, interanchor fasta, haplotype fasta, and GBS fastq
* Setup JNI connection
* Load haplotype graph
* Create "Multimap<<HapIds>>" to count haplotypes
* Only maintain reads with high quality and appropriate length
* Align against both references.
* Keep if a perfect match of a haplotype but not an interanchor
* Deal with issues of missing data in alternative anchors
* Count if passes
* export counts

To create the native bwa and JNI
```commandline
git clone https://github.com/lindenb/jbwa.git
cd jbwa/
make
```

-Djava.library.path=/Users/edbuckler/temp/jbwa/jbwa/src/main/native


This has a perfect match according to BLAST - why is it missed:

@HWI-ST538:168:D0DW9ACXX:4:1101:9583:54663 1:N:0:
TGCAGTGCACGCAGCAGCAAACCAAAGGTGAGCAGTAAGATGATGAGACGTACGTGGCCGTCACTAGTTCCCG
+
HHHHHIJJJJJJJJJJJJJJIJJIJJJJFHIJJJJFHJGIIIJIGJJGJJIGHGGFFF>ACDBDDDDCCDDDD
HAP:HWI-ST538:168:D0DW9ACXX:4:1101:9583:54663 1:N:0:	Strand=-	11915665	555	12	44S21M8S	0

query	Chr4	100.00	73	0	0	1	73	22431252	22431180	1e-30	135	TGCAGTGCACGCAGCAGCAAACCAAAGGTGAGCAGTAAGATGATGAGACGTACGTGGCCGTCACTAGTTCCCG
query	Chr2	100.00	73	0	0	1	73	59182396	59182324	1e-30	135	TGCAGTGCACGCAGCAGCAAACCAAAGGTGAGCAGTAAGATGATGAGACGTACGTGGCCGTCACTAGTTCCCG

The key thing about this hit, is that BWA does not high this region even though it is a perfect match.  It seems more likely
that something is wrong in generating reference.fa

This region is missing from the consensus as only B73 had the sequence - likely the sequence is duplicated.  It should be added to the 
the non-anchor fasta in the future.f

```commandline
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bwaall/bwa" mem -o test.sam HapCallingTestFiles/InterAnchorFasta/OnlyInteranchorsB73.fa smalltest.fastq
"/Users/edbuckler/Box Sync/MaizePHG/PHG_external_apps/bwaall/bwa" index Zea_mays.AGPv4.dna.toplevel.fa

```

Major problem was there was no good way convert from alignment on haplotype back to reference coordinate, I now have to align
using the reference genome.  This is a waste of CPU, and it will not work for really divergent sequences.