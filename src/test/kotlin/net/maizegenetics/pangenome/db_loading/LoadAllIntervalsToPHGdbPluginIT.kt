package net.maizegenetics.pangenome.db_loading

import org.junit.jupiter.api.Test
import net.maizegenetics.junit.SmallSeqExtension
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertTrue
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@ExtendWith(SmallSeqExtension::class)
class LoadAllIntervalsToPHGdbPluginIT {

    @Test
    fun testLoadAllIntervals() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        assertEquals(1, phg.getGenoidFromLine("Ref_Assembly"))
        assertEquals(1, phg.getMethodIdFromName("B73Ref_method"))
        assertEquals(2, phg.getMethodIdFromName("FocusRegion"))
        assertEquals(3, phg.getMethodIdFromName("B73Ref_method_PATH"))

        val methodNameNoDescription = "B73Ref_method"
        val methodNameDescMap1 = phg.getMethodDescriptionFromName(methodNameNoDescription)
        assertNull(methodNameDescMap1["methodDescription"])

        val methodNameWithDescription = "SmallSeqTestImputeReadMethod"
        val methodNameDescMap2 = phg.getMethodDescriptionFromName(methodNameWithDescription)
        assertNotNull(methodNameDescMap2["methodDescription"])
        assertTrue(methodNameDescMap2["methodDescription"] is String)

        // TODO: This counts gameteids, not hapids - rename method and add assert for number of haplotypes given a gamete_grp_id?
        assertEquals(1, phg.getHapidsForGenoid(1).size)

        // TODO:
        //  * Read bedfile line-by-line and assert, instead of hardcoded like below?
        //  * Confirm if ref range IDs loaded from bedfile will always be their line numbers (i.e. always incremented from 1 in order)
        assertEquals(1, phg.getRefRangeIDFromString("1:1:3000"))
        assertEquals(2, phg.getRefRangeIDFromString("1:6501:9500"))
        assertEquals(3, phg.getRefRangeIDFromString("1:13001:16000"))
        assertEquals(4, phg.getRefRangeIDFromString("1:19501:22500"))
        assertEquals(5, phg.getRefRangeIDFromString("1:26001:29000"))
        assertEquals(6, phg.getRefRangeIDFromString("1:32501:35500"))
        assertEquals(7, phg.getRefRangeIDFromString("1:39001:42000"))
        assertEquals(8, phg.getRefRangeIDFromString("1:45501:48500"))
        assertEquals(9, phg.getRefRangeIDFromString("1:52001:55000"))
        assertEquals(10, phg.getRefRangeIDFromString("1:58501:61500"))

        assertEquals(10, phg.getIntervalRangesWithIDForChrom("1").asMapOfRanges().count())

        assertEquals(10, phg.getRefRangesForMethod("FocusRegion").size)

        phg.close()
    }
}
