package net.maizegenetics.pangenome.hapCalling

import junit.framework.TestCase
import org.junit.Test
import java.io.File

class FastqcolToFastaConverterTest : TestCase() {

    @Test
    fun testLineToFastaRemoveAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, false)

        val input = "3,38 35 36 38 39 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 39 40 39 40 35 38 38 38 36 34 31 32 33 33 34 34 34 33 34 30 32 31 31 31 33 21 30 29 32 33 34 33 33 33 31 34 31 29 32 34 25 26 22 26 21 24,TGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT,18"
        val name = "testName"
        val counter = 23
        val expected = ">sample:testName|index:23|nreads:3|adapter:18\nTGCAGCCTCCCGAGCTCCA\n"

        assertEquals(expected, converter.lineToFasta(input, name, counter))
    }

    @Test
    fun testLineToFastaKeepAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", false, false)

        val input = "3,38 35 36 38 39 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 39 40 39 40 35 38 38 38 36 34 31 32 33 33 34 34 34 33 34 30 32 31 31 31 33 21 30 29 32 33 34 33 33 33 31 34 31 29 32 34 25 26 22 26 21 24,TGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT,18"
        val name = "testName"
        val counter = 23

        val expected = ">sample:testName|index:23|nreads:3|adapter:18\nTGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT\n"

        assertEquals(expected, converter.lineToFasta(input, name, counter))
    }


    @Test
    fun testLineToFastaNoAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, false)

        val input = "9,33 10 27 32 34 36 39 38 39 38 35 24 37 39 40 40 40 40 36 38 39 39 40 40 40 40 40 40 40 40 38 33 39 39 40 38 36 38 39 37 26 30 23 30 24 23 32 28 20 29 29 34 34 34 34 34 20 20 20 25 32 32 34 34 34 30 33 29 27 ,TGCAGAAACTAAAAAAAAGACATGGACGGCTTTGCCACCACGCAGACCTCTGCTTTAGTTTCTCCGCAT,0"
        val name = "someOtherName"
        val counter = 45

        val expected = ">sample:someOtherName|index:45|nreads:9|adapter:0\nTGCAGAAACTAAAAAAAAGACATGGACGGCTTTGCCACCACGCAGACCTCTGCTTTAGTTTCTCCGCAT\n"

        assertEquals(expected, converter.lineToFasta(input, name, counter))
    }

    @Test
    fun testLineToFastaExpandReads() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, true)

        val input = "3,36 37 37 39 33 35 39 38 40 41 41 41 41 41 41 41 41 41 41 41 41 40 41 41 41 40 41 41 34 38 39 40 41 41 41 41 41 28 34 39 40 41 41 39 37 35 35 29 32 34 35 35 35 35 35 35 35 35 35 35 30 33 35 35 35 35 34 28 33 ,TGCAGCGTCTCGAGCTCGCCGAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGC,21"
        val name = "823hcog w*(%W(*y4wn"
        val counter = 98

        val expected = ">sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:0\nTGCAGCGTCTCGAGCTCGCCGA\n" +
                ">sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:1\nTGCAGCGTCTCGAGCTCGCCGA\n" +
                ">sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:2\nTGCAGCGTCTCGAGCTCGCCGA\n"

        assertEquals(expected, converter.lineToFasta(input, name, counter))
    }



    @Test
    fun testLineToFastqRemoveAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, false)

        val input = "3,38 35 36 38 39 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 39 40 39 40 35 38 38 38 36 34 31 32 33 33 34 34 34 33 34 30 32 31 31 31 33 21 30 29 32 33 34 33 33 33 31 34 31 29 32 34 25 26 22 26 21 24,TGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT,18"
        val name = "testName"
        val counter = 23
        val expected = "@sample:testName|index:23|nreads:3|adapter:18\nTGCAGCCTCCCGAGCTCCA\n+\nGDEGHIIIIIIIIIIIIII\n"

        assertEquals(expected, converter.lineToFastq(input, name, counter))
    }

    @Test
    fun testLineToFastqKeepAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", false, false)

        val input = "3,38 35 36 38 39 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 39 40 39 40 35 38 38 38 36 34 31 32 33 33 34 34 34 33 34 30 32 31 31 31 33 21 30 29 32 33 34 33 33 33 31 34 31 29 32 34 25 26 22 26 21 24,TGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT,18"
        val name = "testName"
        val counter = 23

        val expected = "@sample:testName|index:23|nreads:3|adapter:18\nTGCAGCCTCCCGAGCTCCAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGT\n+\nGDEGHIIIIIIIIIIIIIIIIIIHIHIDGGGEC@ABBCCCBC?A@@@B6?>ABCBBB@C@>AC:;7;69\n"

        assertEquals(expected, converter.lineToFastq(input, name, counter))
    }

    @Test
    fun testLineToFastqNoAdapter() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, false)

        val input = "9,33 10 27 32 34 36 39 38 39 38 35 24 37 39 40 40 40 40 36 38 39 39 40 40 40 40 40 40 40 40 38 33 39 39 40 38 36 38 39 37 26 30 23 30 24 23 32 28 20 29 29 34 34 34 34 34 20 20 20 25 32 32 34 34 34 30 33 29 27 ,TGCAGAAACTAAAAAAAAGACATGGACGGCTTTGCCACCACGCAGACCTCTGCTTTAGTTTCTCCGCAT,0"
        val name = "someOtherName"
        val counter = 45

        val expected = "@sample:someOtherName|index:45|nreads:9|adapter:0\nTGCAGAAACTAAAAAAAAGACATGGACGGCTTTGCCACCACGCAGACCTCTGCTTTAGTTTCTCCGCAT\n+\nB+<ACEHGHGD9FHIIIIEGHHIIIIIIIIGBHHIGEGHF;?8?98A=5>>CCCCC555:AACCC?B><\n"

        assertEquals(expected, converter.lineToFastq(input, name, counter))
    }

    @Test
    fun testLineToFastqExpandReads() {
        val converter = FastqcolToFastaConverter(File("foo"), File("bar"), 1, ".FASTQCOL",
            ".fasta", true, true)

        val input = "3,36 37 37 39 33 35 39 38 40 41 41 41 41 41 41 41 41 41 41 41 41 40 41 41 41 40 41 41 34 38 39 40 41 41 41 41 41 28 34 39 40 41 41 39 37 35 35 29 32 34 35 35 35 35 35 35 35 35 35 35 30 33 35 35 35 35 34 28 33 ,TGCAGCGTCTCGAGCTCGCCGAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGC,21"
        val name = "823hcog w*(%W(*y4wn"
        val counter = 98

        val expected = "@sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:0\nTGCAGCGTCTCGAGCTCGCCGA\n+\nEFFHBDHGIJJJJJJJJJJJJI\n" +
                "@sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:1\nTGCAGCGTCTCGAGCTCGCCGA\n+\nEFFHBDHGIJJJJJJJJJJJJI\n" +
                "@sample:823hcog w*(%W(*y4wn|index:98|nreads:3|adapter:21|count:2\nTGCAGCGTCTCGAGCTCGCCGA\n+\nEFFHBDHGIJJJJJJJJJJJJI\n"

        assertEquals(expected, converter.lineToFastq(input, name, counter))

    }
}