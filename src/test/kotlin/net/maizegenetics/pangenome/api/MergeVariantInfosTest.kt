package net.maizegenetics.pangenome.api

import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class MergeVariantInfosTest {

    val infosToMerge = mutableListOf<HaplotypeNode.VariantInfo>(
            HaplotypeNode.VariantInfo("1",100,200,"A","A",null,false, intArrayOf(5,0)),
            HaplotypeNode.VariantInfo("1",201,300,"T","T",null,false, intArrayOf(10,0)),
            HaplotypeNode.VariantInfo("1",301,400,"C","C",null,false, intArrayOf(10,0)),
            HaplotypeNode.VariantInfo("1",401,450,"A","A",null,false, intArrayOf(5,0)),
            HaplotypeNode.VariantInfo("1",451,500,"A","A",null,false, intArrayOf(5,0))
    )

    val nonConsecInfo = HaplotypeNode.VariantInfo("1",502,600,"A","A",null,false, intArrayOf(5,0))
    val variantInfo = HaplotypeNode.VariantInfo("1",501,501,"T","A","T",true, intArrayOf(0,5))


    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    /**
     * Test to try various merging scenarios.  Some should not be able to be merged and some should.
     */
    @Test
    fun testAbleToMerge() {
        //True tests
        val consecInfo = HaplotypeNode.VariantInfo("1",501,600,"A","A",null,false, intArrayOf(5,0))
        val overlapInfo =  HaplotypeNode.VariantInfo("1",451,600,"A","A",null,false, intArrayOf(5,0))
        val containedInfo =  HaplotypeNode.VariantInfo("1",451,475,"A","A",null,false, intArrayOf(5,0))

        Assert.assertTrue("MergeVariantInfosTest.testAbleToMerge.  Cannot merge consecutive RefBlock",canVariantInfoBeAddedToRefBlockList(infosToMerge,consecInfo))
        Assert.assertTrue("MergeVariantInfosTest.testAbleToMerge.  Cannot merge overlapping RefBlock",canVariantInfoBeAddedToRefBlockList(infosToMerge,overlapInfo))
        Assert.assertTrue("MergeVariantInfosTest.testAbleToMerge.  Cannot merge contained RefBlock",canVariantInfoBeAddedToRefBlockList(infosToMerge,containedInfo))
        Assert.assertFalse("MergeVariantInfosTest.testAbleToMerge.  Can merge non-consecutive RefBlock",canVariantInfoBeAddedToRefBlockList(infosToMerge,nonConsecInfo))
        Assert.assertFalse("MergeVariantInfosTest.testAbleToMerge.  Can merge variantInfo RefBlock",canVariantInfoBeAddedToRefBlockList(infosToMerge,variantInfo))

    }

    //TODO this test is not aware of number of positions covered by number of bps covered.
    @Test
    fun computeDepthTest() {
        val depth = computeDepths(infosToMerge)
        Assert.assertEquals("MergeVariantInfosTest.computeDepthTest. Averaging Depth does not equal 7.0",7.0,depth,.001)
    }

    /**
     * Test to see if the merged variant infos actually match what is expected.
     */
    @Test
    fun testMergingListVIs() {
        // NOTE: LCJ _ I added refRangeStart and refRangeEnd of 1,3000 arbitrarily as the deterineASMInfo() call
        // referenced by mergRefBlocks() now needs these parameters.  The test passes with these values - I
        // don't believe this test makes use of the asm coordinates that are created.
        val mergedVariantInfo = mergeRefBlocks(infosToMerge,1,3000)

        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs Chrom Does not match","1",mergedVariantInfo.chromosome())
        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs startPos Does not match",100,mergedVariantInfo.start())
        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs endPos Does not match",500,mergedVariantInfo.end())
        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs genotype Does not match","A",mergedVariantInfo.genotypeString())
        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs refAllele Does not match","A",mergedVariantInfo.refAlleleString())
        Assert.assertNull("MergeVariantInfosTest.testMergingListVIs altAllele is not null",mergedVariantInfo.altAlleleString())
        Assert.assertFalse("MergeVariantInfosTest.testMergingListVIs isVariant is not false",mergedVariantInfo.isVariant)
        Assert.assertEquals("MergeVariantInfosTest.testMergingListVIs avg Depth Does not match",7,mergedVariantInfo.depth()[0])
    }

    @Test
    fun canVariantInfosBeMergedTest() {
        Assert.assertTrue("MergeVariantInfosTest.canVariantInfosBeMergedTest Original VariantInfo List cannot be merged", canVariantInfosBeMerged(infosToMerge))
        //Should fail if out of order
        Assert.assertFalse("MergeVariantInfosTest.canVariantInfosBeMergedTest Shuffled VariantInfo merged incorrectly", canVariantInfosBeMerged(infosToMerge.shuffled(Random(12345))))
        Assert.assertFalse("MergeVariantInfosTest.canVariantInfosBeMergedTest VariantInfo plus nonConsec merged incorrectly", canVariantInfosBeMerged(infosToMerge.plus(nonConsecInfo)))
        Assert.assertFalse("MergeVariantInfosTest.canVariantInfosBeMergedTest VariantInfo plus variant merged incorrectly", canVariantInfosBeMerged(infosToMerge.plus(variantInfo)))
    }

}