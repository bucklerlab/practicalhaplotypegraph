**Inbred Line Haplotype Pipeline**

The best practices for processing maize inbred lines for creating anchor 
region haplotypes is the following:

Alignment-> Filter Alignment -> Sentieon GATK Haplotype -> Filter GVCF -> 
Create Fasta -> Load in DB

Details:

**Alignment** 
* Alignment is done with BWA-MEM with the following settings:
* Script: src/main/scripts/bwa/runBWAMemTemplate.sh
* Key parameters are: 

**Filter Alignment**
The alignment is then filtered to remove the low quality mapping reads, and can optionally be filtered to only 
include the anchor regions (saves size) + and - 1000bp.  The buffer is included as GATK uses a Active window, so the 
surrounding bp allow this to perform.
* Filtering is done with samtools 1.3.1 Using htslib 1.3.1
* Script: src/main/scripts/InbredHaplotyperPipeline/filter_bam.sh
* Key parameters: -q 48 (mapping quality)
* Anchor file with buffer: data/hackathon_trimmed_intervals_plusminus1000.bed
* Input Files: data/cbsufsrv4_data2_pangenome_graph_bams_to_filter20170712.txt
* Output Files: data/cbsumm11_filtered_bams20170714.txt

**Sentieon GATK Haplotype**
Inbred haplotypes are then called using Sentieon's version of the GATK 3.X.  Haplotypes are only called in the anchor 
regions, this outputs a GVCF file
* Software: sentieon-genomics-201704.01
* Script: src/main/scripts/InbredHaplotyperPipeline/run_haplotype_caller.sh
* Key parameters: --algo Haplotyper --ploidy 1 --emit_mode gvcf
* Anchor file: data/hackathon_trimmed.intervals

**Filter GVCF**
Low quality SNPs and reference ranges are then eliminated from GVCF.  We would prefer to have Ns in the resulting
rather than errors.
* Software: Current: bcftools, Future: TASSEL filter
* Script:src/main/scripts/InbredHaplotyperPipeline/FilterGVCFs.sh
* Key parameters: -e "FORMAT/DP>upperDPBound || FORMAT/DP<lowerDPBound || FORMAT/GQ<50 || QUAL<200 || (FORMAT/AD[0]>0 && FORMAT/AD[1]>0) || (FORMAT/AD[0]>0 && FORMAT/AD[2]>0) || (FORMAT/AD[1]>0 && FORMAT/AD[2]>0)"
* _Note in the above, there are double ampersands between the FORMAT/AD[0]>0 and FORMAT/AD[1]>0 and all the similar conditions.  It is not showing up in IntelliJs preview._
* _To Compute the upperDPBound and lowerDPBound, compute the median of the gvcf file.  Then create PoissonDistribution and set lowerDPBound >Poisson CDF (0.02) and upperDPBound<Poisson CDF (0.98)_
* Currently working simple TASSEL script to compute median and extract fastas is located in the branch TAS-1214-gvcf-to-fasta-branch 

**Create FASTA**
The filtered GVCF is then combined with the reference genome to produce full sequences.  This create a fasta file
with each line being one anchor.
* Software: TASSEL GVCFFastaSequenceBuilder(on TAS-1214-gvcf-to-fasta-branch right now)
* Script:
* Key parameters:
* _NOT STABLE, Working on unit test._

**Load in DB**
The fasta are then loaded in the the PHG database.
* Software:
* Script:
* Key parameters:

![TrimmingPipeline20170718.jpg](TrimmingPipeline20170718.jpg)