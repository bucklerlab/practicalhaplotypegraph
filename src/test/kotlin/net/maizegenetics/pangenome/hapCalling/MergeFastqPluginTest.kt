package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths.genotypingKeyFile
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths.smallSeqBaseDir
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.AfterAll
import java.io.File
import java.util.Random
import kotlin.test.fail

class MergeFastqPluginTest {
    val testingDir = System.getProperty("user.home") + "/temp/MergeFastqTest/"

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        File(testingDir).deleteRecursively()
        File(testingDir).mkdirs()

    }
    @AfterAll
    fun tearDown() {
        File(testingDir).deleteRecursively()
    }

    @Test
    fun testMergeSimpleFastqs() {
        //make 10 fastqs with 10 reads each
        val testName = "simpleTest"
        val fastqDir = "${testingDir}/${testName}/fastq/"
        val outputGroupingFile = "${testingDir}/${testName}/simpleGroupingFile.txt"
        val outputMergedDir = "${testingDir}/${testName}/simpleFastqMerged/"
        val outputMergedBAMDir = "${testingDir}/${testName}/simpleBAMDir/"
        val outputScriptTemplate = "${testingDir}/${testName}/scriptTemplate"
        val imputationKeyFile = "${testingDir}/${testName}/imputationKeyFile.txt"

        makeMergeFastqDirs(testName)

        val random = Random(12345)
        val fastqFileNameToLines = mutableMapOf<String,List<String>>() //Map of fastq file name to list of lines
        for (i in 0..9) {
            Utils.getBufferedWriter("${fastqDir}/testFastqSimple_${i}.fastq").use { output ->
                val totalLinesToWrite = mutableListOf<String>()
                (0 ..9).forEach { j ->  //Make a random DNA read of 150 bps
                    val sequence = (0..149).map { ("ACGT"[random.nextInt(4)]) }.joinToString("")
                    val qualityScores = (0..149).map { "A" }.joinToString("")

                    val linesToWrite = listOf("@${i}_${j}",sequence,"+",qualityScores)
                    output.write(linesToWrite.joinToString("\n","","\n"))
                    totalLinesToWrite.addAll(linesToWrite)
                }
                fastqFileNameToLines["${fastqDir}/testFastqSimple_${i}.fastq"] = totalLinesToWrite
            }
        }

        //Build a simple KeyFile
        Utils.getBufferedWriter(imputationKeyFile).use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\n")
            (0..9).forEach { output.write("Line${it}\tDUMMY_${it}\t${fastqDir}/testFastqSimple_${it}.fastq\n") }
        }

        //Merge fastqs in batches of 2 files each
        val truthMergeMap = (0 .. 9).map { "${fastqDir}/testFastqSimple_${it}.fastq" }.windowed(2,2,true).mapIndexed() { index, fastqs ->
            //For each batch, look up the lines in the fastqFileNameToLines map and combine the lists
            val mergedFastqLines = fastqs.flatMap { fastqFileNameToLines[it]!! }
            Pair(index,mergedFastqLines)
        }.toMap()

        // testThePlugin()
        MergeFastqPlugin()
            .fastqDir("${fastqDir}")
            .outputBAMDir(outputMergedBAMDir)
            .outputMergedFastqDir(outputMergedDir)
            .outputGroupingFile(outputGroupingFile)
            .numberOfFastqsPerMerge(2)
            .useOriginalReadNames(true)
            .scriptTemplate(outputScriptTemplate)
            .keyFile(imputationKeyFile)
            .minimapNParam(100)
            .numberOfThreads(10)
            .minimapFParam("1000,2000")
            .minimapLocation("/temp/minimap2/minimap2")
            .indexFile("/temp/minimap2/indexFile.mmi")
            .performFunction(null)

        //load in the mergedFastqs and check that they match the truth
        truthMergeMap.forEach { (index,truthLines) ->
            val mergedFastqLines = Utils.getBufferedReader("${outputMergedDir}/mergedFastq_batch_${index}.fq").readLines()
            Assert.assertEquals("Size of loaded Fastq does not match:",truthLines.size,mergedFastqLines.size)
            truthLines.zip(mergedFastqLines).forEach { (truthLine,mergedLine) ->
                Assert.assertEquals("Loaded Fastq does not match truth:",truthLine,mergedLine)
            }
        }

        //load in the grouping file and check that it matches the truth
        val groupingFileLines = Utils.getBufferedReader(outputGroupingFile).readLines()
        val truthGroupingFileLines = listOf("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead",
            "testFastqSimple_0.fastq\tmergedFastq_batch_0.fq\t0\t10\t0\t9",
            "testFastqSimple_1.fastq\tmergedFastq_batch_0.fq\t0\t10\t10\t19",
            "testFastqSimple_2.fastq\tmergedFastq_batch_1.fq\t1\t10\t0\t9",
            "testFastqSimple_3.fastq\tmergedFastq_batch_1.fq\t1\t10\t10\t19",
            "testFastqSimple_4.fastq\tmergedFastq_batch_2.fq\t2\t10\t0\t9",
            "testFastqSimple_5.fastq\tmergedFastq_batch_2.fq\t2\t10\t10\t19",
            "testFastqSimple_6.fastq\tmergedFastq_batch_3.fq\t3\t10\t0\t9",
            "testFastqSimple_7.fastq\tmergedFastq_batch_3.fq\t3\t10\t10\t19",
            "testFastqSimple_8.fastq\tmergedFastq_batch_4.fq\t4\t10\t0\t9",
            "testFastqSimple_9.fastq\tmergedFastq_batch_4.fq\t4\t10\t10\t19")
        Assert.assertEquals("Size of loaded grouping file does not match:",truthGroupingFileLines.size,groupingFileLines.size)
        truthGroupingFileLines.zip(groupingFileLines).forEach { (truthLine,groupingFileLine) ->
            Assert.assertEquals("Loaded grouping file does not match truth:",truthLine,groupingFileLine)
        }

        //check the script templates are correct
        val scriptTemplateLines = Utils.getBufferedReader("${outputScriptTemplate}_0.sh").readLines()
        val truthScriptTemplateLines = listOf("#!/bin/bash -x",
            "time /temp/minimap2/minimap2 -a -x sr -t 10 --secondary=yes -N100 -f1000,2000 --eqx -Q /temp/minimap2/indexFile.mmi ${outputMergedDir}mergedFastq_batch_0.fq | samtools view -b > ${outputMergedBAMDir}/mergedFastq_batch_0.bam",
            "time /temp/minimap2/minimap2 -a -x sr -t 10 --secondary=yes -N100 -f1000,2000 --eqx -Q /temp/minimap2/indexFile.mmi ${outputMergedDir}mergedFastq_batch_1.fq | samtools view -b > ${outputMergedBAMDir}/mergedFastq_batch_1.bam",
            "time /temp/minimap2/minimap2 -a -x sr -t 10 --secondary=yes -N100 -f1000,2000 --eqx -Q /temp/minimap2/indexFile.mmi ${outputMergedDir}mergedFastq_batch_2.fq | samtools view -b > ${outputMergedBAMDir}/mergedFastq_batch_2.bam",
            "time /temp/minimap2/minimap2 -a -x sr -t 10 --secondary=yes -N100 -f1000,2000 --eqx -Q /temp/minimap2/indexFile.mmi ${outputMergedDir}mergedFastq_batch_3.fq | samtools view -b > ${outputMergedBAMDir}/mergedFastq_batch_3.bam",
            "time /temp/minimap2/minimap2 -a -x sr -t 10 --secondary=yes -N100 -f1000,2000 --eqx -Q /temp/minimap2/indexFile.mmi ${outputMergedDir}mergedFastq_batch_4.fq | samtools view -b > ${outputMergedBAMDir}/mergedFastq_batch_4.bam"
        )

        Assert.assertEquals("Size of loaded script template does not match:",truthScriptTemplateLines.size,scriptTemplateLines.size)
        truthScriptTemplateLines.zip(scriptTemplateLines).forEach { (truthLine,scriptTemplateLine) ->
            Assert.assertEquals("Loaded script template does not match truth:",truthLine,scriptTemplateLine)
        }
    }


    @Test
    fun testMergeFilesMethod() {
        val testName = "mergeFilesMethod"
        val fastqDir = "${testingDir}/${testName}/fastq/"
        val outputGroupingFile = "${testingDir}/${testName}/simpleGroupingFile.txt"
        val outputMergedDir = "${testingDir}/${testName}/simpleFastqMerged/"
        val outputMergedBAMDir = "${testingDir}/${testName}/simpleBAMDir/"
        val outputScriptTemplate = "${testingDir}/${testName}/scriptTemplate"
        val imputationKeyFile = "${testingDir}/${testName}/imputationKeyFile.txt"

        makeMergeFastqDirs(testName)

        //Make 2 fastqs with 10 reads each
        val random = Random(12345)
        val fastqFileNameToLines = mutableMapOf<String,List<String>>() //Map of fastq file name to list of lines
        for (i in 0..1) {
            Utils.getBufferedWriter("${fastqDir}/testFastqSimple_${i}.fastq").use { output ->
                val totalLinesToWrite = mutableListOf<String>()
                (0 ..9).forEach { j ->  //Make a random DNA read of 150 bps
                    val sequence = (0..149).map { ("ACGT"[random.nextInt(4)]) }.joinToString("")
                    val qualityScores = (0..149).map { "A" }.joinToString("")

                    val linesToWrite = listOf("@${i}_${j}",sequence,"+",qualityScores)
                    output.write(linesToWrite.joinToString("\n","","\n"))
                    totalLinesToWrite.addAll(linesToWrite)
                }
                fastqFileNameToLines["${fastqDir}/testFastqSimple_${i}.fastq"] = totalLinesToWrite
            }
        }

        //Build a simple KeyFile
        Utils.getBufferedWriter(imputationKeyFile).use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\n")
            (0..1).forEach { output.write("Line${it}\tDUMMY_${it}\t${fastqDir}/testFastqSimple_${it}.fastq\n") }
        }

        Utils.getBufferedWriter(outputGroupingFile).use { writer ->
            writer.write("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead\n")
            val plugin = MergeFastqPlugin()
                .fastqDir("${fastqDir}")
                .outputBAMDir(outputMergedBAMDir)
                .outputMergedFastqDir(outputMergedDir)
                .outputGroupingFile(outputGroupingFile)
                .numberOfFastqsPerMerge(2)
                .useOriginalReadNames(true)
                .scriptTemplate(outputScriptTemplate)
                .keyFile(imputationKeyFile)

            plugin.mergeFiles(
                "mergedFastq_batch_0.fq",
                writer,
                listOf(File("${fastqDir}/testFastqSimple_0.fastq"), File("${fastqDir}/testFastqSimple_1.fastq")),
                0
            )
        }
        val mergedFastqLines = Utils.getBufferedReader("${outputMergedDir}/mergedFastq_batch_0.fq").readLines()
        val truthLines = fastqFileNameToLines.flatMap { it.value }
        Assert.assertEquals("Size of loaded Fastq does not match:",truthLines.size,mergedFastqLines.size)
        truthLines.zip(mergedFastqLines).forEach { (truthLine,mergedLine) ->
            Assert.assertEquals("Loaded Fastq does not match truth:",truthLine,mergedLine)
        }

        //check the grouping file is correct
        val groupingFileLines = Utils.getBufferedReader(outputGroupingFile).readLines()
        val truthGroupingFileLines = listOf("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead",
            "testFastqSimple_0.fastq\tmergedFastq_batch_0.fq\t0\t10\t0\t9",
            "testFastqSimple_1.fastq\tmergedFastq_batch_0.fq\t0\t10\t10\t19")
        Assert.assertEquals("Grouping file size does not match truth:",truthGroupingFileLines.size,groupingFileLines.size)
        Assert.assertEquals("Grouping file does not match truth:",truthGroupingFileLines,groupingFileLines)
    }

    fun makeMergeFastqDirs(testName: String) {
        val fastqDir = "${testingDir}/${testName}/fastq/"
        val outputMergedDir = "${testingDir}/${testName}/simpleFastqMerged/"
        val outputMergedBAMDir = "${testingDir}/${testName}/simpleBAMDir/"


        File("${testingDir}/${testName}/").mkdirs()
        File(fastqDir).mkdirs()
        File(outputMergedDir).mkdirs()
        File(outputMergedBAMDir).mkdirs()
    }

    @Test
    fun testIncompleteFastqs() {

        val testName = "testIncompleteFastqs"
        val fastqDir = "${testingDir}/${testName}/fastq/"
        val outputMergedDir = "${testingDir}/${testName}/simpleFastqMerged/"
        val outputMergedBAMDir = "${testingDir}/${testName}/simpleBAMDir/"
        val outputGroupingFile = "${testingDir}/${testName}/groupingFile.txt"
        val imputationKeyFile = "${testingDir}/${testName}/imputationKeyFile.txt"
        val outputScriptTemplate = "${testingDir}/${testName}/scriptTemplate.txt"

        makeMergeFastqDirs(testName)

        val fastqFileNameToLines = mutableMapOf<String,List<String>>() //Map of fastq file name to list of lines
        val random = Random(1234)

        (0..1).forEach { i ->
            Utils.getBufferedWriter("${fastqDir}/testFastqSimple_${i}.fastq").use { output ->
                val totalLinesToWrite = mutableListOf<String>()
                (0 ..9).forEach { j ->  //Make a random DNA read of 150 bps
                    val sequence = (0..149).map { ("ACGT"[random.nextInt(4)]) }.joinToString("")
                    val qualityScores = (0..149).map { "A" }.joinToString("")

                    val linesToWrite = listOf("@${i}_${j}",sequence,"+",qualityScores)
                    output.write(linesToWrite.joinToString("\n","","\n"))
                    totalLinesToWrite.addAll(linesToWrite)
                }
                output.write("@${i}_10\nAAAAAAAAAAA\n+,AAAAAA\n")
                fastqFileNameToLines["${fastqDir}/testFastqSimple_${i}.fastq"] = totalLinesToWrite
            }
        }

        Utils.getBufferedWriter(imputationKeyFile).use { output ->
            (0..1).forEach { output.write("Line${it}\tDUMMY_${it}\t${fastqDir}/testFastqSimple_${it}.fastq\n") }
        }

        Utils.getBufferedWriter(outputGroupingFile).use { writer ->
            writer.write("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead\n")
            val plugin = MergeFastqPlugin()
                .fastqDir("${fastqDir}")
                .outputBAMDir(outputMergedBAMDir)
                .outputMergedFastqDir(outputMergedDir)
                .outputGroupingFile(outputGroupingFile)
                .numberOfFastqsPerMerge(2)
                .useOriginalReadNames(true)
                .scriptTemplate(outputScriptTemplate)
                .keyFile(imputationKeyFile)

            plugin.mergeFiles(
                "mergedFastq_batch_0.fq",
                writer,
                listOf(File("${fastqDir}/testFastqSimple_0.fastq"), File("${fastqDir}/testFastqSimple_1.fastq")),
                0
            )
        }

        val mergedFastqLines = Utils.getBufferedReader("${outputMergedDir}/mergedFastq_batch_0.fq").readLines()
        val truthLines = fastqFileNameToLines.values.flatten()
        //Should match the truth as we are not adding in the bad reads to the truth set
        truthLines.zip(mergedFastqLines).forEach { (truthLine,mergedLine) ->
            Assert.assertEquals("Loaded Fastq does not match truth:",truthLine,mergedLine)
        }

        //check the grouping file is correct
        val groupingFileLines = Utils.getBufferedReader(outputGroupingFile).readLines()
        val truthGroupingFileLines = listOf("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead",
            "testFastqSimple_0.fastq\tmergedFastq_batch_0.fq\t0\t10\t0\t9",
            "testFastqSimple_1.fastq\tmergedFastq_batch_0.fq\t0\t10\t10\t19")
        Assert.assertEquals("Grouping file size does not match truth:",truthGroupingFileLines.size,groupingFileLines.size)
        Assert.assertEquals("Grouping file does not match truth:",truthGroupingFileLines,groupingFileLines)

    }
}