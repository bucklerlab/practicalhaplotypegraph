package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.LoadHaplotypesFromGVCFPlugin
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.pangenome.hapcollapse.RunHapConsensusPipelinePlugin
import net.maizegenetics.pangenome.liquibase.defineDefaultLiquibaseDir
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesMultiThreadPlugin
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyMAFFromAnchorWavePlugin
import net.maizegenetics.pangenome.processAssemblyGenomes.MAFToGVCFPlugin
import net.maizegenetics.plugindef.*
import net.maizegenetics.util.Tuple
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.BufferedReader
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDate
import javax.swing.ImageIcon

/**
 * This class creates a pipeline that will load an existing PHG database with assembly and/or WGS haplotypes.
 * This plugin runs the entire pipeline from alignment to loadingthe haplotypes into the database using anchorwave
 *  as the assembly aligner.
 *
 * The only assembly aligner method supported for PHG version 1.0 and higher is anchorwave.  This is due to
 * the database changes that no longer store variants in the database.  The software that processed mummer4
 * data relied on the variants being stored in the database.  In addition, the software used for imputation
 * and additional processing currently relies on variants stored in gvcf files.
 *
 * Users may align with any method they choose, then convert those alignment files to GVCF files and
 * load with LoadHaplotypesFromGVCFPlugin.   For external users, this pipeline may be useful when haplotypes from WGS
 * will be created.  It is not recommended if your haplotypes will come from aligned assemblies. Anchorwave uses both
 * a lot of memory and a lot of computing power.  We recommend users align with anchorwave on high performance/
 * high memory machines outside of the pipeline by running the AssemblyMAFFromAnchorWavePlugin.  The generated MAF files
 * can then be run through MAFToGVCFPlugin, followed by the LoadHapltoypesFromGVCFPlugin.
 *
 * This class is useful for general testing, e.g. when creating the developer smallSeq database.
 *
 * When running this plugin for smallSeq, a local folder to hold GVCF files should be mounted to the localGVCFDir
 * paramter.
 */
class PopulatePHGDBPipelinePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(PopulatePHGDBPipelinePlugin::class.java)

    private var gvcfServerPath = PluginParameter.Builder("gvcfServerPath",null,String::class.java)
        .description("semi-colon separated server and path, e.g. myserver.com;/path/to/gvcfs. This is the remote location where the created GVCFs will be stored for use with variant processing later.\nThis is needed for creating the key file used when loading the haplotypes.  This value will be stored in the db's genome_file_data table")
        .required(true)
        .build()

    private var myAssemblyMethod = PluginParameter.Builder("asmMethodName",null,String::class.java)
            .description("Method name for the Assembly Based haplotypes being uploaded.")
            .required(false)
            .build()

    private var myAssemblyKeyFile = PluginParameter.Builder("asmKeyFile",null,String::class.java)
            .description("Key File for Assembly haplotypes.")
            .inFile()
            .required(false)
            .build()

    private var myWgsMethod = PluginParameter.Builder("wgsMethodName",null,String::class.java)
            .description("Method name for the WGS Based haplotypes being uploaded.")
            .required(false)
            .build()

    private var myWgsKeyFile = PluginParameter.Builder("wgsKeyFile",null,String::class.java)
            .description("Key File for WGS haplotypes.")
            .inFile()
            .required(false)
            .build()

    private var myConsensusMethod = PluginParameter.Builder("consensusMethodName",null,String::class.java)
            .description("Method name for the Consensus being uploaded.")
            .required(false)
            .build()

    private var myInputConsensusHaplotypeMethod = PluginParameter.Builder("inputConsensusMethods", null, String::class.java)
            .description("Methods needed to populate a PHG object in order to create consensus on.  " +
                    "If left blank a method will be auto-generated based on asmMethodName and wgsMethodName.  " +
                    "If either are not set only one will be used.")
            .required(false)
            .build()

    private var myForceDBUpdate = PluginParameter.Builder("forceDBUpdate",false,Boolean::class.javaObjectType)
            .description("Parameter to force the DB to update.  If this is set to true and the DB is incompatible with the current PHG code, this will automatically update the DB if it can.")
            .required(false)
            .build()

    private var liquibaseDir = PluginParameter.Builder("liquibaseDir", null, String::class.java)
        .description("Liquibase release directory.  Holds liquibase executable and change logs.")
        .required(false)
        .inDir()
        .build()

    private var liquibaseOutputDir = PluginParameter.Builder("liquibaseOutdir", null, String::class.java)
            .description("The directory to which liquibase output files will be written.")
            .inDir()
            .required(true)
            .build()

    private var skipLiquibaseCheck = PluginParameter.Builder("skipLiquibaseCheck", false, Boolean::class.javaObjectType)
            .description("Should the liquibase check be skipped when liquibase is missing? " +
                    "Only set to true if the PHG DB version is known to match the software version.")
            .build()

    private var localGVCFFolder = PluginParameter.Builder("localGVCFFolder", null, String::class.java)
        .description("Folder where ref and assembly gvcfs are stored.  If non-null, the ref and assembly gvcfs will be copied to the indicated local download folder for use when creating consensus haplotypes.")
        .guiName("Local GVCF Folder")
        .inDir()
        .required(false)
        .build()

    override fun postProcessParameters() {
        // if no liquibase directory is specified, set the value from
        // defineDefaultLiquibaseDir()
        if (liquibaseDir.value() == null) {
            liquibaseDir(defineDefaultLiquibaseDir()) // function defined in LiquibaseUpdatePlugin
        }
    }

    override fun processData(input: DataSet?): DataSet? {

        //Need to check to see if we need to update liquibase
        //check if liquibaseDir exists.
        myLogger.info("Checking if Liquibase can be run.")

        if (liquibaseDir.value() == null || !Files.exists(Paths.get("${liquibaseDir()}/changelogs"))) {
            if (skipLiquibaseCheck()) {
                myLogger.warn("Liquibase changelogs not present. Liquibase check was skipped.")
                // TODO: Investigate this - I think it might be the source of the JVM forking error I've been seeing?
                //System.exit(11)
            }
            else throw IllegalArgumentException("Liquibase not found: You must run liquibase manually or from the phg_liquibase docker.")
        } else {

            // Then I run LiquibaseCommandPlugin with 'status' as the command.
            val status = runLiquibaseCommand("status",liquibaseOutputDir(), liquibaseDir())
            if(status == "UP_TO_DATE") {
                // If it returns back 'UP_TO_DATE' I can proceed without any issue.
                myLogger.info("PHG DB is up to date.  Proceeding with Populating the PHG DB.")
            }
            else if(status.startsWith("NEEDS_UPDATING")) {
                // If it returns back NEEDS_UPDATING,
                // if 'force' command
                if(forceDBUpdate()) {
                    // run LiquibaseCommandPlugin with an 'update' command and log that we are updating.
                    runLiquibaseCommand("update",liquibaseOutputDir(), liquibaseDir())
                }
                else  {
                    throw IllegalStateException("The Current PHG System is incompatible with the provided PHG Database.\n" +
                            "To Run with the existing DB, please use PHG/docker version: ${status.substringAfter("NEEDS_UPDATING:")}\n" +
                            "If you would like the PHG DB to be automatically updated please rerun this Plugin with -forceDBUpdate true\n")
                }
            }
            else {
                myLogger.error("Unknown status from liquibase.  Status Message: ${status}")
                throw IllegalStateException("Unknown status from liquibase.  Status Message: ${status}")
            }

        }

        //Steps for CreateHaplotypes scripts.
        if(assemblyKeyFile() != null) {
            // run anchorwave plugins
            myLogger.info("Creating ASM Haplotypes using anchorwave.")
            createASMAnchorWaveHaplotypes(gvcfServerPath())
        }

        if(wgsKeyFile() != null) {
            myLogger.info("Creating WGS Haplotypes.")
            createWGSHaplotypes()
        }

        if (localGVCFFolder() != null) {
            // copy the gvcf files to the localGVCFFolder
            copyGvcfsToLocalDir(localGVCFFolder()!!)
        }
        myLogger.info("Creating consensus.")
        createConsensus()

        return null
    }

    /**
     * Function to create Assembly based Haplotypes.
     */
    fun createASMHaplotypes() {
        //Need to make an object to setConfigParameters()
        myLogger.info("running createASMHaplotypes()")
        val assemblyPlugin = AssemblyHaplotypesMultiThreadPlugin(null, false)
        assemblyPlugin.setConfigParameters()
        assemblyPlugin.assemblyMethod(assemblyMethod()?:"mummer4")
                .keyFile(assemblyKeyFile()!!)
                .performFunction(null)
    }

    /**
     * Function to create WGS based Haplotypes.
     */
    fun createWGSHaplotypes() {
        val configFile = ParameterCache.value("configFile").get()
        // TODO: Better solution - the Groovy script cannot find classes from other Groovy scripts when called with an absolute path
        val relativeOrAbsolute = if (System.getenv("DB_HOST") != null) "" else "."
        val groovyErrorLog = "$relativeOrAbsolute/groovy.error"
        val groovyOutputLog = "$relativeOrAbsolute/groovy.output"
        var process = ProcessBuilder(listOf("./CreateHaplotypesFromFastq.groovy","-config",configFile))
                .redirectError(File(groovyErrorLog)) // NOTE: Inheriting IO does not work - related to log messages not appearing?
                .redirectOutput(File(groovyOutputLog))
                .directory(File("${System.getenv("BITBUCKET_CLONE_DIR") ?: "."}/docker/buildFiles/scripts/groovy")) // So that other scripts are found
                .start()
        val error = process.waitFor()
        if (error != 0) {
            //myLogger.error("Error creating WGS Haplotypes")
            //throw IllegalStateException("Error creating WGS Haplotypes")

            // TODO: Fix this yucky hack
            val groovyErrorReader: BufferedReader = File(groovyErrorLog).bufferedReader()
            val groovyErrorContents = groovyErrorReader.use { it.readText() }
            throw IllegalStateException("\nError creating WGS Haplotypes\nGroovy Error:\n$groovyErrorContents\n")
        }
    }

    /**
     * Function to run createConsensus.  If the user does not specify an inputConsensusHaplotypeMethod, we try to auto generate one.
     */
    fun createConsensus() {
        //first check whether consensus should be run
        //consensus will not be run if the consensus method is null or if it starts with **
        val consensusMethodName = consensusMethod()
        if (consensusMethodName == null) {
            myLogger.info("Consensus will not be run because no consensus method name was assigned.")
            return
        }
        if (consensusMethodName.startsWith("**")) {
            myLogger.info("Consensus will not be run because the consensus method started with **.")
            return
        }
        if (consensusMethodName == "null") {
            myLogger.info("Consensus will not be run because the consensus method name was 'null'.")
            return
        }

        //Need to see if the user supplied a graph input method.  If not try to get both ASM and WGS and use both if possible.
        val haplotypeMethod = if(inputConsensusHaplotypeMethod() == null) {
            if(assemblyMethod() == null && wgsMethod() == null) {
                throw IllegalStateException("Attempting to auto-generate a method to create a graph to run consensus on.  Both WGS and Assembly methods are missing.")
            }
            listOf(assemblyMethod(), wgsMethod())
                    .filter { it != "" } //Need the filter in case only assembly or only wgs was run.
                    .joinToString(":")
        }
        else {
            inputConsensusHaplotypeMethod()
        }

        //Create the graph
        val haplotypeGraphBuilderPlugin = HaplotypeGraphBuilderPlugin(null, false)
        haplotypeGraphBuilderPlugin.setConfigParameters() //Use this to automatically figure out config params
        val graphDS = haplotypeGraphBuilderPlugin.methods(haplotypeMethod)
                .includeVariantContexts(true)
                .includeSequences(true)
                .performFunction(null)

        //Run create Consensus
        val consensusPlugin = RunHapConsensusPipelinePlugin(null, false)
        consensusPlugin.setConfigParameters() //Use this to automatically figure out config params
        consensusPlugin.dbConfigFile(ParameterCache.value("configFile").get())
                .collapseMethod(consensusMethod())
                .performFunction(graphDS)
    }

    /**
     * function to create assembly haplotypes with anchorwave as the aligner.
     * It runs anchorwave, converts the anchorwave MAF files to GVCF Files,
     * then calls the LoadHaplotypesFromGVCFPlugin to load the assembly data.
     *
     * This also creates a gvcf key file with headers not included for the WGS
     * version but that are needed for loading the genome_file_data table
     * (only relevant for assemblies).
     */
    fun createASMAnchorWaveHaplotypes(gvcfServerPath:String) {
        myLogger.info("running createASMAnchorWaveHaplotypes()")
        createMAFfromAnchorwave()

        // create GVCF key file from mafKeyFile
        val mafKeyFile = ParameterCache.value("AssemblyMAFFromAnchorWavePlugin.keyFile").get()
        val mafPath = ParameterCache.value("AssemblyMAFFromAnchorWavePlugin.outputDir").get()

        val keyFiles = createGVCFKeyFile(mafKeyFile,mafPath, gvcfServerPath) // used mafKeyFile to create gvcfKeyFile
        // This version needs the keyfile with just .gvcf extension
        createGVCFFromMAF(keyFiles.getX())
        // This call needs the keyfile with .gvcf.gz as the extension
        loadASMHaplotypes(keyFiles.getY()) // almost identical key file, but gvcf file has gvcf.gz extension
    }

    /**
     * function to align the assemblies using the anchorwave proali program
     * This method requires the following parameters be defined in the config file
     *  (with appropriate values for the user data):
     *   AssemblyMAFFromAnchorWavePlugin.outputDir=
     *   AssemblyMAFFromAnchorWavePlugin.keyFile=
     *   AssemblyMAFFromAnchorWavePlugin.gffFile=
     *   AssemblyMAFFromAnchorWavePlugin.refFasta=
     *   AssemblyMAFFromAnchorWavePlugin.threadsPerRun= (optional)
     *   AssemblyMAFFromAnchorWavePlugin.numRuns= (optional)
     */
    fun createMAFfromAnchorwave() {

        myLogger.info("running createMAFfromAnchorwave()")
        val configFile = ParameterCache.value("configFile").get()
        ParameterCache.load(configFile)

        // The AssemblyMAFFromAncrhoWavePlugin takes a keyfile and processes all
        // files within it.  They are written to the outputDir defined in the config file for AssemblyMAFFromAnchorWavePlugin
        val mafFromAnchorWave = AssemblyMAFFromAnchorWavePlugin(null, false)
        mafFromAnchorWave.setConfigParameters()
        mafFromAnchorWave.performFunction(null)

    }

    /**
     * This function creates 2  key files based on maf key file values.
     *   The keyfile for MAFTOGVCF is based on the exisintg MAF KeyFile values, but
     *   with a few additional columns lacking from the WGS gvcf keyfiles, namely:
     *      mafFile, assemblyServerPath and assemblyLocalPath.
     * The assemblyServerPath and assemblyLocalPath data is used when populating the genome_file_data
     * table in the db.  The mafFile is used when determining the gvcf file name.
     *
     * The keyfile for loadASMHaplotypes is the same as the first, except laodASMHaplotypes has
     * file as *.gvcf.gz but MAFToGVCF only has .gvcf as extension.
     */
    fun createGVCFKeyFile(mafKeyFile:String, mafPath:String, gvcfServerPath:String): Tuple<String, String> {
        // From the mafKeyFIle, we need the AssemblyServerDir plus AssemblyGenomeFasta concatenated
        // to be the assemblyServerPath entry in the gvcf key file.
        // The AssemblyDir and AssemblyFasta entries are concatenated to become the assemblyLocalPath
        // entry for the gvcf key file
        // The gvcf key file "files" entry is the assembly fasta with ".fa" replaced by "gvcf"
        // The phasing values are set to "true" and the confidence is set to "0.9".  These may
        // not be the values the user wants.  If loading gvcf files is different, then the individual
        // steps should be run separately and not from this simplified pipeline.
        // The second keyfile created is for the loading the haplotypes.  Because MAFToGVCF will create
        // a bgzipped file, it needs the keyfile to show a file extension of .gvcf.gz

        myLogger.info("running createGVCFKeyFile()")
        // gvcf key file is written to same folder that holds the mafKeyFile
        val filePath = File(mafKeyFile).parent
        val gvcfKeyFile = "${filePath}/asmGVCF_keyfile.txt"
        val asmLoadKeyFile = "${filePath}/asmLoadHaplotypes_keyfile.txt"

        var colsAndData = readInKeyFile(mafKeyFile)
        val colNameMap = colsAndData.first
        val mafKeyFileLines = colsAndData.second
        val asmServerDir = colNameMap["AssemblyServerDir"]?:-1
        val assemblyGenomeFasta = colNameMap["AssemblyGenomeFasta"]?:-1
        val asmDir = colNameMap["AssemblyDir"]?:-1
        val assemblyFasta = colNameMap["AssemblyFasta"]?:-1
        val assemblyDBName = colNameMap["AssemblyDBName"]?:-1
        val descriptionCol = colNameMap["Description"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(asmServerDir != -1) { "Error processing keyfile.  Must have AssemblyServerDir column." }
        check(assemblyGenomeFasta != -1) { "Error processing keyfile.  Must have AssemblyGenomeFasta column." }
        check(asmDir != -1) { "Error processing keyfile.  Must have AssemblyDir column." }
        check(assemblyFasta != -1) { "Error processing keyfile.  Must have AssemblyFasta column." }
        check(assemblyDBName != -1) { "Error processing keyfile.  Must have AssemblyDBName column." }

        val headerLine = "type\tsample_name\tsample_description\tmafFile\tfiles\tchrPhased\tgenePhased\tphasingConf\tassemblyServerPath\tassemblyLocalPath\tgvcfServerPath\n"
        val gvcfKFsb = StringBuilder()
        val loadASMKFsb = StringBuilder()
        gvcfKFsb.append(headerLine)
        loadASMKFsb.append(headerLine)
        mafKeyFileLines.forEach { lineList ->

            val asmServerPath = lineList[asmServerDir] + "/" + lineList[assemblyGenomeFasta]
            val asmLocalPath = lineList[asmDir] + "/" + lineList[assemblyFasta]
            val sampleName = lineList[assemblyDBName]

            val currentDate = LocalDate.now().toString()
            var sampleDesc = "MAF and GVCF created from assembly fasta stored at ${asmServerPath}, with assembly processed locally from ${asmLocalPath} on ${currentDate}"
            if (descriptionCol != -1) {
                sampleDesc = lineList[descriptionCol]
            }
            val fastaFile = lineList[assemblyFasta]
            val suffixIdx = fastaFile.lastIndexOf(".")
            // the MAF file created in this pipeline via AssemblyMAFFromAnchorWavePlugin.kt has
            // the same name as the assembly, but the extension is changed to .maf from .fa/.fasta
            val mafFullPath = mafPath + "/" + fastaFile.substring(0,suffixIdx) + ".maf"
            val gvcfFileMAF = File(lineList[assemblyFasta]).nameWithoutExtension + ".gvcf"
            val gvcfFileLoadASM = File(lineList[assemblyFasta]).nameWithoutExtension + ".gvcf.gz"
            gvcfKFsb.append("GVCF\t${sampleName}\t${sampleDesc}\t${mafFullPath}\t${gvcfFileMAF}\ttrue\ttrue\t0.9\t${asmServerPath}\t${asmLocalPath}\t${gvcfServerPath}\n")
            loadASMKFsb.append("GVCF\t${sampleName}\t${sampleDesc}\t${mafFullPath}\t${gvcfFileLoadASM}\ttrue\ttrue\t0.9\t${asmServerPath}\t${asmLocalPath}\t${gvcfServerPath}\n")

        }
        Utils.getBufferedWriter(gvcfKeyFile).use { output ->
            output.write(gvcfKFsb.toString())
        }
        Utils.getBufferedWriter(asmLoadKeyFile).use { output ->
            output.write(loadASMKFsb.toString())
        }
        return Tuple<String,String>(gvcfKeyFile,asmLoadKeyFile)
    }

    /**
     * function to create GVCF Files from anchorwave MAF files.
     */
    fun createGVCFFromMAF(gvcfKeyFile: String) {
        myLogger.info("running createGVCFFromMAF()")

        // These must exist or the program will die
        val configFile = ParameterCache.value("configFile").get()
        ParameterCache.load(configFile)
        val gvcfOutputDir = ParameterCache.value("gvcfDir").get()

        val keyFileParsed = readInKeyFile(gvcfKeyFile)
        val headerMap = keyFileParsed.first
        val sampleNameIndex = headerMap["sample_name"]?:-1
        val sampleTypeIndex = headerMap["type"]?:-1
        val mafFileIndex = headerMap["mafFile"]?:-1
        val gvcfFileIndex = headerMap["files"]?:-1
        val asmServerIndex = headerMap["assemblyServerPath"]?:-1
        val asmLocalIndex = headerMap["assemblyLocalPath"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(asmServerIndex != -1) { "Error processing keyfile.  Must have assemblyServerPath column." }
        check(asmLocalIndex != -1) { "Error processing keyfile.  Must have assemblyLocalPath column." }
        check(sampleNameIndex != -1) { "Error processing keyfile.  Must have sample_name column." }
        check(sampleTypeIndex != -1) { "Error processing keyfile.  Must have type column." }
        check(mafFileIndex != -1) { "Error processing keyfile.  Must have mafFile column." }
        check(gvcfFileIndex != -1) { "Error processing keyfile.  Must have gvcfFile column." }

        val entries = keyFileParsed.second

        entries.forEach { lineList ->

            val mafFile = lineList[mafFileIndex]
            val sampleName = lineList[sampleNameIndex]
            val gvcfFile = gvcfOutputDir + "/" + lineList[gvcfFileIndex]
            if (lineList[sampleTypeIndex].toUpperCase().equals("GVCF")) {
                try {
                    val mafToGVCF = MAFToGVCFPlugin(null,false)
                    mafToGVCF.setConfigParameters()
                    mafToGVCF.sampleName(sampleName)
                        .gVCFOutput(gvcfFile)
                        .mAFFile(mafFile)
                        .performFunction(null)

                } catch (exc: Exception) {
                    throw IllegalStateException("Error creating GVCF from MAF:",exc)
                }
            }
        }
    }

    /**
     * function to load assembly haplotypes from GVCF files via LoadHaplotypesFromGVCFPlugin.
     */
    fun loadASMHaplotypes(gvcfKeyFile: String) {
        myLogger.info("running loadASMHaplotypes() with gvcfKeyFile=${gvcfKeyFile}")

        // Because we are not setting specific "LoadASMHaplotypes.gVCFDirectory" in the keyFile
        // Needs to be the same ref fasta as was used for creating the MAF
        val refFasta = ParameterCache.value("AssemblyMAFFromAnchorWavePlugin.refFasta").get()

        val methodName = "anchorwave_assembly"
        // When there are spaces in the methodDesc below, ProcessBuilder took them as separate
        // commands and failed.
        val methodDesc = "assemblies_aligned_to_reference_from_anchorwave_proali"

        try {
            println("loadASMHaplotypes: calling LoadHaplotypesFromGVCFPlugin with gvcfKeyFile=${gvcfKeyFile}")
            val loadHaps = LoadHaplotypesFromGVCFPlugin(null,false)
            loadHaps.setConfigParameters()
            loadHaps.wGSKeyFile(gvcfKeyFile)
                .reference(refFasta)
                .method(methodName)
                .methodDescription(methodDesc)
                .performFunction(null)

        } catch (exc: Exception) {
            throw IllegalStateException("Error loading haplotypes to DB:",exc)
        }
    }

    /**
     * Function to take the gvcf files used for storing assemblies and wgs,
     * and copy them to the folder used for downloading gvcf files from the server.
     * It is expected that when users run each step of the pipeline separately,
     * they will download the files needed.  When running the steps through
     * PopulatePHGDBPipelinePlugin, the code will need to download these files
     * before consensus can be run (e.g. for smallSeq)
     */
    fun copyGvcfsToLocalDir(localDownloadDir:String) {
        val configFile = ParameterCache.value("configFile").get()
        ParameterCache.load(configFile)
        val gvcfOutputDir = ParameterCache.value("gvcfDir").get()
        val gvcfList = mutableListOf<String>()

        try {
            // get the list of files
            // This should get both the gvcf.gz and gvcf.gz.tbix files
            Files.walk(Paths.get(gvcfOutputDir)).use {
                    paths -> paths.filter { Files.isRegularFile(it) }
                .forEach { gvcfList.add(it.toString()) }
            }

            // copy the files to the download folder
            gvcfList.stream().forEach { srcFile ->
                // String justName = new File(serverPath.getY()).getName();
                var justFileName = File(srcFile).getName()
                var destFilePath = "${localDownloadDir}/${justFileName}"
                FileUtils.copyFile(File(srcFile),File(destFilePath))
            }
        } catch (exc:Exception) {
            throw IllegalStateException("PopulatePHGDBPipelinePlugin:copyGvcfsToLocalDir - exception copying gvcf files to ${localGVCFFolder}: ${exc.message}")
        }



    }
    override fun getIcon(): ImageIcon? {
        val imageURL = PopulatePHGDBPipelinePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MakePHGDBPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to create and poplulate the DB and Run Consensus."
    }

    /**
     * semi-colon separated server and path, e.g. myserver.com;/path/to/gvcfs.
     * If no semi-colon and only the path is present, the
     * gvcf files will be copied to that path on the the current
     * server.
     * .
     *
     * @return Gvcf Server Path
     */
    fun gvcfServerPath(): String {
        return gvcfServerPath.value()
    }

    /**
     * Set Gvcf Server Path. semi-colon separated server and
     * path, e.g. myserver.com;/path/to/gvcfs. If no semi-colon
     * and only the path is present, the gvcf files will be
     * copied to that path on the the current server.
     * .
     *
     * @param value Gvcf Server Path
     *
     * @return this plugin
     */
    fun gvcfServerPath(value: String): PopulatePHGDBPipelinePlugin {
        gvcfServerPath = PluginParameter<String>(gvcfServerPath, value)
        return this
    }

    /**
     * Method name for the Assembly Based haplotypes being
     * uploaded.
     *
     * @return Asm Method Name
     */
    fun assemblyMethod(): String? {
        return myAssemblyMethod.value()
    }

    /**
     * Set Asm Method Name. Method name for the Assembly Based
     * haplotypes being uploaded.
     *
     * @param value Asm Method Name
     *
     * @return this plugin
     */
    fun assemblyMethod(value: String): PopulatePHGDBPipelinePlugin {
        myAssemblyMethod = PluginParameter<String>(myAssemblyMethod, value)
        return this
    }

    /**
     * Key File for Assembly haplotypes.
     *
     * @return Asm Key File
     */
    fun assemblyKeyFile(): String? {
        return myAssemblyKeyFile.value()
    }

    /**
     * Set Asm Key File. Key File for Assembly haplotypes.
     *
     * @param value Asm Key File
     *
     * @return this plugin
     */
    fun assemblyKeyFile(value: String): PopulatePHGDBPipelinePlugin {
        myAssemblyKeyFile = PluginParameter<String>(myAssemblyKeyFile, value)
        return this
    }

    /**
     * Method name for the WGS Based haplotypes being uploaded.
     *
     * @return Wgs Method Name
     */
    fun wgsMethod(): String? {
        return myWgsMethod.value()
    }

    /**
     * Set Wgs Method Name. Method name for the WGS Based
     * haplotypes being uploaded.
     *
     * @param value Wgs Method Name
     *
     * @return this plugin
     */
    fun wgsMethod(value: String): PopulatePHGDBPipelinePlugin {
        myWgsMethod = PluginParameter<String>(myWgsMethod, value)
        return this
    }

    /**
     * Key File for WGS haplotypes.
     *
     * @return Wgs Key File
     */
    fun wgsKeyFile(): String? {
        return myWgsKeyFile.value()
    }

    /**
     * Set Wgs Key File. Key File for WGS haplotypes.
     *
     * @param value Wgs Key File
     *
     * @return this plugin
     */
    fun wgsKeyFile(value: String): PopulatePHGDBPipelinePlugin {
        myWgsKeyFile = PluginParameter<String>(myWgsKeyFile, value)
        return this
    }

    /**
     * Method name for the Consensus being uploaded.
     *
     * @return Consensus Method Name
     */
    fun consensusMethod(): String? {
        return myConsensusMethod.value()
    }

    /**
     * Set Consensus Method Name. Method name for the Consensus
     * being uploaded.
     *
     * @param value Consensus Method Name
     *
     * @return this plugin
     */
    fun consensusMethod(value: String): PopulatePHGDBPipelinePlugin {
        myConsensusMethod = PluginParameter<String>(myConsensusMethod, value)
        return this
    }

    /**
     * Methods needed to populate a PHG object in order to
     * create consensus on.  If left blank a method will be
     * auto-generated based on asmMethodName and wgsMethodName.
     *  If either are not set only one will be used.
     *
     * @return Input Consensus Methods
     */
    fun inputConsensusHaplotypeMethod(): String? {
        return myInputConsensusHaplotypeMethod.value()
    }

    /**
     * Set Input Consensus Methods. Methods needed to populate
     * a PHG object in order to create consensus on.  If left
     * blank a method will be auto-generated based on asmMethodName
     * and wgsMethodName.  If either are not set only one
     * will be used.
     *
     * @param value Input Consensus Methods
     *
     * @return this plugin
     */
    fun inputConsensusHaplotypeMethod(value: String): PopulatePHGDBPipelinePlugin {
        myInputConsensusHaplotypeMethod = PluginParameter<String>(myInputConsensusHaplotypeMethod, value)
        return this
    }

    /**
     * Parameter to force the DB to update.  If this is set
     * to true and the DB is incompatible with the current
     * PHG code, this will automatically update the DB if
     * it can.
     *
     * @return Force D B Update
     */
    fun forceDBUpdate(): Boolean {
        return myForceDBUpdate.value()
    }

    /**
     * Set Force D B Update. Parameter to force the DB to
     * update.  If this is set to true and the DB is incompatible
     * with the current PHG code, this will automatically
     * update the DB if it can.
     *
     * @param value Force D B Update
     *
     * @return this plugin
     */
    fun forceDBUpdate(value: Boolean): PopulatePHGDBPipelinePlugin {
        myForceDBUpdate = PluginParameter<Boolean>(myForceDBUpdate, value)
        return this
    }

    /**
     * The directory to which liquibase output files will
     * be written.
     *
     * @return Liquibase Outdir
     */
    fun liquibaseOutputDir(): String {
        return liquibaseOutputDir.value()
    }

    /**
     * Set Liquibase Outdir. The directory to which liquibase
     * output files will be written.
     *
     * @param value Liquibase Outdir
     *
     * @return this plugin
     */
    fun liquibaseOutputDir(value: String): PopulatePHGDBPipelinePlugin {
        liquibaseOutputDir = PluginParameter<String>(liquibaseOutputDir, value)
        return this
    }


    /**
     * Should the liquibase check be skipped? Do not set to
     * true if running inside a Docker. Then, only set to
     * true if the PHG DB version is known to match the software
     * version.
     *
     * @return Skip Liquibase Check
     */
    fun skipLiquibaseCheck(): Boolean {
        return skipLiquibaseCheck.value()
    }

    /**
     * Set Skip Liquibase Check. Should the liquibase check
     * be skipped? Do not set to true if running inside a
     * Docker. Then, only set to true if the PHG DB version
     * is known to match the software version.
     *
     * @param value Skip Liquibase Check
     *
     * @return this plugin
     */
    fun skipLiquibaseCheck(value: Boolean): PopulatePHGDBPipelinePlugin {
        skipLiquibaseCheck = PluginParameter<Boolean>(skipLiquibaseCheck, value)
        return this
    }

    /**
     * Folder where ref and assembly gvcfs are stored.  If
     * non-null, the ref and assembly gvcfs will be copied
     * to the indicated local download folder for use when
     * creating consensus haplotypes.
     *
     * @return Local GVCF Folder
     */
    fun localGVCFFolder(): String? {
        return localGVCFFolder.value()
    }

    /**
     * Set Local GVCF Folder. Folder where ref and assembly
     * gvcfs are stored.  If non-null, the ref and assembly
     * gvcfs will be copied to the indicated local download
     * folder for use when creating consensus haplotypes.
     *
     * @param value Local GVCF Folder
     *
     * @return this plugin
     */
    fun localGVCFFolder(value: String): PopulatePHGDBPipelinePlugin {
        localGVCFFolder = PluginParameter<String>(localGVCFFolder, value)
        return this
    }

    /**
     * Liquibase release directory.  HOlds liquibase executable
     * and change logs.
     *
     * @return Liquibase Dir
     */
    fun liquibaseDir(): String {
        return liquibaseDir.value()
    }

    /**
     * Set Liquibase Dir. Liquibase release directory.  HOlds
     * liquibase executable and change logs.
     *
     * @param value Liquibase Dir
     *
     * @return this plugin
     */
    fun liquibaseDir(value: String?): PopulatePHGDBPipelinePlugin {
        liquibaseDir = PluginParameter<String>(liquibaseDir, value)
        return this
    }


}

fun main() {
    GeneratePluginCode.generateKotlin(PopulatePHGDBPipelinePlugin::class.java)
}