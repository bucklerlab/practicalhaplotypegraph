## Integration_Test Folder

The Integration_Test folder contains integration test scripts, associated config files, and scripts for visualizing or summarizing test results.

### Writing an Integration Test

##### Guidelines
1. A test should be organized and contain enough comments that it can be run by users other than the author.
2. Script and config file comments should indicate the purpose of the test, which parameters can or should be set by the users, which parameters should not be changed, and suggestions for interpreting the results.
3. Where possible set parameters in the config file rather than as command line options. This avoids requiring the user to modify parameters in two places and makes using the test simpler.
4. Excessive commenting is much better than sparse commenting and is encouraged.