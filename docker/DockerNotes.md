README

###Prerequisites:
* Install Docker
* Have a copy of phg saved as phg.jar
* Have a copy of picard saved as picard.jar
* Save the Dockerfile to the same directory

###Instructions
To build the docker
>docker build -t bucklerlab_phg .


This will download and install all the necessary software to run PHG and make an image on your machine called bucklerlab_phg

Use the following to make sure the image is installed:
>docker images

To Run The docker
>docker run -i -t bucklerlab_phg:latest

Which will load up the image and bring the current terminal inside of the docker
* TODO have multiple PHG scripts set up to run from outside of the docker

When done, you should delete the image/delete the containers it ran on:
>docker rmi bucklerlab_phg

This will often throw an error saying that you cannot remove the image without removing the container.  It will tell you what container is causing the problem:
>docker rm <containerId>

To make it easier to remove(when testing) run using the following command(adding --rm)
>docker run --rm -i -t bucklerlab_phg:latest
* This will delete the container when you type exit

To Run with an external mount point do the following:
>docker run --name test_container --rm -v /localMachineDir/:/tempFileDir/data/ -i -t bucklerlab_phg:latest

To Run a script from outside of the docker just append the scripts name and arguements to the previous command(For Example):
>docker run --name test_container --rm -v /Volumes/ZackBackup/Temp/Pangenome/DockerTests/InputFiles/SmallSeqTestFiles/:/tempFileDir/data/ -i -t bucklerlab_phg:latest ./CreateHaplotypePipeline.sh LineA single LineA_R1.fastq,LineA1_R1.fastq



Other Information:
To Export the docker image to use on another machine:
  * Build the docker so it shows up in the images list
  * Run the following command:
>docker save --output phgExport.tar bucklerlab_phg:latest

  * This will save the image's state to phgExport.tar, I do not think the container's state is stored as well
	 * Could be used to distribute the PHG
	
To Import a saved docker image:
* Run the following:
>docker load -- input ./phgExport.tar
* You can then run the docker using docker run.


On CBSU:
* You can either build and deploy or use a saved image to cbsu:
* Only difference is that instead of using docker you have to use docker1:
>docker1 run -i -t bucklerlab_phg:latest
* You should probably also make sure you clean up the images/containers as I believe they are shared across all users on the machine.