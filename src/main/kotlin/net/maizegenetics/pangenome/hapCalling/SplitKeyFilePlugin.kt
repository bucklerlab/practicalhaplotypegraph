package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

class SplitKeyFilePlugin (parentFrame: Frame?=null, isInteractive: Boolean=false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(SplitKeyFilePlugin::class.java)

    private var inputKeyFile = PluginParameter.Builder("inputKeyFile", null, String::class.java)
        .guiName("Input Key File")
        .inFile()
        .required(true)
        .description("Name of the inputKeyFile file to process.")
        .build()

    private var outputKeyFile = PluginParameter.Builder("outputKeyFile", null, String::class.java)
        .guiName("Output KeyFile")
        .outFile()
        .required(true)
        .description("Name of the output file to process.")
        .build()

    private var fileName = PluginParameter.Builder("fileName", null, String::class.java)
        .guiName("File Name")
        .required(true)
        .description("Name from the SAM or Fastq file to know which key file record to output.  This will look at the filename column")
        .build()

    private var outputFiltered = PluginParameter.Builder("outputFilteredName", false, Boolean::class.javaObjectType)
        .guiName("Output Filtered File Name")
        .required(false)
        .description("If set to true, output the filtered name expected by the snakemake script.")
        .build()

    private var removeLastToken = PluginParameter.Builder("removeLastTokenFromFileName", true, Boolean::class.javaObjectType)
        .guiName("Remove Last Token From FileName")
        .required(false)
        .description("If set to true, this will remove the last token from the file names.")
        .build()

    override fun processData(input: DataSet?): DataSet? {

        val (headerMap, records) = readInKeyFile(inputKeyFile())

        val fileNameCol = headerMap["filename"]?: -1

        check(fileNameCol != -1) { "Error processing keyfile.  Must have filename column." }

        val fileObj = File(fileName())

        val fileNameWithoutPath = fileObj.name

        val recordToExport = records.filter { it[fileNameCol] == fileNameWithoutPath }

        check(recordToExport.isNotEmpty()) {"Unable to Find the requested fastq/SAM file in the key file."}

        check(recordToExport.size < 2) { "Too many records in the key file for the requested Fastq/SAM file" }

        Utils.getBufferedWriter(outputKeyFile()).use { output ->
            val headerList = headerMap.map { Pair(it.key,it.value) }
                                        .filter { it.first != "filename" && it.first != "filename2" }
                                        .sortedBy { it.second }
                                        .map { it.first }
            val headerString = headerList.joinToString("\t")
            output.write("${headerString}\tfilename\n")

            val outputFileName = fileObj.nameWithoutExtension
            val recordString = headerList.map { headerMap[it] ?: -1 }
                .joinToString("\t") { if (it == -1) "" else recordToExport.first()[it] }

            val finalOutputFileName = if(removeLastToken()) outputFileName.substringBeforeLast("_") else outputFileName
            output.write("${recordString}\t${finalOutputFileName}${if(outputFiltered()) "_filtered" else ""}.bam\n")
        }


        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = SplitKeyFilePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "SplitKeyFilePlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to make a key file with only the requested record. "
    }

    /**
     * Name of the inputKeyFile file to process.
     *
     * @return Input Key File
     */
    fun inputKeyFile(): String {
        return inputKeyFile.value()
    }

    /**
     * Set Input Key File. Name of the inputKeyFile file to
     * process.
     *
     * @param value Input Key File
     *
     * @return this plugin
     */
    fun inputKeyFile(value: String): SplitKeyFilePlugin {
        inputKeyFile = PluginParameter<String>(inputKeyFile, value)
        return this
    }

    /**
     * Name of the output file to process.
     *
     * @return Output KeyFile
     */
    fun outputKeyFile(): String {
        return outputKeyFile.value()
    }

    /**
     * Set Output KeyFile. Name of the output file to process.
     *
     * @param value Output KeyFile
     *
     * @return this plugin
     */
    fun outputKeyFile(value: String): SplitKeyFilePlugin {
        outputKeyFile = PluginParameter<String>(outputKeyFile, value)
        return this
    }

    /**
     * Name from the SAM or Fastq file to know which key file
     * record to output.  This will look at the filename column
     *
     * @return File Name
     */
    fun fileName(): String {
        return fileName.value()
    }

    /**
     * Set File Name. Name from the SAM or Fastq file to know
     * which key file record to output.  This will look at
     * the filename column
     *
     * @param value File Name
     *
     * @return this plugin
     */
    fun fileName(value: String): SplitKeyFilePlugin {
        fileName = PluginParameter<String>(fileName, value)
        return this
    }

    /**
     * If set to true, output the filtered name expected by
     * the snakemake script.
     *
     * @return Output Filtered File Name
     */
    fun outputFiltered(): Boolean {
        return outputFiltered.value()
    }

    /**
     * Set Output Filtered File Name. If set to true, output
     * the filtered name expected by the snakemake script.
     *
     * @param value Output Filtered File Name
     *
     * @return this plugin
     */
    fun outputFiltered(value: Boolean): SplitKeyFilePlugin {
        outputFiltered = PluginParameter<Boolean>(outputFiltered, value)
        return this
    }

    /**
     * If set to true, this will remove the last token from
     * the file names.
     *
     * @return Remove Last Token From FileName
     */
    fun removeLastToken(): Boolean {
        return removeLastToken.value()
    }

    /**
     * Set Remove Last Token From FileName. If set to true,
     * this will remove the last token from the file names.
     *
     * @param value Remove Last Token From FileName
     *
     * @return this plugin
     */
    fun removeLastToken(value: Boolean): SplitKeyFilePlugin {
        removeLastToken = PluginParameter<Boolean>(removeLastToken, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(SplitKeyFilePlugin::class.java)
}