package net.maizegenetics.pangenome.db_updateDelete

import net.maizegenetics.analysis.filter.FilterTaxaBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesMultiThreadPlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import javax.swing.ImageIcon


/**
 * This class will delete paths based on a method name, a taxa list, or both.
 * A methodName is required.  Taxa is not required, but may be specified by either
 * the "cultivar" column in a keyFile, or from a taxaList.  User cannot specify both
 * a taxa list and a keyfile.
 *
 * Entries are deleted from both the paths and the read_mapping_paths tables
 * for the identified entries.
 *
 * The specified path method will be deleted if the user specifies "true" for parameter "deleteMethod"
 *
 * Assumptions:  user has loaded the ParmeterCache with config file connection values.
 *
 * @author lcj34 March 1, 2021
 *
 */
class DeletePathsPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = LogManager.getLogger(DeletePathsPlugin::class.java)

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name assigned to read mappings that will be deleted.")
            .build()

    private var taxa = PluginParameter.Builder("taxa", null, TaxaList::class.java)
            .required(false)
            .description("Optional list of taxa whose paths will be deleted from the db paths table.  " +
                    "  This can be a comma separated list of taxa (no spaces unless surrounded by quotes), " +
                    "file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). " +
                    " Either keyFile or taxa must be specified, but not both")
            .build()

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("Key File")
            .required(false)
            .inFile()
            .description("KeyFile containing taxa that will be deleted. Either keyFile or taxa must be specified, but not both.")
            .build()

    private var deleteMethod = PluginParameter.Builder("deleteMethod", false, Boolean::class.javaObjectType)
            .required(false)
            .description("Whether to delete the path method associated with the readMappings.  default=false.")
            .build()

    override fun processData(input: DataSet?): DataSet? {

        if (taxa() != null && keyFile() != null) {
            throw IllegalArgumentException("Cannot specify both taxa and keyFile, please choose one parameter or the other.")
        }

        if ( methodName() == null) {
            throw IllegalArgumentException("DeletePathsPlugin:  methodName is required for this plugin")
        }

        // Get list of taxa names if parameter was not null
        var taxaNameList = mutableListOf<String>()
        if (taxa() != null) {
            for (taxon in taxa()!!) {
                taxaNameList.add(taxon.name)
            }
        }

        if (keyFile() != null) {
            val colsAndData = readInKeyFile(keyFile()!!)
            val colNameMap = colsAndData.first
            val cultivar = colNameMap["cultivar"]?:-1
            if (cultivar == -1) {
                throw IllegalArgumentException("keyFile is missing cultivar column")
            }
            val keyFileLines = colsAndData.second
            keyFileLines.forEach { lineList ->
                // Column names were checked for validity above
                val taxon = lineList[cultivar]
                taxaNameList.add(taxon)
            }

        }
        val dbConnect = DBLoadingUtils.connection(false)
        val phg = PHGdbAccess(dbConnect)

        myLogger.info("deleting paths with method name ${methodName()} and ${taxaNameList.size} taxa")
        phg.deletePaths(methodName(), taxaNameList)

        if (methodName() != null && deleteMethod() == true) {
            myLogger.info("Deleting method ${methodName()} from methods table")
            phg.deleteMethodByName(methodName())
        }

        phg.close()
        return null
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return ("Delete Paths")
    }

    override fun getToolTipText(): String {
        return ("Delete paths having the specified method name")
    }

    /**
     * Method name assigned to read mappings that will be
     * deleted.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name assigned to read mappings
     * that will be deleted.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): DeletePathsPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Optional list of taxa whose paths will be deleted from
     * the db paths table.    This can be a comma separated
     * list of taxa (no spaces unless surrounded by quotes),
     * file (.txt) with list of taxa names to include, or
     * a taxa list file (.json or .json.gz).  Either keyFile
     * or taxa must be specified, but not both
     *
     * @return Taxa
     */
    fun taxa(): TaxaList? {
        return taxa.value()
    }

    /**
     * Set Taxa. Optional list of taxa whose paths will be
     * deleted from the db paths table.    This can be a comma
     * separated list of taxa (no spaces unless surrounded
     * by quotes), file (.txt) with list of taxa names to
     * include, or a taxa list file (.json or .json.gz).
     * Either keyFile or taxa must be specified, but not both
     *
     * @param value Taxa
     *
     * @return this plugin
     */
    fun taxa(value: TaxaList?): DeletePathsPlugin {
        taxa = PluginParameter<TaxaList>(taxa, value)
        return this
    }

    /**
     * KeyFile containing taxa that will be deleted. Either
     * keyFile or taxa must be specified, but not both.
     *
     * @return Key File
     */
    fun keyFile(): String? {
        return keyFile.value()
    }

    /**
     * Set Key File. KeyFile containing taxa that will be
     * deleted. Either keyFile or taxa must be specified,
     * but not both.
     *
     * @param value Key File
     *
     * @return this plugin
     */
    fun keyFile(value: String?): DeletePathsPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Whether to delete the path method associated with the
     * readMappings.  default=false.
     *
     * @return Delete Method
     */
    fun deleteMethod(): Boolean {
        return deleteMethod.value()
    }

    /**
     * Set Delete Method. Whether to delete the path method
     * associated with the readMappings.  default=false.
     *
     * @param value Delete Method
     *
     * @return this plugin
     */
    fun deleteMethod(value: Boolean): DeletePathsPlugin {
        deleteMethod = PluginParameter<Boolean>(deleteMethod, value)
        return this
    }


}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(DeletePathsPlugin::class.java)
}