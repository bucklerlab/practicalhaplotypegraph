package net.maizegenetics.pangenome.gvcfFiltering;

import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.pangenome.api.FilterGraphPlugin;
import net.maizegenetics.pangenome.hapcollapse.GVCFTyperPlugin;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * Created by zrm22 on 10/30/17.
 *
 * Plugin to run the FilterGVCF process.  Will both filter the gvcfs and will export a fasta file which is used for DB uploading
 *
 * It is highly suggested to use the configFile as it provides more flexibility with the filtering process.
 *
 * The parameters for the configuration file are as follows:
 *
 * exclusionString = String used for bcftools filtering.  If this is specified, no additional terms are added to exclude
 * DP_poisson_min = minimum poisson bound used for filtering(absolute minimum is 0.0)
 * DP_poisson_max = maximum poisson bound used for filtering(absolute maximum is 1.0)
 * DP_min = minimum DP bound
 * DP_max = maximum DP bound
 * GQ_min = minimum GQ bound
 * GQ_max = maximum GQ bound
 * QUAL_min = base quality minimum bound
 * QUAL_max = base quality maximum bound
 * filterHets = true/false or t/f, if true will filter sites where 2 or more alleles have above 0 AD
 *
 * If any are left out, the plugin will just ignore that condition
 *
 * Using the exclusionString will allow for greater filtering control as you can specify any VCF annotation.
 */

/**
 * @deprecated  As of using Groovy scripts, replaced by FilterGVCFSingleFilePlugin
 */
@Deprecated
public class FilterGVCFPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(FilterGVCFPlugin.class);

    private PluginParameter<String> myRefFile = new PluginParameter.Builder<>("refFile", null, String.class)
            .description("Reference File used to create the GVCFs.")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myBedFile = new PluginParameter.Builder<>("bedFile", null, String.class)
            .description("Bed file containing the gene information.")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myGVCFInputDir = new PluginParameter.Builder<>("gvcfDir", null, String.class)
            .description("Directory holding the GVCF files to be filtered.")
            .inDir()
            .required(true)
            .build();

    private PluginParameter<String> myOutputDir = new PluginParameter.Builder<>("outputDir", null, String.class)
            .description("Directory to hold the output files.")
            .outDir()
            .required(true)
            .build();

    private PluginParameter<String> myConfigFile = new PluginParameter.Builder<>("configFile",null, String.class)
            .description("Config folder containing the filtering parameters.")
            .inFile()
            .build();

    private PluginParameter<Double> myLowerPoissonBound = new PluginParameter.Builder<>("lowerPoissonBound",0.0,Double.class)
            .description("Lower Poisson Bound used for filtering.")
            .build();

    private PluginParameter<Double> myUpperPoissonBound = new PluginParameter.Builder<>("upperPoissonBound",1.0,Double.class)
            .description("Upper Poisson Bound used for filtering.")
            .build();

    public FilterGVCFPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        myLogger.info("Loading up the Reference File.");
        GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(refFile());

        myLogger.info("Setting Poisson Bounds, if they are [0,1] nothing is done.");
        Tuple<Double,Double> poissonProbabilityBounds = new Tuple<>(lowerPoissonBound(),upperPoissonBound());
        //walk through the directory and filter and extract the fastas
        try (Stream<Path> paths = Files.walk(Paths.get(gVCFInputDir()))) {
            if(configFile()==null) {
                paths.filter(Files::isRegularFile).filter(file -> !file.toString().endsWith("DS_Store"))
                        .filter(file ->!file.toString().endsWith(".idx"))
                        .forEach((file) -> ComputeMedianGVCFAndFilter.filterAndExtractFasta(file,outputDir(),genomeSequence,bedFile(),poissonProbabilityBounds,false, "bcftools"));
            }
            else {
                //Else load up the properties object with whatever is in the config file
                Properties configProperties = new Properties();
                try {
                    configProperties.load(Utils.getBufferedReader(configFile()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                paths.filter(Files::isRegularFile).filter(file -> !file.toString().endsWith("DS_Store"))
                        .filter(file ->!file.toString().endsWith(".idx"))
                        .forEach((file) -> ComputeMedianGVCFAndFilter.filterAndExtractFasta(file,outputDir(),genomeSequence,bedFile(),configProperties,false, "bcftools"));
            }


        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Filter GVCFs";
    }

    @Override
    public String getToolTipText() {
        return "Filter GVCFs";
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // public static void main(String[] args) {
    //     GeneratePluginCode.generate(FilterGVCFPlugin.class);
    // }

    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * Reference File used to create the GVCFs.
     *
     * @return Ref File
     */
    public String refFile() {
        return myRefFile.value();
    }

    /**
     * Set Ref File. Reference File used to create the GVCFs.
     *
     * @param value Ref File
     *
     * @return this plugin
     */
    public FilterGVCFPlugin refFile(String value) {
        myRefFile = new PluginParameter<>(myRefFile, value);
        return this;
    }

    /**
     * Bed file containing the gene information.
     *
     * @return Bed File
     */
    public String bedFile() {
        return myBedFile.value();
    }

    /**
     * Set Bed File. Bed file containing the gene information.
     *
     * @param value Bed File
     *
     * @return this plugin
     */
    public FilterGVCFPlugin bedFile(String value) {
        myBedFile = new PluginParameter<>(myBedFile, value);
        return this;
    }

    /**
     * Directory holding the GVCF files to be filtered.
     *
     * @return Gvcf Dir
     */
    public String gVCFInputDir() {
        return myGVCFInputDir.value();
    }

    /**
     * Set Gvcf Dir. Directory holding the GVCF files to be
     * filtered.
     *
     * @param value Gvcf Dir
     *
     * @return this plugin
     */
    public FilterGVCFPlugin gVCFInputDir(String value) {
        myGVCFInputDir = new PluginParameter<>(myGVCFInputDir, value);
        return this;
    }

    /**
     * Directory to hold the output files.
     *
     * @return Output Dir
     */
    public String outputDir() {
        return myOutputDir.value();
    }

    /**
     * Set Output Dir. Directory to hold the output files.
     *
     * @param value Output Dir
     *
     * @return this plugin
     */
    public FilterGVCFPlugin outputDir(String value) {
        myOutputDir = new PluginParameter<>(myOutputDir, value);
        return this;
    }

    /**
     * Config folder containing the filtering parameters.
     *
     * @return Config File
     */
    public String configFile() {
        return myConfigFile.value();
    }

    /**
     * Set Config File. Config folder containing the filtering
     * parameters.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    public FilterGVCFPlugin configFile(String value) {
        myConfigFile = new PluginParameter<>(myConfigFile, value);
        return this;
    }

    /**
     * Lower Poisson Bound used for filtering.
     *
     * @return Lower Poisson Bound
     */
    public Double lowerPoissonBound() {
        return myLowerPoissonBound.value();
    }

    /**
     * Set Lower Poisson Bound. Lower Poisson Bound used for
     * filtering.
     *
     * @param value Lower Poisson Bound
     *
     * @return this plugin
     */
    public FilterGVCFPlugin lowerPoissonBound(Double value) {
        myLowerPoissonBound = new PluginParameter<>(myLowerPoissonBound, value);
        return this;
    }

    /**
     * Upper Poisson Bound used for filtering.
     *
     * @return Upper Poisson Bound
     */
    public Double upperPoissonBound() {
        return myUpperPoissonBound.value();
    }

    /**
     * Set Upper Poisson Bound. Upper Poisson Bound used for
     * filtering.
     *
     * @param value Upper Poisson Bound
     *
     * @return this plugin
     */
    public FilterGVCFPlugin upperPoissonBound(Double value) {
        myUpperPoissonBound = new PluginParameter<>(myUpperPoissonBound, value);
        return this;
    }

}
