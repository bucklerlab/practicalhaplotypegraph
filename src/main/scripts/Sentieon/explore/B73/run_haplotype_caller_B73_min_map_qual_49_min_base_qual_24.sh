#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/B73/4882_6665_17312_HBEN2ADXX_B73_GTTTCG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/B73/4882_6665_17312_HFFGKADXX_B73_GTTTCG_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/B73/B73_HL5WNCCXX_L5_renamed_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_ALLchr_genesExactCoordinates_chr8.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 49 --min_base_qual 24 \
   B73_min_map_qual_49_min_base_qual_24_haplotype_caller_output.g.vcf

