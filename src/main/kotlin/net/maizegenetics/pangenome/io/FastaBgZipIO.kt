@file:JvmName("FastaBgZipIO")

package net.maizegenetics.pangenome.io

import htsjdk.samtools.reference.*
import htsjdk.samtools.util.BlockCompressedOutputStream
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.io.*
import java.nio.file.Paths
import java.util.*

private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.io.FastaBgZipIO")

/**
 * Write the haplotype graph to a fasta file.
 * The file is bgzip compressed.
 *
 * @param graph the graph to write
 * @param filename the name of the file to write (.fa.gz will be added if not present)
 *
 * @return the name of the file written
 */
fun writeFastaBgZip(graph: HaplotypeGraph, filename: String?): String? {

    var startTime = System.nanoTime()

    val file = Utils.addSuffixIfNeeded(filename, ".fa.gz")

    myLogger.info("Writing graph sequences to fasta file: $file")

    try {

        // compression level 2 for better speed without much cost in file size
        BufferedWriter(OutputStreamWriter(BlockCompressedOutputStream(File(file), 2))).use { writer ->
            graph.nodeStream()
                .forEach { node: HaplotypeNode ->
                    try {
                        writer.write(">")
                        writer.write(node.id().toString())
                        writer.write("\n")
                        writer.write(node.haplotypeSequence().sequence())
                        writer.write("\n")
                    } catch (e: Exception) {
                        myLogger.debug(e.message, e)
                        throw IllegalStateException(
                            """
                                FastaBgZipIO: writeFasta: problem writing: $file
                                ${e.message}
                                """.trimIndent()
                        )
                    }
                }
            myLogger.info("writeFastaBgZip: wrote file: $file")
        }

    } catch (e: Exception) {
        myLogger.debug(e.message, e)
        throw IllegalStateException(
            """
                FastaBgZipIO: writeFastaBgZip: problem writing: $file
                ${e.message}
                """.trimIndent()
        )
    }

    myLogger.info("writeFastaBgZip: time to write ${file}: ${(System.nanoTime() - startTime) / 1e9} seconds")

    startTime = System.nanoTime()

    // fasta index file name adds .fai
    FastaSequenceIndexCreator.create(Paths.get(file), true)

    myLogger.info("writeFastaBgZip: time to write $file.fai: ${(System.nanoTime() - startTime) / 1e9} seconds")

    return file

}

/**
 * Read a bgzipped fasta file into a map of hapid to sequence.
 */
fun readFastaBgZip(filename: String): Int2ObjectOpenHashMap<String> {

    try {

        Utils.getBufferedReader(filename).use { reader ->

            val result = Int2ObjectOpenHashMap<String>()

            var currentID: String? = null
            var line = reader.readLine()
            var sequence = false
            while (line != null) {
                line = line.trim { it <= ' ' }
                if (line.startsWith(";")) {
                    line = reader.readLine()
                } else if (line.startsWith(">")) {
                    currentID = line.substring(1).trim { it <= ' ' }
                    sequence = true
                    line = reader.readLine()
                } else if (sequence) {
                    val builder = StringBuilder()
                    while (line != null && !line.startsWith(">") && !line.startsWith(";")) {
                        line = line.trim { it <= ' ' }.uppercase()
                        builder.append(line)
                        line = reader.readLine()
                    }
                    result[currentID!!.toInt()] = builder.toString()
                    sequence = false
                    currentID = null
                } else {
                    myLogger.error("readFasta: file: $filename invalid format. Line: $line")
                    throw IllegalArgumentException("GraphIO: readFasta: invalid format.")
                }
            }

            return result

        }

    } catch (e: java.lang.Exception) {
        myLogger.error(e.message, e)
        throw IllegalStateException(
            """
            GraphIO: readFasta: problem reading: $filename
            ${e.message}
            """.trimIndent()
        )
    }

    // This may be useful in the future if we use the index file .fai
    // return BlockCompressedIndexedFastaSequenceFile(Paths.get(filename)) //, FastaSequenceIndex(Paths.get("$filename.fai")))

}