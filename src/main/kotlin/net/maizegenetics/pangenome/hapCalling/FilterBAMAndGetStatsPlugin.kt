package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Multimap
import htsjdk.samtools.*
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.tree.read
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.BufferedWriter
import java.io.File
import javax.swing.ImageIcon

/**
 * Plugin to Filter out the BAMs and build a stats file to get numbers of each of the CIGAR operators for each reference range as well as how many reads hit each one.
 */
class FilterBAMAndGetStatsPlugin (parentFrame: Frame?=null, isInteractive: Boolean=false) : AbstractPlugin(parentFrame, isInteractive) {

    data class RefRangeStats(val refRangeId: Int, var numReads: Int = 0 ,var totalReadBps: Int = 0, var totalAlignedBps:Int = 0, var totalNM:Int = 0,
                             var totalSClip:Int = 0, var totalHClip:Int = 0, var totalEQ:Int = 0, var totalX:Int = 0,
                             var totalI:Int = 0, var totalD:Int = 0 )

    private val myLogger = LogManager.getLogger(MultisampleBAMToMappingPlugin::class.java)


    private var bamFile = PluginParameter.Builder("bamFile", null, String::class.java)
        .guiName("bamFile")
        .inFile()
        .required(true)
        .description("Name of the BAM file to process.  Also will support SAM Files")
        .build()

    private var outputBamFile = PluginParameter.Builder("outputBamFile", null, String::class.java)
        .guiName("Output Bam File")
        .outFile()
        .required(true)
        .description("Name of the  output filtered BAM/SAM file.")
        .build()

    private var outputStatsFile = PluginParameter.Builder("outputStatsFile", null, String::class.java)
        .guiName("Output Stats File")
        .outFile()
        .required(true)
        .description("Name of the  output stats file of RefRange to total counts.")
        .build()

    private var isPairedEnd = PluginParameter.Builder("isPairedEnd", false, Boolean::class.javaObjectType)
        .required(false)
        .description("Flag to set if you are using Paired End reads.")
        .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
        .required(false)
        .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
        .build()

    override fun processData(input: DataSet?): DataSet? {

        //Load in the graph to check reference Ranges

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("FilterBAMAndGetStatsPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("FilterBAMAndGetStatsPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph

        val hapIdToRefRangeMap = graph.referenceRanges().flatMap { graph.nodes(it) }.map { Pair(it.id(), it.referenceRange().id()) }.toMap()

        //Read In BAM file
        val samReader = loadSAMFileIntoSAMReader(bamFile())

        processSAMRecords(samReader, isPairedEnd(), hapIdToRefRangeMap, maxRefRangeError(), outputBamFile(), outputStatsFile())
        return null
    }

    /**
     * Function that processes the records within a SAM file similar to what is done in Minimap2Utils.
     * This will keep track of the alignment stats as it is going and will output the correct values.
     */
    fun processSAMRecords(samReader: SamReader, pairedEnd:Boolean, hapIdToRefRangeMap:Map<Int,Int>, maxRefRangeError : Double,
                          outputBamFile : String, outputStatsFile: String ) {
        val samIterator = samReader.iterator()
        var mappingCounter = 0
        var currentStoredReadName = ""
        var bestReadMap = mutableMapOf<Pair<String, Boolean>, MutableList<SAMRecord>>()
        SAMFileWriterFactory().makeBAMWriter(samReader.fileHeader,false,File(outputBamFile)).use { bamFileWriter ->

                val refRangeBPCounters = mutableMapOf<Int,RefRangeStats>()

                while (samIterator.hasNext()) {
                    val currentSamRecord = samIterator.next()

                    mappingCounter++
                    if (mappingCounter % 1000000 == 0) {
                        myLogger.info("Processed ${mappingCounter} alignments.")
                    }

                    if (filterRead(currentSamRecord, pairedEnd, false)) {
                        continue
                    }
                    val readName = currentSamRecord.readName
                    //Set the read name for the first mapping in the iterator
                    if (currentStoredReadName == "") {
                        currentStoredReadName = readName
                    }
                    if (readName != currentStoredReadName) {
                        val filteredRecords = keepHapIdsForSingleRefRange(bestReadMap, hapIdToRefRangeMap, maxRefRangeError)

                        addRefRangeStatsToMap(refRangeBPCounters, hapIdToRefRangeMap ,filteredRecords)

                        //write out the SAMRecords to the BAM/SAM file
                        outputRecords( filteredRecords, bamFileWriter)

                        //reset the map and current name
                        currentStoredReadName = readName //update the read name
                        bestReadMap = mutableMapOf() //reset the bestReadMap so we can add new entries to it
                    }

                    attemptToAddSAMRecordToBestRecordGroup(pairedEnd, currentSamRecord, readName, bestReadMap)
                }
                writeRefRangeStatsToFile(outputStatsFile, refRangeBPCounters)


        }

    }


    /**
     * Function to write out the reference range stats to a file
     */
    private fun writeRefRangeStatsToFile(outputStatsFile: String, refRangeBPCounters: MutableMap<Int, RefRangeStats>) {
        Utils.getBufferedWriter(outputStatsFile).use { outputFileWriter ->
            outputFileWriter.write("refRangeId\ttotalReads\ttotalReadBps\ttotalAlignmentBps\ttotalNM\ttotalS\ttotalH\ttotalEQ\ttotalX\ttotalI\ttotalD\n")

            for((_,stats) in refRangeBPCounters) {
                outputFileWriter.write("${stats.refRangeId}\t${stats.numReads}\t${stats.totalReadBps}\t${stats.totalAlignedBps}\t${stats.totalNM}\t${stats.totalSClip}\t" +
                        "${stats.totalHClip}\t${stats.totalEQ}\t${stats.totalX}\t${stats.totalI}\t${stats.totalD}\n")
            }
        }
    }

    /**
     * Function to try to add the SAM Records to the current best Record group.
     *
     * This differs from Minimap2Utils.attemptToAddSAMRecordToBestReadMap because we do not need to keep track of as much information if we are just doing the filtering.
     * We also need to know the original SAM record so we can output it into a new SAM file.
     * With attemptToAddSAMRecordToBestReadMap, the original record is lost as we only keep track of the aggregate information.
     */
    private fun attemptToAddSAMRecordToBestRecordGroup(pairedEnd: Boolean, currentSamRecord: SAMRecord, readName: String, bestReadMap: MutableMap<Pair<String, Boolean>, MutableList<SAMRecord>>) {
        val isNegStrand = if (!pairedEnd) pairedEnd else currentSamRecord.readNegativeStrandFlag
        val bestReadMapKey = Pair(readName, isNegStrand)

        //Check to see if its in the map
        if (!bestReadMap.containsKey(bestReadMapKey)) {
            bestReadMap[bestReadMapKey] = mutableListOf(currentSamRecord)
        } else {
            //We have the key already in our map, so we need to see if the edit distance is lower
            val currentBestAlignmentGroup = bestReadMap[bestReadMapKey]

            val currentEditDistance = currentSamRecord.getIntegerAttribute("NM")
            val currentBestEditDistance = currentBestAlignmentGroup?.first()?.getIntegerAttribute("NM") ?: Int.MAX_VALUE
            if (currentEditDistance < currentBestEditDistance) {
                //Replace this group with the new mapping
                bestReadMap.replace(bestReadMapKey, mutableListOf(currentSamRecord))
            } else if (currentEditDistance == currentBestEditDistance) {
                //Add this id to the list
                currentBestAlignmentGroup?.add(currentSamRecord)
            }

        }
    }


    /**
     * Function to extract all the alignment statistics from the SAM Record and output it into the correct data structure to output.
     */
    private fun addRefRangeStatsToMap(refRangeBpCounters: MutableMap<Int,RefRangeStats>, hapIdToRangeMap: Map<Int, Int>, filteredRecords: Map<Pair<String, Boolean>, List<SAMRecord>>) {
        for(entry in filteredRecords) {
            val refRangeId = hapIdToRangeMap[entry.value.first().contig.toInt()]?:-1

            if(refRangeId == -1) {
                entry.value.forEach { println("${it.readName}\t${it.contig}") }
            }
            //Check if refRange is already in map
            val currentRefRangeBpCounter = if(refRangeBpCounters.keys.contains(refRangeId)) {
                //get refRange
                 refRangeBpCounters[refRangeId]?:RefRangeStats(refRangeId)
            }
            else {
                RefRangeStats(refRangeId)
            }

            for(record in entry.value) {
                currentRefRangeBpCounter.numReads++
                currentRefRangeBpCounter.totalReadBps += record.readLength
                currentRefRangeBpCounter.totalAlignedBps += record.alignmentEnd - record.alignmentStart + 1
                currentRefRangeBpCounter.totalNM += record.getIntegerAttribute("NM")

                val cigarElements = record.cigar.cigarElements

                for(cigarElement in cigarElements) {
                    when(cigarElement.operator) {
                        CigarOperator.EQ -> currentRefRangeBpCounter.totalEQ += cigarElement.length
                        CigarOperator.X -> currentRefRangeBpCounter.totalX += cigarElement.length
                        CigarOperator.I -> currentRefRangeBpCounter.totalI += cigarElement.length
                        CigarOperator.D -> currentRefRangeBpCounter.totalD += cigarElement.length
                        CigarOperator.H -> currentRefRangeBpCounter.totalHClip += cigarElement.length
                        CigarOperator.S -> currentRefRangeBpCounter.totalSClip += cigarElement.length
                    }
                }
            }

            refRangeBpCounters[refRangeId] = currentRefRangeBpCounter
        }
    }

    /**
     * Simple writer function to loop through the filtered records and output all the alignments.
     */
    private fun outputRecords(filteredRecords: Map<Pair<String, Boolean>,List<SAMRecord>>, outputBamFileWriter: SAMFileWriter) {
        for(entry in filteredRecords) {
            for(record in entry.value) {
                outputBamFileWriter.addAlignment(record)
            }
        }
    }

    /**
     * Simple function to remobve hapIds if they cross multiple reference ranges.
     */
    fun keepHapIdsForSingleRefRange(bestHitMap: Map<Pair<String,Boolean>, MutableList<SAMRecord>>, hapIdToRangeMap: Map<Int, Int>, maxRefRangeError: Double) : Map<Pair<String,Boolean>, MutableList<SAMRecord>> {
        return bestHitMap.entries
            .map{
                removeExtraRangeAlignments(it,hapIdToRangeMap, maxRefRangeError)
            }
            .filter { it.second.size>0 } //Filter out any reads which do not have any mappings
            .toMap()
    }

    /**
     * Simple function to go through the records grouped by range and remove any suboptimal alignments.
     */
    fun removeExtraRangeAlignments(entry: Map.Entry<Pair<String,Boolean>,MutableList<SAMRecord>>, hapIdToRangeMap: Map<Int, Int>, maxRefRangeError: Double) : Pair<Pair<String,Boolean>,MutableList<SAMRecord>> {
        val recordsGroupedByRefRange = entry.value.groupBy { hapIdToRangeMap[it.contig.toInt()]?:-1 }

        if(recordsGroupedByRefRange.keys.size==1) {
            return Pair(entry.key,entry.value) //Can return entry as they are all in the same refRange
        }

        val bestRefRangeId = findBestRefRangeFromRecords(recordsGroupedByRefRange,maxRefRangeError)

        val bestRecords = recordsGroupedByRefRange[bestRefRangeId]?.toMutableList() ?: mutableListOf()

        return Pair(entry.key, bestRecords)

    }

    /**
     * Function to check to see if the read hits a multiple reference ranges less than maxRefRangeError
     * Basically this is to filter out any reads which hit multiple reference ranges equally well
     */
    fun findBestRefRangeFromRecords(refRangeToIdMapping : Map<Int, List<SAMRecord>>, maxRefRangeError:Double) : Int {
        var bestRefRange = -1
        var totalNumberOfHits = 0
        var bestCount = 0


        refRangeToIdMapping.keys.forEach {
            if((refRangeToIdMapping[it]?.size ?: 0) > bestCount) {
                bestCount = refRangeToIdMapping[it]?.size ?: 0
                bestRefRange = it
            }
            totalNumberOfHits+= refRangeToIdMapping[it]?.size ?: 0
        }

        if(maxRefRangeError <= 1 - (bestCount.toDouble()/totalNumberOfHits)) {
            return -1
        }

        return bestRefRange
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MultisampleBAMToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "FilterBAMAndGetStatsPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to take BAM files remove suboptimal mappings and return some stats on number of clipped bps and edit distance. "
    }

    /**
     * Name of the BAM file to process.  Also will support
     * SAM Files
     *
     * @return bamFile
     */
    fun bamFile(): String {
        return bamFile.value()
    }

    /**
     * Set bamFile. Name of the BAM file to process.  Also
     * will support SAM Files
     *
     * @param value bamFile
     *
     * @return this plugin
     */
    fun bamFile(value: String): FilterBAMAndGetStatsPlugin {
        bamFile = PluginParameter<String>(bamFile, value)
        return this
    }

    /**
     * Name of the  output filtered BAM/SAM file.
     *
     * @return Output Bam File
     */
    fun outputBamFile(): String {
        return outputBamFile.value()
    }

    /**
     * Set Output Bam File. Name of the  output filtered BAM/SAM
     * file.
     *
     * @param value Output Bam File
     *
     * @return this plugin
     */
    fun outputBamFile(value: String): FilterBAMAndGetStatsPlugin {
        outputBamFile = PluginParameter<String>(outputBamFile, value)
        return this
    }

    /**
     * Name of the  output stats file of RefRange to total
     * counts.
     *
     * @return Output Stats File
     */
    fun outputStatsFile(): String {
        return outputStatsFile.value()
    }

    /**
     * Set Output Stats File. Name of the  output stats file
     * of RefRange to total counts.
     *
     * @param value Output Stats File
     *
     * @return this plugin
     */
    fun outputStatsFile(value: String): FilterBAMAndGetStatsPlugin {
        outputStatsFile = PluginParameter<String>(outputStatsFile, value)
        return this
    }

    /**
     * Flag to set if you are using Paired End reads.
     *
     * @return Is Paired End
     */
    fun isPairedEnd(): Boolean {
        return isPairedEnd.value()
    }

    /**
     * Set Is Paired End. Flag to set if you are using Paired
     * End reads.
     *
     * @param value Is Paired End
     *
     * @return this plugin
     */
    fun isPairedEnd(value: Boolean): FilterBAMAndGetStatsPlugin {
        isPairedEnd = PluginParameter<Boolean>(isPairedEnd, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError(): Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double): FilterBAMAndGetStatsPlugin {
        maxRefRangeError = PluginParameter<Double>(maxRefRangeError, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(FilterBAMAndGetStatsPlugin::class.java)
}