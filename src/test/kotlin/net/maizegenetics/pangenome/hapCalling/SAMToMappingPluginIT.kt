package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll

import java.io.File
import java.time.LocalDateTime
import java.util.stream.Collectors

/**
 * NOTE: This requires minimap2 installed on your system and on the PATH
 */
@ExtendWith(SmallSeqExtension::class)
class SAMToMappingPluginIT {

    companion object {
        //Get the date time to tag the method in the DB so Small seq tests does not have to run each time.
        val currentLocalDateTime = LocalDateTime.now()

        @JvmStatic
        @BeforeAll
        fun setup() {
            LoggingUtils.setupDebugLogging()
        }
    }

    /**
     * Test loading up the sam file directly.
     */
    @Test
    fun verifySAMLoading() {
        //Create a SAM keyFile
        val fastqKeyFile = SmallSeqExtension.genotypingKeyFilePath
        val outputKeyFile = "${SmallSeqExtension.baseDir}/samKeyFile.txt"
        //Loop through each entry an run minimap2
        val keyFileLines = Utils.getBufferedReader(fastqKeyFile).readLines()

        //Create some directories.
        File(SmallSeqExtension.samFileDir).mkdirs()
        File("${SmallSeqExtension.samFileDir}/logs/").mkdirs()
        File("${SmallSeqExtension.samFileDir}/readMappings/").mkdirs()

        //Create the SAM genotyping keyfile
        Utils.getBufferedWriter(outputKeyFile).use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
            keyFileLines.filter { !it.startsWith("cultivar") }
                    .map { it.split("\t") }
                    .map {
                        //Execute Minimap2 and write new keyfile
                        val fastqFile = it[2]
                        val samFile = fastqFile.split("_")[0]+"_${it[3]}" + ".sam"

                        //Run Minimap2
                        runMinimap2(fastqFile,samFile)

                        "${it[0]}\t${it[1]}\t${samFile}\t${it[3]}"
                    }
                    .forEach { output.write(it + "\n") }
        }

        println("Processing SAMs")

        ParameterCache.load(SmallSeqExtension.configFilePath)

        //Build a graph to use.
        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                .methods("GATK_PIPELINE")
                .includeSequences(false)
                .includeVariantContexts(false)
                .configFile(SmallSeqExtension.configFilePath)
                .performFunction(null)


        //Load in SAMToMappingPlugin
        SAMToMappingPlugin(null,false)
                .samDir(SmallSeqExtension.samFileDir)
                .keyFile(outputKeyFile)
                .lowMemMode(true)
                .methodName("ReadMappingFromSAM_$currentLocalDateTime")
                .methodDescription("Loading the ReadMappings from a SAM File")
                .outputDebugDir("${SmallSeqExtension.samFileDir}/readMappings/")
                .performFunction(graphDS)
        println("Done Processing SAMs")

        val phg = PHGdbAccess(DBLoadingUtils.connection(false))
        val readMappingDecoder = ReadMappingDecoder(phg)

        // ReadMappingDecoder needs a graph
        // The read mapping decoder needs a graph
        val datasetResult = graphDS?.getDataOfType(HaplotypeGraph::class.java)
            ?: throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (datasetResult.size != 1) {
            throw IllegalArgumentException("MultisampleBAMToMappingPlugin: processData: must input one HaplotypeGraph: " + datasetResult.size)
        }
        val mappingGraph: HaplotypeGraph = datasetResult[0].data as HaplotypeGraph
        //Load in the path keyfiles and use the following to get the mappings:
        // val readMappings = decodeHapIdMapping(phg.getReadMappingsForId(mappingId))
        val originalMappingKeyFile = Utils.getBufferedReader("${SmallSeqExtension.baseDir}/genotypingKeyFile_pathKeyFile.txt").readLines()
                .filter { !it.startsWith("SampleName") }.map { it.split("\t") }
                .map { Pair(it[0],it[1]) }
                .toMap()

        val samMappingKeyFile = Utils.getBufferedReader( "${SmallSeqExtension.baseDir}/samKeyFile_pathKeyFile.txt").readLines()
                .filter { !it.startsWith("SampleName") }.map { it.split("\t") }
                .map { Pair(it[0],it[1]) }
                .toMap()

        assertEquals(originalMappingKeyFile.keys.size,samMappingKeyFile.keys.size,"Number of Taxon in keyfiles do not match")
        assertEquals(originalMappingKeyFile.keys.size,originalMappingKeyFile.keys.filter { samMappingKeyFile.keys.contains(it) }.count(), "All Keys in original are not in sam: ")
        assertEquals(samMappingKeyFile.keys.size,samMappingKeyFile.keys.filter { originalMappingKeyFile.keys.contains(it) }.count(), "All Keys in sam are not in original: ")

        //Test each taxon to see if the ReadMappings are consistent.
        originalMappingKeyFile.keys.forEach { taxon ->
            println("Testing Taxon: $taxon ${originalMappingKeyFile[taxon]} ${samMappingKeyFile[taxon]}")

            val originalReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(originalMappingKeyFile[taxon]?.toInt() ?: -1)
            val samReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(samMappingKeyFile[taxon]?.toInt()?:-1)

            //Map<List<Int>,Int>
            //Check to see if the hapIdSets are equivalent
            assertEquals(originalReadMappings.keys.size,originalReadMappings.keys.filter{samReadMappings.keys.contains(it)}.count(), "Original Read Mapping HapId Sets are missing from SAM: $taxon")
            assertEquals(samReadMappings.keys.size,samReadMappings.keys.filter{originalReadMappings.keys.contains(it)}.count(), "SAM Read Mapping HapId Sets are missing from Original: $taxon")

            originalReadMappings.keys.forEach { hapIdList ->
                val origCount = originalReadMappings[hapIdList]
                val samCount = samReadMappings[hapIdList]

                assertEquals(origCount,samCount, "Counts are different for hapIds:$hapIdList")
            }

        }

        //Get the first and last ids so we can force it to make errors.
        val firstId = samMappingKeyFile[samMappingKeyFile.keys.first()]?.toInt()?:-1
        val lastId = samMappingKeyFile[samMappingKeyFile.keys.last()]?.toInt()?:-1

        //Make sure everything fails:
        originalMappingKeyFile.keys.forEach { taxon ->
            println("Testing Taxon For errors: $taxon ${originalMappingKeyFile[taxon]} ${samMappingKeyFile[taxon]}")

            val samId = samMappingKeyFile[taxon]?.toInt()?:-1

            val originalReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(originalMappingKeyFile[taxon]?.toInt()?:-1)
            //Force it to have an incorrect mapping
            val samReadMappings = if(samId != firstId) {
                readMappingDecoder.getDecodedReadMappingForMappingId(firstId)
            }
            else {
                readMappingDecoder.getDecodedReadMappingForMappingId(lastId)
            }

            var allSame = true
            originalReadMappings.keys.forEach { hapIdList ->
                val origCount = originalReadMappings[hapIdList]
                val samCount = samReadMappings[hapIdList]

                if(origCount != samCount) {
                    allSame = false
                }
            }
            assertFalse(allSame, "Counts are the same for all hapIds:$taxon")

        }
        phg.close()

    }

    fun runMinimap2(fastqFile : String, samFile : String) {
        val indexBuilder = ProcessBuilder("minimap2","-ax", "sr", "-t", "10", "--secondary=yes","-N20", "--eqx", "${SmallSeqExtension.pangenomeDir}/pangenome_GATK_PIPELINE_k21w11I90G.mmi", "${SmallSeqExtension.dataDir}/$fastqFile")

        val indexRedirectOutput = SmallSeqExtension.samFileDir + samFile
        val indexRedirectError = SmallSeqExtension.samFileDir+"/logs/" + samFile +"_error.log"
        indexBuilder.redirectOutput(File(indexRedirectOutput))
        indexBuilder.redirectError(File(indexRedirectError))

        println("runMinimap2 command: " + indexBuilder.command().stream().collect(Collectors.joining(" ")))
        val indexProcess = indexBuilder.start()
        indexProcess.waitFor()
    }
}