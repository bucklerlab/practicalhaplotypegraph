--liquibase formatted sql

--## add fields asm_contig, asm_start_coordinate, asm_end_coordinate and genome_file_id to haplotypes table
--## sqlite doesn't allow multiple columns added in the same query, so separate lines in that changeset

--changeset lcj34:add_asm_data_to_haplotypes_sqlite_0.0.24 dbms:sqlite
ALTER TABLE haplotypes ADD COLUMN genome_file_id INTEGER DEFAULT -1;
ALTER TABLE haplotypes ADD COLUMN asm_contig text DEFAULT '0';
ALTER TABLE haplotypes ADD COLUMN asm_start_coordinate INTEGER DEFAULT 0;
ALTER TABLE haplotypes ADD COLUMN asm_end_coordinate INTEGER DEFAULT 0;

--changeset lcj34:add_asm_data_to_haplotypes_postgres_0.0.24 dbms:postgresql
ALTER TABLE haplotypes
  ADD COLUMN genome_file_id INTEGER DEFAULT -1,
  ADD COLUMN asm_contig text DEFAULT '0',
  ADD COLUMN asm_start_coordinate INTEGER DEFAULT 0,
  ADD COLUMN asm_end_coordinate INTEGER DEFAULT 0;

