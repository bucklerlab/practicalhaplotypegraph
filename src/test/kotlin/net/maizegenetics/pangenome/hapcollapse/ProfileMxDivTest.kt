package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.buildSimpleConsensusGraph
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.Datum
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.fail

class ProfileMxDivTest {

    lateinit var graphDS: DataSet

    companion object {
        val testingDir = "${SmallSeqExtension.testTmpDir}/ProfileMxDivTest/"

        @JvmStatic
        @BeforeAll
        fun setupTests() {
            File(testingDir).mkdirs()
        }

    }

    @Test
    fun testMxDivProfilerWithSnpMethod() {
        fail("Fix this broken test.")
        graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqPaths.dbConfigFile)
            .methods("GATK_PIPELINE")
            .includeSequences(false)
            .includeVariantContexts(true)
            .performFunction(null)

        val out = SmallSeqPaths.outputDir + "/mxdivProfileSnp.txt"
        val startTime = System.nanoTime()
        ProfileMxdivPlugin()
            .outputFile(out)
            .distanceType(ProfileMxdivPlugin.DistanceType.SNP)
            .numThreads(5)
            .performFunction(graphDS)

        println("Snp profile ran in ${(System.nanoTime() - startTime)/1e6} ms")
    }

    @Test
    fun testMxDivProfilerWithKmerMethodDummyGraph() {
        val graph = buildSimpleConsensusGraph(10, 10)
        val graphDS = DataSet(Datum("PHG", graph, "comment"), null)
        val mxOut =  "$testingDir/mxdivProfileKmer.txt"

        ProfileMxdivPlugin()
            .outputFile(mxOut)
            .distanceType(ProfileMxdivPlugin.DistanceType.KMER)
            .numThreads(5)
            .kmerSize(3)
            .performFunction(graphDS)

        val rangeIdMap = mutableMapOf<Int, MutableList<Pair<Double, Int>>>()
        File(mxOut).bufferedReader().use {
            it.lines().skip(1).forEach { line ->
                val cols = line.split("\t")
                val refRangeId = cols[0].toInt()
                val distAndClusterSize = Pair<Double, Int>(cols[3].toDouble(), cols[4].toInt())
                if (rangeIdMap[refRangeId].isNullOrEmpty()) rangeIdMap[refRangeId] = mutableListOf<Pair<Double, Int>>()
                rangeIdMap[refRangeId]!!.add(distAndClusterSize)
            }
        }

        assertTrue(rangeIdMap[0]?.size == rangeIdMap[1]?.size)

        // mxDiv values should be the same
        rangeIdMap[0]?.forEachIndexed { idx, rangeZero ->
            val rangeOne = rangeIdMap[1]!!.get(idx)
            assertTrue(rangeZero.first == rangeOne.first)
        }

        assertNumClustersIncrease(rangeIdMap[0]!!)
        assertNumClustersIncrease(rangeIdMap[1]!!)
    }

    private fun assertNumClustersIncrease(pairs: MutableList<Pair<Double, Int>>) {
        // numHaplotypes (total number of clusters) should increase or stay the same as mxDiv decreases
        //     since we are moving the cut point "lower" down the tree (towards the leaves)
        val maxIdx = pairs.size - 1
        pairs.forEachIndexed { idx, pair ->
            if (idx != 0)      assertTrue(pair.second >= pairs[idx - 1].second) // Previous item
            if (idx != maxIdx) assertTrue(pair.second <= pairs[idx + 1].second) // Next item
        }
    }
}