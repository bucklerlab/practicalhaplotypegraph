package net.maizegenetics.pangenome.hapcollapse;

import com.google.common.collect.ImmutableList;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

/**
 * Created by zrm22 on 8/1/17.
 */
@Deprecated
public class GVCFTyperPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(GVCFTyperPlugin.class);

    //Enum to hold the possible EMIT_Modes
    public enum EMIT_MODE {
        all,
        variant,
        confident
    }

    //Plugin parameter definitions
    //If we need to import some additional parameters we will have to add them here.
    private PluginParameter<String> inputGVCFDir = new PluginParameter.Builder<>("gvcfDir", null, String.class).guiName("Input GVCF Directory").inDir().required(true)
            .description("Input gvcf directory.").build();
    private PluginParameter<String> referenceFile = new PluginParameter.Builder<>("refFasta", null, String.class).guiName("Reference Fasta Sequence").inFile().required(true)
            .description("Input Reference used to create the gvcf").build();
    private PluginParameter<String> outputVCFDir = new PluginParameter.Builder<>("outputVCFDir", null, String.class).guiName("Output VCF Directory").outDir().required(true)
            .description("Output VCF file directory.").build();
    private PluginParameter<EMIT_MODE> emitModeParam = new PluginParameter.Builder<>("emit_mode", EMIT_MODE.variant, EMIT_MODE.class).required(false)
            .description("Emit Mode for running GVCFTyper.  Options are all, variant, or confident").build();
    private PluginParameter<String> inputIntervalFile = new PluginParameter.Builder<>("intervals", null, String.class).guiName("Input Interval File").inFile().required(true)
            .description("Input interval file to run GVCFTyper with.").build();
    private PluginParameter<Integer> numThreads = new PluginParameter.Builder<>("numThreads", 20, Integer.class).required(false)
            .description("Number of threads for Sentieon to use.").build();


    public GVCFTyperPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    //Main Entry point for the plugin
    public DataSet processData(DataSet input) {
        //do whatever your plugin does
        List<String> intervalList = extractIntervals();
        intervalList.stream().forEach(interval -> runGVCFTyper(interval));

        return null;  // Note: this can return null
    }

    /**
     * Simple method to extract all the lines of a file into a list.  In this case we are looking for interval lines in the form:
     * chr:stPos-endPos
     *
     * @return a list of each of the lines
     * @throws IllegalStateException
     */
    private List<String> extractIntervals() throws IllegalStateException {
        ImmutableList.Builder<String> intervalListBuilder = new ImmutableList.Builder<>();
        try (BufferedReader intervalReader = Utils.getBufferedReader(inputIntervalFile())) {
            String currentLine = "";
            while ((currentLine = intervalReader.readLine()) != null) {
                intervalListBuilder.add(currentLine);
            }
            return intervalListBuilder.build();
        } catch (Exception e) {
            myLogger.error(e.getMessage());
            throw new IllegalStateException("Error in reading in the Intervals" + e.getMessage(), e);
        }
    }


    /**
     * Simple method to run Sentieon's GVCFTyper algorithm on a given reference interval
     *
     * @param currentInterval current interval taken from the interval file in the form chr:stPos-endPos
     */
    private void runGVCFTyper(String currentInterval) {
        //Here is where we need to setup the process builder and run the gvcf...remember to wait on the thread before you complete the run.
        try {
            //Parse the interval so we can differentiate the output files.
            String[] intervalParse = currentInterval.split(":");
            String[] positionParse = intervalParse[1].split("-");

            //Have to concatenate all the commands together in order to run from the shell
            ProcessBuilder builder = new ProcessBuilder("sh", "-c", "$SENTIEON driver -t " + numThreads() +
                    " -r " + referenceFile() + " --interval " + currentInterval + " --algo GVCFtyper --emit_mode " + emitModeParam() +
                    " " + outputVCFDir() + "gvcfTyperOutputFiltered_EmitMode_" + emitModeParam() + "_chr" + intervalParse[0] + "_stPos" + positionParse[0] + "_endPos" + positionParse[1] + ".vcf " +
                    inputGVCFDir() + "*.g.vcf").inheritIO();

            //Add in the environment variables so that the shell can pick up where sentieon is
            Map<String, String> env = builder.environment();
            env.put("license", "cbsulogin2.tc.cornell.edu:8990");
            env.put("SENTIEON_LICENSE", env.get("license"));
            env.put("RELEASE", "/programs/sentieon-genomics-201704.01");
            env.put("SENTIEON", "/programs/sentieon-genomics-201704.01/bin/sentieon");

            myLogger.debug("Command:" + builder.command().toString());
            //Start the process and wait for the next one. If we want to multithread this in the future, we will need to change it
            Process process = builder.start();
            process.waitFor();
        } catch (Exception e) {
            throw new IllegalStateException("Error in processing interval: " + currentInterval);
        }
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "GVCF Typer";
    }

    @Override
    public String getToolTipText() {
        return "Run GVCFTyper on GVCF Files";
    }


    @Override
    public String pluginDescription() {
        return "This plugin takes a gvcfInput Directory, a interval file containing anchor intervals, " +
                "a emit_mode and an output directory and runs Sentieon GVCFTyper for each anchor interval.  " +
                "It will export a VCF file for each anchor interval to the output directory. ";

    }


    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // public static void main(String[] args) {
    //     GeneratePluginCode.generate(GVCFTyperPlugin.class);
    // }

//    /**
//     * Convenience method to run plugin with one return object.
//     */
//    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * Input gvcf directory.
     *
     * @return Input GVCF Directory
     */
    public String inputGVCFDir() {
        return inputGVCFDir.value();
    }

    /**
     * Set Input GVCF Directory. Input gvcf directory.
     *
     * @param value Input GVCF Directory
     * @return this plugin
     */
    public GVCFTyperPlugin inputGVCFDir(String value) {
        inputGVCFDir = new PluginParameter<>(inputGVCFDir, value);
        return this;
    }

    /**
     * Input Reference used to create the gvcf
     *
     * @return Reference Fasta Sequence
     */
    public String referenceFile() {
        return referenceFile.value();
    }

    /**
     * Set Reference Fasta Sequence. Input Reference used
     * to create the gvcf
     *
     * @param value Reference Fasta Sequence
     * @return this plugin
     */
    public GVCFTyperPlugin referenceFile(String value) {
        referenceFile = new PluginParameter<>(referenceFile, value);
        return this;
    }

    /**
     * Output VCF file directory.
     *
     * @return Output VCF Directory
     */
    public String outputVCFDir() {
        return outputVCFDir.value();
    }

    /**
     * Set Output VCF Directory. Output VCF file directory.
     *
     * @param value Output VCF Directory
     * @return this plugin
     */
    public GVCFTyperPlugin outputVCFDir(String value) {
        outputVCFDir = new PluginParameter<>(outputVCFDir, value);
        return this;
    }

    /**
     * Emit Mode for running GVCFTyper.  Options are all,
     * variant, or confident
     *
     * @return Emit_mode
     */
    public EMIT_MODE emitModeParam() {
        return emitModeParam.value();
    }

    /**
     * Set Emit_mode. Emit Mode for running GVCFTyper.  Options
     * are all, variant, or confident
     *
     * @param value Emit_mode
     * @return this plugin
     */
    public GVCFTyperPlugin emitModeParam(EMIT_MODE value) {
        emitModeParam = new PluginParameter<>(emitModeParam, value);
        return this;
    }

    /**
     * Input interval file to run GVCFTyper with.
     *
     * @return Input Interval File
     */
    public String inputIntervalFile() {
        return inputIntervalFile.value();
    }

    /**
     * Set Input Interval File. Input interval file to run
     * GVCFTyper with.
     *
     * @param value Input Interval File
     * @return this plugin
     */
    public GVCFTyperPlugin inputIntervalFile(String value) {
        inputIntervalFile = new PluginParameter<>(inputIntervalFile, value);
        return this;
    }

    /**
     * Number of threads for Sentieon to use.
     *
     * @return Num Threads
     */
    public Integer numThreads() {
        return numThreads.value();
    }

    /**
     * Set Num Threads. Number of threads for Sentieon to
     * use.
     *
     * @param value Num Threads
     * @return this plugin
     */
    public GVCFTyperPlugin numThreads(Integer value) {
        numThreads = new PluginParameter<>(numThreads, value);
        return this;
    }


}
