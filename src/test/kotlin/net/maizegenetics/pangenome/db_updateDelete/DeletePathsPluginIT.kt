package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.db_updateDelete.DeletePathsPlugin
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Test
import kotlin.test.*

@ExtendWith(SmallSeqExtension::class)
class DeletePathsPluginIT {

    val taxaListOfStrings = listOf("Ref_wgs", "RefA1_wgs", "LineA_wgs", "LineB_wgs", "LineA1_wgs", "LineB1_wgs")

    // TODO:
    //  Testing of the underlying PHGdbAccess methods that DeletePathsPlugin uses is also done in PHGdbAccessIT
    //  For the sake of ensuring that the plugin is tested, we duplicate a bit of that here
    //  Maybe in future we make this a unit test that takes a graph file as input and outputs a graph file that we test against?

    @Test
    fun deletePathsPluginTestNoTaxa() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val taxaBuilder =  TaxaListBuilder()
        taxaListOfStrings.forEach { taxaBuilder.add(it) }
        val taxaList = taxaBuilder.build()

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(6, pathsForMethodMapBefore.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeletePathsPlugin(null,false)
            .methodName(SmallSeqExtension.pathMethod)
            .performFunction(null)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(0, pathsForMethodMapAfter.size)

        phg.close()
    }

    @Test
    fun deletePathsPluginTestTaxaAndMethod() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val taxaBuilder =  TaxaListBuilder()
        taxaListOfStrings.forEach { taxaBuilder.add(it) }
        val taxaList = taxaBuilder.build()

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(6, pathsForMethodMapBefore.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeletePathsPlugin(null,false)
            .methodName(SmallSeqExtension.pathMethod)
            .taxa(taxaList)
            .performFunction(null)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(0, pathsForMethodMapAfter.size)

        phg.close()
    }

    @Test
    fun deletePathsPluginTestTaxaWrongMethod() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val wrongMethodName = "This_Method_Does_Not_Exist"

        val taxaBuilder =  TaxaListBuilder()
        taxaListOfStrings.forEach { taxaBuilder.add(it) }
        val taxaList = taxaBuilder.build()

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaListOfStrings, wrongMethodName)
        assertEquals(6, pathsForMethodMapBefore.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeletePathsPlugin(null,false)
            .methodName(wrongMethodName)
            .taxa(taxaList)
            .performFunction(null)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaListOfStrings, wrongMethodName)
        assertEquals(6, pathsForMethodMapAfter.size) // Number of paths should not have changed

        phg.close()
    }

    @Test
    fun deletePathsPluginTestTaxaAndKeyFile() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val taxaBuilder =  TaxaListBuilder()
        taxaListOfStrings.forEach { taxaBuilder.add(it) }
        val taxaList = taxaBuilder.build()

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(6, pathsForMethodMapBefore.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeletePathsPlugin(null,false)
            .methodName(SmallSeqExtension.pathMethod)
            .taxa(taxaList)
            .keyFile(SmallSeqExtension.genotypingKeyFilePath)
            .performFunction(null)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(6, pathsForMethodMapAfter.size)

        phg.close()
    }

    @Test
    fun deletePathsPluginTestKeyFile() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val pathsForMethodMapBefore = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(6, pathsForMethodMapBefore.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeletePathsPlugin(null,false)
            .methodName(SmallSeqExtension.pathMethod)
            .keyFile(SmallSeqExtension.genotypingKeyFilePath)
            .performFunction(null)

        val pathsForMethodMapAfter = phg.getPathsForTaxonMethod(taxaListOfStrings, SmallSeqExtension.pathMethod)
        assertEquals(0, pathsForMethodMapAfter.size)

        phg.close()
    }
}
