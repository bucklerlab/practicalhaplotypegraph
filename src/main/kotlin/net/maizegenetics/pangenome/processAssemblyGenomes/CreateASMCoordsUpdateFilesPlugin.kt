package net.maizegenetics.pangenome.processAssemblyGenomes

import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Sizeof
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import java.sql.Connection
import java.sql.SQLException
import javax.swing.ImageIcon

/**
 * This class traverses the db and writes, to csv files, the haplotypes_id, asm_start_coordinate, asm_end_coordinate,
 * asm_strand values.  These files will be loaded to the db into a temp table, from which updates will occur for haplotype
 * nodes.  This class is a successor to UpdateDBAsmCoordinatesPlugin.kt.  There were issues with batching
 * in the original class - only single updates, one at a time, worked.
 *
 * For this plugin:  Strand shouldn't change, but it will be uploaded as well
 *   in kotlin/../api/ConvertVariantContextToVariantInfo:determineASMInfo()
 *     the code determines asm coordinates based on chroms being the same, and strand being the same
 *
 * This example comes from https://tapoueh.org/blog/2018/07/batch-updates-and-concurrency/
 *
 * The commands you need to do in the db:
 *
 * 1. create the temp table via:
 *  postgres# create temp table batch (haplotypes_id INTEGER, asm_start_coordinate integer, asm_end_coordinate integer, asm_strand varchar(1));
 *
 * 2.  Run this plugin to get the csv file for import
 * 3.  you can either load the files separately, or cat them and then load a single file
 *     In folder where the csv files live:
 *          > l -1 *.csv > csvFileList.txt
 *          > xargs cat < csvFileList.txt > all25files.csv
 *          Then sort the file to get all the header lines in 1 place, move a header line back to the top of the file,
 *            delete the other header lines.  You can do that manually, or use sort, e.g.
 *            > sort all25files.csv -t, -k1,1 > all25filesSORTED.csv
 * 3a. (NOT RUN) In the db again run: (this is if you want to dump data to a csv, then add it to a different table.)
 *      NOTE: We aren't doing this - we are getting data from this plugin, and loading that
 *      phgsmallseq=# \COPY haplotypes(haplotypes_id,asm_start_coordinate, asm_end_coordinate, asm_strand) TO '/Users/lcj34/notes_files/phg_2018/debug/zackFix_mafToGVCF_Oct2022/haplotypesPostgres_just3Cols.csv' with delimiter ',' CSV HEADER;
 *
 *  4. Import data from the csv files from 2. above to the temp table named "batch" created in 1. above
 *
 *     postgres=# \copy batch from '/workdir/lcj34/zackFixMAFToGvcf_oct2022/asmCSVs/all25files.csv' with csv header delimiter ',';
 *
 *  5. Now you have data in the temp table - transfer it to the haplotypes table: (only 6 updated in this example as it was smallSeq)
 *  phgsmallseq=# update haplotypes set (asm_start_coordinate, asm_end_coordinate) = (batch.asm_start_coordinate, batch.asm_end_coordinate) from batch where batch.haplotypes_id = haplotypes.haplotypes_id and ( haplotypes.asm_start_coordinate, haplotypes.asm_end_coordinate) <> (batch.asm_start_coordinate,batch.asm_end_coordinate);
 *  UPDATE 6
 *  phgsmallseq=#
 *
 * This will work for postgres - not sure if it will work for sqlite
 * For the record:  I used the methods outlined above to create 13167015 records for potential updates, which resulted in
 *   12828476 udpates.  It took 10.5 minutes to do this update in maize_2_1 on 11/14/22
 *
 * NOTE: the plugin took 5 hours 27 minutes to run on dc01 on Nov 10, 2022 with 86 gvcf files
 *       and lots of haplotypes.  You want to run on a cbsu machine, not your laptop.
 */
class CreateASMCoordsUpdateFilesPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(CreateASMCoordsUpdateFilesPlugin::class.java)

    //Create a Data class to hold the information needed to write to the DB.
    data class UpdateData(val id:Int, val asmStart:Int, val asmEnd:Int, val asmStrand:String)

    // Could use the ReferenceRange class, but it is heavier than i need.
    data class RefRangeData(val chrom:String, val start:Int, val end:Int)
    private var gvcfDirectory = PluginParameter.Builder("gvcfDirectory",null, String::class.java)
        .description("Local directory holding all the gvcf files that were processed into haplotypes for the database.")
        .inDir()
        .required(true)
        .build()
    private var asmCSVdir = PluginParameter.Builder("asmCSVdir",null, String::class.java)
        .description("Local directory to which the haplotype update CSV files will be written.")
        .inDir()
        .required(true)
        .build()

    private var configFile = PluginParameter.Builder("configFile",null, String::class.java)
        .description("Config file with parameters for database connection.")
        .inFile()
        .required(true)
        .build()

    private var queueSize = PluginParameter.Builder("queueSize",5, Int::class.javaObjectType)
        .description("Size of Queue used to pass information to the writing thread.  Increase this number to have better thread utilization at the expense of RAM.  " +
                "If you are running into Java heap Space/RAM issues and cannot use a bigger machine, decrease this parameter.")
        .required(false)
        .build()

    private var myNumThreads = PluginParameter.Builder("numThreads",3, Int::class.javaObjectType)
        .description("Number of threads used to upload.  The GVCF upload will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
        .required(false)
        .build()


    override fun processData(input: DataSet?): DataSet? {

        val dbConnect = DBLoadingUtils.connection(configFile(), false)

        // These are alot of maps, but they will make associating data easier.
        // The data is obtained from multiple db queries.
        val gvcfIdToFileMap = mutableMapOf<Int,String>()
        val gvcfIdToHapidMap = mutableMapOf<Int,MutableSet<Int>>()
        val hapIdToRefRangeIdMap = mutableMapOf<Int,Int>()
        val refIdToRefRangeDataMap = mutableMapOf<Int, RefRangeData>()

        try {
            // First db query:  get a mapping of gvcf_file_id to file name.
            // The gvcf to process will live at gvcfDir/file name
            val query = "select id, genome_file from genome_file_data where type=2" // type=2: only want the gvcf files
            myLogger.info("First DB query: ${query}")

            val rs = dbConnect.createStatement().executeQuery(query)

            while (rs.next()) {
                val id = rs.getInt(1)
                val file = rs.getString(2)
                gvcfIdToFileMap.put(id,file)
            }
        } catch (sql: SQLException) {
            var sqle = sql
            var count = 1
            while (sqle != null) {
                myLogger.error("CreateASMCoordsUpdateFilesPlugin SQLException $count")
                myLogger.error("Code: " + sqle.errorCode)
                myLogger.error("SqlState: " + sqle.sqlState)
                myLogger.error("Error Message: " + sqle.message)
                sqle = sqle.nextException
                count++
            }
            throw IllegalStateException("CreateASMCoordsUpdateFilesPlugin: error getting gvcf file names: ${sqle.message}")
        }

        myLogger.info("\nSize of gvcfIdToFileMap: ${gvcfIdToFileMap.size}")
        // Now we need the haplotypes entries that must be updated.
        // The reference range data is needed to find the gvcf entries with the asm_coordinates
        // for updating.
        try {

            val querySB = StringBuilder()

            // We will update both + and - strands, but not do the unknown "."
            querySB.append("SELECT haplotypes_id, gvcf_file_id, asm_strand, haplotypes.ref_range_id, chrom, range_start, range_end ")
            querySB.append("FROM haplotypes ")
            querySB.append("INNER JOIN reference_ranges on reference_ranges.ref_range_id = haplotypes.ref_range_id ")
            querySB.append("WHERE asm_strand IN ('-','+');")

            val query = querySB.toString()
            myLogger.info("haplotypes gvcf/ref_range_data query statement: $query")

            val rs = dbConnect.createStatement().executeQuery(query)
            // Process the queries into the maps we need
            while (rs.next()) {

                val hapId = rs.getInt("haplotypes_id")
                val gvcfId = rs.getInt("gvcf_file_id")
                val refRangeId = rs.getInt("ref_range_id")
                val chrom = rs.getString("chrom")
                val refStart = rs.getInt("range_start")
                val refEnd = rs.getInt("range_end")

                gvcfIdToHapidMap.getOrPut(gvcfId) {mutableSetOf()}.add(hapId)
                hapIdToRefRangeIdMap.put(hapId,refRangeId)

                // Final map = refId to refRangeData
                val rangeData = RefRangeData(chrom, refStart, refEnd)
                refIdToRefRangeDataMap.put(refRangeId,rangeData)
            }

        } catch (sql: SQLException) {
            var sqle = sql
            var count = 1
            while (sqle != null) {
                myLogger.error("CreateASMCoordsUpdateFIlesPlugin SQLException $count")
                myLogger.error("Code: " + sqle.errorCode)
                myLogger.error("SqlState: " + sqle.sqlState)
                myLogger.error("Error Message: " + sqle.message)
                sqle = sqle.nextException
                count++
            }
            throw IllegalStateException("CreateASMCoordsUpdateFilesPlugin: error getting haplotypes data : ${sqle.message}")
        }

        //Need to close these here otherwise they will throw an error if closed too early.
        dbConnect.close()

        myLogger.info("Size of other maps:")
        myLogger.info("  hapIdToRefRangeIdMap: ${hapIdToRefRangeIdMap.size}")
        myLogger.info("  refIdToRefRangeDataMap: ${refIdToRefRangeDataMap.size}")
        myLogger.info("  gvcfIdToHapidMap(number of keys): ${gvcfIdToHapidMap.keys.size}\n")

        //Coroutines
        //create Blocking scope otherwise execution of this will not wait for it to finish
        runBlocking {

            if(queueSize()<0) {
                throw IllegalArgumentException("Queue size is negative.  Please specify at least 0 for the size of the Multithreading Queue")
            }

            //Setup the channels  The input channel will hold Pairs of gvcfId to gvcfFile.  The Result Channel holds UpdateRecords to write.
            val inputChannel = Channel<Pair<Int,String>>(queueSize())
            val resultChannel = Channel<Pair<String,List<UpdateData>>>(queueSize())
            val numThreads = (numThreads() - 2).coerceAtLeast(1)

            //Setup the channel to pass information to the worker coroutines
            //Each entry is a gvcf file with haplotype list so the worker coroutines can start working on them when ready.
            launch {
                myLogger.info("Adding gvcf entries to the inputChannel:")
                // Put the gvcf id on the list.  Later we will pass the maps needed to process each one
                gvcfIdToFileMap.entries.forEach {
                    val id = it.key
                    val fileName = it.value
                    val gvcfPair = Pair<Int,String>(id,fileName)
                    myLogger.info("Sending gvcf ${fileName} to the input channel")
                    inputChannel.send(gvcfPair)
                }
                myLogger.info("Done Adding gvcf data entries to the inputChannel:")
                inputChannel.close() //Need to close this here to show the workers that it is done adding more data
            }

            //For the number of threads on the machine, set up
            val workerThreads = (1..numThreads).map { threadNum ->
                launch {processGVCFfile(inputChannel, resultChannel, gvcfIdToHapidMap, refIdToRefRangeDataMap, hapIdToRefRangeIdMap, gvcfDirectory())}
            }

            launch {
                writeCSVFiles(resultChannel,dbConnect)
                myLogger.info("Finished writing CSV files.")
            }

            //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
            //If this is not here, this will run forever.
            launch {
                workerThreads.forEach { it.join()}
                resultChannel.close()
            }
            myLogger.info("Setup Coroutines.")
        }



        return null
    }

    //  This method processes the gvcf files, one at a time, putting data onto
    //  a list to be processed to a csv file for a specific sampleName
    private suspend fun processGVCFfile(inputChannel: Channel<Pair<Int, String>>, resultChannel: Channel<Pair<String,List<UpdateData>>>,
                                        gvcfToHapidMap:Map<Int,Set<Int>>, refIdToRefRangeDataMap:Map<Int, RefRangeData>,
                                        hapIdToRefRangeIdMap:Map<Int,Int>, gvcfDir:String)= withContext(Dispatchers.Default) {
        // The channel has a Pair which is <gvcfFileId,gvcf file name>
        for(channelEntry in inputChannel) {
            val id = channelEntry.first
            val name = channelEntry.second
            val fullFileName = "${gvcfDir}/${name}" // the gvcf file to open
            myLogger.info("\nprocessGVCFFile: Processing gvcf file ${fullFileName}")

            // Get the list of hapids to process
            val hapids = gvcfToHapidMap.get(id)

            // Hapids will be null if the asmStrand was ".", ie unknown.  Print a warning,
            // go to the next entry in the channel.
            if (hapids == null) {
                myLogger.warn("processGVCFfile: no  hapids for gvcf file ${name}.  Strand may have been unknown.")
                continue
            }

            val refRangeToHapidsMap = mutableMapOf<Int,MutableSet<Int>>()

            for (hapid in hapids) {
                // create a list of all the refRanges represented by these hapids.
                // Can have multiple hapids mapping to the same refRange - only want to access
                // each ref range in the gvcf once
                val refRangeId = hapIdToRefRangeIdMap.get(hapid)
                if (refRangeId == null) {
                    throw IllegalStateException("processGVCFfile: error getting refRangeId for hapid file ${hapid}")
                }

                // This one is needed so that after processing the ref range, we can create
                // tne update command for each haplotype at this range
                refRangeToHapidsMap.getOrPut(refRangeId){mutableSetOf()}.add(hapid)
            }

            myLogger.info("processGVCFFiles: number of hapids for file ${fullFileName} is ${hapids.size}")
            val dbList = mutableListOf<UpdateData>()


            //   findAsmUpdatesForGVCF basically processes the gvcf file in a manner
            // simliar to what is done in LoadHaplotypesFromGVCFPlugin
            // It must be called for ALL ranges at once to maintain where we are in
            // the gvcf file.  We need to start each range with the next gvcf file entry.

            myLogger.info("processGVCFFiles: calling findAsmUpdatesForGCF")
            val updatedAsmValues = findAsmUpdatesForGVCF(fullFileName, refIdToRefRangeDataMap)

            // Take all data in updatedAsmValues, change refRangeId to hapid, add to
            // dbList for printing.  There will be 1 file for each gvcf

            myLogger.info("processGVCFFiles: number of entries in updatedAsmValues=${updatedAsmValues.size}")
            for (asmData in updatedAsmValues ) {
                val rangeId = asmData.id
                val hapids = refRangeToHapidsMap.get(rangeId)
                if (hapids != null) {
                    for (hapid in hapids) {
                        dbList.add(UpdateData(hapid,asmData.asmStart, asmData.asmEnd, asmData.asmStrand))
                    }
                }
            }

            //  all entries from this gvcf file are written to a single
            // file which will be processed to the db outside of this plugin.
            val csvFileName = name.substringBeforeLast(".")
            val outputFileName = "${asmCSVdir()}/${csvFileName}.csv"
            myLogger.info("processGVCFFiles: adding to result channel ${dbList.size} entries in dbList")
            resultChannel.send(Pair(outputFileName, dbList))
        }
    }

    // This function writes all asm coordinate data from the dbList to a single csv file
    // There will be 1 csv file created for each gvcf file that was processed.
    private suspend fun writeCSVFiles(resultChannel: Channel<Pair<String,List<UpdateData>>>, dbConnect: Connection) = withContext(
        Dispatchers.Default){

        try {
            var listCount = 0
            for (resultList in resultChannel) {
                listCount++
                myLogger.info("writeCSVFiles: processing resultList number ${listCount}")
                // process each entry on each list
                val fileName = resultList.first

                val bw = File(fileName).bufferedWriter()
                bw.write("haplotypes_id,asm_start_coordinate,asm_end_coordinate,asm_strand\n")
                for (item in resultList.second) {
                    bw.write("${item.id},${item.asmStart},${item.asmEnd},${item.asmStrand}\n")
                }
                bw.close()
            }
        } catch (exc:Exception) {
            throw IllegalStateException ("writeCSVFiles: error writing CSV file: ${exc.message}")
        }

    }

    // Here, in UpdateData class, the initial "int" is a refRangeId.  That ID will
    // be used by the calling method to create a different list of UpdateData, one in which the
    // first "int" field is a haplotypes IDs. The remaining fields are the same.
    // This code is mostly taken from LoadHaplotypeFromGVCFPlugin.  The code is a bit simpler
    // as it is not necessary to merge ref blocks. Ultimately, we don't care about the variant
    // data.  We only need the start,end and strand info
    fun findAsmUpdatesForGVCF(gvcfFile:String, refIdToRefRangeDataMap:Map<Int, RefRangeData>):List<UpdateData>
    {
        // get file reader
        val gvcfFileReader = VCFFileReader(File(gvcfFile), true)
        val gvcfFileIterator = gvcfFileReader.iterator()

        //Grab the first gvcf record
        var currentGVCFRecord:VariantContext? = gvcfFileIterator.next()

        var currentChromosome = ""

        // Get the first refRange
        val refRangeIds = refIdToRefRangeDataMap.keys.sorted()
        val firstRangeChrom = refIdToRefRangeDataMap.get(refRangeIds[0])!!.chrom

        myLogger.info("findAsmUpdatesForGVCF: number of refRangeIds= ${refRangeIds.size}")
        // Need to make sure that the gvcf chromosome is not behind the first refRange chromosome
        // If we do not do this, it will not write anything to the DB as it will be out of order.
        // In kotlin < on objects will resolve a .compareTo() Call
        while(currentGVCFRecord != null && (currentGVCFRecord.contig < firstRangeChrom) ) {
            currentGVCFRecord = gvcfFileIterator.next()
        }

        // I'm putting  ALL entries for a single gvcf  on the same dbList
        // This could get long - but we need to process the lines in the correct order.
        // Or maybe not, since these will just be imported to the db's temp table.
        val dbList = mutableListOf<UpdateData>()
        // Process on a per ref range basis
        for (rangeId in refRangeIds) {

            var asmInfoList = mutableListOf<HaplotypeNode.VariantInfo>()
            val currentRefRangeChrom = refIdToRefRangeDataMap.get(rangeId)!!.chrom
            val currentRefRangeStartPos = refIdToRefRangeDataMap.get(rangeId)!!.start
            val currentRefRangeEndPos = refIdToRefRangeDataMap.get(rangeId)!!.end

            // check for gvcf record chrom is greater than refRange Chrom.  If yes, go on
            // to the next ref range record, until the chroms are the same or we run out of ranges
            if(currentGVCFRecord != null && (currentGVCFRecord.contig > currentRefRangeChrom)) {
                continue
            }

            // Set the previous and current chrom values
            // This snippet comes from LoadHaplotypesFromGVCFPlugin.
            //
            //WHat we're looking for is the first and last positions for this chrom and this
            // refRange.
            // this block is mimicing code at line 376-399 of LoadHaplotypesFromGVCFPlugin

            if (currentRefRangeChrom == null || currentRefRangeChrom != currentChromosome) {
                currentChromosome = currentRefRangeChrom

                // The chroms have changed, so you can put data from the list onto the dbList now.
                if (asmInfoList.size > 0) {

                    // We've processed all for that chromosome.  See if there is anything left
                    // on the asmInfoList and if yes, add it to the dbList which gets returned
                    // at the end of this function.

                    val asmVariantInfo = determineASMInfo(asmInfoList, currentRefRangeStartPos, currentRefRangeEndPos)

                    dbList.add(UpdateData(rangeId,asmVariantInfo.asmStart,asmVariantInfo.asmEnd,asmVariantInfo.asmStrand))
                    asmInfoList.clear()

                }
            }

            //Check to see if current range is fully behind GVCF range.  Chromosomes must be the same.  If chromosomes are different
            // |---B---|
            //            |--G----|
            if(currentGVCFRecord != null && currentRefRangeChrom == currentGVCFRecord.contig && currentRefRangeEndPos < currentGVCFRecord.start ) {
                // possible missing node, just continue for now
                continue
            }

            // This is lines 411-447 in LoadHaplotypesFromGVCF
            //check to see if the GVCF should be added to the temp list
            if(currentGVCFRecord != null && currentRefRangeChrom == currentGVCFRecord.contig
                && checkRegionsOverlap(currentRefRangeStartPos,currentRefRangeEndPos,currentGVCFRecord.start, currentGVCFRecord.end)) {
                val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                asmInfoList.add(newGVCFInfo)

            } // end line 411-447

            // lines 449-452 from LoadHaplotypesFromGVCF
            //Loop through the GVCF file until we catch up with the current Bed File range.
            while(currentGVCFRecord != null && (currentGVCFRecord.contig < currentRefRangeChrom) ) {
                currentGVCFRecord = gvcfFileIterator.next()
            }

            // Code at LoadHaplotypesFromGVCFPlugin 457-514
            //Check to see if the previous GVCF record fully covers the Bed range
            //      |---B---|
            // |---------G-------|
            if(currentGVCFRecord != null && currentRefRangeChrom == currentGVCFRecord.contig
                && currentRefRangeStartPos >= currentGVCFRecord.start && currentRefRangeEndPos >= currentGVCFRecord.start
                && currentRefRangeStartPos <= currentGVCFRecord.end && currentRefRangeEndPos <= currentGVCFRecord.end) {

                val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                asmInfoList.add(newGVCFInfo)

                // Here is where we add to the UpdateDataList - based on asmInfo and rangeId info
                // The start and end are merely the start from the first entry, and the end from the last.
                // If first and last entries have different strand values, mark strand as unknown
                // determineASMInfo() will shift the start/end when necessary for refBlocks overlapping refRanges

                val asmVariantInfo = determineASMInfo(asmInfoList, currentRefRangeStartPos, currentRefRangeEndPos)
                dbList.add(UpdateData(rangeId,asmVariantInfo.asmStart,asmVariantInfo.asmEnd,asmVariantInfo.asmStrand))
                asmInfoList.clear()
                continue // on to the next refRange
            }

            //If not, we should loop through the GVCF records until we either leave the refRange region, or the file ends
            // lines 517-576 LoadHaplotypesFromGVCFPlugin
            while(gvcfFileIterator.hasNext()) {
                //check to see if current VCF record is past range
                currentGVCFRecord = gvcfFileIterator.next()
                val currentVCFChr = currentGVCFRecord?.contig
                val currentVCFStart = currentGVCFRecord?.start ?: 0
                val currentVCFEnd = currentGVCFRecord?.end ?: 0

                //Need to break out once the VCF has moved past the bed region
                // |---B---|
                //             |---G----|
                if(currentVCFChr == null || currentVCFChr != currentRefRangeChrom ||currentVCFStart > currentRefRangeEndPos) {
                    break
                }

                //If the GVCF End is still before the Bed region, it means that we need to just move to the next GVCF record
                //                  |-----B----|
                // |---G---|
                if(currentVCFEnd < currentRefRangeStartPos) {
                    continue
                }

                //If it has passed, either GVCF overlaps BED region ends, GVCF fully covers BED file or GVCF is fully covered by the BED
                // This represents lines 538-567 of LoadHaplotypesFromGVCFPlugin
                if(currentGVCFRecord != null) {
                    val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)
                    asmInfoList.add(newGVCFInfo) // this list is cleared once it is put onto the dbList.
                    // dblist is never cleared in this function - all on it are passed back to calling function
                }

                //This is to fix if there is a GVCF record which spans multiple BedRegions
                // |---B---|    |---B---|
                //     |------------G------------|
                //If we do not check this, we will move on to the next gvcf file incorrectly and have a chance to skip the second Bed region
                if(currentRefRangeEndPos < currentVCFEnd) {
                    break
                }

            }

            // Lines 578-585 from LoadHaplotypesFromGVCFPlugin not relevant for this function

            // ADd remaining values to list
            if (asmInfoList.size > 0) {

                val asmVariantInfo = determineASMInfo(asmInfoList, currentRefRangeStartPos, currentRefRangeEndPos)
                dbList.add(UpdateData(rangeId,asmVariantInfo.asmStart,asmVariantInfo.asmEnd,asmVariantInfo.asmStrand))
                asmInfoList.clear()
            }
        }// end rangeId in refRangeIds

        myLogger.info("findAsmUpdatesForGVCF: at end, closing gvcfFIleReader, dbList size=${dbList.size}")
        gvcfFileReader.close()
        return dbList // send to resultChannel will happen from the suspended calling function
    }

    private fun checkRegionsOverlap(bedStart : Int, bedEnd : Int, gvcfStart : Int, gvcfEnd:Int) : Boolean {
        return (gvcfStart in bedStart..bedEnd) ||
                (gvcfEnd in bedStart..bedEnd)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = CreateASMCoordsUpdateFilesPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("CreateASMCOordsUpdateFiles")
    }

    override fun getToolTipText(): String {
       return("plugin used to faciliate updating a PHG Database's assembly coordinates stored in the haploytpes table")
    }

    /**
     * Local directory holding all the gvcf files that were
     * processed into haplotypes for the database.
     *
     * @return Gvcf Directory
     */
    fun gvcfDirectory(): String {
        return gvcfDirectory.value()
    }

    /**
     * Set Gvcf Directory. Local directory holding all the
     * gvcf files that were processed into haplotypes for
     * the database.
     *
     * @param value Gvcf Directory
     *
     * @return this plugin
     */
    fun gvcfDirectory(value: String): CreateASMCoordsUpdateFilesPlugin {
        gvcfDirectory = PluginParameter<String>(gvcfDirectory, value)
        return this
    }

    /**
     * Local directory to which the haplotype update CSV files
     * will be written.
     *
     * @return Asm C S Vdir
     */
    fun asmCSVdir(): String {
        return asmCSVdir.value()
    }

    /**
     * Set Asm C S Vdir. Local directory to which the haplotype
     * update CSV files will be written.
     *
     * @param value Asm C S Vdir
     *
     * @return this plugin
     */
    fun asmCSVdir(value: String): CreateASMCoordsUpdateFilesPlugin {
        asmCSVdir = PluginParameter<String>(asmCSVdir, value)
        return this
    }

    /**
     * Config file with paraeters for database connection.
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Config file with paraeters for database
     * connection.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): CreateASMCoordsUpdateFilesPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Size of Queue used to pass information to the DB writing
     * thread.  Increase this number to have better thread
     * utilization at the expense of RAM.  If you are running
     * into Java heap Space/RAM issues and cannot use a bigger
     * machine, decrease this parameter.
     *
     * @return Queue Size
     */
    fun queueSize(): Int {
        return queueSize.value()
    }

    /**
     * Set Queue Size. Size of Queue used to pass information
     * to the DB writing thread.  Increase this number to
     * have better thread utilization at the expense of RAM.
     *  If you are running into Java heap Space/RAM issues
     * and cannot use a bigger machine, decrease this parameter.
     *
     * @param value Queue Size
     *
     * @return this plugin
     */
    fun queueSize(value: Int): CreateASMCoordsUpdateFilesPlugin {
        queueSize = PluginParameter<Int>(queueSize, value)
        return this
    }

    /**
     * Number of threads used to upload.  The GVCF upload
     * will subtract 2 from this number to have the number
     * of worker threads.  It leaves 1 thread for IO to the
     * DB and 1 thread for the Operating System.
     *
     * @return Num Threads
     */
    fun numThreads(): Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to upload.
     *  The GVCF upload will subtract 2 from this number to
     * have the number of worker threads.  It leaves 1 thread
     * for IO to the DB and 1 thread for the Operating System.
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value: Int): CreateASMCoordsUpdateFilesPlugin {
        myNumThreads = PluginParameter<Int>(myNumThreads, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(CreateASMCoordsUpdateFilesPlugin::class.java)
}