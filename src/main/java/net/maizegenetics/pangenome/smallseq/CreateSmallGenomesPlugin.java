/**
 * 
 */
package net.maizegenetics.pangenome.smallseq;

import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;

import net.maizegenetics.pangenome.pipeline.MakeDefaultDirectoryPlugin;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.LoggingUtils;

import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*;


/**
 * This class provides a standalone plugin to create the small sequence test genome files.
 * Files are stored as per relative paths defined in SmallSeqPaths.java
 * 
 * This plugin also creates reference index files as may be neede by BWA, GATK or other
 * methods.
 * 
 * Currently there are no required parameters - each parameter has a default.
 * 
 *  NOTE:  This plugin creates the indices needed, so if the user is not running through
 *  RUnSmallSeqTestsDocker, the path to samtools, bwa and picard must be given.
 *  If running from a CBSU machine, these are:
 *  
 *  /programs/bin/bwa/bwa
 *  /programs/bin/picard-tools/picard
 *  /programs/bin/samtools/samtools
 *  
 *  To run this plugin on a cbsu machine using default parameters except for the indexes tools,
 *  do this (replace your own
 *  
 * @author lcj34
 *
 */
public class CreateSmallGenomesPlugin extends AbstractPlugin {
    private static final Logger myLogger = LogManager.getLogger(CreateSmallGenomesPlugin.class);

    private PluginParameter<Integer> geneLength = new PluginParameter.Builder<Integer>("geneLength", 256, Integer.class).guiName("Length Of Genes").required(false)
            .description("Number of base pairs to include for each gene region").build();
    private PluginParameter<Integer> interGeneLength = new PluginParameter.Builder<Integer>("interGeneLength", 256, Integer.class).guiName("Length Of Inter Genes")
            .description("Number of base pairs to include for each intergenic region").build();
    private PluginParameter<Integer> numberOfGenes = new PluginParameter.Builder<Integer>("numberOfGenes", 3, Integer.class).guiName("Number Of Genes").required(false)
            .description("Number of gene regions to create").build();
    private PluginParameter<Integer> readLength = new PluginParameter.Builder<Integer>("readLength", 128, Integer.class).guiName("Length Of Reads").required(false)
            .description("Desired length of reads for fastq files").build();
    private PluginParameter<Integer> hapDivergence = new PluginParameter.Builder<Integer>("hapDivergence", 64, Integer.class).guiName("Haplotype Divergence").required(false)
            .description("Percent each haplotype sequence should diverge from reference").build();
    private PluginParameter<Integer> interHapDivergence = new PluginParameter.Builder<Integer>("interHapDivergence", 256, Integer.class).guiName("Inter-Haplotype Divergence").required(false)
            .description("Percent each haplotype sequence should diverge from reference").build();
    private PluginParameter<Double> refInterGeneDup = new PluginParameter.Builder<Double>("refInterGeneDup", 0.0, Double.class).guiName("Proportion of InterGene Duplicate").required(false)
            .description("Proportion of the refererence inter-gene sequence to duplicate").build();
    private PluginParameter<Double> refInterGeneDelete = new PluginParameter.Builder<Double>("refInterGeneDelete", 0.0, Double.class).guiName("Proportion of InterGene Deleted").required(false)
            .description("Proportion of the reference inter-gene sequence to delete").build();
    private PluginParameter<Double> refGeneInsert = new PluginParameter.Builder<Double>("refGeneInsert", 0.0, Double.class).guiName("Proportion of Gene With Insertions").required(false)
            .description("Proportion of Gene where an insertion will be placed.").build();
    private PluginParameter<Double> wgsDepth = new PluginParameter.Builder<Double>("wgsDepth", 10.0, Double.class).guiName("WGS Depth").required(false)
            .description("Depth for WGS ").build();
    private PluginParameter<Double> gbsDepth = new PluginParameter.Builder<Double>("gbsDepth", 0.5, Double.class).guiName("GBS Depth").required(false)
            .description("Depth for GBS").build();

    private PluginParameter<String> baseDirectory = new PluginParameter.Builder<String>("baseDir", null, String.class)
            .description("The base directory for the small genomes data.").build();

    @Override
    public DataSet processData(DataSet input) {
        Long totalTime = System.nanoTime();
        
        LoggingUtils.setupDebugLogging();

        if (baseDirectory() != null) SmallSeqPaths.resetBaseDirectory(baseDirectory());
        try {
            // Delete answerDir and baseDir then create new paths
            // if SmallSeqPaths.resetBaseDirectory is run SmallSeqPaths.smallSeqBaseDir is set to baseDirectory()
            // if we erased all the contents of baseDirectory() a user who did not realize that was going to happen
            // might be moderately annoyed if it contained important files.
            // As a result only subdirectories of smallSeqBaseDir created by this plugin are erased.
            // If it does not exist smallSeqBaseDir is created by Files.createDirectories(Paths.get(baseDir))
            if (Files.exists(Paths.get(baseDir))) FileUtils.deleteDirectory(new File(baseDir));
            if (Files.exists(Paths.get(answerDir))) FileUtils.deleteDirectory(new File(answerDir));
            if (Files.exists(Paths.get(localGvcfDir))) FileUtils.deleteDirectory(new File(localGvcfDir));
            Files.createDirectories(Paths.get(baseDir));
            //Create DefaultDirectory
            new MakeDefaultDirectoryPlugin()
                    .workingDir(baseDir)
                    .performFunction(null);
            Files.createDirectories(Paths.get(alignDir));
            Files.createDirectories(Paths.get(pangenomeDir));
            Files.createDirectories(Paths.get(localGvcfDir));
            Files.createDirectories(Paths.get(smallSeqBaseRemoteGvcfDir));
            
            myLogger.info("calling CreateTestGenomes.create to make the data sets");
            // Begin data set creation.
            CreateTestGenomes.create(geneLength(),interGeneLength(), refInterGeneDup(), refInterGeneDelete(),numberOfGenes(), 
                    readLength(),hapDivergence(), interHapDivergence(), wgsDepth(), gbsDepth(), refGeneInsert());
           
        } catch (IOException exc) {
            System.err.println("Errors in creating genomes");
            exc.printStackTrace();
        }

        return null;
    }
    
    
    public CreateSmallGenomesPlugin() {
        super(null, false);
    }

    public CreateSmallGenomesPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public CreateSmallGenomesPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
    
    @Override
    public ImageIcon getIcon() {
        
        return null;
    }

    @Override
    public String getButtonName() {       
        return ("CreateTestGenomes");
    }

    @Override
    public String getToolTipText() {        
        return ("Creates a set of small sequence genomes for testing with the PHG");
    }
    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
     public static void main(String[] args) {
         GeneratePluginCode.generate(CreateSmallGenomesPlugin.class);
     }
     
     /**
      * Number of base pairs to include for each gene region
      *
      * @return Length Of Genes
      */
     public Integer geneLength() {
         return geneLength.value();
     }

     /**
      * Set Length Of Genes. Number of base pairs to include
      * for each gene region
      *
      * @param value Length Of Genes
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin geneLength(Integer value) {
         geneLength = new PluginParameter<>(geneLength, value);
         return this;
     }

     /**
      * Number of base pairs to include for each intergenic
      * region
      *
      * @return Length Of Inter Genes
      */
     public Integer interGeneLength() {
         return interGeneLength.value();
     }

     /**
      * Set Length Of Inter Genes. Number of base pairs to
      * include for each intergenic region
      *
      * @param value Length Of Inter Genes
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin interGeneLength(Integer value) {
         interGeneLength = new PluginParameter<>(interGeneLength, value);
         return this;
     }

     /**
      * Number of gene regions to create
      *
      * @return Number Of Genes
      */
     public Integer numberOfGenes() {
         return numberOfGenes.value();
     }

     /**
      * Set Number Of Genes. Number of gene regions to create
      *
      * @param value Number Of Genes
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin numberOfGenes(Integer value) {
         numberOfGenes = new PluginParameter<>(numberOfGenes, value);
         return this;
     }

     /**
      * Desired length of reads for fastq files
      *
      * @return Length Of Reads
      */
     public Integer readLength() {
         return readLength.value();
     }

     /**
      * Set Length Of Reads. Desired length of reads for fastq
      * files
      *
      * @param value Length Of Reads
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin readLength(Integer value) {
         readLength = new PluginParameter<>(readLength, value);
         return this;
     }

     /**
      * Percent each haplotype sequence should diverge from
      * reference
      *
      * @return Haplotype Divergence
      */
     public Integer hapDivergence() {
         return hapDivergence.value();
     }

     /**
      * Set Haplotype Divergence. Percent each haplotype sequence
      * should diverge from reference
      *
      * @param value Haplotype Divergence
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin hapDivergence(Integer value) {
         hapDivergence = new PluginParameter<>(hapDivergence, value);
         return this;
     }

     /**
      * Percent each haplotype sequence should diverge from
      * reference
      *
      * @return Inter-Haplotype Divergence
      */
     public Integer interHapDivergence() {
         return interHapDivergence.value();
     }

     /**
      * Set Inter-Haplotype Divergence. Percent each haplotype
      * sequence should diverge from reference
      *
      * @param value Inter-Haplotype Divergence
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin interHapDivergence(Integer value) {
         interHapDivergence = new PluginParameter<>(interHapDivergence, value);
         return this;
     }

     /**
      * Proportion of the refererence inter-gene sequence to
      * duplicate
      *
      * @return Proportion of InterGene Duplicate
      */
     public Double refInterGeneDup() {
         return refInterGeneDup.value();
     }

     /**
      * Set Proportion of InterGene Duplicate. Proportion of
      * the refererence inter-gene sequence to duplicate
      *
      * @param value Proportion of InterGene Duplicate
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin refInterGeneDup(Double value) {
         refInterGeneDup = new PluginParameter<>(refInterGeneDup, value);
         return this;
     }

     /**
      * Proportion of the reference inter-gene sequence to
      * delete
      *
      * @return Proportion of InterGene Deleted
      */
     public Double refInterGeneDelete() {
         return refInterGeneDelete.value();
     }

     /**
      * Set Proportion of InterGene Deleted. Proportion of
      * the reference inter-gene sequence to delete
      *
      * @param value Proportion of InterGene Deleted
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin refInterGeneDelete(Double value) {
         refInterGeneDelete = new PluginParameter<>(refInterGeneDelete, value);
         return this;
     }
     
     
     /**
      * Proportion of gene that will get an insertion
      *
      * @return Proportion of InterGene Deleted
      */
     public Double refGeneInsert() {
         return refGeneInsert.value();
     }

     /**
      * Set Proportion of gene that will get an insertion
      * 
      * @param value length of insertions
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin refGeneInsert(Double value) {
         refGeneInsert = new PluginParameter<>(refGeneInsert, value);
         return this;
     }
     /**
      * Depth for WGS 
      *
      * @return WGS Depth
      */
     public Double wgsDepth() {
         return wgsDepth.value();
     }

     /**
      * Set WGS Depth. Depth for WGS 
      *
      * @param value WGS Depth
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin wgsDepth(Double value) {
         wgsDepth = new PluginParameter<>(wgsDepth, value);
         return this;
     }

     /**
      * Depth for GBS
      *
      * @return GBS Depth
      */
     public Double gbsDepth() {
         return gbsDepth.value();
     }

     /**
      * Set GBS Depth. Depth for GBS
      *
      * @param value GBS Depth
      *
      * @return this plugin
      */
     public CreateSmallGenomesPlugin gbsDepth(Double value) {
         gbsDepth = new PluginParameter<>(gbsDepth, value);
         return this;
     }

    /**
     * The base directory for the small genomes data.
     *
     * @return Base Dir
     */
    public String baseDirectory() {
        if (baseDirectory.value() == null) return null;
        if (baseDirectory.value().endsWith("/")) return baseDirectory.value();
        return baseDirectory.value() + "/";
    }

    /**
     * Set Base Dir. The base directory for the small genomes
     * data.
     *
     * @param value Base Dir
     *
     * @return this plugin
     */
    public CreateSmallGenomesPlugin baseDirectory(String value) {
        baseDirectory = new PluginParameter<>(baseDirectory, value);
        return this;
    }
}
