@file:JvmName("HaplotypeGraphProtobufIO")

package net.maizegenetics.pangenome.io

import com.google.common.collect.ImmutableSet
import it.unimi.dsi.fastutil.ints.Int2ObjectMap
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.io.Haplotypegraph.HaplotypeGraphProto
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.io.File
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.inputStream
import kotlin.io.path.outputStream

private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.io.HaplotypeGraphProtobufIO")

//
// Code for exporting the Protobuf file
//

/**
 * Export the HaplotypeGraph to a Protobuf file
 */
fun toProtobuf(
    graph: HaplotypeGraph,
    outputFileName: String,
    includeSequences: Boolean = true,
    includeEdges: Boolean = true,
    localGVCFFolder: String? = null
) {

    val startTime = System.nanoTime()

    val filename = Utils.addSuffixIfNeeded(outputFileName, ".proto")
    val path = Path(filename)

    val haplotypeGraphBuilder = if (!path.exists()) {
        myLogger.info("toProtobuf: Writing graph to protobuf file: $filename")
        haplotypeGraphProto {}.toBuilder()
    } else {
        myLogger.error("File already exists: $filename")
        throw IllegalStateException("toProtobuf: File already exists: $filename")
    }

    path.outputStream().use { writer ->

        graph.referenceRangeList().forEach { range ->
            haplotypeGraphBuilder.addReferenceRanges(rangeProto(graph, range))
        }

        VariantUtils.getGvcfRemoteToLocalFiles()
            .filter { (_, path) -> path != null }
            .forEach { (id, path) ->
                val gvcfFilename = if (localGVCFFolder != null) {
                    val filename: String = File(path).name
                    "$localGVCFFolder/$filename"
                } else {
                    path
                }
                haplotypeGraphBuilder.addGvcfIdToFile(gvcfIDToFileProto(id, gvcfFilename))
            }

        graph.nodeStream()
            .forEach { node ->
                haplotypeGraphBuilder.addHaplotypeNodes(nodeProto(node))
            }

        if (includeEdges) {
            graph.nodeStream()
                .forEach { node ->
                    graph.rightEdges(node).forEach { edge ->
                        haplotypeGraphBuilder.addHaplotypeEdges(hapotypeEdgeProto(edge))
                    }
                }
        }

        haplotypeGraphBuilder.build().writeTo(writer)

        myLogger.info("toProtobuf: Time to write graph to protobuf file: ${(System.nanoTime() - startTime) / 1e9} seconds")

        if (includeSequences && graph.hasSequences()) {
            val sequencesFile = "${filename.removeSuffix(".proto")}.fa.gz"
            writeFastaBgZip(graph, sequencesFile)
        }

    }

}

/**
 * Convert a ReferenceRange to a ReferenceRangeProto
 */
private fun rangeProto(graph: HaplotypeGraph, range: ReferenceRange): Haplotypegraph.ReferenceRangeProto {

    val result = Haplotypegraph.ReferenceRangeProto.newBuilder()
        .setId(range.id())
        .setReferenceName(range.referenceName())
        .setChromosome(range.chromosome().name)
        .setStart(range.start())
        .setEnd(range.end())

    range.groupMethods().forEach { method ->
        result.addMethods(method)
    }

    result.addAllMethods(range.groupMethods().map { it })

    return result.build()

}

/**
 * Convert a HaplotypeNode to a HaplotypeNodeProto
 */
private fun nodeProto(node: HaplotypeNode): Haplotypegraph.HaplotypeNodeProto {

    val result = Haplotypegraph.HaplotypeNodeProto.newBuilder()
        .setHapId(node.id())
        .setHaplotypeSequence(sequenceProto(node.haplotypeSequence()))
        .setGenomeFileId(node.genomeFileID())
        .setGvcfFileId(node.gvcfFileID())
        .setAsmContig(node.asmContig())
        .setAsmStartCoordinate(node.asmStart())
        .setAsmEndCoordinate(node.asmEnd())
        .setAsmStrand(node.asmStrand())

    result.addAllTaxa(node.taxaList().map { it.name })

    return result.build()

}

/**
 * Convert a HaplotypeSequence to a HaplotypeSequenceProto
 */
private fun sequenceProto(sequence: HaplotypeSequence): Haplotypegraph.HaplotypeSequenceProto {

    return Haplotypegraph.HaplotypeSequenceProto.newBuilder()
        // The sequence if stored is put into a bgzipped fasta file
        .setLength(sequence.length())
        .setReferenceRangeID(sequence.referenceRange().id())
        .setQualityScore(sequence.qualityScore())
        .setSeqHash(sequence.seqHash())
        .build()

}

/**
 * Convert a GenomeIDToFileProto to a GenomeIDToFileProto
 */
private fun gvcfIDToFileProto(id: Int, path: String): Haplotypegraph.GVCFIDToFileProto {

    return Haplotypegraph.GVCFIDToFileProto.newBuilder()
        .setId(id)
        .setGvcfFile(path)
        .build()

}

/**
 * Convert a HaplotypeEdge to a HaplotypeEdgeProto
 */
private fun hapotypeEdgeProto(edge: HaplotypeEdge): Haplotypegraph.HaplotypeEdgeProto {

    return Haplotypegraph.HaplotypeEdgeProto.newBuilder()
        .setLeftHapId(edge.leftHapNode().id())
        .setRightHapId(edge.rightHapNode().id())
        .setEdgeProbability(edge.edgeProbability())
        .build()

}

//
// Code for reading the Protobuf file
//

/**
 * Read the HaplotypeGraph from a Protobuf file
 */
fun toGraph(
    protobufFilename: String,
    includeSequences: Boolean = true,
    includeEdges: Boolean = true,
    localGVCFFolder: String? = null
): HaplotypeGraph {

    myLogger.info("Reading graph from protobuf file: $protobufFilename")

    val sequencesFile = "${protobufFilename.removeSuffix(".proto")}.fa.gz"

    val referenceSequenceFile = if (includeSequences && File(sequencesFile).exists()) {
        myLogger.info("Reading graph sequences from fasta file: $sequencesFile")
        readFastaBgZip(sequencesFile)
    } else null

    Path(protobufFilename).inputStream().use { input ->

        val proto = HaplotypeGraphProto.parseFrom(input)
        val referenceRangeMap = referenceRangeMap(proto.referenceRangesList)
        val gvcfIDToFileMap = gvcfIDToFileMap(proto.gvcfIdToFileList, localGVCFFolder)
        VariantUtils.setGvcfRemoteToLocalFiles(gvcfIDToFileMap)

        val haplotypeNodeChannel = Channel<Deferred<List<HaplotypeNode>>>(300)

        // Starts the processing of the HaplotypeNodes in separate threads
        CoroutineScope(Dispatchers.IO).launch {
            haplotypeNodeMap(proto.haplotypeNodesList, referenceRangeMap, referenceSequenceFile, haplotypeNodeChannel)
        }

        // Waits for the HaplotypeNodes to be processed and returns the map
        val haplotypeNodeMap = runBlocking { getMapFromChannel(haplotypeNodeChannel) }

        val edges = if (includeEdges) edges(proto.haplotypeEdgesList, haplotypeNodeMap) else emptyList()

        if (edges.isEmpty()) { // If there are no edges, then let HaplotypeGraph create them
            val result = TreeMap<ReferenceRange, MutableList<HaplotypeNode>>()
            haplotypeNodeMap.values.forEach { node ->
                val range = node.haplotypeSequence().referenceRange()
                val nodes = result.getOrDefault(range, mutableListOf())
                nodes.add(node)
                result[range] = nodes
            }
            return HaplotypeGraph(result)
        } else { // Otherwise, use the edges to create the graph
            return HaplotypeGraph(edges)
        }

    }

}

private fun gvcfIDToFileMap(
    gvcfIDToFileList: List<Haplotypegraph.GVCFIDToFileProto>,
    localGVCFFolder: String? = null
): Map<Int, String> {

    return gvcfIDToFileList.associate { gvcfIDToFile ->
        val gvcfFilename = if (localGVCFFolder != null) {
            val filename: String = File(gvcfIDToFile.gvcfFile).name
            "$localGVCFFolder/$filename"
        } else {
            gvcfIDToFile.gvcfFile
        }
        Pair(gvcfIDToFile.id, gvcfFilename)
    }.toMutableMap()

}

/**
 * Convert a list of HaplotypeEdgeProto to a list of HaplotypeEdge
 */
private fun referenceRangeMap(ranges: List<Haplotypegraph.ReferenceRangeProto>): Map<Int, ReferenceRange> {

    return ranges.associate { range ->
        Pair(
            range.id,
            ReferenceRange(
                range.referenceName,
                Chromosome.instance(range.chromosome),
                range.start,
                range.end,
                range.id,
                ImmutableSet.copyOf(range.methodsList)
            )
        )
    }

}

/**
 * Convert a list of HaplotypeNodeProto to a list of HaplotypeNode
 */
private suspend fun haplotypeNodeMap(
    nodes: List<Haplotypegraph.HaplotypeNodeProto>,
    referenceRangeMap: Map<Int, ReferenceRange>,
    sequencesMap: Int2ObjectMap<String>? = null,
    haplotypeNodeChannel: Channel<Deferred<List<HaplotypeNode>>>
) = withContext(Dispatchers.Default) {

    // public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, String asmContig,
    // int asmStart, int asmEnd, String asmStrand, int genomeFileID, int gvcfFileID, Map<Integer,String> gvcfIdToFilePath) {

    val numHaplotypeNodesPerCoroutine = 100

    nodes.windowed(size = numHaplotypeNodesPerCoroutine, step = numHaplotypeNodesPerCoroutine, partialWindows = true)
        .forEach { windowedNodes ->
            haplotypeNodeChannel.send(async {
                haplotypeNode(windowedNodes, referenceRangeMap, sequencesMap)
            })
        }

    haplotypeNodeChannel.close()

}

private fun haplotypeNode(
    nodes: List<Haplotypegraph.HaplotypeNodeProto>,
    referenceRangeMap: Map<Int, ReferenceRange>,
    sequencesMap: Int2ObjectMap<String>? = null
): List<HaplotypeNode> {

    return nodes.map { node ->
        haplotypeNode(node, referenceRangeMap, sequencesMap)
    }

}

/**
 * Convert a HaplotypeNodeProto to a HaplotypeNode
 */
private fun haplotypeNode(
    node: Haplotypegraph.HaplotypeNodeProto,
    referenceRangeMap: Map<Int, ReferenceRange>,
    sequencesMap: Int2ObjectMap<String>? = null
): HaplotypeNode {

    return HaplotypeNode(
        sequence(node.haplotypeSequence, referenceRangeMap, sequencesMap?.get(node.hapId)),
        TaxaListBuilder().addAll(node.taxaList).build(),
        node.hapId,
        node.asmContig,
        node.asmStartCoordinate,
        node.asmEndCoordinate,
        node.asmStrand,
        node.genomeFileId,
        node.gvcfFileId
    )

}

/**
 * Create Map<hapid, HaplotypeNode> from the channel
 * as the HaplotypeNodes are processed
 */
private suspend fun getMapFromChannel(haplotypeNodeChannel: Channel<Deferred<List<HaplotypeNode>>>): Map<Int, HaplotypeNode> {

    val result = mutableMapOf<Int, HaplotypeNode>()

    for (deferredNodes in haplotypeNodeChannel) {
        deferredNodes.await().forEach { node ->
            result[node.id()] = node
        }
    }

    return result

}

/**
 * Convert a HaplotypeSequenceProto to a HaplotypeSequence
 */
private fun sequence(
    sequence: Haplotypegraph.HaplotypeSequenceProto,
    referenceRangeMap: Map<Int, ReferenceRange>,
    genotypeSequence: String? = null
): HaplotypeSequence {

    // public static HaplotypeSequence getInstance(String sequence, ReferenceRange referenceRange, double qualityScore, String seqHash) {

    return HaplotypeSequence.getInstance(
        genotypeSequence,
        referenceRangeMap[sequence.referenceRangeID]!!,
        sequence.qualityScore,
        sequence.seqHash
    )

}

/**
 * Convert a list of HaplotypeEdgeProto to a list of HaplotypeEdge
 */
private fun edges(
    edges: List<Haplotypegraph.HaplotypeEdgeProto>,
    haplotypeNodeMap: Map<Int, HaplotypeNode>
): List<HaplotypeEdge> {

    return edges.map { edge ->
        HaplotypeEdge(haplotypeNodeMap[edge.leftHapId], haplotypeNodeMap[edge.rightHapId], edge.edgeProbability)
    }

}
