package net.maizegenetics.pangenome.smallseq

import java.nio.file.Paths
import org.junit.jupiter.api.Test
import net.maizegenetics.junit.SmallSeqExtension
import org.junit.jupiter.api.Assertions.*
import net.maizegenetics.util.DirectoryCrawler
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.BeforeEach

/**
 * This test class verifies the CreateSmallGenomesPlugin will create small sequence
 * files when invoked.  It can also be used to create various test sets with
 * non-default parameters.
 *
 * @author lcj34
 */
class CreateSmallGenomesPluginTest {

    private val answerDir = "${SmallSeqExtension.testTmpDir}/answer"
    private val dataDir = "${SmallSeqExtension.testTmpDir}/dockerBaseDir/data"
    private val inputFileGlob = "glob:*{.fa,.fq,fq.gz,fastq,fastq.txt,fastq.gz,fastq.txt.gz,.vcf,.txt,.bed}"

    @BeforeEach
    fun cleanTmp() {
        SmallSeqExtension.testTmpDirFile.mkdirs()
        FileUtils.cleanDirectory(SmallSeqExtension.testTmpDirFile)
    }

    @Test
    fun testWithDefaultParams() {
        CreateSmallGenomesPlugin()
            .baseDirectory(SmallSeqExtension.testTmpDir)
            .performFunction(null)

        val dataFiles = DirectoryCrawler.listPaths(inputFileGlob, Paths.get(dataDir).toAbsolutePath())
        assertEquals(14, dataFiles.size)

        val dataFilenames = arrayListOf<String>()
        dataFiles.forEach {
            dataFilenames.add(it.fileName.toString())
        }
        assertTrue(dataFilenames.contains("LineA1_R1.fastq"))
        assertTrue(dataFilenames.contains("LineB1_R1_gbs.fastq"))
        assertFalse(dataFilenames.contains("configSQLite.txt"))

        val answerFiles = DirectoryCrawler.listPaths(inputFileGlob, Paths.get(answerDir).toAbsolutePath())
        assertEquals(12, answerFiles.size)

        val answerFilenames = arrayListOf<String>()
        answerFiles.forEach {
            answerFilenames.add(it.fileName.toString())
        }
        assertTrue(answerFilenames.contains("LineA1.fa"))
        assertTrue(answerFilenames.contains("SmallSeq.vcf"))
        assertTrue(answerFilenames.contains("anchors.bed"))
    }
    
    @Test
    fun testProvideParams() {
        val lengthOfGenes = 2048
        val lengthOfInterGenes = 2048
        val numberOfGenes = 10

        CreateSmallGenomesPlugin()
            .geneLength(lengthOfGenes)
            .interGeneLength(lengthOfInterGenes)
            .numberOfGenes(numberOfGenes)
            .wgsDepth(10.0)
            .gbsDepth(0.5)
            .baseDirectory(SmallSeqExtension.testTmpDir)
            .performFunction(null)

        val dataFiles = DirectoryCrawler.listPaths(inputFileGlob, Paths.get(dataDir).toAbsolutePath())
        assertEquals(28, dataFiles.size)

        val dataFilenames = arrayListOf<String>()
        dataFiles.forEach {
            dataFilenames.add(it.fileName.toString())
        }
        assertTrue(dataFilenames.contains("LineA1_R1.fastq"))
        assertTrue(dataFilenames.contains("LineB1_R1_gbs.fastq"))
        assertFalse(dataFilenames.contains("configSQLiteDocker.txt"))

        val answerFiles = DirectoryCrawler.listPaths(inputFileGlob, Paths.get(answerDir).toAbsolutePath())
        assertEquals(19, answerFiles.size)

        val answerFilenames = arrayListOf<String>()
        answerFiles.forEach {
            answerFilenames.add(it.fileName.toString())
        }
        assertTrue(answerFilenames.contains("LineA1.fa"))
        assertTrue(answerFilenames.contains("SmallSeq.vcf"))
        assertTrue(answerFilenames.contains("anchors.bed"))
    }
}
