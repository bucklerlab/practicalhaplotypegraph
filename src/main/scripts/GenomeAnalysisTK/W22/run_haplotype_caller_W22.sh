#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/W22_HGCJCBGXX.bam \
   -I /SSD/terry/haplotype_caller/bam_files/W22_HVMFTCCXX_L8.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/W22_HVMFTCCXX_L7.clean_srt_dedup.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o W22_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant W22_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o W22_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V W22_haplotype_caller_output.vcf \
   -o W22_haplotype_caller_output.fasta

