package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.BufferedWriter
import java.io.File
import javax.swing.ImageIcon

/**
 * Plugin to combine all fastq files in the input -fastqDir into sets of combined Fastqs.
 * he number to be batched together is set by -numToMerge.  It will also generate a template script for running minimap2
 * If -useOriginalReadNames is set to true, the fastq records will not be renamed.
 * This should only be used if the read names are unique in each fastq.  Otherwise minimap2 will mix samples.
 *
 * It will also output a Grouping file which is used in MultisampleBAMToMappingPlugin so the PHG knows how to split back out the alignments for the reads.
 */
class MergeFastqPlugin(parentFrame: Frame?=null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(MergeFastqPlugin::class.java)

    private var fastqDir = PluginParameter.Builder("fastqDir", null, String::class.java)
            .guiName("Fastq dir to process")
            .inDir()
            .required(true)
            .description("Name of the Fastq dir to process.")
            .build()

    private var outputMergedFastqDir = PluginParameter.Builder("outputDir", null, String::class.java)
            .guiName("Directory to write out merged fastq files.")
            .outDir()
            .required(true)
            .description("Directory to write out the fastq files.")
            .build()

    private var outputBAMDir = PluginParameter.Builder("outputBAMDir", "<bamFolder>", String::class.java)
            .guiName("Output BAM Directory")
            .outDir()
            .required(false)
            .description("Directory to point the script Template to for BAM writing.  If not specified, <bamFolder> will be added to the script and will need to be updated.")
            .build()

    private var makeBAMDir = PluginParameter.Builder("makeBAMDir", true, Boolean::class.javaObjectType)
            .guiName("Make BAM Directory")
            .required(false)
            .description("Make the BAM Directory by adding a mkdir command to the script.")
            .build()

    private var outputGroupingFile = PluginParameter.Builder("outputGroupingFile", null, String::class.java)
            .guiName("File To output the groupings which have been merged.")
            .outFile()
            .required(true)
            .description("File to write out the groupings.")
            .build()

    private var numberOfFastqsPerMerge = PluginParameter.Builder("numToMerge", 50, Int::class.javaObjectType)
            .guiName("Number of Fastas To Merge")
            .required(false)
            .description("Number of fastqs to merge into a single fastq")
            .build()


    private var scriptTemplate = PluginParameter.Builder("scriptTemplate","runMinimapTemp.sh", String::class.java)
            .guiName("Template Script Name")
            .outFile()
            .required(false)
            .description("Generated Template Script for future minimap runs.  This makes it easier to run minimap on the merged fastq files.  Do not specify the script extension(.sh)")
            .build()

    private var numberOfScripts = PluginParameter.Builder("numberOfScriptOutputs", 1, Int::class.javaObjectType)
            .guiName("Number of Scripts to output")
            .required(false)
            .description("Number of script templates to output.  Will append _1.sh to the file name specified in -scriptTemplate.  Fastq Batches will be evenly distributed across scripts.")
            .build()

    private var numberOfThreads = PluginParameter.Builder("numThreads", 20, Int::class.javaObjectType)
            .guiName("Number of Threads")
            .required(false)
            .description("Number of to setup in the script")
            .build()

    private var minimapLocation = PluginParameter.Builder("minimapLocation", "minimap2", String::class.java)
            .guiName("Location of Minimap2 Executable")
            .required(false)
            .description("Location of Minimap2 on file system.  This defaults to use minimap2 if it is on the PATH environment variable.")
            .build()

    private var indexFile = PluginParameter.Builder("minimap2IndexFile", "<refIndex>", String::class.java)
            .guiName("Minimap2 index file for pangenome")
            .required(false)
            .description("Name of the indexFile file for writing to scriptTemplate.  If not specified, <refIndex> will be written to the script and will need to be updated.")
            .build()

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, and filename.  Optionally for paired end reads, filename2 is needed.  " +
                    "If filename2 is not supplied, Minimap2 will be setup to run in single end mode.  Otherwise will be paired.")
            .build()


    private var useOriginalReadNames = PluginParameter.Builder("useOriginalReadNames", false, Boolean::class.javaObjectType)
            .guiName("Use Original Read Names")
            .required(false)
            .description("Use the original ReadNames instead of a running count")
            .build()

    private var minimapNParam = PluginParameter.Builder("minimapN", 60, Int::class.javaObjectType)
        .guiName("Minimap -N parameter")
        .description("Set this to increase the N parameter used in Minimap2")
        .required(false)
        .build()

    private var minimapFParam = PluginParameter.Builder("minimapf", "5000,6000", String::class.java)
        .guiName("Minimap -f Parameter")
        .description("Set this to change the -f parameter in minimap2")
        .required(false)
        .build()

    private var outputSamFiles = PluginParameter.Builder("outputSams", false, Boolean::class.javaObjectType)
        .guiName("Output SAM Files")
        .description("Output SAM files instead of BAM files")
        .required(false)
        .build()

    override fun processData(input: DataSet?): DataSet? {

        //walk through each file
        val filesGrouped = File(fastqDir()).walkTopDown()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .sorted()
                .windowed(numberOfFastqsPerMerge(), numberOfFastqsPerMerge(), true)
                .toList()

        Utils.getBufferedWriter(outputGroupingFile()).use { outputFile ->
            val scriptWriters = (0 until numberOfScripts()).map { "${scriptTemplate()}_${it}.sh" }.map { Utils.getBufferedWriter(it) }

            //Initialize the output scripts to have mkdirs if requested.
            for(outputScript in scriptWriters) {
                outputScript.write("#!/bin/bash -x\n")

                if(makeBAMDir()) {
                    myLogger.info("Creating ${outputBAMDir()}")
                    File(outputBAMDir()).mkdirs()
                }
            }

            outputFile.write("originalFile\tnewFile\tBatch\tnumReads\tstartRead\tendRead\n")
            val totalBatchCount = filesGrouped.size
            for ((index, fastqFileList) in filesGrouped.withIndex()) {
                myLogger.info("Writing out batch ${index} out of ${totalBatchCount}")
                val fastqOutputName = "mergedFastq_batch_${index}.fq"
                val outputExt = if(outputSamFiles()) ".sam" else ".bam"
                val bamOutputName = "mergedFastq_batch_${index}${outputExt}"
                mergeFiles(fastqOutputName, outputFile, fastqFileList, index)
                val outputScript = scriptWriters[index % numberOfScripts()]
//                outputScript.write("time ${minimapLocation()} -a -x sr -t ${numberOfThreads()} --secondary=yes -N50 -f1000,5000 --eqx -Q ${indexFile()} ${outputMergedFastqDir()}${fastqOutputName} | samtools view -b > ${outputBAMDir()}/${bamOutputName}\n")
//                outputScript.write("time ${minimapLocation()} -a -x sr -t ${numberOfThreads()} --secondary=yes -N${minimapNParam()} -f${minimapFParam()} --eqx -Q ${indexFile()} ${outputMergedFastqDir()}${fastqOutputName} | samtools view -b > ${outputBAMDir()}/${bamOutputName}\n")

                if(outputSamFiles()) {
                    outputScript.write("time ${minimapLocation()} -a -x sr -t ${numberOfThreads()} --secondary=yes -N${minimapNParam()} -f${minimapFParam()} --eqx -Q ${indexFile()} ${outputMergedFastqDir()}${fastqOutputName} > ${outputBAMDir()}/${bamOutputName}\n")
                }
                else {
                    outputScript.write("time ${minimapLocation()} -a -x sr -t ${numberOfThreads()} --secondary=yes -N${minimapNParam()} -f${minimapFParam()} --eqx -Q ${indexFile()} ${outputMergedFastqDir()}${fastqOutputName} | samtools view -b > ${outputBAMDir()}/${bamOutputName}\n")
                }
            }
            //Close them all out
            for(outputScript in scriptWriters) {
                outputScript.close()
            }
        }



        return null
    }


    fun mergeUsingKeyFile(fastqDirectory : String, inputKeyFile : String) {
        val fileSetInDir = File(fastqDirectory).walkTopDown()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .map { it.name }
                .toHashSet()


        val keyFileAndHeader = readInKeyFile(inputKeyFile)

        val header = keyFileAndHeader.first
        val keyFileLines = keyFileAndHeader.second

        //Get out the column indices as they will be consistent for the whole file
        val taxonCol = header["cultivar"]?:-1
        val fileNameCol1 = header["filename"]?:-1
        val fileNameCol2 = if (header.containsKey("filename2")) header["filename2"]?:-1 else -1
        val flowcellCol = header["flowcell_lane"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(taxonCol != -1) { "Error processing keyfile.  Must have cultivar column." }
        check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
        check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }


        //Check to see if we have both paired and single end.  If so throw warning and tell user to split out keyfile

        //figure out which keyFile entries are single
        val singleEndFiles = when (fileNameCol2) {
            -1 -> {
                keyFileLines
            }
            else -> {
                keyFileLines.filter { keyFileLines[fileNameCol2].isNotEmpty() }
            }
        }
        .map { it[fileNameCol1] }

        //Check the files and throw warnings if you do not find any files.
        singleEndFiles.filter { !fileSetInDir.contains(it) }
                .forEach { myLogger.warn("Missing Fastq File ${it} found in keyFile:${inputKeyFile} but not Directory:${fastqDirectory}") }

        val singleEndFilesBatched = singleEndFiles.filter { fileSetInDir.contains(it) }
                .map { "${fastqDirectory}/${it}" }
                .windowed(numberOfFastqsPerMerge(), numberOfFastqsPerMerge(), true)
                .toList()

        //Merge the single end files together



    }

    fun mergeFiles(mergedFileName : String, outputGroupingWriter : BufferedWriter, fileList : List<File>, batchId:Int) {
        Utils.getBufferedWriter("${outputMergedFastqDir()}${mergedFileName}").use { outputMergedFastaWriter ->
            var readNumber = 0
            for(fastqFile in fileList) {
                var totalReadNumForBatch = 0
                val startRead = readNumber
                Utils.getBufferedReader(fastqFile.toString()).use { reader ->
                    var line:String? = ""//reader.readLine()
                    var lineNumber = 1
                    while (line != null) {
                        val readName = reader.readLine() //header
                        if(readName == null || readName.isEmpty()) {
                            break
                        }
                        val seq = reader.readLine()
                        val separator = reader.readLine()
                        val qual = reader.readLine()

                        //make sure seq.length and qual.length are the same
                        if(qual != null && seq.length == qual.length) {
                            //We can write everything
                            if(useOriginalReadNames()) {
                                outputMergedFastaWriter.write("${readName}\n")
                            }
                            else {
                                outputMergedFastaWriter.write("@${readNumber}\n")
                            }
                            outputMergedFastaWriter.write("${seq}\n")
                            outputMergedFastaWriter.write("${separator}\n")
                            outputMergedFastaWriter.write("${qual}\n")

                            readNumber++
                            totalReadNumForBatch++
                        }
                        else {
                            myLogger.warn("Error processing read ${readName} in file ${fastqFile.name}.  Sequence and quality length do not match.  Skipping read.")
                        }

                        line = qual
                    }
                }
                outputGroupingWriter.write("${fastqFile.name}\t${mergedFileName}\t${batchId}\t${totalReadNumForBatch}\t${startRead}\t${readNumber-1}\n")
            }

        }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MergeFastqPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MergeFastqPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Merge Fastqs"
    }
    /**
     * Name of the Fastq dir to process.
     *
     * @return Fastq dir to process
     */
    fun fastqDir(): String {
        return fastqDir.value()
    }

    /**
     * Set Fastq dir to process. Name of the Fastq dir to
     * process.
     *
     * @param value Fastq dir to process
     *
     * @return this plugin
     */
    fun fastqDir(value: String): MergeFastqPlugin {
        fastqDir = PluginParameter<String>(fastqDir, value)
        return this
    }

    /**
     * Directory to write out the fastq files.
     *
     * @return Directory to write out merged fastq files.
     */
    fun outputMergedFastqDir(): String {
        return outputMergedFastqDir.value()
    }

    /**
     * Set Directory to write out merged fastq files.. Directory
     * to write out the fastq files.
     *
     * @param value Directory to write out merged fastq files.
     *
     * @return this plugin
     */
    fun outputMergedFastqDir(value: String): MergeFastqPlugin {
        outputMergedFastqDir = PluginParameter<String>(outputMergedFastqDir, value)
        return this
    }

    /**
     * Directory to point the script Template to for BAM writing.
     *  If not specified, <bamFolder> will be added to the
     * script and will need to be updated.
     *
     * @return Output BAM Directory
     */
    fun outputBAMDir(): String {
        return outputBAMDir.value()
    }

    /**
     * Set Output BAM Directory. Directory to point the script
     * Template to for BAM writing.  If not specified, <bamFolder>
     * will be added to the script and will need to be updated.
     *
     * @param value Output BAM Directory
     *
     * @return this plugin
     */
    fun outputBAMDir(value: String): MergeFastqPlugin {
        outputBAMDir = PluginParameter<String>(outputBAMDir, value)
        return this
    }

    /**
     * Make the BAM Directory by adding a mkdir command to
     * the script.
     *
     * @return Make BAM Directory
     */
    fun makeBAMDir(): Boolean {
        return makeBAMDir.value()
    }

    /**
     * Set Make BAM Directory. Make the BAM Directory by adding
     * a mkdir command to the script.
     *
     * @param value Make BAM Directory
     *
     * @return this plugin
     */
    fun makeBAMDir(value: Boolean): MergeFastqPlugin {
        makeBAMDir = PluginParameter<Boolean>(makeBAMDir, value)
        return this
    }

    /**
     * File to write out the groupings.
     *
     * @return File To output the groupings which have been merged.
     */
    fun outputGroupingFile(): String {
        return outputGroupingFile.value()
    }

    /**
     * Set File To output the groupings which have been merged..
     * File to write out the groupings.
     *
     * @param value File To output the groupings which have been merged.
     *
     * @return this plugin
     */
    fun outputGroupingFile(value: String): MergeFastqPlugin {
        outputGroupingFile = PluginParameter<String>(outputGroupingFile, value)
        return this
    }

    /**
     * Number of fastqs to merge into a single fastq
     *
     * @return Number of Fastas To Merge
     */
    fun numberOfFastqsPerMerge(): Int {
        return numberOfFastqsPerMerge.value()
    }

    /**
     * Set Number of Fastas To Merge. Number of fastqs to
     * merge into a single fastq
     *
     * @param value Number of Fastas To Merge
     *
     * @return this plugin
     */
    fun numberOfFastqsPerMerge(value: Int): MergeFastqPlugin {
        numberOfFastqsPerMerge = PluginParameter<Int>(numberOfFastqsPerMerge, value)
        return this
    }

    /**
     * Generated Template Script for future minimap runs.
     *  This makes it easier to run minimap on the merged
     * fastq files.  Do not specify the script extension(.sh)
     *
     * @return Template Script Name
     */
    fun scriptTemplate(): String {
        return scriptTemplate.value()
    }

    /**
     * Set Template Script Name. Generated Template Script
     * for future minimap runs.  This makes it easier to run
     * minimap on the merged fastq files.  Do not specify
     * the script extension(.sh)
     *
     * @param value Template Script Name
     *
     * @return this plugin
     */
    fun scriptTemplate(value: String): MergeFastqPlugin {
        scriptTemplate = PluginParameter<String>(scriptTemplate, value)
        return this
    }

    /**
     * Number of script templates to output.  Will append
     * _1.sh to the file name specified in -scriptTemplate.
     *  Fastq Batches will be evenly distributed across scripts.
     *
     * @return Number of Scripts to output
     */
    fun numberOfScripts(): Int {
        return numberOfScripts.value()
    }

    /**
     * Set Number of Scripts to output. Number of script templates
     * to output.  Will append _1.sh to the file name specified
     * in -scriptTemplate.  Fastq Batches will be evenly distributed
     * across scripts.
     *
     * @param value Number of Scripts to output
     *
     * @return this plugin
     */
    fun numberOfScripts(value: Int): MergeFastqPlugin {
        numberOfScripts = PluginParameter<Int>(numberOfScripts, value)
        return this
    }

    /**
     * Number of to setup in the script
     *
     * @return Number of Threads
     */
    fun numberOfThreads(): Int {
        return numberOfThreads.value()
    }

    /**
     * Set Number of Threads. Number of to setup in the script
     *
     * @param value Number of Threads
     *
     * @return this plugin
     */
    fun numberOfThreads(value: Int): MergeFastqPlugin {
        numberOfThreads = PluginParameter<Int>(numberOfThreads, value)
        return this
    }

    /**
     * Location of Minimap2 on file system.  This defaults
     * to use minimap2 if it is on the PATH environment variable.
     *
     * @return Location of Minimap2 Executable
     */
    fun minimapLocation(): String {
        return minimapLocation.value()
    }

    /**
     * Set Location of Minimap2 Executable. Location of Minimap2
     * on file system.  This defaults to use minimap2 if it
     * is on the PATH environment variable.
     *
     * @param value Location of Minimap2 Executable
     *
     * @return this plugin
     */
    fun minimapLocation(value: String): MergeFastqPlugin {
        minimapLocation = PluginParameter<String>(minimapLocation, value)
        return this
    }

    /**
     * Name of the indexFile file to for writing to scriptTemplate.
     *  If not specified, <refIndex> will be written to the
     * script and will need to be updated.
     *
     * @return Minimap2 index file for pangenome
     */
    fun indexFile(): String {
        return indexFile.value()
    }

    /**
     * Set Minimap2 index file for pangenome. Name of the
     * indexFile file to for writing to scriptTemplate.  If
     * not specified, <refIndex> will be written to the script
     * and will need to be updated.
     *
     * @param value Minimap2 index file for pangenome
     *
     * @return this plugin
     */
    fun indexFile(value: String): MergeFastqPlugin {
        indexFile = PluginParameter<String>(indexFile, value)
        return this
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will be setup to run in single
     * end mode.  Otherwise will be paired.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will be setup to run in single end mode.  Otherwise
     * will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): MergeFastqPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Use the original ReadNames instead of a running count
     *
     * @return Use Original Read Names
     */
    fun useOriginalReadNames(): Boolean {
        return useOriginalReadNames.value()
    }

    /**
     * Set Use Original Read Names. Use the original ReadNames
     * instead of a running count
     *
     * @param value Use Original Read Names
     *
     * @return this plugin
     */
    fun useOriginalReadNames(value: Boolean): MergeFastqPlugin {
        useOriginalReadNames = PluginParameter<Boolean>(useOriginalReadNames, value)
        return this
    }

    /**
     * Set this to increase the N parameter used in Minimap2
     *
     * @return Minimap -N parameter
     */
    fun minimapNParam(): Int {
        return minimapNParam.value()
    }

    /**
     * Set Minimap -N parameter. Set this to increase the
     * N parameter used in Minimap2
     *
     * @param value Minimap -N parameter
     *
     * @return this plugin
     */
    fun minimapNParam(value: Int): MergeFastqPlugin {
        minimapNParam = PluginParameter<Int>(minimapNParam, value)
        return this
    }

    /**
     * Set this to change the -f parameter in minimap2
     *
     * @return Minimap -f Parameter
     */
    fun minimapFParam(): String {
        return minimapFParam.value()
    }

    /**
     * Set Minimap -f Parameter. Set this to change the -f
     * parameter in minimap2
     *
     * @param value Minimap -f Parameter
     *
     * @return this plugin
     */
    fun minimapFParam(value: String): MergeFastqPlugin {
        minimapFParam = PluginParameter<String>(minimapFParam, value)
        return this
    }

    /**
     * Output SAM files instead of BAM files
     *
     * @return Output SAM Files
     */
    fun outputSamFiles(): Boolean {
        return outputSamFiles.value()
    }

    /**
     * Set Output SAM Files. Output SAM files instead of BAM
     * files
     *
     * @param value Output SAM Files
     *
     * @return this plugin
     */
    fun outputSamFiles(value: Boolean): MergeFastqPlugin {
        outputSamFiles = PluginParameter<Boolean>(outputSamFiles, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(MergeFastqPlugin::class.java)
}