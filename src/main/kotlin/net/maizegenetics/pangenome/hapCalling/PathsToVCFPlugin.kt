package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import htsjdk.variant.variantcontext.Allele
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.variantcontext.VariantContextBuilder
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.PositionList
import net.maizegenetics.dna.snp.GenotypeTable
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.HaplotypeNode.VariantInfo
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.util.*
import javax.swing.ImageIcon

/**
 * This exports diploid or haploid paths to a VCF file.
 *
 * Required Input:  This Plugin expects as input a DataSet containing a list of:
 *  -haplotypeGraph
 *  - Path list from ImportDiploidPathPlugin
 *
 * It is expected that ImportDiploidPathPlugin is run prior to running the PathsToVCFPlugin
 * The haplotypeGraph and PathList will be obtained from the output of ImportDiploidPathPlugin
 * which is streamed to this plugin.
 *
 * main thread get Paths (Either 1) haploid or diploid paths from DB, 2) use taxa from graph (TreeMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>)
 * main thread get Taxa List (List<String>)
 * multi-threaded foreach (ReferenceRange) -> variantContextChannel (Channel<Deferred<List<VariantContext>>>)
 *     get list of positions for all SNPs
 *     create a map foreach position that has taxon to VariantInfos (SortedMap<position: Int, Map<taxon: String, List<VariantInfo?>>>)
 *     foreach (position)
 *         create VariantContext (using htsjdk)
 *             haploid → homozygous unphased
 *             diploid → phased
 * main thread write VCF file
 *     Write VCF Header
 *     foreach (ReferenceRange)
 *         Write List<VariantContext> from variantContextChannel (Channel<Deferred<List<VariantContext>>>)
 *
 *
 * @author Terry Casstevens Created July 30, 2020
 */
class PathsToVCFPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(PathsToVCFPlugin::class.java)

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("Output file name")
            .guiName("Output VCF File Name")
            .required(true)
            .outFile()
            .build()

    private var myRefRangeForSNPFile = PluginParameter.Builder("refRangeFileVCF", null, String::class.java)
            .description("Reference Range file used to subset the paths for only specified regions of the genome.")
            .guiName("Reference Range File")
            .required(false)
            .inFile()
            .build()

    private var myRefGenome = PluginParameter.Builder("referenceFasta", null, String::class.java)
            .description("Reference Genome.")
            .guiName("Reference Genome")
            .required(false)
            .inFile()
            .build()

    private var myMakeDiploid = PluginParameter.Builder("makeDiploid", true, Boolean::class.javaObjectType)
            .description("Whether to report haploid paths as homozygousdiploid")
            .build()

    private var myPositions = PluginParameter.Builder("positions", null, PositionList::class.java)
            .description("Positions to include in VCF. Can be specified by Genotype file (i.e. VCF, Hapmap, etc.), bed file, or json file containing the requested positions.")
            .guiName("Position List")
            .required(false)
            .build()

    private var symbolicToN = PluginParameter.Builder("symbolicToN", false, Boolean::class.javaObjectType)
        .description("Convert symbolic alleles to N in the output. Symbolic alleles like <INS> and <NON-REF> are uncommon " +
                "in the vcf output but can occur. If true, these will be output as 'N'. Otherwise, they will be output as '.'")
        .build()

    private var symbolicOutput = PluginParameter.Builder("symbolic", false, Boolean::class.javaObjectType)
        .description("Write symbolic alleles to vcf. Symbolic alleles like <INS> and <NON-REF> are uncommon " +
                "in the vcf output but can occur. By default they will be converted to '.' because many vcf apps do not recognize them as valid. " +
                "If true, these will be output as <INS> or <NON-REF>. Otherwise, they will be output as '.'")
        .build()


    private lateinit var myGraph: HaplotypeGraph

    // Number of ranges included in export used for progress reporting
    private var myNumRanges = 0

    // Channel used to pass lists of VariantContext from multi-threaded code
    // creating them and single threaded code writing to VCF.
    // These are queued as a Deferred Job instead of completed
    // list of VariantContext to maintain the order positions in
    // the resulting VCF file.
    private val variantContextChannel = Channel<Deferred<List<VariantContext>>>(20)

    private var referenceSequence: GenomeSequence? = null

    override fun preProcessParameters(input: DataSet) {
        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size == 1) { "PathsToVCFPlugin: preProcessParameters: must input one HaplotypeGraph" }
        // From ImportDiploidPathPlugin
        myGraph = temp[0].data as HaplotypeGraph
    }

    override fun postProcessParameters() {
        outputFile(Utils.addSuffixIfNeeded(outputFile(), ".vcf"))
        require(positions() == null || refGenome() != null) { "Reference genome must be supplied when providing a position list" }

        if (symbolicOutput() && symbolicToN()) throw IllegalArgumentException("Cannot set both symbolic and symbolicToN flags to true. " +
                "The symbolic flag outputs symbolic alleles as is while the symbolicToN flag changes them to N")
    }

    override fun processData(input: DataSet): DataSet? {

        if (refGenome() != null) {
            referenceSequence = GenomeSequenceBuilder.instance(refGenome())
        }

        // Get optional list of Reference Ranges to export to the VCF file
        val refRangesToKeepSetOptional = parseRefRangeFile(refRangeForSNPFile())
        val refRangesToKeep = if (refRangesToKeepSetOptional.isPresent) refRangesToKeepSetOptional.get() else null

        // This will hold the path information in a different structure
        // for easier use later.
        // TreeMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>
        val rangeToTaxonToHapids = TreeMap<ReferenceRange, MutableMap<String, MutableList<Int>>>()

        // Create empty maps for each range
        myGraph.referenceRangeList()
                .filter { refRangesToKeep == null || refRangesToKeep.contains(it.id()) }
                .forEach { range ->
                    rangeToTaxonToHapids[range] = HashMap()
                }

        myNumRanges = rangeToTaxonToHapids.size

        myLogger.info("PathsToVCFPlugin: processData: number of ranges: $myNumRanges")

        // This converts the path information into the map structure declared above.
        // From ImportDiploidPathPlugin
        val temp = input.getDataOfType(Map::class.java)
        // Gets list of taxa represented in graph
        val taxa = when (temp.size) {
            0 -> { //get the haplotypes from the phg graph

                rangeToTaxonToHapids.keys
                        .map { myGraph.nodes(it) }
                        .flatten()
                        .forEach { node ->
                            // For each taxon at this node, get a list of its hapids.
                            // Add this particular nodeId to that taxon's hapid list
                            // A taxon has more than 1 hapid at a particular node if that taxon
                            // was both in a non-consensus and in a consensus node.
                            node.taxaList().forEach { taxon ->
                                val taxonToHapids = rangeToTaxonToHapids[node.referenceRange()]!!
                                var hapids = taxonToHapids[taxon.name]
                                if (hapids == null) {
                                    hapids = mutableListOf()
                                    taxonToHapids[taxon.name] = hapids
                                }
                                hapids.add(node.id())
                            }
                        }

                myGraph.taxaInGraph().map { it.name }.toList()

            }
            1 -> { // Use paths provided in input

                val taxaToPaths = temp[0].data as Map<String, List<List<HaplotypeNode>>>

                taxaToPaths.forEach { (taxon, paths) ->
                    paths.forEach { nodes ->
                        nodes.forEach { node ->
                            if (refRangesToKeep == null || refRangesToKeep.contains(node.referenceRange().id())) {
                                val taxonToHapids = rangeToTaxonToHapids[node.referenceRange()]!!
                                var hapids = taxonToHapids[taxon]
                                if (hapids == null) {
                                    hapids = mutableListOf()
                                    taxonToHapids[taxon] = hapids!!
                                }
                                hapids!!.add(node.id())
                            }
                        }
                    }
                }

                taxaToPaths.keys.toList()

            }
            else -> {
                throw IllegalArgumentException("PathsToVCFPlugin: processData: At most one paths map can be supplied as input.")
            }

        }

        myLogger.info("PathsToVCFPlugin: processData: number of taxa: ${taxa.size}")

        // multi-threading creation of VariantContexts
        CoroutineScope(Dispatchers.Default).launch {
            infosByRange(rangeToTaxonToHapids)
        }

        // write VariantContexts to file and block main thread until done
        runBlocking(Dispatchers.IO) { writeToVCF(taxa) }

        return null

    }

    /**
     * This starts the multi-threaded processing of paths to VariantContexts,
     * one thread per ReferenceRange.  It puts Deferred Jobs on
     * the variantContextChannel in order of ReferenceRanges.
     *
     * @param rangeToTaxonToHapids SortedMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>
     */
    private suspend fun infosByRange(rangeToTaxonToHapids: SortedMap<ReferenceRange, MutableMap<String, MutableList<Int>>>) =
        withContext(Dispatchers.Default) {

            val hasPositionsFile = positions() != null

            rangeToTaxonToHapids.forEach { (range, taxonToHapids) ->

                val positionsForRange = if (hasPositionsFile) mapPositionsInRanges[range.id()]?:emptyList() else null

                if (positionsForRange == null || positionsForRange.isNotEmpty()) {

                    variantContextChannel.send(async {
                        variantContexts(
                            positionsToInfos(
                                range,
                                taxonToHapids,
                                positionsForRange
                            )
                        )
                    })

                }

            }

            variantContextChannel.close()

        }

    /**
     * Create map of ReferenceRange ids to list of provided positions
     * in that range.
     */
    val mapPositionsInRanges by lazy {

        val positionToReferenceRange = mutableMapOf<String, RangeMap<Int, Int>>()
        myGraph.referenceRangeList().forEach { range ->
            positionToReferenceRange.getOrPut(range.chromosome().name) { TreeRangeMap.create() }
                .put(Range.closed(range.start(), range.end()), range.id())
        }

        val result = myGraph.referenceRangeList().associate { range -> range.id() to mutableListOf<Int>() }

        positions()?.forEach { position ->
            positionToReferenceRange[position.chromosome.name]?.let { rangeMap ->
                rangeMap.get(position.position)?.let { rangeId ->
                    result.get(rangeId)!!.add(position.position)
                }
            }
        }

        result

    }

    /**
     * This pulls deferred jobs from the variantContextChannel.
     * It waits sequentially for each job to finish and
     * writes each VariantContext to the VariantContextWriter.
     */
    private suspend fun writeToVCF(taxa: List<String>) = withContext(Dispatchers.IO) {

        val vcfOutputWriterBuilder = VariantContextWriterBuilder().unsetOption(Options.INDEX_ON_THE_FLY)

        vcfOutputWriterBuilder.setOutputFile(outputFile())
            .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
            .setOption(Options.ALLOW_MISSING_FIELDS_IN_HEADER)
            .build()
            .use { writer ->

                writer.writeHeader(HapCallingUtils.createGenericHeader(taxa))

                var numRangesWritten = 0
                for (contexts in variantContextChannel) {
                    contexts.await().let { variantContexts ->
                        variantContexts.forEach {
                            writer.add(it)
                        }
                    }
                    numRangesWritten++
                    val progress = (numRangesWritten.toDouble() / myNumRanges.toDouble() * 100.0).toInt()
                    progress(progress, null)
                }

            }

    }

    /**
     * This creates a VariantContext for a single chromosome / position.
     * It uses the VariantInfos for each taxon to create Genotypes.
     * The VariantInfos can be null indicate missing, and to keep
     * alleles in order (i.e. phased)
     *
     * @param chromosome chromosome
     * @param position position
     * @param taxaToInfos Map<taxon: String, List<VariantInfo?>>
     *
     * @return VariantContext
     */
    internal fun createVariantContext(chromosome: String, position: Int, taxaToInfos: Map<String, List<VariantInfo?>>): VariantContext {
        var refAllele = if (referenceSequence == null) Allele.NO_CALL
            else Allele.create(referenceSequence!!.genotypeAsString(Chromosome.instance(chromosome), position), true)
        val alleles = {
            val temp = taxaToInfos.values
                    .asSequence()
                    .flatten()
                    .filterNotNull()
                    .map { info ->
                        //when info.isVariant, the ref allele should not be "REF", but that is included to be safe
                        if (info.isVariant && info.refAlleleString() != "REF") {
                            //If info.length is one, then the ref allele string is the reference allele at this position.
                            //If length is more than one do not use it to determine the ref allele.
                            //The code that determined which positions will be output ensured that every position has at least
                            //one sample with a SNP. The VariantInfo for a SNP has length 1 and a valid ref allele. That means
                            //using indels to get the ref allele is unnecessary.
                            val tempRefAllele = if (info.length() == 1) Allele.create(info.refAlleleString(), true)
                            else Allele.NO_CALL
                            if (refAllele == Allele.NO_CALL) {
                                refAllele = tempRefAllele
                            } else if (tempRefAllele != Allele.NO_CALL && refAllele != tempRefAllele) {
                                myLogger.debug("Error when refAllele = $refAllele and tempRefAllele = $tempRefAllele")
                                myLogger.debug("info genotypeString = ${info.genotypeString()}, chr = ${info.chromosome()}, start = ${info.start()}, end = ${info.end()}")
                                throw IllegalStateException("PathsToVCFPlugin: createVariantContext: chromosome: $chromosome position: $position reference allele doesn't match reference from VariantInfo.")
                            }

                            val alt = if (info.isIndel && !info.isSymbolic) allele("<INS>") else allele(info.altAlleleString())
                            listOf(refAllele, alt).filter { it != Allele.NO_CALL }
                        } else emptyList()
                    }
                    .flatten()
                    .toSet()
            if (refAllele == Allele.NO_CALL) refAllele = Allele.create("N", true)
            if (temp.isEmpty()) setOf(refAllele) else temp
        }.invoke()

        require(refAllele != null && refAllele != Allele.NO_CALL) {"No reference allele assigned at chr = $chromosome, pos = $position"}
        val genotypes = taxaToInfos
                .filter {
                    it.key != REMAINING_INFOS_KEY
                }
                .map { (taxon, infos) ->
                    if (infos.isEmpty()) {
                        GenotypeBuilder(taxon, listOf(Allele.NO_CALL)).phased(false).make()
                    } else {
                        val alleles = infos.map { info ->
                            when (val genotype = if (info != null && info.isIndel && !info.isSymbolic) "<INS>" else info?.genotypeString()) {
                                "REF" -> refAllele
                                else -> {
                                    if ((refAllele?.baseString == genotype && genotype != null) || (info != null && !info.isVariant)) refAllele
                                    else {
                                        allele(genotype)
                                    }
                                }
                            }

                        }

                        if (alleles.size == 1 && makeDiploid()) {
                            GenotypeBuilder(taxon, listOf(alleles[0], alleles[0])).phased(false).make()
                        } else {
                            GenotypeBuilder(taxon, alleles).phased(false).make()
                        }
                    }
                }
                .toList()

        return VariantContextBuilder()
                .source(".")
                .alleles(alleles)
                .chr(chromosome)
                .start(position.toLong())
                .stop(position.toLong())
                .genotypes(genotypes)
                .make()

    }

    private fun allele(allele: String?): Allele {
        //debug messages
//        println("symbolic to N is ${symbolicToN()}")
//        println("symbolicOutput() is ${symbolicOutput()}")

        return when {
            allele == null -> Allele.NO_CALL
            allele == "" -> Allele.NO_CALL
            allele == GenotypeTable.UNKNOWN_ALLELE_STR -> Allele.NO_CALL
            allele.startsWith("<") -> when  {
                symbolicOutput() -> Allele.create(allele)
                symbolicToN() -> Allele.ALT_N
                else -> Allele.NO_CALL
            }
            else -> Allele.create(allele)
        }
    }

    // This is a pseudo taxon name used to pass unused VariantInfos
    // through to VariantContext creation. They will be used for ref / alt
    // alleles but not genotypes.
    val REMAINING_INFOS_KEY = "REMAINING_INFOS_KEY"

    /**
     * This returns the positions to be exported for the given reference range.
     * Each position has a corresponding map taxon -> list of variant infos.
     *
     * @param range reference range
     * @param taxaToNodes Map<taxon: String, List<hapid: Int>>
     *
     * @return SortedMap<position: Int, Map<taxon: String, List<VariantInfo>>>
     */
    private fun positionsToInfos(
        range: ReferenceRange,
        taxaToNodes: Map<String, List<Int>>,
        positionsForRange: List<Int>? = null
    ): SortedMap<Int, Map<String, List<VariantInfo?>>> {

        if (positionsForRange != null && positionsForRange.isEmpty()) {
            return sortedMapOf()
        }

        // List of all hapids in this reference range.
        // Will be used to keep track of unused VariantInfos.
        val allHapids = mutableSetOf<Int>()

        // These intermediate range maps are used to find the VariantInfos
        // for HaplotypeNodes at given positions
        // HashMap<hapid: Int, RangeMap<position: Int, VariantInfo>>()
        val hapidToPositionToInfo = HashMap<Int, RangeMap<Int, VariantInfo>>()

        val nodesForRange = myGraph.nodes(range)

        val variantInfos = nodesForRange
            .map { Pair(it.id(), it.variantInfos()) }
            .filter { it.second.isPresent }
            .map { Pair(it.first, it.second.get()) }
            .toMap()

        nodesForRange
            .map { it.id() }
            .forEach { hapid ->
                allHapids.add(hapid)
                hapidToPositionToInfo[hapid] = TreeRangeMap.create()
            }

        variantInfos
            .map { (hapid, variantInfos) -> variantInfos.map { info -> Pair(hapid, info) } }
            .flatten()
//            .filter { !it.second.isIndel } remove this and deal with indels later
            .forEach { (hapid, info) ->
                hapidToPositionToInfo[hapid]!!.put(Range.closed(info.start(), info.end()), info)
            }

        val allPositions = nodesForRange
            .asSequence()
            .map { variantInfos[it.id()] }
            .filterNotNull()
            .flatten()
            .filter { !it.isIndel }
            .filter { it.isVariant }
            .map { it.start() }
            .toSortedSet()

        // positions to include for this reference range
        // If no positionList is specified, we use the variantInfo objects to
        // get the infoList from the graph.
        val positions = if (positions() == null) {
            allPositions
        } else {
            positionsForRange!!
                .filter { it >= range.start() && it <= range.end() }
                .filter { allPositions.contains(it) }
                .toSortedSet()
        }

        val result = TreeMap<Int, Map<String, List<VariantInfo?>>>()

        positions.forEach { position ->

            // tracking unused VariantInfos
            val remainingHapids = allHapids.toMutableSet()

            val taxaToInfo = HashMap<String, MutableList<VariantInfo?>>()
            result[position] = taxaToInfo

            taxaToNodes.entries
                .forEach { (taxon, hapids) ->

                    val listInfos = taxaToInfo.getOrPut(taxon) { mutableListOf() }

                    hapids.forEach { hapid ->
                        remainingHapids.remove(hapid)
                        listInfos.add(hapidToPositionToInfo[hapid]!!.get(position)) // used upon method return
                    }

                }

            if (remainingHapids.isNotEmpty()) {
                taxaToInfo[REMAINING_INFOS_KEY] =
                    remainingHapids.map { hapidToPositionToInfo[it]!!.get(position) }.toMutableList()
            }

        }

        return result

    }

    /**
     * Creates list of VariantContexts for a reference range.
     *
     * @param infos SortedMap<position: Int, Map<taxon: String, List<VariantInfo>>>
     *
     * @return VariantContexts
     */
    public fun variantContexts(infos: SortedMap<Int, Map<String, List<VariantInfo?>>>): List<VariantContext> {

        // The line below is for junit testing - remove it unless you are running a test, e.g. testing variantContextToVariantInfo()
        // referenceSequence = GenomeSequenceBuilder.instance(refGenome())
        // end junit test code
        val chromosome = try {
            infos.values
                .map { it.values }
                .flatten()
                .flatten()
                .filterNotNull()
                .first()
                .chromosome()
        } catch (e: NoSuchElementException) {
            return emptyList()
        }

        return infos
            .map { (position, taxonToInfos) ->
                createVariantContext(chromosome, position, taxonToInfos)
            }
            .toList()

    }

    /**
     * Gets optional list of reference range ids to include in export.
     */
    private fun parseRefRangeFile(refRangeFile: String?): Optional<Set<Int>> {
        if (refRangeFile == null) {
            return Optional.empty()
        }
        val refRangeIdSet: MutableSet<Int> = HashSet()
        try {

            Utils.getBufferedReader(refRangeFile).use { reader ->
                var currentLine = reader.readLine()
                while (currentLine != null) {
                    val tabIndex = currentLine.indexOf("\t")
                    refRangeIdSet.add(currentLine.substring(0, tabIndex).toInt())
                    currentLine = reader.readLine()

                }
            }
        } catch (e: Exception) {
            myLogger.error("Error loading in the refRangeFile: $refRangeFile")
            throw IllegalStateException("Error loading refRangeFile: $refRangeFile", e)
        }
        return Optional.of(refRangeIdSet)
    }

    /**
     * Output file name
     *
     * @return Output VCF File Name
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output VCF File Name. Output file name
     *
     * @param value Output VCF File Name
     *
     * @return this plugin
     */
    fun outputFile(value: String): PathsToVCFPlugin {
        myOutputFile = PluginParameter<String>(myOutputFile, value)
        return this
    }

    /**
     * Reference Range file used to further subset the paths
     * for only specified regions of the genome.
     *
     * @return Reference Range File
     */
    fun refRangeForSNPFile(): String? {
        return myRefRangeForSNPFile.value()
    }

    /**
     * Set Reference Range File. Reference Range file used
     * to further subset the paths for only specified regions
     * of the genome.
     *
     * @param value Reference Range File
     *
     * @return this plugin
     */
    fun refRangeForSNPFile(value: String): PathsToVCFPlugin {
        myRefRangeForSNPFile = PluginParameter<String>(myRefRangeForSNPFile, value)
        return this
    }

    /**
     * Reference Genome.
     *
     * @return Reference Genome
     */
    fun refGenome(): String? {
        return myRefGenome.value()
    }

    /**
     * Set Reference Genome. Reference Genome.
     *
     * @param value Reference Genome
     *
     * @return this plugin
     */
    fun refGenome(value: String): PathsToVCFPlugin {
        myRefGenome = PluginParameter<String>(myRefGenome, value)
        return this
    }

    /**
     * Whether to report haploid paths as homozygousdiploid
     *
     * @return Make Homozygous
     */
    fun makeDiploid(): Boolean {
        return myMakeDiploid.value()
    }

    /**
     * Set Make Diploid. Whether to report haploid paths as homozygousdiploid
     *
     * @param value Make Homozygous
     *
     * @return this plugin
     */
    fun makeDiploid(value: Boolean): PathsToVCFPlugin {
        myMakeDiploid = PluginParameter<Boolean>(myMakeDiploid, value)
        return this
    }

    /**
     * Positions to include in VCF. Can be specified by Genotype
     * file (i.e. VCF, Hapmap, etc.), bed file, or json file
     * containing the requested positions.
     *
     * @return Position List
     */
    fun positions(): PositionList? {
        return myPositions.value()
    }

    /**
     * Set Position List. Positions to include in VCF. Can
     * be specified by Genotype file (i.e. VCF, Hapmap, etc.),
     * bed file, or json file containing the requested positions.
     *
     * @param value Position List
     *
     * @return this plugin
     */
    fun positions(value: PositionList): PathsToVCFPlugin {
        myPositions = PluginParameter<PositionList>(myPositions, value)
        return this
    }

    /**
     * Convert symbolic alleles to N in the output. Symbolic
     * alleles like <INS> and <NON-REF> are uncommon in the
     * vcf output but can occur. If true, these will be output
     * as 'N'. Otherwise, they will be output as '.'
     *
     * @return Symbolic To N
     */
    fun symbolicToN(): Boolean {
        return symbolicToN.value()
    }

    /**
     * Set Symbolic To N. Convert symbolic alleles to N in
     * the output. Symbolic alleles like <INS> and <NON-REF>
     * are uncommon in the vcf output but can occur. If true,
     * these will be output as 'N'. Otherwise, they will be
     * output as '.'
     *
     * @param value Symbolic To N
     *
     * @return this plugin
     */
    fun symbolicToN(value: Boolean): PathsToVCFPlugin {
        symbolicToN = PluginParameter<Boolean>(symbolicToN, value)
        return this
    }

    /**
     * Write symbolic alleles to vcf. Symbolic alleles like
     * <INS> and <NON-REF> are uncommon in the vcf output
     * but can occur. By default they will be converted to
     * '.' because many vcf apps do not recognize them as
     * valid. If true, these will be output as <INS> or <NON-REF>.
     * Otherwise, they will be output as '.'
     *
     * @return Symbolic
     */
    fun symbolicOutput(): Boolean {
        return symbolicOutput.value()
    }

    /**
     * Set Symbolic. Write symbolic alleles to vcf. Symbolic
     * alleles like <INS> and <NON-REF> are uncommon in the
     * vcf output but can occur. By default they will be converted
     * to '.' because many vcf apps do not recognize them
     * as valid. If true, these will be output as <INS> or
     * <NON-REF>. Otherwise, they will be output as '.'
     *
     * @param value Symbolic
     *
     * @return this plugin
     */
    fun symbolicOutput(value: Boolean): PathsToVCFPlugin {
        symbolicOutput = PluginParameter<Boolean>(symbolicOutput, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Diploid Paths to VCF"
    }

    override fun getToolTipText(): String {
        return "Diploid Paths to VCF"
    }

}
