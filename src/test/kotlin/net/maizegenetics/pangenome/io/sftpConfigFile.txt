# To use this config file with the SFTPConnectionTest.kt tests, you must
# edit the values below.  Add your own values for host, username, pasword, localFilePath and remotePath
# In addition, add a single file name for the "singleFile" parameter
# Add multiple comma-separated files for the "multileFiles" parameter
# NOTE - this file is deprecated as we removed the SFTPConnectionTest.kt file, due to no longer
# pulling the gvcf files from the server.  The users need to do that themselves.
host=
username=
password=
localFilePath=
remotePath=
singleFile=
multipleFiles=
uploadFile=