package net.maizegenetics.pangenome.db_updateDelete

import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.ParameterCache
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.lang.IllegalArgumentException
import kotlin.test.*

@ExtendWith(SmallSeqExtension::class)
class DeleteReadMappingPluginIT {

    val taxaList = listOf(
        "LineA_wgs", "LineA_gbs", "LineA1_wgs", "LineA1_gbs", "LineB_wgs", "LineB_gbs",
        "LineB1_wgs", "LineB1_gbs", "RecLineA1Refgco6_wgs", "RecLineA1Refgco6_gbs",
        "RecLineB1LineA1gco1_wgs", "RecLineB1LineA1gco1_gbs", "RecLineB1LineB1gco2_wgs",
        "RecLineB1LineB1gco2_gbs", "RecLineBRefA1gco3_wgs", "RecLineBRefA1gco3_gbs",
        "RecRefA1LineA1gco4_wgs", "RecRefA1LineA1gco4_gbs", "RecRefA1Refgco7_wgs",
        "RecRefA1Refgco7_gbs", "RecRefRefA1gco5_wgs", "RecRefRefA1gco5_gbs", "RecRefRefA1gco8_wgs",
        "RecRefRefA1gco8_gbs", "Ref_wgs", "Ref_gbs", "RefA1_wgs", "RefA1_gbs"
    )

    @Test
    fun deleteReadMappingWithMethodAndKeyfile() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val readMappingIds_before = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(taxaList.size, readMappingIds_before.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeleteReadMappingPlugin(null,false)
            .methodName(SmallSeqExtension.imputeMethod)
            .keyFile(SmallSeqExtension.genotypingKeyFilePath)
            .deleteMethod(false)
            .performFunction(null);

        val readMappingIds_after = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(0, readMappingIds_after.size)
        assertTrue(phg.getMethodIdFromName(SmallSeqExtension.imputeMethod) > 0)

        phg.close()
    }

    @Test
    fun deleteMethodAndReadMappingWithMethodAndKeyfile() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val readMappingIds_before = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(taxaList.size, readMappingIds_before.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeleteReadMappingPlugin(null,false)
            .methodName(SmallSeqExtension.imputeMethod)
            .keyFile(SmallSeqExtension.genotypingKeyFilePath)
            .deleteMethod(true)
            .performFunction(null);

        // Method should no longer exist
        assertFailsWith<IllegalArgumentException> {
            phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        }
        assertTrue(phg.getMethodIdFromName(SmallSeqExtension.imputeMethod) < 1)

        phg.close()
    }

    @Test
    fun deleteReadMappingNoKeyfile() {
        val connection  = DBLoadingUtils.connection(SmallSeqExtension.configFilePath, false);
        val phg = PHGdbAccess(connection)

        val readMappingIds_before = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(taxaList.size, readMappingIds_before.size)

        ParameterCache.load(SmallSeqExtension.configFilePath)
        DeleteReadMappingPlugin(null,false)
            .methodName(SmallSeqExtension.imputeMethod)
            .performFunction(null)

        val readMappingIds_after = phg.getReadMappingIdsForTaxaMethod(taxaList, SmallSeqExtension.imputeMethod)
        assertEquals(0, readMappingIds_after.size)
        assertTrue(phg.getMethodIdFromName(SmallSeqExtension.imputeMethod) > 0)

        phg.close()
    }
}