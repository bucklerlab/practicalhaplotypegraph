package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.util.LoggingUtils
import org.apache.commons.io.FileUtils
import org.jetbrains.kotlinx.dataframe.DataFrame
import org.jetbrains.kotlinx.dataframe.api.filter
import org.jetbrains.kotlinx.dataframe.api.maxBy
import org.jetbrains.kotlinx.dataframe.api.print
import org.jetbrains.kotlinx.dataframe.api.sortBy
import org.junit.Before
import org.junit.Test

import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class AssemblyConsensusMetricPluginTest {
    val userHome = System.getProperty("user.home")
    val outputDir = "$userHome/temp/AssemblyConsensusMetricPluginTests/"

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(File(outputDir))
            Files.createDirectories(Paths.get(outputDir))
        } catch (ioe: IOException) {
            throw IllegalStateException("AssemblyConsensusMetricPluginTest:setup: error deleting/creating folders: " + ioe.message)
        }
    }

    @Test
    fun testAssemblyConsensusMetricPlugin() {
        LoggingUtils.setupDebugLogging()

        // Test with a dummy graph
        // The testing with a real graph (removed as it was a local file) will show multiple hits
        // for each reference range with different taxa at each.  The dummy graph also gives us
        // this, but there is only a single taxa at each.  The resulting db is the same - it takes
        // whatever graph it was given.  What makes this different from the AssemblyHapMetricsPlugin
        // is the data that is presented.  In AssemblyHapMetricsPlugin there is a column for each individual
        // taxa so they can be compared across reference ranges.  For Consensus, you have a row for each
        // hapid at each reference range to allow comparing different methods.
        var graphOutFile = "${outputDir}assemblyConsensusMetricsGraph_dummyGraph_test.txt"

        var myGraph : HaplotypeGraph = buildSimpleDummyGraph()

        val datasetGraph = AssemblyConsensusMetricPlugin(null, false)
            //.methods(graphMethods)
            .outputFile(graphOutFile)
            .performFunction(DataSet.getDataSet(myGraph))

        val df = datasetGraph.getDataWithName("DataFrame")[0].data as DataFrame<*>
        println("\nresults from dummy graph input")
        df.print()

        val numCols = df.columnsCount()
        val colNames = df.columnNames()
        println("colNames: ${colNames}")
        assertEquals(9,numCols)
        assertTrue(df.columnNames().contains("refRangeID"))
        assertTrue(df.columnNames().contains("groupType"))
        assertTrue(df.columnNames().contains("taxa"))
        assertTrue(df.columnNames().contains("len"))
        assertEquals(9,df.rowsCount())

        println(df.getColumnOrNull(7)?.values())

        println("This is df.maxBy column=taxa")
        println(df.maxBy("taxa"))
        println(df.sortBy("len"))
    }
}