package net.maizegenetics.pangenome.minimap2;

public class Minimap2Paths {
    public static final String fastaSuffix = ".fa";
    public static final String refGenomeName = "B73";
 
    public static final String userHome = System.getProperty("user.home");
    public static final String minimap2Path = userHome+"/Box Sync/MaizePHG/PHG_external_apps/bin/minimap2";
    public static final String samToolsPath = userHome+"/Box Sync/MaizePHG/PHG_external_apps/bin/samtools";
    public static final String sam2vcfPath = userHome+"/Box Sync/MaizePHG/PHG_external_apps/bin/sam2vcf";
    public static final String bcftoolsPath = userHome+"/Box Sync/MaizePHG/PHG_external_apps/bcftools-1.5/bcftools";

    public static final String boxW22AlignmentDir = userHome + "/Box Sync/MaizePHG/W22_Alignment/";

    
    public static final String refChromFile = "B73_chr10.fa";
    public static final String w22ChromFile =  "W22_chr10.fa";
    public static final String w22Chrom10ContigFile = "W22_chr10_asContigs.fa.gz";
    public static final String regionFile = "region1_2M.bed";
    public static final String intervalsFile = "hackathon_trimmed_intervals.bed";
    
    public static final String outputDir = userHome + "/temp/minimap2Alignment/";
    public static final String refGenomeDir=outputDir+"ref/";
    public static final String refGenomePath=refGenomeDir+refGenomeName+fastaSuffix;
    public static final String dataDir = outputDir+"data/";
    public static final String w22AlignmentDir = dataDir;

    public static final String compressedB73Chrom = w22AlignmentDir + refChromFile + ".gz";
    public static final String uncompressedB73Chrom = w22AlignmentDir + refChromFile;
    public static final String compressedW22Chrom = w22AlignmentDir + w22ChromFile + ".gz";
    public static final String uncompressedW22Chrom = w22AlignmentDir + w22ChromFile;
    public static final String compressedW22Contigs = w22AlignmentDir + w22Chrom10ContigFile + ".gz";
    public static final String uncompressedW22Contigs = w22AlignmentDir + w22Chrom10ContigFile;
    public static final String regions = w22AlignmentDir + regionFile;
    public static final String intervals = w22AlignmentDir + intervalsFile;
    public static final String w22B73OutputSam = outputDir + "w22B73Asm10.sam";
    public static final String sortedBam = outputDir + "w22B73Asm10.sorted.bam";
    public static final String regionalizedBam = outputDir + "w22B73Asm10.1_2M.sorted.bam";
    public static final String mpileupBCF = outputDir + "w22B73Asm10.1_2M.sorted.bcf";
    public static final String gvcfFile = outputDir + "w22B73Asm10.1_2M.sorted.g.vcf";

    public static final String interanchors = dataDir+"refInterAnchorsFromDB.csv";

}
