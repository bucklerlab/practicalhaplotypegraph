package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.AfterAll
import java.io.File
import kotlin.test.fail

class MakeDefaultDirectoryPluginTest {

    val rootDir = System.getProperty("user.dir") + "MakeDefaultDirectoryPluginTest/"
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
        File(rootDir).mkdirs()
    }

    @AfterAll
    fun tearDown() {
        File(rootDir).deleteRecursively()
    }

    @Test
    fun testSetupDir() {
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)

        //Loop through and make sure each directory was created
        Assert.assertTrue("Working directory was not created", File(rootDir).exists())
        Assert.assertTrue("Config file was not created", File(rootDir + "config.txt").exists())
        Assert.assertTrue("ASM key file was not created", File(rootDir + "load_asm_genome_key_file.txt").exists())
        Assert.assertTrue("WGS key file was not created", File(rootDir + "load_wgs_genome_key_file.txt").exists())
        Assert.assertTrue("Read mapping key file was not created", File(rootDir + "readMapping_key_file.txt").exists())
        Assert.assertTrue("Input directory was not created", File(rootDir + "inputDir/").exists())
        Assert.assertTrue("Assembly Input directory was not created", File(rootDir + "inputDir/assemblies/").exists())
        Assert.assertTrue("LoadDB Input directory was not created", File(rootDir + "inputDir/loadDB/").exists())
        Assert.assertTrue("LoadDB Fastq Input directory was not created", File(rootDir + "inputDir/loadDB/fastq/").exists())
        Assert.assertTrue("LoadDB Temp Input directory was not created", File(rootDir + "inputDir/loadDB/temp/").exists())
        Assert.assertTrue("LoadDB Bam Input directory was not created", File(rootDir + "inputDir/loadDB/bam/").exists())

        Assert.assertTrue("LoadDB Bam Temp Input directory was not created", File(rootDir + "inputDir/loadDB/bam/temp/").exists())
        Assert.assertTrue("LoadDB Bam Dedup Input directory was not created", File(rootDir + "inputDir/loadDB/bam/dedup/").exists())
        Assert.assertTrue("LoadDB Bam MapqFiltered Input directory was not created", File(rootDir + "inputDir/loadDB/bam/mapqFiltered/").exists())
        Assert.assertTrue("LoadDB Gvcf Input directory was not created", File(rootDir + "inputDir/loadDB/gvcf/").exists())


        Assert.assertTrue("Reference Input directory was not created", File(rootDir + "inputDir/reference/").exists())
        Assert.assertTrue("Imputation Input directory was not created", File(rootDir + "inputDir/imputation/").exists())
        Assert.assertTrue("Imputation Fastq Input directory was not created", File(rootDir + "inputDir/imputation/fastq/").exists())
        Assert.assertTrue("Imputation Sam Input directory was not created", File(rootDir + "inputDir/imputation/sam/").exists())
        Assert.assertTrue("Imputation Vcf Input directory was not created", File(rootDir + "inputDir/imputation/vcf/").exists())

        Assert.assertTrue("Load Genome Data File was not created", File(rootDir + "inputDir/reference/load_genome_data.txt").exists())

        Assert.assertTrue("Output directory was not created", File(rootDir + "outputDir/").exists())
        Assert.assertTrue("Align directory was not created", File(rootDir + "outputDir/align/").exists())
        Assert.assertTrue("Align Gvcf directory was not created", File(rootDir + "outputDir/align/gvcfs/").exists())
        Assert.assertTrue("Pangenome directory was not created", File(rootDir + "outputDir/pangenome/").exists())
        Assert.assertTrue("Temp File directory was not created", File(rootDir + "tempFileDir/").exists())

        Assert.assertTrue("Temp File directory was not created", File(rootDir + "tempFileDir/").exists())
        Assert.assertTrue("README file was not created", File(rootDir + "README.txt").exists())

    }

    @Test
    fun exportASMKeyFile() {
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)
        val truthList = listOf<String>("AssemblyServerDir","RefDir","RefFasta","AssemblyDir","AssemblyGenomeFasta","AssemblyFasta","AssemblyDBName","Chromosome")

        val asmKeyFile = File(rootDir + "load_asm_genome_key_file.txt")
        val asmKeyFileContents = asmKeyFile.readLines()
        val header = asmKeyFileContents[0].split("\t")

        Assert.assertEquals("Header size does not match truth list size", truthList.size, header.size)

        for (i in truthList.indices) {
            Assert.assertEquals("Header does not match truth list", truthList[i], header[i])
        }
    }

    @Test
    fun exportWGSKeyFile() {
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)
        val truthList = listOf<String>(
            "sample_name",
            "sample_description",
            "files",
            "type",
            "chrPhased",
            "genePhased",
            "phasingConf",
            "libraryID"
        )

        val wgsKeyFile = File(rootDir + "load_wgs_genome_key_file.txt")
        val wgsKeyFileContents = wgsKeyFile.readLines()
        val header = wgsKeyFileContents[0].split("\t")

        Assert.assertEquals("Header size does not match truth list size", truthList.size, header.size)

        for (i in truthList.indices) {
            Assert.assertEquals("Header does not match truth list", truthList[i], header[i])
        }
    }

    @Test
    fun exportReadMappingKeyFile() {
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)
        val truthList = listOf<String>(
            "cultivar","flowcell_lane", "filename",	"PlateID"
        )

        val readMappingKeyFile = File(rootDir + "readMapping_key_file.txt")
        val readMappingKeyFileContents = readMappingKeyFile.readLines()
        val header = readMappingKeyFileContents[0].split("\t")

        Assert.assertEquals("Header size does not match truth list size", truthList.size, header.size)

        for (i in truthList.indices) {
            Assert.assertEquals("Header does not match truth list", truthList[i], header[i])
        }
    }

    @Test
    fun exportLoadDBKeyFile() {
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)
        val truthList = listOf<String>(
            "Genotype",	"Hapnumber", "Dataline", "ploidy", "genesPhased","chromsPhased","confidence","Method","MethodDetails"
        )

        val loadDBKeyFile = File(rootDir + "/inputDir/reference/load_genome_data.txt")
        val loadDBKeyFileContents = loadDBKeyFile.readLines()
        val header = loadDBKeyFileContents[0].split("\t")

        Assert.assertEquals("Header size does not match truth list size", truthList.size, header.size)

        for (i in truthList.indices) {
            Assert.assertEquals("Header does not match truth list", truthList[i], header[i])
        }
    }
}