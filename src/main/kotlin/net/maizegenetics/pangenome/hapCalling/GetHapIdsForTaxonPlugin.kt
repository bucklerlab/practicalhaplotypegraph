@file:JvmName("GetHapIdsForTaxonPlugin")
package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.GraphUtils.nodesContainingExactly
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import javax.swing.ImageIcon


/**
 * Class takes a configfile with database connect information, an output directory,
 * a set of methods, and an optional taxa list.  It will create one file per taxon with a single column containing
 * the haplotype_ids for that taxon/gamete from the haplotypes table.
 *
 *  The output is written to files in the specified output directory with a file name in the
 * format:  <linename>_path.txt
 *
 * These files can be used an input to ImportHaplotypePathFilePlugin -> PathsToVCFPlugin
 * to create VCF files for taxon which have data populated in the PHG haplotypes table.
 *
 * author:  lcj34
 * date:  Nov 20, 2018 
 */

class GetHapIdsForTaxonPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(GetHapIdsForTaxonPlugin::class.java)

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .guiName("DB Config File")
            .required(true).inFile()
            .description(
                    "File containing lines with data for host=, user=, password= and DB=, DBtype= used for db connection")
            .build()

    private var myTaxaList = PluginParameter.Builder("taxaList", null, TaxaList::class.java)
            .required(false)
            .description("Set Taxa List. Optional list of taxa to include. This can be a comma separated list of taxa (no spaces unless surrounded by quotes), file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). By default, all taxa will be included. ")
            .build()
    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java).guiName("Output Directory ")
            .required(true)
            .description("Path to output directory where files will be written")
            .build()

    private var myMethods = PluginParameter.Builder("methods", null, String::class.java)
            .required(true)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" + "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
            .build()

    override fun getIcon(): ImageIcon? {
        val imageURL = GetHapIdsForTaxonPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "GetHapidsForTaxonKotlinPlugin"
    }

    override fun getToolTipText(): String {
        return "Create path files for db taxon "
    }


    override fun processData(input: DataSet): DataSet? {
        val time = System.nanoTime()

        var graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile())
                .methods(methods())
                .build()


        // subset the graph based on user supplied taxa list
        if (taxaList() != null) {
            myLogger.debug("Filter graph on taxaList.");
            graph = CreateGraphUtils.subsetGraph(graph, taxaList());
        }

        // For each taxon in the graph,
        //  1.  find all nodes for that taxon
        //  2.  write a file per taxon containing a single line for each node's hapid
        //  3.  These should be in reference range order.
        //  4.  The number of files should match the number of taxa in the graph.


        for ( taxa in graph.taxaInGraph()) {
             var taxaBuilder =  TaxaListBuilder();
             taxaBuilder.add(taxa);
             var singleTaxaList = taxaBuilder.build();

             var taxaNodeList = nodesContainingExactly(singleTaxaList, graph);

            // Write output file
            var outputFile = outputDir() + "/" + taxa.name + "_path.txt"
            myLogger.info("Writing  genotype file: $outputFile")
            try {

                val bw = Utils.getBufferedWriter(outputFile)
                for (hapNode in taxaNodeList) {
                    bw.write(Integer.toString(hapNode.id()))
                    bw.write("\n")
                }
                bw.close()
            } catch (exc: Exception) {
                exc.printStackTrace()
                throw IllegalStateException("GetHapIdsForTaxonPlugin: Error finding hapids for taxon")
            }
        }


        myLogger.info("GetHapIdsForTaxonPlugin: finished in " + (System.nanoTime() - time)/1e9 + " seconds")
        return null
    }

    /**
     * File containing lines with data for host=, user=, password=
     * and DB=, DBtype= used for db connection
     *
     * @return DB Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set DB Config File. File containing lines with data
     * for host=, user=, password= and DB=, DBtype= used for
     * db connection
     *
     * @param value DB Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): GetHapIdsForTaxonPlugin {
        configFile = PluginParameter(configFile, value)
        return this
    }

    /**
     * Taxa List
     *
     * @return Position List
     */
    fun taxaList(): TaxaList {
        return myTaxaList.value()
    }

    /**
     * Set Taxa List.
     *
     * @param value Taxa List
     *
     * @return this plugin
     */
    fun taxaList(value: TaxaList): GetHapIdsForTaxonPlugin {
        myTaxaList = PluginParameter<TaxaList>(myTaxaList, value)
        return this
    }

    fun taxaList(value: String): GetHapIdsForTaxonPlugin {
        myTaxaList = PluginParameter<TaxaList>(myTaxaList, AbstractPlugin.convert(value, TaxaList::class.java))
        return this
    }

    /**
     * Path to output directory where files will be written
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory . Path to output directory where
     * files will be written
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): GetHapIdsForTaxonPlugin {
        outputDir = PluginParameter(outputDir, value)
        return this
    }

    /**
     * Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs
     * separated by semicolon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @return Methods
     */
    fun methods(): String {
        return myMethods.value()
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name and range group method name). Method pair separated by a
     * comma, and pairs separated by semicolon. The range group is optional Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): GetHapIdsForTaxonPlugin {
        myMethods = PluginParameter(myMethods, value)
        return this
    }

}