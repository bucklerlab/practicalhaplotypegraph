package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.analysis.ListPluginParameters
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.TableReport
import net.maizegenetics.util.Utils
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

class MakeDefaultDirectoryPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private var myWorkingDir = PluginParameter.Builder("workingDir","./", String::class.java)
            .description("Root directory to write out directory structure.")
            .outDir()
            .required(false)
            .build()

    private var jarFiles = PluginParameter.Builder("jars", "phg.jar", String::class.java)
            .description("Jar files to pull plugin parameters from.")
            .required(false)
            .build()

    val pluginsToGetParams = setOf("GetDBConnectionPlugin", "LoadAllIntervalsToPHGdbPlugin",
            "CreateIntervalBedFilesPlugin", "FilterGVCFSingleFilePlugin", "LoadHaplotypesFromGVCFPlugin",
            "HaplotypeGraphBuilderPlugin", "AssemblyHaplotypesMultiThreadPlugin", "RunHapConsensusPipelinePlugin")

    override fun processData(input: DataSet?): DataSet? {

        //Plugin to build the directory structure

        File(workingDir()).mkdirs()
        exportConfigFile("${workingDir()}/config.txt")
        exportASMKeyFile("${workingDir()}/load_asm_genome_key_file.txt")
        exportWGSKeyFile("${workingDir()}/load_wgs_genome_key_file.txt")
        exportReadMappingKeyFile("${workingDir()}/readMapping_key_file.txt")
        File("${workingDir()}/inputDir/").mkdirs()

        //Make
//        /inputDir/gene_bed_file.bed

        File("${workingDir()}/inputDir/assemblies/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/fastq/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/temp/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/bam/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/bam/temp/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/bam/dedup/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/bam/mapqFiltered/").mkdirs()
        File("${workingDir()}/inputDir/loadDB/gvcf/").mkdirs()

        File("${workingDir()}/inputDir/reference/").mkdirs()

        File("${workingDir()}/inputDir/imputation/fastq/").mkdirs()
        File("${workingDir()}/inputDir/imputation/sam/").mkdirs()
        File("${workingDir()}/inputDir/imputation/vcf/").mkdirs()

        //Create /inputDir/reference/load_genome_data.txt

        exportLoadDataFile("${workingDir()}/inputDir/reference/load_genome_data.txt")
        File("${workingDir()}/outputDir/").mkdirs()
        File("${workingDir()}/outputDir/align/").mkdirs()
        File("${workingDir()}/outputDir/align/gvcfs/").mkdirs()
        File("${workingDir()}/outputDir/pangenome/").mkdirs()
        File("${workingDir()}/tempFileDir/").mkdirs()


        //Write out a README suggesting where to save files
        exportReadMe("${workingDir()}/README.txt")

        return null
    }

    /**
     * Function to export a readme file denoting which things need to be changed before pipeline is run.
     */
    fun exportReadMe(file : String) {
        Utils.getBufferedWriter(file).use { output ->
            output.write("README for PHG Directory Structure\n")
            output.write("In order for the PHG pipeline to run, you will need to copy some files into the directories created by MakeDefaultDirectoryPlugin.  \n" +
                    "Please Note that these directories are simply suggestions for a streamlined use and can be overridden by specific plugin parameters set in the config file.\n" +
                    "MakeDefaultDirectoryPlugin also will make a sample config.txt file filled with all the default parameters.  This sample config.txt file is mainly for MakePHGDBPipelinePlugin. A separate config is provided for ImputationPipelinePlugin.  At the top the unassigned parameters are put and they are marked with **UNASSIGNED**.  \n" +
                    "YOU MUST SET THESE PARAMETERS!  The PHG code will fail fast if any are not set.\n" +
                    "\n" +
                    "MakeDefaultDirectoryPlugin will also make a template for the needed key files which can be filled out with actual information.  We chose to only export the headers for these files to prevent accidental uploading of fake data.\n" +
                    "\n" +
                    "Note We will refer to the current working directory where all the subdirectories are created as workingDir\n" +
                    "\n" +
                    "TASKS TODO:\n" +
                    "Copy the reference fasta into workingDir/inputDir/reference/\n" +
                    "Modify workingDir/inputDir/reference/load_genome_data.txt to have the correct parameters.  \n" +
                    "Copy the haplotype BED file into workingDir/inputDir/\n" +
                    "Modify config.txt in workingDir filling in at least all the parameters set to **UNASSIGNED**.  \n" +
                    "\n" +
                    "If Running Assemblies:\n" +
                    "Copy Assembly files into workingDir/inputDir/assemblies/ \n" +
                    "Modify load_asm_genome_key_file.txt \n" +
                    "\n" +
                    "If Running WGS:\n" +
                    "Copy WGS fastqs, bams and GVCFs into workingDir/inputDir/loadDB/* .  Please put the correct file type in the correct folder. (If running WGS)\n" +
                    "Modify load_wgs_genome_key_file.txt\n")
        }
    }

    /**
     * Function to export a default config file.
     */
    fun exportConfigFile(file: String) {
        val ds = ListPluginParameters().jarFiles(jarFiles().split(" ").toList()).performFunction(null)
        val pluginsTableReport = ds.getDataOfType(TableReport::class.java)[0].data as TableReport


        println(pluginsTableReport.rowCount)

        val params = (0 until pluginsTableReport.rowCount).map { pluginsTableReport.getRow(it as Long) }
                .filter { pluginsToGetParams.contains(it[0]) }
//                .forEach { println(it.joinToString("\t")) }


        val requiredParams = params.filter { it[2] == "true" }
        val defaultedParams = params.filter { it[2] == "false" && it[3]!=null }
        val nonSetNonDefault = params.filter { it[2] == "false" && it[3]==null }



        Utils.getBufferedWriter(file).use { output ->
//            output.write("DB=phg.db\n")
            output.write("########################################\n")
            output.write("#Required Parameters:\n")
            output.write("########################################\n")
            requiredParams.forEach { output.write("${it[0]}.${it[1]}=**UNASSIGNED**\n") }
            output.write("referenceFasta=**UNASSIGNED**") //TODO REMOVE THIS
            output.write("\n\n########################################\n")
            output.write("#Defaulted parameters:\n")
            output.write("########################################\n")
            defaultedParams.forEach { output.write("${it[0]}.${it[1]}=${it[3]}\n") }
            output.write("numThreads=10\n" +
                    "Xmx=10G\n" +
                    "picardPath=/picard.jar\n" +
                    "gatkPath=/gatk/gatk\n" +
                    "tasselLocation=/tassel-5-standalone/run_pipeline.pl\n" +
                    "fastqFileDir=/tempFileDir/data/fastq/\n" +
                    "tempFileDir=/tempFileDir/data/bam/temp/\n" +
                    "dedupedBamDir=/tempFileDir/data/bam/DedupBAMs/\n" +
                    "filteredBamDir=/tempFileDir/data/bam/filteredBAMs/\n" +
                    "gvcfFileDir=/tempFileDir/data/gvcfs/\n" +
                    "extendedWindowSize=1000\n" +
                    "mapQ=48\n")

            output.write("\n#Sentieon Parameters.  Uncomment and set to use sentieon:\n")
            output.write("#sentieon_license=**UNASSIGNED**\n" +
                    "#sentieonPath=/sentieon/bin/sentieon\n")
            output.write("\n\n########################################\n")
            output.write("#Optional Parameters With No Default Values:\n")
            output.write("########################################\n")
            nonSetNonDefault.forEach { output.write("${it[0]}.${it[1]}=${it[3]}\n") }

            output.write("\n\n#FilterGVCF Parameters.  Adding any of these will add more filters.")
            output.write("#exclusionString=**UNASSIGNED**\n" +
                    "#DP_poisson_min=0.0\n" +
                    "#DP_poisson_max=1.0\n" +
                    "#DP_min=**UNASSIGNED**\n" +
                    "#DP_max=**UNASSIGNED**\n" +
                    "#GQ_min=**UNASSIGNED**\n" +
                    "#GQ_max=**UNASSIGNED**\n" +
                    "#QUAL_min=**UNASSIGNED**\n" +
                    "#QUAL_max=**UNASSIGNED**\n" +
                    "#filterHets=**UNASSIGNED**\n")
        }
    }

    /**
     * Function to export the ASM keyFile Template
     */
    fun exportASMKeyFile(file: String) {
        Utils.getBufferedWriter(file).use { output ->
            output.write("AssemblyServerDir\tRefDir\tRefFasta\tAssemblyDir\tAssemblyGenomeFasta\tAssemblyFasta\tAssemblyDBName\tChromosome\n")
        }
    }

    /**
     * Function to export the WGS keyFile Template
     */
    fun exportWGSKeyFile(file: String) {
        Utils.getBufferedWriter(file).use { output ->
            output.write("sample_name\tsample_description\tfiles\ttype\tchrPhased\tgenePhased\tphasingConf\tlibraryID\n")
        }
    }

    /**
     * Function to export the ReadMapping KeyFile
     */
    fun exportReadMappingKeyFile(file: String) {
        Utils.getBufferedWriter(file).use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
        }
    }

    /**
     * Function to create a load_data_file
     */
    fun exportLoadDataFile(file : String) {
        Utils.getBufferedWriter(file).use { output ->
            output.write("Genotype\tHapnumber\tDataline\tploidy\tgenesPhased\tchromsPhased\tconfidence\tMethod\tMethodDetails\n")
        }
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = MakeDefaultDirectoryPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MakeDefaultDirectoryPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to create a directory structure and sample config and keyfiles."
    }


    /**
     * Root directory to write out directory structure.
     *
     * @return Working Dir
     */
    fun workingDir(): String {
        return myWorkingDir.value()
    }

    /**
     * Set Working Dir. Root directory to write out directory
     * structure.
     *
     * @param value Working Dir
     *
     * @return this plugin
     */
    fun workingDir(value: String): MakeDefaultDirectoryPlugin {
        myWorkingDir = PluginParameter<String>(myWorkingDir, value)
        return this
    }
    /**
     * Jar files to pull plugin parameters from.
     *
     * @return Jars
     */
    fun jarFiles(): String {
        return jarFiles.value()
    }

    /**
     * Set Jars. Jar files to pull plugin parameters from.
     *
     * @param value Jars
     *
     * @return this plugin
     */
    fun jarFiles(value: String): MakeDefaultDirectoryPlugin {
        jarFiles = PluginParameter<String>(jarFiles, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(MakeDefaultDirectoryPlugin::class.java)
}