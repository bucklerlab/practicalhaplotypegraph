package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.junit.SmallSeqExtension.Companion.baseDir
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Connection
import java.sql.SQLException

/**
 * This test class is created for testing connection to SQLite db and running
 * queries.  Not all queries need to be tested on both postgres and sqlite,
 * but there will be times when we want to verify either the schema or queries
 * that differ between the 2 supported databases.
 *
 * This is a junit vs IT as it does not depend on all the postgres commands
 * that are run prior to running IT tests.
 */
class SQLiteVerificationTest {

    @Test
    fun testConnectToSQLite() {
        LoggingUtils.setupDebugLogging()

        val dbFile = "$baseDir/phgSmallSeq.db"
        val configProperitesFile = "$baseDir/configSQlite.txt"

        // Delete existing any existing db and/or config file
        try {
            val dbPath = Paths.get(dbFile)
            val configPath = Paths.get(configProperitesFile)
            Files.deleteIfExists(dbPath)
            Files.deleteIfExists(configPath)
            // This is fine if the directory already exists
            Files.createDirectories(Paths.get(baseDir))
        } catch (ioe: IOException) {
            throw IllegalStateException("TestConnectToSQLite:setup: error deleting file: " + ioe.message)
        }

        // write config file
        try {
            val bw = Utils.getBufferedWriter(configProperitesFile);
            bw.write("host=localHost\n");
            bw.write("user=sqlite\n");
            bw.write("password=sqlite\n");
            val dbName = "DB=${dbFile}";
            bw.write(dbName);
            bw.write("\n");
            bw.write("DBtype=sqlite\n");
            bw.close()
        } catch (exc: Exception) {
            throw IllegalStateException("testConnectToSQLITE: error creating config file " + exc.message);
        }

        // Verify we can connect when creating a new db
        val connection: Connection? = DBLoadingUtils.connection(configProperitesFile, true)
        Assertions.assertTrue(connection != null)

        // Verify we can connect when the db exists
        println("\nLCJ second sqlite connection")
        val connect2: Connection? = DBLoadingUtils.connection(configProperitesFile, false)
        Assertions.assertTrue(connect2 != null)

        // Verify we can insert data to the ref ranges table
        // This verifies the reference_ranges table exists, meaning our schema was correctly loaded

        val sql = "insert into reference_ranges (chrom, range_start,range_end) values(?,?,?)"
        try {
            val pstmt = connect2!!.prepareStatement(sql)
            pstmt.setString(1, "1")
            pstmt.setInt(2, 1050)
            pstmt.setInt(3, 3060)
            pstmt.executeUpdate()
        } catch (sqle: SQLException) {
            throw IllegalStateException("error adding reference range to db: " + sqle.message)
        }

        // Verify the data was inserted
        val query = "select count(*) from reference_ranges;"
        try {
            connect2.createStatement().executeQuery(query).use { rs ->
                var numRefRanges = 0
                if (rs.next()) {
                    numRefRanges = rs.getInt(1)
                }
                Assertions.assertEquals(1, numRefRanges)
            }
        } catch (sqle: SQLException) {
            throw IllegalStateException("error querying for reference_ranges count: " + sqle.message)
        }

    }
}