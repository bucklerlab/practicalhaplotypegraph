package net.maizegenetics.pangenome.utils

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.util.*
import javax.swing.ImageIcon

/**
 * Returns taxa names by HaplotypeNode id for given reference range ids.
 */
class TaxaByNodeByRangePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) :
    AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(TaxaByNodeByRangePlugin::class.java)

    private var rangeIds = PluginParameter.Builder("rangeIds", null, SortedSet::class.java)
        .description("Comma separated list of reference range ids to include in output")
        .required(true)
        .build()

    private lateinit var myGraph: HaplotypeGraph

    override fun preProcessParameters(input: DataSet) {
        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size == 1) { "TaxaByNodeByRangePlugin: preProcessParameters: must input one HaplotypeGraph" }
        myGraph = temp[0].data as HaplotypeGraph
    }

    override fun processData(input: DataSet): DataSet? {

        // Map<rangeid: Int, Map<hapid: Int, taxa: List<String>>>
        val result = myGraph.referenceRanges()
            .filter { rangeIds().contains(it.id()) }
            .associate { range ->
                range.id() to myGraph.nodes(range)
                    .associate { node ->
                        node.id() to node.taxaList().map { it.name }
                    }
            }

        return DataSet.getDataSet(result)

    }

    fun runPlugin(graph: HaplotypeGraph): Map<Int, Map<Int, List<String>>> {
        return performFunction(DataSet.getDataSet(graph)).getDataOfType(Map::class.java)[0].data as Map<Int, Map<Int, List<String>>>
    }

    /**
     * Comma separated list of reference range ids to include
     * in output
     *
     * @return Range Ids
     */
    fun rangeIds(): SortedSet<Int> {
        return rangeIds.value() as SortedSet<Int>
    }

    /**
     * Set Range Ids. Comma separated list of reference range
     * ids to include in output
     *
     * @param value Range Ids
     *
     * @return this plugin
     */
    fun rangeIds(value: SortedSet<*>): TaxaByNodeByRangePlugin {
        rangeIds = PluginParameter<SortedSet<*>>(rangeIds, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "TaxaByNodeByRangePlugin"
    }

    override fun getToolTipText(): String {
        return "TaxaByNodeByRangePlugin"
    }

}