package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.Sets
import com.google.common.collect.TreeRangeMap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.util.Utils
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {

}

object MafUtils {




    /**
     * Reads mafRecords into a list and separates them into sets that contain no overlapping blocks within the set
     * Designed to test anchorwave output, which should generate only one or two sets. If there are three sets, the
     * third set could contain overlapping blocks, but the first two will not. The method only creates the sets and reports
     * the number of blocks in each set.
     */
    fun evaluateMafOverlaps(mafFile: String) {
        println("Reading records in $mafFile")
        val mafRecords = loadInMAFFile(mafFile)
        //mafRecords are ordered
        //check if any consecutive records overlap
        //put overlapping records in a separate list

        val firstList = mutableListOf(mafRecords.first())
        val secondList = mutableListOf<MAFToGVCFPlugin.MAFRecord>()
        val thirdList = mutableListOf<MAFToGVCFPlugin.MAFRecord>()

        mafRecords.drop(1).forEach { mafrec ->
            val lastRecord = firstList.last()
            when {
                //not the same chromosome
                mafrec.refRecord.chromName != lastRecord.refRecord.chromName -> firstList.add(mafrec)
                //this records starts after the last one ends
                mafrec.refRecord.start >= lastRecord.refRecord.start + lastRecord.refRecord.size -> firstList.add(mafrec)
                //otherwise there is an overlap
                else -> {
                    //test for overlap with last record added to the second list and put in the third list if there is
                    if (secondList.size == 0) secondList.add(mafrec)
                    else {
                        val lastSecond = secondList.last()
                        when {
                            mafrec.refRecord.chromName != lastSecond.refRecord.chromName -> secondList.add(mafrec)
                            mafrec.refRecord.start >= lastSecond.refRecord.start + lastSecond.refRecord.size -> secondList.add(mafrec)
                            else -> thirdList.add(mafrec)
                        }
                    }
                }
            }
        }

        println ("Alignment block list one has ${firstList.size} blocks. The second list has ${secondList.size} entries.")
        println("The third list has ${thirdList.size} blocks.")

    }

    /**
     * Function to load in the MAF file.
     * This MAF file will need to only have 2 samples.  The first is assumed to be the Reference and the second is the Assembly.
     * Any extra will be ignored.
     * This will also skip over any e or i lines.
     */
    fun loadInMAFFile(mafFile: String) : List<MAFToGVCFPlugin.MAFRecord> {
        val regex = "\\s+".toRegex()

        val records = mutableListOf<MAFToGVCFPlugin.MAFRecord>()
        Utils.getBufferedReader(mafFile).use { reader ->
            var currentLine = reader.readLine()
            while (currentLine != null) {
                if(currentLine.isEmpty()) {
                    currentLine = reader.readLine()
                    continue
                }

                val tokens = currentLine.trim().split(regex)

                if(tokens[0] == "a") {
                    val score = tokens[1].split("=")[1].toDouble()

                    //Get the next two lines
                    currentLine = reader.readLine()
                    while(!currentLine.startsWith("s")) {
                        currentLine = reader.readLine()
                    }
                    val refAlignTokens = currentLine.trim().split(regex)
                    val refAlignment = MAFToGVCFPlugin.AlignmentBlock(
                        refAlignTokens[1],
                        refAlignTokens[2].toInt() + 1,
                        refAlignTokens[3].toInt(),
                        refAlignTokens[4],
                        refAlignTokens[5].toInt(),
                        refAlignTokens[6]
                    )

                    currentLine = reader.readLine()
                    while(!currentLine.startsWith("s")) {
                        currentLine = reader.readLine()
                    }
                    val altAlignTokens = currentLine.trim().split(regex)
                    val altAlignment = MAFToGVCFPlugin.AlignmentBlock(
                        altAlignTokens[1],
                        altAlignTokens[2].toInt() + 1,
                        altAlignTokens[3].toInt(),
                        altAlignTokens[4],
                        altAlignTokens[5].toInt(),
                        altAlignTokens[6]
                    )

                    records += MAFToGVCFPlugin.MAFRecord(score, refAlignment, altAlignment)

                }

                currentLine = reader.readLine()
            }
        }

        return records.sortedWith(compareBy({ Chromosome.instance(it.refRecord.chromName.split(".").last()) }, { it.refRecord.start }))
    }

    /**
     * Method takes 2 lists of Maf Records, returns the gaps indicating which
     * positions appear in the source that are not in the target.  The calling
     * method then augments the target with sequence from the gaps indicated
     * by the findGaps method.
     */
    fun findGaps(target: List<MAFToGVCFPlugin.MAFRecord>, source: List<MAFToGVCFPlugin.MAFRecord>):List<Range<Int>>? {

        val positionsSource = mutableSetOf<Int>()
        val positionsTarget = mutableSetOf<Int>()

        for (mafrec in target) {
            val start = mafrec.refRecord.start
            for ( position  in start until mafrec.refRecord.start + mafrec.refRecord.size) {
                positionsTarget.add(position)
            }
        }

        for (mafrec in source) {
            val start = mafrec.refRecord.start
            for ( position  in start until mafrec.refRecord.start + mafrec.refRecord.size) {
                positionsSource.add(position)
            }
        }

        // get positions contained in positionSource that aren't contained in positionsTarget
        val addedTgtPos = Sets.difference(positionsSource, positionsTarget)

        // Now make ranges of those positions
        return rangesFromPositions(addedTgtPos.toList())
    }

    // Take a list of ordered ints, creates ranges from those that are consecutive
    fun rangesFromPositions(positions:List<Int>): List<Range<Int>>? {
        val rangeList = mutableListOf<Range<Int>>()
        if (positions == null || positions.size == 0) {
            return null
        }
        if (positions.size == 1) {
            rangeList.add(Range.closed(positions[0],positions[0]))
            return rangeList
        }

        val sortedPositions = positions.sorted()
        var prevIndex = 0
        var index = 1
        var start = prevIndex
        while (index < positions.size ) {
            while (sortedPositions[index] == sortedPositions[prevIndex]+1) {
                index++
                prevIndex++
                if (index == positions.size) break
            }
            val newRange = Range.closed(sortedPositions[start],sortedPositions[prevIndex])
            rangeList.add(newRange)
            start = index
            prevIndex = index
            index++
        }
        return rangeList
    }

    /**
     * [target] is a mutable list of maf records that are assumed to be sorted by chromosome and start. If there are
     * reference positions present in source that are absent from target then the sequence for those positions
     * will be added to target.
     * MAF start positions are 0-based numbers
     */
    fun augmentList(target: MutableList<MAFToGVCFPlugin.MAFRecord>, source: List<MAFToGVCFPlugin.MAFRecord>)  {
        /**
        target assumed to be sorted.
        This method will add blocks or parts of blocks from source not covered in target.
        It follows these steps:
        1.  Sorts the records by chromosome
        2. Create a RangeMap from source list
        3.  For each cheomosome
            a. For each gap in target list
               1. make a range of the entire gap
               2. find the blocks from source that overlap the gap range
               3. extract maf records from those blocks that fall within the gap, this will require block splitting
               4. add those records to target
         **/

        //create a map of chromosome name -> RangeMap
        val chromToRangesSRC = mutableMapOf<String, ArrayList<MAFToGVCFPlugin.MAFRecord>>()
        val chromToRanges = mutableMapOf<String, RangeMap<Int, MAFToGVCFPlugin.MAFRecord>>() // needed for pulling sequence from submap
        for (mafrec in source) {
            val chrom = mafrec.refRecord.chromName
            // this needed for creating the list of gap ranges
            var chrMAFRecList = chromToRangesSRC[chrom]
            if (chrMAFRecList == null) {
                chrMAFRecList = ArrayList<MAFToGVCFPlugin.MAFRecord>()
                chromToRangesSRC[chrom] =  chrMAFRecList
            }
            chrMAFRecList!!.add( mafrec)

            // create a map of chromosome name -> RangeMap
            // This one needed for getting subranges later from the gaps
            var chrMap = chromToRanges[chrom]
            if (chrMap == null) {
                chrMap = TreeRangeMap.create()
                chromToRanges[chrom] = chrMap
            }
            val start = mafrec.refRecord.start
            val end = mafrec.refRecord.start + mafrec.refRecord.size - 1
            chrMap!!.put(Range.closed(start,end), mafrec)
        }

        // GEt the target positions
        // This creates a map of chromName (key) to a list of MAFtoGVCFPlugin.MAFRecords (value)
        val chromToRangesTGT = target.groupBy{it.refRecord.chromName}

        val addedBlocks = mutableListOf <MAFToGVCFPlugin.MAFRecord>()
        // Not all chromosomes will be in both target and src
        println("augmentList: number of chromosomes in chromToMAFRecords = ${chromToRangesSRC.keys.size}")
        for (chrom in chromToRangesSRC.keys) {
            val srcRanges = chromToRangesSRC[chrom]
            val tgtRanges = chromToRangesTGT[chrom]

            if (tgtRanges == null) {
                addedBlocks.addAll(srcRanges!!.toList())
                println("augmentList: NO target ranges for chrom ${chrom} - adding all ranges from source chrom ${chrom} to target's list")
                continue
            }

            val newRanges = findGaps(tgtRanges!!,srcRanges!!)
            if (newRanges == null) {
                println("augmentList: newRanges for chrom ${chrom} is NULL ,continue")
                continue
            }

            println("augmentList: found gaps: process gaps for chrom ${chrom}, number of gaps=${newRanges.size}")
            // take those ranges, run through Peter's code to get sequence
            for (gap in newRanges) {

                val sourceRangeMap = chromToRanges[chrom]
                if (sourceRangeMap != null) {
                    val gapRecords = sourceRangeMap.subRangeMap(gap).asMapOfRanges()
                    if (gapRecords.size > 0) {
                        for (sourceRecord in gapRecords.values) {
                            val subRecord = extractSubMafRecord(gap.lowerEndpoint(),gap.upperEndpoint(), sourceRecord)
                            if (subRecord != null) addedBlocks.add(subRecord)
                        }
                    }
                }
            }
        }

        println("augmentList at end: size of addedBlocks ${addedBlocks.size}")
        target.addAll(addedBlocks)
    }

    fun extractSubMafRecord(start: Int, end: Int, mafRecord: MAFToGVCFPlugin.MAFRecord) : MAFToGVCFPlugin.MAFRecord? {
        //if the maf record starts after end return null
        //if the maf record ends before start return null
        //if the maf record falls entirely with [start,end] return mafRecord
        //else extract the parts of the ref and alt blocks between start and end to a new mafRecord
        val mafRecordStart = mafRecord.refRecord.start
        val mafRecordEnd = mafRecord.refRecord.start + mafRecord.refRecord.size - 1
        return when {
            mafRecordStart > end -> null
            mafRecordEnd < start -> null
            mafRecordStart >= start && mafRecordEnd <= end -> mafRecord
            else -> {
                //return only part of the ref and alt blocks
                //which part?
                val newStart = max(start, mafRecordStart)
                val newEnd = min(end, mafRecordEnd)
                val refBlock = mafRecord.refRecord.alignment
                val blockStart = if (newStart == start) newStart - mafRecordStart else mafRecordStart
                val blockEnd = blockStart + (newEnd - newStart)
                val blockIndices = indexOfNonGapCharacters(refBlock, blockStart, blockEnd)
                //val blockIndices = indexOfNonGapCharacters(refBlock, newStart - start, newEnd - start)

                MAFToGVCFPlugin.MAFRecord(mafRecord.score,
                    extractAlignmentBlock(mafRecord.refRecord, blockIndices),
                    extractAlignmentBlock(mafRecord.altRecord, blockIndices)
                )
            }
        }

    }

    fun indexOfNonGapCharacters(seq: String, start: Int, end: Int) : IntArray {
        var index = 0
        var nonDashCount = 0
        val DASH = '-'
        var nonDashTarget = start + 1
        while (nonDashCount < nonDashTarget) {
            if (seq[index++] != DASH ) nonDashCount++
        }
        val startIndex = index - 1
        nonDashTarget = end + 1
        while (nonDashCount < nonDashTarget) {
            if (seq[index++] != DASH ) nonDashCount++
        }
        val endIndex = index - 1

        return intArrayOf(startIndex, endIndex)
    }

    fun extractAlignmentBlock(block: MAFToGVCFPlugin.AlignmentBlock, indices: IntArray) : MAFToGVCFPlugin.AlignmentBlock {
        val dash = '-'
        val subBlock = block.alignment.substring(indices[0], indices[1] + 1)
        val subStart = if (indices[0] == 0) block.start else {
            val skipBlock = block.alignment.substring(0, indices[0])
            val numberNonDash = skipBlock.count { it != dash }
            block.start + numberNonDash
        }
        val numberOfNonDashChar = subBlock.count { it != dash }
        return MAFToGVCFPlugin.AlignmentBlock(block.chromName, subStart, numberOfNonDashChar, block.strand, block.chrSize, subBlock)
    }
}