#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/W22/5509_6665_23535_HGCJCBGXX_W22_GCCAAT_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/W22/W22HVMTCCXXL7_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/W22/W22_HVMFTCCXX_L8_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_ALLchr_genesExactCoordinates_chr8.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 48 --min_base_qual 24 \
   W22_min_map_qual_48_min_base_qual_24_haplotype_caller_output.g.vcf

