**Anchor Coordinate Pipeline**

The best practices for processing maize inbred lines for creating anchor 
region haplotypes is the following:

![AnchorCoordinatesPipelinePic.png](AnchorCoordinatesPipelinePic.png)

Alignment-> Filter Alignment -> Sentieon GATK Haplotype -> Filter GVCF -> 
Create Fasta -> Load in DB

Details:

**Initial Intervals File from V4 GFF Genes**
* Gff gene file from ftp://ftp.ensemblgenomes.org/pub/release-34/plants/gff3/zea_mays, stored in practicalhaplotypegraph data folder.
* Java Program:  src/main/java/net/maizegenetics/pangenome/db_loading/CreateIntervalsFileFromGeneGffPlugin.java
* Key parameters are: 
    * String geneFile
    * String output directory
    * int number of flanking bps to add
    * refFile (to calculate chrom len when adding flanking regions)

**Alignment** 
* Alignment is done with BWA-MEM with the following settings:
* Script: src/main/scripts/bwa/runBWAMemTemplate.sh
* Key parameters are: 

**Filter Alignment**
The alignment is then filtered to remove the low quality mapping reads, and can optionally be filtered to only 
include the anchor regions (saves size) + and - 2000bp.  The buffer is included as GATK uses a Active window, so the 
surrounding bp allow this to perform.
* Filtering is done with samtools 1.3.1 Using htslib 1.3.1
* Script: src/main/scripts/InbredHaplotyperPipeline/filter_bam.sh
* Key parameters: -q 48 (mapping quality)
* Anchor file with buffer: data/hackathon_trimmed_intervals_plusminus1000.bed

**Sentieon GATK Haplotype**
Inbred haplotypes are then called using Sentieon's version of the GATK 3.X.  Haplotypes are only called in the anchor 
regions, this outputs a GVCF file
* Software: sentieon-genomics-201704.01
* Script: src/main/scripts/InbredHaplotyperPipeline/run_haplotype_caller.sh
* Key parameters: --algo Haplotyper --ploidy 1 --emit_mode gvcf
* Anchor file: data/hackathon_trimmed.intervals

**Filter GVCF**
Low quality SNPs and reference ranges are then eliminated from GVCF.  We would prefer to have Ns in the resulting
rather than errors.
* Software: Current: bcftools, Future: Tassel filter
* Script:src/main/scripts/InbredHaplotyperPipeline/FilterGVCFs.sh
Median depth filtering
* Key parameters: -e "FORMAT/DP>16 || FORMAT/DP<3 || FORMAT/GQ<50 || QUAL<200 || (FORMAT/AD[0]>0 && FORMAT/AD[1]>0) || (FORMAT/AD[0]>0 && FORMAT/AD[2]>0) || (FORMAT/AD[1]>0 && FORMAT/AD[2]>0)"
* _Note in the above, there are double ampersands between the FORMAT/AD[0]>0 and FORMAT/AD[1]>0 and all the similar conditions.  It is not showing up in IntelliJs preview._

**Create Tabulate Coverage and Conservation**
???
* Software:
* Script:
* Key parameters:

**Find anchor coordinates**
???
* Software:
* Script:
* Key parameters:

**Load coordinates in DB**
