package net.maizegenetics.pangenome.hapCalling;

import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import net.maizegenetics.dna.tag.FastqReader;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

/**
 * Created by zrm22 on 9/13/17.
 */
public class FilterFastqUsingBAMPlugin extends AbstractPlugin {

    private static final Logger myLogger = LogManager.getLogger(FilterFastqUsingBAMPlugin.class);

    private PluginParameter<String> myReference = new PluginParameter.Builder<>("refFile", null, String.class)
            .description("File Name of the reference fasta")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myReads = new PluginParameter.Builder<>("readFile", null, String.class)
            .description("File Name of the read fastq")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myBamFile = new PluginParameter.Builder<>("bamFile", null, String.class)
            .description("File Name of the bam file")
            .inFile()
            .required(true)
            .build();

    private PluginParameter<String> myOutput = new PluginParameter.Builder<>("outputFile", null, String.class)
            .description("File Name of the filtered read output")
            .outFile()
            .required(true)
            .build();

    public FilterFastqUsingBAMPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        filterFastqByBamInformation(reference(),reads(),bamFile(),output());

        return null;
    }


    private void filterFastqByBamInformation(String refSequence, String fastqFileName, String bamFileName, String outputFileName) {
        try(BufferedWriter writer = Utils.getBufferedWriter(outputFileName)) {
            //Read the fastqFile
            Map<String,String[]> fastqRecords = readFastqFile(fastqFileName);

            Set<String> fastqRecordsToRemove = new HashSet<>();

            SamReader samReader = SamReaderFactory.makeDefault().referenceSequence(new File(refSequence)).open(new File(bamFileName));
            SAMRecordIterator samIterator = samReader.iterator();

            LongAdder numberOfReadsAfterFilter = new LongAdder();
            while (samIterator.hasNext()) {
                SAMRecord samRecord = samIterator.next();

                if(samRecord.getReadUnmappedFlag()) {
                    //do nothing, but we have to check otherwise it will be filtered out as it will have a 0 edit distance
                }
                //if the record has a match where NM is below 20% of the read length we add to the list to filter as it means we hit an interanchor region with this read
                else if(samRecord.getIntegerAttribute("NM") <= fastqRecords.get(samRecord.getReadName())[1].length()*.2) {
                    fastqRecordsToRemove.add(samRecord.getReadName());
                }
            }

            java.util.List<String[]> listOfFilteredFastqRecords = fastqRecords.keySet().stream().filter(readName -> !fastqRecordsToRemove.contains(readName)).map(readName -> fastqRecords.get(readName)).collect(Collectors.toList());

            //loop thorough the list and print out any records we want to keep
            for(String[] currentFastqRecord : listOfFilteredFastqRecords) {
                writer.write(currentFastqRecord[0]);
                writer.write("\n");
                writer.write(currentFastqRecord[1]);
                writer.write("\n");
                writer.write(currentFastqRecord[2]);
                writer.write("\n");
                writer.write(currentFastqRecord[3]);
                writer.write("\n");
            }

            myLogger.info("Number of reads Pre Filter:"+fastqRecords.keySet().size()+", Number of reads Post Filter:"+listOfFilteredFastqRecords.size());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    private static Map<String,String[]> readFastqFile(String fastqFileName) {
        Map<String, String[]> fastqMapping = new HashMap<>();

        FastqReader reader = new FastqReader(fastqFileName);

        String[] fastqRecord = null;
        while((fastqRecord = reader.getNext())!=null) {
            fastqMapping.put(fastqRecord[0].split(" ")[0].substring(1),fastqRecord);
        }

        reader.close();
        return fastqMapping;
    }


    @Override
    public String pluginUserManualURL() {
        //TODO
        return "https://bitbucket.org/tasseladmin/tassel-5-source/wiki/UserManual/Kinship/Missing";
    }

    @Override
    public ImageIcon getIcon() {
        URL imageURL = null;
        if (imageURL == null) {
            return null;
        } else {
            return new ImageIcon(imageURL);
        }
    }

    @Override
    public String getButtonName() {
        return "PHG CountSAMReads";
    }

    @Override
    public String getToolTipText() {
        return "PHG Get counts for how many reads align to each Contig in the reference";
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // public static void main(String[] args) {
    //     GeneratePluginCode.generate(FilterFastqUsingBAMPlugin.class);
    // }

    /**
     * Convenience method to run plugin with one return object.
     */
//    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * File Name of the reference fasta
     *
     * @return Ref File
     */
    public String reference() {
        return myReference.value();
    }

    /**
     * Set Ref File. File Name of the reference fasta
     *
     * @param value Ref File
     *
     * @return this plugin
     */
    public FilterFastqUsingBAMPlugin reference(String value) {
        myReference = new PluginParameter<>(myReference, value);
        return this;
    }

    /**
     * File Name of the read fastq
     *
     * @return Read File
     */
    public String reads() {
        return myReads.value();
    }

    /**
     * Set Read File. File Name of the read fastq
     *
     * @param value Read File
     *
     * @return this plugin
     */
    public FilterFastqUsingBAMPlugin reads(String value) {
        myReads = new PluginParameter<>(myReads, value);
        return this;
    }

    /**
     * File Name of the bam file
     *
     * @return Bam File
     */
    public String bamFile() {
        return myBamFile.value();
    }

    /**
     * Set Bam File. File Name of the bam file
     *
     * @param value Bam File
     *
     * @return this plugin
     */
    public FilterFastqUsingBAMPlugin bamFile(String value) {
        myBamFile = new PluginParameter<>(myBamFile, value);
        return this;
    }

    /**
     * File Name of the filtered read output
     *
     * @return Output File
     */
    public String output() {
        return myOutput.value();
    }

    /**
     * Set Output File. File Name of the filtered read output
     *
     * @param value Output File
     *
     * @return this plugin
     */
    public FilterFastqUsingBAMPlugin output(String value) {
        myOutput = new PluginParameter<>(myOutput, value);
        return this;
    }


}
