package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.hapCalling.SplitKeyFilePlugin
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

class BuildSLURMBatchScriptPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(BuildSLURMBatchScriptPlugin::class.java)

    private var inputKeyFile = PluginParameter.Builder("inputKeyFile", null, String::class.java)
        .guiName("Input Key File")
        .inFile()
        .required(true)
        .description("Name of the inputKeyFile file to process.")
        .build()

    private var readDir = PluginParameter.Builder("readDir", null, String::class.java)
        .guiName("Fasta or Fastq dir to process")
        .inDir()
        .required(true)
        .description("Name of the Fasta/Fastq dir to process.")
        .build()


    private var numNodes = PluginParameter.Builder("numNodes", null, Int::class.javaObjectType)
        .required(true)
        .description("Number Of Nodes to be requested")
        .build()

    private var numCores = PluginParameter.Builder("numCores", null, Int::class.javaObjectType)
        .required(true)
        .description("Number Of Cores to be requested")
        .build()

    private var numConsecutiveJobs = PluginParameter.Builder("numConsecJobs", null, Int::class.javaObjectType)
        .required(true)
        .description("Number Of Consecutive jobs to run.")
        .build()

    private var outputSLURMScript = PluginParameter.Builder("outputScript",null, String::class.java)
        .guiName("Output SLURM Script")
        .outFile()
        .required(true)
        .description("Name of the Output SLURM Script")
        .build()

    private var outputTargetNameFile = PluginParameter.Builder("outputTargetNameFile",null, String::class.java)
        .guiName("Output Target Name File")
        .outFile()
        .required(true)
        .description("Name of the Output Target Name File")
        .build()

    private var emailAddress = PluginParameter.Builder("emailAddress",null, String::class.java)
        .guiName("Email Address")
        .required(true)
        .description("Email Address to send notification when done")
        .build()

    private var loadConda = PluginParameter.Builder("loadFromConda", true, Boolean::class.javaObjectType)
        .guiName("Load From Conda")
        .required(false)
        .description("Load dependencies from conda")
        .build()

    override fun processData(input: DataSet?): DataSet? {

        //Determine which fastq files we actually have
        val fileNamesInDir = getFileNamesFromInputDir(readDir()).toSet()

        val (headerMap, records) = readInKeyFile(inputKeyFile())

        val fileNameCol = headerMap["filename"]?: -1
        val taxonCol = headerMap["cultivar"]?:-1
        val flowcellCol = headerMap["flowcell_lane"]?:-1

        val outputFileNames = records.filter { fileNamesInDir.contains(it[fileNameCol]) }
            .map { "${it[taxonCol]}_${it[flowcellCol]}_ReadMapping.txt\t" }

        //Convert each fastq file into an output read mapping file
        Utils.getBufferedWriter(outputTargetNameFile()).use { output ->
            //Write out those file names to a file for the SLURM script to work on.

                outputFileNames.forEach { output.write("$it\n") }
        }

        //Write out the SLURM file
        Utils.getBufferedWriter(outputSLURMScript()).use { output ->
            output.write("#!/bin/bash\n")
            output.write("#SBATCH --ntasks=1\n")
            output.write("#SBATCH -N ${numNodes()}\n")
            output.write("#SBATCH -n ${numCores()}\n")
            output.write("#SBATCH --mail-user=${emailAddress()}\n")
            output.write("#SBATCH --mail-type=BEGIN,END,FAIL\n")
            output.write("#SBATCH --array=1-${outputFileNames.size}%${numConsecutiveJobs()}\n\n")

            output.write("module load python_3\n")

            if(loadConda()) {
                output.write("module load miniconda\n")

                output.write("source activate snakemake-mapping\n")
            }
            else {
                output.write("module load snakemake\n")
                output.write("module load minimap2\n")
                output.write("module load samtools\n")
                output.write("module load java/11.0.2\n")
            }

            output.write("LINE=$(sed -n \"\$SLURM_ARRAY_TASK_ID\"p ${outputTargetNameFile()})\n")
            output.write("snakemake -c${numCores()/numConsecutiveJobs()} --rerun-incomplete -F -R -p readMappingFiles/\$LINE\n")
        }

        return null
    }

    private fun getFileNamesFromInputDir(readDir: String): List<String> {
        return File(readDir).walk().map { it.name }
            .toList()
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = SplitKeyFilePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "BuildSLURMBatchScriptPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to make a SLURM Job array script."
    }


    /**
     * Name of the inputKeyFile file to process.
     *
     * @return Input Key File
     */
    fun inputKeyFile(): String {
        return inputKeyFile.value()
    }

    /**
     * Set Input Key File. Name of the inputKeyFile file to
     * process.
     *
     * @param value Input Key File
     *
     * @return this plugin
     */
    fun inputKeyFile(value: String): BuildSLURMBatchScriptPlugin {
        inputKeyFile = PluginParameter<String>(inputKeyFile, value)
        return this
    }

    /**
     * Name of the Fasta/Fastq dir to process.
     *
     * @return Fasta or Fastq dir to process
     */
    fun readDir(): String {
        return readDir.value()
    }

    /**
     * Set Fasta or Fastq dir to process. Name of the Fasta/Fastq
     * dir to process.
     *
     * @param value Fasta or Fastq dir to process
     *
     * @return this plugin
     */
    fun readDir(value: String): BuildSLURMBatchScriptPlugin {
        readDir = PluginParameter<String>(readDir, value)
        return this
    }

    /**
     * Number Of Nodes to be requested
     *
     * @return Num Nodes
     */
    fun numNodes(): Int {
        return numNodes.value()
    }

    /**
     * Set Num Nodes. Number Of Nodes to be requested
     *
     * @param value Num Nodes
     *
     * @return this plugin
     */
    fun numNodes(value: Int): BuildSLURMBatchScriptPlugin {
        numNodes = PluginParameter<Int>(numNodes, value)
        return this
    }

    /**
     * Number Of Cores to be requested
     *
     * @return Num Cores
     */
    fun numCores(): Int {
        return numCores.value()
    }

    /**
     * Set Num Cores. Number Of Cores to be requested
     *
     * @param value Num Cores
     *
     * @return this plugin
     */
    fun numCores(value: Int): BuildSLURMBatchScriptPlugin {
        numCores = PluginParameter<Int>(numCores, value)
        return this
    }

    /**
     * Number Of Consecutive jobs to run.
     *
     * @return Num Consec Jobs
     */
    fun numConsecutiveJobs(): Int {
        return numConsecutiveJobs.value()
    }

    /**
     * Set Num Consec Jobs. Number Of Consecutive jobs to
     * run.
     *
     * @param value Num Consec Jobs
     *
     * @return this plugin
     */
    fun numConsecutiveJobs(value: Int): BuildSLURMBatchScriptPlugin {
        numConsecutiveJobs = PluginParameter<Int>(numConsecutiveJobs, value)
        return this
    }

    /**
     * Name of the Output SLURM Script
     *
     * @return Output SLURM Script
     */
    fun outputSLURMScript(): String {
        return outputSLURMScript.value()
    }

    /**
     * Set Output SLURM Script. Name of the Output SLURM Script
     *
     * @param value Output SLURM Script
     *
     * @return this plugin
     */
    fun outputSLURMScript(value: String): BuildSLURMBatchScriptPlugin {
        outputSLURMScript = PluginParameter<String>(outputSLURMScript, value)
        return this
    }

    /**
     * Name of the Output Target Name File
     *
     * @return Output Target Name File
     */
    fun outputTargetNameFile(): String {
        return outputTargetNameFile.value()
    }

    /**
     * Set Output Target Name File. Name of the Output Target
     * Name File
     *
     * @param value Output Target Name File
     *
     * @return this plugin
     */
    fun outputTargetNameFile(value: String): BuildSLURMBatchScriptPlugin {
        outputTargetNameFile = PluginParameter<String>(outputTargetNameFile, value)
        return this
    }

    /**
     * Email Address to send notification when done
     *
     * @return Email Address
     */
    fun emailAddress(): String {
        return emailAddress.value()
    }

    /**
     * Set Email Address. Email Address to send notification
     * when done
     *
     * @param value Email Address
     *
     * @return this plugin
     */
    fun emailAddress(value: String): BuildSLURMBatchScriptPlugin {
        emailAddress = PluginParameter<String>(emailAddress, value)
        return this
    }

    /**
     * Load dependencies from conda
     *
     * @return Load From Conda
     */
    fun loadConda(): Boolean {
        return loadConda.value()
    }

    /**
     * Set Load From Conda. Load dependencies from conda
     *
     * @param value Load From Conda
     *
     * @return this plugin
     */
    fun loadConda(value: Boolean): BuildSLURMBatchScriptPlugin {
        loadConda = PluginParameter<Boolean>(loadConda, value)
        return this
    }
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(BuildSLURMBatchScriptPlugin::class.java)
}