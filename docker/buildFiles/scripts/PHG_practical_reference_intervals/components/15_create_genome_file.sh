ref_fasta_f=$1

#st_bin=$(which samtools)

$st_bin faidx $ref_fasta_f
$awk_bin 'BEGIN{OFS="\t"}{print $1,$2}' ${ref_fasta_f}.fai | \
sort -k1,1V -t$'\t'
