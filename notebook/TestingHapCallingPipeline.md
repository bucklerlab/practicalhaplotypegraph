#This document denotes all the testing done to the HaplotypeCalling from GBS Pipeline from Aug, 28 2017-present.
####Should be used to keep track of each step and the decisions made to debug the pipeline.

## Important Files
* Current database(has 308 taxa with 109 of them making consensus):

        cbsublfs1.tc.cornell.edu://data1/PHG/phg_dbs/aug2017_308haps_ConsensusWithVariantBLOB.db
        
* Interanchor fasta

        cbsublfs1.tc.cornell.edu://data1/PHG/HapCallingTestFiles/InterAnchorFasta/OnlyInteranchorsB73.fa
        
* Consensus anchor fasta(109 taxa making up the consensus)

        cbsublfs1.tc.cornell.edu://data1/PHG/HapCallingTestFiles/ConsensusAnchorFasta/fullConsensus110taxa.fa

* GBS Reads(Chris Bottoms W22)
  * Barcode stripped:
  
        cbsublfs1.tc.cornell.edu://data1/PHG/GBSAlignmentTests/ChrisBottomsData/W22ReadsNoBarcodes/B73W22Cross_W22-Brink-std_D0DW9ACXX_4_CCACTCA.fastq
        
  * After filtering by length>=80
  
        cbsublfs1.tc.cornell.edu://data1/PHG/GBSAlignmentTests/ChrisBottomsData/W22ReadsFilteredByLength/B73W22Cross_W22-Brink-std_D0DW9ACXX_4_CCACTCAFilteredByLength.fastq
        
  * After aligning to interanchor and removing any where NM<=.2*readLength
  
        cbsublfs1.tc.cornell.edu://data1/PHG/GBSAlignmentTests/ChrisBottomsData/W22ReadsFilteredByLength/B73W22Cross_W22-Brink-std_D0DW9ACXX_4_CCACTCAFilteredByLengthNoInterAnchorReads.fastq
  
* GBS BAM Files(Chris Bottoms W22) 
  * BAM of length filtered reads aligned to inter anchors(index is also in this dir)

        cbsublfs1.tc.cornell.edu://data1/PHG/GBSAlignmentTests/ChrisBottomsData/BAMFiles/AlignedToInteranchors/W22-Brink-std_D0DW9ACXX_4_CCACTCA_interAnchorAligned.bam
    
  * BAM Aligned to 110 Taxa consensus from BLOB DB(index is also there)
  
        cbsublfs1.tc.cornell.edu://data1/PHG/GBSAlignmentTests/ChrisBottomsData/BAMFiles/AlignedToConsensusAnchors/B73W22Cross_W22-Brink-std_D0DW9ACXX_4.bam




##Current Pipeline
* Trim barcodes off of each fastq by sample
* Filter Out any short reads:
    * This awk command will remove anything below 80 bps
    
            awk 'BEGIN {OFS = "\n"} {header = $0 ; getline seq ; getline qheader ; getline qseq ; if (length(seq) >=80) {print header, seq, qheader, qseq}}' < inputFastq.fastq > outputFastq.fastq
* Align to the interanchor fasta using BWA
    * Example Script
    
            cbsublfs1.tc.cornell.edu://data1/PHG/HapCallingTestFiles/Scripts/run_bwa_filterInterAnchors.sh
* Run FilterFastqUsingBAMPlugin
    * Use the reference for the Inter anchors
* Align filtered reads against consensus anchor fasta
    * To generate the consensus anchor fasta from the db do the following.
  
            public static HaplotypeGraph buildHaplotypeGraph() {
                HaplotypeGraphBuilderPlugin graphBuilder = new HaplotypeGraphBuilderPlugin(null, false)
                        .databaseName(dbFileName) //String path for the db file
                        .includeVariantContexts(true)
                        .method("collapse_method_1"); //This will change with new dbs
                
                return graphBuilder.build();
            }
    * Then call
  
            GraphIO.writeFasta(HaplotypeGraph graph, String outputFastaFileName);				
    * Example script:
    
            cbsublfs1.tc.cornell.edu://data1/PHG/HapCallingTestFiles/Scripts/run_bwa.sh
* Once you have the bam file you can run the HMM(IN DEVELOPMENT)
    * Sam Counter information( Note referenceFasta is Consensus anchor fasta for these alignments)
        * Call the original counter - No wildcard matching, you can specify how many mistakes you want to allow. 0 means perfect matches(AS = lengthOfRead).  There are 4 methods depending on the files.
    
                 Multiset<Integer> counts = SamCounter.countSamFile(PHGSQLite dbObject, String genomeVersionName, 
                                                                    String methodIdName, String samFileName, 
                                                                    String readFileName, int numOfAlignmentMistakes);
                 
                 Multiset<Integer> counts = SamCounter.countSamFile(PHGSQLite dbObject, String genomeVersionName, 
                                                                    String methodIdName, List<SAMRecord> samRecords, 
                                                                    Map<String,Integer> readNameToLengthMap, int numOfAlignmentMistakes);
                 
                 Multiset<Integer> counts = SamCounter.countSamFile(String samFileName, String readFileName, int numOfAlignmentMistakes);
                 
                 Multiset<Integer> counts = SamCounter.countSamFile(List<SAMRecord> samRecords, Map<String,Integer> readNameToLengthMap, int numOfAlignmentMistakes);
            * I suggest using either of the last 2 if you use a PHG generated fasta.  The first two can be used if you concatenate all the fasta records coming from the consensus fastas instead.  
        * Call the counter which allows for a certain number of Ns as wildcards
    
                 Multiset<Integer> counts = SamCounter.countSamFileWildcardNs(String samFileName, String readFileName, String referenceFasta, 
                                                                        int numberOfAlignmentMistakes, int maxNs);
                                                                        
                 Multiset<Integer> counts = SamCounter.countSamFileWildcardNs(String samFileName, String readFileName, GenomeSequence referenceSequence, 
                                                                        int numOfAlignmentMistakes, int maxNs);                                                              
            * If you need to process a lot of Sam files, use the second method as you can make the GenomeSequence once and keep passing it in.
            * The N wildcard method works probably the best with maxNs = 5 and numAlignmentMistakes = 0
                * Has about 11% error for the two taxa test, but has a decent amount of anchors where the difference between the two choices are greater than 5
            
        * The Third style of Counting.  Wildcard with tossing reads which do not cover enough taxa.
            * Note this has not been tested much and likely contains a couple of errors.
        
                    Multiset<Integer> counts = SamCounter.countSamFileWildcardNsWithTaxaCoverage(String samFileName, String readFileName, String referenceFasta, 
                                                                                int numOfAlignmentMistakes, int maxNs, 
                                                                                HaplotypeGraph phg,double percentage);
            
                    
                    Multiset<Integer> counts = SamCounter.countSamFileWildcardNsWithTaxaCoverage(String samFileName, String readFileName, GenomeSequence referenceSequence, 
                                                                                int numOfAlignmentMistakes, int maxNs, 
                                                                                HaplotypeGraph phg, double percentage);   
            * The PHG object is required as you need to figure out how many taxa each Reference range covers.
            * This tends to increase the number of errors from the straight wildcard distance based.  We are seeing 20-30% error depending on the percentage.  
            * However, we are losing a lot of the BadAnchor errors from before. This means that this method tends to equalize the counts between the 2 taxa.
            * I would not trust this counting method fully.
        
        
  * Run the HMM, You can use Code found in ConvertGBSUtils.  Refer to Peter for help with this.  You will likely need to update this class to build your own Multiset<HapId>
   
  * Export the HaplotypePath to a VCF file:
  
        List<VariantContext> variantContexts = HapCallingUtils.getVariantContextFromHaplotypeNodeList(path);
        HapCallingUtils.writeVariantContextsToVCF(variantContexts, outputVCFFile, null, “W22PHG_Aligned”);
        
    * Where "W22PHG_Aligned" is the name of the taxa you want to export in the VCF file.
    * The null is for the reference.  If you want an index created, specify the reference file(in this case the B73 ref).  If you put a null in it will just export the vcf.
    
            cbsublfs1.tc.cornell.edu://data1/PHG/GVCFTyperInputs/agpv4Ref/Zea_mays.AGPv4.dna.toplevel.fa.gz
    * We should probably wrap this code into a Plugin, but for now it is a utility function.