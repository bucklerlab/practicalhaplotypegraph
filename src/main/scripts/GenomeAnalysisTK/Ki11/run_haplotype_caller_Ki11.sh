#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/Ki11_HFFGKADXX_CTTGTA.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Ki11_HBEN2ADXX_CTTGTA.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Ki11_HL5WNCCXX_L5.clean_srt_dedup.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o Ki11_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant Ki11_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o Ki11_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V Ki11_haplotype_caller_output.vcf \
   -o Ki11_haplotype_caller_output.fasta

