package net.maizegenetics.pangenome.db_loading;


import net.maizegenetics.pangenome.api.CreateGraphUtils;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by zrm22 on 1/16/18.
 *
 * Simple Plugin to Create The interval BED files from the database.
 * If the user provides a extendedBedFile name, it will add the windowSize to both ends of the genome interval.
 */
public class CreateIntervalBedFilesPlugin extends AbstractPlugin  {

    private static final Logger myLogger = LogManager.getLogger(CreateIntervalBedFilesPlugin.class);


    private PluginParameter<String> dbConfigFile = new PluginParameter.Builder<>("dbConfigFile",null,String.class)
            .description("File holding the DB config information")
            .required(true)
            .inFile()
            .build();

    private PluginParameter<String> myBedFile = new PluginParameter.Builder<>("bedFile", "intervals.bed", String.class)
            .description("Name for the bed file")
            .outFile()
            .build();

    private PluginParameter<String> myExtendedBedFile = new PluginParameter.Builder<>("extendedBedFile", null, String.class)
            .description("Name for the extended bed file")
            .outFile()
            .build();

    private PluginParameter<Integer> windowSize = new PluginParameter.Builder<>("windowSize",1000, Integer.class)
            .description("Window size to add to the extendedBedFile")
            .build();


    private PluginParameter<String> myRefRangeMethod = new PluginParameter.Builder<>("refRangeMethods", null, String.class)
            .description("Comma separated list of reference Range methods needed to create a bed file.  Generally you will want to specify methods defined by the user in the name column of the intervals file used when the database was loaded.")
            .required(true)
            .build();



    public CreateIntervalBedFilesPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        //Get a db connection
        myLogger.debug("Getting the db connection from the file");
        Connection conn = DBLoadingUtils.connection(dbConfigFile(),false);

        myLogger.debug("Pulling the reference ranges from the graph stored in the database");
        //Get the referenceRange objects from the db
        SortedSet<ReferenceRange> ranges = CreateGraphUtils.referenceRanges(conn);
        List<String> refMethodsToRetain = new ArrayList(Arrays.asList(refRangeMethod().split(",")));
        SortedSet<ReferenceRange> selectedRanges = new TreeSet<>( ranges.stream().filter(range -> checkRefRangeMethods(range,refMethodsToRetain)).collect(Collectors.toSet()));


        myLogger.debug("Writing out the BED files using the reference ranges pulled from the graph.");
        //export the ranges to the bed files
        writeOutBedFiles(selectedRanges);



        return null;
    }

    private boolean checkRefRangeMethods(ReferenceRange refRange, List<String> methodNames) {
        long numberOfMethodsCovered = methodNames.stream().map(name -> refRange.isPartOf(name))
                .filter(rangeIsPartOfMethod -> rangeIsPartOfMethod == true)
                .count();

        if(numberOfMethodsCovered<=0) {
            return false;
        }
        else {
            return true;
        }


    }

    /**
     * Method to write out the Bed files based on the reference ranges.
     *
     * Note that BED files are zero-based inclusive-exclusive.
     * @param ranges
     */
    private void writeOutBedFiles(SortedSet<ReferenceRange> ranges) {
        try {
            //Create the file writers
            BufferedWriter bedFileWriter = Utils.getBufferedWriter(bedFile());
            BufferedWriter bedExtendedWriter = null;
            //If the extended bed file name is not null we want to write
            if(extendedBedFile() != null) {
                bedExtendedWriter = Utils.getBufferedWriter(extendedBedFile());
            }

            //Loop through the reference ranges and export the start and end positions split by tabs.
            for(ReferenceRange referenceRange : ranges) {

                //Create the original window string
                bedFileWriter.write(referenceRange.chromosome().getName());
                bedFileWriter.write("\t");
                bedFileWriter.write(""+(referenceRange.start()-1));
                bedFileWriter.write("\t"); //inclusive start zero based
                bedFileWriter.write(""+referenceRange.end());
                bedFileWriter.write("\n"); //exclusive end zero based

                if(bedExtendedWriter != null) {
                    //Create the extended window string
                    bedExtendedWriter.write(referenceRange.chromosome().getName());
                    bedExtendedWriter.write("\t");
                    bedExtendedWriter.write(""+((referenceRange.start()-windowSize()<1)? 0 : referenceRange.start()-windowSize()));
                    bedExtendedWriter.write("\t"); //inclusive start zero based, we need to check to see if we slide it past the beginning of the chromosome
                    bedExtendedWriter.write(""+(referenceRange.end()+windowSize()));
                    bedExtendedWriter.write("\n"); //exclusive end zero based
                }

            }


            //Closing the file writers
            bedFileWriter.close();
            if(bedExtendedWriter!=null) {
                bedExtendedWriter.close();
            }
        }
        catch(Exception exc) {
            myLogger.error("Error creating the BED file.");
            throw new IllegalStateException(exc);
        }
    }


    @Override
    public String getButtonName() {
        return "CreateIntervalBedFilesPlugin";
    }

    @Override
    public String getToolTipText() {
        return "Create BED files from the DB.";
    }

    @Override
    public String pluginDescription() {
        return "This plugin will pull all the reference ranges from a PHG database and will export the bed files used for CreateHaplotypes.sh";
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }


    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
//     public static void main(String[] args) {
//         GeneratePluginCode.generate(CreateIntervalBedFilesPlugin.class);
//     }

    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * File holding the DB config information
     *
     * @return Db Config File
     */
    public String dbConfigFile() {
        return dbConfigFile.value();
    }

    /**
     * Set Db Config File. File holding the DB config information
     *
     * @param value Db Config File
     *
     * @return this plugin
     */
    public CreateIntervalBedFilesPlugin dbConfigFile(String value) {
        dbConfigFile = new PluginParameter<>(dbConfigFile, value);
        return this;
    }

    /**
     * Name for the bed file
     *
     * @return Bed File
     */
    public String bedFile() {
        return myBedFile.value();
    }

    /**
     * Set Bed File. Name for the bed file
     *
     * @param value Bed File
     *
     * @return this plugin
     */
    public CreateIntervalBedFilesPlugin bedFile(String value) {
        myBedFile = new PluginParameter<>(myBedFile, value);
        return this;
    }

    /**
     * Name for the extended bed file
     *
     * @return Extended Bed File
     */
    public String extendedBedFile() {
        return myExtendedBedFile.value();
    }

    /**
     * Set Extended Bed File. Name for the extended bed file
     *
     * @param value Extended Bed File
     *
     * @return this plugin
     */
    public CreateIntervalBedFilesPlugin extendedBedFile(String value) {
        myExtendedBedFile = new PluginParameter<>(myExtendedBedFile, value);
        return this;
    }

    /**
     * Window size to add to the extendedBedFile
     *
     * @return Window Size
     */
    public Integer windowSize() {
        return windowSize.value();
    }

    /**
     * Set Window Size. Window size to add to the extendedBedFile
     *
     * @param value Window Size
     *
     * @return this plugin
     */
    public CreateIntervalBedFilesPlugin windowSize(Integer value) {
        windowSize = new PluginParameter<>(windowSize, value);
        return this;
    }

    /**
     * Comma separated list of reference Range methods needed
     * to create a bed file.  Generally you will want to pull
     * down refRegionGroup
     *
     * @return Ref Range Methods
     */
    public String refRangeMethod() {
        return myRefRangeMethod.value();
    }

    /**
     * Set Ref Range Methods. Comma separated list of reference
     * Range methods needed to create a bed file.  Generally
     * you will want to pull down refRegionGroup
     *
     * @param value Ref Range Methods
     *
     * @return this plugin
     */
    public CreateIntervalBedFilesPlugin refRangeMethod(String value) {
        myRefRangeMethod = new PluginParameter<>(myRefRangeMethod, value);
        return this;
    }

}
