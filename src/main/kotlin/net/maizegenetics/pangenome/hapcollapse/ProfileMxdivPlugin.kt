package net.maizegenetics.pangenome.hapcollapse

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.taxa.distance.DistanceMatrix
import net.maizegenetics.taxa.tree.Node
import net.maizegenetics.taxa.tree.Tree
import net.maizegenetics.taxa.tree.UPGMATree
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.PrintWriter
import java.lang.Integer.max
import javax.swing.ImageIcon


/**
 * Using a HaplotypeGraph supplied as input, this plugin evaluates how many consensus haplotypes
 * would be created for each ReferenceRange at different values of mxDiv.
 * It works by walking the UPGMA tree and reporting, for each internal node, the number of branches that exist at that height.
 * The number of branches corresponds to the number of haplotype nodes. The plugin can use either SNP distance or
 * KMER distance to construct the trees. SNP requires that the graph includes variants. KMER requires that the graph
 * includes sequence.
 */
class ProfileMxdivPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false ) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = LogManager.getLogger(ProfileMxdivPlugin::class.java)

    enum class DistanceType {
        SNP, KMER
    }

    private var distanceType = PluginParameter.Builder("distanceType", DistanceType.KMER, DistanceType::class.java)
        .description("The haplotype distance metric (KMER or SNP).")
        .build()

    private var outputFile = PluginParameter.Builder("outfile", null, String::class.java)
        .description("The name and path of the output file which will be written by this plugin.")
        .required(true)
        .outFile()
        .build()

    private var kmerSize = PluginParameter.Builder("kmerSize", 7, Int::class.java)
        .description("Kmer size")
        .build()

    private var numThreads = PluginParameter.Builder("numThreads", 3, Int::class.javaObjectType)
        .description("The number of threads to be used by this plugin. numThreads - 2 threads will be" +
                " assigned as worker threads for computing distance matrices and trees.")
        .build()

    override fun pluginDescription(): String {
        return "ProfileMxdivPlugin reports the number of haplotypes that would be created by the consensus pipeline " +
                "for different values of mxDiv in each reference range. A graph with the haplotypes to be profiled " +
                "must be supplied as input. The distance matrix for haplotypes can be calculated using either " +
                "KMER or SNP methods. If the KMER method is used, the input graph must include sequence. " +
                "If the SNP method is used the input graph must include variants."
    }

    override fun processData(input: DataSet): DataSet? {
        //for an input graph
        //  for each ReferenceRange
        //    calculate the pairwise distance (using either the kmer or snp distance method)
        //    create a upgma tree from the distance matrix
        //    walk the tree to determine number of branches at heights where the branch number changes
        //output: refrange_id, chr, start, mxDiv, number-of-haplotypes

        //get the input graph
        val dataList = input.getDataOfType(HaplotypeGraph::class.java)
        require(dataList.size == 1) {"Exactly one HaplotypeGraph must be input. Instead ${dataList.size} were input."}
        val graph = dataList.first().data as HaplotypeGraph

        val numberOfWorkers = max(numThreads() - 2, 1)
        val inputChannel = Channel<ProfileInput>(1000)
        val resultChannel = Channel<ProfileResult>(1000)

        runBlocking(Dispatchers.Default) {
            val printJob = launch { printResultsToFile(resultChannel) }
            val workerList = mutableListOf<Job>()
            repeat(numberOfWorkers) { workerList.add(
                launch { processRefRange(inputChannel, resultChannel) }
            ) }

            for (refrange in graph.referenceRanges()) {
                val nodeList = graph.nodes(refrange)
                if (nodeList.size > 1) inputChannel.send(ProfileInput(refrange, graph.nodes(refrange)))
                else myLogger.info("Reference Range has fewer than 2 nodes: $refrange")
            }

            inputChannel.close()
            for (worker in workerList) worker.join()

            //once the processing is finished close the print channel
            resultChannel.close()
        }

        return null
    }

    data class ProfileInput(val refRange: ReferenceRange, val nodeList: List<HaplotypeNode>)
    data class ProfileResult(val refRange: ReferenceRange, val pairList: List<Pair<Double, Int>>)

    private suspend fun processRefRange(input: ReceiveChannel<ProfileInput>, output: SendChannel<ProfileResult>) {
        for (myInput in input) {
            try{
                val dm = distanceMatrix(myInput.nodeList.sorted())
                val dmNoMissing = ConsensusProcessingUtils.setNsToMax(dm)
                val tree = UPGMATree(dmNoMissing)
                output.send(ProfileResult(myInput.refRange, getGroupNumbersAtHeights(tree)))

            } catch (e : Exception) {
                myLogger.info("Error finding group number in ${myInput.refRange}")
            }
        }
    }

    private suspend fun printResultsToFile(profileResults: ReceiveChannel<ProfileResult>) {
        PrintWriter(Utils.getBufferedWriter(outputFile())).use { myWriter ->
            myWriter.println("refrangeId\tchr\tstart\tmxDiv\tnumHaplotypes")

            for (result in profileResults) {
                val mxdivBranchNumber = result.pairList
                val refrange = result.refRange
                val chrname = refrange.chromosome().name
                for (pairValues in mxdivBranchNumber)
                    myWriter.println("${refrange.id()}\t${chrname}\t${refrange.start()}\t${pairValues.first}\t${pairValues.second}")
                myWriter.flush()
            }
        }

    }

    private fun distanceMatrix(nodeList: List<HaplotypeNode>): DistanceMatrix {
        when (distanceType()) {
            DistanceType.SNP -> {
                return snpDistanceMatrix(nodeList)
            }
            DistanceType.KMER -> {
                return kmerDistanceMatrix(nodeList.first().referenceRange(), nodeList, kmerSize(), DistanceCalculation.Euclidean)
            }
        }
    }

    private fun snpDistanceMatrix(nodeList: List<HaplotypeNode>): DistanceMatrix {
        val refrange = nodeList.first().referenceRange()
        val chr = refrange.chromosome()
        val taxaToVariantsMap = loadVariantsIntoRangeMap(nodeList)
        val listOfTaxa = TaxaListBuilder().addAll(taxaToVariantsMap.keys).build()
        return ConsensusProcessingUtils.createDistanceMatrix(listOfTaxa.numberOfTaxa(), chr, refrange, listOfTaxa, taxaToVariantsMap)
    }

    private fun getGroupNumbersAtHeights(tree: Tree): List<Pair<Double, Int>> {
        val numberAtHeightList = mutableListOf<Pair<Double, Int>>()
        val leafList = mutableListOf<Node>()
        val heightToNodeMap = mutableMapOf<Double,MutableList<Node>>()
        val root = tree.root

        //make sure root has children
       if (root.childCount == 0) return numberAtHeightList

        heightToNodeMap.put(root.nodeHeight, mutableListOf(root))

        //While nodes remain in heightToNodeMap, remove the highest node. Then, for each child node, if the child node
        //has children add it to the heightToNodeMap. If the child node has no children add it to the leafList. Once all
        //child nodes have been processed, total number of nodes = (number of nodes in heightToNodeMap) +
        //(number of nodes in leafList). Report the maxHeight (height of the node just removed) and the total node count
        //after processing the child nodes.
        while (heightToNodeMap.size > 0) {
            val maxHeight = heightToNodeMap.keys.maxOrNull()!!
            val nodesToSplit = heightToNodeMap.remove(maxHeight)!!
            for (parentNode in nodesToSplit) {
                if (parentNode.childCount == 0) leafList.add(parentNode)
                else for (childIndex in 0 until parentNode.childCount) {
                    val childNode = parentNode.getChild(childIndex)
                    val nodesAtHeight = heightToNodeMap[childNode.nodeHeight]
                    if (nodesAtHeight == null) {
                        heightToNodeMap.put(childNode.nodeHeight, mutableListOf(childNode))
                    } else nodesAtHeight.add(childNode)
                }
            }

            //at this point, the total number of groups equals the number of hodes in the heightToNodeMap
            // plus the number of leaves
            val totalNumberOfNodes = leafList.size + heightToNodeMap.entries.sumBy { it.value.size }
            numberAtHeightList.add(Pair(maxHeight, totalNumberOfNodes))

        }

        return numberAtHeightList
    }

    private fun loadVariantsIntoRangeMap(nodeList: List<HaplotypeNode>): Map<Taxon, RangeMap<Int,HaplotypeNode.VariantInfo>> {
        val infoMap = mutableMapOf<Taxon, RangeMap<Int,HaplotypeNode.VariantInfo>>()
        for (node in nodeList) {
            if (node.id() == -1) continue
            val optVarInfo = node.variantInfos()
            if (optVarInfo.isPresent) {
                val rangeMap = TreeRangeMap.create<Int, HaplotypeNode.VariantInfo>()
                val myInfoList = optVarInfo.get()
                check(myInfoList.size > 0) {"Haplotype ${node.id()} has an empty variantList"}
                //This section of codes tests for overlapping variants.
                //If variants overlap the second one is skipped, because including it would cause an error when calling
                //mapBuilder.build(). As the distance method only ends up counting SNPs and overlapping variants involve indels
                //there is no impact on the distance calculation.
                var previousEnd = 0
                for ( varInfo in myInfoList) {
                    if (varInfo.start() > previousEnd) {
                        rangeMap.put(Range.closed(varInfo.start(), varInfo.end()), varInfo)
                        previousEnd = varInfo.end()
                    }
                }
                for (taxon in node.taxaList()) infoMap[taxon] = rangeMap
            }

        }

        return infoMap
    }


    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "MxDiv Profiler"
    }

    override fun getToolTipText(): String {
        return "Profile MxDiv"
    }

    /**
     * The haplotype distance metric, which is either SNP
     * or KMER.
     *
     * @return Distance Type
     */
    fun distanceType(): DistanceType {
        return distanceType.value()
    }

    /**
     * Set Distance Type. The haplotype distance metric, which
     * is either SNP or KMER.
     *
     * @param value Distance Type
     *
     * @return this plugin
     */
    fun distanceType(value: DistanceType): ProfileMxdivPlugin {
        distanceType = PluginParameter(distanceType, value)
        return this
    }

    /**
     * The name and path of the output file which will be
     * written by this plugin.
     *
     * @return Outfile
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Outfile. The name and path of the output file which
     * will be written by this plugin.
     *
     * @param value Outfile
     *
     * @return this plugin
     */
    fun outputFile(value: String): ProfileMxdivPlugin {
        outputFile = PluginParameter(outputFile, value)
        return this
    }

    /**
     * Kmer size
     *
     * @return Kmer Size
     */
    fun kmerSize(): Int {
        return kmerSize.value()
    }

    /**
     * Set Kmer Size. Kmer size
     *
     * @param value Kmer Size
     *
     * @return this plugin
     */
    fun kmerSize(value: Int): ProfileMxdivPlugin {
        kmerSize = PluginParameter(kmerSize, value)
        return this
    }

    /**
     * The number of threads to be used by this plugin. numThreads
     * - 2 threads will be assigned as worker threads for
     * computing distance matrices and trees.
     *
     * @return Num Threads
     */
    fun numThreads(): Int {
        return numThreads.value()
    }

    /**
     * Set Num Threads. The number of threads to be used by
     * this plugin. numThreads - 2 threads will be assigned
     * as worker threads for computing distance matrices and
     * trees.
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value: Int): ProfileMxdivPlugin {
        numThreads = PluginParameter(numThreads, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(ProfileMxdivPlugin::class.java)
}