package net.maizegenetics.pangenome.db_loading

import htsjdk.samtools.SAMSequenceDictionary
import htsjdk.samtools.SAMSequenceRecord
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import htsjdk.variant.vcf.VCFHeader
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.pangenome.hapCalling.HapCallingUtils
import org.apache.logging.log4j.LogManager
import java.io.File

private val myLogger = LogManager.getLogger("net.maizegenetics.pangenome.db_loading.VariantLoadingUtils")

/**
 * Function to write out the Variant Contexts to a file.
 */
fun exportVariantContext(sampleName: String, variantContexts: List<VariantContext>, outputFileName: String, refGenomeSequence: GenomeSequence) {
    val writer = VariantContextWriterBuilder()
        .unsetOption(Options.INDEX_ON_THE_FLY)
        .setOutputFile(File(outputFileName))
        .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
        .setOption(Options.ALLOW_MISSING_FIELDS_IN_HEADER)
        .build()

    val header = HapCallingUtils.createGenericHeader(listOf(sampleName))
    addSequenceDictionary(header, refGenomeSequence)
    writer.writeHeader(header)
    for(variant in variantContexts) {
        writer.add(variant)
    }

    writer.close()
}

/**
 * function to bgzip and create tabix index on a gvcf file
 *
 * This method uses the -f option on both bgzip and tabix to overwrite any existing files
 * This handles the case where there already exists a bgzipped file, however
 * it is still required to have the non-bzipped file present.
 * @return the filename of the bgzipped gvcf
 */
fun bgzipAndIndexGVCFfile(gvcfFileName: String): String {

    try {
        // First bgzip the file

        // bgzip the file - needed to create index
        // use the -f option to overwrite any existing file
        myLogger.info("bgzipping  file ${gvcfFileName}")
        val gvcfGzippedFile = gvcfFileName + ".gz"
        var builder = ProcessBuilder(
            "bgzip", "-f", gvcfFileName)

        var process = builder.start()
        var error: Int = process.waitFor()
        if (error != 0) {
            myLogger.warn("\nERROR $error creating bgzipped  version of file: $gvcfFileName")
            throw IllegalStateException("bgzipAndIndexGVCFfile: error trying to bgzip file ${gvcfFileName}: ${error}")
        }

        // File has been gzipped, now index it.
        // Use the -f option to overwrite any existing index
        val tabixFile = gvcfGzippedFile + ".tbi"
        builder = ProcessBuilder("tabix", "-f", gvcfGzippedFile)
        process = builder.start()
        error = process.waitFor()
        if (error != 0) {
            myLogger.warn("\nERROR $error creating tabix indexed  version of file: $gvcfGzippedFile")
            throw IllegalStateException("bgzipAndIndexGVCFfile: error trying to run tabix on file ${gvcfGzippedFile}: ${error}")
        }
        return gvcfGzippedFile
    } catch (exc:Exception) {
        throw IllegalStateException("bgzipAndIndexGVCFfile: error bgzipping and/or tabix'ing file ${gvcfFileName}")
    }

}

/**
 * Function to add a sequence Dictionary based on the reference genome.  This uses the loaded genome to get the lengths.
 */
fun addSequenceDictionary(vcfheader : VCFHeader, refGenomeSequence: GenomeSequence) {

    val sequenceRecordList = refGenomeSequence.chromosomes().map { SAMSequenceRecord(it.name,
        refGenomeSequence.chromosomeSize(it)) }

    vcfheader.setSequenceDictionary(SAMSequenceDictionary(sequenceRecordList))
}