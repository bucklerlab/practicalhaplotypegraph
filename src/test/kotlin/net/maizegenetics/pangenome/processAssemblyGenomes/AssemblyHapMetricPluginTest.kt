package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.dna.snp.GenotypeTable
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.util.LoggingUtils
import org.apache.commons.io.FileUtils
import org.jetbrains.kotlinx.dataframe.DataFrame
import org.jetbrains.kotlinx.dataframe.api.dataFrameOf
import org.jetbrains.kotlinx.dataframe.api.head
import org.jetbrains.kotlinx.dataframe.api.print
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AssemblyHapMetricPluginTest {

    val userHome = System.getProperty("user.home")
    val outputDir = "$userHome/temp/AssemblyHapMetricPluginTests/"

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(File(outputDir))
            Files.createDirectories(Paths.get(outputDir))
        } catch (ioe: IOException) {
            throw IllegalStateException("AssemblyHapMetricPluginTest:setup: error deleting/creating folders: " + ioe.message)
        }
    }
    @Test
    fun testAssemblyHapMetricPlugin() {
        LoggingUtils.setupDebugLogging()

        println("Running AssemblyHapMetricPlugin")

        var graphMethods = "v5_geneBased,genic:anchorwave_NAM_assembly,genic"

        var graphOutFile = "${outputDir}assemblyHapMetricsGraph_dummyGraph_test.txt"

        var myGraph : HaplotypeGraph = buildSimpleDummyGraph()

        val dataset = AssemblyHapMetricPlugin(null, false)
            //.methods(graphMethods)
            .outputFile(graphOutFile)
            .performFunction(DataSet.getDataSet(myGraph))

        val df = dataset.getDataWithName("DataFrame")[0].data as DataFrame<*>

        val numCols = df.columnsCount()
        val colNames = df.columnNames()
        println("colNames: ${colNames}")
        assertEquals(9,numCols)
        assertTrue(df.columnNames().contains("refRangeID"))
        assertTrue(df.columnNames().contains("start"))
        assertTrue(df.columnNames().contains("sample1"))

        df.print()

    }
}