#!/bin/bash -x
# Get db connection, run AssemblyHaplotypesPlugin which creates and loads assembly sequence to the DB

#Input Folders
# Mount localMachine:/pathToBaseDockerDir to docker://phg/

# Mummer4 alignments are written to a subdirectory name "align" in the user mounted output directory
MOUNT_DIR=/phg
INPUT_DIR=${MOUNT_DIR}/inputDir
OUTPUT_DIR=${MOUNT_DIR}/outputDir
REFERENCE_DIR=${INPUT_DIR}/reference


# ALIGN_PATH needs the trailing "/"
ALIGN_PATH=${OUTPUT_DIR}/align/

GVCF_PATH=${ALIGN_PATH}/gvcfs/
mkdir -p $GVCF_PATH

#Reference directory, assembly directory, output directory must be mount points
CONFIGFILE=${MOUNT_DIR}/$1
KEYFILE=${MOUNT_DIR}/$2

# Set maximum Java heap size if defined in config file
memoryString="`grep -m 1 '^Xmx=' ${CONFIGFILE} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx10G'
fi

# Using the parameter cache for non-required parameters, keyFile for most required parameters
/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -configParameters ${CONFIGFILE} -AssemblyHaplotypesMultiThreadPlugin  -keyFile ${KEYFILE}  \
                                                        -outputDir $ALIGN_PATH -gvcfOutputDir ${GVCF_PATH} -endPlugin
