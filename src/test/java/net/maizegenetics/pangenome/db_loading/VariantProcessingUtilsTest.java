/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesPlugin;

import static org.junit.Assert.*;

/**
 * @author lcj34
 *
 */
public class VariantProcessingUtilsTest {

    @Test
    public void TestGetLongFromVMData() {
        
        int vmID = 345;
        Chromosome chrom = Chromosome.instance("5");
        int position = 457898;
        int refAlleleId = 12;
        int altAlleleId = 32;
        int refDepth = 6;
        int altDepth = 4;
        int refLen = 45;
        byte isIndel = 1;
        int other = 99;
        
        // Test creating a reference record
        VariantMappingData vmd = new VariantMappingData(Position.of(chrom,position), refAlleleId, altAlleleId, 
                refDepth, altDepth,true,refLen,isIndel,other); // "true" means is reference
        long myLong = VariantsProcessingUtils.getLongFromVMData( vmID,  vmd);
        System.out.println("Value of myLong with TRUE from getLongFromVMData " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        assertEquals(1,(myLong >>> 63));
        myLong  = myLong &  ~(1L << 63); // this wipes out upper 32
        System.out.println("refLong after resetting high bit set: " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        int myPosition = (int)(myLong & 0xFFFFFFFF);
        assertEquals(position,myPosition);
        myLong = myLong >> 32;
        System.out.println("\nrefLong after shift past position " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        int myDepth = (int)(myLong & 0xFF); 
        assertEquals(refDepth,myDepth);
        myLong = myLong >>> 8; 
        System.out.println("refLong after shift past depth " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        int myLength = (int)myLong; // it's been shifted enough
        assertEquals(refLen,myLength);
        
        // now test as a variant record
        vmd = new VariantMappingData(Position.of(chrom,position), refAlleleId, altAlleleId, 
                refDepth, altDepth,false,refLen,isIndel,other); // "false" means is variant
        myLong = VariantsProcessingUtils.getLongFromVMData( vmID,  vmd);
        System.out.println("Value of myLong with FALSE from getLongFromVMData " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        assertEquals(0,(myLong >>> 63));
        System.out.println("\nIS NOT a reference!!");
                
        int myOther = (int) (myLong & 0xFF);
        System.out.println("Value of myOther " + myOther);
        assertEquals(other,myOther);        
        myLong = myLong >> 8;
                
        byte indel = (byte) (myLong & 0xFF);
        System.out.println("Value of isIndel: " + indel);
        assertEquals(indel,isIndel);
        myLong = myLong >>8;
        
        int myAltDepth = (int) (myLong & 0xFF);
        System.out.println("Value of myAltDepth " + myAltDepth);
        assertEquals(altDepth,myAltDepth);
        
        myLong = myLong >> 8;
        int myRefDepth = (int) (myLong & 0xFF);
        assertEquals(refDepth,myRefDepth);
        
        myLong = myLong >> 8;
        int myVariantID = (int) myLong;
        assertEquals(vmID,myVariantID);

        System.out.println("Values expected:  quality=99, altdepth=4, refDepth=6, vmID=345");
        System.out.println("Values actual:    quality=" + myOther + ", altDepth=" + myAltDepth 
                + ", refDepth=" + myRefDepth + ", vmID=" + myVariantID);
        
    } 
    
    @Test
    public void testNegativeDepths() {
        //There were problems storing -1 for ref depth.  It overwrite part of the long,
        // wiping out the vmID when it was 1.  Verify this currently works, though we've
        // changed the defaults to 0 if no refDepth.
        int vmID = 1;
        Chromosome chrom = Chromosome.instance("5");
        int position = 457898;
        int refAlleleId = 12;
        int altAlleleId = 32;
        int refDepth = -1;  // -1 is stored as 255
        int storedRefDepth = 255;
        int altDepth = -32;
        int storedAltDepth = 224;
        int refLen = 45;
        byte isIndel = 0;
        int other = 99;
        
        VariantMappingData vmd = new VariantMappingData(Position.of(chrom,position), refAlleleId, altAlleleId, 
                refDepth, altDepth,false,refLen,isIndel,other); // "false" means is variant
        long myLong = VariantsProcessingUtils.getLongFromVMData( vmID,  vmd);
        System.out.println("Value of myLong from getLongFromVMData " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        assertEquals(0,(myLong >>> 63));
        System.out.println("\nIS NOT a reference!!");
                
        int myOther = (int) (myLong & 0xFF);
        System.out.println("Value of myOther " + myOther);
        assertEquals(other,myOther);        
        myLong = myLong >> 8;
                
        byte indel = (byte) (myLong & 0xFF);
        System.out.println("Value of isIndel: " + indel);
        assertEquals(indel,isIndel);
        myLong = myLong >>8;
        
        int myAltDepth = (int) (myLong & 0xFF);
        System.out.println("Value of myAltDepth " + myAltDepth);
        assertEquals(storedAltDepth,myAltDepth);
        byte altDepthByte = (byte) (myLong & 0xFF);
        System.out.println(" altDepthByte is " + altDepthByte + " should be -32");
        
        int altByteToInt = (int)altDepthByte;
        System.out.println(" altByteToInt is " + altByteToInt + ", expecting -32");
        assertEquals(altByteToInt, altDepth);
        
        myLong = myLong >> 8;
        int myRefDepth = (int)(myLong & 0xFF);
        assertEquals(storedRefDepth,myRefDepth);  // -1 is stored in the byte as 255.
        
        byte refDepthByte = (byte)(myLong & 0xFF);
        System.out.println("refDepthByte is " + refDepthByte + " should be -1");
        int refByteToInt = (int)refDepthByte;
        assertEquals(refByteToInt, refDepth);
        
        // Verify that storing -1 does not disturb the value of the vmId stored.
        myLong = myLong >> 8;
        int myVariantID = (int) myLong;
        assertEquals(vmID,myVariantID);

        System.out.println("Values expected:  other=99, ,isIndel=0; altdepth=0, refDepth=255, vmID=1");
        System.out.println("Values actual:    other=" + myOther + ",indel= " + indel + ", altDepth=" + myAltDepth 
                + ", refDepth=" + myRefDepth + ", vmID=" + myVariantID);
        
        
    }
    
    @Test
    public void testLargeDepthValues() {
        // Verify depths larger than MAX_BYTE are stored as MAX_BYTE
        int vmID = 1;
        Chromosome chrom = Chromosome.instance("5");
        int position = 457898;
        int refAlleleId = 12;
        int altAlleleId = 32;
        int refDepth = 300;  // will be stored as Byte.MAX_VALUE = 127
        int refDepthStored = 127;
        int altDepth = 0;
        int refLen = 45;
        byte isIndel = 0;
        int other = 99;
        
        VariantMappingData vmd = new VariantMappingData(Position.of(chrom,position), refAlleleId, altAlleleId, 
                refDepth, altDepth,false,refLen,isIndel,other); // "false" means is variant
        
        // Get data back out as a long
        long myLong = VariantsProcessingUtils.getLongFromVMData( vmID,  vmd);
        System.out.println("Value of myLong from getLongFromVMData " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        assertEquals(0,(myLong >>> 63));
        System.out.println("\nIS NOT a reference!!");
                
        int myOther = (int) (myLong & 0xFF);
        System.out.println("Value of myOther " + myOther);
        assertEquals(other,myOther);        
        myLong = myLong >> 8;
                
        byte indel = (byte) (myLong & 0xFF);
        System.out.println("Value of isIndel: " + indel);
        assertEquals(indel,isIndel);
        myLong = myLong >>8;
        
        int myAltDepth = (int) (myLong & 0xFF);
        System.out.println("Value of myAltDepth " + myAltDepth);
        assertEquals(altDepth,myAltDepth);
        
        myLong = myLong >> 8;
        int myRefDepth = (int)(myLong & 0xFF);
        System.out.println("Value of myRefDepth " + myRefDepth);
        assertEquals(refDepthStored,myRefDepth);  // 300 should be stored s 127
        
        // Verify that storing -1 does not disturb the value of the vmId stored.
        myLong = myLong >> 8;
        int myVariantID = (int) myLong;
        assertEquals(vmID,myVariantID);

        System.out.println("Values expected:  other=99, ,isIndel=0; altdepth=0, refDepth=255, vmID=1");
        System.out.println("Values actual:    other=" + myOther + ",indel= " + indel + ", altDepth=" + myAltDepth 
                + ", refDepth=" + myRefDepth + ", vmID=" + myVariantID);
        
        altDepth = 350;
        int altDepthStored = 127;
        
        vmd = new VariantMappingData(Position.of(chrom,position), refAlleleId, altAlleleId, 
                refDepth, altDepth,false,refLen,isIndel,other); // "false" means is variant
        
        // Get data back out as a long
         myLong = VariantsProcessingUtils.getLongFromVMData( vmID,  vmd);
        System.out.println("Value of myLong from getLongFromVMData " + myLong + ", as hex " + String.format("0x%08X", myLong));
        
        assertEquals(0,(myLong >>> 63));
        System.out.println("\nIS NOT a reference!!");
                
        myOther = (int) (myLong & 0xFF);
        System.out.println("Value of myOther " + myOther);
        assertEquals(other,myOther);        
        myLong = myLong >> 8;
                
        indel = (byte) (myLong & 0xFF);
        System.out.println("Value of isIndel: " + indel);
        assertEquals(indel,isIndel);
        myLong = myLong >>8;
        
        myAltDepth = (int) (myLong & 0xFF);
        System.out.println("Value of myAltDepth " + myAltDepth);
        assertEquals(altDepthStored,myAltDepth);
        
        myLong = myLong >> 8;
        myRefDepth = (int)(myLong & 0xFF);
        assertEquals(refDepthStored,myRefDepth);  // 300 should be stored s 127
        
        // Verify that storing -1 does not disturb the value of the vmId stored.
        myLong = myLong >> 8;
        myVariantID = (int) myLong;
        assertEquals(vmID,myVariantID);

        System.out.println("Values expected:  other=99, ,isIndel=0; altdepth=0, refDepth=255, vmID=1");
        System.out.println("Values actual:    other=" + myOther + ",indel= " + indel + ", altDepth=" + myAltDepth 
                + ", refDepth=" + myRefDepth + ", vmID=" + myVariantID);
        
    }

    
    @Test
    public void testLongToByteArray() {
        List<Long> longlist = Arrays.asList(10L, -5L, 15L, -20L, 25L, -30L, 123456789L, -123456789L);
        System.out.print("byte array conversion test: ");
        longlist.stream().map(val -> val.toString()).forEach(val -> System.out.print(val + "  "));
        
        byte[] byteArray = VariantsProcessingUtils.longListToByteArray(longlist);
        List<Long> convertedLongs = VariantsProcessingUtils.byteArrayToLongList(byteArray);

        assertTrue("longlist and convertedLongs have different lengths.", longlist.size() == convertedLongs.size());;
        for (int i = 0; i < convertedLongs.size(); i++) {
            assertTrue("longlist and convertedLongs are not equal at element " + i, longlist.get(i).longValue() == convertedLongs.get(i).longValue());
        }
        
    }
}

