#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/M162W/4882_6665_17309_HBEN2ADXX_M162W_GTCCGC_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/M162W/4882_6665_17309_HFFGKADXX_M162W_GTCCGC_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/M162W/M162W_HL5WNCCXX_L6_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_all10_MergedPlus1000orGapDiffcoordinates.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 48 \
   --min_base_qual 21 \
   M162W_haplotype_caller_output.g.vcf

