-- Table: genome_intervals 
-- This data pertains to the reference
-- The methodology (exact genes, genes_100 ,etc)
-- is determined by the anchor_haplotypes - select
-- a haplotype by line name and hapNumber and method_id
-- NOTE:  pre-anchors and anchors are all in the same table!
-- The anchor version ID detemines if this is a pre-anchor
-- or a final.  
-- If there was just 1 set of coordinates for an anchor there
-- would be no need for an genome_interval_versions table.  The anchor_haplotypes
-- table would point to all anchors for B73ref.  But when there
-- are different start/end coordiates for different anchors, these
-- need to be tracked.  
CREATE TABLE genome_intervals (
    genome_interval_is INTEGER PRIMARY KEY,
    version_id INTEGER,
    chrom	TEXT,   
    interval_start INTEGER NOT NULL,
    interval_end INTEGER NOT NULL,
    gene_start INTEGER NOT NULL,
    gene_end INTEGER NOT NULL,
    anchor_UUID TEXT,
    is_anchor BOOLEAN,
    FOREIGN KEY (version_id) REFERENCES genome_interval_versions(version_id),
    UNIQUE (version_id,chrom,interval_start,interval_end)
);

-- Table: anchor_sequences
-- seq_len and sequence_hash are used for finding exsiting sequences before adding
-- Each genome_interval_id can have multiple sequences associated.
-- Ref sequences are found from gametes and genotypes tables
-- seq_hash has been changed to an MD5 digest string
CREATE TABLE anchor_sequences (
    anchor_sequence_id INTEGER PRIMARY KEY,
    sequence TEXT,
    seq_len int,
    seq_hash TEXT,
    anchor_seq_UUID TEXT,
    UNIQUE (seq_hash,seq_len)
);

-- Table: consensus_anchor_sequences
-- This table holds collapsed anchor sequence for an anchor
-- It has a one-to-many relationship with anchor_sequences
-- via the consensus_anchor_sequences linker table
CREATE TABLE consensus_anchor_sequence (
   consensus_anchor_sequence_id INTEGER PRIMARY KEY,
   sequence TEXT
);

-- Table: anchorseq_consensus_anchorseq
-- Link between tables anchor_sequences and consensus_anchor_sequence
CREATE TABLE anchorseq_consensus_anchorseq (
   anchor_sequence_id INTEGER,
   consensus_anchor_sequence_id INTEGER,
   PRIMARY KEY(anchor_sequence_id,consensus_anchor_sequence_id),
   FOREIGN KEY (anchor_sequence_id) REFERENCES anchor_sequences(anchor_sequence_id),
   FOREIGN KEY (consensus_anchor_sequence_id) REFERENCES consensus_anchor_sequence(consensus_anchor_sequence_id)
);

-- Table: Edge
-- Holds edge information between anchor_haplotypes
-- fields "is_id1_anchor" and "is_id2_anchor" indicate if the 
-- specified node is an anchor (true) or an inter-anchor node.
CREATE TABLE edge (
   edge_id INTEGER PRIMARY KEY,
   anchor_haplotype_id1 INTEGER,
   anchor_haplotype_id2 INTEGER,
   is_id1_anchor BOOLEAN,
   is_id2_anchor BOOLEAN,
   method_id INTEGER,
   quality_score FLOAT,
   FOREIGN KEY (anchor_haplotype_id1) REFERENCES anchor_haployptes(anchor_haplotype_id),
   FOREIGN KEY (anchor_haplotype_id2) REFERENCES anchor_haplotypes(anchor_haplotype_id),
   FOREIGN KEY (method_id) REFERENCES methods(method_id)
);
-- Table: genotypes
-- Not everything that goes into this will 
-- be lines (essentially haploids)
-- The "line_data" field is created to allow
-- additional descriptions about the line.  THis is
-- especially useful if the line is a reference line.
-- Store in here AGPVx and other data.

CREATE TABLE genotypes (
    genoid INTEGER   PRIMARY KEY,
    ploidy INTEGER NOT NULL,
    is_reference BOOLEAN,
    line_name TEXT NOT NULL UNIQUE,
    line_data TEXT,
    isPhasedAcrossGenes BOOLEAN,
    isPhasedAcrossChromosomes BOOLEAN
);

-- Table: gametes
-- Only gametes are phased
-- hapNumber: generally 0 or 1.  Inbreds all 0, 
-- diploid heterozygous 0 (from mom) or 1 (from dad)
CREATE TABLE gametes (
    gameteid INTEGER PRIMARY KEY,
    genoid INTEGER NOT NULL,
    hapNumber INTEGER NOT NULL,
    phasingConfidence FLOAT,
    UNIQUE (genoid,hapNumber),
    FOREIGN KEY (genoid) REFERENCES genotypes(genoid)
);

-- Table: anchor_haplotypes
-- chrom field indicates on which assembly chrom the sequence for this haplotype mapped
-- when the ref anchors were BLASTEd against the assembly.
-- genome_interval_id is FK to anchors table
-- Zack data doesn't have the assembly start pos.  Lynn does from the BLAST runs.
-- Anchors from assemblies (data from Lynn' CreateBlastAssemblyAnchors()) will
-- not have the additional_data fields 
CREATE TABLE anchor_haplotypes (
    anchor_haplotypes_id INTEGER PRIMARY KEY,
    gameteid INTEGER,
    genome_interval_id INTEGER NOT NULL,
    anchor_sequence_id INTEGER,
    method_id INTEGER,
    assembly_chrom TEXT,
    assembly_start INTEGER,
    assembly_end INTEGER,
    additional_data TEXT,
    UNIQUE (gameteid, genome_interval_id,anchor_sequence_id, method_id),
    FOREIGN KEY (gameteid) REFERENCES gametes(gameteid),
    FOREIGN KEY (genome_interval_id) REFERENCES genome_intervals(genome_interval_id),
    FOREIGN KEY (anchor_sequence_id) REFERENCES anchor_sequences(anchor_sequence_id),
    FOREIGN KEY (method_id) REFERENCES methods(method_id)
);

-- This table holds a "blob" that is a TASSEL GenotypeTable
-- containing multi-sequence alignment on a per-anchor basis.
-- The Method_id tells which aligner, number of sequences and
-- other data relevant to the MSA.
CREATE TABLE anchor_alignments (
    genome_interval_id INTEGER PRIMARY KEY,
    method_id INTEGER,
    msa BLOB NOT NULL,
    FOREIGN KEY (genome_interval_id) REFERENCES genome_intervals(genome_interval_id),
    FOREIGN KEY (method_id) REFERENCES methods(method_id)
);
-- Method used to create the genome_intervals, the alignments and the edges
-- description should include program run, e.g. BLAST,
-- method of handling hets, when to drop things, etc
-- It is useful if the method text includes if it is for
-- anchors, alignment or edge
CREATE TABLE methods (
   method_id INTEGER PRIMARY KEY,
   name TEXT UNIQUE,
   description TEXT   
);

-- The anchor table has start/end coordinates relative to the reference genome
-- Only the reference has data in the anchor table.
-- But there are anchors and there are pre-anchors.  They need to be identifiable
-- This anchor_version table tells us that
CREATE TABLE genome_interval_versions (
   version_id INTEGER PRIMARY KEY,
   name TEXT,
   description TEXT
);
-- Create mock data for Unit_test
--Populate genotypes table
INSERT INTO genotypes (genoid,ploidy,is_reference,line_name,line_data,isPhasedAcrossGenes,isPhasedAcrossChromosomes) VALUES (1,1,'true','A','B73_fake','false','false');
INSERT INTO genotypes (genoid,ploidy,is_reference,line_name,line_data,isPhasedAcrossGenes,isPhasedAcrossChromosomes) VALUES (2,1,'false','B','Non_B73_fake','false','false');
INSERT INTO genotypes (genoid,ploidy,is_reference,line_name,line_data,isPhasedAcrossGenes,isPhasedAcrossChromosomes) VALUES (3,1,'false','C','Non_B73_fake','false','false');

--Populate haplotypes table
INSERT INTO gametes (gameteid,genoid,hapNumber,phasingConfidence) VALUES (1,1,0,1.0);
INSERT INTO gametes (gameteid,genoid,hapNumber,phasingConfidence) VALUES (2,2,0,1.0);
INSERT INTO gametes (gameteid,genoid,hapNumber,phasingConfidence) VALUES (3,3,0,1.0);

--Populate anchor_haplotypes table
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (1,1,1,1,1,1);
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (2,1,2,2,1,1);
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (3,2,1,3,1,1);
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (4,2,2,4,1,1);
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (5,3,1,5,1,1);
INSERT INTO anchor_haplotypes (anchor_haplotypes_id,gameteid,genome_interval_id,anchor_sequence_id,method_id,assembly_chrom) VALUES (6,3,2,2,1,1);

--Poplate the anchor_sequences table
INSERT INTO anchor_sequences (anchor_sequence_id, sequence, seq_len, seq_hash, anchor_seq_UUID)
VALUES (1, 'ACGT', 4, 'f1f8f4bf413b16ad135722aa4591043e', 'UUID_1');

INSERT INTO anchor_sequences (anchor_sequence_id, sequence, seq_len, seq_hash, anchor_seq_UUID)
VALUES (2, 'GCTA', 4, '7a75594ad17303d925bd7c02d9e2d020', 'UUID_2');

INSERT INTO anchor_sequences (anchor_sequence_id, sequence, seq_len, seq_hash, anchor_seq_UUID)
VALUES (3, 'ACGC', 4, 'edda3af3f92234c237fdc95a6e778273', 'UUID_3');

INSERT INTO anchor_sequences (anchor_sequence_id, sequence, seq_len, seq_hash, anchor_seq_UUID)
VALUES (4, 'GGCG', 4, 'd940c791b39e86d2f2cdf549aabaa33a', 'UUID_4');

INSERT INTO anchor_sequences (anchor_sequence_id, sequence, seq_len, seq_hash, anchor_seq_UUID)
VALUES (5, 'ACTA', 4, 'j1f8f4bf413b16ad135722aa4591043e', 'UUID_1');


--Populate the genome_intervals table
INSERT INTO genome_intervals (genome_interval_id, version_id, chrom, anchor_start, anchor_end, gene_start, gene_end, anchor_UUID, is_anchor)
VALUES (1, 1, '10', '100', '103', '101', '102', 'UUID_a_1', 1);

INSERT INTO genome_intervals (genome_interval_id, version_id, chrom, anchor_start, anchor_end, gene_start, gene_end, anchor_UUID, is_anchor)
VALUES (2, 1, '10', '110', '113', '111', '112', 'UUID_a_1', 1);

--populate anchorseq_consensus_anchorSeq
INSERT INTO anchorseq_consensus_anchorSeq (anchor_sequence_id, consensus_anchor_sequence_id)
VALUES (1,1);

INSERT INTO anchorseq_consensus_anchorSeq (anchor_sequence_id, consensus_anchor_sequence_id)
VALUES (2,2);

INSERT INTO anchorseq_consensus_anchorSeq (anchor_sequence_id, consensus_anchor_sequence_id)
VALUES (3,1);

INSERT INTO anchorseq_consensus_anchorSeq (anchor_sequence_id, consensus_anchor_sequence_id)
VALUES (4,3);

INSERT INTO anchorseq_consensus_anchorSeq (anchor_sequence_id, consensus_anchor_sequence_id)
VALUES (5,4);

--populate anchorseq_consensus_anchorSeq
INSERT INTO consensus_anchor_sequence (consensus_anchor_sequence_id, sequence)
VALUES (1,'ACGN');
INSERT INTO consensus_anchor_sequence (consensus_anchor_sequence_id, sequence)
VALUES (2,'GCTA');
INSERT INTO consensus_anchor_sequence (consensus_anchor_sequence_id, sequence)
VALUES (3,'GGCG');
INSERT INTO consensus_anchor_sequence (consensus_anchor_sequence_id, sequence)
VALUES (4,'ACTA');
