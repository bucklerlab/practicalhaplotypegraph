package net.maizegenetics.pangenome.hapCalling

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.buildSimpleDummyGraph
import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.AfterAll
import java.io.File

class CreateHapIdMapsPluginTest {

    val baseDir = System.getProperty("user.home")
    val outputDir = "${System.getProperty("user.home")}/phgTests"
    lateinit var dummyGraph: HaplotypeGraph
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
        dummyGraph = buildSimpleDummyGraph()
    }
    @AfterAll
    fun tearDown() {
        File(outputDir).deleteRecursively()
    }

    /**
     * Function to test the JSON encoding of a dummy graph object.
     * This makes sure to roundtrip the graph to file and then back to graph objects.
     */
    @Test
    fun testJSONEncodingDummyGraph() {
        //TODO is this the right spot to store the files?
        File(outputDir).mkdirs()
        val outputFile = "${outputDir}/test.json"
        val createPlugin = CreateHapIdMapsPlugin()

        val hapIdMap = getHapToRefRangeMap(dummyGraph)
        val hapIdToSequenceLength = getHapIdToSequenceLength(dummyGraph)
        val refRangeToHapidMap = getRefRangeToHapidMap(dummyGraph)

        createPlugin.createAndWriteHapIdMaps(hapIdMap, hapIdToSequenceLength, refRangeToHapidMap, 1 , mapOf(),outputFile)

        //load the file back in and compare the maps
        val mapper = jacksonObjectMapper()
        val maps = mapper.readValue(File(outputFile), GraphIdMaps::class.java)

        val fileHapIdToRefRangeMap = unwrapRefRanges(maps.hapIdToRefRangeMap)

        Assert.assertEquals(hapIdMap,fileHapIdToRefRangeMap)
        Assert.assertEquals(hapIdToSequenceLength, maps.hapIdToLengthMap)
        Assert.assertEquals(refRangeToHapidMap, maps.refRangeToHapIdMap)
        Assert.assertEquals(1, maps.haplotypeListId)


    }
}