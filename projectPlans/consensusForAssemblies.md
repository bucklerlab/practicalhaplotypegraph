
#Consensus for Assemblies 
| Plan Specifics  |  Details |
|:---|---|
|**Epic**  |  [PHG-333](https://tassel.atlassian.net/browse/PHG-333) |
|**Document status**   | DRAFT  |
|**Project Lead**   | Zack Miller (zrm22@cornell.edu)  |
|**Partners** | Peter Bradbury, Cinta Romay, Arcadio Valdes |
|**Software Lead** |  Zack Miller |

## Objective: Problem Statement

Implement a biologically relevant consensus finding pipeline for haplotypes created from the assemblies.

## Success metrics
|Goal| Metric  |
|---|---|
|  Greater Power to infer genome | When using Consensus to genotype, we should see a bump in power when comparing to the Axiom array.  Less noise in the haplotypes should result in better paths. |
| Minimal Change(Increase) in error rate | If consensus is created correctly, we may see a slight drop in SNP accuracy. However, by correcting low quality assemblies by merging them in with good ones, we may actually see a bump in accuracy.   |
|  Medium Memory Target | If we run this on TACC/Cyverse, needs to run on mm machine(<100GB RAM). Also Genotyping should be using less RAM as well. |
| Genotyping Speed Increases | Using Consensus haplotypes should be faster to minimap index, align and find paths.   |
| Consensus Speed | 1 day of runtime is acceptable.  Targeting ~1hr though.  Priority is accuracy and RAM over speed however. |

## Assumptions

Biology assumptions:

* A "curated" set of IBD regions can be identified by using the 600K SNP array data through overlapping taxa between accessions genotyped and the assemblies being used for the database of haplotypes.
* Defined reference ranges should majorly identify by alignment true regions of identity between assemblies.
* For clustering, between any pair of accessions, multiple individual short SNPs and INDELs are to be more highly weighted as informative than single long INDELs, as these might be due to assembly error/incompleteness.
* For clustering, if a same long INDEL appears in multiple accessions, then it is given due higher weight, understanding it to then represent part of a correct haplotype.

Software Assumptions:

* Needs to use graph api to pull down haplotypes for a given reference range and write to the DB the consensus haplotypes.
* Multithreaded- we can run each reference range independently and queue the objects for the DB write.

## Tests (Biology and Software)

|   Test Description|Data Input | Expected Result   |
|---|---|---|
| Axiom IBD  | IBD regions found in Axiom Array| Clusters Selected Should be in 99% agreement with IBD regions.  If we have this great. If at 80% or lower we need to reevaluate clustering approach and/or parameter tuning.  |
| Axiom SNP Error rate  | Genotyping error rate check against Axiom Array | Similar Results to original Genotyping(WGS <1% B73, 2-5% for Assembly taxon, <10% for rest).  We might see a slight bump in accuracy if Consensus fixes some of the poorer assemblies. Consider this test passed if error rates do not increase more than 1%.  |
| Axiom SNP Power Test | Genotyping Power check against Axiom Array | Test considered passed if Power is consistent or increasing in comparison to Genotyping without Consensus.  If power decreases - FAIL |
| Genotyping Speed Test| Genotype some WGS lines against both Non-consensus and Consensus graphs | Minimap2 Indexing of Pangenome should be faster(~30 minutes for non-consensus), my guess is <15minutes. Aligning reads might be a bit faster as the index will be smaller, but the limiting factor is the number of reads.  My guess it that the time will only decrease by a few minutes per pair of fastqs. |
| RAM Test building consensus for 20 Assemblies | PHG DB with 20 Assemblies.  Either old DB or New DB can be used | RAM usage needs to be below 100GB.  Targeting below 64GB so it can run on even smaller machines. | | 

## Milestones
1. Find IBD regions for tests
2. Finish Implementing prototype pipeline
3. Passes IBD tests correctly
4. Verify we can use Small Seq Test with the new pipeline.
5. Passes SNP Error rate test


## Requirements
*Note the JIRA tasks will be created and updated once the Pull Request goes through.*

| #  | Requirement  | Importance  | Jira Issue | Status |
|---:|---|---|---|---|
| 1 | Implement Initial Design  |  *high* | [PHG-325](https://tassel.atlassian.net/browse/PHG-325)  |  **DONE** |
| 2 | Create IBD region Test | *critical* | [PHG-334](https://tassel.atlassian.net/browse/PHG-334) | **DONE**|
| 3 | Run on IBD regions | *high* | [PHG-334](https://tassel.atlassian.net/browse/PHG-334) | **DONE** |
| 4 | Evaluate if Consensus is *good* enough for full Genotyping test | *high* | [PHG-330](https://tassel.atlassian.net/browse/PHG-330) | **IN PROGRESS** | 
| 5 | Evaluate different consensus haplotype selection schemes | *high* | [PHG-337](https://tassel.atlassian.net/browse/PHG-337) | **DONE** |
| 6 | Discuss alternative clustering approaches | *high* | PHG-XXX | **BACKLOG** |
| 7 | Implement ideas discussed in 4 and repeat 2 and 3. | *high* | PHG-XXX | **BACKLOG** |
| 8 | Update Consensus Documentation on wiki | *critical* | PHG-XXX | **BACKLOG** | 
|   |   |   |   |   |

## Requirements Details
1. Implement the initial prototype of the Consensus pipeline. Use SNPs to do initial clustering then use minimap2 results to further refine the clusters.  Currently using the "de" tag from SAM tools, but this can be anything.
2. Create IBD Region test using Axiom array.  Basically Set a window size and slide to find regions where the difference between sets of taxa is 0.  Terry's VCAPScanPlugin might be roughly the pipeline.  Try different window sizes.  These cluster sets will be our truth tests. Also need to determine negative tests.  Could potentially use merged windows with high divergence(>.5 or even >.75)
3. Run the initial pipeline on the IBD regions and verify that it works.  Use The IBD regions as a Truth set between pairs of taxon.  If the result clusters do not match, we need to reimplement better clustering or tune the parameters.  After each run, we need to start looking at what is wrong and why that is the case.
4. Discuss with Biologists to see if current Approach is working.  If we cannot easily see where the current approach fails, bring in outside knowledge.
5. Figure out different ways to select the consensus haplotype given a clustering.  Number of Ns.  Number of N Blocks. Check Candidate length to be close to the median.  Go Straight towards the ranking file.
6. Brainstorm additional approaches. Check what shortcomings initial pipeline has vs Biological tests.  Attempt to address them.
6. Implement new ideas and validate.
7. Go through and update Documentation and Docker scripts.


## User Interactions
* Should follow the general plugin architecture
* Update Docker scripts to use new plugin

## Open Questions

This table contains a list of questions that need answering as the project develops.  It could be blank.

|  Question | Answer  | Source/Date Answered  |
|---|---|---|
|   |   |   |


## Risks
* Quality of pangenome DB 
* Quality of Assemblies
* Memory vs Speed might be tough to balance depending on sequence length.
## Out of Scope
* None for now.