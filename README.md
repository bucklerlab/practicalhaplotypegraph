# Practical Haplotype Graph (PHG)


## Status
[![Lifecycle: superseded](https://img.shields.io/badge/lifecycle-superseded-blue.svg)](https://lifecycle.r-lib.org/articles/stages.html#superseded)

**PHGv1 is superseded**: only changes necessary for maintenance 
will be made. We recommend using [PHGv2](https://github.com/maize-genetics/phg_v2) 
instead.

## Documentation
For historical information about how PHGv1 operates, please refer
to our [wiki](https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home).
