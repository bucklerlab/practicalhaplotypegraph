package net.maizegenetics.pangenome.hapCalling

import htsjdk.variant.variantcontext.Allele
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.VariantContextBuilder
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.HaplotypeSequence
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.LoggingUtils
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
//import org.junit.After
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
import java.io.File
import java.util.*
import kotlin.test.expect
import kotlin.test.fail

class IndexHaplotypesBySNPPluginTest {


    companion object {
        var directory = System.getProperty("user.home") + "/temp/IndexHaplotypesBySNPPluginTest/"

        @JvmStatic
        @BeforeAll
        fun setup() {
            LoggingUtils.setupDebugLogging()

            File(directory).mkdirs()
        }

        @JvmStatic
        @AfterAll
        fun tearDown() {
            File(directory).deleteRecursively()
        }
    }



    @Test
    fun testGetSNPsFromVCF(){
        val vcfFile = "$directory/test.vcf"
        createTestVCF(vcfFile)

        val plugin = IndexHaplotypesBySNPPlugin()

        val snpsInVCF = plugin.getSNPsFromVCF(vcfFile)

        //: Map<Position,Pair<String,String>>
        val expectedSNPs = mapOf(
            Position.of(1, 10) to Pair("A", "C"),
            Position.of(1, 20) to Pair("A", "C"),
            Position.of(1, 30) to Pair("A", "C"),
            Position.of(1, 40) to Pair("A", "C"),
            Position.of(1, 50) to Pair("A", "C"),
            Position.of(1, 60) to Pair("A", "C"),
            Position.of(1, 70) to Pair("A", "C"),
            Position.of(1, 80) to Pair("A", "C"),
            Position.of(1, 90) to Pair("A", "C"),
            Position.of(1, 100) to Pair("A", "C")
        )

        assertEquals(expectedSNPs,snpsInVCF, "IndexHaplotypesBySNPPlugin.getSNPsFromVCF() are not the same")
    }

    fun createTestVCF(vcfFile: String) {
        val taxa = listOf("taxon1", "taxon2", "taxon3", "taxon4", "taxon5")
        val vcfOutputWriterBuilder = VariantContextWriterBuilder().unsetOption(Options.INDEX_ON_THE_FLY)

        vcfOutputWriterBuilder.setOutputFile(vcfFile)
            .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
            .setOption(Options.ALLOW_MISSING_FIELDS_IN_HEADER)
            .build()
            .use { writer ->

                writer.writeHeader(HapCallingUtils.createGenericHeader(taxa))

                var numRangesWritten = 0

                (1 .. 10).map { i ->
                    val genotypes = taxa.mapIndexed {index,  taxon ->
                        val allele = if(i + index == 10) Allele.NO_CALL else if (i % (index+1) == 0) Allele.REF_A else Allele.ALT_C
                        GenotypeBuilder.create(taxon, listOf(allele))
                    }
                    val variantContext = VariantContextBuilder()
                            .chr("chr1")
                            .start((i * 10).toLong())
                            .stop((i * 10).toLong())
                            .alleles(listOf(Allele.REF_A, Allele.ALT_C))
                            .genotypes(genotypes)
                            .make()
                    variantContext
                }
                .forEach { variantContext ->
                    writer.add(variantContext)
                    numRangesWritten++
                }

            }
    }
}