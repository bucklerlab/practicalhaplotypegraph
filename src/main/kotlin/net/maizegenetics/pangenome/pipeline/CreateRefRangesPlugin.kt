package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.plugindef.*
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import javax.swing.ImageIcon

class CreateRefRangesPlugin(parentFrame: Frame?=null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(CreateRefRangesPlugin::class.java)

    private var wiggleDir = PluginParameter.Builder("wiggleDir", null, String::class.java)
        .guiName("Wiggle directory")
        .inDir()
        .required(true)
        .description("Name of the directory containing the wiggle files for determining coverage.")
        .build()

    private var secondaryWiggleDir = PluginParameter.Builder("secondaryWiggleDir", null, String::class.java)
        .guiName("Secondary wiggle directory")
        .inDir()
        .required(false)
        .description("Name of the directory containing the wiggle files for determining coverage using " +
                "secondaryMinCover (Secondary Minimum Coverage Count). This set of wiggle files will be used " +
                "to further decide whether or not a bp is conserved. It is optional, but we have used it to add " +
                "in some cross-species coverage information to further restrict the sites we consider as identical.")
        .build()

    private var gffFile = PluginParameter.Builder("gffFile", null, String::class.java)
        .guiName("GFF file")
        .inFile()
        .required(true)
        .description("GFF file holding the genic and intergenic regions.")
        .build()

    private var gffFeatureType = PluginParameter.Builder("gffFeatureType", GFFType.CDS, GFFType::class.javaObjectType)
        .guiName("GFF Feature Type")
        .required(false)
        .description("The feature type to use from the GFF file.  This is defaults to CDS, but can also be gene.")
        .build()

    private var minCoverage = PluginParameter.Builder("minCover", 25, Int::class.javaObjectType)
        .guiName("Minimum Coverage Count")
        .required(false)
        .description("The minimum allowed Coverage count for determining if a bp is conserved or not.")
        .build()

    private var secondaryMinCoverage = PluginParameter.Builder("secondaryMinCover", -1, Int::class.javaObjectType)
        .guiName("Secondary Minimum Coverage Count")
        .required(false)
        .description("The minimum allowed Coverage count for determining if a bp is conserved or not for the secondary wiggle Files.")
        .build()

    private var windowSize = PluginParameter.Builder("windowSize", 10, Int::class.javaObjectType)
        .guiName("Window Size")
        .required(false)
        .description("The size of the number of bps which must flank the current site in both directions.  A window of 2 * windowSize +1 must be conserved.")
        .build()

    private var intergenicStepSize = PluginParameter.Builder("intergenicStepSize", 50000, Int::class.javaObjectType)
        .guiName("Intergenic Step Size")
        .required(false)
        .description("The number of bps that the intergenic split algorithm will use to step between cut sites")
        .build()

    private var maxSearchWindow = PluginParameter.Builder("maxSearchWindow", 10000, Int::class.javaObjectType)
        .guiName("Maximum Search Window")
        .required(false)
        .description("The number of bps when checking coverage to walk out.")
        .build()


    private var outputBedFile = PluginParameter.Builder("outputBedFile", null, String::class.java)
        .guiName("Output Bed File")
        .required(true)
        .outFile()
        .description("Output Bed file")
        .build()

    private var referenceGenome = PluginParameter.Builder("refGenome", null, String::class.java)
        .required(true)
        .inFile()
        .description("The reference genome fasta.")
        .build()

    private var vcfDirectory = PluginParameter.Builder("vcfdir", null, String::class.java)
        .required(true)
        .inDir()
        .description("The directory containing all the gvcf files to be used for haplotype clustering for splitting genic reference ranges. " +
                "All files ending in vcf.gz will be used. Files are expected to be bgzipped and indexed.")
        .build()

    private var genicRangesFile = PluginParameter.Builder("outputGeneRanges", null, String::class.java)
        .required(false)
        .outFile()
        .description("Optional output file name for the gene ranges created by finding conserved breakpoints " +
                "near genes from the gff file. If no name is supplied the genic range file will not be written.")
        .build()

    private var useSecondaryForIntergenic = PluginParameter.Builder("useSecondaryForIntergenic",false, Boolean::class.javaObjectType)
        .required(false)
        .description("Flag for using the secondary Coverage set when finding intergenic breakpoints")
        .build()

    private var maxDiversity = PluginParameter.Builder("mxDiv", 0.0001, Double::class.javaObjectType)
        .description("This parameter determines the maximum diversity allowed in the clustering step used to determine " +
                "haplotype number. Haplotype number is used with the maxClusters parameter to decide when to split genic ranges.")
        .build()

    private var minGenicLength = PluginParameter.Builder("minLength", 1000, Int::class.javaObjectType)
        .description("When attempting to subdivide genic ranges, the smallest allowable subdivision length is minLength base pairs.")
        .build()

    private var maxClusters = PluginParameter.Builder("maxClusters", 10, Int::class.javaObjectType)
        .description("When attempting to subdivide genic ranges, only genic ranges with more than maxClusters " +
                "haplotypes after clustering  will be subdivided.")
        .build()

    private var numberOfThreads = PluginParameter.Builder("nThreads", 10, Int::class.javaObjectType)
        .description("The number of threads that will be used for subdividing genic ranges.")
        .build()

    override fun processData(input: DataSet?): DataSet? {

        val idMap = CreateRefRangeUtils.loadInWiggleFiles(coverageDir = wiggleDir())
        myLogger.info("Loaded first batch of wiggle files.")

        val secondaryIdentityMap = if(secondaryWiggleDir() == null) mutableMapOf<String,List<Int>>()
                                    else CreateRefRangeUtils.loadInWiggleFiles(secondaryWiggleDir()!!)

        for(key in idMap.keys) {
            myLogger.info("${key} ${idMap[key]?.size} ${secondaryIdentityMap[key]?.size} ")
        }

        val genicRegions = CreateRefRangeUtils.createGenicRegions(idMap, gffFile(), minCoverage(), windowSize(),secondaryIdentityMap,secondaryMinCoverage(),maxSearchWindow(), geneRangesOutput = genicRangesFile(), gffIdentifier = gffFeatureType())
        myLogger.info("Created Genic Regions.  Size: ${genicRegions.size}")

        //fill in the gaps
        val bedRanges =  if(useSecondaryForIntergenic()) {
            CreateRefRangeUtils.createIntergenicRegions(genicRegions, intergenicStepSize(),idMap, minCoverage(), windowSize(), secondaryIdentityMap, secondaryMinCoverage(),maxSearchWindow())
        }
        else {
            CreateRefRangeUtils.createIntergenicRegions(genicRegions, intergenicStepSize(),idMap, minCoverage(), windowSize(), mapOf<String,List<Int>>(), -1, maxSearchWindow())
        }
        myLogger.info("Created intergenic regions")

        val bedRangesWithFirstAndLast =  CreateRefRangeUtils.createFirstAndLastBedIntergenic(bedRanges, idMap)
        myLogger.info("Created first and last range")

        //split the genic reference ranges based on haplotype number
        val refGenome = GenomeSequenceBuilder.instance(referenceGenome())
        myLogger.info("Loaded reference genome")

        //Parameters
        val windowSize = 5

        //this creates a file name to be used to record split result with haplotype counts for each range
        val fileNameForSplitting = outputBedFile().substringBeforeLast(".bed") + "_splitWithCounts.txt"
        val bedRangesWithSplitGenes = CreateRefRangeUtils.splitGenicReferenceRanges(bedRangesWithFirstAndLast, idMap,
            vcfDirectory(), refGenome, minCoverage(), windowSize, numberOfThreads(),
            fileNameForSplitting, maxDiversity(), minGenicLength(), maxClusters())
        myLogger.info("Finished splitting genes")

        CreateRefRangeUtils.genicRangesToBedFile(outputBedFile(), bedRangesWithSplitGenes)
        myLogger.info("Wrote ${outputBedFile()}")

        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = CreateRefRangesPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "CreateRefRangesPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Create Reference Ranges based on coverage files"
    }

    /**
     * Name of the directory containing the wiggle files for
     * determining coverage.
     *
     * @return Wiggle directory
     */
    fun wiggleDir(): String {
        return wiggleDir.value()
    }

    /**
     * Set Wiggle directory. Name of the directory containing
     * the wiggle files for determining coverage.
     *
     * @param value Wiggle directory
     *
     * @return this plugin
     */
    fun wiggleDir(value: String): CreateRefRangesPlugin {
        wiggleDir = PluginParameter(wiggleDir, value)
        return this
    }

    /**
     * Name of the directory containing the wiggle files for
     * determining coverage using the secondary rule.
     *
     * @return Secondary wiggle directory
     */
    fun secondaryWiggleDir(): String? {
        return secondaryWiggleDir.value()
    }

    /**
     * Set Secondary wiggle directory. Name of the directory
     * containing the wiggle files for determining coverage
     * using the secondary rule.
     *
     * @param value Secondary wiggle directory
     *
     * @return this plugin
     */
    fun secondaryWiggleDir(value: String): CreateRefRangesPlugin {
        secondaryWiggleDir = PluginParameter(secondaryWiggleDir, value)
        return this
    }

    /**
     * GFF file holding the genic and intergenic regions.
     *
     * @return GFF file
     */
    fun gffFile(): String {
        return gffFile.value()
    }

    /**
     * Set GFF file. GFF file holding the genic and intergenic
     * regions.
     *
     * @param value GFF file
     *
     * @return this plugin
     */
    fun gffFile(value: String): CreateRefRangesPlugin {
        gffFile = PluginParameter(gffFile, value)
        return this
    }

    /**
     * The feature type to use from the GFF file.  This is
     * defaults to CDS, but can also be gene.
     *
     * @return GFF Feature Type
     */
    fun gffFeatureType(): GFFType {
        return gffFeatureType.value()
    }

    /**
     * Set GFF Feature Type. The feature type to use from
     * the GFF file.  This is defaults to CDS, but can also
     * be gene.
     *
     * @param value GFF Feature Type
     *
     * @return this plugin
     */
    fun gffFeatureType(value: GFFType): CreateRefRangesPlugin {
        gffFeatureType = PluginParameter<GFFType>(gffFeatureType, value)
        return this
    }

    /**
     * The minimum allowed Coverage count for determining
     * if a bp is conserved or not.
     *
     * @return Minimum Coverage Count
     */
    fun minCoverage(): Int {
        return minCoverage.value()
    }

    /**
     * Set Minimum Coverage Count. The minimum allowed Coverage
     * count for determining if a bp is conserved or not.
     *
     * @param value Minimum Coverage Count
     *
     * @return this plugin
     */
    fun minCoverage(value: Int): CreateRefRangesPlugin {
        minCoverage = PluginParameter(minCoverage, value)
        return this
    }

    /**
     * The minimum allowed Coverage count for determining
     * if a bp is conserved or not for the secondary wiggle
     * Files.
     *
     * @return Secondary Minimum Coverage Count
     */
    fun secondaryMinCoverage(): Int {
        return secondaryMinCoverage.value()
    }

    /**
     * Set Secondary Minimum Coverage Count. The minimum allowed
     * Coverage count for determining if a bp is conserved
     * or not for the secondary wiggle Files.
     *
     * @param value Secondary Minimum Coverage Count
     *
     * @return this plugin
     */
    fun secondaryMinCoverage(value: Int): CreateRefRangesPlugin {
        secondaryMinCoverage = PluginParameter(secondaryMinCoverage, value)
        return this
    }

    /**
     * The size of the number of bps which must flank the
     * current site in both directions.  A window of 2 * windowSize
     * +1 must be conserved.
     *
     * @return Window Size
     */
    fun windowSize(): Int {
        return windowSize.value()
    }

    /**
     * Set Window Size. The size of the number of bps which
     * must flank the current site in both directions.  A
     * window of 2 * windowSize +1 must be conserved.
     *
     * @param value Window Size
     *
     * @return this plugin
     */
    fun windowSize(value: Int): CreateRefRangesPlugin {
        windowSize = PluginParameter(windowSize, value)
        return this
    }

    /**
     * The number of bps that the intergenic split algorithm
     * will use to step between cut sites
     *
     * @return Intergenic Step Size
     */
    fun intergenicStepSize(): Int {
        return intergenicStepSize.value()
    }

    /**
     * Set Intergenic Step Size. The number of bps that the
     * intergenic split algorithm will use to step between
     * cut sites
     *
     * @param value Intergenic Step Size
     *
     * @return this plugin
     */
    fun intergenicStepSize(value: Int): CreateRefRangesPlugin {
        intergenicStepSize = PluginParameter(intergenicStepSize, value)
        return this
    }

    /**
     * The number of bps when checking coverage to walk out.
     *
     * @return Maximum Search Window
     */
    fun maxSearchWindow(): Int {
        return maxSearchWindow.value()
    }

    /**
     * Set Maximum Search Window. The number of bps when checking
     * coverage to walk out.
     *
     * @param value Maximum Search Window
     *
     * @return this plugin
     */
    fun maxSearchWindow(value: Int): CreateRefRangesPlugin {
        maxSearchWindow = PluginParameter(maxSearchWindow, value)
        return this
    }

    /**
     * Output Bed file
     *
     * @return Output Bed File
     */
    fun outputBedFile(): String {
        return outputBedFile.value()
    }

    /**
     * Set Output Bed File. Output Bed file
     *
     * @param value Output Bed File
     *
     * @return this plugin
     */
    fun outputBedFile(value: String): CreateRefRangesPlugin {
        outputBedFile = PluginParameter(outputBedFile, value)
        return this
    }

    /**
     * The reference genome fasta.
     *
     * @return Ref Genome
     */
    fun referenceGenome(): String {
        return referenceGenome.value()
    }

    /**
     * Set Ref Genome. The reference genome fasta.
     *
     * @param value Ref Genome
     *
     * @return this plugin
     */
    fun referenceGenome(value: String): CreateRefRangesPlugin {
        referenceGenome = PluginParameter(referenceGenome, value)
        return this
    }

    /**
     * The directory containing all the gvcf files to be used
     * for haplotype clustering for splitting genic reference
     * ranges. All files ending in vcf.gz will be used. Files
     * are expected to be bgzipped and indexed.
     *
     * @return Vcfdir
     */
    fun vcfDirectory(): String {
        return vcfDirectory.value()
    }

    /**
     * Set Vcfdir. The directory containing all the gvcf files
     * to be used for haplotype clustering for splitting genic
     * reference ranges. All files ending in vcf.gz will be
     * used. Files are expected to be bgzipped and indexed.
     *
     * @param value Vcfdir
     *
     * @return this plugin
     */
    fun vcfDirectory(value: String): CreateRefRangesPlugin {
        vcfDirectory = PluginParameter(vcfDirectory, value)
        return this
    }

    /**
     * Optional output file name for the gene ranges created
     * by finding conserved breakpoints near genes from the
     * gff file. If no name is supplied the genic range file
     * will not be written.
     *
     * @return Ouput Gene Ranges
     */
    fun genicRangesFile(): String? {
        return genicRangesFile.value()
    }

    /**
     * Set Ouput Gene Ranges. Optional output file name for
     * the gene ranges created by finding conserved breakpoints
     * near genes from the gff file. If no name is supplied
     * the genic range file will not be written.
     *
     * @param value Ouput Gene Ranges
     *
     * @return this plugin
     */
    fun genicRangesFile(value: String): CreateRefRangesPlugin {
        genicRangesFile = PluginParameter(genicRangesFile, value)
        return this
    }

    /**
     * Flag for using the secondary Coverage set when finding
     * intergenic breakpoints
     *
     * @return Use Secondary For Intergenic
     */
    fun useSecondaryForIntergenic(): Boolean {
        return useSecondaryForIntergenic.value()
    }

    /**
     * Set Use Secondary For Intergenic. Flag for using the
     * secondary Coverage set when finding intergenic breakpoints
     *
     * @param value Use Secondary For Intergenic
     *
     * @return this plugin
     */
    fun useSecondaryForIntergenic(value: Boolean): CreateRefRangesPlugin {
        useSecondaryForIntergenic = PluginParameter<Boolean>(useSecondaryForIntergenic, value)
        return this
    }

    /**
     * This parameter determines the maximum diversity allowed
     * in the clustering step used to determine haplotype
     * number. Haplotype number is used to decide when to
     * split genic ranges.
     *
     * @return Mx Div
     */
    fun maxDiversity(): Double {
        return maxDiversity.value()
    }

    /**
     * Set Mx Div. This parameter determines the maximum diversity
     * allowed in the clustering step used to determine haplotype
     * number. Haplotype number is used to decide when to
     * split genic ranges.
     *
     * @param value Mx Div
     *
     * @return this plugin
     */
    fun maxDiversity(value: Double): CreateRefRangesPlugin {
        maxDiversity = PluginParameter(maxDiversity, value)
        return this
    }

    /**
     * When attempting to subdivide genic ranges, the smallest
     * allowable subdivision length is minLength base pairs.
     *
     * @return Min Length
     */
    fun minGenicLength(): Int {
        return minGenicLength.value()
    }

    /**
     * Set Min Length. When attempting to subdivide genic
     * ranges, the smallest allowable subdivision length is
     * minLength base pairs.
     *
     * @param value Min Length
     *
     * @return this plugin
     */
    fun minGenicLength(value: Int): CreateRefRangesPlugin {
        minGenicLength = PluginParameter(minGenicLength, value)
        return this
    }

    /**
     * When attempting to subdivide genic ranges, only genic
     * ranges with more than maxClusters haplotypes after
     * clustering  will be subdivided.
     *
     * @return Max Clusters
     */
    fun maxClusters(): Int {
        return maxClusters.value()
    }

    /**
     * Set Max Clusters. When attempting to subdivide genic
     * ranges, only genic ranges with more than maxClusters
     * haplotypes after clustering  will be subdivided.
     *
     * @param value Max Clusters
     *
     * @return this plugin
     */
    fun maxClusters(value: Int): CreateRefRangesPlugin {
        maxClusters = PluginParameter(maxClusters, value)
        return this
    }

    /**
     * The number of threads that will be used for subdividing
     * genic ranges.
     *
     * @return N Threads
     */
    fun numberOfThreads(): Int {
        return numberOfThreads.value()
    }

    /**
     * Set N Threads. The number of threads that will be used
     * for subdividing genic ranges.
     *
     * @param value N Threads
     *
     * @return this plugin
     */
    fun numberOfThreads(value: Int): CreateRefRangesPlugin {
        numberOfThreads = PluginParameter(numberOfThreads, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(CreateRefRangesPlugin::class.java)
}