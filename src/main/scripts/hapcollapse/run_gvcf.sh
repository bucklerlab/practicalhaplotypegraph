#!/bin/bash -x
# Example script to run gvcfTyper.  File paths and extensions need to be altered to run again.
# Will also index the gvcfs exported by the filtering process.

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201704.01
SENTIEON='/programs/sentieon-genomics-201704.01/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/zrm22/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'


GVCFFileDir='/local/workdir/zrm22/GVCFTyper260Taxa/GVCFs'

cd $GVCFFileDir
gvcfFiles="`ls *.g.vcf`"
cd ..


for gvcf in $gvcfFiles
do
   echo 'Indexing'${gvcf}
   $SENTIEON util vcfindex ${GVCFFileDir}/${gvcf}
done


#   --interval /local/workdir/zrm22/hackathon_trimmed_proteome.intervals \
#   --interval /local/workdir/zrm22/SingleAnchor.intervals \

time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   --interval /local/workdir/zrm22/hackathon_trimmed.intervals \
   --algo GVCFtyper \
   --emit_mode confident \
   /local/workdir/zrm22/GVCFTyper260Taxa/gvcfTyperOutputFiltered_EmitModeConfAllAnchors.vcf \
   ${GVCFFileDir}/*.g.vcf