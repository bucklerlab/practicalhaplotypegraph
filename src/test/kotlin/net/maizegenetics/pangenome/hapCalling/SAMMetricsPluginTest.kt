package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.File

class SAMMetricsPluginTest {
    private val editFile = "out.txt"

    private val expectedAlignmentSummary = SAMMetricsPlugin.AlignmentSummary(
        32, 6,
        listOf(1, 3, 1, 0, 10, 1, 0, 0, 10, 0, 4, 15, 2, 2, 2, 1, 2, 0, 3, 3, 11, 17, 11, 0, 8, 7, 1, 0, 1, 13, 0, 3)
    )

    /**
     * Test that plugin accurately counts mapped/unmapped reads and lists minimum edit distances per read
     */
    @Test
    fun testGetSAMStats() {
        val actual = SAMMetricsPlugin().getSAMStats(File("$testingDir/$samFile"))

        assertEquals(expectedAlignmentSummary.unmappedReads, actual.unmappedReads, "wrong number of unmapped reads")
        assertEquals(expectedAlignmentSummary.mappedReads, actual.mappedReads, "wrong number of mapped reads")
        assertEquals(expectedAlignmentSummary.minEditDistances, actual.minEditDistances, "wrong minimum edit distances per mapped read")
    }

    /**
     * Test that plugin can take blank file as input without throwing an error
     */
    @Test
    fun testGetSAMStatsBlank() {
        val actual = SAMMetricsPlugin().getSAMStats(File("$testingDir/$blankFile"))

        assertEquals(0, actual.unmappedReads, "did not expect any unmapped reads")
        assertEquals(0, actual.mappedReads, "did not expect any mapped reads")
        assertEquals(listOf<Int>(), actual.minEditDistances, "did not expect any edit distances")
    }

    /**
     * Test that alignment summary correctly calculates mean and median
     */
    @Test
    fun testAlignmentSummaryToString() {
        val actual = expectedAlignmentSummary.toString()
        val expected = "32\t6\t4.125\t2.0"

        assertEquals(expected, actual, "wrong mean or median")
    }

    /**
     * Test that alignment summary can have empty list without throwing error
     */
    @Test
    fun testAlignmentSummaryToStringNull() {
        val actual = SAMMetricsPlugin.AlignmentSummary(0, 0, listOf<Int>(0)).toString()
        val expected = "0\t0\t0.0\t0.0"

        assertEquals(expected, actual, "empty alignment summary list not handled correctly")
    }

    /**
     * Test that edit distances are written to file correctly and can be retrieved
     */
    @Test
    fun testWriteEditDistancesToFile() {
        SAMMetricsPlugin().writeEditDistancesToFile(expectedAlignmentSummary.minEditDistances, "$testingDir/$editFile")

        val actual = File("$testingDir/$editFile").readLines()
        val expected = expectedAlignmentSummary.minEditDistances.map { it.toString() }

        assertEquals(expected, actual, "write to file not working")
    }

    companion object {

        private val testingDir = System.getProperty("user.home") + "/temp/SAMMetricsTests/"
        private val samFile = "sample.sam"
        private val blankFile = "blank.sam"

        /**
         * Before running tests, make a dummy SAM file and a blank file in a dedicated directory
         */
        @BeforeAll
        @JvmStatic
        fun setup() {
            LoggingUtils.setupDebugLogging()

            File(testingDir).deleteRecursively()
            File(testingDir).mkdirs()

            createDummySAM("$testingDir/$samFile")
            createBlankSAM("$testingDir/$blankFile")
        }

        /**
         * Helper function writes dummy SAM file. Values are nonsense, but are the correct data type for each column.
         */
        private fun createDummySAM(outputFile: String) {
            Utils.getBufferedWriter(outputFile).use { output ->
                output.write(
                    "read_0\t1061\trefName\t70884\t41\t4=1X22=1X41=\t*\t0\t785\t*\t*\tNM:i:13\tms:i:48\tAS:i:50\tnn:i:2\ttp:A:P\tcm:i:2\ts1:i:39\ts2:i:10\tde:f:0.7338132\trl:i:38\n" +
                            "read_1\t3817\trefName\t98553\t60\t4=1X22=1X41=\t*\t0\t387\t*\t*\tNM:i:5\tms:i:7\tAS:i:19\tnn:i:1\ttp:A:P\tcm:i:2\ts1:i:4\ts2:i:26\tde:f:0.44234717\trl:i:0\n" +
                            "read_1\t1153\trefName\t28536\t53\t4=1X22=1X41=\t*\t0\t656\t*\t*\tNM:i:1\tms:i:4\tAS:i:80\tnn:i:3\ttp:A:S\tcm:i:4\ts1:i:32\tde:f:0.022118151\trl:i:21\n" +
                            "read_1\t1483\trefName\t8407\t32\t4=1X22=1X41=\t*\t0\t500\t*\t*\tNM:i:10\tms:i:30\tAS:i:1\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:11\tde:f:0.86207885\trl:i:7\n" +
                            "read_1\t3889\trefName\t43977\t44\t4=1X22=1X41=\t*\t0\t881\t*\t*\tNM:i:17\tms:i:67\tAS:i:57\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:35\tde:f:0.5923676\trl:i:32\n" +
                            "read_1\t3689\trefName\t21254\t15\t4=1X22=1X41=\t*\t0\t500\t*\t*\tNM:i:9\tms:i:42\tAS:i:62\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:45\tde:f:0.12954986\trl:i:24\n" +
                            "read_1\t4025\trefName\t77749\t49\t4=1X22=1X41=\t*\t0\t174\t*\t*\tNM:i:9\tms:i:29\tAS:i:12\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:19\tde:f:0.08111894\trl:i:31\n" +
                            "read_2\t3235\trefName\t7935\t19\t4=1X22=1X41=\t*\t0\t166\t*\t*\tNM:i:12\tms:i:61\tAS:i:78\tnn:i:0\ttp:A:P\tcm:i:3\ts1:i:10\ts2:i:47\tde:f:0.1955179\trl:i:5\n" +
                            "read_2\t2971\trefName\t4596\t43\t4=1X22=1X41=\t*\t0\t500\t*\t*\tNM:i:6\tms:i:98\tAS:i:57\tnn:i:4\ttp:A:S\tcm:i:4\ts1:i:26\tde:f:0.77458644\trl:i:4\n" +
                            "read_2\t1019\trefName\t417\t49\t4=1X22=1X41=\t*\t0\t62\t*\t*\tNM:i:3\tms:i:8\tAS:i:83\tnn:i:3\ttp:A:S\tcm:i:0\ts1:i:6\tde:f:0.70467806\trl:i:6\n" +
                            "read_3\t1673\trefName\t88239\t31\t4=1X22=1X41=\t*\t0\t969\t*\t*\tNM:i:13\tms:i:95\tAS:i:16\tnn:i:0\ttp:A:P\tcm:i:3\ts1:i:15\ts2:i:11\tde:f:0.7960879\trl:i:5\n" +
                            "read_3\t3785\trefName\t41427\t26\t4=1X22=1X41=\t*\t0\t902\t*\t*\tNM:i:14\tms:i:45\tAS:i:90\tnn:i:0\ttp:A:S\tcm:i:1\ts1:i:39\tde:f:0.59971744\trl:i:14\n" +
                            "read_3\t3531\trefName\t93371\t14\t4=1X22=1X41=\t*\t0\t303\t*\t*\tNM:i:2\tms:i:25\tAS:i:70\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:34\tde:f:0.2791673\trl:i:4\n" +
                            "read_3\t3721\trefName\t98438\t13\t4=1X22=1X41=\t*\t0\t304\t*\t*\tNM:i:1\tms:i:77\tAS:i:6\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:42\tde:f:0.07239044\trl:i:31\n" +
                            "read_3\t1697\trefName\t66822\t59\t4=1X22=1X41=\t*\t0\t509\t*\t*\tNM:i:17\tms:i:24\tAS:i:29\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:34\tde:f:0.36290473\trl:i:34\n" +
                            "read_3\t2603\trefName\t82610\t44\t4=1X22=1X41=\t*\t0\t562\t*\t*\tNM:i:10\tms:i:33\tAS:i:53\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:30\tde:f:0.8834799\trl:i:33\n" +
                            "read_3\t3393\trefName\t22014\t35\t4=1X22=1X41=\t*\t0\t644\t*\t*\tNM:i:10\tms:i:93\tAS:i:69\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:0\tde:f:0.90937984\trl:i:16\n" +
                            "read_3\t3411\trefName\t76872\t48\t4=1X22=1X41=\t*\t0\t957\t*\t*\tNM:i:14\tms:i:3\tAS:i:37\tnn:i:1\ttp:A:S\tcm:i:0\ts1:i:5\tde:f:0.35858893\trl:i:24\n" +
                            "read_3\t2227\trefName\t50270\t7\t4=1X22=1X41=\t*\t0\t959\t*\t*\tNM:i:4\tms:i:89\tAS:i:60\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:1\tde:f:0.09265202\trl:i:26\n" +
                            "read_4\t2113\trefName\t41275\t19\t4=1X22=1X41=\t*\t0\t134\t*\t*\tNM:i:6\tms:i:88\tAS:i:63\tnn:i:4\ttp:A:P\tcm:i:4\ts1:i:41\ts2:i:45\tde:f:0.7241713\trl:i:19\n" +
                            "read_4\t1177\trefName\t3883\t20\t4=1X22=1X41=\t*\t0\t914\t*\t*\tNM:i:14\tms:i:96\tAS:i:31\tnn:i:0\ttp:A:S\tcm:i:1\ts1:i:18\tde:f:0.4277771\trl:i:39\n" +
                            "read_4\t3771\trefName\t45742\t40\t4=1X22=1X41=\t*\t0\t622\t*\t*\tNM:i:19\tms:i:55\tAS:i:43\tnn:i:0\ttp:A:S\tcm:i:3\ts1:i:23\tde:f:0.43329352\trl:i:25\n" +
                            "read_4\t4033\trefName\t86491\t38\t4=1X22=1X41=\t*\t0\t56\t*\t*\tNM:i:15\tms:i:62\tAS:i:26\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:10\tde:f:0.72615623\trl:i:15\n" +
                            "read_4\t1507\trefName\t83104\t1\t4=1X22=1X41=\t*\t0\t67\t*\t*\tNM:i:7\tms:i:51\tAS:i:54\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:8\tde:f:0.067964554\trl:i:12\n" +
                            "read_4\t1321\trefName\t97254\t53\t4=1X22=1X41=\t*\t0\t902\t*\t*\tNM:i:0\tms:i:70\tAS:i:90\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:22\tde:f:0.4693098\trl:i:16\n" +
                            "read_4\t1867\trefName\t39575\t35\t4=1X22=1X41=\t*\t0\t53\t*\t*\tNM:i:12\tms:i:75\tAS:i:7\tnn:i:0\ttp:A:S\tcm:i:3\ts1:i:39\tde:f:0.4032117\trl:i:36\n" +
                            "read_4\t603\trefName\t96277\t15\t4=1X22=1X41=\t*\t0\t142\t*\t*\tNM:i:12\tms:i:38\tAS:i:68\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:44\tde:f:0.73864496\trl:i:9\n" +
                            "read_4\t491\trefName\t33386\t25\t4=1X22=1X41=\t*\t0\t498\t*\t*\tNM:i:5\tms:i:31\tAS:i:59\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:27\tde:f:0.88608986\trl:i:11\n" +
                            "read_4\t2315\trefName\t51677\t27\t4=1X22=1X41=\t*\t0\t331\t*\t*\tNM:i:12\tms:i:95\tAS:i:45\tnn:i:0\ttp:A:S\tcm:i:1\ts1:i:28\tde:f:0.6762575\trl:i:14\n" +
                            "read_5\t2617\trefName\t17374\t42\t4=1X22=1X41=\t*\t0\t243\t*\t*\tNM:i:10\tms:i:65\tAS:i:11\tnn:i:2\ttp:A:P\tcm:i:0\ts1:i:30\ts2:i:43\tde:f:0.20427251\trl:i:8\n" +
                            "read_6\t1659\trefName\t34310\t29\t4=1X22=1X41=\t*\t0\t947\t*\t*\tNM:i:1\tms:i:9\tAS:i:84\tnn:i:2\ttp:A:P\tcm:i:2\ts1:i:42\ts2:i:13\tde:f:0.91444993\trl:i:7\n" +
                            "read_6\t577\trefName\t88578\t15\t4=1X22=1X41=\t*\t0\t239\t*\t*\tNM:i:10\tms:i:30\tAS:i:68\tnn:i:1\ttp:A:S\tcm:i:3\ts1:i:6\tde:f:0.009613693\trl:i:26\n" +
                            "read_6\t1777\trefName\t93917\t1\t4=1X22=1X41=\t*\t0\t852\t*\t*\tNM:i:9\tms:i:73\tAS:i:56\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:27\tde:f:0.36300558\trl:i:28\n" +
                            "read_6\t115\trefName\t45270\t56\t4=1X22=1X41=\t*\t0\t544\t*\t*\tNM:i:18\tms:i:19\tAS:i:6\tnn:i:1\ttp:A:S\tcm:i:3\ts1:i:17\tde:f:0.9033934\trl:i:2\n" +
                            "read_6\t3241\trefName\t64746\t35\t4=1X22=1X41=\t*\t0\t254\t*\t*\tNM:i:2\tms:i:82\tAS:i:43\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:41\tde:f:0.9248257\trl:i:15\n" +
                            "read_6\t2571\trefName\t71888\t2\t4=1X22=1X41=\t*\t0\t782\t*\t*\tNM:i:11\tms:i:93\tAS:i:94\tnn:i:4\ttp:A:S\tcm:i:1\ts1:i:25\tde:f:0.8569106\trl:i:21\n" +
                            "read_6\t121\trefName\t34871\t54\t4=1X22=1X41=\t*\t0\t206\t*\t*\tNM:i:6\tms:i:12\tAS:i:28\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:49\tde:f:0.7938318\trl:i:31\n" +
                            "read_6\t3307\trefName\t12534\t27\t4=1X22=1X41=\t*\t0\t984\t*\t*\tNM:i:10\tms:i:87\tAS:i:16\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:44\tde:f:0.055000365\trl:i:30\n" +
                            "read_6\t1859\trefName\t29070\t35\t4=1X22=1X41=\t*\t0\t21\t*\t*\tNM:i:8\tms:i:54\tAS:i:40\tnn:i:1\ttp:A:S\tcm:i:0\ts1:i:25\tde:f:0.5459965\trl:i:15\n" +
                            "read_7\t2083\trefName\t98422\t30\t4=1X22=1X41=\t*\t0\t962\t*\t*\tNM:i:9\tms:i:36\tAS:i:64\tnn:i:4\ttp:A:P\tcm:i:2\ts1:i:20\ts2:i:6\tde:f:0.9186573\trl:i:36\n" +
                            "read_7\t625\trefName\t46477\t6\t4=1X22=1X41=\t*\t0\t254\t*\t*\tNM:i:5\tms:i:73\tAS:i:88\tnn:i:2\ttp:A:S\tcm:i:2\ts1:i:18\tde:f:0.36131358\trl:i:14\n" +
                            "read_7\t579\trefName\t55726\t53\t4=1X22=1X41=\t*\t0\t23\t*\t*\tNM:i:0\tms:i:9\tAS:i:39\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:42\tde:f:0.61674035\trl:i:3\n" +
                            "read_7\t419\trefName\t25434\t24\t4=1X22=1X41=\t*\t0\t305\t*\t*\tNM:i:5\tms:i:53\tAS:i:25\tnn:i:3\ttp:A:S\tcm:i:1\ts1:i:8\tde:f:0.8214364\trl:i:14\n" +
                            "read_7\t1465\trefName\t61914\t10\t4=1X22=1X41=\t*\t0\t584\t*\t*\tNM:i:18\tms:i:25\tAS:i:2\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:23\tde:f:0.98447514\trl:i:22\n" +
                            "read_7\t1545\trefName\t5995\t39\t4=1X22=1X41=\t*\t0\t177\t*\t*\tNM:i:14\tms:i:5\tAS:i:66\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:29\tde:f:0.9111419\trl:i:6\n" +
                            "read_7\t2241\trefName\t27909\t51\t4=1X22=1X41=\t*\t0\t474\t*\t*\tNM:i:0\tms:i:59\tAS:i:91\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:20\tde:f:0.0132851\trl:i:13\n" +
                            "read_8\t3241\trefName\t81249\t17\t4=1X22=1X41=\t*\t0\t346\t*\t*\tNM:i:12\tms:i:10\tAS:i:91\tnn:i:4\ttp:A:P\tcm:i:0\ts1:i:47\ts2:i:30\tde:f:0.2485342\trl:i:13\n" +
                            "read_8\t481\trefName\t85942\t0\t4=1X22=1X41=\t*\t0\t196\t*\t*\tNM:i:5\tms:i:19\tAS:i:50\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:37\tde:f:0.5293217\trl:i:7\n" +
                            "read_8\t1001\trefName\t14661\t47\t4=1X22=1X41=\t*\t0\t843\t*\t*\tNM:i:0\tms:i:97\tAS:i:23\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:24\tde:f:0.3994441\trl:i:35\n" +
                            "read_8\t417\trefName\t77622\t35\t4=1X22=1X41=\t*\t0\t934\t*\t*\tNM:i:14\tms:i:53\tAS:i:8\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:24\tde:f:0.44867903\trl:i:23\n" +
                            "read_8\t3441\trefName\t40648\t6\t4=1X22=1X41=\t*\t0\t759\t*\t*\tNM:i:15\tms:i:56\tAS:i:95\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:43\tde:f:0.12790537\trl:i:5\n" +
                            "read_8\t2451\trefName\t59213\t42\t4=1X22=1X41=\t*\t0\t631\t*\t*\tNM:i:14\tms:i:34\tAS:i:34\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:21\tde:f:0.09654158\trl:i:18\n" +
                            "read_8\t3849\trefName\t55184\t1\t4=1X22=1X41=\t*\t0\t892\t*\t*\tNM:i:6\tms:i:79\tAS:i:0\tnn:i:3\ttp:A:S\tcm:i:2\ts1:i:10\tde:f:0.22069824\trl:i:13\n" +
                            "read_8\t3337\trefName\t4552\t24\t4=1X22=1X41=\t*\t0\t644\t*\t*\tNM:i:19\tms:i:95\tAS:i:92\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:23\tde:f:0.8333381\trl:i:25\n" +
                            "read_8\t1203\trefName\t63293\t33\t4=1X22=1X41=\t*\t0\t267\t*\t*\tNM:i:13\tms:i:50\tAS:i:39\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:5\tde:f:0.8649523\trl:i:34\n" +
                            "read_9\t1585\trefName\t70646\t5\t4=1X22=1X41=\t*\t0\t652\t*\t*\tNM:i:12\tms:i:6\tAS:i:90\tnn:i:4\ttp:A:P\tcm:i:3\ts1:i:30\ts2:i:9\tde:f:0.62531304\trl:i:21\n" +
                            "read_9\t1289\trefName\t13918\t17\t4=1X22=1X41=\t*\t0\t397\t*\t*\tNM:i:10\tms:i:97\tAS:i:3\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:47\tde:f:0.7634203\trl:i:30\n" +
                            "read_10\t25\trefName\t7869\t7\t4=1X22=1X41=\t*\t0\t699\t*\t*\tNM:i:5\tms:i:12\tAS:i:45\tnn:i:4\ttp:A:P\tcm:i:4\ts1:i:44\ts2:i:37\tde:f:0.6207887\trl:i:23\n" +
                            "read_10\t489\trefName\t36008\t51\t4=1X22=1X41=\t*\t0\t485\t*\t*\tNM:i:0\tms:i:66\tAS:i:57\tnn:i:2\ttp:A:S\tcm:i:2\ts1:i:19\tde:f:0.8702467\trl:i:10\n" +
                            "read_10\t3779\trefName\t73289\t54\t4=1X22=1X41=\t*\t0\t666\t*\t*\tNM:i:11\tms:i:95\tAS:i:47\tnn:i:3\ttp:A:S\tcm:i:1\ts1:i:23\tde:f:0.7683856\trl:i:39\n" +
                            "read_10\t433\trefName\t96909\t9\t4=1X22=1X41=\t*\t0\t92\t*\t*\tNM:i:12\tms:i:69\tAS:i:23\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:49\tde:f:0.36641043\trl:i:22\n" +
                            "read_10\t1323\trefName\t15111\t0\t4=1X22=1X41=\t*\t0\t260\t*\t*\tNM:i:1\tms:i:12\tAS:i:85\tnn:i:1\ttp:A:S\tcm:i:1\ts1:i:6\tde:f:0.63841295\trl:i:34\n" +
                            "read_10\t193\trefName\t96548\t46\t4=1X22=1X41=\t*\t0\t930\t*\t*\tNM:i:9\tms:i:42\tAS:i:39\tnn:i:3\ttp:A:S\tcm:i:1\ts1:i:4\tde:f:0.83525074\trl:i:11\n" +
                            "read_10\t2561\trefName\t72638\t48\t4=1X22=1X41=\t*\t0\t830\t*\t*\tNM:i:6\tms:i:68\tAS:i:73\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:16\tde:f:0.040103614\trl:i:6\n" +
                            "read_10\t2211\trefName\t66111\t13\t4=1X22=1X41=\t*\t0\t220\t*\t*\tNM:i:0\tms:i:96\tAS:i:28\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:9\tde:f:0.29914665\trl:i:38\n" +
                            "read_11\t3115\trefName\t89958\t49\t4=1X22=1X41=\t*\t0\t584\t*\t*\tNM:i:12\tms:i:17\tAS:i:24\tnn:i:1\ttp:A:P\tcm:i:0\ts1:i:16\ts2:i:12\tde:f:0.7637613\trl:i:3\n" +
                            "read_11\t1387\trefName\t9417\t23\t4=1X22=1X41=\t*\t0\t493\t*\t*\tNM:i:4\tms:i:51\tAS:i:98\tnn:i:3\ttp:A:S\tcm:i:4\ts1:i:44\tde:f:0.6818062\trl:i:9\n" +
                            "read_11\t3385\trefName\t50259\t56\t4=1X22=1X41=\t*\t0\t393\t*\t*\tNM:i:6\tms:i:60\tAS:i:97\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:17\tde:f:0.5289711\trl:i:17\n" +
                            "read_12\t1031\trefName\t88967\t30\t4=1X22=1X41=\t*\t0\t668\t*\t*\tNM:i:0\tms:i:68\tAS:i:74\tnn:i:0\ttp:A:P\tcm:i:0\ts1:i:14\ts2:i:48\tde:f:0.72132695\trl:i:10\n" +
                            "read_13\t2691\trefName\t56444\t46\t4=1X22=1X41=\t*\t0\t117\t*\t*\tNM:i:15\tms:i:89\tAS:i:66\tnn:i:1\ttp:A:P\tcm:i:2\ts1:i:26\ts2:i:1\tde:f:0.7704609\trl:i:28\n" +
                            "read_14\t3225\trefName\t20518\t45\t4=1X22=1X41=\t*\t0\t807\t*\t*\tNM:i:2\tms:i:60\tAS:i:3\tnn:i:2\ttp:A:P\tcm:i:3\ts1:i:7\ts2:i:39\tde:f:0.24629986\trl:i:9\n" +
                            "read_14\t1537\trefName\t66010\t6\t4=1X22=1X41=\t*\t0\t873\t*\t*\tNM:i:15\tms:i:95\tAS:i:74\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:27\tde:f:0.3870083\trl:i:9\n" +
                            "read_14\t3627\trefName\t91642\t46\t4=1X22=1X41=\t*\t0\t276\t*\t*\tNM:i:9\tms:i:76\tAS:i:86\tnn:i:0\ttp:A:S\tcm:i:1\ts1:i:41\tde:f:0.58400416\trl:i:16\n" +
                            "read_14\t1761\trefName\t29025\t28\t4=1X22=1X41=\t*\t0\t550\t*\t*\tNM:i:6\tms:i:94\tAS:i:47\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:36\tde:f:0.5824675\trl:i:21\n" +
                            "read_14\t3345\trefName\t14666\t37\t4=1X22=1X41=\t*\t0\t887\t*\t*\tNM:i:13\tms:i:1\tAS:i:65\tnn:i:2\ttp:A:S\tcm:i:2\ts1:i:12\tde:f:0.78278565\trl:i:24\n" +
                            "read_14\t2627\trefName\t57454\t13\t4=1X22=1X41=\t*\t0\t524\t*\t*\tNM:i:18\tms:i:21\tAS:i:93\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:12\tde:f:0.6785221\trl:i:7\n" +
                            "read_14\t1595\trefName\t16332\t60\t4=1X22=1X41=\t*\t0\t873\t*\t*\tNM:i:17\tms:i:87\tAS:i:74\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:19\tde:f:0.25857782\trl:i:23\n" +
                            "read_14\t1521\trefName\t16547\t17\t4=1X22=1X41=\t*\t0\t689\t*\t*\tNM:i:17\tms:i:39\tAS:i:34\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:13\tde:f:0.27062094\trl:i:24\n" +
                            "read_14\t835\trefName\t61248\t17\t4=1X22=1X41=\t*\t0\t406\t*\t*\tNM:i:11\tms:i:16\tAS:i:87\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:38\tde:f:0.54560614\trl:i:16\n" +
                            "read_15\t3673\trefName\t26000\t30\t4=1X22=1X41=\t*\t0\t700\t*\t*\tNM:i:9\tms:i:72\tAS:i:2\tnn:i:4\ttp:A:P\tcm:i:3\ts1:i:35\ts2:i:16\tde:f:0.14104235\trl:i:12\n" +
                            "read_15\t817\trefName\t98176\t28\t4=1X22=1X41=\t*\t0\t802\t*\t*\tNM:i:9\tms:i:68\tAS:i:79\tnn:i:3\ttp:A:S\tcm:i:4\ts1:i:47\tde:f:0.39752376\trl:i:6\n" +
                            "read_15\t2443\trefName\t46446\t34\t4=1X22=1X41=\t*\t0\t58\t*\t*\tNM:i:6\tms:i:70\tAS:i:62\tnn:i:0\ttp:A:S\tcm:i:3\ts1:i:39\tde:f:0.20125097\trl:i:34\n" +
                            "read_15\t1913\trefName\t97407\t7\t4=1X22=1X41=\t*\t0\t222\t*\t*\tNM:i:3\tms:i:40\tAS:i:34\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:8\tde:f:0.1514659\trl:i:3\n" +
                            "read_15\t1459\trefName\t80739\t30\t4=1X22=1X41=\t*\t0\t397\t*\t*\tNM:i:13\tms:i:90\tAS:i:28\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:0\tde:f:0.3593579\trl:i:37\n" +
                            "read_15\t3699\trefName\t82993\t31\t4=1X22=1X41=\t*\t0\t31\t*\t*\tNM:i:8\tms:i:51\tAS:i:24\tnn:i:3\ttp:A:S\tcm:i:4\ts1:i:35\tde:f:0.61133695\trl:i:5\n" +
                            "read_15\t353\trefName\t34013\t41\t4=1X22=1X41=\t*\t0\t701\t*\t*\tNM:i:2\tms:i:30\tAS:i:34\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:21\tde:f:0.91957414\trl:i:31\n" +
                            "read_15\t2427\trefName\t82649\t55\t4=1X22=1X41=\t*\t0\t945\t*\t*\tNM:i:2\tms:i:61\tAS:i:95\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:30\tde:f:0.862188\trl:i:21\n" +
                            "read_15\t689\trefName\t70184\t12\t4=1X22=1X41=\t*\t0\t677\t*\t*\tNM:i:4\tms:i:93\tAS:i:29\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:34\tde:f:0.28326988\trl:i:23\n" +
                            "read_15\t2345\trefName\t62907\t5\t4=1X22=1X41=\t*\t0\t839\t*\t*\tNM:i:12\tms:i:91\tAS:i:13\tnn:i:1\ttp:A:S\tcm:i:1\ts1:i:24\tde:f:0.6715261\trl:i:1\n" +
                            "read_16\t3107\trefName\t63898\t3\t4=1X22=1X41=\t*\t0\t531\t*\t*\tNM:i:4\tms:i:6\tAS:i:13\tnn:i:0\ttp:A:P\tcm:i:0\ts1:i:37\ts2:i:2\tde:f:0.9569988\trl:i:0\n" +
                            "read_16\t3083\trefName\t21895\t48\t4=1X22=1X41=\t*\t0\t111\t*\t*\tNM:i:4\tms:i:1\tAS:i:99\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:10\tde:f:0.8079307\trl:i:2\n" +
                            "read_16\t971\trefName\t58520\t20\t4=1X22=1X41=\t*\t0\t241\t*\t*\tNM:i:4\tms:i:44\tAS:i:86\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:36\tde:f:0.61251277\trl:i:32\n" +
                            "read_16\t3099\trefName\t59593\t41\t4=1X22=1X41=\t*\t0\t429\t*\t*\tNM:i:4\tms:i:44\tAS:i:86\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:48\tde:f:0.9816222\trl:i:15\n" +
                            "read_16\t3345\trefName\t32892\t25\t4=1X22=1X41=\t*\t0\t348\t*\t*\tNM:i:18\tms:i:76\tAS:i:65\tnn:i:1\ttp:A:S\tcm:i:0\ts1:i:35\tde:f:0.70684004\trl:i:29\n" +
                            "read_16\t2281\trefName\t71222\t14\t4=1X22=1X41=\t*\t0\t81\t*\t*\tNM:i:2\tms:i:27\tAS:i:6\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:5\tde:f:0.39036173\trl:i:36\n" +
                            "read_17\t739\trefName\t45739\t59\t4=1X22=1X41=\t*\t0\t880\t*\t*\tNM:i:1\tms:i:98\tAS:i:69\tnn:i:3\ttp:A:P\tcm:i:2\ts1:i:3\ts2:i:49\tde:f:0.24909621\trl:i:18\n" +
                            "read_17\t1233\trefName\t15610\t4\t4=1X22=1X41=\t*\t0\t739\t*\t*\tNM:i:9\tms:i:40\tAS:i:76\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:4\tde:f:0.46799928\trl:i:2\n" +
                            "read_17\t1809\trefName\t58845\t37\t4=1X22=1X41=\t*\t0\t46\t*\t*\tNM:i:13\tms:i:76\tAS:i:50\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:33\tde:f:0.8303786\trl:i:5\n" +
                            "read_17\t2265\trefName\t1908\t10\t4=1X22=1X41=\t*\t0\t780\t*\t*\tNM:i:1\tms:i:78\tAS:i:43\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:19\tde:f:0.3310942\trl:i:31\n" +
                            "read_17\t2363\trefName\t38651\t7\t4=1X22=1X41=\t*\t0\t618\t*\t*\tNM:i:10\tms:i:54\tAS:i:39\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:11\tde:f:0.9204226\trl:i:21\n" +
                            "read_18\t657\trefName\t686\t10\t4=1X22=1X41=\t*\t0\t851\t*\t*\tNM:i:8\tms:i:46\tAS:i:77\tnn:i:1\ttp:A:P\tcm:i:1\ts1:i:9\ts2:i:15\tde:f:0.19520074\trl:i:6\n" +
                            "read_18\t3851\trefName\t71579\t18\t4=1X22=1X41=\t*\t0\t361\t*\t*\tNM:i:11\tms:i:14\tAS:i:73\tnn:i:4\ttp:A:S\tcm:i:0\ts1:i:24\tde:f:0.4616071\trl:i:1\n" +
                            "read_18\t3841\trefName\t1025\t34\t4=1X22=1X41=\t*\t0\t467\t*\t*\tNM:i:4\tms:i:2\tAS:i:68\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:18\tde:f:0.31782836\trl:i:20\n" +
                            "read_18\t1265\trefName\t89715\t1\t4=1X22=1X41=\t*\t0\t626\t*\t*\tNM:i:5\tms:i:9\tAS:i:55\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:9\tde:f:0.3036629\trl:i:23\n" +
                            "read_18\t4081\trefName\t28355\t35\t4=1X22=1X41=\t*\t0\t775\t*\t*\tNM:i:6\tms:i:0\tAS:i:2\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:13\tde:f:0.5123987\trl:i:20\n" +
                            "read_18\t473\trefName\t74895\t39\t4=1X22=1X41=\t*\t0\t492\t*\t*\tNM:i:2\tms:i:79\tAS:i:47\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:19\tde:f:0.90169775\trl:i:10\n" +
                            "read_18\t2761\trefName\t82873\t54\t4=1X22=1X41=\t*\t0\t698\t*\t*\tNM:i:13\tms:i:71\tAS:i:83\tnn:i:0\ttp:A:S\tcm:i:3\ts1:i:41\tde:f:0.5525387\trl:i:29\n" +
                            "read_18\t705\trefName\t96974\t45\t4=1X22=1X41=\t*\t0\t312\t*\t*\tNM:i:18\tms:i:51\tAS:i:49\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:10\tde:f:0.7044408\trl:i:17\n" +
                            "read_19\t2729\trefName\t7162\t28\t4=1X22=1X41=\t*\t0\t553\t*\t*\tNM:i:0\tms:i:33\tAS:i:29\tnn:i:4\ttp:A:P\tcm:i:3\ts1:i:46\ts2:i:2\tde:f:0.25326264\trl:i:21\n" +
                            "read_19\t187\trefName\t85361\t17\t4=1X22=1X41=\t*\t0\t445\t*\t*\tNM:i:9\tms:i:82\tAS:i:87\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:31\tde:f:0.29061675\trl:i:35\n" +
                            "read_19\t315\trefName\t66457\t45\t4=1X22=1X41=\t*\t0\t731\t*\t*\tNM:i:17\tms:i:14\tAS:i:13\tnn:i:4\ttp:A:S\tcm:i:1\ts1:i:3\tde:f:0.19604039\trl:i:35\n" +
                            "read_19\t553\trefName\t85413\t7\t4=1X22=1X41=\t*\t0\t45\t*\t*\tNM:i:17\tms:i:34\tAS:i:96\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:16\tde:f:0.41687685\trl:i:23\n" +
                            "read_19\t1099\trefName\t36438\t4\t4=1X22=1X41=\t*\t0\t791\t*\t*\tNM:i:14\tms:i:29\tAS:i:47\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:12\tde:f:0.4567802\trl:i:0\n" +
                            "read_20\t129\trefName\t88924\t33\t4=1X22=1X41=\t*\t0\t84\t*\t*\tNM:i:7\tms:i:86\tAS:i:9\tnn:i:4\ttp:A:P\tcm:i:1\ts1:i:19\ts2:i:15\tde:f:0.20038188\trl:i:36\n" +
                            "read_20\t3059\trefName\t45162\t5\t4=1X22=1X41=\t*\t0\t522\t*\t*\tNM:i:14\tms:i:98\tAS:i:4\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:39\tde:f:0.27901733\trl:i:4\n" +
                            "read_20\t193\trefName\t54438\t28\t4=1X22=1X41=\t*\t0\t73\t*\t*\tNM:i:16\tms:i:24\tAS:i:29\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:12\tde:f:0.41373855\trl:i:7\n" +
                            "read_20\t3241\trefName\t92986\t16\t4=1X22=1X41=\t*\t0\t616\t*\t*\tNM:i:11\tms:i:96\tAS:i:16\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:31\tde:f:0.4313264\trl:i:4\n" +
                            "read_20\t1769\trefName\t82173\t26\t4=1X22=1X41=\t*\t0\t688\t*\t*\tNM:i:3\tms:i:49\tAS:i:25\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:47\tde:f:0.39937496\trl:i:7\n" +
                            "read_20\t1049\trefName\t48460\t31\t4=1X22=1X41=\t*\t0\t105\t*\t*\tNM:i:10\tms:i:95\tAS:i:52\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:22\tde:f:0.82546914\trl:i:27\n" +
                            "read_20\t179\trefName\t72724\t9\t4=1X22=1X41=\t*\t0\t68\t*\t*\tNM:i:14\tms:i:41\tAS:i:9\tnn:i:1\ttp:A:S\tcm:i:0\ts1:i:36\tde:f:0.196239\trl:i:1\n" +
                            "read_20\t1921\trefName\t1036\t42\t4=1X22=1X41=\t*\t0\t47\t*\t*\tNM:i:14\tms:i:1\tAS:i:44\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:29\tde:f:0.6358941\trl:i:10\n" +
                            "read_20\t1561\trefName\t14337\t11\t4=1X22=1X41=\t*\t0\t804\t*\t*\tNM:i:14\tms:i:31\tAS:i:27\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:10\tde:f:0.75760907\trl:i:18\n" +
                            "read_21\t3673\trefName\t30315\t32\t4=1X22=1X41=\t*\t0\t227\t*\t*\tNM:i:16\tms:i:4\tAS:i:94\tnn:i:4\ttp:A:P\tcm:i:1\ts1:i:31\ts2:i:37\tde:f:0.8891204\trl:i:3\n" +
                            "read_21\t3809\trefName\t52175\t8\t4=1X22=1X41=\t*\t0\t123\t*\t*\tNM:i:16\tms:i:49\tAS:i:40\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:33\tde:f:0.94986707\trl:i:19\n" +
                            "read_21\t1843\trefName\t33469\t3\t4=1X22=1X41=\t*\t0\t875\t*\t*\tNM:i:10\tms:i:2\tAS:i:28\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:45\tde:f:0.5623727\trl:i:7\n" +
                            "read_21\t2595\trefName\t41250\t40\t4=1X22=1X41=\t*\t0\t861\t*\t*\tNM:i:3\tms:i:86\tAS:i:37\tnn:i:4\ttp:A:S\tcm:i:2\ts1:i:32\tde:f:0.7106766\trl:i:14\n" +
                            "read_21\t1939\trefName\t73500\t37\t4=1X22=1X41=\t*\t0\t786\t*\t*\tNM:i:10\tms:i:48\tAS:i:96\tnn:i:3\ttp:A:S\tcm:i:1\ts1:i:5\tde:f:0.48953992\trl:i:16\n" +
                            "read_21\t3299\trefName\t86058\t9\t4=1X22=1X41=\t*\t0\t206\t*\t*\tNM:i:8\tms:i:25\tAS:i:13\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:43\tde:f:0.4615637\trl:i:26\n" +
                            "read_21\t3251\trefName\t41669\t58\t4=1X22=1X41=\t*\t0\t756\t*\t*\tNM:i:10\tms:i:75\tAS:i:48\tnn:i:4\ttp:A:S\tcm:i:0\ts1:i:30\tde:f:0.9033396\trl:i:8\n" +
                            "read_21\t2003\trefName\t27246\t15\t4=1X22=1X41=\t*\t0\t246\t*\t*\tNM:i:16\tms:i:34\tAS:i:43\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:47\tde:f:0.48513782\trl:i:14\n" +
                            "read_21\t585\trefName\t65912\t11\t4=1X22=1X41=\t*\t0\t712\t*\t*\tNM:i:5\tms:i:20\tAS:i:22\tnn:i:3\ttp:A:S\tcm:i:2\ts1:i:24\tde:f:0.5356106\trl:i:21\n" +
                            "read_22\t3633\trefName\t33852\t2\t4=1X22=1X41=\t*\t0\t341\t*\t*\tNM:i:14\tms:i:12\tAS:i:80\tnn:i:0\ttp:A:P\tcm:i:1\ts1:i:37\ts2:i:3\tde:f:0.7836676\trl:i:26\n" +
                            "read_22\t2033\trefName\t89796\t5\t4=1X22=1X41=\t*\t0\t657\t*\t*\tNM:i:12\tms:i:58\tAS:i:23\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:36\tde:f:0.057862222\trl:i:6\n" +
                            "read_22\t3667\trefName\t22856\t16\t4=1X22=1X41=\t*\t0\t444\t*\t*\tNM:i:17\tms:i:84\tAS:i:7\tnn:i:1\ttp:A:S\tcm:i:1\ts1:i:42\tde:f:0.3370573\trl:i:13\n" +
                            "read_22\t2075\trefName\t35773\t45\t4=1X22=1X41=\t*\t0\t392\t*\t*\tNM:i:11\tms:i:59\tAS:i:29\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:8\tde:f:0.6310755\trl:i:36\n" +
                            "read_23\t1539\trefName\t45172\t0\t4=1X22=1X41=\t*\t0\t245\t*\t*\tNM:i:17\tms:i:66\tAS:i:44\tnn:i:1\ttp:A:P\tcm:i:1\ts1:i:21\ts2:i:25\tde:f:0.0893144\trl:i:22\n" +
                            "read_23\t3377\trefName\t80112\t20\t4=1X22=1X41=\t*\t0\t305\t*\t*\tNM:i:19\tms:i:67\tAS:i:77\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:29\tde:f:0.6941662\trl:i:9\n" +
                            "read_24\t1089\trefName\t81611\t54\t4=1X22=1X41=\t*\t0\t358\t*\t*\tNM:i:11\tms:i:70\tAS:i:42\tnn:i:0\ttp:A:P\tcm:i:2\ts1:i:19\ts2:i:41\tde:f:0.10226619\trl:i:26\n" +
                            "read_25\t129\trefName\t60805\t6\t4=1X22=1X41=\t*\t0\t0\t*\t*\tNM:i:7\tms:i:94\tAS:i:17\tnn:i:4\ttp:A:P\tcm:i:2\ts1:i:1\ts2:i:21\tde:f:0.7873634\trl:i:25\n" +
                            "read_25\t3521\trefName\t83052\t42\t4=1X22=1X41=\t*\t0\t805\t*\t*\tNM:i:18\tms:i:64\tAS:i:31\tnn:i:3\ttp:A:S\tcm:i:2\ts1:i:3\tde:f:0.92978746\trl:i:30\n" +
                            "read_25\t323\trefName\t59168\t21\t4=1X22=1X41=\t*\t0\t127\t*\t*\tNM:i:0\tms:i:81\tAS:i:86\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:48\tde:f:0.36342734\trl:i:33\n" +
                            "read_25\t913\trefName\t71735\t44\t4=1X22=1X41=\t*\t0\t9\t*\t*\tNM:i:7\tms:i:42\tAS:i:64\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:9\tde:f:0.017554283\trl:i:33\n" +
                            "read_25\t761\trefName\t50748\t7\t4=1X22=1X41=\t*\t0\t505\t*\t*\tNM:i:1\tms:i:68\tAS:i:26\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:4\tde:f:0.6831026\trl:i:7\n" +
                            "read_25\t2233\trefName\t78378\t50\t4=1X22=1X41=\t*\t0\t730\t*\t*\tNM:i:0\tms:i:10\tAS:i:7\tnn:i:3\ttp:A:S\tcm:i:0\ts1:i:9\tde:f:0.32476884\trl:i:10\n" +
                            "read_25\t1099\trefName\t6115\t13\t4=1X22=1X41=\t*\t0\t536\t*\t*\tNM:i:19\tms:i:53\tAS:i:39\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:49\tde:f:0.23703468\trl:i:39\n" +
                            "read_25\t977\trefName\t79463\t53\t4=1X22=1X41=\t*\t0\t264\t*\t*\tNM:i:12\tms:i:97\tAS:i:3\tnn:i:1\ttp:A:S\tcm:i:3\ts1:i:0\tde:f:0.6652405\trl:i:34\n" +
                            "read_25\t1331\trefName\t87810\t28\t4=1X22=1X41=\t*\t0\t321\t*\t*\tNM:i:18\tms:i:5\tAS:i:59\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:43\tde:f:0.586323\trl:i:17\n" +
                            "read_26\t101\trefName\t14124\t37\t4=1X22=1X41=\t*\t0\t910\t*\t*\tNM:i:17\tms:i:48\tAS:i:10\tnn:i:4\ttp:A:P\tcm:i:1\ts1:i:40\ts2:i:36\tde:f:0.6837952\trl:i:31\n" +
                            "read_27\t2145\trefName\t75614\t57\t4=1X22=1X41=\t*\t0\t996\t*\t*\tNM:i:19\tms:i:15\tAS:i:48\tnn:i:0\ttp:A:P\tcm:i:3\ts1:i:35\ts2:i:11\tde:f:0.8151458\trl:i:7\n" +
                            "read_27\t3953\trefName\t70289\t51\t4=1X22=1X41=\t*\t0\t758\t*\t*\tNM:i:13\tms:i:24\tAS:i:48\tnn:i:2\ttp:A:S\tcm:i:4\ts1:i:40\tde:f:0.620053\trl:i:21\n" +
                            "read_27\t1313\trefName\t26159\t46\t4=1X22=1X41=\t*\t0\t327\t*\t*\tNM:i:9\tms:i:92\tAS:i:19\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:2\tde:f:0.8558242\trl:i:31\n" +
                            "read_27\t1595\trefName\t94826\t17\t4=1X22=1X41=\t*\t0\t578\t*\t*\tNM:i:10\tms:i:35\tAS:i:2\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:36\tde:f:0.100999296\trl:i:18\n" +
                            "read_27\t659\trefName\t90311\t24\t4=1X22=1X41=\t*\t0\t289\t*\t*\tNM:i:8\tms:i:6\tAS:i:47\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:31\tde:f:0.22732961\trl:i:33\n" +
                            "read_27\t889\trefName\t3996\t28\t4=1X22=1X41=\t*\t0\t245\t*\t*\tNM:i:9\tms:i:88\tAS:i:96\tnn:i:4\ttp:A:S\tcm:i:4\ts1:i:40\tde:f:0.7609504\trl:i:30\n" +
                            "read_28\t3267\trefName\t98567\t7\t4=1X22=1X41=\t*\t0\t687\t*\t*\tNM:i:7\tms:i:81\tAS:i:28\tnn:i:1\ttp:A:P\tcm:i:1\ts1:i:32\ts2:i:41\tde:f:0.504163\trl:i:37\n" +
                            "read_29\t537\trefName\t64453\t40\t4=1X22=1X41=\t*\t0\t275\t*\t*\tNM:i:2\tms:i:49\tAS:i:77\tnn:i:3\ttp:A:P\tcm:i:1\ts1:i:18\ts2:i:30\tde:f:0.08866435\trl:i:8\n" +
                            "read_29\t3169\trefName\t46855\t50\t4=1X22=1X41=\t*\t0\t20\t*\t*\tNM:i:14\tms:i:26\tAS:i:26\tnn:i:3\ttp:A:S\tcm:i:0\ts1:i:16\tde:f:0.9977871\trl:i:4\n" +
                            "read_29\t1129\trefName\t29780\t60\t4=1X22=1X41=\t*\t0\t870\t*\t*\tNM:i:16\tms:i:6\tAS:i:4\tnn:i:3\ttp:A:S\tcm:i:3\ts1:i:18\tde:f:0.4117654\trl:i:39\n" +
                            "read_29\t3193\trefName\t58394\t2\t4=1X22=1X41=\t*\t0\t548\t*\t*\tNM:i:10\tms:i:64\tAS:i:98\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:21\tde:f:0.28067672\trl:i:35\n" +
                            "read_29\t3611\trefName\t93971\t26\t4=1X22=1X41=\t*\t0\t459\t*\t*\tNM:i:5\tms:i:99\tAS:i:73\tnn:i:2\ttp:A:S\tcm:i:0\ts1:i:5\tde:f:0.24875808\trl:i:3\n" +
                            "read_29\t3009\trefName\t13526\t15\t4=1X22=1X41=\t*\t0\t972\t*\t*\tNM:i:1\tms:i:25\tAS:i:28\tnn:i:4\ttp:A:S\tcm:i:4\ts1:i:48\tde:f:0.34576863\trl:i:7\n" +
                            "read_29\t2747\trefName\t81266\t54\t4=1X22=1X41=\t*\t0\t165\t*\t*\tNM:i:5\tms:i:12\tAS:i:35\tnn:i:3\ttp:A:S\tcm:i:2\ts1:i:35\tde:f:0.2855037\trl:i:18\n" +
                            "read_30\t581\trefName\t23231\t56\t4=1X22=1X41=\t*\t0\t743\t*\t*\tNM:i:1\tms:i:22\tAS:i:74\tnn:i:3\ttp:A:P\tcm:i:3\ts1:i:9\ts2:i:36\tde:f:0.9709843\trl:i:37\n" +
                            "read_31\t2667\trefName\t13246\t56\t4=1X22=1X41=\t*\t0\t759\t*\t*\tNM:i:16\tms:i:36\tAS:i:97\tnn:i:0\ttp:A:P\tcm:i:1\ts1:i:39\ts2:i:22\tde:f:0.19247717\trl:i:5\n" +
                            "read_31\t1321\trefName\t82240\t23\t4=1X22=1X41=\t*\t0\t235\t*\t*\tNM:i:16\tms:i:35\tAS:i:37\tnn:i:4\ttp:A:S\tcm:i:4\ts1:i:1\tde:f:0.5176245\trl:i:16\n" +
                            "read_31\t1057\trefName\t33753\t20\t4=1X22=1X41=\t*\t0\t382\t*\t*\tNM:i:13\tms:i:93\tAS:i:59\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:5\tde:f:0.4895255\trl:i:20\n" +
                            "read_31\t3721\trefName\t62591\t35\t4=1X22=1X41=\t*\t0\t830\t*\t*\tNM:i:15\tms:i:38\tAS:i:0\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:11\tde:f:0.44821566\trl:i:11\n" +
                            "read_31\t1619\trefName\t53810\t42\t4=1X22=1X41=\t*\t0\t119\t*\t*\tNM:i:4\tms:i:37\tAS:i:57\tnn:i:4\ttp:A:S\tcm:i:3\ts1:i:35\tde:f:0.44350028\trl:i:12\n" +
                            "read_31\t1451\trefName\t87200\t50\t4=1X22=1X41=\t*\t0\t230\t*\t*\tNM:i:0\tms:i:29\tAS:i:67\tnn:i:3\ttp:A:S\tcm:i:4\ts1:i:23\tde:f:0.39068407\trl:i:20\n" +
                            "read_31\t3411\trefName\t18555\t35\t4=1X22=1X41=\t*\t0\t356\t*\t*\tNM:i:8\tms:i:59\tAS:i:53\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:13\tde:f:0.5512168\trl:i:25\n" +
                            "read_31\t1633\trefName\t86444\t4\t4=1X22=1X41=\t*\t0\t419\t*\t*\tNM:i:5\tms:i:51\tAS:i:38\tnn:i:3\ttp:A:S\tcm:i:2\ts1:i:36\tde:f:0.47351265\trl:i:5\n" +
                            "read_32\t1261\trefName\t73977\t9\t4=1X22=1X41=\t*\t0\t890\t*\t*\tNM:i:4\tms:i:43\tAS:i:88\tnn:i:0\ttp:A:P\tcm:i:2\ts1:i:14\ts2:i:21\tde:f:0.32046556\trl:i:27\n" +
                            "read_33\t3837\trefName\t78373\t36\t4=1X22=1X41=\t*\t0\t664\t*\t*\tNM:i:12\tms:i:35\tAS:i:32\tnn:i:4\ttp:A:P\tcm:i:4\ts1:i:22\ts2:i:34\tde:f:0.72054505\trl:i:28\n" +
                            "read_34\t3249\trefName\t65818\t26\t4=1X22=1X41=\t*\t0\t121\t*\t*\tNM:i:15\tms:i:98\tAS:i:24\tnn:i:2\ttp:A:P\tcm:i:4\ts1:i:30\ts2:i:2\tde:f:0.97060084\trl:i:3\n" +
                            "read_34\t1531\trefName\t67020\t13\t4=1X22=1X41=\t*\t0\t220\t*\t*\tNM:i:1\tms:i:12\tAS:i:6\tnn:i:1\ttp:A:S\tcm:i:4\ts1:i:10\tde:f:0.41788083\trl:i:0\n" +
                            "read_34\t2251\trefName\t67449\t26\t4=1X22=1X41=\t*\t0\t430\t*\t*\tNM:i:15\tms:i:12\tAS:i:82\tnn:i:3\ttp:A:S\tcm:i:0\ts1:i:16\tde:f:0.48370594\trl:i:31\n" +
                            "read_34\t307\trefName\t1522\t8\t4=1X22=1X41=\t*\t0\t596\t*\t*\tNM:i:10\tms:i:20\tAS:i:42\tnn:i:1\ttp:A:S\tcm:i:3\ts1:i:11\tde:f:0.20667839\trl:i:22\n" +
                            "read_34\t1235\trefName\t17987\t9\t4=1X22=1X41=\t*\t0\t437\t*\t*\tNM:i:15\tms:i:88\tAS:i:25\tnn:i:3\ttp:A:S\tcm:i:1\ts1:i:11\tde:f:0.9531628\trl:i:10\n" +
                            "read_34\t1131\trefName\t34239\t60\t4=1X22=1X41=\t*\t0\t625\t*\t*\tNM:i:8\tms:i:93\tAS:i:26\tnn:i:2\ttp:A:S\tcm:i:3\ts1:i:9\tde:f:0.9035632\trl:i:1\n" +
                            "read_34\t2531\trefName\t82115\t18\t4=1X22=1X41=\t*\t0\t975\t*\t*\tNM:i:15\tms:i:37\tAS:i:46\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:46\tde:f:0.3141876\trl:i:10\n" +
                            "read_34\t1521\trefName\t10350\t25\t4=1X22=1X41=\t*\t0\t959\t*\t*\tNM:i:8\tms:i:82\tAS:i:55\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:15\tde:f:0.5620008\trl:i:39\n" +
                            "read_35\t2105\trefName\t11612\t2\t4=1X22=1X41=\t*\t0\t767\t*\t*\tNM:i:13\tms:i:37\tAS:i:82\tnn:i:2\ttp:A:P\tcm:i:2\ts1:i:10\ts2:i:24\tde:f:0.21968877\trl:i:25\n" +
                            "read_36\t1203\trefName\t60999\t51\t4=1X22=1X41=\t*\t0\t58\t*\t*\tNM:i:0\tms:i:8\tAS:i:34\tnn:i:4\ttp:A:P\tcm:i:2\ts1:i:17\ts2:i:46\tde:f:0.30402392\trl:i:27\n" +
                            "read_36\t3937\trefName\t57247\t2\t4=1X22=1X41=\t*\t0\t685\t*\t*\tNM:i:8\tms:i:11\tAS:i:24\tnn:i:0\ttp:A:S\tcm:i:3\ts1:i:28\tde:f:0.7688387\trl:i:22\n" +
                            "read_36\t1531\trefName\t55915\t39\t4=1X22=1X41=\t*\t0\t183\t*\t*\tNM:i:8\tms:i:63\tAS:i:25\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:7\tde:f:0.11590749\trl:i:38\n" +
                            "read_36\t1033\trefName\t42611\t32\t4=1X22=1X41=\t*\t0\t824\t*\t*\tNM:i:19\tms:i:67\tAS:i:93\tnn:i:0\ttp:A:S\tcm:i:4\ts1:i:45\tde:f:0.42330772\trl:i:4\n" +
                            "read_36\t1051\trefName\t91584\t18\t4=1X22=1X41=\t*\t0\t314\t*\t*\tNM:i:7\tms:i:41\tAS:i:69\tnn:i:2\ttp:A:S\tcm:i:1\ts1:i:16\tde:f:0.38725048\trl:i:24\n" +
                            "read_36\t289\trefName\t99453\t46\t4=1X22=1X41=\t*\t0\t863\t*\t*\tNM:i:15\tms:i:28\tAS:i:16\tnn:i:3\ttp:A:S\tcm:i:0\ts1:i:17\tde:f:0.9750361\trl:i:9\n" +
                            "read_36\t947\trefName\t79743\t24\t4=1X22=1X41=\t*\t0\t885\t*\t*\tNM:i:16\tms:i:74\tAS:i:93\tnn:i:1\ttp:A:S\tcm:i:0\ts1:i:16\tde:f:0.92740417\trl:i:30\n" +
                            "read_37\t1625\trefName\t32261\t24\t4=1X22=1X41=\t*\t0\t645\t*\t*\tNM:i:9\tms:i:34\tAS:i:5\tnn:i:0\ttp:A:P\tcm:i:0\ts1:i:12\ts2:i:15\tde:f:0.9321523\trl:i:24\n" +
                            "read_37\t625\trefName\t86894\t47\t4=1X22=1X41=\t*\t0\t838\t*\t*\tNM:i:3\tms:i:25\tAS:i:17\tnn:i:0\ttp:A:S\tcm:i:2\ts1:i:41\tde:f:0.13880378\trl:i:21\n" +
                            "read_37\t1883\trefName\t93093\t2\t4=1X22=1X41=\t*\t0\t874\t*\t*\tNM:i:16\tms:i:24\tAS:i:70\tnn:i:1\ttp:A:S\tcm:i:3\ts1:i:40\tde:f:0.82506466\trl:i:16\n" +
                            "read_37\t2939\trefName\t2868\t15\t4=1X22=1X41=\t*\t0\t829\t*\t*\tNM:i:3\tms:i:2\tAS:i:7\tnn:i:0\ttp:A:S\tcm:i:0\ts1:i:25\tde:f:0.962518\trl:i:26\n" +
                            "read_37\t1265\trefName\t69112\t15\t4=1X22=1X41=\t*\t0\t747\t*\t*\tNM:i:10\tms:i:88\tAS:i:24\tnn:i:1\ttp:A:S\tcm:i:2\ts1:i:5\tde:f:0.9540686\trl:i:4\n"
                )
            }
        }

        /**
         * Helper function writes blank "SAM" file, simulating potential failure of alignment software in a previous step.
         */
        private fun createBlankSAM(outputFile: String) {
            Utils.getBufferedWriter(outputFile).use {}
        }
    }
}