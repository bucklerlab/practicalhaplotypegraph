gene_extend_trimmed_f=$1
gene_extend_f=$2
gene_f=$3

#bt_bin="$(which bedtools)"

( echo -e "#Chromosome level summary statistics (mean)\nchromosome\tgene_block_length\texpanded_block_length\ttrimmed_block_length\tinitial_gene_block_expansion\ttrimmed_gene_block_expansion" && \
    $bt_bin intersect -nonamecheck -wa -wb -a $gene_extend_trimmed_f -b $gene_extend_f | \
    $bt_bin intersect -nonamecheck -wa -wb -a stdin -b $gene_f | \
    $awk_bin 'BEGIN{FS="\t";OFS="\t"}{print $1, $11-$10, $7-$6, $3-$2, ($7-$6)-($11-$10), ($3-$2)-($11-$10)}' | \
    $bt_bin  groupby -g 1 -c 2,3,4,5,6 -o mean | \
    $awk_bin '
BEGIN{
  FS="\t";
  OFS="\t"
}
{
  for(i = 2; i <= NF; i++){
    $i = sprintf("%.0f", $i);
  }
  print $0
}'
)
