**Anchor rAmpSeq to DB genome intervals**

The current process for associating rAmpSeq contigs with PHG database genome intervals 
is the following:

130 bp tag sequences-> map to assembly -> BED file -> Fasta with +-100kb -> MinimapPipeline ->
Create BAM -> RampSeqContigToGenomeIntervalPlugin -> tab-delimited File of contig-to-genomeInterval

Details:

**Processing by Dan Ilut**
* Run tags (130 bp sequences) from each assembly taxon against its own assembly, look for exact matches
    * this procedure is now done by Lynn using methods in pipelineTests.  See notebook FindRampSeqShortSeqInAssemblies.md for details
* Generate a BED File for each assembly taxon 
* Use bedtools to pull +-100kb around each match
* Generate a fasta file for each taxon

**Minimap2PipelinePlugin Processing**
* take fasta from above and B73 reference genome file, run through Minimap2PipelinePlugin, the "onlyBam" flag =true
* Pipeline creates an indexed/sorted BAM file to the specified output directory

**RampSeqContigToGEnomeIntervalPlugin Processing**
* input:  the BAM file created from Minimap2PipelinePlugin, and a PHG DB containing ref anchors and inter-anchors
* output:  a tab delimited file with headers:
    * ContigName  GenomeIntervalRange  GenomeIntervalId  IsAnchor
    
The PHG db does not need to be populated with anything other than the ref anchors and interanchors as we are only making use of the genome_intervals table.  This means a smaller db and this method can be run on a laptop vs CBSU.

UPDATE: October 5, 2017

New data from Dan, new run on cbsudc01.  Minimap2 is run via shell script minimapDanOct5_11minum2Assemblies.sh on cbsudc01.  It contains a line for each of the 9 (out of 11, 2 had no data) assemblies from Dan.  Each command line looks as below:

time /workdir/lcj34/tassel-5-standalone/run_pipeline.pl -Xmx200g -Minimap2PipelinePlugin -ref maize_assemblies/Zea_mays.AGPv4.
dna.toplevel.fa -assembly rampSeq_fastas2/Zm-B104-DRAFT-ISU_USDA-0.2_shortSeqPositions-expand100000.fasta -name B104expand1000
00 -intervals /workdir/lcj34/Practical_haplotype_graph/project_pangenome_files/intervals_files/hackathon_trimmed_intervals.bed
 -minimap2Path jars/minimap2 -o rampSeq_fastas2/minimap2output/ -onlyBam true  > rampSeq_fastas2/minimap2output/B104expand1000
00_output.txt


Interval mapping data was moved to cbsublfs1 in the
/data1/PHG/rAmpSeq_files/align_to_assembly folder, files stored by assembly.