package net.maizegenetics.pangenome.hapCalling

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.common.collect.ImmutableSet
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.ReferenceRange
import java.io.File

/**
 * Class to hold the 4 pieces of information which are needed for readMapping.
 * Using Jackson this class can be serialized into JSON
 */
data class GraphIdMaps(val hapIdToRefRangeMap:Map<Int, RefRangeWrapper>,
                       val hapIdToLengthMap: Map<Int,Int>,
                       val refRangeToHapIdMap: Map<Int, Map<Int, Int>>,
                       val haplotypeListId: Int,
                       val hapIdToTaxaList : Map<Int,List<String>> = mapOf())

/**
 * Need a ReferenceRange Wrapper function as the Current ReferenceRange is not serializable due to the Chromosome object.
 */
data class RefRangeWrapper(val myReferenceName: String, val myChromosome: String , val myStart: Int, val myEnd: Int, val myID: Int, val myMethods: Set<String>)

/**
 * Simple helper method to reserialize the JSON file into the GraphIdMap class
 */
fun deserializeGraphIdMaps(fileName: String) : GraphIdMaps {
    val mapper = jacksonObjectMapper()
    return mapper.readValue(File(fileName), GraphIdMaps::class.java)
}

/**
 * Simple helper method to unwrap the Wrapped ReferenceRanges into a real ReferenceRange instance
 */
fun unwrapRefRanges(map: Map<Int,RefRangeWrapper>): Map<Int,ReferenceRange> {
    return map.map { Pair(it.key,createSingleRefRange(it.value)) }.toMap()
}

/**
 * Unwrapper function for a single Reference Range.
 */
//String referenceName, Chromosome chromosome, int start, int end, int id, ImmutableSet<String> methods
fun createSingleRefRange(wrappedRefRange: RefRangeWrapper) : ReferenceRange {
    val methodSet = ImmutableSet.builder<String>()
    wrappedRefRange.myMethods.forEach { methodSet.add(it) }

    return ReferenceRange(wrappedRefRange.myReferenceName, Chromosome.instance(wrappedRefRange.myChromosome), wrappedRefRange.myStart, wrappedRefRange.myEnd, wrappedRefRange.myID, methodSet.build() )
}