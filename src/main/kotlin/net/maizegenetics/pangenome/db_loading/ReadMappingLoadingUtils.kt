package net.maizegenetics.pangenome.db_loading

data class ReadMappingDBRecord(val methodName: String, val methodDetails:String , val readMappingId:Int, val readMappings: ByteArray, val genoName : String,val fileGroupName:String)

//similar to ReadMappingDBRecord but does not include readMappings or methodDetails
data class ReadMappingIdDBRecord(val methodName: String, val readMappingId:Int, val genoName : String,val fileGroupName:String)
