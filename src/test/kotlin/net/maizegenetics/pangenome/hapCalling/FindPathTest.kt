package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import java.io.PrintWriter

fun main() {
    testPathFindingSmallSeq()
    println("**************************")
}

fun testPathFindingSmallSeq() {
    LoggingUtils.setupDebugLogging()
    val hgbp = HaplotypeGraphBuilderPlugin(null, false)
    hgbp.configFile("${SmallSeqPaths.dataDir}configSQLite.txt")
    hgbp.methods("CONSENSUS")
    val graph = hgbp.build()

    ParameterCache.load("/Users/peterbradbury/temp/phgSmallSeq/data/configSQLite.txt")
    val pathFinder = BestHaplotypePathPlugin(null, false)
            .pathMethodName("test_path_classic1")
            .readMethodName("HAP_COUNT_METHOD")
            .keyFile("${SmallSeqPaths.dataDir}genotypingKeyFile_pathKeyFile.txt")
            .minProbBF(0.1)
            .useBackwardForward(false)
            .bfInfoFilename("/Users/peterbradbury/temp/phgtest/bfaNewInfo.txt")
            .minTaxaPerRange(2)
            .minReads(0)
            .requiredTaxaList(TaxaListBuilder().add("LineA").build())
            .algorithmType(BestHaplotypePathPlugin.ALGORITHM_TYPE.classic)
            .performFunction(DataSet.getDataSet(graph));
}


