package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.Range
import com.google.common.collect.TreeRangeSet
import net.maizegenetics.dna.map.Position
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.logging.log4j.LogManager
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon

/**
 * Plugin to score a PAF file by different NM(Edit Distance) thresholds.
 *
 * In PAF files, NM means the number of edits for that alignment.
 * We are looking for a percentage of edit distance over the length of the aligned region.
 *
 * For each line in the PAF file, the aligned number of 'contig' coordinates is compared to the edit distance.
 * If NM/numSites < our current NMthreshold, that aligned region is loaded into a RangeSet.
 * The RangeSet will merge any overlapping regions.
 * Then once all the mappings have been processed, this walks through each contig and determines
 * how many of the bps of the full contig were mapped with a good enough NM.  These values are output to a file created in outputDir.
 *
 * This plugin allows you to run multiple NM thresholds to get an overall idea of how well things map.
 */
class ScorePafFileByNMThresholdPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = LogManager.getLogger(ScorePafFileByNMThresholdPlugin::class.java)

    private var inputPAFFile = PluginParameter.Builder("inputFile",null, String::class.java)
            .guiName("Input PAF file")
            .required(true)
            .inFile()
            .description("Input PAF file")
            .build()

    private var outputDir = PluginParameter.Builder("outputDir",null, String::class.java)
            .guiName("Output Directory")
            .required(true)
            .outDir()
            .description("Directory to output the processed PAF files")
            .build()

    private var editDistanceThresholds = PluginParameter.Builder("NMThresholds",".001,.005,.01,.02,.05,.1,.25", String::class.java)
            .guiName("NMThresholds")
            .required(false)
            .description("Edit distance thresholds separated by comma's")
            .build()

    /**
     * For each line in the PAF file, the aligned number of 'contig' coordinates is compared to the edit distance.
     * If NM/numSites < our current NMthreshold, that aligned region is loaded into a RangeSet.
     * The RangeSet will merge any overlapping regions.
     * Then once all the mappings have been processed, this walks through each contig and determines
     * how many of the bps of the full contig were mapped with a good enough NM.  These values are output to a file created in outputDir.
     */
    override fun processData(input: DataSet?): DataSet? {
        val editDistanceThresholds = editDistanceThresholds().split(",").map{it.toDouble()}.toList()

        editDistanceThresholds.forEach {

            val fileString = "$it".replace(".","NMDist_")
            val inputToken = File(inputPAFFile()).nameWithoutExtension
            val outputMappingFile = "${outputDir()}${inputToken}_${fileString}.txt"

            val maxAllowedEditDistProportion = it

            val reader = Utils.getBufferedReader(inputPAFFile())
            var currentLine: String? = ""

            val contigLengths = mutableMapOf<String, Int>()
            val alignedRegions = TreeRangeSet.create<Position>()

            while (currentLine != null) {
                if (currentLine != "") {
                    val currentLineSplit = currentLine.split("\t")
                    val contigName = currentLineSplit[0].replace(":", "_").toUpperCase()
                    val contigLength = currentLineSplit[1].toInt()
                    val alignedStart = currentLineSplit[2].toInt()
                    val alignedEnd = currentLineSplit[3].toInt()
                    val lengthAligned = alignedEnd - alignedStart
                    val nmTag = currentLineSplit[12].split(":")
                    val editDist = nmTag[2].toInt()


                    val editDistProportion = editDist.toDouble() / lengthAligned
                    //Keep track of the contig lengths
                    if (!contigLengths.containsKey(contigName)) {
                        contigLengths[contigName] = contigLength
                    }

                    if (editDistProportion < maxAllowedEditDistProportion) {
                        //Add to the rangeSet
                        alignedRegions.add(Range.closed(Position.of(contigName, alignedStart), Position.of(contigName, alignedEnd - 1)))
                    }
                }

                currentLine = reader.readLine()
            }


            //This map is used to be able to get the alignment stats for each contig.
            val contigToAlignmentMap = alignedRegions.asRanges().groupBy {alignedRegion -> alignedRegion.lowerEndpoint().chromosome.name }
                    .map { alignedRegion -> Pair(alignedRegion.key, alignedRegion.value.map { posRange -> posRange.upperEndpoint().position - posRange.lowerEndpoint().position }.sum()) }
                    .toMap()

            Utils.getBufferedWriter(outputMappingFile).use { output ->
                output.write("Contig\tContigLength\tTotalCovered\tPercentageMapped\n")
                contigLengths.keys.forEach {contigName ->
                    val totalCovered = if (contigToAlignmentMap.containsKey(contigName)) contigToAlignmentMap[contigName] else 0
                    output.write("$contigName\t${contigLengths[contigName]}\t${totalCovered}\t${(totalCovered?.toDouble()
                            ?: 0.0) / (contigLengths[contigName] ?: 1)}\n")
                }
            }
        }


        return null
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = ScorePafFileByNMThresholdPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "ScorePafFileByNMThresholdPlugin"
    }

    override fun getToolTipText(): String {
        return "ScorePafFileByNMThresholdPlugin to process the PAF file by edit distance and split them out."
    }

    /**
     * Input PAF file
     *
     * @return Input PAF file
     */
    fun inputPAFFile(): String {
        return inputPAFFile.value()
    }

    /**
     * Set Input PAF file. Input PAF file
     *
     * @param value Input PAF file
     *
     * @return this plugin
     */
    fun inputPAFFile(value: String): ScorePafFileByNMThresholdPlugin {
        inputPAFFile = PluginParameter<String>(inputPAFFile, value)
        return this
    }

    /**
     * Directory to output the processed PAF files
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Directory to output the processed
     * PAF files
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): ScorePafFileByNMThresholdPlugin {
        outputDir = PluginParameter<String>(outputDir, value)
        return this
    }

    /**
     * Edit distance thresholds separated by comma's
     *
     * @return NMThresholds
     */
    fun editDistanceThresholds(): String {
        return editDistanceThresholds.value()
    }

    /**
     * Set NMThresholds. Edit distance thresholds separated
     * by comma's
     *
     * @param value NMThresholds
     *
     * @return this plugin
     */
    fun editDistanceThresholds(value: String): ScorePafFileByNMThresholdPlugin {
        editDistanceThresholds = PluginParameter<String>(editDistanceThresholds, value)
        return this
    }
}

fun main() {
    GeneratePluginCode.generateKotlin(ScorePafFileByNMThresholdPlugin::class.java)
}