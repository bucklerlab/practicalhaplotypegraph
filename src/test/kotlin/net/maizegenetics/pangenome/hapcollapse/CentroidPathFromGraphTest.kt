package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.ParameterCache
import org.apache.log4j.BasicConfigurator

fun main() {
    CentroidPathFromGraphTest().testCentroids()
}

class CentroidPathFromGraphTest {
    val pathName = "centroidPathTest"
    val method = "Centroid_Path"

    fun testCentroids() {
        BasicConfigurator.configure()
        calculateCentroids()
        evaluateCentroids()
    }

    private fun calculateCentroids() {
        ParameterCache.load(SmallSeqPaths.dbConfigFile)
        val myGraphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        myGraphBuilder.setConfigParameters()
        val myGraph = myGraphBuilder
            .methods("GATK_PIPELINE")
            .build()

        CentroidPathFromGraphPlugin()
            .pathName(pathName)
            .bedfileName("${SmallSeqPaths.baseDir}/centroidtest.bed")
            .fastaName("${SmallSeqPaths.baseDir}/centroidtest.fa")
            .writeToDB(true)
            .performFunction(DataSet.getDataSet(myGraph))
    }

    private fun evaluateCentroids() {

        val dbConn = DBLoadingUtils.connection(SmallSeqPaths.dbConfigFile, false)

        //list hapids
        val hapidSql = "SELECT line_name, haplotypes_id FROM haplotypes, gamete_haplotypes, gametes, methods, genotypes  " +
                "WHERE haplotypes.gamete_grp_id=gamete_haplotypes.gamete_grp_id AND " +
                "gamete_haplotypes.gameteid=gametes.gameteid AND gametes.genoid=genotypes.genoid AND " +
                "methods.method_id=haplotypes.method_id AND methods.name='GATK_PIPELINE'"
        println("Executing query: $hapidSql \n---------------")
        dbConn.createStatement().executeQuery(hapidSql).use {
            while (it.next()) {
                println("${it.getString(1)}\t${it.getInt(2)}")
            }
        }

        //list hapids in centroid path
        val pathQuery = "SELECT paths_data FROM paths " +
                "WHERE method_id in (SELECT method_id FROM methods WHERE name = '${method}') AND " +
                "genoid in (SELECT genoid FROM genotypes WHERE line_name = '${pathName}')"
        println("Executing query: $pathQuery \n---------------")
        dbConn.createStatement().executeQuery(pathQuery).use {
            it.next()
            val idList = DBLoadingUtils.decodePathsForMultipleLists(it.getBytes(1))
            idList.map { idList -> idList.map { it } }.flatten().forEach { println(it.toString()) }
        }


    }

}