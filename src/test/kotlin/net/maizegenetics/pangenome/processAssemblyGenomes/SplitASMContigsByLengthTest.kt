package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.util.LoggingUtils
import org.junit.AfterClass
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.AfterAll
import java.io.File
import java.util.*
import kotlin.test.fail

class SplitASMContigsByLengthTest {

    val testingDir = "${System.getProperty("user.home")}/phgTests"
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
        File(testingDir).mkdirs()
    }
    @AfterAll
    fun tearDown() {
        File(testingDir).deleteRecursively()
    }

    @Test
    fun testSplit() {
        //make 100 random DNA sequences of length 1000
        val random = Random(12345)
        val randomDNA = (1..100).map { (1..1000).map { random.nextInt(4) }.joinToString("") { "ACGT"[it].toString() } }
        val inputFasta = "$testingDir/randomDNA.fasta"
        val outputFasta = "$testingDir/randomDNA_split1kbp.fasta"

        //batch randomDNA into 10 batches
        val batchedDNA = randomDNA.chunked(10)

        //write to fasta where each batch is a contig
        File(inputFasta).printWriter().use { out ->
            batchedDNA.forEachIndexed { index, batch ->
                out.println(">contig$index")
                out.println(batch.joinToString(""))
            }
        }

        //use SplitASMContigsByLengthPlugin to split each contig into 1000bp chunks
        SplitASMContigsByLengthPlugin(null,false)
                .inputASMFile(inputFasta)
                .outputSplitFile(outputFasta)
                .splitSize(1000)
                .performFunction(null)


        //read in the output fasta and check that each contig is split into 1000bp chunks
        val sequences = File(outputFasta).readLines().filter { !it.startsWith(">") }

        for(idx in sequences.indices) {
            val currentSeq = sequences[idx]
            val currentTruth = randomDNA[idx]
            Assert.assertEquals("Sequence $idx does not match truth", currentTruth, currentSeq)
        }
    }

    @Test
    fun testSplitJaggedEdges(){
        //make 100 random DNA sequences of length 1000
        val random = Random(12345)
        val randomDNA = (1..100).map { (1..1000).map { random.nextInt(4) }.joinToString("") { "ACGT"[it].toString() } }
        val inputFasta = "$testingDir/randomDNA.fasta"
        val outputFasta = "$testingDir/randomDNA_split1kbp.fasta"

        //batch randomDNA into 10 batches
        val batchedDNA = randomDNA.chunked(10)

        //add in a "ACCT" every 10 sequences
        val mutableTruth = randomDNA.toMutableList()
        for(idx in (10..100 step 10).reversed() ) {
            mutableTruth.add(idx, "ACCT")
        }

        //write to fasta where each batch is a contig
        File(inputFasta).printWriter().use { out ->
            batchedDNA.forEachIndexed { index, batch ->
                out.println(">contig$index")
                out.println("${batch.joinToString("")}ACCT")
            }
        }

        //use SplitASMContigsByLengthPlugin to split each contig into 1000bp chunks
        SplitASMContigsByLengthPlugin(null,false)
            .inputASMFile(inputFasta)
            .outputSplitFile(outputFasta)
            .splitSize(1000)
            .performFunction(null)


        //read in the output fasta and check that each contig is split into 1000bp chunks
        val sequences = File(outputFasta).readLines().filter { !it.startsWith(">") }

        for(idx in sequences.indices) {
            val currentSeq = sequences[idx]
            val currentTruth = mutableTruth[idx]
            Assert.assertEquals("Sequence $idx does not match truth", currentTruth, currentSeq)
        }
    }
}