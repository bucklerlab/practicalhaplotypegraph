package net.maizegenetics.pangenome.hapCalling

import htsjdk.variant.variantcontext.*
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.junit.SmallSeqExtension
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.smallseq.RunSmallSeqTestsDockerTest.compareResultAndAnswer
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import kotlin.test.fail

@ExtendWith(SmallSeqExtension::class)
class SNPToReadMappingPluginIT {

    //get the current time for the method name
    val currentLocalDateTime = LocalDateTime.now()

    //NOTE: Must run IndexHaplotypesBySNPPluginTest first

    companion object {
        //get the current time for the method name
        val currentLocalDateTime = LocalDateTime.now()

        val snpToReadMappingDir = "${SmallSeqExtension.smallSeqDir}/SnpToReadMappingPluginIT/"

        val keyFileStub = "${snpToReadMappingDir}/SNPToReadMappingKeyFile"
        val indexFile = "${snpToReadMappingDir}/testVCFIndex_Streaming.txt"
        val vcfDirectory = "${snpToReadMappingDir}/vcfs/"
        val debugDir = "${snpToReadMappingDir}/debugOutput/"
        val readMappingDir = "${debugDir}readmappings/"
        val methodName = "VCFUpload_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val pathMethodName = "path_VCFUpload_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val methodDesc = "VCFReadMapping"
        val configFile = SmallSeqExtension.configFilePath

        val outputPathKeyFileStub = "${snpToReadMappingDir}/pathKeyFile"
        val outputPathDir = "${snpToReadMappingDir}/paths/"
        val finalVCFFile = "${snpToReadMappingDir}/phg.vcf"

        val withADVCFFileName = "output_withAD.vcf"


        @JvmStatic
        @BeforeAll
        fun setup() {
            LoggingUtils.setupDebugLogging()

            File(snpToReadMappingDir).mkdirs()
            File(readMappingDir).mkdirs()
            File(vcfDirectory).mkdirs()
            File(debugDir).mkdirs()
            File(outputPathDir).mkdirs()

            ParameterCache.load(configFile)

            Utils.getBufferedWriter("${keyFileStub}_withAd.txt").use { output ->
                output.write("filename\tflowcell_lane\n")
                output.write("${withADVCFFileName}\tvcfUpload${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
            }
        }
    }

    @Test
    fun testVCFDummyFilesNoAD() {
        val indexFile = "${snpToReadMappingDir}simpleVCFIndex.txt"
        val vcfDirectory = "${snpToReadMappingDir}vcfs/"
        val vcfFileName = "simpleVCFFile.vcf"
        val methodName = "TestUpload_NoAD_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val methodDesc = "TestDescrption"

        val keyFileName = "${snpToReadMappingDir}SNPToReadMappingKeyFile_NoAD.txt"
        val outputPathKeyFile = "${snpToReadMappingDir}SNPToReadMappingKeyFile_NoAD_paths.txt"
        val readMappingDir = "${snpToReadMappingDir}readMappings_NoAD/"

        File(vcfDirectory).mkdirs()
        File(readMappingDir).mkdirs()


        buildSNPIndexFile(indexFile)
        buildSimpleVCFFile("${vcfDirectory}${vcfFileName}")

        Utils.getBufferedWriter(keyFileName).use { output ->
            output.write("filename\tflowcell_lane\n")
            output.write("$vcfFileName\tvcfUpload${Companion.currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
        }

        SNPToReadMappingPlugin()
            .keyFile(keyFileName)
            .vcfDirectory(vcfDirectory)
            .indexFile(indexFile)
            .methodName(methodName)
            .methodDescription(methodDesc)
            .countAD(false)
            .outputPathKeyFile(outputPathKeyFile)
            .readMappingOutputDir(readMappingDir)
            .updateDB(false)
            .performFunction(null)

        val expectedReadmappings = mapOf<String,Set<Pair<String,Int>>>(
            "sample1" to setOf(Pair("10,20",12),Pair("10",6),Pair("10,20,30",6), Pair("60,70,80",6),Pair("60,80",6),Pair("60",6)),
            "sample2" to setOf(Pair("30,40",12),Pair("20,30,40",6),Pair("40",6), Pair("90",6),Pair("70,90",6),Pair("70,80,90",6)),
            "sample3" to setOf(Pair("10,20",6),Pair("20,30,40",6),Pair("30,40",6), Pair("10,20,30",6),Pair("90",6),Pair("60,80",6), Pair("60",6))
        )

        //load in the readMappings into something we can compare
        File(readMappingDir).walk().forEach { file ->
            if (file.isFile) {
                val sampleName = file.nameWithoutExtension.split("_")[0]
                val readMappings = mutableSetOf<Pair<String,Int>>()
                Utils.getBufferedReader(file.absolutePath).use { input ->
                    input.readLines().filter{!it.startsWith("#") && !it.startsWith("HapIds")}.forEach { line ->
                        val splitLine = line.split("\t")
                        val readMapping = splitLine[0]
                        val readCount = splitLine[1].toInt()
                        readMappings.add(Pair(readMapping,readCount))
                    }
                }
                assertEquals(expectedReadmappings[sampleName],readMappings, "Sample $sampleName readMappings do not match expected")
            }
        }
    }

    @Test
    fun testVCFDummyFilesWithAD() {
        val indexFile = "${snpToReadMappingDir}simpleVCFIndex.txt"
        val vcfDirectory = "${snpToReadMappingDir}vcfs/"
        val vcfFileName = "simpleVCFFile.vcf"
        val methodName = "TestUpload_AD_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val methodDesc = "TestDescription"

        val keyFileName = "${snpToReadMappingDir}SNPToReadMappingKeyFile_AD.txt"
        val outputPathKeyFile = "${snpToReadMappingDir}SNPToReadMappingKeyFile_AD_paths.txt"
        val readMappingDir = "${snpToReadMappingDir}readMappings_AD/"

        File(vcfDirectory).mkdirs()
        File(readMappingDir).mkdirs()


        buildSNPIndexFile(indexFile)
        buildSimpleVCFFile("${vcfDirectory}${vcfFileName}")

        Utils.getBufferedWriter(keyFileName).use { output ->
            output.write("filename\tflowcell_lane\n")
            output.write("$vcfFileName\tvcfUpload${Companion.currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
        }

        SNPToReadMappingPlugin()
            .keyFile(keyFileName)
            .vcfDirectory(vcfDirectory)
            .indexFile(indexFile)
            .methodName(methodName)
            .methodDescription(methodDesc)
            .countAD(true)
            .outputPathKeyFile(outputPathKeyFile)
            .readMappingOutputDir(readMappingDir)
            .updateDB(false)
            .performFunction(null)

        val expectedReadmappings = mapOf<String,Set<Pair<String,Int>>>(
            "sample1" to setOf(Pair("10,20",20),Pair("10",10),Pair("10,20,30",10), Pair("60,70,80",60),Pair("60,80",60),Pair("60",60)),
            "sample2" to setOf(Pair("30,40",20),Pair("20,30,40",10),Pair("40",10), Pair("90",70),Pair("70,90",70),Pair("70,80,90",70)),
            "sample3" to setOf(Pair("10,20",10),Pair("20,30,40",10),Pair("30,40",10), Pair("10,20,30",10),Pair("90",80),Pair("60,80",80), Pair("60",80))
        )

        //load in the readMappings into something we can compare
        File(readMappingDir).walk().forEach { file ->
            if (file.isFile) {
                val sampleName = file.nameWithoutExtension.split("_")[0]
                val readMappings = mutableSetOf<Pair<String,Int>>()
                Utils.getBufferedReader(file.absolutePath).use { input ->
                    input.readLines().filter{!it.startsWith("#") && !it.startsWith("HapIds")}.forEach { line ->
                        val splitLine = line.split("\t")
                        val readMapping = splitLine[0]
                        val readCount = splitLine[1].toInt()
                        readMappings.add(Pair(readMapping,readCount))
                    }
                }
                assertEquals(expectedReadmappings[sampleName],readMappings, "Sample $sampleName readMappings do not match expected")
            }
        }
    }

    @Test
    fun testSNPToReadMappingDBWrite() {
        val indexFile = "${snpToReadMappingDir}simpleVCFIndex.txt"
        val vcfDirectory = "${snpToReadMappingDir}vcfs/"
        val vcfFileName = "simpleVCFFile.vcf"
        val methodName = "TestUpload_NoAD_DB_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val methodDesc = "TestDescription"

        val keyFileName = "${snpToReadMappingDir}SNPToReadMappingKeyFile_NoAD_DB.txt"
        val outputPathKeyFile = "${snpToReadMappingDir}SNPToReadMappingKeyFile_NoAD_DB_paths.txt"
        val readMappingDir = "${snpToReadMappingDir}readMappings_NoAD_DB/"

        ParameterCache.load(SmallSeqExtension.configFilePath)


        val dbConnect = DBLoadingUtils.connection(false)
        val phgDBAccess = PHGdbAccess(dbConnect)
        val decoder = ReadMappingDecoder(phgDBAccess)

        File(vcfDirectory).mkdirs()
        File(readMappingDir).mkdirs()


        buildSNPIndexFile(indexFile)
        buildSimpleVCFFile("${vcfDirectory}${vcfFileName}")

        Utils.getBufferedWriter(keyFileName).use { output ->
            output.write("filename\tflowcell_lane\n")
            output.write("$vcfFileName\tvcfUpload${Companion.currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
        }

        val graph = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqExtension.configFilePath)
            .methods("GATK_PIPELINE")
            .includeSequences(false)
            .build()

        SNPToReadMappingPlugin()
            .keyFile(keyFileName)
            .vcfDirectory(vcfDirectory)
            .indexFile(indexFile)
            .methodName(methodName)
            .methodDescription(methodDesc)
            .countAD(false)
            .outputPathKeyFile(outputPathKeyFile)
            .readMappingOutputDir(readMappingDir)
            .updateDB(true)
            .performFunction(null)

        val expectedReadmappings = mapOf<String,Set<Pair<String,Int>>>(
            "sample1" to setOf(Pair("10,20",12),Pair("10",6),Pair("10,20,30",6), Pair("60,70,80",6),Pair("60,80",6),Pair("60",6)),
            "sample2" to setOf(Pair("30,40",12),Pair("20,30,40",6),Pair("40",6), Pair("90",6),Pair("70,90",6),Pair("70,80,90",6)),
            "sample3" to setOf(Pair("10,20",6),Pair("20,30,40",6),Pair("30,40",6), Pair("10,20,30",6),Pair("90",6),Pair("60,80",6), Pair("60",6))
        )

        //load in the readMappings into something we can compare

        //open up the path key file to get out the readMappingIds
        val readMappingIds = mutableMapOf<String,Int>()
        Utils.getBufferedReader(outputPathKeyFile).use { input ->
            input.readLines().filter{!it.startsWith("#") && !it.startsWith("SampleName")}.forEach { line ->
                val splitLine = line.split("\t")
                val sampleName = splitLine[0]
                val readMappingId = splitLine[1].toInt()
                readMappingIds[sampleName] = readMappingId
            }
        }
        File(readMappingDir).walk().forEach { file ->
            if (file.isFile) {
                val sampleName = file.nameWithoutExtension.split("_")[0]
                val readMappingDBId = readMappingIds[sampleName]?: fail("Could not find readMappingId for sample $sampleName")
                val dbReadMappings = decoder.getDecodedReadMappingForMappingId(readMappingDBId).map { Pair(it.key.joinToString(","),it.value) }.toSet()
                val readMappings = mutableSetOf<Pair<String,Int>>()
                Utils.getBufferedReader(file.absolutePath).use { input ->
                    input.readLines().filter{!it.startsWith("#") && !it.startsWith("HapIds")}.forEach { line ->
                        val splitLine = line.split("\t")
                        val readMapping = splitLine[0]
                        val readCount = splitLine[1].toInt()
                        readMappings.add(Pair(readMapping,readCount))
                    }
                }
                assertEquals(expectedReadmappings[sampleName],readMappings, "Sample $sampleName readMappings do not match expected")
                assertEquals(expectedReadmappings[sampleName],dbReadMappings, "Sample $sampleName DB readMappings do not match expected")
            }
        }




    }

    fun buildSNPIndexFile(indexFileName : String) : List<IndexRow> {
        val indexRows = mutableListOf<IndexRow>()
        Utils.getBufferedWriter(indexFileName).use { output ->
            output.write("refRangeId\tchr\tposition\trefAllele\taltAllele\trefHapIds\taltHapIds\n")
            indexRows.add(IndexRow(1, "1", 10, "G", "T", listOf(10, 20), listOf(30, 40)))
            indexRows.add(IndexRow(1, "1", 20, "A", "T", listOf(10), listOf(20, 30, 40)))
            indexRows.add(IndexRow(1, "1", 30, "G", "T", listOf(10, 20), listOf(30, 40)))
            indexRows.add(IndexRow(1, "1", 40, "A", "T", listOf(10, 20, 30), listOf(40)))
            indexRows.add(IndexRow(2, "1", 6500, "T", "C", listOf(60, 70, 80), listOf(90)))
            indexRows.add(IndexRow(2, "1", 6510, "T", "C", listOf(60, 80), listOf(70, 90)))
            indexRows.add(IndexRow(2, "1", 6520, "T", "C", listOf(60), listOf(70, 80, 90)))

            for (indexRow in indexRows) {
                output.write(
                    "${indexRow.refRangeId}\t${indexRow.chr}\t${indexRow.position}\t${indexRow.refAllele}\t${indexRow.altAllele}\t${
                        indexRow.refHapIds.joinToString(
                            ","
                        )
                    }\t${indexRow.altHapIds.joinToString(",")}\n"
                )
            }
        }
        return indexRows
    }

    fun buildSimpleVCFFile(vcfFileName : String) : List<VariantContext> {
        val samples = listOf("sample1", "sample2", "sample3")

        val variants = mutableListOf<VariantContext>()

        val vcfFileWriter = VariantContextWriterBuilder()
            .setOutputFile(vcfFileName)
            .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
            .unsetOption(Options.INDEX_ON_THE_FLY)
            .build()
        vcfFileWriter.writeHeader(HapCallingUtils.createGenericHeader(samples))

        variants.add(VariantContextBuilder()
                            .id("1")
                            .chr("1")
                            .start(10)
                            .stop(10)
                            .alleles(listOf(Allele.create("G", true), Allele.create("T")))
                            .genotypes(
                                GenotypeBuilder("sample1", listOf(Allele.create("G", true),Allele.create("G", true)))
                                    .AD(intArrayOf(10,0)).DP(10).make(),
                                GenotypeBuilder("sample2", listOf(Allele.create("T"),Allele.create("T")))
                                    .AD(intArrayOf(0, 10)).DP(10).make(),
                                GenotypeBuilder("sample3", listOf(Allele.create("G", true),Allele.create("G", true)))
                                    .AD(intArrayOf(10,0)).DP(10).make()
                            ).make())

        variants.add(VariantContextBuilder()
            .id("2")
            .chr("1")
            .start(20)
            .stop(20)
            .alleles(listOf(Allele.create("A", true), Allele.create("T")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("A", true),Allele.create("A", true)))
                    .AD(intArrayOf(10,0)).DP(10).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("T"),Allele.create("T")))
                    .AD(intArrayOf(0, 10)).DP(10).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("T"),Allele.create("T",)))
                    .AD(intArrayOf(0,10)).DP(10).make()
            ).make())

        variants.add(VariantContextBuilder()
            .id("3")
            .chr("1")
            .start(30)
            .stop(30)
            .alleles(listOf(Allele.create("G", true), Allele.create("T")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("G", true),Allele.create("G", true)))
                    .AD(intArrayOf(10,0)).DP(10).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("T"),Allele.create("T")))
                    .AD(intArrayOf(0, 10)).DP(10).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("T"),Allele.create("T")))
                    .AD(intArrayOf(0,10)).DP(10).make()
            ).make())

        variants.add(VariantContextBuilder()
            .id("4")
            .chr("1")
            .start(40)
            .stop(40)
            .alleles(listOf(Allele.create("A", true), Allele.create("T")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("A", true),Allele.create("A", true)))
                    .AD(intArrayOf(10,0)).DP(10).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("T"),Allele.create("T")))
                    .AD(intArrayOf(0, 10)).DP(10).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("A", true),Allele.create("A", true)))
                    .AD(intArrayOf(10,0)).DP(10).make()
            ).make())

        variants.add(VariantContextBuilder()
            .id("5")
            .chr("1")
            .start(6500)
            .stop(6500)
            .alleles(listOf(Allele.create("T", true), Allele.create("C")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("T", true),Allele.create("T", true)))
                    .AD(intArrayOf(60,0)).DP(60).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("C"),Allele.create("C")))
                    .AD(intArrayOf(0, 70)).DP(70).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("C"),Allele.create("C")))
                    .AD(intArrayOf(0,80)).DP(80).make()
            ).make())

        variants.add(VariantContextBuilder()
            .id("6")
            .chr("1")
            .start(6510)
            .stop(6510)
            .alleles(listOf(Allele.create("T", true), Allele.create("C")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("T", true),Allele.create("T", true)))
                    .AD(intArrayOf(60,0)).DP(60).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("C"),Allele.create("C")))
                    .AD(intArrayOf(0, 70)).DP(70).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("T", true),Allele.create("T", true)))
                    .AD(intArrayOf(80,0)).DP(80).make()
            ).make())

        variants.add(VariantContextBuilder()
            .id("6")
            .chr("1")
            .start(6520)
            .stop(6520)
            .alleles(listOf(Allele.create("T", true), Allele.create("C")))
            .genotypes(
                GenotypeBuilder("sample1", listOf(Allele.create("T", true),Allele.create("T", true)))
                    .AD(intArrayOf(60,0)).DP(60).make(),
                GenotypeBuilder("sample2", listOf(Allele.create("C"),Allele.create("C")))
                    .AD(intArrayOf(0, 70)).DP(70).make(),
                GenotypeBuilder("sample3", listOf(Allele.create("T",true),Allele.create("T",true)))
                    .AD(intArrayOf(80,0)).DP(80).make()
            ).make())




        for(variant in variants) {
            vcfFileWriter.add(variant)
        }
        vcfFileWriter.close()
        return variants
    }



    // Leaving this old test in even though it is not currently used as it gives a good idea of how to run this
    // outside of the normal testing pipeline.
    fun testSNPReadMappings(vcfInputFilename: String, writeToDB: Boolean = false) {

        //create the read mapping directory if it does not already exist
        Files.createDirectories(Paths.get(readMappingDir))
        //erase any old read mapppings to make sure they do not just get re-used
        for (mappingFile in File(readMappingDir).listFiles()) mappingFile.delete()

        ParameterCache.load(configFile)

        Utils.getBufferedWriter(keyFileStub).use { output ->
            output.write("filename\tflowcell_lane\n")
            output.write("${vcfInputFilename}\tvcfUpload${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
        }

        if (writeToDB) {
            SNPToReadMappingPlugin()
                .keyFile(keyFileStub)
                .vcfDirectory(vcfDirectory)
                .indexFile(indexFile)
                .methodName(methodName)
                .methodDescription(methodDesc)
                .countAD(true)
                .outputPathKeyFile(outputPathKeyFileStub)
                .performFunction(null)

            println("Done creating ReadMappings")

            println("Done loading mappings to DB.")
        } else {
            SNPToReadMappingPlugin()
                .keyFile(keyFileStub)
                .vcfDirectory(vcfDirectory)
                .indexFile(indexFile)
                .readMappingOutputDir(readMappingDir)
                .methodName(methodName)
                .methodDescription(methodDesc)
                .countAD(true)
                .outputPathKeyFile(outputPathKeyFileStub)
                .performFunction(null)

            println("Done creating ReadMappings")

//        //Load in the ReadMappings to the DB
            ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(configFile)
                .outputPathFindingKeyFile(outputPathKeyFileStub)
                .inputDir(readMappingDir)
                .readMappingMethod(methodName)
                .loadFromDB(false)
                .performFunction(null)

            println("Done loading mappings to DB.")

        }

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeVariantContexts(false)
                .includeSequences(false)
                .configFile(configFile)
                .methods("CONSENSUS")
                .performFunction(null)
        //Then run BestHaplotypePathPlugin
        BestHaplotypePathPlugin(null,false)
                .readMethodName(methodName)
                .pathMethodName(pathMethodName)
                .keyFile(outputPathKeyFileStub)
                .maxNodesPerRange(30)
                .minTaxaPerRange(1)
                .minReads(0)
                .maxNodesPerRange(1000)
                .minTransitionProb(0.0001)
                .probReadMappedCorrectly(0.99)
                .splitConsensusNodes(true)
                .performFunction(graph)


        //Compare the paths to the paths we know about.
        val graphWithVariants = HaplotypeGraphBuilderPlugin(null,false)
                .includeVariantContexts(true)
                .includeSequences(false)
                .configFile(configFile)
                .methods("CONSENSUS")
                .performFunction(null)

        val paths = ImportDiploidPathPlugin()
            .pathMethodName(pathMethodName)
            .performFunction(graphWithVariants)

        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(finalVCFFile)
            .performFunction(paths)

        compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", finalVCFFile)

    }

    fun addDepthToVcf(inVcf: String, outVcf: String) {
        //use this to add depth to output.vcf for testing
        val vcfFileReader = VCFFileReader(File(inVcf), false)
        val vcfFileWriter = VariantContextWriterBuilder()
            .setOutputFile(outVcf)
            .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
            .unsetOption(Options.INDEX_ON_THE_FLY)
            .build()
        vcfFileWriter.writeHeader(vcfFileReader.fileHeader)

        val vcfIterator = vcfFileReader.iterator()
        while (vcfIterator.hasNext()) {
            val myVariantContext = vcfIterator.next()
            val myGenotypeContext = myVariantContext.genotypes
            val myVCBuilder = VariantContextBuilder(myVariantContext)

            val genotypesWithDepth = ArrayList<Genotype>()
            for (geno in myGenotypeContext) {
                val genoBuilder = GenotypeBuilder(geno)
                val myAlleles = geno.alleles
                val ADvalue = intArrayOf(0,0)
                for (alleleIndex in (0..1)) {
                    if (myAlleles[alleleIndex].isReference) ADvalue[0] += 2 else ADvalue[1] += 2
                }
                genoBuilder.AD(ADvalue)
                genotypesWithDepth.add(genoBuilder.make())
            }

            myVCBuilder.genotypes(GenotypesContext.create(genotypesWithDepth))
            vcfFileWriter.add(myVCBuilder.make())
        }

        vcfIterator.close()
        vcfFileReader.close()
        vcfFileWriter.close()
    }
    
}