#!/bin/bash -x
# Run CreateSmallGenomesPlugin with a config file to set parameters

# The configFile parameter must be the fully mounted path provided by the user
# Create a docker container invoking this shell script with the config file as parameter
# The config file should contain all the CreateSmallGenomePlugin parameters whose value
# the user wishes to change from the default, e.g.

# CreateSmallGenomesPlugin.geneLength=2500
# CreateSmallGenomesPlugin.interGeneLength=3000
# CreateSmallGenomesPlugin.numberOfGenes=30
# CreateSmallGenomesPlugin.refInterGeneDelete=0.3
# 
# Example script to run docker container and execute this script: 
#  docker run --name cbsu_loadgenomes_container --rm \
#       -v /workdir/lcj34/data/smallSeqConfig.txt:/tempFileDir/data/smallSeqConfig.txt
#       -t phgrepository_test:latest \
#       /CreateSmallDataSet.sh /tempFileDir/data/smallSeqConfig.txt

configFile=$1

/tassel-5-standalone/run_pipeline.pl -configParameters $configFile -Xmx10g -debug -CreateSmallGenomesPlugin  -endPlugin 
